﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.ComponentModel;
using System.Diagnostics;

namespace ColumnMenuExtender
{
	public delegate Brush GetColorForRow(int rowNum, CurrencyManager source);
	public class FormattableTextBoxColumn : DataGridTextBoxColumn, IExtendedQuery
	{
		private string oldText;
		private bool isEditing;

		[DefaultValue(false)]
		public bool AllowExpand { get; set; }

		//Для того чтобы определить метод, определяющий цвет строки (например, если какое-то значение строки больше чего-либо - цвет бэкграунда строки розовый)

		public event GetColorForRow OnGetColorForRow;
		public event GetColorForRow OnGetForeColorForRow;

		public event FormatCellEventHandler SetCellFormat;
		public event ValidateCellEventHandler CommitCell;
		/// <summary>
		/// Remember how many times the GetMinimumHeight method is called.
		/// </summary>
		protected int currentIteration;

		public FormattableTextBoxColumn()
		{
			AllowExpand = false;
			//TextBox.TextChanged += new EventHandler(TextBox_TextChanged);
		}

		//used to fire an event to retrieve formatting info
		//and then draw the cell with this formatting info
		protected override void Paint(Graphics g, Rectangle bounds, CurrencyManager source, int rowNum, Brush backBrush, Brush foreBrush, bool alignToRight)
		{
			var backBr = backBrush;
			var foreBr = foreBrush;
			var e = new DataGridFormatCellEventArgs(GetColumnValueAtRow(source, rowNum), source.List[rowNum]);

			bool isNewBrush = SetCellColor(rowNum, ref backBrush, source);
			if (!isNewBrush)
			{
				if (backBrush != backBr)
				{
					backBrush.Dispose();
					backBrush = backBr;
				}
			}
			bool isNewForeBrush = SetCellForeColor(rowNum, ref foreBrush, source);
			if (!isNewForeBrush)
			{
				if (foreBrush != foreBr)
				{
					foreBrush.Dispose();
					foreBrush = foreBr;
				}
			}

			//fire the formatting event
			if (SetCellFormat != null)
			{
				SetCellFormat(this, e);
				if (e.BackBrush != null)
				{
					backBrush.Dispose();
					backBrush = e.BackBrush;
					isNewBrush = false;
				}
				if (e.ForeBrush != null)
				{
					foreBrush.Dispose();
					foreBrush = e.ForeBrush;
					isNewForeBrush = false;
				}
			}
			if (((SolidBrush)backBrush).Color == DataGridTableStyle.SelectionBackColor)
			{
				if (backBrush != backBr)
				{
					backBrush.Dispose();
					backBrush = backBr;
					isNewBrush = false;
				}
			}
			if (((SolidBrush)foreBrush).Color == DataGridTableStyle.SelectionForeColor)
			{
				if (foreBrush != foreBr)
				{
					foreBrush.Dispose();
					foreBrush = foreBr;
					isNewForeBrush = false;
				}
			}
			if (e.TextFont == null)
				e.TextFont = DataGridTableStyle.DataGrid.Font;
			g.FillRectangle(backBrush, bounds);
			var saveRegion = g.Clip;
			var rect = new Rectangle(bounds.X, bounds.Y, bounds.Width, bounds.Height);
			using (var newRegion = new Region(rect))
			{
				g.Clip = newRegion;

				string s = null;
				if (Format == "" && GetColumnValueAtRow(source, rowNum) != null)
					s = GetColumnValueAtRow(source, rowNum).ToString();
				else
				{
					object o = GetColumnValueAtRow(source, rowNum);
					if (o is DateTime) s = ((DateTime)o).ToString(Format, FormatInfo);
					else if (o is int) s = ((int)o).ToString(Format, FormatInfo);
					else if (o is Int64) s = ((Int64)o).ToString(Format, FormatInfo);
					else if (o is decimal) s = ((decimal)o).ToString(Format, FormatInfo);
					else if (o is double) s = ((double)o).ToString(Format, FormatInfo);
					else if (o is float) s = ((float)o).ToString(Format, FormatInfo);
				}

				try
				{
					g.DrawString(s, e.TextFont, foreBrush, bounds.X, bounds.Y + 2);
				}
				finally
				{
					g.Clip = saveRegion;
				}
			}
			//clean up
			if (e.BackBrushDispose)
				e.BackBrush.Dispose();
			if (e.ForeBrushDispose)
				e.ForeBrush.Dispose();
			if (e.TextFontDispose)
				e.TextFont.Dispose();
			if (isNewBrush)
				backBrush.Dispose();
			if (isNewForeBrush)
				foreBrush.Dispose();
		}

		private bool SetCellColor(int rowNum, ref Brush backBrush, CurrencyManager source)
		{
			bool isNewBrush = false;

			if (!(DataGridTableStyle.DataGrid is ExtendedDataGrid))
				return false;
			var dataGrid = (ExtendedDataGrid)DataGridTableStyle.DataGrid;
			var RowColorsTable = dataGrid.RowColorsTable;
			//число строк в этой таблицe
			var rowColorCount = (dataGrid.AllowCustomRowColorList ? RowColorsTable.Rows.Count : 0);

			if (rowColorCount > 0)
			{ //если были указаны цвета для отображения грида
				//в последней строкие таблицы RowColorsTable хранится общее число строк,
				//для которых указана цветновая последовательность. После этой строки начинается повторение				
				//rowsInBatch - число строк в гриде, после которых начинается повторение цветновой последовательности
				var rowsInBatch = (int)RowColorsTable.Rows[rowColorCount - 1]["position"];
				//номер строки в текущем блоке(rowsInBatch)
				var rowColorIndex = rowNum % rowsInBatch + 1;
				if (rowColorIndex == 1) //если начинается прорисовка нового блока, обнуляем позицию строки в таблице RowColorsTable					
					RowColorsTable.ExtendedProperties["currentRowColorsPosition"] = 0;

				//текущая позиция строки в таблице RowColorsTable
				//требуется для того, чтобы несколько раз напрасно не пробегать по таблице  RowColorsTable в поисках требцемой строки
				//т.к. строки которые уже были ипользованы, не будут использоваться до прорисовки следующего блока
				var currentRowColorsPosition = (int)RowColorsTable.ExtendedProperties["currentRowColorsPosition"];
				//ищем строку в которой указан цвет, рисуемой строки
				for (var i = currentRowColorsPosition; i < rowColorCount; i++)
				{
					if (((int)RowColorsTable.Rows[i]["position"]) < rowColorIndex) continue;
					if (((SolidBrush)backBrush).Color != DataGridTableStyle.SelectionBackColor)
					{
						backBrush = new SolidBrush((Color)RowColorsTable.Rows[i]["color"]);
						isNewBrush = true;
					}
					RowColorsTable.ExtendedProperties["currentRowColorsPosition"] = i;
					break;
				}
			}
			else if (OnGetColorForRow != null)
			{//был определён делегат для выбора цвета
				if (((SolidBrush)backBrush).Color != DataGridTableStyle.SelectionBackColor)
				{
					var backBr = OnGetColorForRow(rowNum, source);
					if (backBr != null)
					{
						backBrush = backBr;
						isNewBrush = true;
					}
				}
			}
			else
			{
				if (((SolidBrush)backBrush).Color == DataGridTableStyle.SelectionBackColor)
					return false;

				var backBr = (DataGridTableStyle.DataGrid as ExtendedDataGrid).FireEventGetColorForRow(rowNum, source);
				if (backBr != null)
				{
					backBrush = backBr;
					isNewBrush = true;
				}

			}
			return isNewBrush;
		}
		private bool SetCellForeColor(int rowNum, ref Brush foreBrush, CurrencyManager source)
		{
			if (((SolidBrush)foreBrush).Color == DataGridTableStyle.SelectionForeColor)
				return false;
			bool isNewBrush = false;

			if (OnGetForeColorForRow != null)
			{//был определён делегат для выбора цвета
				Brush foreBr = OnGetForeColorForRow(rowNum, source);
				if (foreBr != null)
				{
					foreBrush = foreBr;
					isNewBrush = true;
				}
			}
			else
			{
				if ((DataGridTableStyle.DataGrid is ExtendedDataGrid))
				{
					Brush foreBr = (DataGridTableStyle.DataGrid as ExtendedDataGrid).FireEventGetForeColorForRow(rowNum, source);
					if (foreBr != null)
					{
						foreBrush = foreBr;
						isNewBrush = true;
					}
				}
			}
			return isNewBrush;
		}


		protected override void Edit(CurrencyManager source, int rowNum, Rectangle bounds, bool readOnly, string instantText, bool cellIsVisible)
		{
			if (!(readOnly || ReadOnly))
			{
				base.Edit(source, rowNum, bounds, false, instantText, cellIsVisible);
				oldText = TextBox.Text;
				TextBox.TextChanged += TextBox_TextChanged;
			}
			else
			{
				int cri = DataGridTableStyle.DataGrid.CurrentRowIndex;
				if (cri > 0 && cri < source.List.Count)
					DataGridTableStyle.DataGrid.UnSelect(cri - 1);
				if (cri < source.List.Count)
					DataGridTableStyle.DataGrid.Select(cri);
				if (cri < DataGridTableStyle.DataGrid.VisibleRowCount - 1)
					DataGridTableStyle.DataGrid.UnSelect(cri + 1);
			}
		}
		protected override void Abort(int rowNum)
		{
			isEditing = false;
			TextBox.TextChanged -= TextBox_TextChanged;
			base.Abort(rowNum);
		}

		protected override bool Commit(CurrencyManager dataSource, int rowNum)
		{

			TextBox.TextChanged -= TextBox_TextChanged;
			if (!isEditing)
				return true;
			isEditing = false;

			try
			{
				object text = TextBox.Text;
				if (text.Equals(NullText))
				{
					text = Convert.DBNull;
					//TextBox.Text = NullText;
				}

				if (CommitCell != null)
				{
					var e = new DataGridValidateCellEventArgs(dataSource, rowNum, text);
					CommitCell(this, e);
					if (!e.IsCommit)
						throw new Exception();
				}
				return base.Commit(dataSource, rowNum);

			}
			catch (Exception)
			{
				TextBox.Text = oldText;
				Abort(rowNum);
				return false;
			}
		}

		#region Public Properties

		[DefaultValue(typeof(string), ""), Category("Misc")]
		public string FieldName { get; set; }

		[DefaultValue(typeof(string), ""), Category("Misc")]
		public string FilterFieldName { get; set; }

		#endregion

		private void TextBox_TextChanged(object sender, EventArgs e)
		{
			TextBox.TextChanged -= TextBox_TextChanged;
			isEditing = true;
		}

		/// <summary>
		/// Reset the iteration count to 0.
		/// </summary>
		public void ResetIterations()
		{
			currentIteration = 0;
		}

		/// <summary>
		/// The actual work is done in this method. 
		/// - First verify if there are rows, find out from CurrencyManager
		/// - increment iteration count
		/// - calculate height of cell at iterationCount - 1
		/// - if iterationCount is rowcount, reset the iterationcount
		/// - return
		/// </summary>
		/// <returns>The height of the requested row.</returns>
		protected override int GetMinimumHeight()
		{
			if (!AllowExpand) return base.GetMinimumHeight();

			var dg = DataGridTableStyle.DataGrid as ExtendedDataGrid;
			if (dg == null || (!dg.AutoRowHeight)) return base.GetMinimumHeight();

			try
			{
				var frame = new StackFrame(4);
				var method = frame.GetMethod();
				var s = method.DeclaringType.FullName;
				if (s.EndsWith("DataGridAddNewRow"))
				{
					ResetIterations();
					return base.GetMinimumHeight();
				}
				var cur = (CurrencyManager)DataGridTableStyle.DataGrid.BindingContext
											[DataGridTableStyle.DataGrid.DataSource, DataGridTableStyle.DataGrid.DataMember];
				if (cur.Count == 0)
					return base.GetMinimumHeight();

				currentIteration++;

				var rowNum = dg.TableStyles[0].GridColumnStyles.IndexOf(this);
				var retVal = base.GetMinimumHeight();

				var stringHeight = CalcStringHeight(GetColumnValueAtRow(cur, currentIteration - 1).ToString());
				if (currentIteration == cur.Count)
					ResetIterations();
				return retVal > stringHeight ? retVal : stringHeight;
			}
			catch
			{
				// Error, return default
				ResetIterations();
				return base.GetMinimumHeight();
			}
		}

		/// <summary>
		/// Calculates the height of the supplied string.
		/// </summary>
		/// <param name="s"></param>
		/// <returns></returns>
		private int CalcStringHeight(string s)
		{
			try
			{
				// Create graphics for calculation
				Graphics g = TextBox.CreateGraphics();
				// Do measure, and add a bit (4 pixels for me)
				return (int)g.MeasureString(s.Trim(), TextBox.Font, Width).Height + 4;
			}
			catch
			{
				// Error, return default font height.
				return base.GetMinimumHeight();
			}
		}
	}

	#region CellFormatting Event

	public delegate void FormatCellEventHandler(object sender, DataGridFormatCellEventArgs e);
	public delegate void ValidateCellEventHandler(object sender, DataGridValidateCellEventArgs e);

	public class DataGridFormatCellEventArgs : DataGridCellEventArgs
	{
		//private int colNum;
		//private int rowNum;
		//private object currentCellValue;

		public DataGridFormatCellEventArgs(object cellValue, object currentRow)
			: base(cellValue, currentRow)
		{
			//			rowNum = row;
			//			colNum = col;
			TextFont = null;
			BackBrush = null;
			ForeBrush = null;
			TextFontDispose = false;
			BackBrushDispose = false;
			ForeBrushDispose = false;
			UseBaseClassDrawing = true;
			//			currentCellValue = cellValue;
		}


		//		//column being painted
		//		public int Column
		//		{
		//			get{ return colNum;}
		//			set{ colNum = value;}
		//		}
		//
		//		//row being painted
		//		public int Row
		//		{
		//			get{ return rowNum;}
		//			set{ rowNum = value;}
		//		}

		//font used for drawing the text
		public Font TextFont { get; set; }

		//background brush
		public Brush BackBrush { get; set; }

		//foreground brush
		public Brush ForeBrush { get; set; }

		//set true if you want the Paint method to call Dispose on the font
		public bool TextFontDispose { get; set; }

		//set true if you want the Paint method to call Dispose on the brush
		public bool BackBrushDispose { get; set; }

		//set true if you want the Paint method to call Dispose on the brush
		public bool ForeBrushDispose { get; set; }

		//set true if you want the Paint method to call base class
		public bool UseBaseClassDrawing { get; set; }
	}
	/// <summary>
	/// Класс с предпологаемыми параметрами редактируемой ячейки
	/// </summary>
	public class DataGridValidateCellEventArgs : EventArgs
	{
		/// <summary>
		/// Создает класс с параметрами редактируемой ячейки
		/// </summary>
		/// <param name="dataSource">Источник данных.</param>
		/// <param name="rowNum">Номер строки.</param>
		/// <param name="value">Предпологаемое значение ячейки.</param>
		public DataGridValidateCellEventArgs(CurrencyManager dataSource, int rowNum, object value)
		{
			CurrencyManager = dataSource;
			RowNum = rowNum;
			Value = value;
		}

		/// <summary>
		/// Источник данных
		/// </summary>
		public CurrencyManager CurrencyManager { get; set; }

		/// <summary>
		/// Номер строки ячейки
		/// </summary>
		public int RowNum { get; set; }

		/// <summary>
		/// Подтверждать ли предпологаемое значение яченйки.
		/// </summary>
		public bool IsCommit { get; set; }

		/// <summary>
		/// Предпологаемое значение ячейки
		/// </summary>
		public object Value { get; set; }
	}
	#endregion
}