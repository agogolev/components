﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.ComponentModel;
using System.Drawing.Design;

namespace ColumnMenuExtender {
	public class ExtendedDataGridSelectorColumnExt : DataGridColumnStyle, IExtendedQuery {
		// Methods
		public ExtendedDataGridSelectorColumnExt() {
			this.xMargin = 0;
			this.yMargin = 0;
			this.OldVal = new string(string.Empty.ToCharArray());
			this.InEdit = false;
			this.btnEllipsis = new Button();
			this.btnEllipsis.Visible = false;
			this.btnEllipsis.Size = new Size(20,15);
			this.btnEllipsis.BackColor = System.Drawing.SystemColors.Control;
			this.btnEllipsis.Click += new EventHandler(btnEllipsis_Click);
		}

		public ImageList ImageList {
			get {
				return btnEllipsis.ImageList;
			}
			set {
				btnEllipsis.ImageList = value;
			}
		}
		public int ImageIndex {
			get {
				return btnEllipsis.ImageIndex;
			}
			set {
				btnEllipsis.ImageIndex = value;
			}
		}

		public string ElipsisText {
			get {
				return this.btnEllipsis.Text;
			}
			set {
				this.btnEllipsis.Text = value;
			}
		}
		public int ElipsisWidth {
			get {
				return this.btnEllipsis.Width;
			}
			set {
				this.btnEllipsis.Width = value;
			}
		}
		public Font ElipsisFont {
			get {
				return this.btnEllipsis.Font;
			}
			set {
				this.btnEllipsis.Font = value;
			}
		}
		public Color ForeColor {
			get {
				return this.btnEllipsis.ForeColor;
			}
			set {
				this.btnEllipsis.ForeColor = value;
			}
	}
		
		protected override void Abort(int RowNum) {
			this.HideBtnEllipsis();
			this.EndEdit();
		}

		protected override bool Commit(CurrencyManager DataSource, int RowNum) {
			this.HideBtnEllipsis();
			if (this.InEdit) {
				try {

				}
				catch {
					return false;
				}
				this.EndEdit();
			}
			return true;
		}

		protected override void ConcedeFocus() {
			this.btnEllipsis.Visible = false;
		}

		protected override void Edit(CurrencyManager Source, int Rownum, Rectangle Bounds, bool ReadOnly, string InstantText, bool CellIsVisible) {
			//Rectangle rectangle1 = Bounds;
			this.source = Source;
			this.rowNum = Rownum;
			if (this.ReadOnly) {
				this.btnEllipsis.Enabled = false;
			}
			else {
				this.btnEllipsis.Enabled = true;
			}


			if (CellIsVisible) {
				Bounds.Offset(this.xMargin, this.yMargin);
				Bounds.Width -= (this.xMargin * 2);
				Bounds.Height -= this.yMargin;
				this.btnEllipsis.Top = Bounds.Top;
				this.btnEllipsis.Left = Bounds.Right - this.btnEllipsis.Width;
				this.btnEllipsis.Height = Bounds.Height;
				this.btnEllipsis.Visible = true;
			}
			else {
				this.btnEllipsis.Top = Bounds.Top;
				this.btnEllipsis.Left = Bounds.Right - this.btnEllipsis.Width;
				this.btnEllipsis.Height = Bounds.Height;
				this.btnEllipsis.Visible = false;
			}

			if (this.btnEllipsis.Visible) {
				this.DataGridTableStyle.DataGrid.Invalidate(Bounds);
			}
			this.InEdit = true;
		}

		private void HideBtnEllipsis() {
			if (this.btnEllipsis.Focused) {
				this.DataGridTableStyle.DataGrid.Focus();
			}
			this.btnEllipsis.Visible = false;
		}
		public void EndEdit() {
			this.InEdit = false;
			this.Invalidate();
		}

		protected override int GetMinimumHeight() {
			return ((this.btnEllipsis.Height + this.yMargin) + 1);
		}

		protected override int GetPreferredHeight(Graphics g, object Value) {
			int num1 = 0;
			int num2 = 0;
			string text1 = this.GetText(Value);
			do {
				num1 = text1.IndexOf("r\n", (int) (num1 + 1));
				num2++;
			}
			while (num1 != -1);
			return ((base.FontHeight * num2) + this.yMargin);
		}

		protected override Size GetPreferredSize(Graphics g, object Value) {
			Size size1 = Size.Ceiling(g.MeasureString(this.GetText(Value), this.DataGridTableStyle.DataGrid.Font));
			size1.Width += ((this.xMargin * 2) + this.DataGridTableGridLineWidth);
			size1.Height += this.yMargin;
			return size1;
		}


		private string GetText(object Value) {
			if (Value == DBNull.Value) {
				return this.NullText;
			}
			if (Value != null) {
				return Value.ToString();
			}
			return string.Empty;

		}

		protected override void Paint(Graphics g, Rectangle Bounds, CurrencyManager Source, int RowNum) {
			this.Paint(g, Bounds, Source, RowNum, false);
		}

		protected override void Paint(Graphics g, Rectangle Bounds, CurrencyManager Source, int RowNum, bool AlignToRight) {
			Brush brush1 = new SolidBrush(this.DataGridTableStyle.BackColor);
			Brush brush2 = new SolidBrush(this.DataGridTableStyle.ForeColor);
			string text1 = this.GetText(this.GetColumnValueAtRow(Source, RowNum));
			g.FillRectangle(brush1, Bounds);
			g.DrawString(text1, this.DataGridTableStyle.DataGrid.Font, brush2, (RectangleF) new Rectangle(Bounds.X, Bounds.Y + 2, Bounds.Width, Bounds.Height));
		}

		protected override void Paint(Graphics g, Rectangle Bounds, CurrencyManager Source, int RowNum, Brush BackBrush, Brush ForeBrush, bool AlignToRight) {
			string text1 = this.GetText(this.GetColumnValueAtRow(Source, RowNum));
			g.FillRectangle(BackBrush, Bounds);
			g.DrawString(text1, this.DataGridTableStyle.DataGrid.Font, ForeBrush, (RectangleF) new Rectangle(Bounds.X, Bounds.Y + 2, Bounds.Width, Bounds.Height));
		}

		protected override void SetDataGridInColumn(DataGrid Value) {
			base.SetDataGridInColumn(Value);
			if ((this.btnEllipsis.Parent != Value) && (this.btnEllipsis.Parent != null)) {
				this.btnEllipsis.Parent.Controls.Remove(this.btnEllipsis);
			}
			if (Value != null) {
				Value.Controls.Add(this.btnEllipsis);
			}
		}
 
		// Properties
		private int DataGridTableGridLineWidth {
			get {
				if (this.DataGridTableStyle.GridLineStyle == DataGridLineStyle.Solid) {
					return 1;
				}
				return 0;
			}
		}


		// Fields
		private Button btnEllipsis;
		private bool InEdit;
		private string OldVal;
		private int xMargin;
		private int yMargin;

		private CurrencyManager source;
		private int rowNum;

		public event CellEventHandler ButtonClick;

		private void btnEllipsis_Click(object sender, EventArgs e) {
			if (ButtonClick != null) {
				ButtonClick(this, new DataGridCellEventArgs(this.GetColumnValueAtRow(this.source, this.rowNum), source.List[this.rowNum]));
			}
		}

		#region Public Properties

		private string fieldName;
		private string filterFieldName;

		[DefaultValue(typeof(string), ""), Category("Misc")]
		public string FieldName {
			set {
				fieldName = value;
			}
			get {
				return fieldName;
			}
		}

		[DefaultValue(typeof(string), ""), Category("Misc")]
		public string FilterFieldName {
			set {
				filterFieldName = value;
			}
			get {
				return filterFieldName;
			}
		}
		#endregion
	}
}
