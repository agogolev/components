﻿using System.Collections;
using System.ComponentModel;

namespace ColumnMenuExtender
{
    /// <summary>
    ///     Summary description for ExtendedDataGridObjectColumn.
    /// </summary>
    public class ExtendedDataGridAggregateColumn : FormattableTextBoxColumn
    {
        public Hashtable Filters = new Hashtable();

        [DefaultValue(typeof (string), ""), Category("Misc")]
        public string ClassName { get; set; }

        [DefaultValue(typeof (string), ""), Category("Misc")]
        public string ClassNameRus { get; set; }
    }
}