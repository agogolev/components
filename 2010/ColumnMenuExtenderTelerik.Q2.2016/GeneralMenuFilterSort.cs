﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using MetaData;
using System.Collections;

namespace ColumnMenuExtender
{
	public abstract class GeneralMenuFilterSort : ContextMenu
	{
		public Hashtable ImagesAndValues;//сюда надо передавать объект типа ImagesAndValues, ключ = ColumnName

		public MenuItem menuItemFilter;
		public MenuItem menuItemSort;
		public MenuItem menuItemSortAsc;
		public MenuItem menuItemSortDesc;
		public MenuItem menuItemSortSep;
		public MenuItem menuItemSortClear;

		public MenuItem menuItemSep;
		public MenuItem menuItemDeleteFilters;

		protected DataColumn _column;

		protected DataGridISM _dataGridISM;		
		
		public GeneralMenuFilterSort() : base()
		{
			ImagesAndValues = new Hashtable();

			menuItemFilter = new MenuItem();
			menuItemFilter.Index = 0;
			menuItemFilter.Text = "Фильтровать";
			menuItemFilter.Click += new EventHandler(this.menuItemFilter_Click);

			menuItemSort = new MenuItem();
			menuItemSort.Index = 1;
			menuItemSort.Text = "Сортировать";

			menuItemSortAsc = new MenuItem();
			menuItemSortAsc.Index = 0;
			menuItemSortAsc.Text = "По возрастанию";
			menuItemSortAsc.Click += new EventHandler(this.menuItemSortAsc_Click);

			menuItemSortDesc = new MenuItem();
			menuItemSortDesc.Index = 1;
			menuItemSortDesc.Text = "По убыванию";
			menuItemSortDesc.Click += new EventHandler(this.menuItemSortDesc_Click);

			menuItemSortSep = new MenuItem();
			menuItemSortSep.Index = 2;
			menuItemSortSep.Text = "-";

			menuItemSortClear = new MenuItem();
			menuItemSortClear.Index = 3;
			menuItemSortClear.Text = "Очистить сортировку";
			menuItemSortClear.Click += new EventHandler(this.menuItemSortClear_Click);

			menuItemSort.MenuItems.AddRange(new MenuItem[] {menuItemSortAsc, menuItemSortDesc, menuItemSortSep, menuItemSortClear});

			menuItemSep = new MenuItem();
			menuItemSep.Index = 2;
			menuItemSep.Text = "-";


			menuItemDeleteFilters = new MenuItem();
			menuItemDeleteFilters.Index = 3;
			menuItemDeleteFilters.Text = "Очистить все фильтры";
			menuItemDeleteFilters.Click += new EventHandler(this.menuItemDeleteFilters_Click);

			this.MenuItems.AddRange(new MenuItem[] {menuItemFilter, menuItemSort, menuItemSep, menuItemDeleteFilters});
		}

		public void Show(Control control, Point pos, string tableName, DataColumn column)
		{
			if (control is DataGridISM)
				_dataGridISM = (DataGridISM)control;

			if (_dataGridISM.Order != null && _dataGridISM.Order.ColumnName == column.ColumnName)
			{
				if (_dataGridISM.Order.order == OrderDir.Asc)
					menuItemSortAsc.Checked = true;
				else
					menuItemSortDesc.Checked = true;
			}

			if (_dataGridISM.StorableFilters[column.ColumnName.ToLower()] != null)
			{
				menuItemFilter.Checked = true;
			}

			if (_dataGridISM.StorableFilters.Count > 0)
			{
				menuItemSep.Enabled = true;
				menuItemDeleteFilters.Enabled = true;
			}
			else
			{
				menuItemSep.Enabled = false;
				menuItemDeleteFilters.Enabled = false;
			}

			if (_dataGridISM.Order != null)
				menuItemSortClear.Enabled = true;			
			else			
				menuItemSortClear.Enabled = false;			

			//_tableName = tableName;
			_column = column;
			base.Show(control, pos);

			menuItemSortAsc.Checked = false;
			menuItemSortDesc.Checked = false;

			menuItemFilter.Checked = false;
		}

		protected void menuItemDeleteFilters_Click(object sender, System.EventArgs e)
		{
			_dataGridISM.StorableFilters.Clear();
			UpdateDataGrid();
		}

		protected virtual void menuItemFilter_Click(object sender, System.EventArgs e)
		{}

		protected void UpdateDataGrid()
		{
			// save config disabled
			//_dataGridISM.SaveFilterSortData();

			UpdateDataGrid(null);
		}

		protected void UpdateDataGrid(string columnName)
		{
			if(columnName != null)
				_dataGridISM.OnFilterSet(columnName);
			_dataGridISM.OnReload(new EventArgs());

		}
		
		protected void menuItemSortAsc_Click(object sender, System.EventArgs e)
		{
			// sort in ascending order
			_dataGridISM.Order = new OrderInfo();
			_dataGridISM.Order.ColumnName = _column.ColumnName;
			_dataGridISM.Order.order = OrderDir.Asc;
			
			UpdateDataGrid();
		}

		protected void menuItemSortDesc_Click(object sender, System.EventArgs e)
		{
			// sort in descending order
			_dataGridISM.Order = new OrderInfo();
			_dataGridISM.Order.ColumnName = _column.ColumnName;
			_dataGridISM.Order.order = OrderDir.Desc;

			UpdateDataGrid();
		}

		protected void menuItemSortClear_Click(object sender, System.EventArgs e)
		{
			// sort in descending order
			_dataGridISM.Order = null;

			UpdateDataGrid();
		}		
	}
}
