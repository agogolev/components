﻿using System;
using System.Windows.Forms;

namespace ColumnMenuExtender
{
	/// <summary>
	/// Summary description for CustomMessageBox.
	/// </summary>
	public class CustomMessageBox
	{
		public static DialogResult Show(string message, string caption)
		{
			fmCustomMessageBox fm = new fmCustomMessageBox();
			fm.Text = caption;
			fm.tbText.Text = message;
			return fm.ShowDialog();
		}
	}
}
