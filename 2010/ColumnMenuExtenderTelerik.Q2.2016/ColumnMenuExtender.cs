﻿using System;
using System.Data;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using ColumnMenuExtender.MetaData;

namespace ColumnMenuExtender
{
	[ProvideProperty("UseGridMenu", typeof(DataGrid)), ProvideProperty("Menu", typeof(DataGridColumnStyle))]
	public partial class ColumnMenuExtender : Component, IExtenderProvider
	{
		private Hashtable Extendees;

		public ColumnMenuExtender()
		{
			this.Extendees = new Hashtable();
		}

		public bool CanExtend(object target)
		{
			return (target is DataGridColumnStyle) || (target is DataGridISM);
		}
		[DefaultValue(null)]
		public ContextMenu GetMenu(DataGridColumnStyle c)
		{
			if (Extendees.ContainsKey(c))
			{
				DataGridColumnStyleInfo info = (DataGridColumnStyleInfo)Extendees[c];

				return info.Menu;
			}
			else
				return null;

		}

		public void SetMenu(DataGridColumnStyle c, ContextMenu menu)
		{
			DataGridColumnStyleInfo info;
			if (!Extendees.ContainsKey(c))
				info = new DataGridColumnStyleInfo();
			else
				info = (DataGridColumnStyleInfo)Extendees[c];
			info.Menu = menu;


			Extendees[c] = info;
		}

		public bool GetUseGridMenu(DataGrid c)
		{
			if (Extendees.ContainsKey(c))
			{
				DataGridInfo info = (DataGridInfo)Extendees[c];

				return info.UseGridMenu;
			}
			else
				return false;
		}

		public void SetUseGridMenu(DataGrid c, bool useGridMenu)
		{
			DataGridInfo info;
			if (!Extendees.ContainsKey(c))
				info = new DataGridInfo();
			else
				info = (DataGridInfo)Extendees[c];

			info.UseGridMenu = useGridMenu;

			if (useGridMenu && !info.EventsWired)
			{
				c.MouseUp += new MouseEventHandler(DataGrid_MouseUp);

				info.EventsWired = true;
			}

			Extendees[c] = info;
		}
		private void DataGrid_MouseUp(object sender, MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Right)
			{
				if (sender is DataGrid)
				{
					DataGrid d = (DataGrid)sender;
					DataGrid.HitTestInfo hti = d.HitTest(e.X, e.Y);
					if (hti.Type == DataGrid.HitTestType.ColumnHeader)
					{
						int n = hti.Column;

						DataTable dt = TableInGrid.GetTable(d);
						DataGridColumnStyle s = d.TableStyles[dt.TableName].GridColumnStyles[n];
						ContextMenu cm = GetMenu(s);
						if (cm != null)
						{
							if (cm is GeneralMenuFilterSort)
							{
								((GeneralMenuFilterSort)cm).Show(d, new Point(e.X, e.Y), dt.TableName, dt.Columns[s.MappingName]);
							}
							else
								cm.Show(d, new Point(e.X, e.Y));
						}
					}
				}
			}
		}



	}

	public class TableInGrid
	{
		public static DataTable GetTable(DataGrid dg)
		{
			DataTable tbl = null;
			if ((dg.DataSource is DataSet) && (dg.DataMember.Length > 0))
			{
				tbl = ((DataSet)dg.DataSource).Tables[dg.DataMember];
			}
			else if (dg.DataSource is DataTable)
				tbl = (DataTable)dg.DataSource;
			else if (dg.DataSource is DataView)
				tbl = ((DataView)dg.DataSource).Table;

			return tbl;
		}

	}
}
