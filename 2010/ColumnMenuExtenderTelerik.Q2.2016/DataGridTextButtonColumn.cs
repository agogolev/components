﻿using System;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using System.ComponentModel;
using System.Diagnostics;
using System.Reflection;

namespace ColumnMenuExtender
{
	/// <summary>
	/// колонка с кнопкой
	/// чтобы использовать эту колонку, надо в форме с таблицей привязаться к событиям нажатия кнопки
	/// dataGridISM1.MouseUp += new MouseEventHandler(dataGridTextButtonColumn1.HandleMouseUp);	
	/// при нажатии кнопки возникает событие CellButtonClicked
	/// </summary>
	public class DataGridTextButtonColumn : DataGridTextBoxColumn
	{
		public event DataGridCellButtonClickEventHandler CellButtonClicked;

		//private Bitmap _buttonFace;
		//private Bitmap _buttonFacePressed;
		//private int _columnNum = -1;
		private int pressedRow = -1;
		public string ButtonText { get; set; }

		public DataGridTextButtonColumn()
		{
			//_columnNum = colNum;
			pressedRow = -1;

			/*try
			{
				System.IO.Stream strm = this.GetType().Assembly.GetManifestResourceStream("DataGridButton.buttonface.bmp");
				_buttonFace = new Bitmap(strm);
				strm = this.GetType().Assembly.GetManifestResourceStream("DataGridButton.buttonfacepressed.bmp");
				_buttonFacePressed = new Bitmap(strm);
			}
			catch{}*/
		}

		protected override void Edit(System.Windows.Forms.CurrencyManager source, int rowNum, System.Drawing.Rectangle bounds, bool readOnly, string instantText, bool cellIsVisible)
		{
			//pressedRow = rowNum;
			// dont call the baseclass so no editing done...
			//	base.Edit(source, rowNum, bounds, readOnly, instantText, cellIsVisible); 
		}

		public void HandleMouseUp(object sender, MouseEventArgs e)
		{
			DataGrid dg = this.DataGridTableStyle.DataGrid;
			DataGrid.HitTestInfo hti = dg.HitTest(new Point(e.X, e.Y));
			int columnNum = this.DataGridTableStyle.GridColumnStyles.IndexOf(this);
			//bool isClickInCell = hti.Column == columnNum && hti.Row > -1;
			Rectangle rect = new Rectangle(0, 0, 0, 0);

			if (pressedRow != -1)
			{
				rect = dg.GetCellBounds(pressedRow, columnNum);
				//isClickInCell = (e.X > rect.Right - this._buttonFace.Width);

				ExtendedDataGrid edg = dg as ExtendedDataGrid;
				string caption = "Button";
				if (edg != null)
				{
					CurrencyManager source = edg.GetCurrencyManager();
					if (source != null)
						caption = this.GetColumnValueAtRow(source, pressedRow).ToString();
				}

				Graphics g = Graphics.FromHwnd(dg.Handle);
				DrawButton(g, rect, hti.Row, caption, ((DataGrid)sender).Font, ((DataGrid)sender).ForeColor);
				g.Dispose();

				if (CellButtonClicked != null)
					CellButtonClicked(this, new DataGridCellButtonClickEventArgs(pressedRow, columnNum));

				pressedRow = -1;
			}
		}

		public void HandleMouseDown(object sender, MouseEventArgs e)
		{
			DataGrid dg = this.DataGridTableStyle.DataGrid;
			DataGrid.HitTestInfo hti = dg.HitTest(new Point(e.X, e.Y));
			int columnNum = this.DataGridTableStyle.GridColumnStyles.IndexOf(this);
			bool isClickInCell = hti.Column == columnNum && hti.Row > -1;
			Rectangle rect = new Rectangle(0, 0, 0, 0);
			if (isClickInCell)
			{
				pressedRow = hti.Row;

				rect = dg.GetCellBounds(hti.Row, hti.Column);
				//isClickInCell = (e.X > rect.Right - this._buttonFace.Width);
				ExtendedDataGrid edg = dg as ExtendedDataGrid;
				string caption = "Button";
				if (edg != null)
				{
					CurrencyManager source = edg.GetCurrencyManager();
					if (source != null)
						caption = this.GetColumnValueAtRow(source, pressedRow).ToString();
				}


				//Console.WriteLine("HandleMouseDown " + hti.Row.ToString());
				Graphics g = Graphics.FromHwnd(dg.Handle);
				//g.DrawImage(this._buttonFacePressed, rect.Right - this._buttonFacePressed.Width, rect.Y);
				DrawButton(g, rect, hti.Row, caption, ((DataGrid)sender).Font, ((DataGrid)sender).ForeColor);
				g.Dispose();
				//_pressedRow = hti.Row;
			}
		}

		protected override void Paint(System.Drawing.Graphics g, System.Drawing.Rectangle bounds, System.Windows.Forms.CurrencyManager source, int rowNum, System.Drawing.Brush backBrush, System.Drawing.Brush foreBrush, bool alignToRight)
		{
			//base.Paint(g, bounds, source, rowNum, backBrush, foreBrush, alignToRight);

			DataGrid parent = this.DataGridTableStyle.DataGrid;
			int columnNum = this.DataGridTableStyle.GridColumnStyles.IndexOf(this);
			bool current = parent.IsSelected(rowNum) || (parent.CurrentRowIndex == rowNum && parent.CurrentCell.ColumnNumber == columnNum);

			Color BackColor = current ? parent.SelectionBackColor : parent.BackColor;
			Color ForeColor = /*current ? parent.SelectionForeColor : */parent.ForeColor;

			g.FillRectangle(backBrush, bounds);//new SolidBrush(BackColor), bounds);

			string s;
			if (string.IsNullOrEmpty(ButtonText))
			{
				if (string.IsNullOrEmpty(Format))
					s = this.GetColumnValueAtRow(source, rowNum).ToString();
				else
					s = string.Format("{0:1}", this.GetColumnValueAtRow(source, rowNum), Format);
			}
			else s = ButtonText;
			DrawButton(g, bounds, rowNum, s, parent.Font, ForeColor);

		}

		private void DrawButton(Graphics g, Rectangle bounds, int rowNum, string text, Font captionFont, Color captionColor)
		{
			Brush surfaceBrush = SystemBrushes.Control;
			Pen leftTopPen1 = SystemPens.ControlLightLight;
			Pen leftTopPen2 = SystemPens.Control;
			Pen rightBottomPen1 = SystemPens.ControlDarkDark;
			Pen rightBottomPen2 = SystemPens.ControlDark;

			if (pressedRow == rowNum)
			{
				surfaceBrush = SystemBrushes.Control;
				leftTopPen1 = SystemPens.ControlDarkDark;
				leftTopPen2 = SystemPens.ControlDark;
				rightBottomPen1 = SystemPens.ControlLightLight;
				rightBottomPen2 = SystemPens.Control;
			}

			g.FillRectangle(surfaceBrush, bounds);
			g.DrawLine(leftTopPen1, bounds.Left, bounds.Top, bounds.Left, bounds.Top + bounds.Height - 1);
			g.DrawLine(leftTopPen1, bounds.Left, bounds.Top, bounds.Left + bounds.Width - 1, bounds.Top);
			g.DrawLine(leftTopPen2, bounds.Left + 1, bounds.Top + 1, bounds.Left + 1, bounds.Top + bounds.Height - 3);
			g.DrawLine(leftTopPen2, bounds.Left + 1, bounds.Top + 1, bounds.Left + bounds.Width - 3, bounds.Top + 1);
			g.DrawLine(rightBottomPen1, bounds.Left + bounds.Width - 1, bounds.Top, bounds.Left + bounds.Width - 1, bounds.Top + bounds.Height - 1);
			g.DrawLine(rightBottomPen1, bounds.Left, bounds.Top + bounds.Height - 1, bounds.Left + bounds.Width - 1, bounds.Top + bounds.Height - 1);
			g.DrawLine(rightBottomPen2, bounds.Left + bounds.Width - 2, bounds.Top + 1, bounds.Left + bounds.Width - 2, bounds.Top + bounds.Height - 2);
			g.DrawLine(rightBottomPen2, bounds.Left + 1, bounds.Top + bounds.Height - 2, bounds.Left + bounds.Width - 2, bounds.Top + bounds.Height - 2);

			StringFormat strFormat = new StringFormat();
			strFormat.Alignment = StringAlignment.Center;
			strFormat.LineAlignment = StringAlignment.Center;
			Brush captionBrush = new SolidBrush(captionColor);
			g.DrawString(text, captionFont, captionBrush, bounds, strFormat);
			captionBrush.Dispose();

		}
		#region GridCellButton click event
		public delegate void DataGridCellButtonClickEventHandler(object sender, DataGridCellButtonClickEventArgs e);

		public class DataGridCellButtonClickEventArgs : EventArgs
		{
			private int _row;
			private int _col;

			public DataGridCellButtonClickEventArgs(int row, int col)
			{
				_row = row;
				_col = col;
			}

			public int RowIndex { get { return _row; } }
			public int ColIndex { get { return _col; } }
		}
		#endregion
	}
}