using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;

namespace ColumnMenuExtender.Forms.Dialogs
{
	public partial class BaseListDialog
	{
		#region Windows Form Designer generated code
		protected DataGridISM dataGridISM1;
		private System.Windows.Forms.ContextMenu contextMenu1;
		private System.Windows.Forms.MenuItem miSelect;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.DataGridTextBoxColumn dataGridTextBoxColumn1;
		private DataGridPager dataGridPager1;
		private System.Windows.Forms.Panel panel1;
		private System.ComponentModel.Container components = null;

		private void InitializeComponent()
		{
			this.dataSet1 = new System.Data.DataSet();
			this.contextMenu1 = new System.Windows.Forms.ContextMenu();
			this.miSelect = new System.Windows.Forms.MenuItem();
			this.menuFilterSort1 = new MenuFilterSort();
			this.columnMenuExtender1 = new ColumnMenuExtender();
			this.dataGridTextBoxColumn1 = new System.Windows.Forms.DataGridTextBoxColumn();
			this.dataGridISM1 = new DataGridISM();
			this.extendedDataGridTableStyle1 = new ExtendedDataGridTableStyle();
			this.panel2 = new System.Windows.Forms.Panel();
			this.dataGridPager1 = new DataGridPager();
			this.panel1 = new System.Windows.Forms.Panel();
			((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataGridISM1)).BeginInit();
			this.panel2.SuspendLayout();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// dataSet1
			// 
			this.dataSet1.DataSetName = "NewDataSet";
			this.dataSet1.Locale = new System.Globalization.CultureInfo("ru-RU");
			// 
			// contextMenu1
			// 
			this.contextMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
						this.miSelect});
			// 
			// miSelect
			// 
			this.miSelect.Index = 0;
			this.miSelect.Text = "Выбрать";
			this.miSelect.Click += new System.EventHandler(this.miSelect_Click);
			// 
			// dataGridTextBoxColumn1
			// 
			this.dataGridTextBoxColumn1.Format = "";
			this.dataGridTextBoxColumn1.FormatInfo = null;
			this.dataGridTextBoxColumn1.MappingName = "OID";
			this.dataGridTextBoxColumn1.Width = 0;
			// 
			// dataGridISM1
			// 
			this.dataGridISM1.BackgroundColor = System.Drawing.SystemColors.Window;
			this.dataGridISM1.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.dataGridISM1.DataMember = "";
			this.dataGridISM1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dataGridISM1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dataGridISM1.Location = new System.Drawing.Point(0, 0);
			this.dataGridISM1.Name = "dataGridISM1";
			this.dataGridISM1.Order = null;
			this.dataGridISM1.ReadOnly = true;
			this.dataGridISM1.Size = new System.Drawing.Size(590, 334);
			this.dataGridISM1.TabIndex = 0;
			this.dataGridISM1.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
						this.extendedDataGridTableStyle1});
			this.columnMenuExtender1.SetUseGridMenu(this.dataGridISM1, true);
			this.dataGridISM1.Reload += new System.EventHandler(this.dataGridISM1_Reload);
			this.dataGridISM1.DoubleClick += new System.EventHandler(this.dataGridISM1_DoubleClick);
			this.dataGridISM1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.dataGridISM1_MouseUp);
			// 
			// extendedDataGridTableStyle1
			// 
			this.extendedDataGridTableStyle1.AllowSorting = false;
			this.extendedDataGridTableStyle1.DataGrid = this.dataGridISM1;
			this.extendedDataGridTableStyle1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.extendedDataGridTableStyle1.MappingName = "table";
			this.extendedDataGridTableStyle1.ReadOnly = true;
			// 
			// panel2
			// 
			this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.panel2.Controls.Add(this.dataGridISM1);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel2.Location = new System.Drawing.Point(0, 0);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(592, 336);
			this.panel2.TabIndex = 3;
			// 
			// dataGridPager1
			// 
			this.dataGridPager1.AllowAll = true;
			this.dataGridPager1.Batch = 30;
			this.dataGridPager1.Location = new System.Drawing.Point(8, 8);
			this.dataGridPager1.Name = "dataGridPager1";
			this.dataGridPager1.PageCount = 0;
			this.dataGridPager1.PageNum = 1;
			this.dataGridPager1.Size = new System.Drawing.Size(288, 24);
			this.dataGridPager1.TabIndex = 1;
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.dataGridPager1);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(0, 336);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(592, 40);
			this.panel1.TabIndex = 2;
			// 
			// BaseListDialog
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(592, 376);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
			this.Name = "BaseListDialog";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Load += new System.EventHandler(this.BaseListDialog_Load);
			((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataGridISM1)).EndInit();
			this.panel2.ResumeLayout(false);
			this.panel1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
	}
}
