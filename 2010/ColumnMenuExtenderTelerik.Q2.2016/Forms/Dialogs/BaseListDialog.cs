﻿using System;
using System.Data;
using System.Text;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using MetaData;
using System.Collections.Generic;

namespace ColumnMenuExtender.Forms.Dialogs {
	/// <summary>
	/// Summary description for BaseListDialog.
	/// </summary>
	public partial class BaseListDialog : BaseGenericForm {
		protected virtual DataSet GetDataSet() {
			return null;
		}

		protected virtual string GetFormName(string ClassName) {
			return "";
		}
				
		#region Vars
		/// <summary>
		/// Набор колонок, где ключ - FieldName колонки, значение - спецефичная колонка
		/// </summary>
		private Hashtable _dataGridColumnStyleArray;
		private Boolean autoSelect = false;
		public string ClassName {
			get { return className; }
		}
		public string FilterString = "";
		public Guid SelectedOID;
		public string SelectedNK;
		public IList<string> SelectedNKs;

		public OrderInfo Order {
			set {dataGridISM1.Order = value;}
			get {return dataGridISM1.Order;}
		}

		public Hashtable Filters {
			//set {dataGridISM1.Filters = value;}
			get {return dataGridISM1.Filters;}
		}

		public DataRow SelectedRow {
			get {return dataGridISM1.GetSelectedRow();}
		}

		public void AddFilters(Hashtable Filters) {
			if (Filters != null)
				foreach(DictionaryEntry	de in Filters)
					dataGridISM1.Filters[de.Key] = (FilterInfo)de.Value;
		}

		public void AddFilter(string Name, FilterInfo Filter) {
			dataGridISM1.Filters[Name] = Filter;
		}

		public string KeyFields {
			get{return this.dataGridISM1.KeyFields;}
			set{this.dataGridISM1.KeyFields=value;}
		}

		public DataGridISM DataGridISM {
			get{return this.dataGridISM1;}
		}

		private System.Data.DataSet dataSet1;
		private string className;
		readonly private IList<string> columnNames;
		readonly private IList<string> fieldNames;
		readonly private IList<string> columnHeaderNames;
		readonly private IList<string> formatRows;
		readonly private int[] widths;
		

		/// <summary>
		/// Required designer variable.
		/// </summary>
		
		
		
		private MenuFilterSort menuFilterSort1;
		private ColumnMenuExtender columnMenuExtender1;
		private ExtendedDataGridTableStyle extendedDataGridTableStyle1;
		
		
		
		
		
		#endregion

		#region .ctror and .dtror
		/// <summary>
		/// Для дизайнера
		/// </summary>
		public BaseListDialog() {
			InitializeComponent();
		}

		public BaseListDialog(string className, IList<string> columnNames, IList<string> columnHeaderNames) {
			InitializeComponent();

			this.className = className;			
			this.columnNames = columnNames;
			this.columnHeaderNames = columnHeaderNames;

			//CreateColumns() лучше вызывать в конструкторе, а то не будут восстанавливаться размеры колонок
			CreateColumns();
		}

		public BaseListDialog(string className, IList<string> columnNames, IList<string> columnHeaderNames, OrderInfo Order, Hashtable Filters) {
			InitializeComponent();

			this.className = className;			
			this.columnNames = columnNames;
			this.columnHeaderNames = columnHeaderNames;

			if(Order != null)
				dataGridISM1.Order = Order;
			if(Filters != null)
				foreach(DictionaryEntry	de in Filters)
					dataGridISM1.Filters[de.Key] = (FilterInfo)de.Value;

			//CreateColumns() лучше вызывать в конструкторе, а то не будут восстанавливаться размеры колонок
			CreateColumns();

		}
		
		public BaseListDialog(string className, IList<string> columnNames, IList<string> columnHeaderNames, OrderInfo Order, Hashtable Filters, int[] widths) {
			InitializeComponent();

			this.className = className;			
			this.columnNames = columnNames;
			this.columnHeaderNames = columnHeaderNames;
			this.widths = widths; 

			if(Order != null)
				dataGridISM1.Order = Order;
			if(Filters != null)
				foreach(DictionaryEntry	de in Filters)
					dataGridISM1.Filters[de.Key] = (FilterInfo)de.Value;
			
			//CreateColumns() лучше вызывать в конструкторе, а то не будут восстанавливаться размеры колонок
			CreateColumns();
		}

		public BaseListDialog(string className, IList<string> fieldNames, IList<string> columnNames, IList<string> columnHeaderNames, IList<string> formatRows, OrderInfo Order, Hashtable Filters, int[] widths) 
			: this(className, fieldNames, columnNames, columnHeaderNames, Order, Filters, widths) {
			this.formatRows = formatRows;
		}

		public BaseListDialog(string className, IList<string> fieldNames, IList<string> columnNames, IList<string> columnHeaderNames, OrderInfo Order, Hashtable Filters, int[] widths) {			
			InitializeComponent();

			this.className = className;			
			this.fieldNames = new string[fieldNames.Count];
			this.columnNames = new string[fieldNames.Count];

			for(int i = 0; i<this.fieldNames.Count; i++) {
				this.fieldNames[i] = (string)fieldNames[i];

				if (columnNames != null && columnNames.Count > i)
					this.columnNames[i] = columnNames[i];
				else this.columnNames[i] = "field"+i.ToString();
			
			}

			this.columnHeaderNames = columnHeaderNames;
			this.widths = widths; 

			if(Order != null)
				dataGridISM1.Order = Order;
			if(Filters != null)
				foreach(DictionaryEntry	de in Filters)
					dataGridISM1.Filters[de.Key] = (FilterInfo)de.Value;

			
			//CreateColumns() лучше вызывать в конструкторе, а то не будут восстанавливаться размеры колонок
			CreateColumns();
		}

		public BaseListDialog(string className, IList<string> fieldNames, IList<string> columnNames, IList<string> columnHeaderNames, Hashtable DataGridColumnStyleArray, int[] widths) {
			InitializeComponent();

			this.className = className;			
			this.fieldNames = fieldNames;
			this.columnNames = new string[fieldNames.Count];

			if (columnNames != null) {
				for(int i = 0; i<this.fieldNames.Count; i++) {
					if (columnNames.Count > i)
						this.columnNames[i] = columnNames[i];
					else this.columnNames[i] = "field"+i.ToString();
				}
			}
			this.columnHeaderNames = columnHeaderNames;
			this._dataGridColumnStyleArray = DataGridColumnStyleArray;
			this.widths = widths;

			//CreateColumns() лучше вызывать в конструкторе, а то не будут восстанавливаться размеры колонок
			CreateColumns();
		}

		public BaseListDialog(string className, IList<string> fieldNames, IList<string> columnNames, IList<string> columnHeaderNames,  IList<string> formatRows, Hashtable DataGridColumnStyleArray, int[] widths)
			: this(className, fieldNames, columnNames, columnHeaderNames, DataGridColumnStyleArray, widths) {
			this.formatRows = formatRows;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		
		#endregion


		private void LoadData() {
			if(!columnsCreated)
				CreateColumns();
			
			try {
				dataSet1 = GetDataSet();//dataSet1 = listProvider.GetList(dataGridISM1.GetDataXml().OuterXml);

				dataGridISM1.Enabled = false;
				dataGridISM1.SetDataBinding(dataSet1, "table");
				dataGridISM1.Enabled = true;
				dataGridISM1.Width++;dataGridISM1.Width--;
			}
			catch(Exception ex) {
				BaseGenericForm.ShowError(this, "Ошибка получения данных", ex);
			}
		}

		private bool columnsCreated = false;

		private void CreateColumns() {
			dataGridISM1.StockClass = className;

			if(fieldNames != null) {
				for(int i = 0; i < fieldNames.Count; i++) {
					if (_dataGridColumnStyleArray != null && _dataGridColumnStyleArray.ContainsKey(fieldNames[i])) {
						DataGridColumnStyle dgcs = (DataGridColumnStyle)_dataGridColumnStyleArray[fieldNames[i]];
						// Заставляем формировать XML в новом формате
						if(dgcs is FormattableTextBoxColumn) 
							((FormattableTextBoxColumn)dgcs).FieldName = fieldNames[i];
						
						//Загрузка свойств из массивов если где-нибудь i окажется больше, считаем, что сами указали в колонке и оставляем всё как есть
						dgcs.MappingName = columnNames[i];
						if (formatRows != null && formatRows.Count > i && dgcs is DataGridTextBoxColumn)//если это textBox то можем формат задать
							((DataGridTextBoxColumn)dgcs).Format = formatRows[i];
						if (dgcs is IExtendedQuery)
							((IExtendedQuery)dgcs).FieldName = fieldNames[i];
						if (columnHeaderNames != null && columnHeaderNames.Count > i)
							dgcs.HeaderText = columnHeaderNames[i];
						if (widths != null && widths.Length > i)
							dgcs.Width = widths[i];

						this.columnMenuExtender1.SetMenu(dgcs, this.menuFilterSort1);
						this.extendedDataGridTableStyle1.GridColumnStyles.Add(dgcs);
					}
					else {
						FormattableTextBoxColumn dgcs = new FormattableTextBoxColumn();
						if (formatRows != null && formatRows.Count > i)
							dgcs.Format = formatRows[i];
						else
							dgcs.Format = "";
						dgcs.FormatInfo = null;
						// Заставляем формировать XML в новом формате
						dgcs.MappingName = columnNames[i];
						dgcs.FieldName = fieldNames[i];
						if (columnHeaderNames != null && columnHeaderNames.Count > i)
							dgcs.HeaderText = columnHeaderNames[i];
						else
							dgcs.HeaderText = "";
						if (widths != null && widths.Length > i)
							dgcs.Width = widths[i];
						else
							dgcs.Width = 75;
						dgcs.NullText = "";

						this.columnMenuExtender1.SetMenu(dgcs, this.menuFilterSort1);
						this.extendedDataGridTableStyle1.GridColumnStyles.Add(dgcs);
					}
				}
				SelectedNKs = new string[columnNames.Count];
			}
			else if (columnNames != null) {		
				for(int i = 0; i < columnNames.Count; i++) {
					FormattableTextBoxColumn dgcs = new FormattableTextBoxColumn();
					if (formatRows != null && formatRows.Count > i)
						dgcs.Format = formatRows[i];
					else
						dgcs.Format = "";					
					dgcs.FormatInfo = null;
					dgcs.MappingName = columnNames[i];
					// Заставляем формировать XML в новом формате
					dgcs.FieldName = columnNames[i];
					if (columnHeaderNames != null && columnHeaderNames.Count > i)
						dgcs.HeaderText = columnHeaderNames[i];
					else
						dgcs.HeaderText = "";
					if (widths != null && widths.Length > i)
						dgcs.Width = widths[i];
					else
						dgcs.Width = 75;
					dgcs.NullText = "";
					this.columnMenuExtender1.SetMenu(dgcs, this.menuFilterSort1);

					this.extendedDataGridTableStyle1.GridColumnStyles.Add(dgcs);
				}
				SelectedNKs = new string[columnNames.Count];
			}
		
			columnsCreated = true;
		}

		private void BaseListDialog_Load(object sender, System.EventArgs e) {
			if (!autoSelect) {
				if (this.Text == "")
					this.Text = GetFormName(className);//this.Text = (string)GenericForm.ClassRUName[className];

				if (!columnsCreated)
					CreateColumns();

				dataGridISM1.FilterString = FilterString;

				LoadData();
				dataGridPager1.BindToDataGrid(dataGridISM1);
			}
		}

		private void miSelect_Click(object sender, System.EventArgs e) {
			this.DialogResult = DialogResult.OK;
			Close();
		}

		private void dataGridISM1_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e) {
			if (e.Button == MouseButtons.Right) {
				DataRow dr = null;
				if ((dr = dataGridISM1.GetSelectedRow()) != null) {
					SelectedOID = (Guid)dr["OID"];
					StringBuilder strB = new StringBuilder("");
					for (int i = 0; i < columnNames.Count; i++) {
						strB.Append(dr[columnNames[i]].ToString() + " ");
						SelectedNKs[i] = dr[columnNames[i]].ToString();
					}
					SelectedNK = strB.ToString();
					contextMenu1.Show(dataGridISM1, new Point(e.X, e.Y));
				}
			}
		}

		private void dataGridISM1_Reload(object sender, System.EventArgs e) {
			LoadData();
		}

		private void dataGridISM1_DoubleClick(object sender, System.EventArgs e) {
			System.Drawing.Point pt = dataGridISM1.PointToClient(Cursor.Position);

			DataGrid.HitTestInfo hti = dataGridISM1.HitTest(pt); 
			if(hti.Type == DataGrid.HitTestType.Cell) {
				DataRow dr = dataGridISM1.GetSelectedRow();
				SelectedOID = (Guid)dr["OID"];
				StringBuilder strB = new StringBuilder("");
				object o;
				for (int i = 0; i < columnNames.Count; i++) {
					if ((o = dr[columnNames[i]]) != System.DBNull.Value)
						strB.Append(o.ToString() + " ");
					SelectedNKs[i] = o.ToString();
				}
				SelectedNK = strB.ToString();
				
				this.DialogResult = DialogResult.OK;
				Close();
			}
		}

		public void SetMultiFilter(Guid[] OIDs, string classContainer, string propName) {
			dataGridISM1.SetMultiFilter(OIDs, classContainer, propName);
		}

		public void SetMultiFilter(Guid OID, string classContainer, string propName) {
			dataGridISM1.SetMultiFilter(OID, classContainer, propName);
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			dataGridPager1.BindToDataGrid(dataGridISM1);
			LoadData();
			dataGridPager1.UpdatePager(this, e);
		}
		public DialogResult ShowDialog(System.Windows.Forms.IWin32Window owner = null, bool AutoSelect = true) {
			this.autoSelect = AutoSelect;

			if (AutoSelect) {
				if (this.Text == "")
					this.Text = GetFormName(className);//this.Text = (string)GenericForm.ClassRUName[className];

				if (!columnsCreated)
					CreateColumns();

				dataGridISM1.FilterString = FilterString;
			}
			if (owner != null)
				return base.ShowDialog(owner);
			else
				return base.ShowDialog();
		}
	}
}
