﻿using System;
using System.Collections;
using System.Drawing;

namespace ColumnMenuExtender.Forms
{
	/// <summary>
	/// Summary description for IfmMain.
	/// </summary>
	public interface IfmMain
	{
		Font Font {get; set;}

		bool isSaveFilters {get; set;}
		bool isCloseWarningShow {get; set;}
		bool isCloseAfterSave {get; set;}
		bool isExportBeforePrint {get; set;}
		
		/// <summary>
		/// Специально поменял название -  чтобы везде изменили логику
		/// 1. обновить upGetSession (последняя версия от 11.10.2006 в базах: WLSites, WhiteBridge, StockISM)
		/// 2. в MainDataGrid ListProvider не делать статическим!!! - иначе при отключении\подключении новый Ticket не зачитается
		/// 3. fmMain тоже поменялась логика работы касательно AuthorizeProvider (смотреть в WLSites, WhiteBridge)
		/// 4. в ServiceUtility надо добавить provider.SecurityHeaderValue = new WLClient.AuthorizeProvider.SecurityHeader();
		/// </summary>
		string CurrentTicket {get;}

		Guid Manager {get; set;}
		string ManagerName {get; set;}
	}

}
