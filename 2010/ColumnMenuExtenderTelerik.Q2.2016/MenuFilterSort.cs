﻿using System;
using System.Globalization;
using System.Windows.Forms;
using MetaData;
using Telerik.WinControls.UI;

namespace ColumnMenuExtender
{
	public class MenuFilterSort : GeneralMenuFilterSort
	{
		protected override void menuItemFilter_Click(object sender, EventArgs e)
		{
			DataGridTableStyle dgts = _dataGridISM.DataMember != "" ? _dataGridISM.TableStyles[_dataGridISM.DataMember] : _dataGridISM.TableStyles[0];
			//фильтрация для булевых колонок (Boolean)
			//фильтрация для колонок с картинками (Int32,Boolean)
			if (dgts != null && dgts.GridColumnStyles[_column.ColumnName] is FormattableBooleanColumn)
			{//0
				#region ...
				var fbf = new FilterBooleanForm();
				var _filter = (IntegerFilterInfo)_dataGridISM.StorableFilters[_column.ColumnName.ToLower()];
				if (_filter != null)//если фильтр уже задан, выставим соответствующее значение
					fbf.checkBox1.Checked = (_filter.Values[0] != 0);

				if (fbf.ShowDialog() == DialogResult.OK)
				{
			
					if (fbf.ToClear && _filter != null)
					{
						_dataGridISM.StorableFilters.Remove(_filter.ColumnName);
						UpdateDataGrid();
					}
					else if (!fbf.ToClear)
					{
						var filter = new IntegerFilterInfo {Verb = FilterVerb.Equal, Values = new int[1]};
						filter.Values[0] = (fbf.checkBox1.Checked ? 1 : 0);

						_filter = filter;
						_filter.ColumnName = _column.ColumnName;
						_dataGridISM.StorableFilters[_filter.ColumnName.ToLower()] = _filter;

						UpdateDataGrid(_filter.ColumnName);
					}
				}
				fbf.Dispose();
				#endregion
			}//0
			else if (dgts != null && dgts.GridColumnStyles[_column.ColumnName] is ExtendedDataGridImageColumn)
			{//1
				#region ...
				ImagesAndValues imAndVal;//проверим передали ли специальную структуру для фильтрации
				if (ImagesAndValues.ContainsKey(_column.ColumnName))
					imAndVal = (ImagesAndValues)ImagesAndValues[_column.ColumnName];
				else 
					return;

				var fif = new FilterImageForm(imAndVal);
				switch(_column.DataType.ToString())
				{//switch
					case "System.Int32":
					case "System.Boolean":
						var _filter = (IntegerFilterInfo)_dataGridISM.StorableFilters[_column.ColumnName.ToLower()];
						if (_filter != null)
						{
							foreach (int val in _filter.Values)
							{
								fif.imagedCheckedListBox1.SetItemChecked((int)fif.htIndicies[val], true);
							}
							
							/*for (int i=0; i<imAndVal.Length; i++)//если фильтр уже задан, выставим соответствующее значение
							{
								if (_filter.Values[0] == (int)imAndVal[i].Value)
								{
									fif.comboboxEx1.SelectedIndex = i;
									break;
								}
							}*/
						}

						if (fif.ShowDialog() == DialogResult.OK)
						{
							if (fif.imagedCheckedListBox1.CheckedItems.Count == 0)
							{
								if (_filter != null)
								{
									_dataGridISM.StorableFilters.Remove(_filter.ColumnName);
									//_filter = null;
									UpdateDataGrid();
								}
								return;
							}

							var filter = new IntegerFilterInfo
							             	{Verb = FilterVerb.In, Values = new int[fif.imagedCheckedListBox1.CheckedItems.Count]};

							for (int i = 0; i < fif.imagedCheckedListBox1.CheckedItems.Count; i++)
							{
								filter.Values[i] = (int)imAndVal[fif.imagedCheckedListBox1.CheckedIndices[i]].Value;
								//((ComboBoxExItem)flf.checkedListBox1.CheckedItems[i]).ImageIndex;
							}

							//ImagePair ip = imAndVal[fif.comboboxEx1.SelectedIndex];
							//filter.Values[0] = (int)ip.Value;
	
							_filter = filter;
							_filter.ColumnName = _column.ColumnName;
							_dataGridISM.StorableFilters[_filter.ColumnName.ToLower()] = _filter;
	
							UpdateDataGrid(_filter.ColumnName);
					
							/*if ((fif.ToClear || fif.comboboxEx1.SelectedIndex == -1) && _filter != null)
							{
								_dataGridISM.StorableFilters.Remove(_filter.ColumnName);
								UpdateDataGrid();
							}
							else if (!fif.ToClear)
							{
								
							}*/
						}
						break;
				}//switch
				fif.Dispose();
				#endregion
			}//1
			//фильтрация для колонок с комбобоксами (Int32,Byte)
			else if (dgts != null && ((_column.DataType == typeof(Int32) || _column.DataType == typeof(Byte)) && dgts.GridColumnStyles[_column.ColumnName] is ExtendedDataGridComboBoxColumn))
			{
//2
				#region ...

				var dataGridTableStyle = _dataGridISM.TableStyles[_dataGridISM.DataMember];
				if (dataGridTableStyle != null)
				{
					var cbc = (dataGridTableStyle.GridColumnStyles[_column.ColumnName] as ExtendedDataGridComboBoxColumn);
					var flf = new FilterLookupFieldForm();

					if (cbc != null)
						foreach (RadListDataItem li in cbc.ComboBox.Items)
						{
							flf.checkedListBox1.Items.Add(li);
							flf.htIndicies.Add(li.Value, flf.checkedListBox1.Items.Count-1);
						}


					var _filter = (IntegerFilterInfo)_dataGridISM.StorableFilters[_column.ColumnName.ToLower()];

					if (_filter != null)
					{
						foreach (int val in _filter.Values)
						{
							flf.checkedListBox1.SetItemChecked((int)flf.htIndicies[val], true);
						}
					}

					if (flf.ShowDialog() == DialogResult.OK)
					{
						if (flf.checkedListBox1.CheckedItems.Count == 0)
						{
							if (_filter != null)
							{
								_dataGridISM.StorableFilters.Remove(_filter.ColumnName);
								//_filter = null;
								UpdateDataGrid();
							}
							return;
						}

						var filter = new IntegerFilterInfo {Verb = FilterVerb.In, Values = new int[flf.checkedListBox1.CheckedItems.Count]};

						for (int i = 0; i < flf.checkedListBox1.CheckedItems.Count; i++)
						{
							filter.Values[i] = ((ListItem)flf.checkedListBox1.CheckedItems[i]).Value;
						}
					

						_filter = filter;
						_filter.ColumnName = _column.ColumnName;
						_dataGridISM.StorableFilters[_filter.ColumnName.ToLower()] = _filter;

						UpdateDataGrid(_filter.ColumnName);
					}
					flf.Dispose();
				}

				#endregion
			} //2
			//фильтрация колонок (Int32,Double,Decimal)
			else if (_column.DataType == typeof(Int32) || _column.DataType == typeof(Double) || _column.DataType == typeof(Decimal))
			{//3
				#region ...

				var fn = new FilterNumbersForm {DataType = _column.DataType};

				var _filter = (FilterInfo)_dataGridISM.StorableFilters[_column.ColumnName.ToLower()];

				if (_filter != null) // use existing filter
				{
					if (_filter is IntegerFilterInfo)
					{
						fn.textBoxNumStart.Text = ((IntegerFilterInfo)_filter).Values[0].ToString(CultureInfo.InvariantCulture);
						if (((IntegerFilterInfo)_filter).Values.Length > 1)
							fn.textBoxNumEnd.Text = ((IntegerFilterInfo)_filter).Values[1].ToString(CultureInfo.InvariantCulture);
					}
					else if (_filter is DoubleFilterInfo)
					{
						fn.textBoxNumStart.Text = ((DoubleFilterInfo)_filter).Values[0].ToString(CultureInfo.InvariantCulture);
						if (((DoubleFilterInfo)_filter).Values.Length > 1)
							fn.textBoxNumEnd.Text = ((DoubleFilterInfo)_filter).Values[1].ToString(CultureInfo.InvariantCulture);
					}

					if (_filter.Verb == FilterVerb.Equal)
						fn.radioButtonByNumber.Checked = true;
					else if (_filter.Verb == FilterVerb.Between)
						fn.radioButtonByRange.Checked = true;
					else if (_filter.Verb == FilterVerb.Greater)
					{
						fn.radioButtonByCond.Checked = true;
						fn.radioButtonLarger.Checked = true;
					}
					else if (_filter.Verb == FilterVerb.Less)
					{
						fn.radioButtonByCond.Checked = true;
						fn.radioButtonLess.Checked = true;
					}
				}
				if (fn.ShowDialog() == DialogResult.OK)
				{
					// filter numbers
					if (_column.DataType == typeof(Int32))
					{
						var filter = new IntegerFilterInfo();
						
						if (fn.textBoxNumEnd.Enabled && fn.textBoxNumEnd.Text != "")
						{
							filter.Values = new int[2];
							filter.Values[1] = Convert.ToInt32(fn.textBoxNumEnd.Text);
						}
						else
							filter.Values = new int[1];
						if (fn.textBoxNumStart.Text != "")
							filter.Values[0] = Convert.ToInt32(fn.textBoxNumStart.Text);
						else if (_filter != null/* && _filter.ColumnName == _column.ColumnName*/)  // delete filter
						{
							_dataGridISM.StorableFilters.Remove(_filter.ColumnName);
							//_filter = null;
							UpdateDataGrid();
							return;
						}
						else return;

						_filter = filter;
					}
					else if (_column.DataType == typeof(Double) || _column.DataType == typeof(Decimal))
					{
						var filter = new DoubleFilterInfo();

						if (fn.textBoxNumEnd.Enabled && fn.textBoxNumEnd.Text != "")
						{
							filter.Values = new double[2];
							filter.Values[1] = Convert.ToDouble(fn.textBoxNumEnd.Text);
						}
						else
							filter.Values = new double[1];
						if (fn.textBoxNumStart.Text != "")
							filter.Values[0] = Convert.ToDouble(fn.textBoxNumStart.Text);
						else if (_filter != null/* && _filter.ColumnName == _column.ColumnName*/)  // delete filter
						{
							_dataGridISM.StorableFilters.Remove(_filter.ColumnName);
							UpdateDataGrid();
							return;
						}
						else 
							return;

						_filter = filter;
					}

					if (_filter != null)
					{
						_filter.ColumnName = _column.ColumnName;
						//_filter.TableName = _tableName;
//					//если задана строка фильтрации, то необходимо задать имя фильтра и указать этот фильтр в ней
//					if(_dataGridISM.FilterString != null && _dataGridISM.FilterString != "")
//						_filter.FilterName = _column.ColumnName;//DataGridISM сам укажет не отмеченные фильтры в строке фильтрации через AND в конце

						if (fn.radioButtonByNumber.Checked)
							_filter.Verb = FilterVerb.Equal;
						else if (fn.radioButtonByRange.Checked)
							_filter.Verb = FilterVerb.Between;
						else if (fn.radioButtonByCond.Checked)
						{
							if (fn.radioButtonLarger.Checked)
								_filter.Verb = FilterVerb.Greater;
							else if (fn.radioButtonLess.Checked)
								_filter.Verb = FilterVerb.Less;
						}

						//_dataGridISM.Filters.Add(_filter.ColumnName, _filter);
						_dataGridISM.StorableFilters[_filter.ColumnName.ToLower()] = _filter;

						UpdateDataGrid(_filter.ColumnName);
					}
				}
				fn.Dispose();
				#endregion
			}//3
			//фильтрация колонок (String)
			else if (_column.DataType == typeof(String))
			{//4
				#region ...
				var fs = new FilterStringForm();

				var _filter = (FilterInfo)_dataGridISM.StorableFilters[_column.ColumnName.ToLower()];

				if (_filter != null) // use existing filter
				{
					var stringFilterInfo = _filter as StringFilterInfo;
					if (stringFilterInfo != null)
						fs.textBoxString.Text = (stringFilterInfo).Values[0];

					if (_filter.Verb == FilterVerb.Like && fs.textBoxString.Text != "")
					{
						if (fs.textBoxString.Text[0] == '*' && fs.textBoxString.Text[fs.textBoxString.Text.Length - 1] == '*')
						{
							fs.radioButtonContain.Checked = true;
							fs.textBoxString.Text = fs.textBoxString.Text.Substring(1, fs.textBoxString.Text.Length - 2); 
						}
						else if (fs.textBoxString.Text[0] == '*')
						{
							fs.radioButtonEnd.Checked = true;
							fs.textBoxString.Text = fs.textBoxString.Text.Substring(1); 
						}
						else if (fs.textBoxString.Text[fs.textBoxString.Text.Length - 1] == '*')
						{
							fs.radioButtonStart.Checked = true;
							fs.textBoxString.Text = fs.textBoxString.Text.Substring(0, fs.textBoxString.Text.Length - 1); 
						}
					}
					else if (_filter.Verb == FilterVerb.IsNull)
					{
						if (_filter.Negation)
							fs.radioButtonNotEmpty.Checked = true;
						else
							fs.radioButtonEmpty.Checked = true;
					}
				}

				if (fs.ShowDialog() == DialogResult.OK)
				{
					// filter string
					var filter = new StringFilterInfo {ColumnName = _column.ColumnName, Values = new string[1]};
					//filter.TableName = _tableName;
//					//если задана строка фильтрации, то необходимо задать имя фильтра и указать этот фильтр в ней
//					if(_dataGridISM.FilterString != null && _dataGridISM.FilterString != "")
//						filter.FilterName = _column.ColumnName;//DataGridISM сам укажет не отмеченные фильтры в строке фильтрации через AND в конце

					if (fs.textBoxString.Text != "")
						filter.Values[0] = fs.textBoxString.Text;
					else if (fs.radioButtonNotEmpty.Checked || fs.radioButtonEmpty.Checked)
						filter.Values[0] = "";
					else if (_filter != null/* && _filter.ColumnName == _column.ColumnName*/)  // delete filter
					{
						_dataGridISM.StorableFilters.Remove(_filter.ColumnName.ToLower());
						UpdateDataGrid();
						return;
					}
					else
						return;

					if (fs.radioButtonStart.Checked)
					{
						filter.Verb = FilterVerb.Like;
						filter.Values[0] = filter.Values[0] + "*";
					}
					else if (fs.radioButtonEnd.Checked)
					{
						filter.Verb = FilterVerb.Like;
						filter.Values[0] = "*" + filter.Values[0];
					}
					else if (fs.radioButtonContain.Checked)
					{
						filter.Verb = FilterVerb.Like;
						filter.Values[0] = "*" + filter.Values[0] + "*";
					}
					else if (fs.radioButtonNotEmpty.Checked)
					{
						filter.Verb = FilterVerb.IsNull;
						filter.Negation = true;
					}
					else if (fs.radioButtonEmpty.Checked)
					{
						filter.Verb = FilterVerb.IsNull;
						filter.Negation = false;
					}

					_filter = filter;

					//_dataGridISM.Filters.Add(_filter.ColumnName, _filter);
					_dataGridISM.StorableFilters[_filter.ColumnName.ToLower()] = _filter;

					UpdateDataGrid();
				}
				fs.Dispose();
				#endregion
			}//4
			//фильтрация колонок (DateTime)
			else if (_column.DataType == typeof(DateTime))
			{//5
				#region ...
				var fd = new FilterDateForm();
				
				var _filter = (FilterInfo)_dataGridISM.StorableFilters[_column.ColumnName.ToLower()];

				if (_filter != null) // use existing filter
				{
					var dateTimeFilterInfo = _filter as DateTimeFilterInfo;
					if (dateTimeFilterInfo != null)
					{
						fd.dtpStart.Value = (dateTimeFilterInfo).Values[0];
						if ((dateTimeFilterInfo).Values.Length > 1)
						{
							if ((dateTimeFilterInfo).Values[0].Date != (dateTimeFilterInfo).Values[1].Date)
							{
								fd.dtpEnd.Value = (dateTimeFilterInfo).Values[1].Date;
								fd.radioButtonByRange.Checked = true;
							}
							else
								fd.radioButtonByDate.Checked = true;
						}
					}
					
					if (_filter.Verb == FilterVerb.GreaterOrEqual)
					{
						fd.radioButtonByCond.Checked = true;
						fd.radioButtonLarger.Checked = true;
					}
					else if (_filter.Verb == FilterVerb.LessOrEqual)
					{
						fd.radioButtonByCond.Checked = true;
						fd.radioButtonLess.Checked = true;
					}
				}
				if (fd.ShowDialog() == DialogResult.OK)
				{
					// filter datetime
					if (fd.ToClear)
					{
						if (_filter != null)
						{
							_dataGridISM.StorableFilters.Remove(_filter.ColumnName);
							//_filter = null;
							UpdateDataGrid();
						}
						return;
					}

					var filter = new DateTimeFilterInfo();
					
					if (fd.dtpEnd.Enabled)
					{
						filter.Values = new DateTime[2];
						filter.Values[1] = fd.dtpEnd.Value.Date.AddDays(1).AddSeconds(-1);
					}
					else if (fd.radioButtonByDate.Checked)
					{
						filter.Values = new DateTime[2];
						filter.Values[1] = fd.dtpStart.Value.Date.AddDays(1).AddSeconds(-1);
					}
					else
						filter.Values = new DateTime[1];
					
					filter.Values[0] = fd.dtpStart.Value.Date;


					_filter = filter;
					

					_filter.ColumnName = _column.ColumnName;
//					//если задана строка фильтрации, то необходимо задать имя фильтра и указать этот фильтр в ней
//					if(_dataGridISM.FilterString != null && _dataGridISM.FilterString != "")
//						_filter.FilterName = _column.ColumnName;//DataGridISM сам укажет не отмеченные фильтры в строке фильтрации через AND в конце

					if (fd.radioButtonByDate.Checked || fd.radioButtonByRange.Checked)
						_filter.Verb = FilterVerb.Between;
					else if (fd.radioButtonByCond.Checked)
					{
						if (fd.radioButtonLarger.Checked)
							_filter.Verb = FilterVerb.GreaterOrEqual;
						else if (fd.radioButtonLess.Checked)
							_filter.Verb = FilterVerb.LessOrEqual;
					}

					_dataGridISM.StorableFilters[_filter.ColumnName.ToLower()] = _filter;

					UpdateDataGrid();
				}
				fd.Dispose();
				#endregion
			}//5

			//UpdateDataGrid();
		}
	
	}
}