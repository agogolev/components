﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using Telerik.WinControls.UI;

namespace ColumnMenuExtender
{
    public class ExtendedDataGridComboBoxColumn : DataGridTextBoxColumn, IExtendedQuery
    {
        [DefaultValue(false)]
        public bool AllowExpand { get; set; }

        /// <summary>
        /// Remember how many times the GetMinimumHeight method is called.
        /// </summary>
        private int currentIteration;
        //Для того чтобы определить метод, определяющий цвет строки (например, если какое-то значение строки больше чего-либо - цвет бэкграунда строки розовый)
        public event GetColorForRow OnGetColorForRow;
        public event GetColorForRow OnGetForeColorForRow;

        public event FormatCellEventHandler SetCellFormat;

        // Methods
        public ExtendedDataGridComboBoxColumn()
        {
            AllowExpand = false;
            xMargin = 0;
            yMargin = -1;
            InEdit = false;
            Combo = new DataBoundComboBox { Visible = false };
        }

        protected override void Abort(int rowNum)
        {
            EndEdit();
            base.Abort(rowNum);
        }

        protected override bool Commit(CurrencyManager dataSource, int rowNum)
        {
            HideComboBox();

            if (!InEdit)
                return true;

            InEdit = false;

            try
            {
                object value = Combo.SelectedValue != -1 ? (object)Combo.SelectedValue : DBNull.Value;
                if (!GetColumnValueAtRow(dataSource, rowNum).Equals(value))
                    SetColumnValueAtRow(dataSource, rowNum, value);
            }
            catch
            {
                Abort(rowNum);
                return false;
            }
            EndEdit();
            return true;
        }

        protected override void ConcedeFocus()
        {
            HideComboBox();
        }

        protected override void Edit(CurrencyManager source, int rowNum, Rectangle bounds, bool readOnly, string displayText, bool cellIsVisible)
        {
            if (readOnly || ReadOnly)
            {
                var cri = DataGridTableStyle.DataGrid.CurrentRowIndex;
                if (cri > 0)
                    DataGridTableStyle.DataGrid.UnSelect(cri - 1);
                DataGridTableStyle.DataGrid.Select(cri);
                if (cri < DataGridTableStyle.DataGrid.VisibleRowCount - 1)
                    DataGridTableStyle.DataGrid.UnSelect(cri + 1);
            }
            else
            {
                //object value = null;
                //if (GetColumnValueAtRow(source, rowNum) != DBNull.Value)
                //{
                //    value = GetColumnValueAtRow(source, rowNum);
                //}
                Combo.SelectedValue = GetValue(GetColumnValueAtRow(source, rowNum));
                if (cellIsVisible)
                {
                    bounds.Offset(xMargin, yMargin);
                    bounds.Width -= (xMargin * 2);
                    bounds.Height -= yMargin;
                    Combo.Bounds = bounds;
                    Combo.Visible = true;
                    if (!InEdit)
                    {
                        InEdit = true;
                        base.ColumnStartedEditing(Combo);
                    }
                }
                else
                {
                    Combo.Visible = false;
                }

                if (Combo.Visible)
                {
                    DataGridTableStyle.DataGrid.Invalidate(bounds);
                }

                Combo.Focus();
            }
        }

        public new void EndEdit()
        {
            InEdit = false;
            Invalidate();
        }

        private int GetComboBoxHeight()
        {
            return ((Combo.Height + yMargin) + 1);
        }
        protected override int GetMinimumHeight()
        {
            if (!AllowExpand) return GetComboBoxHeight();

            var dg = DataGridTableStyle.DataGrid as ExtendedDataGrid;
            if (dg == null || !dg.AutoRowHeight)
                return GetComboBoxHeight();

            try
            {
                var frame = new StackFrame(4);
                var method = frame.GetMethod();
                string s = method.DeclaringType.FullName;
                if (s.EndsWith("DataGridAddNewRow"))
                {
                    ResetIterations();
                    return GetComboBoxHeight();
                }
                var cur = (CurrencyManager)DataGridTableStyle.DataGrid.BindingContext
                                            [DataGridTableStyle.DataGrid.DataSource, DataGridTableStyle.DataGrid.DataMember];
                if (cur.Count == 0)
                    return GetComboBoxHeight();

                currentIteration++;

                var retVal = GetComboBoxHeight();
                var stringHeight = CalcStringHeight(GetText(GetColumnValueAtRow(cur, currentIteration - 1).ToString()));
                if (currentIteration == cur.Count)
                    ResetIterations();
                return retVal > stringHeight ? retVal : stringHeight;
            }
            catch
            {
                // Error, return default
                ResetIterations();
                return GetComboBoxHeight();
            }
        }
        private void ResetIterations()
        {
            currentIteration = 0;
        }
        /// <summary>
        /// Calculates the height of the supplied string.
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        private int CalcStringHeight(string s)
        {
            try
            {
                // Create graphics for calculation
                Graphics g = ComboBox.CreateGraphics();
                // Do measure, and add a bit (4 pixels for me)
                return (int)g.MeasureString(s, ComboBox.Font, Width).Height + 4;
            }
            catch
            {
                // Error, return default font height.
                return GetComboBoxHeight();
            }
        }
        protected override int GetPreferredHeight(Graphics g, object Value)
        {
            var num1 = 0;
            var num2 = 0;
            var text1 = GetText(Value);
            do
            {
                num1 = text1.IndexOf("r\n", num1 + 1, StringComparison.Ordinal);
                num2++;
            }
            while (num1 != -1);
            return ((FontHeight * num2) + yMargin);
        }

        protected override Size GetPreferredSize(Graphics g, object Value)
        {
            var size1 = Size.Ceiling(g.MeasureString(GetText(Value), DataGridTableStyle.DataGrid.Font));
            size1.Width += ((xMargin * 2) + DataGridTableGridLineWidth);
            size1.Height += yMargin;
            return size1;
        }

        private string GetText(object Value)
        {
            if (Value == null || Value == DBNull.Value) return "";
            return ComboBox.Items.Where(item => int.Parse(item.Value.ToString()) == int.Parse(Value.ToString())).Select(item => item.Text).SingleOrDefault();
        }

        private int GetValue(object value)
        {
            if (value == DBNull.Value)
            {
                return -1;
            }
            return int.Parse(value.ToString());
        }

        private void HideComboBox()
        {
            if (Combo.Focused)
            {
                DataGridTableStyle.DataGrid.Focus();
            }
            Combo.Visible = false;
        }

        protected override void Paint(Graphics g, Rectangle bounds, CurrencyManager source, int rowNum)
        {
            if (source != null && g != null) Paint(g, bounds, source, rowNum, false);
        }

        protected override void Paint(Graphics g, Rectangle bounds, CurrencyManager source, int rowNum, bool alignToRight)
        {
            Brush backBrush = new SolidBrush(DataGridTableStyle.BackColor);
            Brush foreBrush = new SolidBrush(DataGridTableStyle.ForeColor);

            Paint(g, bounds, source, rowNum, backBrush, foreBrush, alignToRight);

            backBrush.Dispose();
            foreBrush.Dispose();
        }

        protected override void Paint(Graphics g, Rectangle bounds, CurrencyManager source, int rowNum, Brush backBrush, Brush foreBrush, bool alignToRight)
        {
            var backBr = backBrush;
            var foreBr = foreBrush;
            var e = new DataGridFormatCellEventArgs(GetColumnValueAtRow(source, rowNum), source.List[rowNum]);

            bool isNewBrush = SetCellColor(rowNum, ref backBrush, source);
            if (!isNewBrush)
            {
                if (backBrush != backBr)
                {
                    backBrush.Dispose();
                    backBrush = backBr;
                }
            }
            bool isNewForeBrush = SetCellForeColor(rowNum, ref foreBrush, source);
            if (!isNewForeBrush)
            {
                if (foreBrush != foreBr)
                {
                    foreBrush.Dispose();
                    foreBrush = foreBr;
                }
            }

            //fire the formatting event
            if (SetCellFormat != null)
            {
                SetCellFormat(this, e);
                if (e.BackBrush != null)
                {
                    backBrush.Dispose();
                    backBrush = e.BackBrush;
                    isNewBrush = false;
                }
                if (e.ForeBrush != null)
                {
                    foreBrush.Dispose();
                    foreBrush = e.ForeBrush;
                    isNewForeBrush = false;
                }
            }
            if (((SolidBrush)backBrush).Color == DataGridTableStyle.SelectionBackColor)
            {
                if (backBrush != backBr)
                {
                    backBrush.Dispose();
                    backBrush = backBr;
                    isNewBrush = false;
                }
            }
            if (((SolidBrush)foreBrush).Color == DataGridTableStyle.SelectionForeColor)
            {
                if (foreBrush != foreBr)
                {
                    foreBrush.Dispose();
                    foreBrush = foreBr;
                    isNewForeBrush = false;
                }
            }

            if (e.TextFont == null)
                e.TextFont = DataGridTableStyle.DataGrid.Font;
            g.FillRectangle(backBrush, bounds);
            Region saveRegion = g.Clip;
            var rect = new Rectangle(bounds.X, bounds.Y, bounds.Width, bounds.Height);
            using (var newRegion = new Region(rect))
            {
                g.Clip = newRegion;

                string s = GetText(GetColumnValueAtRow(source, rowNum));
                try
                {
                    g.DrawString(s, e.TextFont, foreBrush, bounds.X, bounds.Y + 2);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                finally
                {
                    g.Clip = saveRegion;
                }
            }
            //clean up
            if (e.BackBrushDispose)
                e.BackBrush.Dispose();
            if (e.ForeBrushDispose)
                e.ForeBrush.Dispose();
            if (e.TextFontDispose)
                e.TextFont.Dispose();
            if (isNewBrush)
                backBrush.Dispose();
            if (isNewForeBrush)
                foreBrush.Dispose();
        }

        private bool SetCellColor(int rowNum, ref Brush backBrush, CurrencyManager source)
        {
            var isNewBrush = false;

            if (!(DataGridTableStyle.DataGrid is ExtendedDataGrid))
                return false;
            var dataGrid = (ExtendedDataGrid)DataGridTableStyle.DataGrid;
            DataTable RowColorsTable = dataGrid.RowColorsTable;
            //число строк в этой таблицe
            int rowColorCount = (dataGrid.AllowCustomRowColorList ? RowColorsTable.Rows.Count : 0);

            if (rowColorCount > 0) //если были указаны цвета для отображения грида
            {
                //в последней строкие таблицы RowColorsTable хранится общее число строк,
                //для которых указана цветновая последовательность. После этой строки начинается повторение				
                //rowsInBatch - число строк в гриде, после которых начинается повторение цветновой последовательности
                var rowsInBatch = (int)RowColorsTable.Rows[rowColorCount - 1]["position"];
                //номер строки в текущем блоке(rowsInBatch)
                var rowColorIndex = rowNum % rowsInBatch + 1;
                if (rowColorIndex == 1) //если начинается прорисовка нового блока, обнуляем позицию строки в таблице RowColorsTable					
                    RowColorsTable.ExtendedProperties["currentRowColorsPosition"] = 0;

                //текущая позиция строки в таблице RowColorsTable
                //требуется для того, чтобы несколько раз напрасно не пробегать по таблице  RowColorsTable в поисках требцемой строки
                //т.к. строки которые уже были ипользованы, не будут использоваться до прорисовки следующего блока
                var currentRowColorsPosition = (int)RowColorsTable.ExtendedProperties["currentRowColorsPosition"];
                //ищем строку в которой указан цвет, рисуемой строки
                for (var i = currentRowColorsPosition; i < rowColorCount; i++)
                {
                    if (((int)RowColorsTable.Rows[i]["position"]) < rowColorIndex) continue;
                    if (((SolidBrush)backBrush).Color != DataGridTableStyle.SelectionBackColor)
                    {
                        backBrush = new SolidBrush((Color)RowColorsTable.Rows[i]["color"]);
                        isNewBrush = true;
                    }
                    RowColorsTable.ExtendedProperties["currentRowColorsPosition"] = i;
                    break;
                }
            }
            else if (OnGetColorForRow != null)
            {//был определён делегат для выбора цвета
                if (((SolidBrush)backBrush).Color != DataGridTableStyle.SelectionBackColor)
                {
                    Brush backBr = OnGetColorForRow(rowNum, source);
                    if (backBr != null)
                    {
                        backBrush = backBr;
                        isNewBrush = true;
                    }
                }
            }
            else
            {
                if (((SolidBrush)backBrush).Color == DataGridTableStyle.SelectionBackColor)
                    return false;
                {
                    var backBr = (DataGridTableStyle.DataGrid as ExtendedDataGrid).FireEventGetColorForRow(rowNum, source);
                    if (backBr != null)
                    {
                        backBrush = backBr;
                        isNewBrush = true;
                    }
                }
            }
            return isNewBrush;
        }

        private bool SetCellForeColor(int rowNum, ref Brush foreBrush, CurrencyManager source)
        {
            if (((SolidBrush)foreBrush).Color == DataGridTableStyle.SelectionForeColor)
                return false;
            var isNewBrush = false;

            if (OnGetForeColorForRow != null)
            {//был определён делегат для выбора цвета
                Brush foreBr = OnGetForeColorForRow(rowNum, source);
                if (foreBr != null)
                {
                    foreBrush = foreBr;
                    isNewBrush = true;
                }
            }
            else
            {
                if ((DataGridTableStyle.DataGrid is ExtendedDataGrid))
                {
                    Brush foreBr = (DataGridTableStyle.DataGrid as ExtendedDataGrid).FireEventGetForeColorForRow(rowNum, source);
                    if (foreBr != null)
                    {
                        foreBrush = foreBr;
                        isNewBrush = true;
                    }
                }
            }
            return isNewBrush;
        }

        protected override void SetDataGridInColumn(DataGrid Value)
        {
            base.SetDataGridInColumn(Value);
            if ((Combo.Parent != Value) && (Combo.Parent != null))
            {
                Combo.Parent.Controls.Remove(Combo);
            }
            if (Value != null)
            {
                Value.Controls.Add(Combo);
            }
        }


        protected override void UpdateUI(CurrencyManager Source, int RowNum, string InstantText)
        {
            Combo.Text = GetText(GetColumnValueAtRow(Source, RowNum));
            if (InstantText != null)
            {
                Combo.Text = InstantText;
            }
        }


        // Properties
        private int DataGridTableGridLineWidth
        {
            get
            {
                return DataGridTableStyle.GridLineStyle == DataGridLineStyle.Solid ? 1 : 0;
            }
        }

        [Browsable(false)]
        public DataBoundComboBox ComboBox
        {
            get
            {
                return Combo;
            }
        }

        #region Public properties

        [DefaultValue(typeof(string), ""), Category("Misc")]
        public string FieldName { get; set; }

        [DefaultValue(typeof(string), ""), Category("Misc")]
        public string FilterFieldName { get; set; }

        #endregion Public properties

        // Fields
        private readonly DataBoundComboBox Combo;
        private bool InEdit;
        private readonly int xMargin;
        private readonly int yMargin;

    }

}