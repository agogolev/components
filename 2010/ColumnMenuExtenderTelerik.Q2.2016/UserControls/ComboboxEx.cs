﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Diagnostics;
using System.Windows.Forms;
using System.Drawing;

namespace ColumnMenuExtender
{
	/// <summary>
	/// Summary description for ComboboxEx.
	/// </summary>
	public class ComboboxEx : System.Windows.Forms.ComboBox
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public ComboboxEx(System.ComponentModel.IContainer container)
		{
			///
			/// Required for Windows.Forms Class Composition Designer support
			///
			container.Add(this);
			InitializeComponent();

			DrawMode = DrawMode.OwnerDrawFixed;
		}

		public ComboboxEx()
		{
			///
			/// Required for Windows.Forms Class Composition Designer support
			///
			InitializeComponent();

			DrawMode = DrawMode.OwnerDrawFixed;
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}


		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
		}
		#endregion

		private ImageList imageList;
		public ImageList ImageList
		{
			get {return imageList;}
			set {imageList = value;}
		}

		protected override void OnDrawItem(DrawItemEventArgs ea)
		{
			ea.DrawBackground();
			ea.DrawFocusRectangle();

			ComboBoxExItem item;
			Size imageSize = imageList.ImageSize;
			Rectangle bounds = ea.Bounds;

			try
			{
				item = (ComboBoxExItem)Items[ea.Index];

				if (item.ImageIndex != -1)
				{
					imageList.Draw(ea.Graphics, bounds.Left, bounds.Top,
						item.ImageIndex);
					ea.Graphics.DrawString(item.Text, ea.Font, new
						SolidBrush(ea.ForeColor), bounds.Left+imageSize.Width, bounds.Top);
				}
				else
				{
					ea.Graphics.DrawString(item.Text, ea.Font, new
						SolidBrush(ea.ForeColor), bounds.Left, bounds.Top);
				}
			}
			catch
			{
				if (ea.Index != -1)
				{
					ea.Graphics.DrawString(Items[ea.Index].ToString(), ea.Font, new
						SolidBrush(ea.ForeColor), bounds.Left, bounds.Top);
				}
				else
				{
					ea.Graphics.DrawString(Text, ea.Font, new
						SolidBrush(ea.ForeColor), bounds.Left, bounds.Top);
				}
			}

			base.OnDrawItem(ea);
		}
	}

	class ComboBoxExItem
	{
		private string _text;
		public string Text
		{
			get {return _text;}
			set {_text = value;}
		}

		private int _imageIndex;
		public int ImageIndex
		{
			get {return _imageIndex;}
			set {_imageIndex = value;}
		}

		public ComboBoxExItem()
			: this("") 
		{
		}

		public ComboBoxExItem(string text)
			: this(text, -1) 
		{
		}

		public ComboBoxExItem(string text, int imageIndex)
		{
			_text = text;
			_imageIndex = imageIndex;
		}

		public override string ToString()
		{
			return _text;
		}
	}
}
