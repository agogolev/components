﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Xml;

namespace ColumnMenuExtender
{
    public enum SortType
    {
        NoSort = 0,
        Alphabet = 1,
        Popularity = 2
    }

    public class HintTextBox : TextBox
    {
        private readonly ArrayList data;
        private readonly fmHint listBox;
        private bool fText = true;
        private int maxHintCount = 100;
        private int minLettersForHint = 3;
        private bool needSaveHint;
        private string oldText = "";

        private SortType sortType = SortType.Alphabet;

        public HintTextBox()
        {
            IsHinted = false;
            listBox = new fmHint();
            data = new ArrayList();
            listBox.ItemSelect += listBox_ItemSelect;
        }

        [DefaultValue(false)]
        public bool IsHinted { get; set; }

        //Максимально кол-во запоминаемых значений для подсказок
        [DefaultValue(100)]
        public int MaxHintCount
        {
            get { return maxHintCount; }
            set { maxHintCount = value; }
        }

        //Минимальное кол-во букв для отображения подподподсказки
        [DefaultValue(3)]
        public int MinLettersForHint
        {
            get { return minLettersForHint; }
            set { minLettersForHint = value; }
        }

        //Высота подсказки
        [DefaultValue(13)]
        public int ItemHeight
        {
            get { return listBox.ItemHeight; }
            set { listBox.ItemHeight = value; }
        }

        [DefaultValue(4)]
        public int HeightInHints
        {
            get { return listBox.HeightInHints; }
            set { listBox.HeightInHints = value; }
        }

        [DefaultValue(SortType.Alphabet)]
        public SortType Sort
        {
            get { return sortType; }
            set
            {
                sortType = value;
                sort();
            }
        }

        public Font HintFont
        {
            get { return listBox.Font; }
            set { listBox.Font = value; }
        }

        public void SaveDataXml(XmlNode node)
        {
            XmlDocument xmlDoc = node.OwnerDocument;
            bool newItem = true;

            for (int i = 0; i < data.Count; i++)
            {
                var item = (ListItem) data[i];
                XmlNode newNode = xmlDoc.CreateNode(XmlNodeType.Element, "data", "");
                newNode.InnerText = item.Data;
                if (item.Data == Text)
                {
                    newItem = false;
                    item.Index++;
                }
                XmlAttribute attr = xmlDoc.CreateAttribute("index");
                attr.Value = item.Index.ToString();
                newNode.Attributes.Append(attr);
                node.AppendChild(newNode);
            }
            if (needSaveHint && newItem && Text != "")
            {
                XmlNode newNode = xmlDoc.CreateNode(XmlNodeType.Element, "data", "");
                newNode.InnerText = Text;
                XmlAttribute attr = xmlDoc.CreateAttribute("index");
                attr.Value = 1.ToString();
                newNode.Attributes.Append(attr);
                node.AppendChild(newNode);
            }
        }

        public void LoadDataXml(XmlNode node)
        {
            XmlNodeList nl = node.SelectNodes("data");
            ListItem minItem = null;
            int count = 0;
            data.Clear();

            if (nl != null)
            {
                foreach (XmlNode dNode in nl)
                {
                    ListItem item = ListItem.ReadXmlNode(dNode);
                    data.Add(item);
                    count++;
                    if (minItem == null || minItem.Index > item.Index)
                        minItem = item;
                }
                if (count > MaxHintCount)
                    data.Remove(minItem);
            }
            sort();
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                data.Clear();
                listBox.Dispose();
            }
            base.Dispose(disposing);
        }


        private void listBox_ItemSelect(object sender, ItemSelectEventArgs e)
        {
            fText = false;
            Text = ((ListItem) e.SelectedItem).Data;
            SelectionStart = TextLength;
            oldText = "";
            fText = true;
        }

        protected override void OnTextChanged(EventArgs e)
        {
            if (fText)
            {
                #region Блок отвечающий за подсказки

                try
                {
                    int len = Text.Length;
                    string low = Text.ToLower();
                    if (Text.Length >= minLettersForHint)
                    {
                        if (oldText == Text && !listBox.Visible)
                        {
                            showHint();
                        }
                        else if (oldText != "" && len >= oldText.Length && oldText == Text.Substring(0, oldText.Length))
                        {
                            int i = 0;
                            while (i < listBox.Items.Count)
                            {
                                var item = (ListItem) listBox.Items[i];
                                if (item.Data.Length < len)
                                    listBox.Items.Remove(item);
                                else if (item.Data.Substring(0, len).ToLower() != low)
                                    listBox.Items.Remove(item);
                                else i++;
                            }
                        }
                        else
                        {
                            listBox.Items.Clear();
                            foreach (ListItem item in data)
                            {
                                if (item.Data.Length >= len && item.Data.Substring(0, len).ToLower() == low)
                                    listBox.Items.Add(item);
                            }
                        }
                        oldText = Text;
                        if (listBox.Items.Count > 0)
                            showHint();
                        else listBox.Hide();
                    }
                    else listBox.Visible = false;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                #endregion
            }
            base.OnTextChanged(e);
        }

        protected override void OnLeave(EventArgs e)
        {
            if (listBox.Visible)
                listBox.Hide();
            base.OnLeave(e);
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            needSaveHint = true;
            if (listBox.Visible)
            {
                switch (e.KeyData)
                {
                    case Keys.Up:
                    case Keys.Down:
                        listBox.SendKey(e.KeyData);
                        e.Handled = true;
                        break;
                    case Keys.Enter:
                        if (listBox.SelectedIndex != -1)
                        {
                            listBox.SendKey(e.KeyData);
                            e.Handled = true;
                        }
                        break;
                    case Keys.Escape:
                        listBox.Hide();
                        break;
                }
            }
            base.OnKeyDown(e);
        }

        private void sort()
        {
            IComparer myComp;
            switch (sortType)
            {
                case SortType.NoSort:
                    return;
                case SortType.Alphabet:
                    myComp = new CompareAlphabet();
                    data.Sort(myComp);
                    break;
                case SortType.Popularity:
                    myComp = new ComparePopularity();
                    data.Sort(myComp);
                    break;
            }
        }

        private void showHint()
        {
            if (Focused)
            {
                int selStart = SelectionStart;
                int selLen = SelectionLength;
                Point loc = Location;
                Point sl = PointToScreen(Location);
                loc.X = sl.X - loc.X;
                loc.Y = sl.Y - loc.Y + Size.Height;
                listBox.Location = loc;
                listBox.Width = Width;
                listBox.Show();
                SelectionStart = selStart;
                SelectionLength = selLen;
                Focus();
            }
        }


        private class CompareAlphabet : IComparer
        {
            int IComparer.Compare(object x, object y)
            {
                int z = x.ToString().CompareTo(y.ToString());
                return z;
            }
        }

        private class ComparePopularity : IComparer
        {
            int IComparer.Compare(object x, object y)
            {
                return ((ListItem) y).Index.CompareTo(((ListItem) x).Index);
            }
        }

        private class ListItem
        {
            public readonly string Data;
            public int Index;

            private ListItem(string data, int index)
            {
                Data = data;
                Index = index;
            }

            public static ListItem ReadXmlNode(XmlNode node)
            {
                int index = Convert.ToInt32(node.Attributes["index"].Value);
                string data = node.InnerText;
                return new ListItem(data, index);
            }

            public override string ToString()
            {
                return Data;
            }
        }
    }
}