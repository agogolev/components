﻿using System;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;

namespace ColumnMenuExtender
{
    /// <summary>
    ///     Summary description for DataGridPager.
    /// </summary>
    public class DataGridPager : UserControl
    {
        /// <summary>
        ///     Required designer variable.
        /// </summary>
        private readonly Container components = null;

        private int _batch = 30;
        private int _pageNum = 1;
        private bool allowAll = true;
        private Button btnGo;

        private Button btnPageEnd;
        private Button btnPageNext;
        private Button btnPagePrev;
        private Button btnPageStart;
        private DataGridISM currentDataGrid;
        private DataBoundComboBox dbcbBatch;
        private Label lblPage;
        private TextBox tbPageNum;

        public DataGridPager()
        {
            PageCount = 0;
            InitializeComponent();
        }

        public bool AllowAll
        {
            get { return allowAll; }
            set { allowAll = value; }
        }

        public int PageNum
        {
            get { return _pageNum; }
            set
            {
                if (_pageNum != value)
                {
                    if (currentDataGrid != null)
                    {
                        _pageNum = value;
                        currentDataGrid.StockPageNum = value;
                        tbPageNum.Text = _pageNum.ToString();
                    }
                }
            }
        }

        public int Batch
        {
            get
            {
                if (_batch < 0) return 30;
                return _batch;
            }
            set { _batch = value; }
        }

        public int PageCount { get; set; }

        public void BindToDataGrid(DataGridISM dataGridISM)
        {
            currentDataGrid = dataGridISM;
            DataTable dt = TableInGrid.GetTable(currentDataGrid);
            if (dt != null && dt.Rows.Count > 0)
                PageCount = (int) dt.Rows[0]["pageCount"];
            else
                PageCount = 1;


            if (Batch < 0)
            {
                currentDataGrid.SetNumInBatchWOReload(30);
                Batch = 30;
            }
            else if (currentDataGrid.StockNumInBatch != Batch)
                currentDataGrid.SetNumInBatchWOReload(Batch);

            dbcbBatch.Items.Clear();
            if (AllowAll)
                dbcbBatch.Items.Add(new RadListDataItem("Все", 0));
            dbcbBatch.Items.Add(new RadListDataItem("15", 15));
            dbcbBatch.Items.Add(new RadListDataItem("30", 30));
            dbcbBatch.Items.Add(new RadListDataItem("60", 60));
            dbcbBatch.Items.Add(new RadListDataItem("120", 120));
            dbcbBatch.Items.Add(new RadListDataItem("250", 250));
            dbcbBatch.SelectedIndex = 2;


            dbcbBatch.DataBindings.Clear();
            tbPageNum.DataBindings.Clear();

            dbcbBatch.DataBindings.Add("SelectedValue", currentDataGrid, "StockNumInBatch");
            tbPageNum.DataBindings.Add("Text", currentDataGrid, "StockPageNum");

            setText();
            currentDataGrid.Reload += UpdatePager;
        }

        public void BindToDataGrid(DataGridISM dataGridISM, int batch)
        {
            currentDataGrid = dataGridISM;
            DataTable dt = TableInGrid.GetTable(currentDataGrid);
            if (dt != null && dt.Rows.Count > 0)
                PageCount = (int) dt.Rows[0]["pageCount"];
            else
                PageCount = 1;

            Batch = batch;

            batch = currentDataGrid.StockNumInBatch;
            if (batch < 0)
                currentDataGrid.SetNumInBatchWOReload(30);
            else if (currentDataGrid.StockNumInBatch != batch)
                currentDataGrid.SetNumInBatchWOReload(Batch);

            if (AllowAll)
                dbcbBatch.Items.Add(new RadListDataItem("Все", 0));
            dbcbBatch.Items.Add(new RadListDataItem("15", 15));
            dbcbBatch.Items.Add(new RadListDataItem("30", 30));
            dbcbBatch.Items.Add(new RadListDataItem("60", 60));
            dbcbBatch.Items.Add(new RadListDataItem("120", 120));
            dbcbBatch.Items.Add(new RadListDataItem("250", 250));
            dbcbBatch.SelectedIndex = 2;


            dbcbBatch.DataBindings.Clear();
            dbcbBatch.DataBindings.Add("SelectedValue", currentDataGrid, "StockNumInBatch");
            tbPageNum.DataBindings.Clear();
            tbPageNum.DataBindings.Add("Text", currentDataGrid, "StockPageNum");

            setText();
            currentDataGrid.Reload += UpdatePager;
        }

        public void BindClear()
        {
            dbcbBatch.DataBindings.Clear();
            tbPageNum.DataBindings.Clear();
            dbcbBatch.Items.Clear();
            PageCount = 0;
        }


        /// <summary>
        ///     Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        private void btnGo_Click(object sender, EventArgs e)
        {
        }


        private void dbcbBatch_SelectedIndexChanged(object sender, EventArgs e)
        {
            _batch = dbcbBatch.SelectedValue;
            PageNum = 1;
            btnGo.Focus();
            DataTable dt = TableInGrid.GetTable(currentDataGrid);
            if (dt != null && dt.Rows.Count > 0)
                PageCount = (int) dt.Rows[0]["pageCount"];
            else
                PageCount = 1;
            setText();
        }

        public void UpdatePager(object obj, EventArgs e)
        {
            DataTable dt = TableInGrid.GetTable(currentDataGrid);
            if (dt != null)
            {
                if (dt.Rows.Count == 0 && currentDataGrid.StockPageNum != 1)
                {
                    currentDataGrid.StockPageNum = 1;
                    tbPageNum.Text = 1.ToString();
                    dt = TableInGrid.GetTable(currentDataGrid);
                }
                if (dt.Rows.Count > 0)
                    PageCount = (int) dt.Rows[0]["pageCount"];
                setText();
            }
        }

        private void btnPagePrev_Click(object sender, EventArgs e)
        {
            if (currentDataGrid.StockPageNum > 1) PageNum--;
        }

        private void btnPageNext_Click(object sender, EventArgs e)
        {
            if (currentDataGrid.StockPageNum < PageCount) PageNum++;
        }

        private void btnPageStart_Click(object sender, EventArgs e)
        {
            PageNum = 1;
        }

        private void btnPageEnd_Click(object sender, EventArgs e)
        {
            PageNum = PageCount;
        }

        private void tbPageNum_Validated(object sender, EventArgs e)
        {
            setText();
        }

        private void tbPageNum_TextChanged(object sender, EventArgs e)
        {
            setText();
        }

        private void setText()
        {
            //"стр " + 
            lblPage.Text = currentDataGrid.StockPageNum + " из " + PageCount;
        }

        private void tbPageNum_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                int n = Convert.ToInt32(tbPageNum.Text);
                if (PageCount == 0)
                {
                    tbPageNum.Text = "1";
                }
                else if (n < 1)
                {
                    e.Cancel = true;
                    tbPageNum.Text = "1";
                }
                else if (n > PageCount)
                {
                    e.Cancel = true;
                    tbPageNum.Text = PageCount.ToString();
                }
            }
            catch
            {
                e.Cancel = true;
            }
        }

        #region Component Designer generated code

        /// <summary>
        ///     Required method for Designer support - do not modify
        ///     the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblPage = new System.Windows.Forms.Label();
            this.dbcbBatch = new DataBoundComboBox();
            this.btnGo = new System.Windows.Forms.Button();
            this.btnPagePrev = new System.Windows.Forms.Button();
            this.btnPageNext = new System.Windows.Forms.Button();
            this.tbPageNum = new System.Windows.Forms.TextBox();
            this.btnPageStart = new System.Windows.Forms.Button();
            this.btnPageEnd = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dbcbBatch)).BeginInit();
            this.SuspendLayout();
            // 
            // lblPage
            // 
            this.lblPage.Location = new System.Drawing.Point(48, 0);
            this.lblPage.Name = "lblPage";
            this.lblPage.Size = new System.Drawing.Size(72, 20);
            this.lblPage.TabIndex = 2;
            this.lblPage.Text = "1 из 1";
            this.lblPage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dbcbBatch
            // 
            this.dbcbBatch.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.dbcbBatch.Location = new System.Drawing.Point(230, 1);
            this.dbcbBatch.Name = "dbcbBatch";
            this.dbcbBatch.SelectedValue = -1;
            this.dbcbBatch.Size = new System.Drawing.Size(50, 18);
            this.dbcbBatch.TabIndex = 7;
            // 
            // btnGo
            // 
            this.btnGo.BackColor = System.Drawing.SystemColors.Control;
            this.btnGo.Location = new System.Drawing.Point(192, 0);
            this.btnGo.Name = "btnGo";
            this.btnGo.Size = new System.Drawing.Size(32, 20);
            this.btnGo.TabIndex = 6;
            this.btnGo.Text = ">>";
            this.btnGo.UseVisualStyleBackColor = false;
            this.btnGo.Click += new System.EventHandler(this.btnGo_Click);
            // 
            // btnPagePrev
            // 
            this.btnPagePrev.BackColor = System.Drawing.SystemColors.Control;
            this.btnPagePrev.Location = new System.Drawing.Point(24, 0);
            this.btnPagePrev.Name = "btnPagePrev";
            this.btnPagePrev.Size = new System.Drawing.Size(24, 20);
            this.btnPagePrev.TabIndex = 2;
            this.btnPagePrev.Text = "<";
            this.btnPagePrev.UseVisualStyleBackColor = false;
            this.btnPagePrev.Click += new System.EventHandler(this.btnPagePrev_Click);
            // 
            // btnPageNext
            // 
            this.btnPageNext.BackColor = System.Drawing.SystemColors.Control;
            this.btnPageNext.Location = new System.Drawing.Point(120, 0);
            this.btnPageNext.Name = "btnPageNext";
            this.btnPageNext.Size = new System.Drawing.Size(24, 20);
            this.btnPageNext.TabIndex = 3;
            this.btnPageNext.Text = ">";
            this.btnPageNext.UseVisualStyleBackColor = false;
            this.btnPageNext.Click += new System.EventHandler(this.btnPageNext_Click);
            // 
            // tbPageNum
            // 
            this.tbPageNum.Location = new System.Drawing.Point(168, 0);
            this.tbPageNum.Name = "tbPageNum";
            this.tbPageNum.Size = new System.Drawing.Size(24, 20);
            this.tbPageNum.TabIndex = 5;
            this.tbPageNum.Text = "1";
            this.tbPageNum.TextChanged += new System.EventHandler(this.tbPageNum_TextChanged);
            this.tbPageNum.Validating += new System.ComponentModel.CancelEventHandler(this.tbPageNum_Validating);
            this.tbPageNum.Validated += new System.EventHandler(this.tbPageNum_Validated);
            // 
            // btnPageStart
            // 
            this.btnPageStart.BackColor = System.Drawing.SystemColors.Control;
            this.btnPageStart.Location = new System.Drawing.Point(0, 0);
            this.btnPageStart.Name = "btnPageStart";
            this.btnPageStart.Size = new System.Drawing.Size(24, 20);
            this.btnPageStart.TabIndex = 1;
            this.btnPageStart.Text = "|<";
            this.btnPageStart.UseVisualStyleBackColor = false;
            this.btnPageStart.Click += new System.EventHandler(this.btnPageStart_Click);
            // 
            // btnPageEnd
            // 
            this.btnPageEnd.BackColor = System.Drawing.SystemColors.Control;
            this.btnPageEnd.Location = new System.Drawing.Point(144, 0);
            this.btnPageEnd.Name = "btnPageEnd";
            this.btnPageEnd.Size = new System.Drawing.Size(24, 20);
            this.btnPageEnd.TabIndex = 4;
            this.btnPageEnd.Text = ">|";
            this.btnPageEnd.UseVisualStyleBackColor = false;
            this.btnPageEnd.Click += new System.EventHandler(this.btnPageEnd_Click);
            // 
            // DataGridPager
            // 
            this.Controls.Add(this.btnPageEnd);
            this.Controls.Add(this.btnPageStart);
            this.Controls.Add(this.tbPageNum);
            this.Controls.Add(this.btnPageNext);
            this.Controls.Add(this.btnPagePrev);
            this.Controls.Add(this.btnGo);
            this.Controls.Add(this.dbcbBatch);
            this.Controls.Add(this.lblPage);
            this.Name = "DataGridPager";
            this.Size = new System.Drawing.Size(288, 21);
            ((System.ComponentModel.ISupportInitialize)(this.dbcbBatch)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        void dbcbBatch_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            _batch = dbcbBatch.SelectedValue;
            PageNum = 1;
            btnGo.Focus();
            DataTable dt = TableInGrid.GetTable(currentDataGrid);
            if (dt != null && dt.Rows.Count > 0)
                PageCount = (int)dt.Rows[0]["pageCount"];
            else
                PageCount = 1;
            setText();
        }

        #endregion
    }
}