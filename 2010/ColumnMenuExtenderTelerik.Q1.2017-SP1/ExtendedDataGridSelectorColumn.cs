﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace ColumnMenuExtender
{
    public class ExtendedDataGridSelectorColumn : DataGridTextBoxColumn, IExtendedQuery
    {
        //public event GetColorForRow OnGetColorForRow;
        private readonly Button btnEllipsis;
        private readonly int xMargin;
        private readonly int yMargin;
        private bool InEdit;
        private string OldVal;
        private int currentRow;
        private CurrencyManager currentSource;

        public ExtendedDataGridSelectorColumn()
        {
            xMargin = 0;
            yMargin = 0;
            OldVal = new string(string.Empty.ToCharArray());
            InEdit = false;
            btnEllipsis = new Button();
            btnEllipsis.Visible = false;
            btnEllipsis.Text = "\x2026"; //"\x0324";
            btnEllipsis.TextAlign = ContentAlignment.BottomCenter;
            btnEllipsis.Size = new Size(20, 20);
            btnEllipsis.BackColor = SystemColors.Control;
            btnEllipsis.Click += btnEllipsis_Click;
        }

        private int DataGridTableGridLineWidth
        {
            get
            {
                if (DataGridTableStyle.GridLineStyle == DataGridLineStyle.Solid)
                {
                    return 1;
                }
                return 0;
            }
        }

        public event GetColorForRow OnGetForeColorForRow;
        public event FormatCellEventHandler SetCellFormat;

        // Methods

        protected override void Abort(int currentRow)
        {
            HideBtnEllipsis();
            EndEdit();
        }

        protected override bool Commit(CurrencyManager DataSource, int currentRow)
        {
            HideBtnEllipsis();
            if (InEdit)
            {
                EndEdit();
            }
            return true;
        }

        protected override void ConcedeFocus()
        {
            btnEllipsis.Visible = false;
        }

        protected override void Edit(CurrencyManager Source, int currentRow, Rectangle Bounds, bool ReadOnly,
            string InstantText, bool CellIsVisible)
        {
            //Rectangle rectangle1 = Bounds;
            currentSource = Source;
            this.currentRow = currentRow;
            if (this.ReadOnly)
            {
                btnEllipsis.Enabled = false;
            }
            else
            {
                btnEllipsis.Enabled = true;
            }


            if (CellIsVisible)
            {
                Bounds.Offset(xMargin, yMargin);
                Bounds.Width -= (xMargin * 2);
                Bounds.Height -= yMargin;
                btnEllipsis.Top = Bounds.Top;
                btnEllipsis.Left = Bounds.Right - btnEllipsis.Width;
                btnEllipsis.Height = Bounds.Height;
                btnEllipsis.Visible = true;
            }
            else
            {
                btnEllipsis.Top = Bounds.Top;
                btnEllipsis.Left = Bounds.Right - btnEllipsis.Width;
                btnEllipsis.Height = Bounds.Height;
                btnEllipsis.Visible = false;
            }

            if (btnEllipsis.Visible)
            {
                DataGridTableStyle.DataGrid.Invalidate(Bounds);
            }
            InEdit = true;
        }

        private void HideBtnEllipsis()
        {
            if (btnEllipsis.Focused)
            {
                DataGridTableStyle.DataGrid.Focus();
            }
            btnEllipsis.Visible = false;
        }

        public new void EndEdit()
        {
            InEdit = false;
            Invalidate();
        }

        protected override int GetMinimumHeight()
        {
            return ((btnEllipsis.Height + yMargin) + 1);
        }

        protected override int GetPreferredHeight(Graphics g, object Value)
        {
            int num1 = 0;
            int num2 = 0;
            string text1 = GetText(Value);
            do
            {
                num1 = text1.IndexOf("r\n", num1 + 1);
                num2++;
            } while (num1 != -1);
            return ((base.FontHeight * num2) + yMargin);
        }

        protected override Size GetPreferredSize(Graphics g, object Value)
        {
            Size size1 = Size.Ceiling(g.MeasureString(GetText(Value), DataGridTableStyle.DataGrid.Font));
            size1.Width += ((xMargin * 2) + DataGridTableGridLineWidth);
            size1.Height += yMargin;
            return size1;
        }


        private string GetText(object Value)
        {
            if (Value == DBNull.Value)
            {
                return NullText;
            }
            if (Value != null)
            {
                if (!string.IsNullOrEmpty(Format))
                {
                    if (Value is DateTime) return ((DateTime)Value).ToString(Format, FormatInfo);
                    if (Value is int) return ((int)Value).ToString(Format, FormatInfo);
                    if (Value is Int64) return ((Int64)Value).ToString(Format, FormatInfo);
                    if (Value is decimal) return ((decimal)Value).ToString(Format, FormatInfo);
                    if (Value is double) return ((double)Value).ToString(Format, FormatInfo);
                    if (Value is float) return ((float)Value).ToString(Format, FormatInfo);
                }

                return Value.ToString();
            }
            return string.Empty;
        }

        protected override void Paint(Graphics g, Rectangle Bounds, CurrencyManager Source, int currentRow)
        {
            Paint(g, Bounds, Source, currentRow, false);
        }

        protected override void Paint(Graphics g, Rectangle Bounds, CurrencyManager Source, int currentRow,
            bool AlignToRight)
        {
            Brush backBrush = new SolidBrush(DataGridTableStyle.BackColor);
            Brush foreBrush = new SolidBrush(DataGridTableStyle.ForeColor);
            var e = new DataGridFormatCellEventArgs(GetColumnValueAtRow(Source, currentRow), Source.List[currentRow]);

            Brush foreBr = foreBrush;

            bool isNewForeBrush = SetCellForeColor(currentRow, ref foreBr, Source);
            if (isNewForeBrush)
            {
                if (foreBrush != foreBr)
                {
                    foreBrush.Dispose();
                    foreBrush = foreBr;
                }
            }
            if (SetCellFormat != null)
            {
                SetCellFormat(this, e);
                if (e.ForeBrush != null)
                {
                    foreBrush.Dispose();
                    foreBrush = e.ForeBrush;
                    isNewForeBrush = false;
                }
            }
            if (e.TextFont == null)
                e.TextFont = DataGridTableStyle.DataGrid.Font;
            string text1 = GetText(GetColumnValueAtRow(Source, currentRow));
            g.FillRectangle(backBrush, Bounds);
            g.DrawString(text1, e.TextFont, foreBrush,
                new Rectangle(Bounds.X, Bounds.Y + 2, Bounds.Width, Bounds.Height));
        }

        protected override void Paint(Graphics g, Rectangle Bounds, CurrencyManager Source, int currentRow,
            Brush BackBrush, Brush ForeBrush, bool AlignToRight)
        {
            Brush foreBr = ForeBrush;
            var e = new DataGridFormatCellEventArgs(GetColumnValueAtRow(Source, currentRow), Source.List[currentRow]);

            bool isNewForeBrush = SetCellForeColor(currentRow, ref foreBr, Source);
            if (isNewForeBrush)
            {
                if (ForeBrush != foreBr)
                {
                    ForeBrush = foreBr;
                }
            }
            if (SetCellFormat != null)
            {
                SetCellFormat(this, e);
            }
            if (e.TextFont == null)
                e.TextFont = DataGridTableStyle.DataGrid.Font;
            string text1 = GetText(GetColumnValueAtRow(Source, currentRow));
            g.FillRectangle(BackBrush, Bounds);
            g.DrawString(text1, e.TextFont, ForeBrush,
                new Rectangle(Bounds.X, Bounds.Y + 2, Bounds.Width, Bounds.Height));
        }

        private bool SetCellForeColor(int currentRow, ref Brush foreBrush, CurrencyManager source)
        {
            bool isNewBrush = false;

            if (OnGetForeColorForRow != null)
            {
                //был определён делегат для выбора цвета
                Brush foreBr = OnGetForeColorForRow(currentRow, source);
                if (foreBr != null)
                {
                    foreBrush = foreBr;
                    isNewBrush = true;
                }
                else
                {
                    foreBr = (DataGridTableStyle.DataGrid as ExtendedDataGrid).FireEventGetForeColorForRow(currentRow,
                        source);
                    if (foreBr != null)
                    {
                        foreBrush = foreBr;
                        isNewBrush = true;
                    }
                }
            }
            else
            {
                if ((DataGridTableStyle.DataGrid is ExtendedDataGrid))
                {
                    Brush foreBr =
                        (DataGridTableStyle.DataGrid as ExtendedDataGrid).FireEventGetForeColorForRow(currentRow, source);
                    if (foreBr != null)
                    {
                        foreBrush = foreBr;
                        isNewBrush = true;
                    }
                }
            }
            return isNewBrush;
        }


        protected override void SetDataGridInColumn(DataGrid Value)
        {
            base.SetDataGridInColumn(Value);
            if ((btnEllipsis.Parent != Value) && (btnEllipsis.Parent != null))
            {
                btnEllipsis.Parent.Controls.Remove(btnEllipsis);
            }
            if (Value != null)
            {
                Value.Controls.Add(btnEllipsis);
            }
        }

        // Properties

        public event CellEventHandler ButtonClick;

        private void btnEllipsis_Click(object sender, EventArgs e)
        {
            if (ButtonClick != null)
            {
                ButtonClick(this,
                    new DataGridCellEventArgs(GetColumnValueAtRow(currentSource, currentRow),
                        currentSource.List[currentRow]));
            }
        }

        #region Public Properties

        [DefaultValue(typeof(string), ""), Category("Misc")]
        public string FieldName { get; set; }

        [DefaultValue(typeof(string), ""), Category("Misc")]
        public string FilterFieldName { get; set; }

        #endregion
    }

    public delegate void CellEventHandler(object sender, DataGridCellEventArgs e);

    public class DataGridCellEventArgs : EventArgs
    {
        private readonly object cellValue;
        private readonly object currentRow;

        public DataGridCellEventArgs(object cellValue, object currentRow)
        {
            this.cellValue = cellValue;
            this.currentRow = currentRow;
        }

        //contains the current cell value
        public object CellValue
        {
            get { return cellValue; }
        }

        //contains the current row
        public object CurrentRow
        {
            get { return currentRow; }
        }
    }
}