﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace ColumnMenuExtender
{
	/// <summary>
	/// Summary description for FilterNumbers.
	/// </summary>
	public class FilterNumbersForm : System.Windows.Forms.Form
	{
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.Button btnOk;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label lblNumStart;
		private System.Windows.Forms.Label lblNumEnd;
		public  System.Windows.Forms.TextBox textBoxNumStart;
		public  System.Windows.Forms.TextBox textBoxNumEnd;
		private System.Windows.Forms.GroupBox groupBoxCond;
		public System.Windows.Forms.RadioButton radioButtonByNumber;
		public System.Windows.Forms.RadioButton radioButtonByRange;
		public System.Windows.Forms.RadioButton radioButtonByCond;
		public System.Windows.Forms.RadioButton radioButtonLarger;
		public System.Windows.Forms.RadioButton radioButtonLess;

		public Type DataType;
		private System.Windows.Forms.Button btnClear;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public FilterNumbersForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.textBoxNumEnd = new System.Windows.Forms.TextBox();
			this.textBoxNumStart = new System.Windows.Forms.TextBox();
			this.lblNumEnd = new System.Windows.Forms.Label();
			this.lblNumStart = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.radioButtonByCond = new System.Windows.Forms.RadioButton();
			this.radioButtonByRange = new System.Windows.Forms.RadioButton();
			this.radioButtonByNumber = new System.Windows.Forms.RadioButton();
			this.groupBoxCond = new System.Windows.Forms.GroupBox();
			this.radioButtonLarger = new System.Windows.Forms.RadioButton();
			this.radioButtonLess = new System.Windows.Forms.RadioButton();
			this.btnOk = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnClear = new System.Windows.Forms.Button();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.groupBoxCond.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.textBoxNumEnd);
			this.groupBox1.Controls.Add(this.textBoxNumStart);
			this.groupBox1.Controls.Add(this.lblNumEnd);
			this.groupBox1.Controls.Add(this.lblNumStart);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Controls.Add(this.groupBox2);
			this.groupBox1.Location = new System.Drawing.Point(8, 8);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(504, 224);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Номер или число";
			// 
			// textBoxNumEnd
			// 
			this.textBoxNumEnd.Enabled = false;
			this.textBoxNumEnd.Location = new System.Drawing.Point(136, 96);
			this.textBoxNumEnd.Name = "textBoxNumEnd";
			this.textBoxNumEnd.TabIndex = 1;
			this.textBoxNumEnd.Text = "";
			this.textBoxNumEnd.Validating += new System.ComponentModel.CancelEventHandler(this.textBoxNumEnd_Validating);
			// 
			// textBoxNumStart
			// 
			this.textBoxNumStart.Location = new System.Drawing.Point(136, 64);
			this.textBoxNumStart.Name = "textBoxNumStart";
			this.textBoxNumStart.TabIndex = 0;
			this.textBoxNumStart.Text = "";
			this.textBoxNumStart.Validating += new System.ComponentModel.CancelEventHandler(this.textBoxNumStart_Validating);
			// 
			// lblNumEnd
			// 
			this.lblNumEnd.Enabled = false;
			this.lblNumEnd.Location = new System.Drawing.Point(16, 96);
			this.lblNumEnd.Name = "lblNumEnd";
			this.lblNumEnd.Size = new System.Drawing.Size(104, 23);
			this.lblNumEnd.TabIndex = 3;
			this.lblNumEnd.Text = "Конечный номер";
			this.lblNumEnd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lblNumStart
			// 
			this.lblNumStart.Location = new System.Drawing.Point(16, 64);
			this.lblNumStart.Name = "lblNumStart";
			this.lblNumStart.Size = new System.Drawing.Size(104, 23);
			this.lblNumStart.TabIndex = 2;
			this.lblNumStart.Text = "Начальный номер";
			this.lblNumStart.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(24, 40);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(100, 16);
			this.label1.TabIndex = 1;
			this.label1.Text = "Искомый номер:";
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.radioButtonByCond);
			this.groupBox2.Controls.Add(this.radioButtonByRange);
			this.groupBox2.Controls.Add(this.radioButtonByNumber);
			this.groupBox2.Controls.Add(this.groupBoxCond);
			this.groupBox2.Location = new System.Drawing.Point(304, 16);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(184, 168);
			this.groupBox2.TabIndex = 0;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Условия поиска";
			// 
			// radioButtonByCond
			// 
			this.radioButtonByCond.Location = new System.Drawing.Point(16, 64);
			this.radioButtonByCond.Name = "radioButtonByCond";
			this.radioButtonByCond.TabIndex = 4;
			this.radioButtonByCond.Text = "по условию";
			this.radioButtonByCond.CheckedChanged += new System.EventHandler(this.radioButtonByCond_CheckedChanged);
			// 
			// radioButtonByRange
			// 
			this.radioButtonByRange.Location = new System.Drawing.Point(16, 40);
			this.radioButtonByRange.Name = "radioButtonByRange";
			this.radioButtonByRange.Size = new System.Drawing.Size(136, 24);
			this.radioButtonByRange.TabIndex = 3;
			this.radioButtonByRange.Text = "по диапазону чисел";
			this.radioButtonByRange.CheckedChanged += new System.EventHandler(this.radioButtonByRange_CheckedChanged);
			// 
			// radioButtonByNumber
			// 
			this.radioButtonByNumber.Checked = true;
			this.radioButtonByNumber.Location = new System.Drawing.Point(16, 16);
			this.radioButtonByNumber.Name = "radioButtonByNumber";
			this.radioButtonByNumber.TabIndex = 2;
			this.radioButtonByNumber.TabStop = true;
			this.radioButtonByNumber.Text = "по числу";
			// 
			// groupBoxCond
			// 
			this.groupBoxCond.Controls.Add(this.radioButtonLarger);
			this.groupBoxCond.Controls.Add(this.radioButtonLess);
			this.groupBoxCond.Enabled = false;
			this.groupBoxCond.Location = new System.Drawing.Point(32, 88);
			this.groupBoxCond.Name = "groupBoxCond";
			this.groupBoxCond.Size = new System.Drawing.Size(128, 64);
			this.groupBoxCond.TabIndex = 1;
			this.groupBoxCond.TabStop = false;
			// 
			// radioButtonLarger
			// 
			this.radioButtonLarger.Checked = true;
			this.radioButtonLarger.Location = new System.Drawing.Point(8, 8);
			this.radioButtonLarger.Name = "radioButtonLarger";
			this.radioButtonLarger.TabIndex = 5;
			this.radioButtonLarger.TabStop = true;
			this.radioButtonLarger.Text = "больше";
			// 
			// radioButtonLess
			// 
			this.radioButtonLess.Location = new System.Drawing.Point(8, 32);
			this.radioButtonLess.Name = "radioButtonLess";
			this.radioButtonLess.TabIndex = 6;
			this.radioButtonLess.Text = "меньше";
			// 
			// btnOk
			// 
			this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnOk.Location = new System.Drawing.Point(440, 240);
			this.btnOk.Name = "btnOk";
			this.btnOk.TabIndex = 7;
			this.btnOk.Text = "Ok";
			// 
			// btnCancel
			// 
			this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Location = new System.Drawing.Point(8, 240);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.TabIndex = 9;
			this.btnCancel.Text = "Закрыть";
			// 
			// btnClear
			// 
			this.btnClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnClear.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnClear.Location = new System.Drawing.Point(352, 240);
			this.btnClear.Name = "btnClear";
			this.btnClear.TabIndex = 8;
			this.btnClear.Text = "Очистить";
			this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
			// 
			// FilterNumbersForm
			// 
			this.AcceptButton = this.btnOk;
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.CancelButton = this.btnCancel;
			this.ClientSize = new System.Drawing.Size(522, 271);
			this.ControlBox = false;
			this.Controls.Add(this.btnClear);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnOk);
			this.Controls.Add(this.groupBox1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Name = "FilterNumbersForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Фильтр";
			this.groupBox1.ResumeLayout(false);
			this.groupBox2.ResumeLayout(false);
			this.groupBoxCond.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void radioButtonByCond_CheckedChanged(object sender, System.EventArgs e)
		{
			if (radioButtonByCond.Checked)
				groupBoxCond.Enabled = true;
			else
				groupBoxCond.Enabled = false;
		}

		private void radioButtonByRange_CheckedChanged(object sender, System.EventArgs e)
		{
			if (radioButtonByRange.Checked)
			{
				lblNumEnd.Enabled = true;
				textBoxNumEnd.Enabled = true;
			}
			else
			{
				lblNumEnd.Enabled = false;
				textBoxNumEnd.Enabled = false;
			}
		}

		private void textBoxNumStart_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (textBoxNumStart.Text == "")
				return;
			try
			{
				if (DataType == typeof(Int32))
					Convert.ToInt32(textBoxNumStart.Text);
				else if (DataType == typeof(Double))
					Convert.ToDouble(textBoxNumStart.Text);
				else if (DataType == typeof(Decimal))
					Convert.ToDecimal(textBoxNumStart.Text);
			}
			catch
			{
				e.Cancel = true;
			}
		}

		private void textBoxNumEnd_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (textBoxNumEnd.Text == "")
				return;
			try
			{
				if (DataType == typeof(Int32))
					Convert.ToInt32(textBoxNumEnd.Text);
				else if (DataType == typeof(Double))
					Convert.ToDouble(textBoxNumEnd.Text);
				else if (DataType == typeof(Decimal))
					Convert.ToDecimal(textBoxNumEnd.Text);
			}
			catch
			{
				e.Cancel = true;
			}
		}

		private void btnClear_Click(object sender, System.EventArgs e)
		{
			textBoxNumEnd.Text = "";
			textBoxNumStart.Text = "";
		}

	}
}
