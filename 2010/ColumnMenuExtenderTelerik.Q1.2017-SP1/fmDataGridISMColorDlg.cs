﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

namespace ColumnMenuExtender
{	
	public class fmDataGridISMColorDlg : System.Windows.Forms.Form
	{
		#region Vars
		private DataTable tblRowColors;
		//ColoredTextBoxColumn coloredColumn1;
		#endregion

		#region Windows Vars

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button btnAddColor;
		private System.Windows.Forms.Button btnRemoveColor;
		private System.Windows.Forms.Button btnColor;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnSave;
		private System.Windows.Forms.DataGrid dataGrid1;
		private System.Windows.Forms.TextBox tbRowQuantity;
		private System.Windows.Forms.Button btnUp;
		private System.Windows.Forms.Button btnDown;
		private System.Windows.Forms.Label lbColor;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Panel panel2;
		//private ColoredDataGrid dataGrid1;
		
		private System.ComponentModel.Container components = null;
		#endregion

		#region Properties
		public DataTable RowColors
		{
			get { return tblRowColors; }			
		}
		#endregion

		#region .ctor
		public fmDataGridISMColorDlg(DataTable tblRowColors)
		{			
			InitializeComponent();

			this.tblRowColors = tblRowColors.Copy();
			this.tblRowColors.DefaultView.AllowEdit = false; 
			this.tblRowColors.DefaultView.AllowNew = false; 
	
			dataGrid1.TableStyles.Add(GetTableStyle()); 	
			dataGrid1.DataSource = this.tblRowColors; 					
			
			tbRowQuantity.DataBindings.Add("Text",this.tblRowColors,"quantity");	
		
			CurrencyManager cm = (CurrencyManager) dataGrid1.BindingContext[dataGrid1.DataSource, dataGrid1.DataMember];			
			
			cm.PositionChanged += new EventHandler(cm_PositionChanged);
			cm_PositionChanged(null,null);
		}
	
		private DataGridTableStyle GetTableStyle() 
		{ 
			ColoredTextBoxColumn coloredColumn = new ColoredTextBoxColumn(); 
			coloredColumn.MappingName = "color";             
			coloredColumn.ReadOnly = true;
			coloredColumn.Width = 132;
			
			DataGridTableStyle tableStyle = new DataGridTableStyle(); 
			tableStyle.MappingName = "RowColors"; 
			tableStyle.GridColumnStyles.AddRange(new DataGridColumnStyle[] {coloredColumn}); 
			tableStyle.RowHeadersVisible = false;	
			tableStyle.ColumnHeadersVisible = false;
		
			//coloredColumn1 = coloredColumn;
			return tableStyle; 
		}
				
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}
		#endregion

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(fmDataGridISMColorDlg));
			this.btnAddColor = new System.Windows.Forms.Button();
			this.btnRemoveColor = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.btnColor = new System.Windows.Forms.Button();
			this.tbRowQuantity = new System.Windows.Forms.TextBox();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnSave = new System.Windows.Forms.Button();
			this.dataGrid1 = new System.Windows.Forms.DataGrid();
			this.btnUp = new System.Windows.Forms.Button();
			this.btnDown = new System.Windows.Forms.Button();
			this.lbColor = new System.Windows.Forms.Label();
			this.panel1 = new System.Windows.Forms.Panel();
			this.panel2 = new System.Windows.Forms.Panel();
			((System.ComponentModel.ISupportInitialize)(this.dataGrid1)).BeginInit();
			this.panel1.SuspendLayout();
			this.panel2.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnAddColor
			// 
			this.btnAddColor.Location = new System.Drawing.Point(8, 200);
			this.btnAddColor.Name = "btnAddColor";
			this.btnAddColor.Size = new System.Drawing.Size(64, 23);
			this.btnAddColor.TabIndex = 1;
			this.btnAddColor.Text = "Добавить";
			this.btnAddColor.Click += new System.EventHandler(this.btnAddColor_Click);
			// 
			// btnRemoveColor
			// 
			this.btnRemoveColor.Location = new System.Drawing.Point(80, 200);
			this.btnRemoveColor.Name = "btnRemoveColor";
			this.btnRemoveColor.Size = new System.Drawing.Size(64, 23);
			this.btnRemoveColor.TabIndex = 2;
			this.btnRemoveColor.Text = "Удалить";
			this.btnRemoveColor.Click += new System.EventHandler(this.btnRemoveColor_Click);
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(152, 88);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(40, 23);
			this.label1.TabIndex = 3;
			this.label1.Text = "Цвет:";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(152, 120);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(104, 23);
			this.label2.TabIndex = 4;
			this.label2.Text = "Количество строк:";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// btnColor
			// 
			this.btnColor.Location = new System.Drawing.Point(260, 88);
			this.btnColor.Name = "btnColor";
			this.btnColor.Size = new System.Drawing.Size(24, 23);
			this.btnColor.TabIndex = 5;
			this.btnColor.Text = "...";
			this.btnColor.Click += new System.EventHandler(this.btnColor_Click);
			// 
			// tbRowQuantity
			// 
			this.tbRowQuantity.Location = new System.Drawing.Point(260, 120);
			this.tbRowQuantity.Name = "tbRowQuantity";
			this.tbRowQuantity.Size = new System.Drawing.Size(56, 20);
			this.tbRowQuantity.TabIndex = 6;
			this.tbRowQuantity.Text = "";
			// 
			// btnCancel
			// 
			this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnCancel.Location = new System.Drawing.Point(4, 4);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(72, 23);
			this.btnCancel.TabIndex = 7;
			this.btnCancel.Text = "Отменить";
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// btnSave
			// 
			this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnSave.Location = new System.Drawing.Point(244, 4);
			this.btnSave.Name = "btnSave";
			this.btnSave.Size = new System.Drawing.Size(72, 23);
			this.btnSave.TabIndex = 8;
			this.btnSave.Text = "Сохранить";
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			// 
			// dataGrid1
			// 
			this.dataGrid1.CaptionVisible = false;
			this.dataGrid1.ColumnHeadersVisible = false;
			this.dataGrid1.DataMember = "";
			this.dataGrid1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dataGrid1.Location = new System.Drawing.Point(8, 8);
			this.dataGrid1.Name = "dataGrid1";
			this.dataGrid1.RowHeadersVisible = false;
			this.dataGrid1.Size = new System.Drawing.Size(136, 184);
			this.dataGrid1.TabIndex = 9;
			this.dataGrid1.DoubleClick += new System.EventHandler(this.dataGrid1_DoubleClick);
			// 
			// btnUp
			// 
			this.btnUp.Image = ((System.Drawing.Image)(resources.GetObject("btnUp.Image")));
			this.btnUp.Location = new System.Drawing.Point(152, 8);
			this.btnUp.Name = "btnUp";
			this.btnUp.Size = new System.Drawing.Size(16, 24);
			this.btnUp.TabIndex = 10;
			this.btnUp.Click += new System.EventHandler(this.btnUp_Click);
			// 
			// btnDown
			// 
			this.btnDown.Image = ((System.Drawing.Image)(resources.GetObject("btnDown.Image")));
			this.btnDown.Location = new System.Drawing.Point(152, 40);
			this.btnDown.Name = "btnDown";
			this.btnDown.Size = new System.Drawing.Size(16, 24);
			this.btnDown.TabIndex = 11;
			this.btnDown.Click += new System.EventHandler(this.btnDown_Click);
			// 
			// lbColor
			// 
			this.lbColor.Location = new System.Drawing.Point(216, 88);
			this.lbColor.Name = "lbColor";
			this.lbColor.Size = new System.Drawing.Size(32, 23);
			this.lbColor.TabIndex = 12;
			this.lbColor.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.btnSave);
			this.panel1.Controls.Add(this.btnCancel);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(0, 256);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(320, 32);
			this.panel1.TabIndex = 13;
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.btnUp);
			this.panel2.Controls.Add(this.btnDown);
			this.panel2.Controls.Add(this.lbColor);
			this.panel2.Controls.Add(this.btnAddColor);
			this.panel2.Controls.Add(this.btnRemoveColor);
			this.panel2.Controls.Add(this.label1);
			this.panel2.Controls.Add(this.label2);
			this.panel2.Controls.Add(this.btnColor);
			this.panel2.Controls.Add(this.tbRowQuantity);
			this.panel2.Controls.Add(this.dataGrid1);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel2.Location = new System.Drawing.Point(0, 0);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(320, 256);
			this.panel2.TabIndex = 14;
			// 
			// fmDataGridISMColorDlg
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(320, 288);
			this.ControlBox = false;
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel1);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.MinimumSize = new System.Drawing.Size(328, 296);
			this.Name = "fmDataGridISMColorDlg";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Выбор цветов строк таблицы";
			this.Load += new System.EventHandler(this.fmDataGridISMColorDlg_Load);
			((System.ComponentModel.ISupportInitialize)(this.dataGrid1)).EndInit();
			this.panel1.ResumeLayout(false);
			this.panel2.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		#region EventHandlers
		private void btnAddColor_Click(object sender, System.EventArgs e)
		{
			ColorDialog clrdlg = new ColorDialog();
			clrdlg.FullOpen = true;
			
			if (clrdlg.ShowDialog() == DialogResult.OK) 
			{
				DataRow row = tblRowColors.NewRow();
				row["color"] = clrdlg.Color;
				row["quantity"] = 1;
				tblRowColors.Rows.Add(row);
				
				CurrencyManager cm = (CurrencyManager) dataGrid1.BindingContext[dataGrid1.DataSource, dataGrid1.DataMember];
				if(cm.Count > 0)
					cm.Position = cm.Count - 1;
			}
		}

		private void btnSave_Click(object sender, System.EventArgs e)
		{
			tblRowColors.AcceptChanges();
			int position = 0;
			foreach(DataRow row in tblRowColors.Rows)
			{
				position += (int)row["quantity"];
				row["position"] = position;
			}			
			DialogResult = DialogResult.OK;
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{			
			tblRowColors.RejectChanges();
			DialogResult = DialogResult.Cancel;			
		}

		private void btnRemoveColor_Click(object sender, System.EventArgs e)
		{
			DataRow selectedRow = GetSelectedRow();
			if(selectedRow != null) {
				selectedRow.Delete();		
				tblRowColors.AcceptChanges();
				cm_PositionChanged(null,null);
			}
		}

		private void cm_PositionChanged(object sender, EventArgs e)
		{
			DataRow selectedRow = GetSelectedRow();
			if(selectedRow != null)
				lbColor.BackColor = (Color) selectedRow["color"];	
			else
				lbColor.BackColor = this.BackColor;
				
		}

		private void btnUp_Click(object sender, System.EventArgs e)
		{
			DataRow selectedRow = GetSelectedRow();
			if(selectedRow == null)
				return;
			
			int index = 0;
			for(int i = 0; i < tblRowColors.Rows.Count; i++)
			{
				DataRow currentRow = tblRowColors.Rows[i];
				if (currentRow == selectedRow){
					index = i;
					break;
				}	
			}
			
			if (index > 0) {
				DataRow row = tblRowColors.NewRow();
				row.ItemArray = selectedRow.ItemArray;
				tblRowColors.Rows.InsertAt(row, index - 1);
				selectedRow.Delete();		
				tblRowColors.AcceptChanges();
				dataGrid1.CurrentRowIndex = dataGrid1.CurrentRowIndex - 1;
			}		
		}

		private void btnDown_Click(object sender, System.EventArgs e)
		{
			DataRow selectedRow = GetSelectedRow();
			if(selectedRow == null)
				return;
			
			int index = 0;
			for(int i = 0; i < tblRowColors.Rows.Count; i++)
			{
				DataRow currentRow = tblRowColors.Rows[i];
				if (currentRow == selectedRow)
				{
					index = i;
					break;
				}	
			}
			
			if (index < tblRowColors.Rows.Count - 1) 
			{
				DataRow row = tblRowColors.NewRow();
				row.ItemArray = selectedRow.ItemArray;
				selectedRow.Delete();		
				tblRowColors.AcceptChanges();
				tblRowColors.Rows.InsertAt(row, index + 1);
				tblRowColors.AcceptChanges();
				dataGrid1.CurrentRowIndex = dataGrid1.CurrentRowIndex + 1;
			}			
		}

		private void btnColor_Click(object sender, System.EventArgs e)
		{
			DataRow selectedRow = GetSelectedRow();
			if(selectedRow == null)
				return;

			ColorDialog clrdlg = new ColorDialog();
			try
			{
				Color color = (Color) selectedRow["color"];
				clrdlg.Color = color;
			}
			catch{}
			
			clrdlg.FullOpen = true;
			
			if (clrdlg.ShowDialog() == DialogResult.OK) 
				selectedRow["color"] = clrdlg.Color;
		}

		private void dataGrid1_DoubleClick(object sender, System.EventArgs e)
		{
			btnColor_Click(null,null);
		}		
 		#endregion				
		
		#region Functions
		public DataRow GetSelectedRow()
		{
			CurrencyManager cm = (CurrencyManager) dataGrid1.BindingContext[dataGrid1.DataSource, dataGrid1.DataMember];
			if(cm.Position != -1)
			{
				DataRowView rv = (DataRowView)cm.Current;
				return rv.Row;
			}
			else
				return null;
		}
		#endregion				

		private void fmDataGridISMColorDlg_Load(object sender, System.EventArgs e)
		{
			((Control) dataGrid1).Select();
		}
	}

	class ColoredTextBoxColumn:  DataGridTextBoxColumn //DataGridColumnStyle
	{ 		
		protected override void Paint(Graphics g, Rectangle bounds, CurrencyManager source, int rowNum, 
			Brush backBrush, Brush foreBrush, bool alignToRight) 
		{ 
			DataRowView drv = (DataRowView) source.List[rowNum];
			Color currentColor = Color.White;
			if(drv["color"] != DBNull.Value)
				currentColor = (Color) drv["color"];

			backBrush = new SolidBrush(currentColor);
			g.FillRectangle(backBrush, bounds);
						
			if(rowNum == this.DataGridTableStyle.DataGrid.CurrentRowIndex)			
			{				
				float brightness = (float)(currentColor.R + currentColor.G + currentColor.B)/3;
				Color penColor;
				if (brightness >= 127) 
					penColor = Color.Black;
				else
					penColor = Color.White;

				Pen pen = new Pen(penColor, 2);
				pen.DashStyle = System.Drawing.Drawing2D.DashStyle.Dash;				
				g.DrawRectangle(pen, bounds.X + 1, bounds.Y + 1, bounds.Width - 2, bounds.Height - 2);	
				pen.Dispose();
			}
	
			backBrush.Dispose();			
		}

		protected override void Edit(CurrencyManager source, int rowNum, Rectangle bounds, bool readOnly, string instantText, bool cellIsVisible)
		{			
		}

	}


}
