﻿using System;
using System.Xml;
using System.Data;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;
using System.ComponentModel;
using MetaData;
using System.Drawing;
using System.IO;
using ColumnMenuExtender.Forms.Dialogs;
using Telerik.WinControls;
using Telerik.WinControls.UI;

namespace ColumnMenuExtender.Forms
{
	/// <summary>
	/// Summary description for BaseGenericForm.
	/// </summary>
	public partial class BaseGenericForm : RadForm
	{
		public event EventHandler BeforeLoad;
		public event EventHandler BeforeClosing;
		public static IfmMain MainForm;//для того чтобы все формы имели доступ к свойствам и методам MainForm
		//private SizeF fontCorrection = SizeF.Empty;

		//protected bool IsSavingFonts = true;

		#region ShowMsgs
		public virtual DialogResult ShowError(string message)
		{
			return MessageBox.Show(this, message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
		}

		public virtual DialogResult ShowErrorWithoutForm(string message)
		{
			return MessageBox.Show(message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
		}

		public virtual DialogResult ShowError(IWin32Window owner, string message)
		{
			return MessageBox.Show(owner, message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
		}

		public virtual DialogResult ShowError(string message, string exMessage)
		{
			string mes = message;
			//#if(DEBUG)
			mes += "\n" + exMessage;
			//#endif
			return MessageBox.Show(this, mes, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
		}

		public virtual DialogResult ShowErrorWithoutForm(string message, string exMessage)
		{
			string mes = message;
			//#if(DEBUG)
			mes += "\n" + exMessage;
			//#endif
			return MessageBox.Show(mes, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
		}

		public virtual DialogResult ShowError(IWin32Window owner, string message, string exMessage)
		{
			string mes = message;
			//#if(DEBUG)
			mes += "\n" + exMessage;
			//#endif
			return MessageBox.Show(owner, message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
		}

		public virtual DialogResult ShowError(string message, MessageBoxButtons MBButtons)
		{
			return MessageBox.Show(this, message, "Ошибка", MBButtons, MessageBoxIcon.Error);
		}

		public virtual DialogResult ShowErrorWithoutForm(string message, MessageBoxButtons MBButtons)
		{
			return MessageBox.Show(message, "Ошибка", MBButtons, MessageBoxIcon.Error);
		}

		public virtual DialogResult ShowError(IWin32Window owner, string message, MessageBoxButtons MBButtons)
		{
			return MessageBox.Show(owner, message, "Ошибка", MBButtons, MessageBoxIcon.Error);
		}

		public virtual DialogResult ShowWarningConstraintEx()
		{
			return MessageBox.Show(this, "Дубли не разрешены", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
		}

		public virtual DialogResult ShowWarningConstraintEx(IWin32Window owner)
		{
			return MessageBox.Show(owner, "Дубли не разрешены", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
		}

		public virtual DialogResult ShowWarning(string message)
		{
			return MessageBox.Show(this, message, "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
		}

		public virtual DialogResult ShowWarning(string message, MessageBoxButtons MBButtons)
		{
			return MessageBox.Show(this, message, "Внимание", MBButtons, MessageBoxIcon.Warning);
		}

		public virtual DialogResult ShowWarningWithoutForm(string message)
		{
			return MessageBox.Show(message, "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
		}

		public virtual DialogResult ShowWarningWithoutForm(string message, MessageBoxButtons MBButtons)
		{
			return MessageBox.Show(message, "Внимание", MBButtons, MessageBoxIcon.Warning);
		}

		/*public void ShowSoapException(SoapException err)
		{
			string message = "Type: "+err.GetType().ToString()+"\r\n\r\n";
			message += "*** Message: ***\r\n"+err.Message+"\r\n\r\n";
			message += "*** Actor: ***\r\n"+err.Actor+"\r\n\r\n";
			message += "*** Code: ***\r\n"+err.Code+"\r\n\r\n";
			message += "*** Detail: ***\r\n"+err.Detail.OuterXml;
			MessageBox.Show(this, message);
		}*/

		//PUBLIC STATIC
		public static DialogResult ShowError_DBConnection(IWin32Window owner, Exception ex)
		{
			string mes = "Ошибка соединения с базой данных";
			//#if(DEBUG)
			if (ex != null)
				mes += Environment.NewLine + ex.Message;
			//#endif
			return MessageBox.Show(owner, mes, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
		}

		public static DialogResult ShowError(IWin32Window owner, string message, Exception ex)
		{
			string mes = message;
			//#if(DEBUG)
			if (ex != null)
				mes += Environment.NewLine + ex.Message;
			//#endif
			return MessageBox.Show(owner, mes, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
		}
		#endregion

		private bool isSavingOrder = true;
		private bool isSavingFilters = true;

		[DefaultValue(true)]
		public bool IsSavingOrder
		{
			get { return isSavingOrder; }
			set { isSavingOrder = value; }
		}

		[DefaultValue(true)]
		public bool IsSavingFilters
		{
			get { return isSavingFilters; }
			set { isSavingFilters = value; }
		}

		static BaseGenericForm()
		{
			Cache = new Hashtable();
			//almost_cash - создаётся только там где он нужен
			//ThemeResolutionService.ApplicationThemeName = "Desert";
		}

		public Hashtable almost_cash;//КЭШ конкретной формы (создавать по необходимости)
		public Dictionary<string, string> Additionals = new Dictionary<string, string>();
		public static Hashtable Cache;//Общий КЭШ

		private string filename_load = Application.StartupPath + @"\forms.xml";
		protected virtual string FileName//{get;}
		{
			get
			{
				string s = AppDomain.CurrentDomain.FriendlyName;
				return Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) +
					@"\Invento\" + s + @"\forms.xml";
			}
		}

		protected virtual string HintTextBoxFileName//{get;}
		{
			get
			{
				string s = AppDomain.CurrentDomain.FriendlyName;
				return Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) +
					@"\Invento\" + s + @"\HintTextBox.xml";
			}
		}

		private string dgNamePrefix = "";

		private bool isCorrect(XmlDocument xmlDoc)
		{
			if (xmlDoc.SelectSingleNode("/root/forms") != null)
				return true;
			else
				return false;
		}

		private XmlDocument createNewXmlDoc()
		{
			XmlDocument xmlDoc = new XmlDocument();
			XmlNode root = xmlDoc.CreateNode(XmlNodeType.Element, "root", "");
			xmlDoc.AppendChild(root);
			XmlNode node = xmlDoc.CreateNode(XmlNodeType.Element, "forms", "");
			root.AppendChild(node);
			return xmlDoc;
		}

		private void writeAttribute(XmlNode node, string attrName, string attrValue)
		{
			XmlDocument xmlDoc = node.OwnerDocument;
			XmlAttribute attr = node.Attributes[attrName];
			if (attr == null)
				attr = xmlDoc.CreateAttribute(attrName);
			attr.Value = attrValue;
			node.Attributes.Append(attr);
		}

		private XmlNode createNamedNode(XmlNode node, string nodeName, string name)
		{
			XmlDocument xmlDoc = node.OwnerDocument;
			XmlNode newNode = node.SelectSingleNode(nodeName + "[@name='" + name + "']");
			if (newNode == null)
			{
				newNode = xmlDoc.CreateNode(XmlNodeType.Element, nodeName, "");
			}
			else
			{
				newNode.RemoveAll();
			}
			XmlAttribute attr = xmlDoc.CreateAttribute("name");
			attr.Value = name;
			newNode.Attributes.Append(attr);
			node.AppendChild(newNode);
			return newNode;
		}

		private void createPositionNode(XmlNode node, Control ctrl)
		{
			XmlDocument xmlDoc = node.OwnerDocument;
			XmlNode ctrlNode = createNamedNode(node, ctrl.GetType().ToString(), ctrl.Name);
			XmlAttribute attr = xmlDoc.CreateAttribute("width");
			ctrlNode.Attributes.Append(xmlDoc.CreateAttribute("left")).Value = ctrl.Location.X.ToString();
			ctrlNode.Attributes.Append(xmlDoc.CreateAttribute("top")).Value = ctrl.Location.Y.ToString();
			ctrlNode.Attributes.Append(xmlDoc.CreateAttribute("width")).Value = ctrl.Size.Width.ToString();
			ctrlNode.Attributes.Append(xmlDoc.CreateAttribute("height")).Value = ctrl.Size.Height.ToString();
		}
		private void createDataGridNode(XmlNode node, DataGrid dataGrid)
		{
			XmlDocument xmlDoc = node.OwnerDocument;
			XmlNode dgNode = createNamedNode(node, "datagrid", dgNamePrefix + dataGrid.Name);

			foreach (DataGridTableStyle style in dataGrid.TableStyles)
			{
				XmlNode styleNode = createNamedNode(dgNode, "style", style.MappingName);
				foreach (DataGridColumnStyle column in style.GridColumnStyles)
				{
					if (column.Width > 0)
					{
						XmlNode columnNode = createNamedNode(styleNode, "column", column.MappingName);
						writeAttribute(columnNode, "Width", column.Width.ToString());
					}
				}
			}

			if (IsSavingOrder && dataGrid is DataGridISM)
			{
				DataGridISM dgi = dataGrid as DataGridISM;
				XmlNode ordersNode = xmlDoc.CreateNode(XmlNodeType.Element, "orders", "");
				dgNode.AppendChild(ordersNode);
				if (dgi.Order != null && !dgi.Order.IsDefault)
					dgi.Order.CreateXmlNode(ordersNode);
			}

			if (MainForm != null && MainForm.isSaveFilters && IsSavingFilters && dataGrid is DataGridISM)
			{
				DataGridISM dgi = dataGrid as DataGridISM;
				XmlNode filtersNode = xmlDoc.CreateNode(XmlNodeType.Element, "filters", "");
				dgNode.AppendChild(filtersNode);
				foreach (DictionaryEntry de in dgi.StorableFilters)
				{
					((FilterInfo)de.Value).CreateXmlNode(filtersNode);
				}
			}

			if (dataGrid is ExtendedDataGrid)
			{
				ExtendedDataGrid dg = (ExtendedDataGrid)dataGrid;
				DataTable RowColorsTable = dg.RowColorsTable;
				XmlElement RowColorsTableNode = xmlDoc.CreateElement("RowColorsTable");
				foreach (DataRow row in RowColorsTable.Rows)
				{
					XmlNode rowNode = xmlDoc.CreateElement("row");
					XmlNode dataNode = xmlDoc.CreateElement("color");
					dataNode.InnerText = ((Color)row["color"]).ToArgb().ToString();
					rowNode.AppendChild(dataNode);

					dataNode = xmlDoc.CreateElement("quantity");
					dataNode.InnerText = row["quantity"].ToString();
					rowNode.AppendChild(dataNode);

					RowColorsTableNode.AppendChild(rowNode);
				}

				dgNode.AppendChild(RowColorsTableNode);
			}
		}

		private void createRadGridViewNode(XmlNode node, RadGridView gridView)
		{
			XmlDocument xmlDoc = node.OwnerDocument;
			XmlNode dgNode = createNamedNode(node, "gridview", dgNamePrefix + gridView.Name);

			foreach (var column in gridView.Columns)
			{
				if (column.Width > 0)
				{
					XmlNode columnNode = createNamedNode(dgNode, "column", column.Name);
					writeAttribute(columnNode, "Width", column.Width.ToString());
				}
			}
		}

		private void createPagerNode(XmlNode node, DataGridPager dataGridPager)
		{
			XmlDocument xmlDoc = node.OwnerDocument;
			XmlNode dgpNode = createNamedNode(node, "datagridpager", dgNamePrefix + dataGridPager.Name);
			writeAttribute(dgpNode, "batch", dataGridPager.Batch.ToString());
		}

		protected virtual string GetFormName()
		{
			string name = "unnamed";//Не должно быть!

			if (this is BaseListDialog)
				name = Name + "_" + ((BaseListDialog)this).ClassName;
			else name = Name;

			return name;
		}

		private void saveToFile()
		{
			XmlDocument xmlDoc = new XmlDocument();
			if (File.Exists(FileName))
			{
				try
				{
					xmlDoc.Load(FileName);
					if (!isCorrect(xmlDoc))
						xmlDoc = createNewXmlDoc();
				}
				catch
				{
					xmlDoc = createNewXmlDoc();
				}
			}
			else
			{
				try
				{
					xmlDoc.Load(filename_load);
					if (!isCorrect(xmlDoc))
						xmlDoc = createNewXmlDoc();
				}
				catch
				{
					xmlDoc = createNewXmlDoc();
				}
			}

			XmlDocument xmlDoc2 = new XmlDocument();
			try
			{
				xmlDoc2.Load(HintTextBoxFileName);
				if (!isCorrect(xmlDoc2))
					xmlDoc2 = createNewXmlDoc();
			}
			catch
			{
				xmlDoc2 = createNewXmlDoc();
			}

			XmlNode formsNode = xmlDoc.SelectSingleNode("/root/forms");
			XmlNode formsNode2 = xmlDoc2.SelectSingleNode("/root/forms");


			string name = GetFormName();//Получаем имя формы
			XmlNode formNode;
			XmlNode formNode2;

			formNode = createNamedNode(formsNode, "form", name);
			formNode2 = createNamedNode(formsNode2, "form", name);
			if (this is IfmMain)
			{
				XmlElement node = formNode.OwnerDocument.CreateElement("isCloseWarningShow");
				node.InnerText = (this as IfmMain).isCloseWarningShow.ToString();
				formNode.AppendChild(node);
				node = formNode.OwnerDocument.CreateElement("isCloseAfterSave");
				node.InnerText = (this as IfmMain).isCloseAfterSave.ToString();
				formNode.AppendChild(node);
				node = formNode.OwnerDocument.CreateElement("isExportBeforePrint");
				node.InnerText = (this as IfmMain).isExportBeforePrint.ToString();
				formNode.AppendChild(node);
				node = formNode.OwnerDocument.CreateElement("isSaveFilters");
				node.InnerText = (this as IfmMain).isSaveFilters.ToString();
				formNode.AppendChild(node);

				//Сохраним шрифт
				//if (IsSavingFonts)
				//{
				//  node = formNode.OwnerDocument.CreateElement("font");
				//  formNode.AppendChild(node);
				//  XmlAttribute atr = formNode.OwnerDocument.CreateAttribute("fontSize");//size
				//  node.Attributes.Append(atr);
				//  atr.Value = (this as IfmMain).Font.SizeInPoints.ToString();
				//  atr = formNode.OwnerDocument.CreateAttribute("fontFamily");//fontFamily
				//  node.Attributes.Append(atr);
				//  atr.Value = (this as IfmMain).Font.FontFamily.Name;
				//  atr = formNode.OwnerDocument.CreateAttribute("fontStyle");//fontStyle
				//  node.Attributes.Append(atr);
				//  atr.Value = (this as IfmMain).Font.Style.ToString();
				//}
			}

			if (FormBorderStyle == FormBorderStyle.Sizable || FormBorderStyle == FormBorderStyle.SizableToolWindow)
			{
				if (WindowState == FormWindowState.Normal)
				{
					int width = Width;
					int height = Height;
					//if (IsSavingFonts)
					//{
					//  width = (int)(((float)this.Width) / fontCorrection.Width);
					//  height = (int)(((float)this.Height) / fontCorrection.Height);
					//}

					writeAttribute(formNode, "Width", width.ToString());
					writeAttribute(formNode, "Height", height.ToString());
				}
			}

			if (WindowState == FormWindowState.Normal)
			{
				writeAttribute(formNode, "Top", Top.ToString());
				writeAttribute(formNode, "Left", Left.ToString());
			}

			createControls(formNode, formNode2, Controls);

			//Допишем необходимые дополнительные параметры (если они есть)
			createAdditionals(formNode);

			try
			{
				xmlDoc.Save(FileName);
			}
			catch (DirectoryNotFoundException)
			{
				string path = Path.GetDirectoryName(FileName);
				Directory.CreateDirectory(path);
				xmlDoc.Save(FileName);
			}
			try
			{
				xmlDoc2.Save(HintTextBoxFileName);
			}
			catch (DirectoryNotFoundException)
			{
				string path = Path.GetDirectoryName(HintTextBoxFileName);
				Directory.CreateDirectory(path);
				xmlDoc2.Save(HintTextBoxFileName);
			}
		}

		private void createAdditionals(XmlNode formNode)
		{
			XmlNode addNode = addNewNode(formNode, "additionals");

			foreach (KeyValuePair<string, string> kv in Additionals)
				addNewNode(addNode, kv.Key, "Value", kv.Value);
		}

		private void loadAdditionals(XmlNode formNode)
		{
			XmlNode addNode = formNode.SelectSingleNode("additionals");
			if (addNode != null)
			{
				foreach (XmlNode node in addNode.ChildNodes)
				{
					try
					{
						Additionals[node.Name] = node.Attributes["Value"].Value;
					}
					catch { }
				}
			}
		}

		private XmlNode addNewNode(XmlNode Node, string NodeName)
		{
			XmlNode newNode = Node.SelectSingleNode(NodeName);
			if (newNode == null)
				newNode = Node.OwnerDocument.CreateNode(XmlNodeType.Element, NodeName, "");
			else newNode.RemoveAll();

			Node.AppendChild(newNode);
			return newNode;
		}

		private XmlNode addNewNode(XmlNode Node, string NodeName, string AttrName, string Value)
		{
			XmlNode newNode = Node.SelectSingleNode(NodeName);
			if (newNode == null)
				newNode = Node.OwnerDocument.CreateNode(XmlNodeType.Element, NodeName, "");
			else newNode.RemoveAll();

			XmlAttribute attr = Node.OwnerDocument.CreateAttribute(AttrName);
			attr.Value = Value;
			newNode.Attributes.Append(attr);

			Node.AppendChild(newNode);
			return newNode;
		}

		private void createControls(XmlNode formNode, XmlNode formNode2, Control.ControlCollection controls)
		{
			foreach (Control control in controls)
			{
				if (control is DataGrid)
				{
					createDataGridNode(formNode, (DataGrid)control);
				}
				else if (control is RadGridView)
				{
					createRadGridViewNode(formNode, (RadGridView)control);
				}
				else if (control is HintTextBox)
				{
					saveHintTextBox(formNode2, (HintTextBox)control);
				}
				else if (control is DataGridPager)
				{
					createPagerNode(formNode, (DataGridPager)control);
				}
				else if (control is Panel || control is GroupBox || control is TabControl || control is TabPage)
				{
					//					if(control is GroupBox || control is Panel)
					//						createPositionNode(formNode2, control);
					createControls(formNode, formNode2, control.Controls);
				}
				else if (control is UserControl)
				{
					string tmpPrefix = dgNamePrefix;
					dgNamePrefix += control.Name + "_";
					createControls(formNode, formNode2, control.Controls);
					dgNamePrefix = tmpPrefix;
				}
			}
		}

		private void saveHintTextBox(XmlNode node, HintTextBox hTextBox)
		{
			if (!hTextBox.IsHinted)
				return;

			XmlNode dgNode = createNamedNode(node, "DataBoundTextBox", dgNamePrefix + hTextBox.Name);
			hTextBox.SaveDataXml(dgNode);
		}


		private void setAttribute(XmlNode node, string attrName, object obj)
		{
			Type typeOfObj = obj.GetType();
			PropertyInfo pi = typeOfObj.GetProperty(attrName);
			if (pi == null)
				return;

			XmlAttribute attr = node.Attributes[attrName];
			if (attr != null)
			{
				if (pi.PropertyType == typeof(int))
					pi.SetValue(obj, Convert.ToInt32(attr.Value), null);
			}
		}

		private void readDataGridNode(XmlNode node, DataGrid dataGrid)
		{
			XmlNode dgNode = node.SelectSingleNode("datagrid[@name='" + dgNamePrefix + dataGrid.Name + "']");
			if (dgNode == null)
				return;

			if (dataGrid is ExtendedDataGrid && ((ExtendedDataGrid)dataGrid).ColumnDragEnabled)
			{//воссоздаем порядок колонок в таблице сохраненный в файле настроек				
				foreach (DataGridTableStyle style in dataGrid.TableStyles)
				{
					XmlNode styleNode = dgNode.SelectSingleNode("style[@name='" + style.MappingName + "']");
					if (styleNode == null)
						continue;

					SuspendLayout();
					//создаем массив стилей колонок, которые будут создаваться в порядке сохраненном в файле
					ArrayList columns = new ArrayList();
					if (styleNode.ChildNodes == null)
						continue;
					foreach (XmlNode columnNode in styleNode.ChildNodes)
					{
						XmlAttribute columnNameAttr = columnNode.Attributes["name"];
						if (columnNameAttr == null)
							continue;
						DataGridColumnStyle columnStyle = style.GridColumnStyles[columnNameAttr.Value];
						if (columnStyle == null)
							continue;
						XmlAttribute columnWidthAttr = columnNode.Attributes["Width"];
						if (columnWidthAttr != null)
						{
							try
							{
								columnStyle.Width = int.Parse(columnWidthAttr.Value);
							}
							catch
							{
							}
						}

						columns.Add(columnStyle);
						style.GridColumnStyles.Remove(columnStyle);
					}

					//дозаписываем оставшиеся столбцы					
					foreach (DataGridColumnStyle column in style.GridColumnStyles)
						columns.Add(column);

					style.GridColumnStyles.Clear();
					DataGridColumnStyle[] cols = new DataGridColumnStyle[columns.Count];
					for (int i = 0; i < columns.Count; i++)
						cols[i] = (DataGridColumnStyle)columns[i];

					style.GridColumnStyles.AddRange(cols);
					ResumeLayout();
				}
			}
			else
			{
				foreach (DataGridTableStyle style in dataGrid.TableStyles)
				{
					XmlNode styleNode = dgNode.SelectSingleNode("style[@name='" + style.MappingName + "']");
					if (styleNode == null)
						continue;
					foreach (DataGridColumnStyle column in style.GridColumnStyles)
					{
						if (column.Width <= 0)
							continue;
						XmlNode columnNode = styleNode.SelectSingleNode("column[@name='" + column.MappingName + "']");
						if (columnNode == null)
							continue;
						setAttribute(columnNode, "Width", column);
					}
				}
			}

			//bool updated = false;
			if (IsSavingOrder && dataGrid is DataGridISM)
			{
				XmlNodeList nl = dgNode.SelectNodes("orders/order");
				if (nl != null && nl.Count > 0)
				{
                    var orderInfo = OrderInfo.ReadXmlNode(nl[0]);
				    ((DataGridISM) dataGrid).Order = orderInfo;

                    for (var i = 1; i < nl.Count; i++)
				    {
				        orderInfo.NextOrder = OrderInfo.ReadXmlNode(nl[i]);
				        orderInfo = orderInfo.NextOrder;
				    }
				}
			}

			if (MainForm != null && MainForm.isSaveFilters && IsSavingFilters && dataGrid is DataGridISM)
			{
				XmlNodeList filters = dgNode.SelectNodes("filters/filter");
				if (filters != null)
				{
					foreach (XmlNode filter in filters)
					{
						FilterInfo _filter = FilterInfo.ReadXmlNode(filter);
						if (_filter != null)
							((DataGridISM)dataGrid).StorableFilters[_filter.ColumnName.ToLower()] = _filter;
					}
					//updated = true;
				}

			}

			/*if (updated)
			{
				((DataGridISM)dataGrid).OnReload(new EventArgs());
			}*/

			if (dataGrid is ExtendedDataGrid)
			{

				XmlNodeList rowsNodes = dgNode.SelectNodes("RowColorsTable/row");
				if (rowsNodes == null)
					return;
				ExtendedDataGrid dg = (ExtendedDataGrid)dataGrid;
				int position = 0;
				foreach (XmlNode rowNode in rowsNodes)
				{
					XmlNode dataNode = rowNode.SelectSingleNode("color");
					Color color = Color.White;
					if (dataNode != null)
					{
						try
						{
							color = Color.FromArgb(int.Parse(dataNode.InnerText));
						}
						catch
						{
							color = Color.White;
						}
					}


					dataNode = rowNode.SelectSingleNode("quantity");
					int quantity = 0;
					if (dataNode != null)
					{
						try
						{
							quantity = int.Parse(dataNode.InnerText);
						}
						catch
						{
							quantity = 0;
						}
					}

					DataRow row = dg.RowColorsTable.NewRow();
					row["color"] = color;
					row["quantity"] = quantity;
					position += quantity;
					row["position"] = position;
					dg.RowColorsTable.Rows.Add(row);
				}

				dg.RowColorsTable.AcceptChanges();
			}
		}

		private void readRadGridViewNode(XmlNode node, RadGridView gridView)
		{
			XmlNode dgNode = node.SelectSingleNode("gridview[@name='" + dgNamePrefix + gridView.Name + "']");
			if (dgNode == null)
				return;


			foreach (var column in gridView.Columns)
			{
				if (column.Width <= 0)
					continue;
				var columnNode = dgNode.SelectSingleNode("column[@name='" + column.Name + "']");
				if (columnNode == null)
					continue;
				var columnWidthAttr = columnNode.Attributes["Width"];
				if (columnWidthAttr != null)
				{
					column.Width = int.Parse(columnWidthAttr.Value);
				}
			}


		}

		private void readPositionNode(XmlNode node, Control ctrl)
		{
			XmlNode ctrlNode = node.SelectSingleNode(ctrl.GetType() + "[@name='" + ctrl.Name + "']");
			if (ctrlNode != null)
			{
				ctrl.Location = new Point(int.Parse(ctrlNode.Attributes["left"].Value), int.Parse(ctrlNode.Attributes["top"].Value));
				ctrl.Size = new Size(int.Parse(ctrlNode.Attributes["width"].Value), int.Parse(ctrlNode.Attributes["height"].Value));
			}
		}
		private void readPagerNode(XmlNode node, DataGridPager dataGridPager)
		{
			//XmlDocument xmlDoc = node.OwnerDocument;
			//XmlNode dgpNode = createNamedNode(node, "datagridpager", dgNamePrefix + dataGridPager.Name);
			//writeAttribute(dgpNode, "Batch", dataGridPager.Batch.ToString());	

			XmlNode dgpNode = node.SelectSingleNode("datagridpager[@name='" + dgNamePrefix + dataGridPager.Name + "']");
			if (dgpNode == null)
				return;

			XmlAttribute batchAttr = dgpNode.Attributes["batch"];
			if (batchAttr != null)
			{
				try
				{
					dataGridPager.Batch = int.Parse(batchAttr.Value);
				}
				catch
				{
					dataGridPager.Batch = -1;
				}
			}
		}

		private void readHintTextBox(XmlNode node, HintTextBox hTextBox)
		{
			XmlNode dgNode = node.SelectSingleNode("DataBoundTextBox[@name='" + dgNamePrefix + hTextBox.Name + "']");
			if (dgNode == null)
				return;
			hTextBox.LoadDataXml(dgNode);
		}

		private void loadFromFile()
		{
			XmlDocument xmlDoc = new XmlDocument();
			try
			{
				xmlDoc.Load(FileName);
			}
			catch
			{
				try
				{
					xmlDoc.Load(filename_load);
				}
				catch
				{
					xmlDoc = null;
				}
			}
			XmlDocument xmlDoc2 = new XmlDocument();
			try
			{
				xmlDoc2.Load(HintTextBoxFileName);
			}
			catch
			{
				xmlDoc2 = null;
			}

			if (xmlDoc == null && xmlDoc2 == null)
				return;

			string name = GetFormName();//Получаем имя формы

			XmlNode formNode = xmlDoc == null ? null : xmlDoc.SelectSingleNode("/root/forms/form[@name='" + name + "']");
			XmlNode formNode2 = xmlDoc2 == null ? null : xmlDoc2.SelectSingleNode("/root/forms/form[@name='" + name + "']");
			if (formNode == null && formNode2 == null)
				return;
			//fmMain.isCloseAfterSave
			if (formNode != null)
			{
				if (this is IfmMain)
				{
					XmlNode node = formNode.SelectSingleNode("isCloseWarningShow");
					try
					{
						(this as IfmMain).isCloseWarningShow = bool.Parse(node.InnerText);
					}
					catch { }
					node = formNode.SelectSingleNode("isCloseAfterSave");
					try
					{
						(this as IfmMain).isCloseAfterSave = bool.Parse(node.InnerText);
					}
					catch { }
					node = formNode.SelectSingleNode("isExportBeforePrint");
					try
					{
						(this as IfmMain).isExportBeforePrint = bool.Parse(node.InnerText);
					}
					catch { }
					node = formNode.SelectSingleNode("isSaveFilters");
					try
					{
						(this as IfmMain).isSaveFilters = bool.Parse(node.InnerText);
					}
					catch { }

					//Загрузим шрифт
					//if (IsSavingFonts)
					//{
					//  node = formNode.SelectSingleNode("font");
					//  try
					//  {
					//    float fsize = Convert.ToSingle(node.Attributes["fontSize"].Value);
					//    string familyName = node.Attributes["fontFamily"].Value;
					//    FontStyle fstyle = (FontStyle)Enum.Parse(typeof(FontStyle), node.Attributes["fontStyle"].Value, true);
					//    (this as IfmMain).Font = new Font(familyName, fsize, fstyle);
					//  }
					//  catch(Exception ex)
					//  {
					//    ShowErrorWithoutForm(ex.Message);
					//  }
					//}
				}

				if (FormBorderStyle == FormBorderStyle.Sizable || FormBorderStyle == FormBorderStyle.SizableToolWindow)
				{
					if (WindowState == FormWindowState.Normal)
					{
						setAttribute(formNode, "Width", this);
						setAttribute(formNode, "Height", this);
					}
				}

				if (WindowState == FormWindowState.Normal)
				{
					setAttribute(formNode, "Top", this);
					setAttribute(formNode, "Left", this);
				}

				//Загрузим необходимые дополнительные параметры (если они есть)
				loadAdditionals(formNode);
			}

			readControls(formNode, formNode2, Controls);
		}

		private void readControls(XmlNode formNode, XmlNode formNode2, Control.ControlCollection controls)
		{
			foreach (Control control in controls)
			{
				if (control is DataGrid)
				{
					if (formNode != null)
						readDataGridNode(formNode, (DataGrid)control);
				}
				else if (control is RadGridView)
				{
					if (formNode != null)
						readRadGridViewNode(formNode, (RadGridView)control);
				}
				else if (control is HintTextBox)
				{
					if (formNode2 != null)
						readHintTextBox(formNode2, (HintTextBox)control);
				}
				else if (control is DataGridPager)
				{
					if (formNode != null)
						readPagerNode(formNode, (DataGridPager)control);
				}
				else if (control is Panel || control is GroupBox || control is TabControl || control is TabPage)
				{
					//					if(control is Panel || control is GroupBox) 
					//						readPositionNode(formNode2, control);
					readControls(formNode, formNode2, control.Controls);
				}
				else if (control is UserControl)
				{
					string tmpPrefix = dgNamePrefix;
					dgNamePrefix += control.Name + "_";
					readControls(formNode, formNode2, control.Controls);
					dgNamePrefix = tmpPrefix;
				}
			}
		}

		private System.Globalization.CultureInfo ci = null;
		public virtual System.Globalization.CultureInfo DefaultCultureInfo
		{
			get
			{
				if (ci == null)
				{
					ci = new System.Globalization.CultureInfo("en-US");
					ci.NumberFormat.CurrencyDecimalSeparator = ",";
					ci.NumberFormat.CurrencyGroupSeparator = " ";
					ci.NumberFormat.CurrencyNegativePattern = 1;
					ci.NumberFormat.CurrencySymbol = "";
				}
				return ci;
			}
		}

		public virtual System.Globalization.NumberFormatInfo DefaultNumberFormatInfo
		{
			get { return DefaultCultureInfo.NumberFormat; }
		}

		private bool isLoaded = false;

		protected override void OnClosing(CancelEventArgs e)
		{
			if (BeforeClosing != null) BeforeClosing(this, EventArgs.Empty);
			base.OnClosing(e);

			saveToFile();
			isLoaded = false;
		}

		protected override void OnLoad(EventArgs e)
		{
			if (!DesignMode)
			{
				if (!isLoaded)
				{
					loadFromFile();
					isLoaded = true;
				}
                if (BeforeLoad != null) BeforeLoad(this, EventArgs.Empty);
                base.OnLoad(e);
			}
		}

		public virtual new void Show()
		{
            if (!isLoaded)
            {
                loadFromFile();
                isLoaded = true;
            }
			base.Show();
			//this.Visible = true;
		}

		private void InitializeComponent()
		{
            aquaTheme1 = new Telerik.WinControls.Themes.AquaTheme();
            breezeTheme1 = new Telerik.WinControls.Themes.BreezeTheme();
            desertTheme1 = new Telerik.WinControls.Themes.DesertTheme();
            office2010BlackTheme1 = new Telerik.WinControls.Themes.Office2010BlackTheme();
            windows7Theme1 = new Telerik.WinControls.Themes.Windows7Theme();
            ((ISupportInitialize)(this)).BeginInit();
            SuspendLayout();
            // 
            // BaseGenericForm
            // 
            ClientSize = new Size(1137, 642);
            Name = "BaseGenericForm";
            // 
            // 
            // 
            RootElement.ApplyShapeToControl = true;
            ThemeName = "Desert";
            ((ISupportInitialize)(this)).EndInit();
            ResumeLayout(false);

		}
	}
}
