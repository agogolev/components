﻿using System;
using System.Windows.Forms;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing.Design;

namespace ColumnMenuExtender
{
	/// <summary>
	/// Summary description for ExtendedDataGridTableStyle.
	/// </summary>
	public class ChecksDataGridTableStyle : DataGridTableStyle
	{
		public ChecksDataGridTableStyle()
		{
			//this.ReadOnly = true;
		}

		[Editor(typeof(ChecksDataGridTableStyle.DataGridColumnStylesCollectionEditor), typeof(UITypeEditor))]
		public override GridColumnStylesCollection GridColumnStyles
		{
			get
			{
				return base.GridColumnStyles;
			}
		}

		//метод возращает индексное значение по названию колонки
		public int GetIndex(string ColumnName)
		{
			for(int i = 0; i < this.GridColumnStyles.Count; i++)
			{
				DataGridColumnStyle ds = this.GridColumnStyles[i];
				if(ds.MappingName == ColumnName)
					return i;
			}
			return -1;//типа не нашёл
		}

		private class DataGridColumnStylesCollectionEditor : CollectionEditor
		{
			public DataGridColumnStylesCollectionEditor(Type type) : base(type)
			{
			}

			protected override Type[] CreateNewItemTypes()
			{
				return new Type[2] {typeof(FormattableTextBoxColumn), typeof(ChecksDataGridColumn)};
			}
 
		}

	}
}
