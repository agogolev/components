﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace ColumnMenuExtender
{
	/// <summary>
	/// Summary description for FilterImageForm.
	/// </summary>
	public class FilterImageForm : System.Windows.Forms.Form
	{
		//public bool ToClear = false;
		private System.Windows.Forms.Button btnClear;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnOk;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label label1;
		public ImagedCheckedListBox imagedCheckedListBox1;
		private System.ComponentModel.IContainer components = null;
		public Hashtable htIndicies;

		public FilterImageForm(ImagesAndValues ImAndVal)
		{
			InitializeComponent();

			/*comboboxEx1.ImageList = ImAndVal.ImageList;
			// not needed but... no icon for index -1 else
			comboboxEx1.DropDownStyle = ComboBoxStyle.DropDownList;
			// specify a valid imageIndex
			for (int i=0; i<ImAndVal.Length; i++)
			{
				ImagePair ip = ImAndVal[i];
				comboboxEx1.Items.Add(new ComboBoxExItem(ip.Comment, i));
			}

			if (comboboxEx1.Items.Count>0)
				comboboxEx1.SelectedIndex=0;*/

			htIndicies = new Hashtable();
			imagedCheckedListBox1.ImageList = ImAndVal.ImageList;
			for (int i=0; i<ImAndVal.Length; i++)
			{
				ImagePair ip = ImAndVal[i];
				imagedCheckedListBox1.Items.Add(new ComboBoxExItem(ip.Comment, i));
				htIndicies.Add(ip.Value, imagedCheckedListBox1.Items.Count-1);
			}

			
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.btnClear = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnOk = new System.Windows.Forms.Button();
			this.panel2 = new System.Windows.Forms.Panel();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.imagedCheckedListBox1 = new ImagedCheckedListBox(this.components);
			this.label1 = new System.Windows.Forms.Label();
			this.panel2.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnClear
			// 
			this.btnClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnClear.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnClear.Location = new System.Drawing.Point(88, 4);
			this.btnClear.Name = "btnClear";
			this.btnClear.TabIndex = 11;
			this.btnClear.Text = "Очистить";
			this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Location = new System.Drawing.Point(8, 4);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.TabIndex = 12;
			this.btnCancel.Text = "Закрыть";
			// 
			// btnOk
			// 
			this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnOk.Location = new System.Drawing.Point(168, 4);
			this.btnOk.Name = "btnOk";
			this.btnOk.TabIndex = 10;
			this.btnOk.Text = "Ok";
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.btnCancel);
			this.panel2.Controls.Add(this.btnClear);
			this.panel2.Controls.Add(this.btnOk);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel2.Location = new System.Drawing.Point(0, 125);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(248, 32);
			this.panel2.TabIndex = 14;
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.imagedCheckedListBox1);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBox1.Location = new System.Drawing.Point(0, 0);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(248, 125);
			this.groupBox1.TabIndex = 15;
			this.groupBox1.TabStop = false;
			// 
			// imagedCheckedListBox1
			// 
			this.imagedCheckedListBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.imagedCheckedListBox1.ImageList = null;
			this.imagedCheckedListBox1.Location = new System.Drawing.Point(8, 32);
			this.imagedCheckedListBox1.Name = "imagedCheckedListBox1";
			this.imagedCheckedListBox1.Size = new System.Drawing.Size(232, 79);
			this.imagedCheckedListBox1.TabIndex = 2;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 16);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(96, 16);
			this.label1.TabIndex = 1;
			this.label1.Text = "Статус:";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// FilterImageForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(248, 157);
			this.ControlBox = false;
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.panel2);
			this.MinimumSize = new System.Drawing.Size(256, 104);
			this.Name = "FilterImageForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Фильтр";
			this.panel2.ResumeLayout(false);
			this.groupBox1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void btnClear_Click(object sender, System.EventArgs e)
		{
			for (int i = imagedCheckedListBox1.CheckedIndices.Count-1; i >=0 ; i--)
			{
				imagedCheckedListBox1.SetItemChecked(imagedCheckedListBox1.CheckedIndices[i], false);
			}
		}

	}
}
