﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Reflection;

namespace ColumnMenuExtender
{
	public class ChecksDataGridColumn : DataGridColumnStyle
	{
		public bool AllowNull
		{
			get
			{
				return checkItem.AllowNull;
			}
			set
			{
				checkItem.AllowNull = value;
			}
		}

		private bool isEditing;
		private int imageHeight;
		private int imageWidth;
		private CheckItem checkItem;

 

		public ChecksDataGridColumn()
		{
			this.isEditing = false;

			checkItem = new CheckItem();
			checkItem.Visible = false;

			imageHeight = checkItem.ImageHeight;
			imageWidth = checkItem.ImageWidth;
		}

		protected override void Abort(int rowNum)
		{
			HideCheckItem();
			this.isEditing = false;
			this.Invalidate();
		}

		private void HideCheckItem()
		{
			if (this.checkItem.Focused)
			{
				this.DataGridTableStyle.DataGrid.Focus();
			}
			this.checkItem.Visible = false;
		}

		protected override bool Commit(CurrencyManager dataSource, int rowNum)
		{
			this.HideCheckItem();
			if (this.isEditing)
			{
				this.SetColumnValueAtRow(dataSource, rowNum, checkItem.Value);
				this.isEditing = false;
				this.Invalidate();
			}
			return true;
		}


		protected override void Edit(CurrencyManager source, int rowNum, Rectangle bounds, bool readOnly, string instantText, bool cellIsVisible)
		{
//			DataGrid grid1 = this.DataGridTableStyle.DataGrid;
//			if (!grid1.Focused)
//			{
//				grid1.FocusInternal();
//			}
			if (!readOnly && !this.ReadOnly)
			{
				this.checkItem.Enabled = true;
			}
			else
				this.checkItem.Enabled = false;
			
			this.checkItem.Value = this.GetColumnValueAtRow(source, rowNum);
			if (this.DataGridTableStyle.DataGrid is ChecksDataGrid && ((ChecksDataGrid)this.DataGridTableStyle.DataGrid).IsCellClicked)
				this.checkItem.NextValue();
				
			if (cellIsVisible)
			{
				checkItem.Bounds = bounds;
				checkItem.Visible = true;
			}
			checkItem.Focus();

			if (this.checkItem.Visible)
			{
				this.DataGridTableStyle.DataGrid.Invalidate(bounds);
			}
			isEditing = true;
		}
 
		protected override int GetMinimumHeight()
		{
			return imageHeight + 2;
		}

		protected override int GetPreferredHeight(Graphics g, object value)
		{
			return imageHeight + 2; 
		}

		protected override Size GetPreferredSize(Graphics g, object value)
		{
			return new Size(imageWidth + 2, imageHeight + 2); // поставить размер Image
		}
 
		protected override void Paint(Graphics g, Rectangle bounds, CurrencyManager source, int rowNum)
		{
			this.Paint(g, bounds, source, rowNum, false);
		}

		protected override void Paint(Graphics g, Rectangle bounds, CurrencyManager source, int rowNum, bool alignToRight)
		{
			Brush backBrush = new SolidBrush(this.DataGridTableStyle.BackColor);
			Brush foreBrush = new SolidBrush(this.DataGridTableStyle.ForeColor);
			this.Paint(g, bounds, source, rowNum, backBrush, foreBrush, alignToRight);
		}

		protected override void Paint(Graphics g, Rectangle bounds, CurrencyManager source, int rowNum, Brush backBrush, Brush foreBrush, bool alignToRight)
		{
			g.FillRectangle(backBrush, bounds);
			int imgW_2 = imageWidth / 2;
			Rectangle rect = new Rectangle(bounds.X + bounds.Width / 2 - imgW_2, bounds.Y, imageWidth, imageHeight);
			Object obj = this.GetColumnValueAtRow(source, rowNum);
			g.DrawImage(checkItem.Images[obj], rect);

		}

		protected override void SetDataGridInColumn(DataGrid Value)
		{
			base.SetDataGridInColumn(Value);
			if ((this.checkItem.Parent != Value) && (this.checkItem.Parent != null))
			{
				this.checkItem.Parent.Controls.Remove(this.checkItem);
			}
			if (Value != null)
			{
				Value.Controls.Add(this.checkItem);
			}
		}
 

		protected override void UpdateUI(CurrencyManager Source, int RowNum, string InstantText)
		{
			this.checkItem.Value = this.GetColumnValueAtRow(Source, RowNum);
		}

	}
}
