﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using System.ComponentModel.Design;
using System.Drawing.Design;
using System.Data;
using System.Drawing;
using System.Collections;
//using System.ComponentModel.Design;
//using System.Drawing.Design;
using System.Runtime.InteropServices;

namespace ColumnMenuExtender
{
	public class ExtendedDataGrid : DataGrid
	{
		[DefaultValue(false)]
		public bool AllowCustomRowColorList { get; set; }

		public event GetColorForRow GetColorForRow;
		public event GetColorForRow GetForeColorForRow;
		public Brush FireEventGetColorForRow(int rowNum, CurrencyManager source)
		{
			return GetColorForRow != null ? GetColorForRow(rowNum, source) : null;
		}

		public Brush FireEventGetForeColorForRow(int rowNum, CurrencyManager source)
		{
			return GetForeColorForRow != null ? GetForeColorForRow(rowNum, source) : null;
		}

		[Browsable(false)]
		public DataTable RowColorsTable { get; protected set; }

		[Browsable(true), DefaultValue(false)]
		public bool AutoRowHeight { get; set; }

		public ExtendedDataGrid()
		{			
			//////////////////////////////////////////////////////////////////////////
			//tblRowColors
			RowColorsTable = new DataTable("RowColors");
			RowColorsTable.Columns.Add("color",typeof(Color));	
			RowColorsTable.Columns.Add("quantity",typeof(int));
			RowColorsTable.Columns.Add("position",typeof(int));	
			RowColorsTable.ExtendedProperties["currentRowPosition"] = 0;
			//////////////////////////////////////////////////////////////////////////
			
			// DnD funcs
			/////////////
			ResetMembersToDefault();
			SetStyle( ControlStyles.DoubleBuffer, true );
			
			MouseDown += ExtendedDataGrid_MouseDown;
			MouseUp += ExtendedDataGrid_MouseUp;
			MouseMove += ExtendedDataGrid_MouseMove;
			/////////////			
		}

		public void DisableNewDel()
		{
			CurrencyManager cm;
			if(DataMember == "")
				cm = BindingContext[DataSource] as CurrencyManager;
			else 
				cm = BindingContext[DataSource, DataMember] as CurrencyManager;
			if (cm != null && (cm.List is DataView))
			{			
				var dview1 = (DataView)cm.List;
				dview1.AllowNew = false;
				dview1.AllowDelete = false;
			}
		}

		public void DisableNew()
		{
			CurrencyManager cm;
			if(DataMember == "")
				cm = BindingContext[DataSource] as CurrencyManager;
			else 
				cm = BindingContext[DataSource, DataMember] as CurrencyManager;
			if (cm == null) return;
			var dview1 = (DataView)cm.List;
			dview1.AllowNew = false;
		}

		public void DisableDel()
		{
			CurrencyManager cm;
			if(DataMember == "")
				cm = BindingContext[DataSource] as CurrencyManager;
			else 
				cm = BindingContext[DataSource, DataMember] as CurrencyManager;
			if (cm == null) return;
			var dview1 = (DataView)cm.List;
			dview1.AllowDelete = false;
		}

		/// <summary>
		/// возвращает ссылку на строку DataTable связанную с текущей выделенной строкой
		/// </summary>		
		public DataRow GetSelectedRow()
		{
			if(DataSource == null || DataMember == null)
				return null;
			CurrencyManager cm;
			if(DataMember == "")
				cm = BindingContext[DataSource] as CurrencyManager;
			else 
				cm = BindingContext[DataSource, DataMember] as CurrencyManager;
			
			if(cm != null && cm.Position != -1 && cm.Current is DataRowView)
			{
				var rv = (DataRowView)cm.Current;
				return rv.Row;
			}
			return null;
		}

		public void SetSelectedRow(DataRow row)
		{
			CurrencyManager cm;
			if(DataSource == null || DataMember == null)
				return;
			if( DataMember == "")
				cm = (CurrencyManager) BindingContext[DataSource];
			else 
				cm = (CurrencyManager) BindingContext[DataSource, DataMember];
						
			for(var i = 0; i < cm.List.Count; i++)
			{
				var rv = (DataRowView) cm.List[i];
				if (rv.Row != row) continue;
				cm.Position = i;
				break;
			}
		}

		/// <summary>
		/// возвращает ссылку на строку DataTable связанную с текущей выделенной строкой
		/// </summary>		
		public DataRowView GetSelectedRowView()
		{
			if(DataSource == null || DataMember == null)
				return null;
			CurrencyManager cm;
			if( DataMember == "")
				cm = (CurrencyManager) BindingContext[DataSource];
			else 
				cm = (CurrencyManager) BindingContext[DataSource, DataMember];

			if (cm.Position != -1 && cm.Current is DataRowView)
			{
				var rv = (DataRowView)cm.Current;
				return rv;
			}
			return null;
		}

		/// <summary>
		/// возвращает ссылку на строку DataTable связанную с текущей выделенной строкой
		/// </summary>		
		public object GetSelectedItem()
		{
			if (DataSource == null || DataMember == null)
				return null;
			CurrencyManager cm;
			if (DataMember == "")
				cm = (CurrencyManager)BindingContext[DataSource];
			else
				cm = (CurrencyManager)BindingContext[DataSource, DataMember];

			return cm.Position != -1 ? cm.Current : null;
		}

		/// <summary>
		/// возвращает источник
		/// </summary>		
		public IList GetSourceList()
		{
			if(DataSource == null || DataMember == null)
				return null;
			CurrencyManager cm;
			if( DataMember == "")
				cm = (CurrencyManager) BindingContext[DataSource];
			else 
				cm = (CurrencyManager) BindingContext[DataSource, DataMember];
			return cm.List;
		}

		/// <summary>
		/// возвращает CurrencyManager
		/// </summary>		
		public CurrencyManager GetCurrencyManager()
		{
			if(DataSource == null || DataMember == null)
				return null;
			CurrencyManager cm;
			if( DataMember == "")
				cm = (CurrencyManager) BindingContext[DataSource];
			else 
				cm = (CurrencyManager) BindingContext[DataSource, DataMember];
			return cm;
		}

		/// <summary>
		/// прекращает редактирование текущей строки грида
		/// </summary>
		public void CommitEditRow(int rowNumber)
		{
			if(TableStyles["table"] != null && TableStyles["table"].ReadOnly == false && ReadOnly == false)
			{
				foreach(DataGridColumnStyle column in TableStyles["table"].GridColumnStyles)
				{
					if(column.ReadOnly == false)
						EndEdit(column,rowNumber,false);
				}
			}			
		}

		public void EndEdit()
		{
			if(CurrentRowIndex >= 0)
				CommitEditRow(CurrentRowIndex);

			if(DataSource == null || DataMember == null)
				return;
			CurrencyManager cm;
			if( DataMember == "")
				cm = (CurrencyManager) BindingContext[DataSource];
			else 
				cm = (CurrencyManager) BindingContext[DataSource, DataMember];
			cm.EndCurrentEdit();
		}

		[Editor(typeof(TableStylesCollectionEditor), typeof(UITypeEditor))]
		public new GridTableStylesCollection TableStyles
		{
			get
			{
				return base.TableStyles;
			}
		}

		public class TableStylesCollectionEditor : CollectionEditor
		{
			// Methods
			public TableStylesCollectionEditor(Type type) : base (type)
			{
			}

			protected override Type[] CreateNewItemTypes()
			{
				return new[] { typeof(ExtendedDataGridTableStyle) } ;
			}
		}

		//Для автосайза колонок
		public void AutoSizeCol(int col)
		{ 
			float width = 0; 
			int numRows;

			var dataSet = DataSource as DataSet;
			if(dataSet != null)
				numRows =dataSet.Tables[0].Rows.Count;
			else
			{
				var dataTable = DataSource as DataTable;
				if(dataTable != null)
					numRows =dataTable.Rows.Count;
				else
				{
					var dataView = DataSource as DataView;
					if(dataView != null) 
						numRows = dataView.Count;
					else throw new Exception("Unknown DataSource: "+DataSource);
				}
			}

			var g = Graphics.FromHwnd(Handle); 
			var sf = new StringFormat(StringFormat.GenericTypographic);

			//размер хидера
			if(TableStyles.Count>0)
				width = g.MeasureString(TableStyles[0].GridColumnStyles[col].HeaderText, Font, 600, sf).Width;
 
			for(int i = 0; i < numRows; ++ i)
			{
				//строки
				var size = g.MeasureString(this[i, col].ToString(), Font, 600, sf);
				if(size.Width > width) 
					width = size.Width;
			}

			g.Dispose(); 
			TableStyles[0].GridColumnStyles[col].Width = (int) width + 15;//8 is for leading and trailing padding 
		}

		public void ShowColorForm()
		{
			var form = new fmDataGridISMColorDlg(RowColorsTable);
			if (form.ShowDialog() != DialogResult.OK) return;
			RowColorsTable = form.RowColors;
			Refresh();
		}

		//Drag'n'Drop (DnD) columns
		#region DnD override methods
		protected override void OnPaint(PaintEventArgs e)
		{
			base.OnPaint (e);

			if (m_draggedColumn == null) return;
			var g = e.Graphics;
		
			var x = m_draggedColumn.InitialRegion.X;
			var y = m_draggedColumn.InitialRegion.Y;
			var width = m_draggedColumn.InitialRegion.Width;
			var height = m_draggedColumn.InitialRegion.Height;
			var region = new Rectangle( x, y, width, height );

			var blackBrush = new SolidBrush( Color.FromArgb( 255, 0, 0, 0 ) );
			var darkGreyBrush = new SolidBrush( Color.FromArgb( 150, 50, 50, 50 ) );
			var blackPen = new Pen( blackBrush, 2F );

			g.FillRectangle( darkGreyBrush, m_draggedColumn.InitialRegion );
			g.DrawRectangle( blackPen, region );

			blackBrush.Dispose();
			darkGreyBrush.Dispose();
			blackPen.Dispose();

			// user feedback indicating which column the dragged column is over
			if ( m_mouseOverColumnIndex != -1 ) 
			{
				
				using ( var b = new SolidBrush( Color.FromArgb( 100, 100, 100, 100 ) ) ) 
				{
					g.FillRectangle( b, m_mouseOverColumnRect );				
				}

			}
				
			// draw bitmap image
			if ( ShowColumnWhileDragging || ShowColumnHeaderWhileDragging ) 
			{ 

				var rect = new Rectangle( m_draggedColumn.CurrentRegion.X, 
				                                m_draggedColumn.CurrentRegion.Y, 
				                                m_draggedColumn.ColumnImage.Width, 
				                                m_draggedColumn.ColumnImage.Height );
							
				g.DrawImage(m_draggedColumn.ColumnImage, 
				            rect, 
				            0, 
				            0, 
				            m_draggedColumn.ColumnImage.Width, 
				            m_draggedColumn.ColumnImage.Height, 
				            GraphicsUnit.Pixel ); 
				
			}

			// translucent film
			var filmBorder = new Pen( new SolidBrush( Color.FromArgb( 255, 200, 200, 230 ) ), 2F );
			var filmFill = new SolidBrush( Color.FromArgb( 100, 200, 200, 255 ) );

			g.FillRectangle(  filmFill, m_draggedColumn.CurrentRegion.X, m_draggedColumn.CurrentRegion.Y, m_draggedColumn.CurrentRegion.Width, m_draggedColumn.CurrentRegion.Height );				
			g.DrawRectangle( filmBorder, new Rectangle( m_draggedColumn.CurrentRegion.X, m_draggedColumn.CurrentRegion.Y + Convert.ToInt16( filmBorder.Width ), width, height ) );

			filmBorder.Dispose();
			filmFill.Dispose();
		}
			
		#endregion

		#region DnD Private fields
		bool m_showColumnWhileDragging = true;
		bool m_showColumnHeaderWhileDragging = true;

		DraggedDataGridColumn m_draggedColumn;
		Rectangle m_mouseOverColumnRect;		
		int m_mouseOverColumnIndex;

		bool columnDragEnabled;
		#endregion

		#region DnD Properties

		[DefaultValue(true)]
		public bool ShowColumnWhileDragging 
		{
			get { return m_showColumnWhileDragging; }
			set { m_showColumnWhileDragging = value; }
		}

		[DefaultValue(true)]
		public bool ShowColumnHeaderWhileDragging 
		{
			get { return m_showColumnHeaderWhileDragging; }
			set { m_showColumnHeaderWhileDragging = value; }
		}
		[DefaultValue(false)]
		public bool ColumnDragEnabled
		{
			get { return columnDragEnabled; }
			set { columnDragEnabled = value; }
		}
		#endregion

		#region DnD Event Handlers

		private void ExtendedDataGrid_MouseDown(object sender, MouseEventArgs e) 
		{
			if (e.Button != MouseButtons.Left || !columnDragEnabled) return;
			var hti = HitTest( e.X, e.Y );

		    if (TableStyles.Count == 0) return;
			// make sure that the user is hovering above a column header and not a column border.
			if ( ( hti.Type & HitTestType.ColumnHeader ) != 0 && ( ( hti.Type & HitTestType.ColumnResize ) == 0 ) && m_draggedColumn == null ) 
			{
				Console.WriteLine( hti.Type );

				var xCoordinate = GetLeftmostColumnHeaderXCoordinate( hti.Column );
				var yCoordinate = GetTopmostColumnHeaderYCoordinate( e.X, e.Y );
				var columnWidth = TableStyles[0].GridColumnStyles[hti.Column].Width;
				var columnHeight = GetColumnHeight( yCoordinate );

				// check to see if the column is partially hidden (to the right then to the left)
				if ( ( xCoordinate + columnWidth ) > ( ClientSize.Width - VertScrollBar.Width ) ) 
				{

					var num = columnWidth - ( ClientSize.Width - VertScrollBar.Width - xCoordinate ) + 2;
					MoveHorizScrollBar( num + HorizScrollBar.Value );
					xCoordinate -= num;

				} 
				else if ( TableStyles[0].RowHeadersVisible && xCoordinate < TableStyles[0].RowHeaderWidth ) 
				{
	
					Console.WriteLine( "condition matches" );
					MoveHorizScrollBar( - ( TableStyles[0].RowHeaderWidth - xCoordinate) );
					xCoordinate = TableStyles[0].GridColumnStyles[hti.Column].Width;
									
				} 
				else if ( xCoordinate < 0 ) 
				{
					
					int numberOfHiddenPixels = 0;
					for( int i=0; i < hti.Column; i++ ) 
					{
						numberOfHiddenPixels += TableStyles[0].GridColumnStyles[i].Width;
					}

					MoveHorizScrollBar( numberOfHiddenPixels );
					xCoordinate = 0;

				}

				var startingLocation = new Point( xCoordinate, yCoordinate );
				var columnRegion = new Rectangle( xCoordinate, yCoordinate, columnWidth, columnHeight );
				var cursorLocation = new Point( e.X - xCoordinate, e.Y - yCoordinate );
								
				if ( ShowColumnWhileDragging || ShowColumnHeaderWhileDragging ) 
				{
					var columnSize = ShowColumnWhileDragging ? new Size( columnWidth, columnHeight ) : new Size( columnWidth, GetColumnHeaderHeight( e.X, yCoordinate ) );

					var columnImage = ( Bitmap ) ScreenImage.GetScreenshot( Handle, startingLocation, columnSize );
					m_draggedColumn = new DraggedDataGridColumn( hti.Column, columnRegion, cursorLocation, columnImage );
				
				} 
				else 
				{				
					m_draggedColumn = new DraggedDataGridColumn( hti.Column, columnRegion, cursorLocation );
				}
				
				m_draggedColumn.CurrentRegion = columnRegion;

			}

			// Force the grid to repaint.
			Update();
		}

		private void ExtendedDataGrid_MouseUp(object sender, MouseEventArgs e) 
		{

			HitTestInfo hti = HitTest( e.X, e.Y );
																
			// is column being dropped above itself? if so, we don't want 
			// to do anything
			if ( m_draggedColumn != null && hti.Column != -1 && hti.Column != m_draggedColumn.Index ) 
			{
				DataGridTableStyle dgts = TableStyles[0];
				var columns = new DataGridColumnStyle[dgts.GridColumnStyles.Count];
				
				int index;
				columns[hti.Column] = dgts.GridColumnStyles[m_draggedColumn.Index];
				// NOTE: csi = columnStyleIndex
				for ( int csi = index = 0; csi < dgts.GridColumnStyles.Count; csi++ ) 
				{										
					
					if(csi == m_draggedColumn.Index)
						continue;
					if(columns[index] != null)
					{
						index++;
						columns[index] = dgts.GridColumnStyles[csi];
					}					
					else
						columns[index] = dgts.GridColumnStyles[csi];

					index++;					
				}
				SuspendLayout();
				TableStyles[0].GridColumnStyles.Clear();
				TableStyles[0].GridColumnStyles.AddRange( columns );
				ResumeLayout();

			} 
			else 
			{
				InvalidateColumnArea();
			}

			ResetMembersToDefault();
	
		}
						
		private void ExtendedDataGrid_MouseMove(object sender, MouseEventArgs e) 
		{
			
			var hti = HitTest( e.X, e.Y );

			if ( m_draggedColumn != null && e.X >= 0 ) 
			{

				var x = e.X - m_draggedColumn.CursorLocation.X;

				// detect the column that the cursor is currently hovering above and
				// calculate its region.
				if ( hti.Column >= 0 ) 
				{
				
					if ( hti.Column != m_mouseOverColumnIndex ) 
					{
						var mocX = GetLeftmostColumnHeaderXCoordinate( hti.Column );
						var mocWidth = TableStyles[0].GridColumnStyles[hti.Column].Width;
												
						// indicate that we want to invalidate the old rectangle area
						if ( m_mouseOverColumnRect != Rectangle.Empty ) 
						{
							Invalidate( m_mouseOverColumnRect, false );
						}

						// if the mouse is hovering over the original column, we do not want to
						// paint anything, so we negate the index.
						if ( hti.Column == m_draggedColumn.Index ) 
						{
							m_mouseOverColumnIndex = -1;
						} 
						else 
						{
							m_mouseOverColumnIndex = hti.Column;
						}

						m_mouseOverColumnRect = new Rectangle( mocX, m_draggedColumn.InitialRegion.Y, mocWidth, m_draggedColumn.InitialRegion.Height );

						// invalidate this area so it gets painted when OnPaint is called.
						Invalidate( m_mouseOverColumnRect, false );

					}
				
					var oldX = m_draggedColumn.CurrentRegion.X;

					// column is being dragged to the right
					var oldPoint = oldX < x ? new Point( oldX - 5, m_draggedColumn.InitialRegion.Y ) : new Point( x - 5, m_draggedColumn.InitialRegion.Y ); 

					var sizeOfRectangleToInvalidate = new Size( Math.Abs( x - oldX ) + m_draggedColumn.InitialRegion.Width + ( oldPoint.X * 2 ), m_draggedColumn.InitialRegion.Height );
					Invalidate( new Rectangle( oldPoint, sizeOfRectangleToInvalidate ) );
										
					m_draggedColumn.CurrentRegion = new Rectangle( x, m_draggedColumn.InitialRegion.Y, m_draggedColumn.InitialRegion.Width, m_draggedColumn.InitialRegion.Height );
				
				} 
				else 
				{
						
					Invalidate();
					ResetMembersToDefault();
					Update();
									
				}
				
			} 
			else 
			{
		
				Invalidate( ClientRectangle );
				ResetMembersToDefault();
				Update();

			}

		}

		#endregion

		#region DnD Helper Methods

		/// <summary>
		/// When a dragged column is dropped on top of its original location, 
		/// whether it’s because the user has decided that they no longer want 
		/// to drag it, or they’ve just happened to release the column in this 
		/// location, we need to invalidate the area where the current drawings 
		/// reside.
		/// </summary>
		private void InvalidateColumnArea() 
		{
		
			if ( m_draggedColumn != null ) 
			{

				var startX = ( ( m_draggedColumn.InitialRegion.X < m_draggedColumn.CurrentRegion.X ) ? m_draggedColumn.InitialRegion.X : m_draggedColumn.CurrentRegion.X ) - 5;
				var width = m_draggedColumn.InitialRegion.Width + m_draggedColumn.CurrentRegion.Width + 10;
				var rectangleToInvalidate = new Rectangle( startX, m_draggedColumn.InitialRegion.Y, width, m_draggedColumn.InitialRegion.Height );
				
				Invalidate( rectangleToInvalidate );
				Update();
			}
			
		}

		/// <summary>
		/// Resets all of the member fields to their default values.
		/// </summary>
		private void ResetMembersToDefault() 
		{

			if ( m_draggedColumn != null ) 
			{
				m_draggedColumn.Dispose();
			}
			
			m_draggedColumn = null;
			m_mouseOverColumnRect = Rectangle.Empty;		
			m_mouseOverColumnIndex = -1;
		
		}

		/// <summary>
		/// Returns the height of the column. The height is defined as the area 
		/// between the bottom portion of the caption area and either the 
		/// bottom of the client rectangle, or the top of the horizontal scroll 
		/// bar if it’s visible.
		/// </summary>
		private int GetColumnHeight( int topmostYCoordinate ) 
		{
					
			var height = ClientSize.Height;
			
			if ( HorizScrollBar.Visible ) 
			{
				height -= HorizScrollBar.Height; 
			}
			
			return height - topmostYCoordinate;
			
		}
		
		/// <summary>
		/// Returns the height of the column header. In order to make this 
		/// calculation, you  need to pass in the topmost y-coordinate of the 
		/// header. This method will then invoke the 
		/// GetBottommostColumnHeaderYCoordinate, which is a recursive method 
		/// that is invoked repeatedly until the DataGrid hit test determines 
		/// that the current coordinates no longer lie within the boundaries 
		/// of a ColumnHeader.
		/// </summary>
		private int GetColumnHeaderHeight( int x, int y ) 
		{
			return GetBottommostColumnHeaderYCoordinate( x, y ) - y;			
		}

		/// <summary>
		/// Calculates the leftmost x coordinate for the column corresponding 
		/// to the parameterized column index. By accessing the GridColumnStyle 
		/// style – which is discussed in the article -- we’re able to get the 
		/// current column widths (this changes when you resize columns) for 
		/// the columns that precede the current column. 
		/// </summary>
		private int GetBottommostColumnHeaderYCoordinate( int x, int currentY ) 
		{

			var hti = HitTest( x, currentY );
			var yCoordinate = currentY;
			
			if ( hti.Type == HitTestType.ColumnHeader  ) 
			{
				yCoordinate = GetBottommostColumnHeaderYCoordinate( x, ++currentY );
			} 

			return yCoordinate;

		}

		/// <summary>
		/// Calculates the leftmost x coordinate for the column corresponding 
		/// to the parameterized column index. By accessing the 
		/// GridColumnStyle style, which is discussed in detail in the article,
		/// we’re able to get the current column widths (this changes when you 
		/// resize columns) for the columns that precede the current column. 		
		/// </summary>
		private int GetLeftmostColumnHeaderXCoordinate( int columnIndex )
		{
		    if (TableStyles.Count == 0) return 0;
			var xCoordinate = ( RowHeadersVisible ) ? RowHeaderWidth : 0;

			for ( var i = 0; i < columnIndex; i++ ) 
			{
				var dgcs = TableStyles[0].GridColumnStyles[i];
				xCoordinate += dgcs.Width;

			}			

			return xCoordinate - HorizScrollBar.Value + 2; // 2 represents the 1px solid this lines on both sides of the cell
		
		}

		/// <summary>
		/// This is another recursive method that returns the Y-coordinate of 
		/// the topmost portion of the column header. First, a check is 
		/// performed to see if the DataGrid caption is visible. If not, the 
		/// Y-coordinate is set to zero and the method returns. Otherwise, a 
		/// recursion is performed until the DataGrid hit test determines that 
		/// the current Y-coordinate value does not fall within the boundaries 
		/// of a ColumnHeader. 
		/// </summary>
		private int GetTopmostColumnHeaderYCoordinate( int currentX, int currentY ) 
		{
		
			HitTestInfo hti = HitTest( currentX, currentY );
			var yCoordinate = currentY;

			if ( !ColumnHeadersVisible ) 
			{
				yCoordinate = 0;
			} 
			else if ( hti.Type == HitTestType.ColumnHeader ) 
			{
				yCoordinate = GetTopmostColumnHeaderYCoordinate( currentX, --currentY );
			} 
			else 
			{
				yCoordinate++;
			}
				
			return yCoordinate;
		
		}

		/// <summary>
		/// Positions the horizontal scroll bar and invalidates the 
		/// </summary>
		public void MoveHorizScrollBar( int amount ) 
		{

			GridHScrolled( this, new ScrollEventArgs( ScrollEventType.ThumbPosition, amount ) );
			Update();

		}
	
		#endregion
			
	}

	#region DnD Classes
	//класс позволяющий сделать копию изображения экрана
	public sealed class ScreenImage 
	{

		#region Unmanaged declarations

		[DllImport("gdi32.dll")]
		private static extern bool BitBlt( IntPtr handlerToDestinationDeviceContext, int x, int y, int nWidth, int nHeight, IntPtr handlerToSourceDeviceContext, int xSrc, int ySrc, int opCode);
		
		[DllImport("user32.dll")]
		private static extern IntPtr GetWindowDC( IntPtr windowHandle );
		
		[DllImport("user32.dll")]
		private static extern int ReleaseDC( IntPtr windowHandle, IntPtr dc );

		private const int SRCCOPY = 0x00CC0020; // dest = source 

		#endregion

		/// <summary>
		/// делает копию экрана
		/// </summary>		
		public static Image GetScreenshot( IntPtr windowHandle, Point location, Size size ) 
		{
				
			Image myImage = new Bitmap( size.Width, size.Height );

			using ( var g = Graphics.FromImage( myImage ) ) 
			{
		
				var destDeviceContext = g.GetHdc();
				var srcDeviceContext = GetWindowDC( windowHandle );
						
				BitBlt( destDeviceContext, 0, 0, size.Width, size.Height, srcDeviceContext, location.X, location.Y, SRCCOPY );
		
				ReleaseDC( windowHandle, srcDeviceContext );
				g.ReleaseHdc( destDeviceContext );

			} // dispose the Graphics object

			return myImage;

		}
		
	} // ScreenImage
	
	internal class DraggedDataGridColumn : IDisposable 
	{

		#region Private data fields

		// private data fields
		private Rectangle m_initialRegion;
		private Rectangle m_currentRegion;

		private int m_index;
		private readonly Image m_columnImage;
		private Point m_cursorLocation;
		
		private bool disposed;
		
		#endregion

		#region Properties
		
		/// <summary>
		/// An integer representing the original column index.
		/// </summary>
		public int Index 
		{
			get 
			{ 
				CheckState();
				return m_index; 
			}
		}

		/// <summary>
		/// A Rectangle structure that identifies the region of the column at
		/// the time the drag and drop operation was initiated.
		/// </summary>	
		public Rectangle InitialRegion 
		{
			
			get 
			{ 
				
				CheckState();
				return m_initialRegion; 
			
			}
		
		}

		/// <summary>
		/// A Rectangle structure that identifies the current region of the 
		/// column that is being dragged. This is the only member that can be 
		/// modified after an instance has been created.
		/// </summary>
		public Rectangle CurrentRegion 
		{

			get 
			{ 

				CheckState();
				return m_currentRegion; 
				
			}
			
			set 
			{ 
				
				CheckState();
				m_currentRegion = value; 
			
			}

		}

		/// <summary>
		/// A Bitmap object containing a bitmap representation of the column at 
		/// the time that the drag and drop operation was initiated.
		/// </summary>
		public Image ColumnImage 
		{
			
			get 
			{ 
			
				CheckState();
				return m_columnImage; 
				
			}
		
		}

		/// <summary>
		/// A Point structure representing the cursor location relative to the 
		/// origin of m_initialRegion.
		/// </summary>
		public Point CursorLocation 
		{
			
			get 
			{ 
				
				CheckState();
				return m_cursorLocation; 
				
			}
		
		}

		#endregion

		#region Constructors
		
		public DraggedDataGridColumn( int index, Rectangle initialRegion, Point cursorLocation, Image columnImage ) 
		{
			
			m_index = index;
			m_initialRegion = initialRegion;
			m_currentRegion = initialRegion;
			m_cursorLocation = cursorLocation;
			m_columnImage = columnImage;
				
		}

		public DraggedDataGridColumn( int index, Rectangle initialRegion, Point cursorLocation ) : this( index, initialRegion, cursorLocation, null ) {}

		#endregion

		public void Dispose() 
		{
			
			if ( !disposed ) 
			{
			
				m_initialRegion = Rectangle.Empty;
				m_currentRegion = Rectangle.Empty;

				m_index = -1;
				m_cursorLocation = Point.Empty;

				if ( m_columnImage != null ) 
				{
					m_columnImage.Dispose();
				}

				disposed = true;

			} 
					
			// Remove this object from the finalization queue so the 
			// finalizer doesn't invoke this method again.
			GC.SuppressFinalize( this );

		}

		// NOTE: We do not implement the destructor because we are not 
		// explicitly dealing with unmanaged resources.

		// ~DraggedDataGridColumn() { }
		
		/// <summary>
		/// Thow an ObjectDisposedException if this object has already been
		/// disposed.
		/// </summary>
		private void CheckState() 
		{
			
			if ( disposed ) 
			{
				throw new ObjectDisposedException( "DraggedDataGridColumn object has already been disposed." );
			}
		
		}
	
	} // DraggedDataGridColumn
	#endregion
}
