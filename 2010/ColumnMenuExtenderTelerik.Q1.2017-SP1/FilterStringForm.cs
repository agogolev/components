﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace ColumnMenuExtender
{
	/// <summary>
	/// Summary description for FilterNumbers.
	/// </summary>
	public class FilterStringForm : System.Windows.Forms.Form
	{
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.Button btnOk;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Label label1;
		public  HintTextBox textBoxString;
		public  System.Windows.Forms.RadioButton radioButtonEnd;
		public  System.Windows.Forms.RadioButton radioButtonContain;
		public  System.Windows.Forms.RadioButton radioButtonStart;
		public  System.Windows.Forms.RadioButton radioButtonNotEmpty;
		public  System.Windows.Forms.RadioButton radioButtonEmpty;
		private System.Windows.Forms.Button btnClear;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public FilterStringForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.textBoxString = new HintTextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.radioButtonEmpty = new System.Windows.Forms.RadioButton();
			this.radioButtonNotEmpty = new System.Windows.Forms.RadioButton();
			this.radioButtonEnd = new System.Windows.Forms.RadioButton();
			this.radioButtonContain = new System.Windows.Forms.RadioButton();
			this.radioButtonStart = new System.Windows.Forms.RadioButton();
			this.btnOk = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnClear = new System.Windows.Forms.Button();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.textBoxString);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Controls.Add(this.groupBox2);
			this.groupBox1.Location = new System.Drawing.Point(8, 8);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(504, 224);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Строка";
			// 
			// textBoxString
			// 
			this.textBoxString.IsHinted = true;
			this.textBoxString.Location = new System.Drawing.Point(24, 64);
			this.textBoxString.MinLettersForHint = 2;
			this.textBoxString.Name = "textBoxString";
			this.textBoxString.Size = new System.Drawing.Size(248, 20);
			this.textBoxString.TabIndex = 0;
			this.textBoxString.Text = "";
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(24, 40);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(100, 16);
			this.label1.TabIndex = 1;
			this.label1.Text = "Искомая строка";
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.radioButtonEmpty);
			this.groupBox2.Controls.Add(this.radioButtonNotEmpty);
			this.groupBox2.Controls.Add(this.radioButtonEnd);
			this.groupBox2.Controls.Add(this.radioButtonContain);
			this.groupBox2.Controls.Add(this.radioButtonStart);
			this.groupBox2.Location = new System.Drawing.Point(304, 16);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(184, 168);
			this.groupBox2.TabIndex = 0;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Условия поиска";
			// 
			// radioButtonEmpty
			// 
			this.radioButtonEmpty.Location = new System.Drawing.Point(16, 112);
			this.radioButtonEmpty.Name = "radioButtonEmpty";
			this.radioButtonEmpty.TabIndex = 5;
			this.radioButtonEmpty.Text = "Пустая";
			// 
			// radioButtonNotEmpty
			// 
			this.radioButtonNotEmpty.Location = new System.Drawing.Point(16, 88);
			this.radioButtonNotEmpty.Name = "radioButtonNotEmpty";
			this.radioButtonNotEmpty.TabIndex = 4;
			this.radioButtonNotEmpty.Text = "Не пустая";
			// 
			// radioButtonEnd
			// 
			this.radioButtonEnd.Location = new System.Drawing.Point(16, 64);
			this.radioButtonEnd.Name = "radioButtonEnd";
			this.radioButtonEnd.TabIndex = 3;
			this.radioButtonEnd.Text = "Заканчивается";
			// 
			// radioButtonContain
			// 
			this.radioButtonContain.Checked = true;
			this.radioButtonContain.Location = new System.Drawing.Point(16, 16);
			this.radioButtonContain.Name = "radioButtonContain";
			this.radioButtonContain.TabIndex = 2;
			this.radioButtonContain.TabStop = true;
			this.radioButtonContain.Text = "Содержится";
			// 
			// radioButtonStart
			// 
			this.radioButtonStart.Location = new System.Drawing.Point(16, 40);
			this.radioButtonStart.Name = "radioButtonStart";
			this.radioButtonStart.TabIndex = 1;
			this.radioButtonStart.Text = "Начинается";
			// 
			// btnOk
			// 
			this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnOk.Location = new System.Drawing.Point(440, 240);
			this.btnOk.Name = "btnOk";
			this.btnOk.TabIndex = 6;
			this.btnOk.Text = "Ok";
			// 
			// btnCancel
			// 
			this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Location = new System.Drawing.Point(8, 240);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.TabIndex = 8;
			this.btnCancel.Text = "Закрыть";
			// 
			// btnClear
			// 
			this.btnClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnClear.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnClear.Location = new System.Drawing.Point(352, 240);
			this.btnClear.Name = "btnClear";
			this.btnClear.TabIndex = 7;
			this.btnClear.Text = "Очистить";
			this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
			// 
			// FilterStringForm
			// 
			this.AcceptButton = this.btnOk;
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.CancelButton = this.btnCancel;
			this.ClientSize = new System.Drawing.Size(522, 271);
			this.ControlBox = false;
			this.Controls.Add(this.btnClear);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnOk);
			this.Controls.Add(this.groupBox1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Name = "FilterStringForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Фильтр";
			this.groupBox1.ResumeLayout(false);
			this.groupBox2.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void btnClear_Click(object sender, System.EventArgs e)
		{
			textBoxString.Text = "";
			radioButtonStart.Checked = true;
		}

		
	}
}
