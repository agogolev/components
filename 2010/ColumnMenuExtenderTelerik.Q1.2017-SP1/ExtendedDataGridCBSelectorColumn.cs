﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.ComponentModel;
using System.Drawing.Design;
//using System.ComponentModel;

namespace ColumnMenuExtender
{
	public class ExtendedDataGridCBSelectorColumn : DataGridColumnStyle, IExtendedQuery
	{
		// Methods
		public ExtendedDataGridCBSelectorColumn()
		{
			this.xMargin = 0;
			this.yMargin = -1;
			this.OldVal = new string(string.Empty.ToCharArray());
			this.InEdit = false;
			this.comboBox = new ComboBox();
			this.comboBox.DropDownStyle = ComboBoxStyle.DropDownList;
			this.comboBox.Visible = false;
			this.comboBox.Items.Add(" ");
			this.comboBox.Items.Add("Выбрать...");
			this.comboBox.Items.Add("Удалить");
			this.comboBox.SelectedIndexChanged += new EventHandler(comboBox_SelectedIndexChanged);
		}

		protected override void Abort(int RowNum)
		{
			this.HideComboBox();
			this.EndEdit();
		}

		protected override bool Commit(CurrencyManager DataSource, int RowNum)
		{
			this.HideComboBox();
			if (this.InEdit)
			{
				try
				{

				}
				catch
				{
					return false;
				}
				this.EndEdit();
			}
			return true;
		}

		protected override void ConcedeFocus()
		{
			this.comboBox.Visible = false;
		}

		protected override void Edit(CurrencyManager Source, int Rownum, Rectangle Bounds, bool ReadOnly, string InstantText, bool CellIsVisible)
		{
			this.source = Source;
			this.rowNum = Rownum;

			if (this.ReadOnly)
			{
				this.comboBox.Enabled = false;
			}
			else
			{
				this.comboBox.Enabled = true;
			}

			Rectangle rectangle1 = Bounds;

			if (CellIsVisible)
			{
				Bounds.Offset(this.xMargin, this.yMargin);
				Bounds.Width -= (this.xMargin * 2);
				Bounds.Height -= this.yMargin;
				this.comboBox.Bounds = Bounds;
				this.comboBox.Visible = true;
				this.comboBox.SelectedIndex = 0;
			}
			else
			{
				this.comboBox.Bounds = rectangle1;
				this.comboBox.Visible = false;
			}
			this.comboBox.Items[0] = this.GetText(this.GetColumnValueAtRow(Source, Rownum));

			this.comboBox.RightToLeft = this.DataGridTableStyle.DataGrid.RightToLeft;
			this.comboBox.Focus();

			if (this.comboBox.Visible)
			{
				this.DataGridTableStyle.DataGrid.Invalidate(rectangle1);
			}
			this.InEdit = true;
		}

		public void EndEdit()
		{
			this.InEdit = false;
			this.Invalidate();
		}

		protected override int GetMinimumHeight()
		{
			return ((this.comboBox.PreferredHeight + this.yMargin) + 1);
		}

		protected override int GetPreferredHeight(Graphics g, object Value)
		{
			int num1 = 0;
			int num2 = 0;
			string text1 = this.GetText(Value);
			do
			{
				num1 = text1.IndexOf("r\n", (int) (num1 + 1));
				num2++;
			}
			while (num1 != -1);
			return ((base.FontHeight * num2) + this.yMargin);
		}

		protected override Size GetPreferredSize(Graphics g, object Value)
		{
			Size size1 = Size.Ceiling(g.MeasureString(this.GetText(Value), this.DataGridTableStyle.DataGrid.Font));
			size1.Width += ((this.xMargin * 2) + this.DataGridTableGridLineWidth);
			size1.Height += this.yMargin;
			return size1;
		}

		private string GetText(object Value)
		{
			if (Value == DBNull.Value)
			{
				return this.NullText;
			}
			if (Value != null)
			{
				return Value.ToString();
			}
			return string.Empty;
		}

		private void HideComboBox()
		{
			if (this.comboBox.Focused)
			{
				this.DataGridTableStyle.DataGrid.Focus();
			}
			this.comboBox.Visible = false;
		}

		protected override void Paint(Graphics g, Rectangle Bounds, CurrencyManager Source, int RowNum)
		{
			this.Paint(g, Bounds, Source, RowNum, false);
		}

		protected override void Paint(Graphics g, Rectangle Bounds, CurrencyManager Source, int RowNum, bool AlignToRight)
		{
			Brush brush1 = new SolidBrush(this.DataGridTableStyle.BackColor);
			Brush brush2 = new SolidBrush(this.DataGridTableStyle.ForeColor);
			string text1 = this.GetText(this.GetColumnValueAtRow(Source, RowNum));
			g.FillRectangle(brush1, Bounds);
			g.DrawString(text1, this.DataGridTableStyle.DataGrid.Font, brush2, (RectangleF) new Rectangle(Bounds.X, Bounds.Y + 2, Bounds.Width, Bounds.Height));
		}

		protected override void Paint(Graphics g, Rectangle Bounds, CurrencyManager Source, int RowNum, Brush BackBrush, Brush ForeBrush, bool AlignToRight)
		{
			string text1 = this.GetText(this.GetColumnValueAtRow(Source, RowNum));
			g.FillRectangle(BackBrush, Bounds);
			g.DrawString(text1, this.DataGridTableStyle.DataGrid.Font, ForeBrush, (RectangleF) new Rectangle(Bounds.X, Bounds.Y + 2, Bounds.Width, Bounds.Height));
		}

		protected override void SetDataGridInColumn(DataGrid Value)
		{
			base.SetDataGridInColumn(Value);
			if ((this.comboBox.Parent != Value) && (this.comboBox.Parent != null))
			{
				this.comboBox.Parent.Controls.Remove(this.comboBox);
			}
			if (Value != null)
			{
				Value.Controls.Add(this.comboBox);
			}
		}
 

		// Properties
		private int DataGridTableGridLineWidth
		{
			get
			{
				if (this.DataGridTableStyle.GridLineStyle == DataGridLineStyle.Solid)
				{
					return 1;
				}
				return 0;
			}
		}


		// Fields
		private ComboBox comboBox;
		private bool InEdit;
		private string OldVal;
		private int xMargin;
		private int yMargin;

		private CurrencyManager source;
		private int rowNum;

		public event CellEventHandler ChooseClick;
		public event CellEventHandler DelClick;

		private void comboBox_SelectedIndexChanged(object sender, EventArgs e)
		{
			if ((string)comboBox.SelectedItem == "Выбрать...")
			{
				if (ChooseClick != null)
				{
					ChooseClick(this, new DataGridCellEventArgs(this.GetColumnValueAtRow(this.source, this.rowNum), source.List[this.rowNum]));
					ConcedeFocus();
				}
			}
			else if ((string)comboBox.SelectedItem == "Удалить")
			{
				if (DelClick != null)
				{
					DelClick(this, new DataGridCellEventArgs(this.GetColumnValueAtRow(this.source, this.rowNum), source.List[this.rowNum]));
					ConcedeFocus();
				}
			}
		}
		
		#region Public Properties

		private string fieldName;
		private string filterFieldName;

		[DefaultValue(typeof(string), ""), Category("Misc")]
		public string FieldName
		{
			set 
			{
				fieldName = value;
			}
			get
			{
				return fieldName;
			}
		}

		[DefaultValue(typeof(string), ""), Category("Misc")]
		public string FilterFieldName
		{
			set 
			{
				filterFieldName = value;
			}
			get
			{
				return filterFieldName;
			}
		}

		#endregion
	}
}
