﻿using System;
using System.Windows.Forms;
using System.Drawing;

namespace ColumnMenuExtender
{
	/// <summary>
	/// Summary description for ImagesAndValues.
	/// </summary>
	public class ImagesAndValues
	{
		/// <summary>
		/// Сделал, чтобы была возможность привязывать к колонке и соответствующему фильтру разные ImageList,
		/// может быть пригодиться в будущем, если не надо - просто пихайте сюда тот же самый ImageList.
		/// Values.Length должно быть равно или меньше остальных размерностей, передаваемых элементов, 
		/// (количество элементов в фильтре определяется именно размерностью Values)
		/// </summary>
		/// <param name="aImageList">Набор картинок</param>
		/// <param name="Comments">Набор комментариев к картинкам</param>
		/// <param name="Values">Набор значений</param>
		public ImagesAndValues(ImageList aImageList, string [] Comments ,object [] Values)
		{
			//Values.Length должно быть равно или меньше остальных
			if (Values.Length > aImageList.Images.Count || Values.Length > Comments.Length)
				throw new Exception("Не хватает значений переданных в фильтр");
			this.imageList = aImageList;
			this.values = Values;
			this.comments = Comments;
		}

		private ImageList imageList;
		public ImageList ImageList
		{
			get{return imageList;}
		}

		private object [] values;
		public object [] Values
		{
			get{return values;}
		}
		private string [] comments;
		public object [] Comments
		{
			get{return comments;}
		}

		public ImagePair this [int index]
		{
			get 
			{
				if (index < 0 || index >= values.Length)
					return null;
				else return new ImagePair(imageList.Images[index], comments[index] ,values[index]);
			}
		}

		public int Length
		{
			get{return Values.Length;}
		}
	}

	public class ImagePair
	{
		public ImagePair(Image aImage, string Comment, object Value)
		{
			this.aImage = aImage;
			this.Value = Value;
			this.Comment = Comment;
		}

		public Image aImage;
		public object Value;
		public string Comment;
	}
}
