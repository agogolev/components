﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Diagnostics;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Imaging;

namespace ColumnMenuExtender
{
	/// <summary>
	/// Summary description for ImagedCheckedListBox.
	/// </summary>
	public class ImagedCheckedListBox : System.Windows.Forms.CheckedListBox
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		private ImageAttributes imgAttributes;

		public ImagedCheckedListBox(System.ComponentModel.IContainer container)
		{
			///
			/// Required for Windows.Forms Class Composition Designer support
			///
			container.Add(this);
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			init();
		}

		public ImagedCheckedListBox()
		{
			///
			/// Required for Windows.Forms Class Composition Designer support
			///
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			init();
		}

		private void init()
		{
			DrawMode = DrawMode.OwnerDrawFixed;

			float[][] ptsArray = {	new float[] {1, 0, 0, 0, 0},
									 new float[] {0, 1, 0, 0, 0},
									 new float[] {0, 0, 1, 0, 0},
									 new float[] {0, 0, 0, 0.5f, 0}, 
									 new float[] {0, 0, 0, 0, 1}}; 
			ColorMatrix clrMatrix = new ColorMatrix(ptsArray);
			imgAttributes = new ImageAttributes();
			imgAttributes.SetColorMatrix(clrMatrix,
				ColorMatrixFlag.Default,
				ColorAdjustType.Bitmap);
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}


		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
		}
		#endregion

		private ImageList imageList;
		public ImageList ImageList
		{
			get {return imageList;}
			set {imageList = value;}
		}


		protected override void OnDrawItem(DrawItemEventArgs ea)
		{
			ea.DrawBackground();
			ea.DrawFocusRectangle();
			//ea.State = DrawItemState.

			base.OnDrawItem(ea);
//			CheckBox cb = new CheckBox();
//			if (ea.State == DrawItemState.Checked)
//				cb.CheckState = CheckState.Checked;
//			else
//				cb.CheckState = CheckState.Unchecked;
//
			int cbWidth = 15;
//
//			cb.Draw

			ComboBoxExItem item;
			if (imageList != null)
			{
			
				Size imageSize = imageList.ImageSize;
				Rectangle bounds = ea.Bounds;

				try
				{
					item = (ComboBoxExItem)Items[ea.Index];

					if (item.ImageIndex != -1)
					{
						//imageList.Draw(ea.Graphics, bounds.Left+cbWidth, bounds.Top, item.ImageIndex);
						
						Size sz = ea.Graphics.MeasureString(item.Text, ea.Font).ToSize();
						ea.Graphics.FillRectangle(new SolidBrush(ea.BackColor), bounds.Left+cbWidth, bounds.Top, sz.Width, sz.Height+1);

						Image img = imageList.Images[item.ImageIndex];
						Rectangle rct = new Rectangle(bounds.Left+cbWidth, bounds.Top, img.Width, img.Height);
						if ((ea.State & DrawItemState.Selected) == DrawItemState.Selected)
							ea.Graphics.DrawImage(img, rct, 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, imgAttributes);
						else
							ea.Graphics.DrawImage(img, rct);
						
						ea.Graphics.DrawString(item.Text, ea.Font, new SolidBrush(ea.ForeColor), bounds.Left+imageSize.Width+cbWidth, bounds.Top);
					}
					else
					{
						ea.Graphics.DrawString(item.Text, ea.Font, new
							SolidBrush(ea.ForeColor), bounds.Left+cbWidth, bounds.Top);
					}
				}
				catch
				{
					if (ea.Index != -1)
					{
						ea.Graphics.DrawString(Items[ea.Index].ToString(), ea.Font, new
							SolidBrush(ea.ForeColor), bounds.Left+cbWidth, bounds.Top);
					}
					else
					{
						ea.Graphics.DrawString(Text, ea.Font, new
							SolidBrush(ea.ForeColor), bounds.Left+cbWidth, bounds.Top);
					}
				}
			}
			//base.OnDrawItem(ea);
		}
	}
}
