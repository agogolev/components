﻿using System;

namespace ColumnMenuExtender
{
	/// <summary>
	/// Summary description for Interfaces.
	/// </summary>
	public interface IExtendedQuery
	{
		/// <summary>
		/// То имя, которое непосредственно будет обработано в базе
		/// </summary>
		string FieldName{get; set;}
		/// <summary>
		/// То имя, по которому будет производится фильтрация в базе
		/// </summary>
		string FilterFieldName{get; set;}
	}

}
