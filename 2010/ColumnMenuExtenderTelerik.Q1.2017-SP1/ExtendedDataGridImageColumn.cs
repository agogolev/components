﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.ComponentModel;
using System.Drawing.Design;
using System.Drawing.Imaging;
//using System.ComponentModel;

namespace ColumnMenuExtender
{
	public class ExtendedDataGridImageColumn : DataGridTextBoxColumn, IExtendedQuery
	{
		// Fields
		private int _defaultImageWidth;
		private bool _drawText;
		private int _imageHeight;
		private int _minimumHeight;
		private int _space;
		//private ImageRequestedDelegate imageRequested;
		private ImageList imageList;
		private int imageIndex;
		private Image curImage;
		private ImageAttributes imgAttributes;

		public ExtendedDataGridImageColumn()
		{
			this._space = 0;
			this._minimumHeight = 0;
			this._defaultImageWidth = 0;
			this._imageHeight = 0;
			this._drawText = false;
			this.imageList = null;
			this.imageIndex = -1;
			this.curImage = null;

			float[][] ptsArray = {	new float[] {1, 0, 0, 0, 0},
									new float[] {0, 1, 0, 0, 0},
									new float[] {0, 0, 1, 0, 0},
									new float[] {0, 0, 0, 0.5f, 0}, 
									new float[] {0, 0, 0, 0, 1}}; 
			ColorMatrix clrMatrix = new ColorMatrix(ptsArray);
			imgAttributes = new ImageAttributes();
			imgAttributes.SetColorMatrix(clrMatrix,
				ColorMatrixFlag.Default,
				ColorAdjustType.Bitmap);

		}

		protected override void Edit(CurrencyManager source, int rowNum, Rectangle bounds, bool readOnly, string instantText, bool cellIsVisible)
		{
		}

		protected override int GetMinimumHeight()
		{
			return this.MinimumHeight;
		}

		protected override void Paint(Graphics g, Rectangle bounds, CurrencyManager source, int rowNum, Brush backBrush, Brush foreBrush, bool alignToRight)
		{
			//Image image1 = null;
//			if (this.ImageRequested != null)
//			{
//				image1 = this.ImageRequested(this, this.GetColumnValueAtRow(source, rowNum), source.List[rowNum]);
//			}
			//bool imageIndex = -1;// = OnShowImage(this.GetColumnValueAtRow(source, rowNum), source.List[rowNum]);
			if (this.SetShowImage != null)
			{
				DataGridImageCellEventArgs e = new DataGridImageCellEventArgs(this.GetColumnValueAtRow(source, rowNum), source.List[rowNum]);
				SetShowImage(this, e);
				this.imageIndex = e.ImageIndex;
			}

			if (this.imageIndex < 0 || Image == null)
			{
				Rectangle rectangle1;
				Rectangle rectangle2;
				rectangle1 = new Rectangle(bounds.X, bounds.Y, this.DefaultImageWidth, bounds.Height);
				g.FillRectangle(backBrush, rectangle1);
				rectangle2 = new Rectangle(bounds.X + this.DefaultImageWidth, bounds.Y, this.Space, bounds.Height);
				g.FillRectangle(backBrush, rectangle2);
				bounds.X = (bounds.X + rectangle1.Width) + this.Space;
				bounds.Width -= rectangle1.Width;
				if (this.DrawText)
				{
					base.Paint(g, bounds, source, rowNum, backBrush, foreBrush, alignToRight);
				}
				else
				{
					g.FillRectangle(backBrush, bounds.X, bounds.Y, bounds.Width, bounds.Height);
				}
			}
			else
			{
				Rectangle rectangle3;
				Rectangle rectangle5;

				
				//Color col = Color.FromArgb(128, ((SolidBrush)backBrush).Color);//SystemColors.ActiveCaption);
				//Brush backBrush2 = new SolidBrush(col);
				
				if (this.ImageHeight > 0)
				{
					rectangle3 = new Rectangle(bounds.X, bounds.Y, Image.Size.Width, this.ImageHeight);
					g.FillRectangle(backBrush, rectangle3);
					
					if (((SolidBrush)backBrush).Color == this.DataGridTableStyle.SelectionBackColor)
						g.DrawImage(Image, rectangle3, 0, 0, Image.Width, Image.Height,GraphicsUnit.Pixel, imgAttributes);
					else
						g.DrawImage(Image, rectangle3);

					if (this.ImageHeight < bounds.Height)
					{
						Rectangle rectangle4;
						rectangle4 = new Rectangle(bounds.X, bounds.Y + this.ImageHeight, bounds.Width, bounds.Height - this.ImageHeight);
						g.FillRectangle(backBrush, rectangle4);
					}
				}
				else
				{
					rectangle3 = new Rectangle(bounds.X, bounds.Y, Image.Size.Width, bounds.Height);
					g.FillRectangle(backBrush, rectangle3);
					if (((SolidBrush)backBrush).Color == this.DataGridTableStyle.SelectionBackColor)
						g.DrawImage(Image, rectangle3, 0, 0, Image.Width, Image.Height,GraphicsUnit.Pixel, imgAttributes);
					else
						g.DrawImage(Image, rectangle3);
				}
				rectangle5 = new Rectangle(bounds.X + Image.Size.Width, bounds.Y, this.Space, bounds.Height);
				g.FillRectangle(backBrush, rectangle5);
				bounds.X = (bounds.X + rectangle3.Width) + this.Space;
				bounds.Width -= rectangle3.Width;
				if (this.DrawText)
				{
					base.Paint(g, bounds, source, rowNum, backBrush, foreBrush, alignToRight);
				}
				else
				{
					g.FillRectangle(backBrush, bounds.X, bounds.Y, bounds.Width, bounds.Height);
				}
			}
		}

		#region Public Properties

		private string fieldName;
		private string filterFieldName;

		[DefaultValue(typeof(string), ""), Category("Misc")]
		public string FieldName
		{
			set 
			{
				fieldName = value;
			}
			get
			{
				return fieldName;
			}
		}

		[DefaultValue(typeof(string), ""), Category("Misc")]
		public string FilterFieldName
		{
			set 
			{
				filterFieldName = value;
			}
			get
			{
				return filterFieldName;
			}
		}

		[DefaultValue(0)]
		public int DefaultImageWidth 
		{
			get
			{
				return this._defaultImageWidth;
			}
			set
			{
				this._defaultImageWidth = value;
			}
		}

		[DefaultValue(false)]
		public bool DrawText
		{
			get
			{
				return this._drawText;
			}
			set
			{
				this._drawText = value;
			}
		}

		public int ImageHeight
		{
			get
			{
				return this._imageHeight;
			}
			set
			{
				this._imageHeight = value;
			}
		}

		[DefaultValue(0)]
		public int MinimumHeight
		{
			get
			{
				return this._minimumHeight;
			}
			set
			{
				this._minimumHeight = value;
			}
		}

		[DefaultValue(5)]
		public int Space
		{
			get
			{
				return this._space;
			}
			set
			{
				this._space = value;
			}
		}

		[
		DefaultValue((string) null)
		]
		public ImageList ImageList
		{
			get
			{
				return this.imageList;
			}
			set
			{
				if (this.imageList != value)
				{
					EventHandler handler1 = new EventHandler(this.ImageListRecreateHandle);
					EventHandler handler2 = new EventHandler(this.DetachImageList);
					if (this.imageList != null)
					{
						this.imageList.RecreateHandle -= handler1;
						this.imageList.Disposed -= handler2;
					}
					if (value != null)
					{
						this.curImage = null;
					}
					this.imageList = value;
					if (value != null)
					{
						value.RecreateHandle += handler1;
						value.Disposed += handler2;
					}
					base.Invalidate();
				}
			}
		}

		[
		Editor("System.Windows.Forms.Design.ImageIndexEditor, System.Design, Version=1.0.5000.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(UITypeEditor)), 
		DefaultValue(-1), 
		Localizable(true), 
		TypeConverter(typeof(ImageIndexConverter))
		]
		public int ImageIndex
		{
			get
			{
				if (((this.imageIndex != -1) && (this.imageList != null)) && (this.imageIndex >= this.imageList.Images.Count))
				{
					return (this.imageList.Images.Count - 1);
				}
				return this.imageIndex;
			}
			set
			{
				if (value < -1)
				{
					object[] objArray1 = new object[3] { "value", value.ToString(), "-1" } ;
					throw new ArgumentException("InvalidLowBoundArgumentEx");//SR.GetString("InvalidLowBoundArgumentEx", objArray1));
				}
				if (this.imageIndex != value)
				{
					if (value != -1)
					{
						this.curImage = null;
					}
					this.imageIndex = value;
					base.Invalidate();
				}
			}
		}

		[
		Localizable(true)
		]
		public Image Image
		{
			get
			{
				if (((this.curImage == null) && (this.imageList != null)) && (this.ImageIndex >= 0))
				{
					return this.imageList.Images[this.ImageIndex];
				}
				return this.curImage;
			}
			set
			{
				if (this.Image != value)
				{
					//this.StopAnimate();
					this.curImage = value;
					if (this.curImage != null)
					{
						this.ImageIndex = -1;
						this.ImageList = null;
					}
					//this.Animate();
					base.Invalidate();
				}
			}
		}

		#endregion Public Properties

		private void ImageListRecreateHandle(object sender, EventArgs e)
		{
//			if (base.IsHandleCreated)
//			{
				base.Invalidate();
//			}
		}

		private void DetachImageList(object sender, EventArgs e)
		{
			this.ImageList = null;
		}

		// Events

		public event ImageCellEventHandler SetShowImage;
	}

	public delegate void ImageCellEventHandler(object sender, DataGridImageCellEventArgs e);

	public class DataGridImageCellEventArgs : DataGridCellEventArgs
	{
		private int imageIndex;

		public DataGridImageCellEventArgs(object cellValue, object currentRow) : base(cellValue, currentRow)
		{
			imageIndex = -1;
		}

		//set index if you want the Paint method to draw Image
		public int ImageIndex
		{
			get{ return imageIndex;}
			set{ imageIndex = value;}
		}

	}
}
