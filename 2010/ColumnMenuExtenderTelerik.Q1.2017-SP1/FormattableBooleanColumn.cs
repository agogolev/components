﻿using System;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using System.ComponentModel;

namespace ColumnMenuExtender
{
	public class FormattableBooleanColumn : DataGridBoolColumn, IExtendedQuery
	{
		//Для того чтобы определить метод, определяющий цвет строки (например, если какое-то значение строки больше чего-либо - цвет бэкграунда строки розовый)
		public event GetColorForRow OnGetColorForRow;

		public event FormatCellEventHandler SetCellFormat;

		//overridden to fire BoolChange event and Formatting event
		protected override void Paint(System.Drawing.Graphics g, System.Drawing.Rectangle bounds, System.Windows.Forms.CurrencyManager source, int rowNum, System.Drawing.Brush backBrush, System.Drawing.Brush foreBrush, bool alignToRight)
		{
			System.Drawing.Brush backBr = backBrush;			

			bool isNewBrush = SetCellColor(rowNum, ref backBrush, source);
			if(!isNewBrush) {
				if(backBrush != backBr) {
					backBrush.Dispose();
					backBrush = backBr;
					isNewBrush = false;
				}
			}

			//used to handle the boolchanging
			//ManageBoolValueChanging(rowNum, colNum);
			
			//fire formatting event
			DataGridFormatCellEventArgs e = null;
			
			if(SetCellFormat != null)
			{
				e = new DataGridFormatCellEventArgs(this.GetColumnValueAtRow(source, rowNum), source.List[rowNum]);
				SetCellFormat(this, e);
				if(e.BackBrush != null) {
					backBrush.Dispose();
					backBrush = e.BackBrush;
					isNewBrush = false;
				}
			}
			if(((SolidBrush)backBrush).Color == this.DataGridTableStyle.SelectionBackColor) {
				if(backBrush != backBr) {
					backBrush.Dispose();
					backBrush = backBr;
					isNewBrush = false;
				}
			}
			base.Paint(g, bounds, source, rowNum, backBrush, foreBrush, alignToRight);

			//clean up
			if(e != null)
			{
				if(e.BackBrushDispose)
					e.BackBrush.Dispose();
				else if(e.BackBrush == null && isNewBrush)
					backBrush.Dispose();
				if(e.ForeBrushDispose)
					e.ForeBrush.Dispose();
				if(e.TextFontDispose)
					e.TextFont.Dispose();
			}
			else 
			{
				if(isNewBrush)
					backBrush.Dispose();
			}
		}

		private bool SetCellColor(int rowNum, ref System.Drawing.Brush backBrush, System.Windows.Forms.CurrencyManager source)
		{
			bool isNewBrush = false;

			if (!(DataGridTableStyle.DataGrid is ExtendedDataGrid))
				return false;
			ExtendedDataGrid dataGrid = (ExtendedDataGrid)DataGridTableStyle.DataGrid;
			DataTable RowColorsTable = dataGrid.RowColorsTable;
			//число строк в этой таблицe
			int rowColorCount = (dataGrid.AllowCustomRowColorList ? RowColorsTable.Rows.Count : 0);

			if (rowColorCount > 0) //если были указаны цвета для отображения грида
			{
				//в последней строкие таблицы RowColorsTable хранится общее число строк,
				//для которых указана цветновая последовательность. После этой строки начинается повторение				
				//rowsInBatch - число строк в гриде, после которых начинается повторение цветновой последовательности
				int rowsInBatch = (int)RowColorsTable.Rows[rowColorCount - 1]["position"];
				//номер строки в текущем блоке(rowsInBatch)
				int rowColorIndex = rowNum % rowsInBatch + 1;
				if (rowColorIndex == 1) //если начинается прорисовка нового блока, обнуляем позицию строки в таблице RowColorsTable					
					RowColorsTable.ExtendedProperties["currentRowColorsPosition"] = 0;

				//текущая позиция строки в таблице RowColorsTable
				//требуется для того, чтобы несколько раз напрасно не пробегать по таблице  RowColorsTable в поисках требцемой строки
				//т.к. строки которые уже были ипользованы, не будут использоваться до прорисовки следующего блока
				int currentRowColorsPosition = (int)RowColorsTable.ExtendedProperties["currentRowColorsPosition"];
				//ищем строку в которой указан цвет, рисуемой строки
				for (int i = currentRowColorsPosition; i < rowColorCount; i++)
				{
					if (((int)RowColorsTable.Rows[i]["position"]) >= rowColorIndex)
					{
						if (((SolidBrush)backBrush).Color != this.DataGridTableStyle.SelectionBackColor)
						{
							backBrush = new SolidBrush((Color)RowColorsTable.Rows[i]["color"]);
							isNewBrush = true;
						}
						RowColorsTable.ExtendedProperties["currentRowColorsPosition"] = i;
						break;
					}
				}
			}
			else if (OnGetColorForRow != null)//был определён делегат для выбора цвета
			{
				if (((SolidBrush)backBrush).Color != this.DataGridTableStyle.SelectionBackColor)
				{
					backBrush = OnGetColorForRow(rowNum, source);
					isNewBrush = true;
				}
			}
			else
			{
				if (((SolidBrush)backBrush).Color == this.DataGridTableStyle.SelectionBackColor)
					return false;
				if ((this.DataGridTableStyle.DataGrid is ExtendedDataGrid))
				{
					System.Drawing.Brush backBr = (this.DataGridTableStyle.DataGrid as ExtendedDataGrid).FireEventGetColorForRow(rowNum, source);
					if (backBr != null)
					{
						backBrush = backBr;
						isNewBrush = true;
					}
				}
			}

			return isNewBrush;
		}

		//changed event
		public event BoolValueChangedEventHandler BoolValueChanged;
		
		bool saveValue = false;
		int saveRow = -1;
		bool lockValue = false;
		bool beingEdited = false;
		public const int VK_SPACE = 32 ;// 0x20

		//needed to get the space bar changing of the bool value
		[System.Runtime.InteropServices.DllImport("user32.dll")]
		static extern short GetKeyState(int nVirtKey);

		//set variables to start tracking bool changes
		protected override void Edit(System.Windows.Forms.CurrencyManager source, int rowNum, System.Drawing.Rectangle bounds, bool readOnly, string instantText, bool cellIsVisible)
		{	
			if (!(readOnly || this.ReadOnly))
			{
				lockValue = true;
				beingEdited = true;
				saveRow = rowNum;
				saveValue = (bool)(base.GetColumnValueAtRow(source, rowNum) != DBNull.Value ? base.GetColumnValueAtRow(source, rowNum) : false);
				base.Edit(source, rowNum,  bounds, readOnly, instantText, cellIsVisible);
			}
			else
			{
				int cri = this.DataGridTableStyle.DataGrid.CurrentRowIndex;
				if (cri > 0)
					this.DataGridTableStyle.DataGrid.UnSelect(cri-1);
				this.DataGridTableStyle.DataGrid.Select(cri);
				if (cri < this.DataGridTableStyle.DataGrid.VisibleRowCount-1)
					this.DataGridTableStyle.DataGrid.UnSelect(cri+1);
			}
		}

		//turn off tracking bool changes
		protected override bool Commit(System.Windows.Forms.CurrencyManager dataSource, int rowNum)
		{
			lockValue = false;
			beingEdited = false;
			return base.Commit(dataSource, rowNum);
		}

		//fire the bool change event if the value changes
		private void ManageBoolValueChanging(int rowNum, int colNum)
		{
			Point mousePos = this.DataGridTableStyle.DataGrid.PointToClient(Control.MousePosition);
			DataGrid dg = this.DataGridTableStyle.DataGrid;
			bool isClickInCell = ((Control.MouseButtons == MouseButtons.Left) && 
				dg.GetCellBounds(dg.CurrentCell).Contains(mousePos) );

			bool changing = dg.Focused && ( isClickInCell 
				|| GetKeyState(VK_SPACE) < 0 ); // or spacebar
			
			if(!lockValue && beingEdited && changing && saveRow == rowNum)
			{
				saveValue = !saveValue;
				lockValue = false;

				//fire the event
				if(BoolValueChanged != null)
				{
					BoolValueChangedEventArgs e = new BoolValueChangedEventArgs(rowNum, colNum, saveValue);
					BoolValueChanged(this, e);
				}
			}
			//if(saveRow == rowNum)
			lockValue = false;	
		}

		#region Public Properties

		private string fieldName;
		private string filterFieldName;

		[DefaultValue(typeof(string), ""), Category("Misc")]
		public string FieldName
		{
			set 
			{
				fieldName = value;
			}
			get
			{
				return fieldName;
			}
		}

		[DefaultValue(typeof(string), ""), Category("Misc")]
		public string FilterFieldName
		{
			set 
			{
				filterFieldName = value;
			}
			get
			{
				return filterFieldName;
			}
		}

		#endregion
	}

	#region BoolValueChanging Event
	public delegate void BoolValueChangedEventHandler(object sender, BoolValueChangedEventArgs e);

	public class BoolValueChangedEventArgs : EventArgs
	{
		private int columnVal;
		private int rowVal;
		private bool boolVal;

		public BoolValueChangedEventArgs(int row, int col, bool val)
		{
			rowVal = row;
			columnVal = col;
			boolVal = val;
		}
		
		//column to be painted
		public int Column
		{
			get{ return columnVal;}
			set{ columnVal = value;}
		}

		//row to be painted
		public int Row
		{
			get{ return rowVal;}
			set{ rowVal = value;}
		}

		//current value to be painted
		public bool BoolValue
		{
			get{return boolVal;}
		}
	}
	#endregion
}