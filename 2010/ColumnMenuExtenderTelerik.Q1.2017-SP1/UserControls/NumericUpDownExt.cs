﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

namespace ColumnMenuExtender.UserControls
{
	/// <summary>
	/// Summary description for NumericUpDownExt.
	/// </summary>
	public class NumericUpDownExt : System.Windows.Forms.NumericUpDown
	{
		public delegate void DoSomething();
		public event DoSomething OnDownClick;
		public event DoSomething OnUpClick;
		private bool need = true;

		public NumericUpDownExt()
		{}
		
		public override void DownButton()
		{
			if (need)
				base.DownButton();

			if (OnDownClick != null)
				OnDownClick();
		}

		public override void UpButton()
		{
			if (need)
				base.UpButton();

			if (OnUpClick != null)
				OnUpClick();
		}

		[Browsable(true),DefaultValue(true),
		Category("Misc"),Description("Вызывать ли base методы при DownClick и UpClick")]
		public bool NeedBaseMethods
		{
			get{ return need; }
			set{ need = value; }
		}
	}
}
