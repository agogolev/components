﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using ColumnMenuExtender.Forms.Dialogs;
using MetaData;

namespace ColumnMenuExtender.UserControls
{
    /// <summary>
    ///     Summary description for BaseSelectorEdtTextBox.
    /// </summary>
    public class BaseSelectorEdtTextBox : UserControl
    {
        protected static readonly object EventChanged = new object();

        private string _autoTextColumn;
        private ObjectItem _boundProp;
        private Button _btnChoose;
        private Button _btnDel;

        private Button _btnEdit;

        //private Form currentForm;

        private IContainer components;
        public Hashtable Filters = new Hashtable();

        public string FilterString = "";
        private ImageList _imageList1;

        private string _multiClassContainer = "";
        private Guid[] _multiOiDs;
        private string _multiPropName = "";
        public int NKNumber = -1;

        public OrderInfo Order = null;

        public string SelectedNK;
        public string[] SelectedNKs;
        private HintTextBox _tbText;

        public int[] Widths;

        public BaseSelectorEdtTextBox()
        {
            InitializeComponent();

            ClassName = "";

            //if (autoTextColumn == "")
            //	this.tbText.ReadOnly = true;
        }

        protected virtual BaseListDialog GetBaseListDialog(
            string className,
            IList<string> fieldNames,
            IList<string> columnNames,
            IList<string> columnHeaderNames,
            IList<string> formatRows, 
            OrderInfo order, 
            Hashtable filters, 
            int[] widths)
        {
            return null;
        }

        protected virtual BaseListDialog GetBaseListDialog(
            string className,
            IList<string> columnNames,
            IList<string> columnHeaderNames,
            IList<string> formatRows,
            OrderInfo order, 
            Hashtable filters, 
            int[] widths)
        {
            return null;
        }

        /*public void BindForm(Form form)
		{
			currentForm = form;
		}*/

        /// <summary>
        ///     Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
                components?.Dispose();
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        ///     Required method for Designer support - do not modify
        ///     the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BaseSelectorEdtTextBox));
            this._btnChoose = new System.Windows.Forms.Button();
            this._btnDel = new System.Windows.Forms.Button();
            this._tbText = new HintTextBox();
            this._btnEdit = new System.Windows.Forms.Button();
            this._imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.SuspendLayout();
            // 
            // _btnChoose
            // 
            this._btnChoose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._btnChoose.Location = new System.Drawing.Point(200, 0);
            this._btnChoose.Name = "_btnChoose";
            this._btnChoose.Size = new System.Drawing.Size(24, 24);
            this._btnChoose.TabIndex = 1;
            this._btnChoose.Text = "...";
            this._btnChoose.Click += new System.EventHandler(this.btnChoose_Click);
            // 
            // _btnDel
            // 
            this._btnDel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._btnDel.Location = new System.Drawing.Point(224, 0);
            this._btnDel.Name = "_btnDel";
            this._btnDel.Size = new System.Drawing.Size(24, 24);
            this._btnDel.TabIndex = 2;
            this._btnDel.Text = "X";
            this._btnDel.Click += new System.EventHandler(this.btnDel_Click);
            // 
            // _tbText
            // 
            this._tbText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tbText.HintFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._tbText.Location = new System.Drawing.Point(0, 0);
            this._tbText.Name = "_tbText";
            this._tbText.ReadOnly = true;
            this._tbText.Size = new System.Drawing.Size(176, 20);
            this._tbText.TabIndex = 3;
            this._tbText.TabStop = false;
            this._tbText.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbText_KeyDown);
            // 
            // _btnEdit
            // 
            this._btnEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._btnEdit.ForeColor = System.Drawing.SystemColors.ControlText;
            this._btnEdit.ImageAlign = System.Drawing.ContentAlignment.BottomRight;
            this._btnEdit.ImageIndex = 0;
            this._btnEdit.ImageList = this._imageList1;
            this._btnEdit.Location = new System.Drawing.Point(176, 0);
            this._btnEdit.Name = "_btnEdit";
            this._btnEdit.Size = new System.Drawing.Size(24, 24);
            this._btnEdit.TabIndex = 3;
            this._btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // _imageList1
            // 
            this._imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("_imageList1.ImageStream")));
            this._imageList1.TransparentColor = System.Drawing.Color.Magenta;
            this._imageList1.Images.SetKeyName(0, "pencil-tip.png");
            // 
            // BaseSelectorEdtTextBox
            // 
            this.Controls.Add(this._tbText);
            this.Controls.Add(this._btnDel);
            this.Controls.Add(this._btnChoose);
            this.Controls.Add(this._btnEdit);
            this.Name = "BaseSelectorEdtTextBox";
            this.Size = new System.Drawing.Size(248, 24);
            this.Resize += new System.EventHandler(this.SelectorTextBox_Resize);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private void SelectorTextBox_Resize(object sender, EventArgs e)
        {
            _tbText.Width = Width - _btnEdit.Width - _btnDel.Width - _btnChoose.Width;
        }

        public void SetMultiFilter(Guid oid, string classContainer, string propName)
        {
            _multiOiDs = new[] { oid };
            _multiClassContainer = classContainer;
            _multiPropName = propName;
        }

        public void SetMultiFilter(Guid[] oids, string classContainer, string propName)
        {
            _multiOiDs = new Guid[oids.Length];
            for (var i = 0; i < oids.Length; i++)
                _multiOiDs[i] = oids[i];
            _multiClassContainer = classContainer;
            _multiPropName = propName;
        }

        protected virtual void btnChoose_Click(object sender, EventArgs e)
        {
            var fieldNames = new List<string>();

            var columnNames = RowNames?.Split(',');
            var columnHeaderNames = HeaderNames?.Split(',');
            var formatRows = FormatRows?.Split(',');

            if (!string.IsNullOrEmpty(FieldNames))
            {
                var fields = FieldNames.Split(',');

                //могла разделиться функция, типа func(a,b,c)
                //объединяем ее опять
                for (var i = 0; i < fields.Length;)
                {
                    var field = fields[i];
                    if (fields[i].IndexOf('(') != -1)
                        while (fields[i].IndexOf(')') == -1)
                        {
                            i++;
                            field += "," + fields[i];
                        }
                    fieldNames.Add(field);
                    i++;
                }
            }
            else
            {
                fieldNames = null;
            }

            var autoSelect = false;
            if (AutoTextColumn != "" && _tbText.Text != "" && !_tbText.ReadOnly)
            {
                var filter = new StringFilterInfo
                {
                    ColumnName = AutoTextColumn,
                    Values = new string[1]
                };
                filter.Values[0] = _tbText.Text + "*";
                filter.Verb = FilterVerb.Like;
                Filters[AutoTextColumn] = filter;
                autoSelect = true;
            }
            else
            {
                Filters.Remove(AutoTextColumn);
            }

            var fm = fieldNames != null ?
                GetBaseListDialog(ClassName, fieldNames, columnNames, columnHeaderNames, formatRows, Order, Filters, Widths) :
                GetBaseListDialog(ClassName, columnNames, columnHeaderNames, formatRows, Order, Filters, Widths);

            fm.FilterString = FilterString;

            //if (AutoTextColumn != "" && tbText.Text != "")
            //	fm.WindowState = FormWindowState.Minimized;

            if (_multiOiDs != null)
                if (_multiOiDs.Length == 1)
                    fm.SetMultiFilter(_multiOiDs[0], _multiClassContainer, _multiPropName);
                else fm.SetMultiFilter(_multiOiDs, _multiClassContainer, _multiPropName);
            fm.Text = ListFormName;

            if (fm.ShowDialog(this, autoSelect) == DialogResult.OK)
            {
                var o = fm.SelectedOID;
                SelectedNK = fm.SelectedNK;
                SelectedNKs = fm.SelectedNKs;
                BoundProp = NKNumber == -1 ? new ObjectItem(o, SelectedNK, ClassName) : new ObjectItem(o, SelectedNKs[NKNumber], ClassName);
                OnChanged();
            }
        }

        private void btnDel_Click(object sender, EventArgs e)
        {
            BoundProp = new ObjectItem(Guid.Empty, "", "");
            OnChanged();
            if (AutoTextColumn != "")
            {
                _tbText.ReadOnly = false;
                Filters.Remove(AutoTextColumn);
            }
        }

        [Category("Action")]
        [Description("Fired when data is changed")]
        public event EventHandler Changed
        {
            add { Events.AddHandler(EventChanged, value); }
            remove { Events.RemoveHandler(EventChanged, value); }
        }

        public virtual void OnChanged()
        {
            var initHandler = (EventHandler)Events[EventChanged];
            initHandler?.Invoke(this, EventArgs.Empty);
        }

        private void tbText_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Return && AutoTextColumn != "")
                btnChoose_Click(sender, null);
        }

        public virtual void btnEdit_Click(object sender, EventArgs e)
        {
        }

        #region Public Properties

        [DefaultValue(BorderStyle.Fixed3D)]
        [Category("Appearance")]
        public BorderStyle TextBoxBorderStyle
        {
            get { return _tbText.BorderStyle; }
            set { _tbText.BorderStyle = value; }
        }

        [DefaultValue(FlatStyle.Standard)]
        [Category("Appearance")]
        public FlatStyle ButtonFlatStyle
        {
            get { return _btnChoose.FlatStyle; }
            set
            {
                _btnChoose.FlatStyle = value;
                _btnDel.FlatStyle = value;
            }
        }

        [Category("Appearance")]
        public new Color BackColor
        {
            get { return _tbText.BackColor; }
            set { _tbText.BackColor = value; }
        }

        [Category("Appearance")]
        public Color ButtonBackColor
        {
            get { return _btnChoose.BackColor; }
            set
            {
                _btnChoose.BackColor = value;
                _btnDel.BackColor = value;
            }
        }

        /*[//DefaultValue(System.Drawing.SystemColors.Control),
        Category("Appearance")]
        public Color TextBoxBackColor
        {
            set {tbText.BackColor = value;}
            get	{return tbText.BackColor;}
        }*/

        [DefaultValue("")]
        [Description("Имя класса")]
        public string ClassName { set; get; }

        [DefaultValue("")]
        [Description("Имена желаемых полей. Старый вариант, без поддержки функций")]
        public string RowNames { set; get; }

        [DefaultValue("")]
        [Description("Имена желаемых полей. Новый вариант, с поддержкой функций")]
        public string FieldNames { set; get; }

        [DefaultValue("")]
        [Description("Имена заголовков")]
        public string HeaderNames { set; get; }

        [DefaultValue("")]
        [Description("Форматы колонок (типа d или 0.00)")]
        public string FormatRows { set; get; }

        [Browsable(false)]
        public ObjectItem BoundProp
        {
            get { return _boundProp; }
            set
            {
                _boundProp = value;
                if (_boundProp != null)
                {
                    _tbText.Text = _boundProp.NK;
                    if (AutoTextColumn != "")
                        _tbText.ReadOnly = _boundProp.OID != Guid.Empty;
                }
                else
                {
                    _tbText.Text = "";
                    if (AutoTextColumn != "")
                        _tbText.ReadOnly = false;
                }
            }
        }

        [DefaultValue("")]
        [Description("Имя листовой формы (form.Text)")]
        public string ListFormName { get; set; } = "";

        [DefaultValue("")]
        [Description(
            "Имя поля, по которому будет производиться поиск (появляется возможность набора руками искомого значения)")]
        public string AutoTextColumn
        {
            get
            {
                if (_autoTextColumn == null) return "";
                return _autoTextColumn;
            }
            set
            {
                _autoTextColumn = value;
                _tbText.ReadOnly = string.IsNullOrEmpty(value);
            }
        }

        [DefaultValue(false)]
        [Description("Включить/выключить поддержку хинтов")]
        public bool IsHinted
        {
            get { return _tbText.IsHinted; }

            set { _tbText.IsHinted = value; }
        }

        [DefaultValue(100)]
        [Description("Максимальное количество хинтов")]
        public int MaxHintCount
        {
            get { return _tbText.MaxHintCount; }
            set { _tbText.MaxHintCount = value; }
        }

        [DefaultValue(3)]
        [Description("Минимальное количство набранных букв для срабатывания хинтов")]
        public int MinLettersForHint
        {
            get { return _tbText.MinLettersForHint; }
            set { _tbText.MinLettersForHint = value; }
        }

        [DefaultValue(13)]
        public int HintItemHeight
        {
            get { return _tbText.ItemHeight; }
            set { _tbText.ItemHeight = value; }
        }

        [DefaultValue(4)]
        public int HeightInHints
        {
            get { return _tbText.HeightInHints; }
            set { _tbText.HeightInHints = value; }
        }

        [DefaultValue(SortType.Alphabet)]
        public SortType HintSort
        {
            get { return _tbText.Sort; }
            set { _tbText.Sort = value; }
        }

        #endregion Public Properties
    }
}