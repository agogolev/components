﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using MetaData;
using System.Drawing.Design;

namespace ColumnMenuExtender.UserControls
{
	/// <summary>
	/// Summary description for AddDelDataGridExt.
	/// </summary>
	public class AddDelDataGridExt : System.Windows.Forms.UserControl
	{
		public OrderInfo Order = null;
		public Hashtable Filters = new Hashtable();
		public ExtendedDataGrid extendedDataGrid1;

		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.Windows.Forms.Panel pnlButtons;
		private System.Windows.Forms.Button btnAdd;
		private System.Windows.Forms.Button btnDel;
		private System.Windows.Forms.ContextMenu contextMenu1;
		private System.Windows.Forms.MenuItem miDel;
		private System.Windows.Forms.MenuItem miAdd;
		private System.Windows.Forms.Panel pnlDataGrid;
		private System.ComponentModel.Container components = null;
		
		public AddDelDataGridExt()
		{
			InitializeComponent();
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.pnlButtons = new System.Windows.Forms.Panel();
			this.btnDel = new System.Windows.Forms.Button();
			this.btnAdd = new System.Windows.Forms.Button();
			this.contextMenu1 = new System.Windows.Forms.ContextMenu();
			this.miAdd = new System.Windows.Forms.MenuItem();
			this.miDel = new System.Windows.Forms.MenuItem();
			this.extendedDataGrid1 = new ExtendedDataGrid();
			this.pnlDataGrid = new System.Windows.Forms.Panel();
			this.pnlButtons.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.extendedDataGrid1)).BeginInit();
			this.pnlDataGrid.SuspendLayout();
			this.SuspendLayout();
			// 
			// pnlButtons
			// 
			this.pnlButtons.Controls.Add(this.btnDel);
			this.pnlButtons.Controls.Add(this.btnAdd);
			this.pnlButtons.Dock = System.Windows.Forms.DockStyle.Right;
			this.pnlButtons.Location = new System.Drawing.Point(280, 0);
			this.pnlButtons.Name = "pnlButtons";
			this.pnlButtons.Size = new System.Drawing.Size(32, 176);
			this.pnlButtons.TabIndex = 0;
			// 
			// btnDel
			// 
			this.btnDel.BackColor = System.Drawing.SystemColors.Control;
			this.btnDel.Location = new System.Drawing.Point(8, 32);
			this.btnDel.Name = "btnDel";
			this.btnDel.Size = new System.Drawing.Size(24, 23);
			this.btnDel.TabIndex = 2;
			this.btnDel.Text = "-";
			this.btnDel.Click += new System.EventHandler(this.btnDel_Click);
			// 
			// btnAdd
			// 
			this.btnAdd.BackColor = System.Drawing.SystemColors.Control;
			this.btnAdd.Location = new System.Drawing.Point(8, 0);
			this.btnAdd.Name = "btnAdd";
			this.btnAdd.Size = new System.Drawing.Size(24, 23);
			this.btnAdd.TabIndex = 1;
			this.btnAdd.Text = "+";
			this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
			// 
			// contextMenu1
			// 
			this.contextMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																																								 this.miAdd,
																																								 this.miDel});
			// 
			// miAdd
			// 
			this.miAdd.Index = 0;
			this.miAdd.Text = "Добавить";
			this.miAdd.Click += new System.EventHandler(this.btnAdd_Click);
			// 
			// miDel
			// 
			this.miDel.Index = 1;
			this.miDel.Text = "Удалить";
			this.miDel.Click += new System.EventHandler(this.btnDel_Click);
			// 
			// extendedDataGrid1
			// 
			this.extendedDataGrid1.BackgroundColor = System.Drawing.SystemColors.Control;
			this.extendedDataGrid1.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.extendedDataGrid1.CaptionVisible = false;
			this.extendedDataGrid1.DataMember = "";
			this.extendedDataGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.extendedDataGrid1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.extendedDataGrid1.Location = new System.Drawing.Point(0, 0);
			this.extendedDataGrid1.Name = "extendedDataGrid1";
			this.extendedDataGrid1.Size = new System.Drawing.Size(278, 174);
			this.extendedDataGrid1.TabIndex = 1;
			this.extendedDataGrid1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dataGrid_MouseDown);
			this.extendedDataGrid1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.dataGrid_MouseUp);
			// 
			// pnlDataGrid
			// 
			this.pnlDataGrid.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.pnlDataGrid.Controls.Add(this.extendedDataGrid1);
			this.pnlDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pnlDataGrid.Location = new System.Drawing.Point(0, 0);
			this.pnlDataGrid.Name = "pnlDataGrid";
			this.pnlDataGrid.Size = new System.Drawing.Size(280, 176);
			this.pnlDataGrid.TabIndex = 1;
			// 
			// AddDelDataGridExt
			// 
			this.Controls.Add(this.pnlDataGrid);
			this.Controls.Add(this.pnlButtons);
			this.Name = "AddDelDataGridExt";
			this.Size = new System.Drawing.Size(312, 176);
			this.pnlButtons.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.extendedDataGrid1)).EndInit();
			this.pnlDataGrid.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// Можно подключить своё контесктное меню
		/// </summary>
		/// <param name="ContextMenu"></param>
		public void SetCustomContextMenu(ContextMenu ContextMenu)
		{
			this.contextMenu1 = ContextMenu;
		}

		private void btnAdd_Click(object sender, System.EventArgs e)
		{
			OnAddClick(new EventArgs());
		}

		private void btnDel_Click(object sender, System.EventArgs e)
		{
			OnDeleteClick(new EventArgs());
		}
		
		#region Properties
		[DefaultValue(false)]		
		public bool ColumnHeadersVisibleOfDataGrid
		{
			set {extendedDataGrid1.ColumnHeadersVisible=value;}
			get {return this.extendedDataGrid1.ColumnHeadersVisible;}
		}

		[DefaultValue(false)]		
		public bool ReadOnlyOfDataGrid
		{
			set {this.extendedDataGrid1.ReadOnly = value;}
			get {return this.extendedDataGrid1.ReadOnly;}
		}

		[DefaultValue(false)]		
		public bool ColumnDragEnabled
		{
			set {this.extendedDataGrid1.ColumnDragEnabled = value;}
			get {return this.extendedDataGrid1.ColumnDragEnabled;}
		}

		public Color BackgroundColorOfDataGrid
		{
			set {this.extendedDataGrid1.BackgroundColor = value;}
			get {return this.extendedDataGrid1.BackgroundColor;}
		}

		public BorderStyle BorderStyleOfDataGrid
		{
			set {this.extendedDataGrid1.BorderStyle = value;}
			get {return this.extendedDataGrid1.BorderStyle;}
		}

		public BorderStyle BorderStyleOfPnlDataGrid
		{
			set {this.pnlDataGrid.BorderStyle = value;}
			get {return this.pnlDataGrid.BorderStyle;}
		}

		public FlatStyle FlatStyleOfBtnAdd
		{
			set {this.btnAdd.FlatStyle = value;}
			get {return this.btnAdd.FlatStyle;}
		}

		public Color BackColorOfBtnAdd
		{
			set {this.btnAdd.BackColor = value;}
			get {return this.btnAdd.BackColor;}
		}

		public FlatStyle FlatStyleOfBtnDel
		{
			set {this.btnDel.FlatStyle = value;}
			get {return this.btnDel.FlatStyle;}
		}

		public Color BackColorOfBtnDel
		{
			set {this.btnDel.BackColor = value;}
			get {return this.btnDel.BackColor;}
		}

		protected static readonly object EventAdd = new object();
		[Category("Action"),Description("Fired when add click")]
		public event EventHandler AddClick
		{
			add 
			{
				Events.AddHandler(EventAdd, value);
			}
			remove 
			{
				Events.RemoveHandler(EventAdd, value);
			}
		}

		public virtual void OnAddClick(EventArgs e) 
		{       
			EventHandler addHandler = (EventHandler)Events[EventAdd];
			if (addHandler != null) 
				addHandler(this, e);
		}

		protected static readonly object EventDelete = new object();
		[Category("Action"),Description("Fired when delete click")]
		public event EventHandler DeleteClick
		{
			add 
			{
				Events.AddHandler(EventDelete, value);
			}
			remove 
			{
				Events.RemoveHandler(EventDelete, value);
			}
		}

		public virtual void OnDeleteClick(EventArgs e) 
		{       
			EventHandler deleteHandler = (EventHandler)Events[EventDelete];
			if (deleteHandler != null) 
				deleteHandler(this, e);
		}

		[
		Localizable(true), 
		DesignerSerializationVisibility(DesignerSerializationVisibility.Content),
		Editor(typeof(ExtendedDataGrid.TableStylesCollectionEditor), typeof(UITypeEditor))
		]
		public GridTableStylesCollection TableStyles
		{
			get
			{
				return extendedDataGrid1.TableStyles;
			}
		}

//		[
//		DefaultValue("")
//		]
//		public string ClassName
//		{
//			set 
//			{
//				className = value;
//			}
//			get
//			{
//				return className;
//			}
//		}
//
//		[
//		DefaultValue("")
//		]
//		public string ColumnNames
//		{
//			set 
//			{
//				columnNames = value;
//			}
//			get
//			{
//				return columnNames;
//			}
//		}
//
//		[
//		DefaultValue("")
//		]
//		public string ColumnHeaderNames
//		{
//			set 
//			{
//				columnHeaderNames = value;
//			}
//			get
//			{
//				return columnHeaderNames;
//			}
//		}
		#endregion

		#region For dataGrid 
		private void dataGrid_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left || e.Button == MouseButtons.Right)
			{
				DataGrid.HitTestInfo hti = extendedDataGrid1.HitTest(e.X, e.Y);
				if (hti.Type == DataGrid.HitTestType.Cell || hti.Type == DataGrid.HitTestType.RowHeader)
				{
					int rowNum = hti.Row;
					extendedDataGrid1.UnSelect(extendedDataGrid1.CurrentRowIndex);
					extendedDataGrid1.CurrentRowIndex = rowNum;
					extendedDataGrid1.Select(rowNum);
					if (e.Button == MouseButtons.Right)
						contextMenu1.Show(extendedDataGrid1, new Point(e.X, e.Y));
				}
			}
		}

		private void dataGrid_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left)
			{
				DataGrid.HitTestInfo hti = extendedDataGrid1.HitTest(e.X, e.Y);
				if (hti.Type == DataGrid.HitTestType.Cell || hti.Type == DataGrid.HitTestType.RowHeader)
				{
					int rowNum = hti.Row;
					extendedDataGrid1.UnSelect(extendedDataGrid1.CurrentRowIndex);
					extendedDataGrid1.CurrentRowIndex = rowNum;
					extendedDataGrid1.Select(rowNum);
				}
			}
		}
		#endregion
	}
}
