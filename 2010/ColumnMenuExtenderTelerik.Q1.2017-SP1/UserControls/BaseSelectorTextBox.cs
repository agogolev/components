﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using ColumnMenuExtender.Forms.Dialogs;
using MetaData;

namespace ColumnMenuExtender.UserControls
{
    /// <summary>
    ///     Summary description for BaseSelectorTextBox.
    /// </summary>
    public class BaseSelectorTextBox : UserControl
    {
        protected static readonly object EventChanged = new object();

        /// <summary>
        ///     Required designer variable.
        /// </summary>
        private readonly Container _components = null;

        public string FilterString = "";

        public Hashtable Filters = new Hashtable();
        public int NKNumber = -1;
        public OrderInfo Order = null;

        public string SelectedNK;
        public IList<string> SelectedNKs;

        public int[] Widths;
        private string _autoTextColumn;
        private ObjectItem _boundProp;
        private Button btnChoose;
        private Button btnDel;
        private string className = "";
        private string listFormName = "";
        private Guid[] multi_OIDs;

        private string multi_classContainer = "";
        private string multi_propName = "";
        private HintTextBox tbText;

        public BaseSelectorTextBox()
        {
            InitializeComponent();
        }

        [DefaultValue(BorderStyle.Fixed3D),
         Category("Appearance")]
        public BorderStyle TextBoxBorderStyle
        {
            set { tbText.BorderStyle = value; }
            get { return tbText.BorderStyle; }
        }

        [DefaultValue(FlatStyle.Standard),
         Category("Appearance")]
        public FlatStyle ButtonFlatStyle
        {
            set
            {
                btnChoose.FlatStyle = value;
                btnDel.FlatStyle = value;
            }
            get { return btnChoose.FlatStyle; }
        }

        [Category("Appearance")]
        public new Color BackColor
        {
            get { return tbText.BackColor; }
            set { tbText.BackColor = value; }
        }

        [Category("Appearance")]
        public Color ButtonBackColor
        {
            set
            {
                btnChoose.BackColor = value;
                btnDel.BackColor = value;
            }
            get { return btnChoose.BackColor; }
        }

        [DefaultValue(""), Description("Имя класса")]
        public string ClassName
        {
            set { className = value; }
            get { return className; }
        }

        [DefaultValue(""), Description("Имена желаемых полей. Старый вариант, без поддержки функций")]
        public string RowNames { get; set; }

        [DefaultValue(""), Description("Имена желаемых полей. Новый вариант, с поддержкой функций")]
        public string FieldNames { get; set; }

        [DefaultValue(""), Description("Имена заголовков")]
        public string HeaderNames { get; set; }

        [DefaultValue(""), Description("Форматы колонок (типа d или 0.00)")]
        public string FormatRows { get; set; }

        [Browsable(false)]
        public ObjectItem BoundProp
        {
            get { return _boundProp; }
            set
            {
                _boundProp = value;
                if (_boundProp != null)
                {
                    tbText.Text = _boundProp.NK;
                    if (AutoTextColumn != "")
                        tbText.ReadOnly = _boundProp.OID != Guid.Empty;
                }
                else
                {
                    tbText.Text = "";
                    if (AutoTextColumn != "")
                        tbText.ReadOnly = false;
                }
            }
        }

        [DefaultValue(""), Description("Имя листовой формы (form.Text)")]
        public string ListFormName
        {
            get { return listFormName; }
            set { listFormName = value; }
        }

        [DefaultValue(""),
         Description(
             "Имя поля, по которому будет производиться поиск (появляется возможность набора руками искомого значения)")
        ]
        public string AutoTextColumn
        {
            get
            {
                if (_autoTextColumn == null) return "";
                return _autoTextColumn;
            }
            set
            {
                _autoTextColumn = value;
                if (string.IsNullOrEmpty(value))
                    tbText.ReadOnly = true;
                else tbText.ReadOnly = false;
            }
        }

        [DefaultValue(false), Description("Включить/выключить поддержку хинтов")]
        public bool IsHinted
        {
            get { return tbText.IsHinted; }
            set { tbText.IsHinted = value; }
        }

        [DefaultValue(100), Description("Максимальное количество хинтов")]
        public int MaxHintCount
        {
            get { return tbText.MaxHintCount; }
            set { tbText.MaxHintCount = value; }
        }

        [DefaultValue(3), Description("Минимальное количство набранных букв для срабатывания хинтов")]
        public int MinLettersForHint
        {
            get { return tbText.MinLettersForHint; }
            set { tbText.MinLettersForHint = value; }
        }

        [DefaultValue(13)]
        public int HintItemHeight
        {
            get { return tbText.ItemHeight; }
            set { tbText.ItemHeight = value; }
        }

        [DefaultValue(4)]
        public int HeightInHints
        {
            get { return tbText.HeightInHints; }
            set { tbText.HeightInHints = value; }
        }

        [DefaultValue(SortType.Alphabet)]
        public SortType HintSort
        {
            get { return tbText.Sort; }
            set { tbText.Sort = value; }
        }

        public event EventHandler ChooseClick;
        public event EventHandler DelClick;

        protected virtual BaseListDialog GetBaseListDialog(
            string className,
            IList<string> fieldNames,
            IList<string> columnNames,
            IList<string> columnHeaderNames,
            IList<string> formatRows,
            OrderInfo Order,
            Hashtable Filters,
            int[] widths)
        {
            return null;
        }

        protected virtual BaseListDialog GetBaseListDialog(
            string className,
            IList<string> columnNames,
            IList<string> columnHeaderNames,
            IList<string> formatRows,
            OrderInfo Order,
            Hashtable Filters,
            int[] widths)
        {
            return null;
        }

        protected override void OnLoad(EventArgs e)
        {
            btnChoose.Click += btnChoose_Click;
            if (ChooseClick != null)
            {
                btnChoose.Click += ChooseClick;
            }

            btnDel.Click += btnDel_Click;
            if (DelClick != null)
            {
                btnDel.Click += DelClick;
            }

            base.OnLoad(e);
        }

        /// <summary>
        ///     Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_components != null)
                {
                    _components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        public void SetMultiFilter(Guid OID, string classContainer, string propName)
        {
            multi_OIDs = new[] { OID };
            multi_classContainer = classContainer;
            multi_propName = propName;
        }

        public void SetMultiFilter(Guid[] OIDs, string classContainer, string propName)
        {
            multi_OIDs = new Guid[OIDs.Length];
            for (int i = 0; i < OIDs.Length; i++)
                multi_OIDs[i] = OIDs[i];
            multi_classContainer = classContainer;
            multi_propName = propName;
        }

        protected virtual void btnChoose_Click(object sender, EventArgs e)
        {
            IList<string> columnNames;
            IList<string> columnHeaderNames;
            IList<string> formatRows;
            var fieldNames = new List<string>();

            if (RowNames != null)
                columnNames = RowNames.Split(',');
            else
                columnNames = null;
            if (HeaderNames != null)
                columnHeaderNames = HeaderNames.Split(',');
            else
                columnHeaderNames = null;
            if (FormatRows != null)
                formatRows = FormatRows.Split(',');
            else
                formatRows = null;

            if (!string.IsNullOrEmpty(FieldNames))
            {
                IList<string> fields = FieldNames.Split(',');

                //могла разделиться функция, типа func(a,b,c)
                //объединяем ее опять
                for (int i = 0; i < fields.Count;)
                {
                    string field = fields[i];
                    if (fields[i].IndexOf('(') != -1)
                        while (fields[i].IndexOf(')') == -1)
                        {
                            i++;
                            field += "," + fields[i];
                        }
                    fieldNames.Add(field);
                    i++;
                }
            }
            else
                fieldNames = null;

            BaseListDialog fm;

            Boolean autoSelect = false;
            if (AutoTextColumn != "" && tbText.Text != "" && !tbText.ReadOnly)
            {
                var _filter = new StringFilterInfo();
                _filter.ColumnName = AutoTextColumn;
                _filter.Values = new string[1];
                _filter.Values[0] = tbText.Text + "*";
                _filter.Verb = FilterVerb.Like;
                Filters[AutoTextColumn] = _filter;
                autoSelect = true;
            }
            else
            {
                Filters.Remove(AutoTextColumn);
            }

            if (fieldNames != null)
            {
                fm = GetBaseListDialog(ClassName, fieldNames, columnNames, columnHeaderNames, formatRows, Order, Filters,
                    Widths);
            }
            else
                fm = GetBaseListDialog(ClassName, columnNames, columnHeaderNames, formatRows, Order, Filters, Widths);

            fm.FilterString = FilterString;

            if (multi_OIDs != null)
            {
                if (multi_OIDs.Length == 1)
                    fm.SetMultiFilter(multi_OIDs[0], multi_classContainer, multi_propName);
                else fm.SetMultiFilter(multi_OIDs, multi_classContainer, multi_propName);
            }
            fm.Text = listFormName;

            if (fm.ShowDialog(this, autoSelect) == DialogResult.OK)
            {
                Guid o = fm.SelectedOID;
                SelectedNK = fm.SelectedNK;
                SelectedNKs = fm.SelectedNKs;
                if (NKNumber == -1)
                    BoundProp = new ObjectItem(o, SelectedNK, ClassName);
                else BoundProp = new ObjectItem(o, SelectedNKs[NKNumber], ClassName);
                OnChanged(new EventArgs());
            }
        }

        private void btnDel_Click(object sender, EventArgs e)
        {
            BoundProp = new ObjectItem(Guid.Empty, "", "");
            OnChanged(new EventArgs());
            if (AutoTextColumn != "")
            {
                tbText.ReadOnly = false;
                Filters.Remove(AutoTextColumn);
            }
        }

        [Category("Action"), Description("Fired when data is changed")]
        public event EventHandler Changed
        {
            add { Events.AddHandler(EventChanged, value); }
            remove { Events.RemoveHandler(EventChanged, value); }
        }

        public virtual void OnChanged(EventArgs e)
        {
            var initHandler = (EventHandler)Events[EventChanged];
            if (initHandler != null)
            {
                initHandler(this, e);
            }
        }

        private void tbText_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Return && AutoTextColumn != "")
            {
                btnChoose_Click(sender, null);
            }
        }

        #region Component Designer generated code

        /// <summary>
        ///     Required method for Designer support - do not modify
        ///     the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnChoose = new System.Windows.Forms.Button();
            this.btnDel = new System.Windows.Forms.Button();
            this.tbText = new HintTextBox();
            this.SuspendLayout();
            // 
            // btnChoose
            // 
            this.btnChoose.Anchor =
                ((System.Windows.Forms.AnchorStyles)
                    ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnChoose.Location = new System.Drawing.Point(250, 0);
            this.btnChoose.Name = "btnChoose";
            this.btnChoose.Size = new System.Drawing.Size(26, 20);
            this.btnChoose.TabIndex = 1;
            this.btnChoose.Text = "...";
            // 
            // btnDel
            // 
            this.btnDel.Anchor =
                ((System.Windows.Forms.AnchorStyles)
                    ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDel.Location = new System.Drawing.Point(282, 0);
            this.btnDel.Name = "btnDel";
            this.btnDel.Size = new System.Drawing.Size(23, 20);
            this.btnDel.TabIndex = 2;
            this.btnDel.Text = "X";
            // 
            // tbText
            // 
            this.tbText.Anchor =
                ((System.Windows.Forms.AnchorStyles)
                    (((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                      | System.Windows.Forms.AnchorStyles.Right)));
            this.tbText.BackColor = System.Drawing.Color.White;
            this.tbText.HintFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F,
                System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbText.Location = new System.Drawing.Point(0, 0);
            this.tbText.Name = "tbText";
            this.tbText.ReadOnly = true;
            this.tbText.Size = new System.Drawing.Size(244, 20);
            this.tbText.TabIndex = 3;
            this.tbText.TabStop = false;
            this.tbText.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbText_KeyDown);
            // 
            // BaseSelectorTextBox
            // 
            this.Controls.Add(this.tbText);
            this.Controls.Add(this.btnChoose);
            this.Controls.Add(this.btnDel);
            this.Name = "BaseSelectorTextBox";
            this.Size = new System.Drawing.Size(308, 22);
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion
    }
}