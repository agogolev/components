﻿using System;
using System.ComponentModel;
using System.Globalization;
using MetaData;

namespace ColumnMenuExtender
{
    public class DataBoundTextBox : HintTextBox
    {
        private object _boundProp;
        private bool textChanging;

        public DataBoundTextBox()
        {
            IsCurrency = false;
        }

        public bool IsCurrency { get; set; }

        /// Устарело теперь используется св-во Format

        //private int precision = 2;
        //public int Precision
        //{
        //  get { return precision; }
        //  set { precision = value; }
        //}
        [DefaultValue(null)]
        public string Format { get; set; }

        public object BoundProp
        {
            get { return _boundProp; }
            set
            {
                if (value == null)
                {
                    _boundProp = null;
                    return;
                }
                var valueChange = false;
                if (value is string)
                {
                    if (_boundProp == null || _boundProp.GetType() != typeof (string) ||
                        (string) _boundProp != (string) value)
                        valueChange = true;
                    Text = Format == null ? (string) value : string.Format(Format, value);
                }
                else if (value is Guid)
                {
                    if (_boundProp == null || _boundProp.GetType() != typeof (Guid) || (Guid) _boundProp != (Guid) value)
                        valueChange = true;
                    var o = (Guid) value;
                    if (o == Guid.Empty)
                        Text = "";
                    else
                        Text = o.ToString();
                }
                else if (value is ObjectItem)
                {
                    if (_boundProp == null || _boundProp.GetType() != typeof (ObjectItem) ||
                        ((ObjectItem) _boundProp).OID != ((ObjectItem) value).OID)
                        valueChange = true;
                    var o = (ObjectItem) value;
                    if (o.OID == Guid.Empty)
                        Text = "";
                    else
                        Text = Format == null ? o.NK : string.Format(Format, o.NK);
                }
                else if (value is int)
                {
                    if (_boundProp == null || _boundProp.GetType() != typeof (int) || (int) _boundProp != (int) value)
                        valueChange = true;
                    var o = (int) value;
                    if (o == int.MinValue)
                        Text = "";
                    else
                        Text = Format == null ? o.ToString() : string.Format(Format, o);
                }
                else if (value is double)
                {
                    if (_boundProp == null || _boundProp.GetType() != typeof (double) ||
                        (double) _boundProp != (double) value)
                        valueChange = true;
                    var o = (double) value;
                    if (!textChanging)
                    {
                        if (o == double.MinValue)
                            Text = "";
                        else
                            Text = Format == null ? o.ToString() : string.Format(Format, o);
                    }
                }
                else if (value is decimal)
                {
                    if (_boundProp == null || _boundProp.GetType() != typeof (decimal) ||
                        (decimal) _boundProp != (decimal) value)
                        valueChange = true;
                    var o = (decimal) value;
                    if (!textChanging)
                    {
                        if (o == decimal.MinValue)
                            Text = "";
                        else if (IsCurrency)
                            Text = Math.Round(o, 2).ToString();
                        else
                            Text = string.IsNullOrEmpty(Format) ? o.ToString() : string.Format(Format, o);
                    }
                }
                else if (value is float)
                {
                    if (_boundProp == null || _boundProp.GetType() != typeof (float) ||
                        (float) _boundProp != (float) value)
                        valueChange = true;
                    var o = (float) value;
                    if (!textChanging)
                    {
                        if (o == float.MinValue)
                            Text = "";
                        else
                            Text = Format == null ? o.ToString() : string.Format(Format, o);
                    }
                }
                else if (value is DateTime)
                {
                    if (_boundProp == null || _boundProp.GetType() != typeof (DateTime) ||
                        (DateTime) _boundProp != (DateTime) value)
                        valueChange = true;
                    var o = (DateTime) value;
                    if (o == DateTime.MinValue)
                        Text = "";
                    else
                        Text = Format == null ? o.ToString() : string.Format(Format, o);
                }
                _boundProp = value;
                Refresh();
                if (valueChange)
                    OnValueChanged();
            }
        }

        /// <summary>
        ///     Если изначально isCurrency=true и требуется в ручную изменить значение свойство объекта - используй это!
        /// </summary>
        public object BoundPropForCurrencyManualChanging
        {
            set
            {
                IsCurrency = true;
                BoundProp = value;
            }
        }

        //private int efforts = 0; //количество попыток преобразования типа decimal
        //используется для того, чтобы при обычном заходе в контрол и выходе из него без изменения содержимого
        //не происходило обновление связанных с ним данных.
        public event EventHandler ValueChanged;

        protected virtual void OnValueChanged()
        {
            if (ValueChanged != null) ValueChanged(this, EventArgs.Empty);
        }

        protected override void OnTextChanged(EventArgs e)
        {
            textChanging = true;
            try
            {
                if (_boundProp == null)
                {
                    return;
                }
                if (BoundProp is string)
                {
                    BoundProp = Text;
                }
                else if (BoundProp is Guid)
                {
                    if (Text == "") _boundProp = Guid.Empty;
                    else BoundProp = new Guid(Text);
                }
                else if (BoundProp is int)
                {
                    if (Text == "") BoundProp = int.MinValue;
                    else BoundProp = Convert.ToInt32(Text);
                }
                else if (BoundProp is double)
                {
                    if (Text == "") BoundProp = double.MinValue;
                    else BoundProp = Convert.ToDouble(Text);
                }
                else if (BoundProp is decimal)
                {
                    if (Text == "") BoundProp = decimal.MinValue;
                    else
                    {
                        var value = Text;
                        var decimalSeparator = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;
                        if (decimalSeparator == "," && value.IndexOf(".") != -1)
                        {
                            value = value.Replace(".", ",");
                        }
                        else if (decimalSeparator == "." && value.IndexOf(",") != -1)
                        {
                            value = value.Replace(",", ".");
                        }
                        BoundProp = Convert.ToDecimal(value);
                    }
                }
                else if (BoundProp is DateTime)
                {
                    if (Text == "") BoundProp = DateTime.MinValue;
                    else BoundProp = Convert.ToDateTime(Text);
                }
            }
            catch
            {
            }
            finally
            {
                textChanging = false;
            }

            base.OnTextChanged(e);
        }
    }
}