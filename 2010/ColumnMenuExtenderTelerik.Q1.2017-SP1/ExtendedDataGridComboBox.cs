﻿using System.Windows.Forms;

namespace ColumnMenuExtender
{
	public class ExtendedDataGridComboBox : ComboBox
	{
		// Methods
		public ExtendedDataGridComboBox()
		{
			this.isInEditOrNavigateMode = true;
		}


		// Fields
		protected bool isInEditOrNavigateMode;
	}
}
