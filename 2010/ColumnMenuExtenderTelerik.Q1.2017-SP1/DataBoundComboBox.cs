﻿using System;
using System.Collections;
using System.Windows.Forms;
using System.Xml;
//using System.Collections;
using Telerik.WinControls;
using Telerik.WinControls.UI;

namespace ColumnMenuExtender
{
    public class DataBoundComboBox : RadDropDownList
    {
        public string DataXml = "";
        public string PropName = "";
        public DataBoundComboBox()
        {
            DropDownStyle = RadDropDownStyle.DropDownList;
        }

        public new int SelectedValue
        {
            get
            {
                if (SelectedItem != null)
                    return (int)SelectedItem.Value;
                return -1;
            }
            set
            {
                if (SelectedValue == value) return;
                var i = 0;
                foreach (RadListDataItem item in Items)
                {
                    if ((int)item.Value == value)
                    {
                        SelectedIndex = i;
                        break;
                    }
                    i++;
                }
            }
        }

        /// <summary>
        /// Загружает lookup поля с именем propName из xml
        /// </summary>
        public void LoadXml(string xml, string propName)
        {
            if (xml != "" && propName != "")
            {
                DataXml = xml;
                PropName = propName;
                var xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(xml);
                var nodes = xmlDoc.SelectNodes("/root/lookupField[@propName='" + propName + "']/item");
                Items.Clear();
                foreach (XmlNode n in nodes)
                {
                    var val = Convert.ToInt32(n.Attributes["ID"].Value);
                    var li = new RadListDataItem(n.InnerText, val);
                    Items.Add(li);
                }
            }
        }

        public void AddItems(ICollection ar)
        {
            foreach (ListItem li in ar)
            {
                Items.Add(new RadListDataItem(li.Text, li.Value)
                {
                    Tag = li.Tag
                });
            }
        }
        public void AddItem(ListItem li)
        {
            Items.Add(new RadListDataItem(li.Text, li.Value)
            {
                Tag = li.Tag
            });
        }

        public void InsertItem(int index, ListItem li)
        {
            Items.Insert(index, new RadListDataItem(li.Text, li.Value)
            {
                Tag = li.Tag
            });
        }

    }

    public class ListItem
    {
        public ListItem()
        {
            Value = 0;
            Text = "";
            //OID = Guid.Empty;
        }

        public ListItem(int val, string text)
        {
            Value = val;
            Text = text;
            //OID = Guid.Empty;
        }

        //public ListItem(int val, string text, Guid oid)
        //{
        //    Value = val;
        //    Text = text;
        //    OID = oid;
        //}

        public override string ToString()
        {
            return Text;
        }

        public int Value { get; set; }

        public string Text { get; set; }

        //public Guid OID { get; set; }

        public object Tag { get; set; }
    }
}
