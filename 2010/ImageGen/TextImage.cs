using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Web;
using System.Web.UI;
using System.Drawing.Drawing2D;
using System.Text;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.Text.RegularExpressions;


namespace Invento.TextImage
{
	/// <summary>
	/// Summary description for TextImage.
	/// </summary>
	public class ImageGen : IHttpHandler
	{
		public bool IsReusable 
		{
			get {return true;}
		}
		public Color GetColor(string rgb)
		{
			Color color;
			int r = HexToInt(rgb.Substring(0,2));
			int g = HexToInt(rgb.Substring(2,2));
			int b = HexToInt(rgb.Substring(4,2));
			color = Color.FromArgb(r, g, b); 
			return color;
		}
		
		public int HexToInt(string val) 
		{
			return Convert.ToInt32("0x"+val, 16);
		}

		public void ProcessRequest (HttpContext context) 
		{
			string img_Text = context.Request.RawUrl; // Params["Text"];
			Regex re = new Regex(@"\?Text=([^&]*)&");
			Match m = re.Match(img_Text);
			if(m.Success) {
				img_Text = m.Groups[1].Value;
			}
			else img_Text = "error";
			img_Text = HttpUtility.UrlDecode(img_Text, Encoding.UTF8); 
			Color img_ForeColor = Color.Black;
			if (context.Request["ForeColor"]!= null) 
				img_ForeColor = GetColor(context.Request["ForeColor"]);
			Color img_BackColor = Color.White;
			if (context.Request["BackColor"]!= null) 
				img_BackColor = GetColor(context.Request["BackColor"]);
			
			int img_width = Convert.ToInt32(context.Request["Width"]);
			int img_height = Convert.ToInt32(context.Request["Height"]);
			string img_font = context.Request["Font"];
			bool fontBold = context.Request["FontBold"] != null;
			bool fontItalic = context.Request["FontItalic"] != null;
			System.Globalization.NumberFormatInfo nfi = new System.Globalization.CultureInfo( "en-US", false ).NumberFormat;
			nfi.NumberDecimalSeparator = ".";
			double img_fontSize = Convert.ToDouble(context.Request["FontSize"], nfi);
			
			FontStyle fs = FontStyle.Regular;
			if(fontBold) fs |= FontStyle.Bold;
			if(fontItalic) fs |= FontStyle.Italic;
			Font fnt = new Font(img_font, (float)img_fontSize, fs);
			
			SolidBrush br = new SolidBrush(img_ForeColor);
			SolidBrush brArea = new SolidBrush(img_BackColor);
			Bitmap bmp = new Bitmap(img_width, img_height, PixelFormat.Format32bppArgb);
			Graphics g = Graphics.FromImage(bmp);
			g.SmoothingMode = SmoothingMode.AntiAlias;
			g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
			SizeF sf = g.MeasureString(img_Text, fnt);
			g.FillRectangle(brArea, new Rectangle(-1, -1, img_width+1, img_height+1));
			float cW = 0;
			float cH = 0;
			//��������� �������� ������ ��������� ������
			if (context.Request["X"] != null && !context.Request["X"].Equals(""))
				cW = (float)Convert.ToInt32(context.Request["X"]);
			else cW = (img_width - sf.Width)/2; 
			if (context.Request["Y"] != null && !context.Request["Y"].Equals(""))
				cH = (float)Convert.ToInt32(context.Request["Y"]);
			else cH = (img_height - sf.Height)/2;
			
			g.DrawString(img_Text, fnt, br, new PointF(cW, cH));
			context.Response.ContentType = "image/png";
			context.Response.Cache.AppendCacheExtension("post-check=900,pre-check=3600");
			System.IO.MemoryStream mem = new System.IO.MemoryStream();
			bmp.Save(mem, ImageFormat.Png);
			mem.WriteTo(context.Response.OutputStream);
			bmp.Dispose();
			br.Dispose();
			brArea.Dispose();
			fnt.Dispose();
			g.Dispose();
		}
	}
	
	public class TextImage : Control
	{
		#region TextImage Properties
		
		private string _text;
		private Color _foreColor = Color.Black;
		private Color _backColor = Color.White;
		private int _width = 50;
        private int _height = 50;
		private string _font = "Arial";
		private int _fontSize = 12;
		private string _x = "";
		private string _y = "";
					
		[Bindable(true), Category("Appearance"), DefaultValue(""), Description("����� ��� ������.")] 
		public string Text
		{
			get {
				return _text;
			}
			set {
				_text = value;
			}
		}
				
		public Color ForeColor
		{
			get {return _foreColor;}
			set {_foreColor = value;}
		}
		
		public Color BackColor
		{
			get {return _backColor;}
			set {_backColor = value;}
		}
		
		public int Width
		{
			get {return _width;}
			set
			{
				if (value >= 0) _width = value;
				else throw new ArgumentOutOfRangeException ();
			}
		}

		public int Height
		{
			get {return _height;}
			set
			{
				if (value >= 0) _height = value;
				else throw new ArgumentOutOfRangeException ();
			}
		}
		[Description("���������� X ��� ������ ������. ���� �������� ������, �� ����� ������������ ������������ �����������")] 
		public string X
		{
			get {return _x;}
			set {_x = value;}
		}
		[Description("���������� Y ��� ������ ������. ���� �������� ������, �� ����� ������������ ������������ �����������")] 
		public string Y
		{
			get {return _y;}
			set	{_y = value;}
		}
		public string Font
		{
			get {return _font;}
			set 
			{
				if (value != "") _font = value;
				else throw new ArgumentOutOfRangeException ();
			}
		}
		[Description("������ ������")] 
		public int FontSize
		{
			get {return _fontSize;}
			set 
			{
				if (value > 0) _fontSize = value;
				else throw new ArgumentOutOfRangeException ();
			}
		}

		#endregion TextImage Properties

		protected override void Render(HtmlTextWriter writer)
		{
			StringBuilder builder = new StringBuilder();
			string forecolor, backcolor;
			builder.Append("ImageGen.ashx?");
			builder.Append("Text=");
			builder.Append(HttpUtility.UrlEncode(Text, Encoding.UTF8));
			
			forecolor = GetRGB(ForeColor);
			if (forecolor.Equals("000"))
			{
				builder.Append("&ForeColor=");
				builder.Append("000000");
			}
			else 
			{
				builder.Append("&ForeColor=");
				builder.Append(forecolor);
			}
			backcolor = GetRGB(BackColor);
			
			
			if (Convert.ToInt32(BackColor.ToArgb().ToString("D"))!=-1)
			{
				if (backcolor.Equals("000"))
				{
					builder.Append("&BackColor=");
					builder.Append("000000");
				}
				else 
				{
					builder.Append("&BackColor=");
					builder.Append(backcolor);
				}
			}
			builder.Append("&Width=");
			builder.Append(Width);
			builder.Append("&Height=");
			builder.Append(Height);
			builder.Append("&Font=");
			builder.Append(_font);
			builder.Append("&FontSize=");
			builder.Append(_fontSize);
			//��������� ��������
			if (_x.Equals(""))
			{
				builder.Append("&X=");
				builder.Append("");
			}
			else 
			{
				builder.Append("&X=");
				builder.Append(_x);
			}
			if (_y.Equals(""))
			{
				builder.Append("&Y=");
				builder.Append("");
			}
			else 
			{
				builder.Append("&Y=");
				builder.Append(_y);
			}
			writer.WriteBeginTag("img");
			writer.WriteAttribute("src", builder.ToString());
			writer.WriteAttribute("width", this.Width.ToString());
			writer.WriteAttribute("height", this.Height.ToString());
			writer.WriteAttribute("border", "0");
			if (ID != null)
				writer.WriteAttribute("id", ClientID);
			writer.Write (HtmlTextWriter.TagRightChar);
		}
		
		public string GetRGB(Color clr)
		{
			string r,g,b;
			string finalColor = "";
			r = clr.R.ToString("X2").ToUpper();
			g = clr.G.ToString("X2").ToUpper();
			b = clr.B.ToString("X2").ToUpper();
			finalColor += r + g + b;
			return finalColor;
		}
	}
		
}
