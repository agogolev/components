using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
[assembly:AssemblyVersion("1.0.0.0")]
[assembly:AssemblyTitle("Invento.TImageGen")]
[assembly:AssemblyDescription("(Custom Build)")]
[assembly:AssemblyConfiguration("")]
[assembly:AssemblyCompany("Invento")]
[assembly:AssemblyProduct("Invento.TextImage (Custom Build)")]
[assembly:AssemblyCopyright("Copyright (C) 2004 Invento. All rights reserved.")]
[assembly:AssemblyTrademark("")]
[assembly:AssemblyCulture("")]
[assembly:TagPrefixAttribute("Invento.TextImage", "txtimg")]