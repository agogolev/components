using System;
using System.ComponentModel;
using System.Drawing;

namespace Invento.Web
{
	public abstract class ImageTransform
	{
		[Browsable(false)]
		public virtual string UniqueString
		{
			get
			{
				return this.GetType().FullName;
			}
		}

		protected ImageTransform()
		{
		}

		public abstract Image ProcessImage(Image image);
	}
}