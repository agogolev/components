using System;
using System.ComponentModel.Design;
using System.Drawing;

namespace Invento.Web
{
	public class ImageTransformCollectionEditor : CollectionEditor
	{
		private readonly static Type[] s_newItemTypes;

		static ImageTransformCollectionEditor()
		{
			ImageTransformCollectionEditor.s_newItemTypes = new Type[] { typeof(ImageResizeTransform) };
		}

		public ImageTransformCollectionEditor(Type type) : base(type)
		{
		}

		protected override Type CreateCollectionItemType()
		{
			return typeof(ImageTransformCollectionEditor.ImageTransform);
		}

		protected override Type[] CreateNewItemTypes()
		{
			return ImageTransformCollectionEditor.s_newItemTypes;
		}

		private sealed class ImageTransform : Invento.Web.ImageTransform
		{
			public int DummyProperty
			{
				get
				{
					return 1;
				}
				set
				{
				}
			}

			public ImageTransform()
			{
			}

			public override Image ProcessImage(Image image)
			{
				throw new NotImplementedException();
			}
		}
	}
}