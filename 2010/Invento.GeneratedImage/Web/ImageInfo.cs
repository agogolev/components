using System;
using System.Drawing;
using System.Net;
using System.Runtime.CompilerServices;

namespace Invento.Web
{
	public class ImageInfo
	{
		public System.Net.HttpStatusCode? HttpStatusCode
		{
			get;
			private set;
		}

		public System.Drawing.Image Image
		{
			get;
			private set;
		}

		public byte[] ImageByteBuffer
		{
			get;
			private set;
		}

	    public ImageInfo(System.Net.HttpStatusCode statusCode)
		{
			this.HttpStatusCode = new System.Net.HttpStatusCode?(statusCode);
		}

		public ImageInfo(System.Drawing.Image image)
		{
			if (image == null)
			{
				throw new ArgumentNullException("image");
			}
			this.Image = image;
		}

		public ImageInfo(byte[] imageBuffer)
		{
			if (imageBuffer == null)
			{
				throw new ArgumentNullException("imageBuffer");
			}
			this.ImageByteBuffer = imageBuffer;
		}
	}
}