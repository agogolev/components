using System;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Web.UI;

namespace Invento.Web
{
	[Bindable(true)]
	public class ImageParameter : IDataBindingsAccessor
	{
		private DataBindingCollection _dataBindings = new DataBindingCollection();

		[Bindable(false)]
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public Control BindingContainer
		{
			get;
			internal set;
		}

		[Category("Data")]
		public string Name
		{
			get;
			set;
		}

		DataBindingCollection System.Web.UI.IDataBindingsAccessor.DataBindings
		{
			get
			{
				return this._dataBindings;
			}
		}

		bool System.Web.UI.IDataBindingsAccessor.HasDataBindings
		{
			get
			{
				return this._dataBindings.Count != 0;
			}
		}

		[Bindable(true)]
		[Category("Data")]
		public string Value
		{
			get;
			set;
		}

		public ImageParameter()
		{
		}

		internal void DataBind()
		{
			if (this.DataBinding != null)
			{
				this.DataBinding(this, EventArgs.Empty);
			}
		}

		public override string ToString()
		{
			if (string.IsNullOrEmpty(this.Name) && string.IsNullOrEmpty(this.Value))
			{
				return this.ToString();
			}
			CultureInfo invariantCulture = CultureInfo.InvariantCulture;
			object[] name = new object[] { this.Name, this.Value };
			return string.Format(invariantCulture, "{0} = {1}", name);
		}

		public event EventHandler DataBinding;
	}
}