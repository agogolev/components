using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Design;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Invento.Web
{
    [Designer(typeof (GeneratedImageDesigner))]
    [ParseChildren(true)]
    [PersistChildren(false)]
    public class GeneratedImage : Image
    {
        private const string s_timestampField = "__timestamp";

        private readonly Control _bindingContainer;

        private readonly HttpContextBase _context;

        private string _imageHandlerUrl;

        private string _timestamp;

        public GeneratedImage()
        {
            Parameters = new List<ImageParameter>();
        }

        internal GeneratedImage(HttpContextBase context, Control bindingContainer) : this()
        {
            _context = context;
            _bindingContainer = bindingContainer;
        }

        private new Control BindingContainer
        {
            get { return _bindingContainer ?? base.BindingContainer; }
        }

        private new HttpContextBase Context
        {
            get
            {
                var httpContextWrapper = _context ?? new HttpContextWrapper(HttpContext.Current);
                return httpContextWrapper;
            }
        }

        [Category("Behavior")]
        [DefaultValue("")]
        [Editor(
            "System.Web.UI.Design.UrlEditor, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a",
            typeof (UITypeEditor))]
        [UrlProperty("*.ashx")]
        public string ImageHandlerUrl
        {
            get { return _imageHandlerUrl ?? string.Empty; }
            set { _imageHandlerUrl = value; }
        }

        [Category("Data")]
        [DefaultValue(null)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [MergableProperty(false)]
        [NotifyParentProperty(true)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        public List<ImageParameter> Parameters { get; }

        [Bindable(true)]
        [Category("Data")]
        [DefaultValue("")]
        public string Timestamp
        {
            get { return _timestamp ?? string.Empty; }
            set { _timestamp = value; }
        }

        private static void AddQueryStringParameter(StringBuilder stringBuilder, bool paramAlreadyAdded, string name,
            string value)
        {
            if (!paramAlreadyAdded)
            {
                stringBuilder.Append('?');
            }
            else
            {
                stringBuilder.Append('&');
            }
            stringBuilder.Append(HttpUtility.UrlEncode(name));
            stringBuilder.Append('=');
            stringBuilder.Append(HttpUtility.UrlEncode(value));
        }

        private string BuildImageUrl()
        {
            var stringBuilder = new StringBuilder();
            stringBuilder.Append(ImageHandlerUrl);
            var flag = false;
            foreach (var parameter in Parameters)
            {
                AddQueryStringParameter(stringBuilder, flag, parameter.Name, parameter.Value);
                flag = true;
            }
            var str = Timestamp?.Trim();
            if (!string.IsNullOrEmpty(str))
            {
                AddQueryStringParameter(stringBuilder, flag, "__timestamp", str);
            }
            return stringBuilder.ToString();
        }

        protected override void OnDataBinding(EventArgs e)
        {
            base.OnDataBinding(e);
            var bindingContainer = BindingContainer;
            foreach (var parameter in Parameters)
            {
                parameter.BindingContainer = bindingContainer;
                parameter.DataBind();
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if (DesignMode)
            {
                return;
            }
            ImageUrl = BuildImageUrl();
        }
    }
}