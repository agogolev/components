using System;
using System.Collections;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.IO;
using System.Security.Permissions;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI.Design;
using Invento.Web.Resources;

namespace Invento.Web
{
    [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
    public class GeneratedImageDesigner : ControlDesigner
    {
        public override DesignerActionListCollection ActionLists
        {
            get
            {
                var designerActionListCollection = new DesignerActionListCollection();
                designerActionListCollection.AddRange(base.ActionLists);
                designerActionListCollection.Add(new ImageHandlerActionList(this));
                return designerActionListCollection;
            }
        }

        private void CreateImageHandler()
        {
            var service = (IWebApplication) base.Component.Site.GetService(typeof (IWebApplication));
            var component = base.Component as GeneratedImage;
            if (component == null)
            {
                return;
            }
            try
            {
                var rootProjectItem = (IFolderProjectItem) service.RootProjectItem;
                IProjectItem projectItemFromUrl = service.GetProjectItemFromUrl(base.RootDesigner.DocumentUrl);
                string handlerTemplate = GetHandlerTemplate(InferLanguage(projectItemFromUrl));
                IDocumentProjectItem documentProjectItem = rootProjectItem.AddDocument("ImageHandler1.ashx",
                                                                                       Encoding.Default.GetBytes(
                                                                                           handlerTemplate));
                var projectItem = documentProjectItem as IProjectItem;
                PropertyDescriptor item = TypeDescriptor.GetProperties(component)["ImageHandlerUrl"];
                if (item != null)
                {
                    item.SetValue(component, projectItem.AppRelativeUrl);
                }
                documentProjectItem.Open();
            }
            catch (Exception exception)
            {
            }
        }

        private static string GetHandlerTemplate(Language language)
        {
            switch (language)
            {
                case Language.CSharp:
                    {
                        return WebResources.ImageHandlerTemplate_CSharp;
                    }
                case Language.VB:
                    {
                        return WebResources.ImageHandlerTemplate_VB;
                    }
            }
            throw new InvalidOperationException("Invalid language detected");
        }

        private Language InferLanguage(IProjectItem document)
        {
            Language value;
            try
            {
                Language? nullable = InferLanguageFromContent(document);
                if (nullable.HasValue)
                {
                    value = nullable.Value;
                    return value;
                }
            }
            catch (Exception exception)
            {
            }
            try
            {
                Language? nullable1 = InferLanguageFromCodebehindExtension(document);
                if (nullable1.HasValue)
                {
                    value = nullable1.Value;
                    return value;
                }
            }
            catch (Exception exception1)
            {
            }
            return Language.CSharp;
        }

        private Language? InferLanguageFromCodebehindExtension(IProjectItem document)
        {
            Language? nullable;
            IEnumerator enumerator = ((IFolderProjectItem) document.Parent).Children.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    string name = ((IProjectItem) enumerator.Current).Name;
                    if (!name.StartsWith(document.Name) || !(name != document.Name))
                    {
                        continue;
                    }
                    string lowerInvariant = name.Substring(name.LastIndexOf('.') + 1).ToLowerInvariant();
                    string str = lowerInvariant;
                    string str1 = str;
                    if (str == null)
                    {
                        continue;
                    }
                    if (str1 == "cs")
                    {
                        nullable = Language.CSharp;
                        return nullable;
                    }
                    else if (str1 == "vb")
                    {
                        nullable = Language.VB;
                        return nullable;
                    }
                }
                return null;
            }
            finally
            {
                var disposable = enumerator as IDisposable;
                if (disposable != null)
                {
                    disposable.Dispose();
                }
            }
            return nullable;
        }

        private static Language? InferLanguageFromContent(IProjectItem document)
        {
            Language? nullable;
            using (var streamReader = new StreamReader(((IDocumentProjectItem) document).GetContents()))
            {
                var regex = new Regex("<%@.*Language=\"(?<lang>[^\"]*)\".*%>");
                while (true)
                {
                    string str = streamReader.ReadLine();
                    string str1 = str;
                    if (str == null)
                    {
                        break;
                    }
                    Group item = regex.Match(str1).Groups["lang"];
                    if (item.Success)
                    {
                        string lowerInvariant = item.Value.ToLowerInvariant();
                        string str2 = lowerInvariant;
                        if (lowerInvariant != null)
                        {
                            if (str2 == "c#")
                            {
                                nullable = Language.CSharp;
                                return nullable;
                            }
                            if (str2 == "vb")
                            {
                                nullable = Language.VB;
                                return nullable;
                            }
                        }
                    }
                }
                return null;
            }
            return nullable;
        }

        private class ImageHandlerActionList : DesignerActionList
        {
            public ImageHandlerActionList(GeneratedImageDesigner parent) : base(parent.Component)
            {
                Parent = parent;
            }

            private GeneratedImageDesigner Parent { get; set; }

            public void CreateImageHandler()
            {
                Parent.CreateImageHandler();
            }

            public override DesignerActionItemCollection GetSortedActionItems()
            {
                var designerActionItemCollection = new DesignerActionItemCollection();
                designerActionItemCollection.Add(new DesignerActionMethodItem(this, "CreateImageHandler",
                                                                              "Create Image Handler", "Create Handler",
                                                                              string.Empty, false));
                return designerActionItemCollection;
            }
        }

        private enum Language
        {
            CSharp,
            VB
        }
    }
}