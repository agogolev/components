using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Runtime.CompilerServices;

namespace Invento.Web
{
	public class ImageResizeTransform : ImageTransform
	{
		private int _width;

		private int _height;

		[Category("Behavior")]
		[DefaultValue(0)]
		public int Height
		{
			get
			{
				return this._height;
			}
			set
			{
				ImageResizeTransform.CheckValue(value);
				this._height = value;
			}
		}

		[Category("Behavior")]
		[DefaultValue(System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic)]
		public System.Drawing.Drawing2D.InterpolationMode InterpolationMode
		{
			get;
			set;
		}

		[Category("Behavior")]
		[DefaultValue(ImageResizeMode.Fit)]
		public ImageResizeMode Mode
		{
			get;
			set;
		}

		[Browsable(false)]
		public override string UniqueString
		{
			get
			{
				object[] uniqueString = new object[] { base.UniqueString, this.Width, this.InterpolationMode.ToString(), this.Height, this.Mode.ToString() };
				return string.Concat(uniqueString);
			}
		}

		[Category("Behavior")]
		[DefaultValue(0)]
		public int Width
		{
			get
			{
				return this._width;
			}
			set
			{
				ImageResizeTransform.CheckValue(value);
				this._width = value;
			}
		}

		public ImageResizeTransform()
		{
			this.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
			this.Mode = ImageResizeMode.Fit;
		}

		private static void CheckValue(int value)
		{
			if (value < 0)
			{
				throw new ArgumentOutOfRangeException("value");
			}
		}

		private Image CropImage(Image img, int scaledHeight, int scaledWidth)
		{
			int width = 0;
			int height = 0;
			if ((float)this.Width / (float)img.Width <= (float)this.Height / (float)img.Height)
			{
				width = scaledWidth;
				height = this.Height;
			}
			else
			{
				width = this.Width;
				height = scaledHeight;
			}
			Bitmap bitmap = new Bitmap(this.Width, this.Height);
			Graphics graphic = Graphics.FromImage(bitmap);
			this.SetupGraphics(graphic);
			graphic.DrawImage(img, (this.Width - width) / 2, (this.Height - height) / 2, width, height);
			return bitmap;
		}

		private Image FitImage(Image img, int scaled_height, int scaled_width)
		{
			int width = 0;
			int scaledHeight = 0;
			if (this.Height == 0)
			{
				width = this.Width;
				scaledHeight = scaled_height;
			}
			else if (this.Width == 0)
			{
				width = scaled_width;
				scaledHeight = this.Height;
			}
			else if ((float)this.Width / (float)img.Width >= (float)this.Height / (float)img.Height)
			{
				width = scaled_width;
				scaledHeight = this.Height;
			}
			else
			{
				width = this.Width;
				scaledHeight = scaled_height;
			}
			Bitmap bitmap = new Bitmap(width, scaledHeight);
			Graphics graphic = Graphics.FromImage(bitmap);
			this.SetupGraphics(graphic);
			graphic.DrawImage(img, 0, 0, width, scaledHeight);
			return bitmap;
		}

		public override Image ProcessImage(Image img)
		{
			int height = (int)((float)img.Height * ((float)this.Width / (float)img.Width));
			int width = (int)((float)img.Width * ((float)this.Height / (float)img.Height));
			switch (this.Mode)
			{
				case ImageResizeMode.Fit:
				{
					return this.FitImage(img, height, width);
				}
				case ImageResizeMode.Crop:
				{
					return this.CropImage(img, height, width);
				}
			}
			return null;
		}

		private void SetupGraphics(Graphics graphics)
		{
			graphics.CompositingMode = CompositingMode.SourceCopy;
			graphics.CompositingQuality = CompositingQuality.HighSpeed;
			graphics.InterpolationMode = this.InterpolationMode;
		}

		public override string ToString()
		{
			return "ImageResizeTransform";
		}
	}
}