using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using Invento.GeneratedImage.Web;

namespace Invento.Web
{
    internal class ImageHandlerInternal
    {
        private static TimeSpan s_defaultClientCacheExpiration;

        private readonly IImageStore _imageStore;

        private readonly DateTime? _now;
        private TimeSpan _clientCacheExpiration = s_defaultClientCacheExpiration;

        static ImageHandlerInternal()
        {
            s_defaultClientCacheExpiration = new TimeSpan(0, 10, 0);
        }

        public ImageHandlerInternal()
        {
            ContentType = ImageFormat.Jpeg;
            ImageTransforms = new List<ImageTransform>();
        }

        internal ImageHandlerInternal(IImageStore imageStore, DateTime now) : this()
        {
            _imageStore = imageStore;
            _now = now;
        }

        public TimeSpan ClientCacheExpiration
        {
            get { return _clientCacheExpiration; }
            set
            {
                if (value.Ticks < 0)
                {
                    throw new ArgumentOutOfRangeException("value", "ClientCacheExpiration must be positive");
                }
                _clientCacheExpiration = value;
                EnableClientCache = true;
            }
        }

        public ImageFormat ContentType { get; set; }

        private DateTime DateTime_Now
        {
            get
            {
                DateTime? nullable = _now;
                if (!nullable.HasValue)
                {
                    return DateTime.Now;
                }
                return nullable.GetValueOrDefault();
            }
        }

        public bool EnableClientCache { get; set; }

        public bool EnableServerCache { get; set; }

        private IImageStore ImageStore
        {
            get { return _imageStore ?? DiskImageStore.Instance; }
        }

        public List<ImageTransform> ImageTransforms { get; private set; }

        private static string GetIDFromBytes(byte[] buffer)
        {
            byte[] numArray = SHA1.Create().ComputeHash(buffer);
            var stringBuilder = new StringBuilder();
            for (int i = 0; i < numArray.Length; i++)
            {
                stringBuilder.Append(numArray[i].ToString("X2", CultureInfo.InvariantCulture));
            }
            return stringBuilder.ToString();
        }

        internal static string GetImageMimeType(ImageFormat format)
        {
            string str = "image/x-unknown";
            if (format.Equals(ImageFormat.Gif))
            {
                str = "image/gif";
            }
            else if (format.Equals(ImageFormat.Jpeg))
            {
                str = "image/jpeg";
            }
            else if (format.Equals(ImageFormat.Png))
            {
                str = "image/png";
            }
            else if (format.Equals(ImageFormat.Bmp) || format.Equals(ImageFormat.MemoryBmp))
            {
                str = "image/bmp";
            }
            else if (format.Equals(ImageFormat.Tiff))
            {
                str = "image/tiff";
            }
            else if (format.Equals(ImageFormat.Icon))
            {
                str = "image/x-icon";
            }
            return str;
        }

        private Image GetImageThroughTransforms(Image image)
        {
            Image image1 = image;
            foreach (ImageTransform imageTransform in ImageTransforms)
            {
                image1 = imageTransform.ProcessImage(image1);
            }
            return image1;
        }

        private Image GetImageThroughTransforms(byte[] buffer)
        {
            return GetImageThroughTransforms(Image.FromStream(new MemoryStream(buffer)));
        }

        private string GetUniqueIDString(HttpContextBase context, string uniqueIdStringSeed)
        {
            var stringBuilder = new StringBuilder();
            stringBuilder.Append(uniqueIdStringSeed);
            foreach (string str in 
                from k in context.Request.QueryString.AllKeys
                orderby k
                select k)
            {
                stringBuilder.Append(str);
                stringBuilder.Append(context.Request.QueryString.Get(str));
            }
            foreach (ImageTransform imageTransform in ImageTransforms)
            {
                stringBuilder.Append(imageTransform.UniqueString);
            }
            return GetIDFromBytes(Encoding.ASCII.GetBytes(stringBuilder.ToString()));
        }

        public void HandleImageRequest(HttpContextBase context, Func<NameValueCollection, ImageInfo> imageGenCallback,
                                       string uniqueIdStringSeed)
        {
            context.Response.Clear();
            context.Response.ContentType = GetImageMimeType(ContentType);
            HttpCachePolicyBase cache = context.Response.Cache;
            cache.SetValidUntilExpires(true);
            if (EnableClientCache)
            {
                cache.SetCacheability(HttpCacheability.Public);
                cache.SetExpires(DateTime_Now + ClientCacheExpiration);
            }
            string uniqueIDString = GetUniqueIDString(context, uniqueIdStringSeed);
            if (EnableServerCache && ImageStore.TryTransmitIfContains(uniqueIDString, context.Response))
            {
                context.Response.End();
                return;
            }
            ImageInfo imageInfo = imageGenCallback(context.Request.QueryString);
            if (imageInfo == null)
            {
                throw new InvalidOperationException("The image generation handler cannot return null.");
            }
            if (imageInfo.HttpStatusCode.HasValue)
            {
                context.Response.StatusCode = (int) imageInfo.HttpStatusCode.Value;
                context.Response.End();
                return;
            }
            var memoryStream = new MemoryStream();
            if (imageInfo.Image != null)
            {
                RenderImage(GetImageThroughTransforms(imageInfo.Image), memoryStream);
            }
            else if (imageInfo.ImageByteBuffer != null)
            {
                RenderImage(GetImageThroughTransforms(imageInfo.ImageByteBuffer), memoryStream);
            }
            byte[] buffer = memoryStream.GetBuffer();
            context.Response.OutputStream.Write(buffer, 0, buffer.Length);
            if (EnableServerCache)
            {
                ImageStore.Add(uniqueIDString, buffer);
            }
            context.Response.End();
        }

        private void RenderImage(Image image, Stream outStream)
        {
            var bmp = new Bitmap(image);
            bmp.Save(outStream, this.ContentType);
        }
    }
}