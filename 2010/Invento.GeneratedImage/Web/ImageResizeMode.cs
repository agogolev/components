using System;

namespace Invento.Web
{
	public enum ImageResizeMode
	{
		Fit,
		Crop
	}
}