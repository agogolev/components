using System;
using System.Web;

namespace Invento.Web
{
	internal interface IImageStore
	{
		void Add(string id, byte[] data);

		bool TryTransmitIfContains(string id, HttpResponseBase response);
	}
}