using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Drawing.Imaging;
using System.Runtime.CompilerServices;
using System.Web;

namespace Invento.Web
{
	public abstract class ImageHandler : IHttpHandler
	{
		public TimeSpan ClientCacheExpiration
		{
			get
			{
				return this.Implementation.ClientCacheExpiration;
			}
			set
			{
				this.Implementation.ClientCacheExpiration = value;
			}
		}

		public ImageFormat ContentType
		{
			get
			{
				return this.Implementation.ContentType;
			}
			set
			{
				this.Implementation.ContentType = value;
			}
		}

		public bool EnableClientCache
		{
			get
			{
				return this.Implementation.EnableClientCache;
			}
			set
			{
				this.Implementation.EnableClientCache = value;
			}
		}

		public bool EnableServerCache
		{
			get
			{
				return this.Implementation.EnableServerCache;
			}
			set
			{
				this.Implementation.EnableServerCache = value;
			}
		}

		protected List<ImageTransform> ImageTransforms
		{
			get
			{
				return this.Implementation.ImageTransforms;
			}
		}

		private ImageHandlerInternal Implementation
		{
			get;
			set;
		}

		public virtual bool IsReusable
		{
			get
			{
				return false;
			}
		}

		protected ImageHandler() : this(new ImageHandlerInternal())
		{
		}

		private ImageHandler(ImageHandlerInternal implementation)
		{
			this.Implementation = implementation;
		}

		internal ImageHandler(IImageStore imageStore, DateTime now) : this(new ImageHandlerInternal(imageStore, now))
		{
		}

		public abstract ImageInfo GenerateImage(NameValueCollection parameters);

		public void ProcessRequest(HttpContext context)
		{
			if (context == null)
			{
				throw new ArgumentNullException("context");
			}
			this.ProcessRequest(new HttpContextWrapper(context));
		}

		internal void ProcessRequest(HttpContextBase context)
		{
			this.Implementation.HandleImageRequest(context, (NameValueCollection queryString) => this.GenerateImage(queryString), this.ToString());
		}
	}
}