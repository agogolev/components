using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Web;
using System.Web.Hosting;
using Invento.Web;

namespace Invento.GeneratedImage.Web
{
	public class DiskImageStore : IImageStore
	{
		private const string s_tempFileExtension = ".tmp";

		private const string s_cacheAppRelativePath = "~\\App_Data\\_imagecache\\";

		private static DiskImageStore s_instance;

		private static object s_instanceLock;

		private static string _cachePath;

		private DateTime _lastPurge;

		private object _purgeQueuedLock = new object();

		private bool _purgeQueued;

		private static TimeSpan _purgeInterval;

		private object _fileLock = new object();

		public static string CachePath
		{
			get
			{
				return DiskImageStore._cachePath;
			}
			set
			{
				if (string.IsNullOrEmpty(value))
				{
					throw new ArgumentNullException("value");
				}
				DiskImageStore._cachePath = value;
			}
		}

		public static bool EnableAutoPurge
		{
			get;
			set;
		}

		internal static IImageStore Instance
		{
			get
			{
				if (DiskImageStore.s_instance == null)
				{
					lock (DiskImageStore.s_instanceLock)
					{
						if (DiskImageStore.s_instance == null)
						{
							DiskImageStore.s_instance = new DiskImageStore();
						}
					}
				}
				return DiskImageStore.s_instance;
			}
		}

		private DateTime LastPurge
		{
			get
			{
				return this._lastPurge;
			}
			set
			{
				this._lastPurge = value;
			}
		}

		public static TimeSpan PurgeInterval
		{
			get
			{
				return DiskImageStore._purgeInterval;
			}
			set
			{
				if (value.Ticks < (long)0)
				{
					throw new ArgumentOutOfRangeException("value");
				}
				DiskImageStore._purgeInterval = value;
			}
		}

		static DiskImageStore()
		{
			DiskImageStore.s_instanceLock = new object();
			DiskImageStore.EnableAutoPurge = true;
			DiskImageStore.PurgeInterval = new TimeSpan(0, 5, 0);
			DiskImageStore.CachePath = HostingEnvironment.MapPath("~\\App_Data\\_imagecache\\");
		}

		internal DiskImageStore()
		{
			if (DiskImageStore.CachePath != null && !Directory.Exists(DiskImageStore.CachePath))
			{
				Directory.CreateDirectory(DiskImageStore.CachePath);
			}
			this._lastPurge = DateTime.Now;
		}

		private void Add(string id, byte[] data)
		{
			string str = DiskImageStore.BuildFilePath(id);
			lock (this.GetFileLockObject(id))
			{
				try
				{
					File.WriteAllBytes(str, data);
				}
				catch (Exception exception)
				{
				}
			}
		}

		private static string BuildFilePath(string id)
		{
			return string.Concat(DiskImageStore.CachePath, id, ".tmp");
		}

		private object GetFileLockObject(string id)
		{
			return this._fileLock;
		}

		void Invento.Web.IImageStore.Add(string id, byte[] data)
		{
			this.Add(id, data);
		}

		bool Invento.Web.IImageStore.TryTransmitIfContains(string id, HttpResponseBase response)
		{
			return this.TryTransmitIfContains(id, response);
		}

		private void PurgeCallback(object target)
		{
			FileInfo[] files = (new DirectoryInfo(DiskImageStore.CachePath)).GetFiles();
			DateTime dateTime = DateTime.Now.Subtract(DiskImageStore.PurgeInterval);
			List<FileInfo> fileInfos = new List<FileInfo>();
			FileInfo[] fileInfoArray = files;
			for (int i = 0; i < (int)fileInfoArray.Length; i++)
			{
				FileInfo fileInfo = fileInfoArray[i];
				if (fileInfo.CreationTime < dateTime)
				{
					try
					{
						fileInfo.Delete();
					}
					catch (Exception exception)
					{
						fileInfos.Add(fileInfo);
					}
				}
			}
			Thread.Sleep(0);
			foreach (FileInfo fileInfo1 in fileInfos)
			{
				try
				{
					fileInfo1.Delete();
				}
				catch (Exception exception1)
				{
				}
			}
			this.LastPurge = DateTime.Now;
			this._purgeQueued = false;
		}

		private void QueueAutoPurge()
		{
			DateTime now = DateTime.Now;
			if (!this._purgeQueued && now.Subtract(this.LastPurge) > DiskImageStore.PurgeInterval)
			{
				lock (this._purgeQueuedLock)
				{
					if (!this._purgeQueued)
					{
						this._purgeQueued = true;
						ThreadPool.QueueUserWorkItem(new WaitCallback(this.PurgeCallback));
					}
				}
			}
		}

		private bool TryTransmitIfContains(string id, HttpResponseBase response)
		{
			bool flag;
			if (DiskImageStore.EnableAutoPurge)
			{
				this.QueueAutoPurge();
			}
			string str = DiskImageStore.BuildFilePath(id);
			lock (this.GetFileLockObject(id))
			{
				if (!File.Exists(str))
				{
					flag = false;
				}
				else
				{
					response.TransmitFile(str);
					flag = true;
				}
			}
			return flag;
		}
	}
}