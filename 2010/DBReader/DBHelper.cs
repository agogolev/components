using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Xml;
using System.Web;
using System.Collections;
using System.Globalization;
using MetaData;
using System.Xml.Linq;
using System.Configuration;
using NLog;

namespace DBReader
{
    /// <summary>
    /// Summary description for XmlDBHelper.
    /// </summary>
    public class DBHelper
    {
        private static readonly Logger log = LogManager.GetCurrentClassLogger();
        private const int MAX_RETRY_COUNT = 7;
        public static XElement GetXmlData(string sql, Hashtable prms, string rootName, bool isStoredProc)
        {
            using (var conn = new SqlConnection(ConfigurationManager.AppSettings["dbdata.connection"]))
            {
                using (var cmd = new SqlCommand(sql, conn))
                {
                    if (isStoredProc)
                        cmd.CommandType = CommandType.StoredProcedure;
                    if (prms != null)
                        if (isStoredProc && sql.Trim().ToLower() == "sp_executesql") FillParamExecSql(cmd, prms);
                        else foreach (DictionaryEntry e in prms) cmd.Parameters.AddWithValue((string)e.Key, e.Value ?? DBNull.Value);
                    cmd.Connection.Open();
                    if (ConfigurationManager.AppSettings["commandTimeout"] != null)
                        cmd.CommandTimeout = int.Parse(ConfigurationManager.AppSettings["commandTimeout"]);
                    return FillXml(cmd, rootName);
                }
            }

        }
        public static XElement GetXmlData(string sql, Hashtable prms, string rootName, bool isStoredProc, SqlConnection conn, SqlTransaction trans)
        {
            if (conn == null)
                return GetXmlData(sql, prms, rootName, isStoredProc);
            using (var cmd = new SqlCommand(sql, conn, trans))
            {
                var connectionOpened = conn.State == ConnectionState.Open;
                if (isStoredProc)
                    cmd.CommandType = CommandType.StoredProcedure;
                if (prms != null)
                    if (isStoredProc && sql.Trim().ToLower() == "sp_executesql")
                        FillParamExecSql(cmd, prms);
                    else
                        foreach (DictionaryEntry e in prms)
                            cmd.Parameters.AddWithValue((string)e.Key, e.Value ?? DBNull.Value);
                if (!connectionOpened) cmd.Connection.Open();
                if (ConfigurationManager.AppSettings["commandTimeout"] != null)
                    cmd.CommandTimeout = int.Parse(ConfigurationManager.AppSettings["commandTimeout"]);

                try
                {
                    return FillXml(cmd, rootName);
                }
                finally
                {
                    if (!connectionOpened) cmd.Connection.Close();
                }
            }
        }

        private static XElement FillXml(SqlCommand cmd, string rootName, int retryCount = 1)
        {
            try
            {
                var dr = cmd.ExecuteReader();
                try
                {
                    var sb = new StringBuilder();
                    if (!string.IsNullOrWhiteSpace(rootName)) sb.AppendFormat("<{0}>", rootName);
                    while (dr.Read())
                    {
                        sb.Append((string)dr[0]);
                    }
                    if (!string.IsNullOrWhiteSpace(rootName)) sb.AppendFormat("</{0}>", rootName);
                    return XElement.Parse(sb.ToString().Replace("&#x1F;", string.Empty).Replace("&#x1E;", string.Empty));
                }
                finally
                {
                    dr.Close();
                }
            }
            catch (SqlException sqlEx)
            {
                if (retryCount == MAX_RETRY_COUNT) //5, 7, Whatever
                {
                    log.Error("Unable to FillXml, reached maximum number of retries.");
                    throw;
                }

                switch (sqlEx.Number)
                {
                    case DBConstants.SQLDeadlockErrorCode: //1205
                        log.Warn("FillXml was deadlocked, will try again.");
                        break;
                    case DBConstants.SQLTimeoutErrorCode: //-2
                        log.Warn("FillXml was timedout, will try again.");
                        break;
                    default:
                        throw;
                }

                System.Threading.Thread.Sleep(1000); //Can also use Math.Rand for a random interval of time
                return FillXml(cmd, rootName, ++retryCount);
            }


        }
        //метод без поддержки транзакций
        public static object ExecuteScalar(string sql, Hashtable prms, bool isStoredProc, int retryCount = 1)
        {
            using (var cmd = new SqlCommand(sql, new SqlConnection(ConfigurationManager.AppSettings["dbdata.connection"])))
            {
                if (prms != null)
                    if (isStoredProc && sql.Trim().ToLower() == "sp_executesql")
                        FillParamExecSql(cmd, prms);
                    else
                        foreach (DictionaryEntry e in prms)
                            cmd.Parameters.AddWithValue((string)e.Key, e.Value ?? DBNull.Value);
                if (isStoredProc)
                    cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();
                if (ConfigurationManager.AppSettings["commandTimeout"] != null)
                    cmd.CommandTimeout = int.Parse(ConfigurationManager.AppSettings["commandTimeout"]);

                try
                {
                    return cmd.ExecuteScalar();
                }
                catch (SqlException sqlEx)
                {
                    cmd.Connection.Close();
                    if (retryCount == MAX_RETRY_COUNT) //5, 7, Whatever
                    {
                        log.Error("Unable to ExecuteScalar, reached maximum number of retries.");
                        throw;
                    }

                    switch (sqlEx.Number)
                    {
                        case DBConstants.SQLDeadlockErrorCode: //1205
                            log.Warn("ExecuteScalar was deadlocked, will try again.");
                            break;
                        case DBConstants.SQLTimeoutErrorCode: //-2
                            log.Warn("ExecuteScalar was timedout, will try again.");
                            break;
                        default:
                            throw;
                    }

                    System.Threading.Thread.Sleep(1000); //Can also use Math.Rand for a random interval of time
                    return ExecuteScalar(sql, prms, isStoredProc, ++retryCount);
                }
                finally
                {
                    if (cmd.Connection.State == ConnectionState.Open)
                    {
                        cmd.Connection.Close();
                    }
                }
            }
        }
        //метод с поддержкой транзакций
        public static object ExecuteScalar(string sql, Hashtable prms, bool isStoredProc, SqlConnection conn, SqlTransaction trans, int retryCount = 1)
        {
            using (var cmd = new SqlCommand(sql, conn, trans))
            {
                if (prms != null)
                    if (isStoredProc && sql.Trim().ToLower() == "sp_executesql")
                        FillParamExecSql(cmd, prms);
                    else
                        foreach (DictionaryEntry e in prms)
                            cmd.Parameters.AddWithValue((string)e.Key, e.Value ?? DBNull.Value);
                if (isStoredProc)
                    cmd.CommandType = CommandType.StoredProcedure;
                if (ConfigurationManager.AppSettings["commandTimeout"] != null)
                    cmd.CommandTimeout = int.Parse(ConfigurationManager.AppSettings["commandTimeout"]);
                try
                {
                    return cmd.ExecuteScalar();
                }
                catch (SqlException sqlEx)
                {
                    if (retryCount == MAX_RETRY_COUNT) //5, 7, Whatever
                    {
                        log.Error("Unable to ExecuteScalar, reached maximum number of retries.");
                        throw;
                    }

                    switch (sqlEx.Number)
                    {
                        case DBConstants.SQLDeadlockErrorCode: //1205
                            log.Warn("ExecuteScalar was deadlocked, will try again.");
                            break;
                        case DBConstants.SQLTimeoutErrorCode: //-2
                            log.Warn("ExecuteScalar was timedout, will try again.");
                            break;
                        default:
                            throw;
                    }

                    System.Threading.Thread.Sleep(1000); //Can also use Math.Rand for a random interval of time
                    return ExecuteScalar(sql, prms, isStoredProc, conn, trans, ++retryCount);
                }
            }
        }

        public static SqlDataReader GetData(string sql, Hashtable prms, bool isStoredProc)
        {
            using (var cmd = new SqlCommand(sql, new SqlConnection(ConfigurationManager.AppSettings["dbdata.connection"])))
            {
                return DoGetData(sql, prms, isStoredProc, cmd, true);
            }
        }
        public static SqlDataReader GetData(string sql, Hashtable prms, bool isStoredProc, SqlConnection conn, SqlTransaction trans)
        {
            if (conn == null) return GetData(sql, prms, isStoredProc);
            using (var cmd = new SqlCommand(sql, conn, trans))
            {
                return DoGetData(sql, prms, isStoredProc, cmd, false);
            }
        }

        private static SqlDataReader DoGetData(string sql, Hashtable prms, bool isStoredProc, SqlCommand cmd, bool isCloseConnection, int retryCount = 1)
        {
            if (prms != null)
                if (isStoredProc && sql.Trim().ToLower() == "sp_executesql")
                    FillParamExecSql(cmd, prms);
                else
                    foreach (DictionaryEntry e in prms)
                        cmd.Parameters.AddWithValue((string)e.Key, e.Value ?? DBNull.Value);
            if (isStoredProc)
                cmd.CommandType = CommandType.StoredProcedure;
            if (cmd.Connection.State != ConnectionState.Open)
                cmd.Connection.Open();
            if (ConfigurationManager.AppSettings["commandTimeout"] != null)
                cmd.CommandTimeout = int.Parse(ConfigurationManager.AppSettings["commandTimeout"]);
            try
            {
                return isCloseConnection ? cmd.ExecuteReader(CommandBehavior.CloseConnection) : cmd.ExecuteReader();
            }
            catch (SqlException sqlEx)
            {
                if (retryCount == MAX_RETRY_COUNT) //5, 7, Whatever
                {
                    log.Error("Unable to DoGetData, reached maximum number of retries.");
                    throw;
                }

                switch (sqlEx.Number)
                {
                    case DBConstants.SQLDeadlockErrorCode: //1205
                        log.Warn("DoGetData was deadlocked, will try again.");
                        break;
                    case DBConstants.SQLTimeoutErrorCode: //-2
                        log.Warn("DoGetData was timedout, will try again.");
                        break;
                    default:
                        throw;
                }

                System.Threading.Thread.Sleep(1000); //Can also use Math.Rand for a random interval of time
                return DoGetData(sql, prms, isStoredProc, cmd, isCloseConnection, ++retryCount);
            }

        }
        //метод без поддержки транзакций
        public static void ExecuteCommand(string sql, Hashtable prms, bool isStoredProc, int retryCount = 1)
        {
            using (var cmd = new SqlCommand(sql, new SqlConnection(ConfigurationManager.AppSettings["dbdata.connection"])))
            {
                if (prms != null)
                    if (isStoredProc && sql.Trim().ToLower() == "sp_executesql")
                        FillParamExecSql(cmd, prms);
                    else
                        foreach (DictionaryEntry e in prms)
                            cmd.Parameters.AddWithValue((string)e.Key, e.Value ?? DBNull.Value);
                if (isStoredProc)
                    cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();
                if (ConfigurationManager.AppSettings["commandTimeout"] != null)
                    cmd.CommandTimeout = int.Parse(ConfigurationManager.AppSettings["commandTimeout"]);
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SqlException sqlEx)
                {
                    cmd.Connection.Close();
                    if (retryCount == MAX_RETRY_COUNT) //5, 7, Whatever
                    {
                        log.Error("Unable to ExecuteCommand, reached maximum number of retries.");
                        throw;
                    }

                    switch (sqlEx.Number)
                    {
                        case DBConstants.SQLDeadlockErrorCode: //1205
                            log.Warn("ExecuteCommand was deadlocked, will try again.");
                            break;
                        case DBConstants.SQLTimeoutErrorCode: //-2
                            log.Warn("ExecuteCommand was timedout, will try again.");
                            break;
                        default:
                            throw;
                    }

                    System.Threading.Thread.Sleep(1000); //Can also use Math.Rand for a random interval of time
                    ExecuteCommand(sql, prms, isStoredProc, ++retryCount);
                }
                finally
                {
                    if (cmd.Connection.State == ConnectionState.Open)
                    {
                        cmd.Connection.Close();
                    }
                }
            }
        }
        //метод с поддержкой транзакций
        public static void ExecuteCommand(string sql, Hashtable prms, bool isStoredProc, SqlConnection conn, SqlTransaction trans, int retryCount = 1)
        {
            if (conn == null)//если conn = null то значит надо создавать новое соединение
                ExecuteCommand(sql, prms, isStoredProc);
            else
            {
                var cmd = new SqlCommand(sql, conn, trans);
                if (prms != null)
                {
                    if (isStoredProc && sql.Trim().ToLower() == "sp_executesql")
                    {
                        FillParamExecSql(cmd, prms);
                    }
                    else
                    {
                        foreach (DictionaryEntry e in prms)
                        {
                            cmd.Parameters.AddWithValue((string)e.Key, e.Value ?? DBNull.Value);
                        }
                    }
                }
                if (isStoredProc)
                    cmd.CommandType = CommandType.StoredProcedure;
                if (ConfigurationManager.AppSettings["commandTimeout"] != null)
                    cmd.CommandTimeout = int.Parse(ConfigurationManager.AppSettings["commandTimeout"]);
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SqlException sqlEx)
                {
                    cmd.Connection.Close();
                    if (retryCount == MAX_RETRY_COUNT) //5, 7, Whatever
                    {
                        log.Error("Unable to ExecuteCommand, reached maximum number of retries.");
                        throw;
                    }

                    switch (sqlEx.Number)
                    {
                        case DBConstants.SQLDeadlockErrorCode: //1205
                            log.Warn("ExecuteCommand was deadlocked, will try again.");
                            break;
                        case DBConstants.SQLTimeoutErrorCode: //-2
                            log.Warn("ExecuteCommand was timedout, will try again.");
                            break;
                        default:
                            throw;
                    }

                    System.Threading.Thread.Sleep(1000); //Can also use Math.Rand for a random interval of time
                    ExecuteCommand(sql, prms, isStoredProc, ++retryCount);
                }
            }
        }
        private static XmlDocument GetOptionXml()
        {
            XmlDocument doc;
            const string cacheName = "www_option_xml";
            if (HttpContext.Current.Cache[cacheName] != null) doc = (XmlDocument)HttpContext.Current.Cache[cacheName];
            else
            {
                doc = new XmlDocument();
                doc.Load(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["option"]));
                HttpContext.Current.Cache.Insert(cacheName, doc, null, DateTime.MaxValue, TimeSpan.FromMinutes(20));
            }
            return doc;
        }
        public static string AppSetting(string XPath)
        {
            var doc = GetOptionXml();
            var n = doc.DocumentElement.SelectSingleNode(XPath);
            if (n != null && n is XmlElement) return n.InnerText;
            if (n != null && n is XmlAttribute) return n.Value;
            return "";
        }
        public static XmlNodeList AppSettings(string XPath)
        {
            XmlDocument doc = GetOptionXml();
            return doc.DocumentElement.SelectNodes(XPath);
        }
        public static DataSet GetDataSetSql(string sql, string tableName, UserInfo usr, int retryCount = 1)
        {
            var ds = new DataSet();

            var cmd = QueryConstructor.GetCommand(sql, CommandType.Text, usr);
            var da = new SqlDataAdapter(cmd);
            if (string.IsNullOrEmpty(tableName))
                tableName = "table";
            try
            {
                da.Fill(ds, tableName);
                return ds;
            }
            catch (SqlException sqlEx)
            {
                if (retryCount == MAX_RETRY_COUNT) //5, 7, Whatever
                {
                    log.Error("Unable to GetDataSetSql, reached maximum number of retries.");
                    throw;
                }

                switch (sqlEx.Number)
                {
                    case DBConstants.SQLDeadlockErrorCode: //1205
                        log.Warn("GetDataSetSql was deadlocked, will try again.");
                        break;
                    case DBConstants.SQLTimeoutErrorCode: //-2
                        log.Warn("GetDataSetSql was timedout, will try again.");
                        break;
                    default:
                        throw;
                }

                System.Threading.Thread.Sleep(1000); //Can also use Math.Rand for a random interval of time
                return GetDataSetSql(sql, tableName, usr, ++retryCount);
            }
        }
        public static DataSet GetDataSetSql(string sql, string tableName, Hashtable prms, bool isStoredProc, UserInfo usr, int retryCount = 1)
        {
            var ds = new DataSet();

            var cmd = QueryConstructor.GetCommand(sql, CommandType.Text, usr);
            if (isStoredProc) cmd.CommandType = CommandType.StoredProcedure;
            if (prms != null)
            {
                if (isStoredProc && sql.Trim().ToLower() == "sp_executesql")
                {
                    FillParamExecSql(cmd, prms);
                }
                else
                {
                    foreach (DictionaryEntry e in prms)
                    {
                        cmd.Parameters.AddWithValue((string)e.Key, e.Value ?? DBNull.Value);
                    }
                }
            }
            if (ConfigurationManager.AppSettings["commandTimeout"] != null)
                cmd.CommandTimeout = int.Parse(ConfigurationManager.AppSettings["commandTimeout"]);
            var da = new SqlDataAdapter(cmd);
            if (string.IsNullOrEmpty(tableName)) tableName = "table";
            try
            {
                da.Fill(ds, tableName);
                return ds;
            }
            catch (SqlException sqlEx)
            {
                if (retryCount == MAX_RETRY_COUNT) //5, 7, Whatever
                {
                    log.Error("Unable to GetDataSetSql, reached maximum number of retries.");
                    throw;
                }

                switch (sqlEx.Number)
                {
                    case DBConstants.SQLDeadlockErrorCode: //1205
                        log.Warn("GetDataSetSql was deadlocked, will try again.");
                        break;
                    case DBConstants.SQLTimeoutErrorCode: //-2
                        log.Warn("GetDataSetSql was timedout, will try again.");
                        break;
                    default:
                        throw;
                }

                System.Threading.Thread.Sleep(1000); //Can also use Math.Rand for a random interval of time
                return GetDataSetSql(sql, tableName, prms, isStoredProc, usr, ++retryCount);
            }
        }
        public static void AddDataTable(DataSet ds, string sql, string tableName, Hashtable prms, bool isStoredProc, UserInfo usr, int retryCount = 1)
        {
            var cmd = QueryConstructor.GetCommand(sql, CommandType.Text, usr);
            if (isStoredProc) cmd.CommandType = CommandType.StoredProcedure;
            if (prms != null)
            {
                if (isStoredProc && sql.Trim().ToLower() == "sp_executesql")
                {
                    FillParamExecSql(cmd, prms);
                }
                else
                {
                    foreach (DictionaryEntry e in prms)
                    {
                        cmd.Parameters.AddWithValue((string)e.Key, e.Value ?? DBNull.Value);
                    }
                }
            }
            if (ConfigurationManager.AppSettings["commandTimeout"] != null)
                cmd.CommandTimeout = int.Parse(ConfigurationManager.AppSettings["commandTimeout"]);
            var da = new SqlDataAdapter(cmd);
            if (string.IsNullOrEmpty(tableName)) tableName = "table";
            try
            {
                da.Fill(ds, tableName);
            }
            catch (SqlException sqlEx)
            {
                if (retryCount == MAX_RETRY_COUNT) //5, 7, Whatever
                {
                    log.Error("Unable to AddDataTable, reached maximum number of retries.");
                    throw;
                }

                switch (sqlEx.Number)
                {
                    case DBConstants.SQLDeadlockErrorCode: //1205
                        log.Warn("AddDataTable was deadlocked, will try again.");
                        break;
                    case DBConstants.SQLTimeoutErrorCode: //-2
                        log.Warn("AddDataTable was timedout, will try again.");
                        break;
                    default:
                        throw;
                }

                System.Threading.Thread.Sleep(1000); //Can also use Math.Rand for a random interval of time
                AddDataTable(ds, sql, tableName, prms, isStoredProc, usr, ++retryCount);
            }

        }

        private static void FillParamExecSql(SqlCommand cmd, Hashtable prms)
        {
            cmd.Parameters.AddWithValue("@stmt", prms["@stmt"]);
            cmd.Parameters.AddWithValue("@params", prms["@params"]);
            foreach (DictionaryEntry e in prms)
            {
                if ((string)e.Key != "@stmt" && (string)e.Key != "@params")
                {
                    cmd.Parameters.AddWithValue((string)e.Key, e.Value ?? DBNull.Value);
                }
            }
        }
        public static string ToOurString(object obj)
        {
            var nfi = new CultureInfo("en-US").NumberFormat;
            nfi.NumberDecimalSeparator = ".";
            if ((obj == null))
                return "null";
            var strA = obj as string;
            if (strA != null)
            {
                return string.Compare(strA, "\0", true) == 0 ? "" : EscapeApos(strA);
            }
            if (obj is DateTime)
            {
                return (DateTime)obj == DateTime.MinValue ? "null" : ((DateTime)obj).ToString("dd.MM.yyyy HH:mm:ss.fff");
            }
            if (obj is int)
            {
                return (int)obj == int.MinValue ? "null" : obj.ToString();
            }
            if (obj is double)
            {
                return Math.Abs((double)obj - double.MinValue) < 0.01 ? "null" : ((double)obj).ToString(nfi);
            }
            if (obj is decimal)
            {
                return (decimal)obj == decimal.MinValue ? "null" : ((decimal)obj).ToString(nfi);
            }
            if (obj is Guid)
            {
                return (Guid)obj == Guid.Empty ? "null" : obj.ToString();
            }
            if (obj is Boolean)
            {
                return (bool)obj == false ? "0" : "1";
            }
            var value = obj as byte[];
            return value != null ? FormatByteString(value) : null;
        }
        private static string EscapeApos(string str)
        {
            return str.Replace("'", "''");
        }
        private static string FormatByteString(IEnumerable<byte> value)
        {
            var sb = new StringBuilder();
            foreach (var b in value)
            {
                sb.Append(b.ToString("X2"));
            }
            return sb.ToString();
        }

        //For DataSetISM
        //метод без поддержки транзакций
        public static DataSetISM GetDataSetISMSql(string sql, string tableName, UserInfo usr, int retryCount = 1)
        {
            var ds = new DataSetISM();

            var cmd = QueryConstructor.GetCommand(sql, CommandType.Text, usr);
            var da = new SqlDataAdapter(cmd);
            if (ConfigurationManager.AppSettings["commandTimeout"] != null)
                cmd.CommandTimeout = int.Parse(ConfigurationManager.AppSettings["commandTimeout"]);

            if (string.IsNullOrEmpty(tableName)) tableName = "table";
            try
            {
                da.Fill(ds, tableName);
                return ds;
            }
            catch (SqlException sqlEx)
            {
                if (retryCount == MAX_RETRY_COUNT) //5, 7, Whatever
                {
                    log.Error("Unable to GetDataSetISMSql, reached maximum number of retries.");
                    throw;
                }

                switch (sqlEx.Number)
                {
                    case DBConstants.SQLDeadlockErrorCode: //1205
                        log.Warn("GetDataSetISMSql was deadlocked, will try again.");
                        break;
                    case DBConstants.SQLTimeoutErrorCode: //-2
                        log.Warn("GetDataSetISMSql was timedout, will try again.");
                        break;
                    default:
                        throw;
                }

                System.Threading.Thread.Sleep(1000); //Can also use Math.Rand for a random interval of time
                return GetDataSetISMSql(sql, tableName, usr, ++retryCount);
            }

        }
        public static DataSetISM GetDataSetMTSql(string sql, UserInfo usr, int retryCount = 1)
        {
            var ds = new DataSetISM();

            var cmd = QueryConstructor.GetCommand(sql, CommandType.Text, usr);
            var da = new SqlDataAdapter(cmd);
            if (ConfigurationManager.AppSettings["commandTimeout"] != null)
                cmd.CommandTimeout = int.Parse(ConfigurationManager.AppSettings["commandTimeout"]);
            try
            {
                da.Fill(ds);
                return ds;
            }
            catch (SqlException sqlEx)
            {
                if (retryCount == MAX_RETRY_COUNT) //5, 7, Whatever
                {
                    log.Error("Unable to AddDataTable, reached maximum number of retries.");
                    throw;
                }

                switch (sqlEx.Number)
                {
                    case DBConstants.SQLDeadlockErrorCode: //1205
                        log.Warn("GetDataSetMTSql was deadlocked, will try again.");
                        break;
                    case DBConstants.SQLTimeoutErrorCode: //-2
                        log.Warn("GetDataSetMTSql was timedout, will try again.");
                        break;
                    default:
                        throw;
                }

                System.Threading.Thread.Sleep(1000); //Can also use Math.Rand for a random interval of time
                return GetDataSetMTSql(sql, usr, ++retryCount);
            }

        }
        //метод с поддержкой транзакций
        public static DataSetISM GetDataSetISMSql(string sql, string tableName, SqlConnection conn, SqlTransaction trans, int retryCount = 1)
        {
            var ds = new DataSetISM();

            var cmd = QueryConstructor.GetCommand(sql, CommandType.Text, conn, trans);
            var da = new SqlDataAdapter(cmd);
            if (ConfigurationManager.AppSettings["commandTimeout"] != null)
                cmd.CommandTimeout = int.Parse(ConfigurationManager.AppSettings["commandTimeout"]);

            if (string.IsNullOrEmpty(tableName)) tableName = "table";
            try
            {
                da.Fill(ds, tableName);
                return ds;
            }
            catch (SqlException sqlEx)
            {
                if (retryCount == MAX_RETRY_COUNT) //5, 7, Whatever
                {
                    log.Error("Unable to GetDataSetISMSql, reached maximum number of retries.");
                    throw;
                }

                switch (sqlEx.Number)
                {
                    case DBConstants.SQLDeadlockErrorCode: //1205
                        log.Warn("GetDataSetISMSql was deadlocked, will try again.");
                        break;
                    case DBConstants.SQLTimeoutErrorCode: //-2
                        log.Warn("GetDataSetISMSql was timedout, will try again.");
                        break;
                    default:
                        throw;
                }

                System.Threading.Thread.Sleep(1000); //Can also use Math.Rand for a random interval of time
                return GetDataSetISMSql(sql, tableName, conn, trans, ++retryCount);
            }

        }
        public static DataSetISM GetDataSetMTSql(string sql, SqlConnection conn, SqlTransaction trans, int retryCount = 1)
        {
            var ds = new DataSetISM();

            var cmd = QueryConstructor.GetCommand(sql, CommandType.Text, conn, trans);
            var da = new SqlDataAdapter(cmd);
            if (ConfigurationManager.AppSettings["commandTimeout"] != null)
                cmd.CommandTimeout = int.Parse(ConfigurationManager.AppSettings["commandTimeout"]);
            try
            {
                da.Fill(ds);
                return ds;
            }
            catch (SqlException sqlEx)
            {
                if (retryCount == MAX_RETRY_COUNT) //5, 7, Whatever
                {
                    log.Error("Unable to GetDataSetMTSql, reached maximum number of retries.");
                    throw;
                }

                switch (sqlEx.Number)
                {
                    case DBConstants.SQLDeadlockErrorCode: //1205
                        log.Warn("GetDataSetMTSql was deadlocked, will try again.");
                        break;
                    case DBConstants.SQLTimeoutErrorCode: //-2
                        log.Warn("GetDataSetMTSql was timedout, will try again.");
                        break;
                    default:
                        throw;
                }

                System.Threading.Thread.Sleep(1000); //Can also use Math.Rand for a random interval of time
                return GetDataSetMTSql(sql, conn, trans, ++retryCount);
            }

        }
        public static DataSetISM GetDataSetISMSql(string sql, string tableName, Hashtable prms, bool isStoredProc, UserInfo usr, int retryCount = 1)
        {
            var ds = new DataSetISM();

            var cmd = QueryConstructor.GetCommand(sql, CommandType.Text, usr);
            if (isStoredProc) cmd.CommandType = CommandType.StoredProcedure;
            if (prms != null)
            {
                if (isStoredProc && sql.Trim().ToLower() == "sp_executesql")
                {
                    FillParamExecSql(cmd, prms);
                }
                else
                {
                    foreach (DictionaryEntry e in prms)
                    {
                        cmd.Parameters.AddWithValue((string)e.Key, e.Value ?? DBNull.Value);
                    }
                }
            }
            var da = new SqlDataAdapter(cmd);
            if (ConfigurationManager.AppSettings["commandTimeout"] != null)
                cmd.CommandTimeout = int.Parse(ConfigurationManager.AppSettings["commandTimeout"]);

            if (string.IsNullOrEmpty(tableName)) tableName = "table";
            try
            {
                da.Fill(ds, tableName);
                return ds;
            }
            catch (SqlException sqlEx)
            {
                if (retryCount == MAX_RETRY_COUNT) //5, 7, Whatever
                {
                    log.Error("Unable to GetDataSetISMSql, reached maximum number of retries.");
                    throw;
                }

                switch (sqlEx.Number)
                {
                    case DBConstants.SQLDeadlockErrorCode: //1205
                        log.Warn("GetDataSetISMSql was deadlocked, will try again.");
                        break;
                    case DBConstants.SQLTimeoutErrorCode: //-2
                        log.Warn("GetDataSetISMSql was timedout, will try again.");
                        break;
                    default:
                        throw;
                }

                System.Threading.Thread.Sleep(1000); //Can also use Math.Rand for a random interval of time
                return GetDataSetISMSql(sql, tableName, prms, isStoredProc, usr, ++retryCount);
            }

        }
        public static DataSetISM GetDataSetMTSql(string sql, Hashtable prms, bool isStoredProc, UserInfo usr, int retryCount = 1)
        {
            var ds = new DataSetISM();

            var cmd = QueryConstructor.GetCommand(sql, CommandType.Text, usr);
            if (isStoredProc) cmd.CommandType = CommandType.StoredProcedure;
            if (prms != null)
            {
                if (isStoredProc && sql.Trim().ToLower() == "sp_executesql")
                {
                    FillParamExecSql(cmd, prms);
                }
                else
                {
                    foreach (DictionaryEntry e in prms)
                    {
                        cmd.Parameters.AddWithValue((string)e.Key, e.Value ?? DBNull.Value);
                    }
                }
            }
            var da = new SqlDataAdapter(cmd);
            if (ConfigurationManager.AppSettings["commandTimeout"] != null)
                cmd.CommandTimeout = int.Parse(ConfigurationManager.AppSettings["commandTimeout"]);

            try
            {
                da.Fill(ds);
                return ds;
            }
            catch (SqlException sqlEx)
            {
                if (retryCount == MAX_RETRY_COUNT) //5, 7, Whatever
                {
                    log.Error("Unable to GetDataSetMTSql, reached maximum number of retries.");
                    throw;
                }

                switch (sqlEx.Number)
                {
                    case DBConstants.SQLDeadlockErrorCode: //1205
                        log.Warn("GetDataSetMTSql was deadlocked, will try again.");
                        break;
                    case DBConstants.SQLTimeoutErrorCode: //-2
                        log.Warn("GetDataSetMTSql was timedout, will try again.");
                        break;
                    default:
                        throw;
                }

                System.Threading.Thread.Sleep(1000); //Can also use Math.Rand for a random interval of time
                return GetDataSetMTSql(sql, prms, isStoredProc, usr, ++retryCount);
            }
        }

        public static DataSetISM GetDataSetISMSql(string sql, string tableName, Hashtable prms, bool isStoredProc, SqlConnection conn, SqlTransaction trans, int retryCount = 1)
        {
            var ds = new DataSetISM();

            var cmd = QueryConstructor.GetCommand(sql, CommandType.Text, conn, trans);
            if (isStoredProc) cmd.CommandType = CommandType.StoredProcedure;
            if (prms != null)
            {
                if (isStoredProc && sql.Trim().ToLower() == "sp_executesql")
                {
                    FillParamExecSql(cmd, prms);
                }
                else
                {
                    foreach (DictionaryEntry e in prms)
                    {
                        cmd.Parameters.AddWithValue((string)e.Key, e.Value ?? DBNull.Value);
                    }
                }
            }
            var da = new SqlDataAdapter(cmd);
            if (ConfigurationManager.AppSettings["commandTimeout"] != null)
                cmd.CommandTimeout = int.Parse(ConfigurationManager.AppSettings["commandTimeout"]);

            if (string.IsNullOrEmpty(tableName)) tableName = "table";

            try
            {
                da.Fill(ds, tableName);
                return ds;
            }
            catch (SqlException sqlEx)
            {
                if (retryCount == MAX_RETRY_COUNT) //5, 7, Whatever
                {
                    log.Error("Unable to GetDataSetISMSql, reached maximum number of retries.");
                    throw;
                }

                switch (sqlEx.Number)
                {
                    case DBConstants.SQLDeadlockErrorCode: //1205
                        log.Warn("GetDataSetISMSql was deadlocked, will try again.");
                        break;
                    case DBConstants.SQLTimeoutErrorCode: //-2
                        log.Warn("GetDataSetISMSql was timedout, will try again.");
                        break;
                    default:
                        throw;
                }

                System.Threading.Thread.Sleep(1000); //Can also use Math.Rand for a random interval of time
                return GetDataSetISMSql(sql, tableName, prms, isStoredProc, conn, trans, ++retryCount);
            }
        }
        public static DataSetISM GetDataSetMTSql(string sql, Hashtable prms, bool isStoredProc, SqlConnection conn, SqlTransaction trans, int retryCount = 1)
        {
            var ds = new DataSetISM();

            var cmd = QueryConstructor.GetCommand(sql, CommandType.Text, conn, trans);
            if (isStoredProc) cmd.CommandType = CommandType.StoredProcedure;
            if (prms != null)
            {
                if (isStoredProc && sql.Trim().ToLower() == "sp_executesql")
                {
                    FillParamExecSql(cmd, prms);
                }
                else
                {
                    foreach (DictionaryEntry e in prms)
                    {
                        cmd.Parameters.AddWithValue((string)e.Key, e.Value ?? DBNull.Value);
                    }
                }
            }
            var da = new SqlDataAdapter(cmd);
            if (ConfigurationManager.AppSettings["commandTimeout"] != null)
                cmd.CommandTimeout = int.Parse(ConfigurationManager.AppSettings["commandTimeout"]);
            try
            {
                da.Fill(ds);
                return ds;
            }
            catch (SqlException sqlEx)
            {
                if (retryCount == MAX_RETRY_COUNT) //5, 7, Whatever
                {
                    log.Error("Unable to GetDataSetMTSql, reached maximum number of retries.");
                    throw;
                }

                switch (sqlEx.Number)
                {
                    case DBConstants.SQLDeadlockErrorCode: //1205
                        log.Warn("GetDataSetMTSql was deadlocked, will try again.");
                        break;
                    case DBConstants.SQLTimeoutErrorCode: //-2
                        log.Warn("GetDataSetMTSql was timedout, will try again.");
                        break;
                    default:
                        throw;
                }

                System.Threading.Thread.Sleep(1000); //Can also use Math.Rand for a random interval of time
                return GetDataSetMTSql(sql, prms, isStoredProc, conn, trans, ++retryCount);
            }

        }

        public static void AddParam(Hashtable prms, XmlNode n)
        {
            var type = n.Attributes["type"].Value;
            var name = n.Attributes["name"].Value;
            var value = n.Attributes["value"].Value;
            object param;
            switch (type)
            {
                case "System.Guid":
                    param = new Guid(value);
                    break;
                case "System.Int32":
                    param = Convert.ToInt32(value);
                    break;
                case "System.String":
                    param = value;
                    break;
                case "System.Decimal":
                    param = Convert.ToDecimal(value, CultureInfo.InvariantCulture.NumberFormat);
                    break;
                case "System.Double":
                    param = Convert.ToDouble(value, CultureInfo.InvariantCulture.NumberFormat);
                    break;
                case "System.Single":
                    param = Convert.ToSingle(value, CultureInfo.InvariantCulture.NumberFormat);
                    break;
                case "System.DateTime":
                    param = Convert.ToDateTime(value, DateTimeFormat);
                    break;
                case "System.Boolean":
                    param = (value != "0");
                    break;
                default:
                    throw new NotSupportedException();
            }
            prms[name] = param;
        }
        public static DateTimeFormatInfo DateTimeFormat
        {
            get
            {
                var dtfi = new CultureInfo("ru-RU").DateTimeFormat;
                dtfi.ShortDatePattern = "dd/MM/yyyy";
                dtfi.LongDatePattern = "dd/MM/yyyy HH:mm:ss";
                return dtfi;
            }
        }

        public static DataSet GetTableTemplate(Guid objectOID, Guid TableTemplate)
        {
            const string firstColumnName = "rows_names";
            //			string lastColumnName = "rows_OrdValue";
            var dsResult = new DataSet();
            //1 Получаем данные из базы (колонки, строки, значения)
            //1.1 получаем колонки
            var sql = String.Format(@"SELECT ColOrdValue,Name,postfix FROM t_tableTemplateColumns WHERE OID='{0}'' ORDER BY ColOrdValue", TableTemplate);
            var dsColumns = GetDataSetISMSql(sql, null, null);

            //1.2 получаем строки
            sql = String.Format(@"SELECT RowOrdValue,Name FROM t_tableTemplateRows WHERE OID='{0}'' ORDER BY RowOrdValue", TableTemplate);
            var dsRows = GetDataSetISMSql(sql, null, null);

            //1.3 получаем значения
            sql = String.Format(@"SELECT RowOrdValue,ColOrdValue,Value FROM t_GoodsTableValues WHERE OID='{0}'' AND tableTemplateOID=''{1}''", objectOID, TableTemplate);
            var dsValues = GetDataSetISMSql(sql, null, null);

            //если нет строк или колонок в шаблоне
            if (dsColumns.Table.Rows.Count == 0 || dsRows.Table.Rows.Count == 0)
                return null;

            //2 Создадим структуру таблицы и заполним данными
            var dt = new DataTable("table");

            //2.1 добавим служебную ReadOnly колонку для имён строк
            {
                var dc = new DataColumn(firstColumnName, typeof(string));
                dt.Columns.Add(dc);
            }

            //2.2 добавим колонки
            foreach (DataRow dr in dsColumns.Table.Rows)
            {
                var dc = new DataColumn(dr["ColOrdValue"].ToString(), typeof(string)) { Caption = dr["Name"].ToString() };
                dt.Columns.Add(dc);
            }
            dsResult.Tables.Add(dt);

            //2.3 добавим служебную ReadOnly колонку для RowOrdValue
            //		{
            //			DataColumn dc = new DataColumn(lastColumnName,typeof(int));
            //			dt.Columns.Add(dc);

            //			//установим первичный ключ на колонку RowOrdValue
            //			dt.PrimaryKey = new DataColumn[]{dc};
            //		}

            //2.4 добавим строки
            foreach (DataRow dr in dsRows.Table.Rows)
            {
                var newdr = dsResult.Tables[0].NewRow();
                newdr[firstColumnName] = dr["name"].ToString();
                //				newdr[lastColumnName] = (int)dr["RowOrdValue"];
                dsResult.Tables[0].Rows.Add(newdr);
            }

            //2.5 добавим данные
            bool hasData = false;
            foreach (DataRow dr in dsValues.Table.Rows)
            {
                var colName = (int)dr["ColOrdValue"];//они там подряд, поэтому можно так позиционироваться
                var postfix = dsColumns.Table.Rows[colName]["postFix"].ToString();
                //				DataRow drToChange = dsResult.Tables[0].Rows.Find((int)dr["RowOrdValue"]);
                var drToChange = dsResult.Tables[0].Rows[((int)dr["RowOrdValue"])];//они там подряд, поэтому можно так позиционироваться
                var cellValue = dr["Value"] + postfix;
                if (cellValue != "") hasData = true;
                drToChange[colName.ToString()] = dr["Value"] + postfix;
            }

            return hasData ? dsResult : null;
        }
        public static void UpdateBLOB(Guid OID, string className, string fieldName, int offset, string content)
        {
            var pi = (PropInfo)QueryConstructor.BuildMetadataInfo(className).FindProp(fieldName, 1);
            string sql = String.Format(@"
									 Declare @PtrDst binary(16)
									 SELECT @PtrDst = TEXTPTR([{0}]) FROM [{1}] WHERE OID = @OID
									 if @PtrDst is Null Begin 
										UPDATE [{1}] SET [{0}] = '''' WHERE OID = @OID\\\SELECT @PtrDst = TEXTPTR([{0}]) FROM [{1}] WHERE OID = @OID
									 End
									 UPDATETEXT [{1}].[{0}] @PtrDst @offset null 
									 ", pi.PropName, pi.TableName);
            if (pi.ColumnType == DataType.Binary)
            {
                var sb = new StringBuilder();
                foreach (byte b in Convert.FromBase64String(content))
                {
                    sb.Append(b.ToString("X2"));
                }
                sql = String.Format("{0}0x{1}", sql, sb);
            }
            else
            {
                sql = String.Format("{0}'{1}'", sql, content);
            }
            var prms = new Hashtable();
            prms["@OID"] = OID;
            prms["@offset"] = offset;
            ExecuteCommand(sql, prms, false);
        }
    }

    internal class DBConstants
    {
        public const int SQLDeadlockErrorCode = 1205;
        public const int SQLTimeoutErrorCode = -2;
    }
}
