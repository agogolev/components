using System;
using System.Collections;
using System.Data;
using System.Text;
using NLog;

namespace DBReader
{
	public class ObjectProvider
	{
		private static readonly Logger log = LogManager.GetCurrentClassLogger();
		public static string GetObject(string OID, string fieldNames, string multiProps, UserInfo usr)
		{
			string sql = null;
			try
			{
				var oid = new Guid(OID);
				sql = QueryConstructor.BuildGetObject(oid, null, fieldNames, multiProps, null);
				var cmd = QueryConstructor.GetCommand(sql, CommandType.Text, usr);
				cmd.Parameters.AddWithValue("@OID", oid);
				cmd.Connection.Open();
				var sb = new StringBuilder();
				sb.Append("<?xml version=\"1.0\" ?>");
				try
				{
					var dr = cmd.ExecuteReader();
					while (dr.Read())
					{
						sb.Append((string)dr[0]);
					}
				}
				finally
				{
					cmd.Connection.Close();
				}
				return sb.ToString();
			}
			catch (Exception e)
			{
				log.Error("sql = {0}\r\nerror: {1}", sql, e.Message);
				throw;
			}

		}
		public static string GetObject(string OID, string className, string fieldDescription)
		{
			string sql = null;
			try
			{
				var oid = new Guid(OID);
				sql = QueryConstructor.BuildGetObject(oid, className, fieldDescription);
				var cmd = QueryConstructor.GetCommand(sql, CommandType.Text);
				cmd.Parameters.AddWithValue("@OID", oid);
				cmd.Connection.Open();
				var sb = new StringBuilder();
				sb.Append("<?xml version=\"1.0\" ?>");
				try
				{
					var dr = cmd.ExecuteReader();
					while (dr.Read())
					{
						sb.Append((string)dr[0]);
					}
				}
				finally
				{
					cmd.Connection.Close();
				}
				return sb.ToString();
			}
			catch (Exception e)
			{
				log.Error("sql = {0}\r\nerror: {1}", sql, e.Message);
				throw;
			}

		}

		public static string GetObject(string OID, string fieldNames, string multiProps, UserInfo usr, string className)
		{
			string sql = null;
			try
			{
				var oid = new Guid(OID);
				sql = QueryConstructor.BuildGetObject(oid, className, fieldNames, multiProps, null);
				var cmd = QueryConstructor.GetCommand(sql, CommandType.Text, usr);
				cmd.Parameters.AddWithValue("@OID", oid);
				cmd.Connection.Open();
				var sb = new StringBuilder();
				sb.Append("<?xml version=\"1.0\" ?>");
				try
				{
					var dr = cmd.ExecuteReader();
					while (dr.Read())
					{
						sb.Append((string)dr[0]);
					}
				}
				finally
				{
					cmd.Connection.Close();
				}
				return sb.ToString();
			}
			catch (Exception e)
			{
				log.Error("sql = {0}\r\nerror: {1}", sql, e.Message);
				throw;
			}

		}

		public static string GetObjectDs(string OID, string fieldNames, string multiProps, UserInfo usr)
		{
			string sql = null;
			try
			{
				var oid = new Guid(OID);
				sql = QueryConstructor.BuildGetObject(oid, null, fieldNames, multiProps, null);
				var cmd = QueryConstructor.GetCommand(sql, CommandType.Text, usr);
				cmd.Parameters.AddWithValue("@OID", oid);
				cmd.Connection.Open();
				var sb = new StringBuilder();
				sb.Append("<?xml version=\"1.0\" ?>");
				try
				{
					var dr = cmd.ExecuteReader();
					while (dr.Read())
					{
						sb.Append((string)dr[0]);
					}
				}
				finally
				{
					cmd.Connection.Close();
				}
				return sb.ToString();
			}
			catch (Exception e)
			{
				log.Error("sql = {0}\r\nerror: {1}", sql, e.Message);
				throw;
			}

		}
		public static string GetObjectDs(string OID, string fieldNames, string multiProps, UserInfo usr, string className)
		{
			string sql = null;
			try
			{
				var oid = new Guid(OID);
				sql = QueryConstructor.BuildGetObject(oid, className, fieldNames, multiProps, null);
				var cmd = QueryConstructor.GetCommand(sql, CommandType.Text, usr);
				cmd.Parameters.AddWithValue("@OID", oid);
				cmd.Connection.Open();
				var sb = new StringBuilder();
				sb.Append("<?xml version=\"1.0\" ?>");
				try
				{
					var dr = cmd.ExecuteReader();
					while (dr.Read())
					{
						sb.Append((string)dr[0]);
					}
				}
				finally
				{
					cmd.Connection.Close();
				}
				return sb.ToString();
			}
			catch (Exception e)
			{
				log.Error("sql = {0}\r\nerror: {1}", sql, e.Message);
				throw;
			}

		}

		public static string DeleteObject(Guid OID, UserInfo usr)
		{
			var cmd = QueryConstructor.GetCommand("spDeleteObject", CommandType.StoredProcedure, usr);
			cmd.Parameters.AddWithValue("@OID", OID);
			cmd.Connection.Open();
			try
			{
				cmd.ExecuteNonQuery();
			}
			catch (Exception e)
			{
				return e.Message;
			}
			finally
			{
				cmd.Connection.Close();
			}
			return "";
		}

		public static String GetMultiCollection(string OID, string propName, string content, string className)
		{
			var oid = new Guid(OID);
			if (string.IsNullOrEmpty(className))
			{
				var prms = new Hashtable();
				prms["@OID"] = oid;
				className = (string)DBHelper.ExecuteScalar(@"SELECT dbo.className(@OID)", prms, false);
			}
			var ca = new CollectionAdapter(OID, className, propName, content);
			try
			{
				return ca.GetDataSet().SaveToString();
			}
			catch (Exception e)
			{
				return e.Message;
			}
		}
		public static MetaData.DataSetISM GetMultiDataSet(Guid OID, string propName, string content, string className)
		{
			if (string.IsNullOrEmpty(className))
			{
				var prms = new Hashtable();
				prms["@OID"] = OID;
				className = (string)DBHelper.ExecuteScalar(@"SELECT dbo.className(@OID)", prms, false);
			}
			var ca = new CollectionAdapter(OID.ToString(), className, propName, content);
			return ca.GetDataSet();
		}

		public static string ProcessCollection(string className, string propName, string processContent, string OID)
		{
			if (string.IsNullOrEmpty(className))
			{
				var prms = new Hashtable();
				prms["@OID"] = new Guid(OID);
				className = (string)DBHelper.ExecuteScalar(@"SELECT dbo.className(@OID)", prms, true);
			}
			var ca = new CollectionAdapter(OID, className, propName, processContent);
			ca.AcceptChanges();
			return "";
		}

	}
}

