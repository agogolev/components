using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml;
using System.Text;
using System.Linq;
using System.Collections;
using MetaData;
using System.Xml.Linq;

namespace DBReader
{
	/// <summary>
	/// Summary description for DBReader.
	/// </summary>
	public class QueryConstructor
	{
		//метод без поддержки транзакций
		public static SqlCommand GetCommand(string sql, CommandType type, UserInfo user)
		{
			var cmd = new SqlCommand(sql, new SqlConnection(GetConnectionStr(user))) {CommandType = type};
			return cmd;
		}
		//метод без поддержки транзакций
		public static SqlCommand GetCommand(string sql, CommandType type)
		{
			var cmd = new SqlCommand(sql,
			                         new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["dbdata.connection"]))
			          	{CommandType = type};
			return cmd;
		}
		//метод с поддержкой транзакций
		public static SqlCommand GetCommand(string sql, CommandType type, SqlConnection conn, SqlTransaction trans)
		{
			var cmd = new SqlCommand(sql, conn, trans) {CommandType = type};
			return cmd;
		}
		public static SqlDataAdapter GetDataAdapter(string sql, UserInfo user)
		{
			var da = new SqlDataAdapter(sql, new SqlConnection(GetConnectionStr(user)));
			return da;
		}
		public static string GetConnectionStr(UserInfo user)
		{
			var connStr = System.Configuration.ConfigurationManager.AppSettings["dbdata.connection"];
			/*if (user != null) {
				Regex passw = new Regex("password=(.*);", RegexOptions.IgnoreCase);
				Regex.Replace(connStr, "uid=(.*);", "uid="+user.UserRole+";", RegexOptions.IgnoreCase);
				Regex.Replace(connStr, "password=(.*);", "password="+user.UserRole+";", RegexOptions.IgnoreCase);
			}*/
			return connStr;
		}
		public static string BuildQueryObjects(string className, int numInBatch, int pageNum, string addFields, FilterCollection filter, OrderInfo[] order, UserInfo user)
		{
			return BuildQueryObjects(className, numInBatch, pageNum, "", addFields, filter, order, user, true);
		}
		public static string BuildQueryObjects(string className, int numInBatch, int pageNum, string keyFields, string addFields, FilterCollection filter, OrderInfo[] order, UserInfo user)
		{
			return BuildQueryObjects(className, numInBatch, pageNum, keyFields, addFields, filter, order, user, true);
		}
		public static string BuildQueryObjects(string className, int numInBatch, int pageNum, string keyFields, string addFields, FilterCollection filter, OrderInfo[] order, UserInfo user, bool showPaging)
		{
			var info = BuildMetadataInfo(className);
			return InnerBuildQueryObjects(info, numInBatch, pageNum, keyFields, addFields, filter, order);
		}
		public static string BuildGetMulti(int OIDCount, string propName, string className, string multiClass, int numInBatch, int pageNum, string addFields, FilterCollection filter, OrderInfo[] order, UserInfo user)
		{
			var info = BuildMetadataInfo(className);
			return InnerBuildGetMulti(info, OIDCount, propName, multiClass, numInBatch, pageNum, addFields, filter, order);
		}
		public static string BuildGetObject(Guid OID, string className, string fieldNames, string multiProps, UserInfo user)
		{
			var info = string.IsNullOrEmpty(className) ? BuildMetadataInfo(OID) : BuildMetadataInfo(className);
			if (info == null) throw new Exception("class not found");
			return InnerBuildGetObject(info, fieldNames, multiProps);
		}
		public static string BuildGetObject(Guid OID, string className, string fieldNames)
		{
			var info = string.IsNullOrEmpty(className) ? BuildMetadataInfo(OID) : BuildMetadataInfo(className);
			if (info == null) throw new Exception("class not found");
			return InnerBuildGetObject(info, fieldNames);
		}
		public static string BuildGetObjectDS(Guid OID, string className, string fieldNames, UserInfo user)
		{
			var info = string.IsNullOrEmpty(className) ? BuildMetadataInfo(OID) : BuildMetadataInfo(className);
			if (info == null) throw new Exception("class not found");
			return InnerBuildGetObjectDS(info, fieldNames);
		}
		public static MetadataInfo BuildMetadataInfo(Guid OID)
		{
			var prms = new Hashtable();
			prms["@OID"] = OID;
			var className = (string)DBHelper.ExecuteScalar(@"
SELECT c.className 
FROM t_Object o
	inner join t_className c on o.CID = c.CID
WHERE o.OID = @OID", prms, false);
			return BuildMetadataInfo(className);
		}
		public static MetadataInfo BuildMetadataInfo(string className)
		{
			MetadataInfo result = null;
			if (HttpContext.Current != null)
				result = (MetadataInfo)HttpContext.Current.Cache["MetaDataInfo_" + className];
			if (result != null) return result;
			result = new MetadataInfo();
			var cmd = GetCommand("spMetadataInfo", CommandType.StoredProcedure, null);
			cmd.Parameters.AddWithValue("@className", className);
			cmd.Connection.Open();
			try
			{
				var dr = cmd.ExecuteReader();
				var sb = new StringBuilder();
				sb.Append("<?xml version=\"1.0\"?><root>");
				try
				{
					while (dr.Read())
					{
						sb.Append((string)dr[0]);
					}
				}
				finally
				{
					sb.Append("</root>");
					dr.Close();
				}
				var doc = new XmlDocument();
				doc.LoadXml(sb.ToString());
				var root = doc.DocumentElement;
				var nl = root.SelectNodes("class");
				result.Classes = new ClassInfo[nl.Count];
				for (var i = 0; i < nl.Count; i++)
				{
					var n = nl[i];
					var ci = new ClassInfo();
					result.Classes[i] = ci;
					ci.CID = new Guid(n.Attributes["CID"].Value);
					ci.ClassName = n.SelectSingleNode("className").InnerText;
					ci.TableName = n.SelectSingleNode("tableName").InnerText;
					var nl1 = n.SelectNodes("prop");
					if (nl1.Count != 0)
					{
						ci.Fields = new PropInfo[nl1.Count];
						for (var k = 0; k < nl1.Count; k++)
						{
							var n1 = nl1[k];
							var pi = new PropInfo();
							ci.Fields[k] = pi;
							pi.PropName = n1.SelectSingleNode("columnName").InnerText;
							pi.TableName = ci.TableName;
							var dt = n1.Attributes["dataType"].Value;
							if (n1.Attributes["characterLength"] != null && dt != "text" && dt != "image") pi.MaximumLength = Convert.ToInt16(n1.Attributes["characterLength"].Value);
							pi.ColumnType = GetDataType(dt);
							if (n1.Attributes["isRefer"].Value != "0") pi.IsRefer = true;
						}
					}
					nl1 = n.SelectNodes("multiProp");
					if (nl1.Count != 0)
					{
						ci.MultiFields = new MultiPropInfo[nl1.Count];
						for (var k = 0; k < nl1.Count; k++)
						{
							var n1 = nl1[k];
							var mpi = new MultiPropInfo {ParentFieldName = "OID", ParentTableName = ci.TableName};
							ci.MultiFields[k] = mpi;
							if (n1.Attributes["withOrder"].Value == "1") mpi.WithOrder = true;
							if (n1.Attributes["isPersistent"].Value == "1") mpi.IsPersistent = true;
							if (n1.Attributes["one2many"].Value == "1") mpi.One2Many = true;
							if (mpi.One2Many)
							{
								var n3 = n1.SelectSingleNode("className");
								mpi.ClassName = n3.InnerText;
								mpi.CID = new Guid(n3.Attributes["CID"].Value);
							}
							mpi.PropName = n1.SelectSingleNode("propName").InnerText;
							mpi.TableName = n1.SelectSingleNode("tableName").InnerText;
							mpi.FieldOID = n1.SelectSingleNode("fieldOID").InnerText;
							mpi.FieldProp = new PropInfo();
							var n2 = n1.SelectSingleNode("fieldName");
							if (n2 == null) throw new Exception("error in MultiProp: " + mpi.PropName);
							mpi.FieldProp.PropName = n2.InnerText;
							var dt = n2.Attributes["dataType"].Value;
							if (n2.Attributes["characterLength"] != null && dt != "text" && dt != "image") mpi.FieldProp.MaximumLength = Convert.ToInt16(n2.Attributes["characterLength"].Value);
							mpi.FieldProp.ColumnType = GetDataType(dt);
							if (n2.Attributes["isRefer"].Value != "0") mpi.FieldProp.IsRefer = true;
							if (!mpi.WithOrder) continue;
							mpi.FieldOrd = new PropInfo();
							n2 = n1.SelectSingleNode("ordField");
							mpi.FieldOrd.PropName = n2.InnerText;
							dt = n2.Attributes["dataType"].Value;
							if (n2.Attributes["characterLength"] != null && dt != "text" && dt != "image") mpi.FieldOrd.MaximumLength = Convert.ToInt16(n2.Attributes["characterLength"].Value);
							mpi.FieldOrd.ColumnType = GetDataType(dt);
						}
					}
					nl1 = n.SelectNodes("lookupProp");
					if (nl1.Count != 0)
					{
						ci.LookupFields = new LookupPropInfo[nl1.Count];
						for (var k = 0; k < nl1.Count; k++)
						{
							var n1 = nl1[k];
							var lpi = new LookupPropInfo();
							ci.LookupFields[k] = lpi;
							lpi.PropName = n1.SelectSingleNode("propName").InnerText;
							lpi.TableName = n1.SelectSingleNode("tableName").InnerText;
							lpi.FieldName = n1.SelectSingleNode("fieldName").InnerText;
							lpi.FieldID = n1.SelectSingleNode("fieldID").InnerText;
							lpi.ParentFieldName = lpi.FieldID;
							lpi.ParentTableName = ci.TableName;
						}
					}
				}
			}
			finally
			{
				cmd.Connection.Close();
				if (HttpContext.Current != null)
					HttpContext.Current.Cache.Insert("MetaDataInfo_" + className, result, null, DateTime.MaxValue, TimeSpan.FromMinutes(20));
			}
			return result;
		}
		public static MetadataInfo BuildMetadataByTable(string tableName, UserInfo user)
		{
			string className;
			var cmd = GetCommand(@"
SELECT c.className 
FROM t_className c 
WHERE c.tableName = @tableName", CommandType.Text, user);
			cmd.Parameters.AddWithValue("@tableName", tableName);
			cmd.Connection.Open();
			try
			{
				className = (string)cmd.ExecuteScalar();
				if (className == null) throw new Exception("class not found tableName = " + tableName);
			}
			finally
			{
				cmd.Connection.Close();
			}
			return BuildMetadataInfo(className);
		}
		public static MetadataInfo BuildTableData(string tableName, UserInfo user)
		{
			MetadataInfo result = null;
			if (HttpContext.Current != null)
				result = (MetadataInfo)HttpContext.Current.Cache["TableData_" + tableName];
			if (result != null) return result;
			result = new MetadataInfo {Classes = new ClassInfo[1]};
			var ci = new ClassInfo {ClassName = "", TableName = tableName};
			result.Classes[0] = ci;
			var cmd = GetCommand("spTableInfo", CommandType.StoredProcedure, user);
			cmd.Parameters.AddWithValue("@tableName", tableName);
			cmd.Connection.Open();
			try
			{
				var dr = cmd.ExecuteReader();
				var arr = new ArrayList();
				while (dr.Read())
				{
					var pi = new PropInfo {PropName = (string) dr["COLUMN_NAME"], TableName = ci.TableName};
					var dt = (string)dr["DATA_TYPE"];
					if (dr["CHARACTER_MAXIMUM_LENGTH"] != DBNull.Value && dt != "text" && dt != "image") pi.MaximumLength = (int)dr["CHARACTER_MAXIMUM_LENGTH"];
					pi.ColumnType = GetDataType(dt);
					if ((int)dr["isRefer"] != 0) pi.IsRefer = true;
					arr.Add(pi);
				}
				ci.Fields = new PropInfo[arr.Count];
				for (var i = 0; i < arr.Count; i++)
				{
					ci.Fields[i] = (PropInfo)arr[i];
				}
			}
			finally
			{
				cmd.Connection.Close();
				if (HttpContext.Current != null)
					HttpContext.Current.Cache.Insert("TableData_" + tableName, result, null, DateTime.MaxValue, TimeSpan.FromMinutes(20));
			}
			return result;
		}
		public static CollectionInfo BuildCollectionInfo(string className, string propName)
		{
			CollectionInfo res = null;
			if (HttpContext.Current != null)
				res = (CollectionInfo)HttpContext.Current.Cache["CollectionInfo_" + className.ToLower() + "_" + propName.ToLower()];
			if (res != null) return res;
			res = new CollectionInfo {ClassName = className, PropName = propName};
			var prms = new Hashtable();
			prms["@className"] = className;
			prms["@propName"] = propName;
			var elem = DBHelper.GetXmlData("spMultiPropInfo", prms, null, true);
			res.TableName = elem.Attribute("tableName").Value;
			res.FieldOID = elem.Attribute("fieldOID").Value;
			res.PrimaryKey = (from element in elem.Descendants("primaryKey")
													select element.Value).ToArray();

			res.Fields = (from element in elem.Descendants("columns")
			              select new PropInfo {
			                                  	PropName = element.Attribute("columnName").Value,
			                                  	TableName = res.TableName,
			                                  	ColumnType = GetDataType(element.Attribute("dataType").Value),
			                                  	MaximumLength = GetMaximumLength(element.Attribute("charMaxLength"), element.Attribute("dataType").Value),
			                                  	IsRefer = element.Attribute("isRefer").Value != "0"
			                                  }).ToArray();
		
			
			if (HttpContext.Current != null)
				HttpContext.Current.Cache.Insert("CollectionInfo_" + className.ToLower() + "_" + propName.ToLower(), res, null, DateTime.MaxValue, TimeSpan.FromMinutes(20));
			return res;
		}
		public static TableInfo BuildTableInfo(string tableName)
		{
			TableInfo res = null;
			if (HttpContext.Current != null)
				res = (TableInfo)HttpContext.Current.Cache["TableInfo_" + tableName.ToLower()];
			if (res != null) return res;
			res = new TableInfo();
			var prms = new Hashtable();
			prms["@tableName"] = tableName;
			var elem = DBHelper.GetXmlData("spTableDescription", prms, null, true);
			res.TableName = elem.Attribute("tableName").ToString();
			res.PrimaryKey = (from element in elem.Descendants("primaryKey")
												select element.Value).ToArray();

			res.Fields = (from element in elem.Descendants("columns")
			              select new PropInfo
			                     	{
			                     		PropName = element.Attribute("columnName").Value,
			                     		TableName = res.TableName,
			                     		ColumnType = GetDataType(element.Attribute("dataType").Value),
			                     		MaximumLength = GetMaximumLength(element.Attribute("charMaxLength"), element.Attribute("dataType").Value),
			                     		IsRefer = element.Attribute("isRefer").Value != "0"
			                     	}).ToArray();
		
			if (HttpContext.Current != null)
				HttpContext.Current.Cache.Insert("TableInfo_" + tableName.ToLower(), res, null, DateTime.MaxValue, TimeSpan.FromMinutes(20));
			return res;
		}
		public static void BuildForeignRelationInfo(RelationInfo relation, UserInfo user)
		{
			RelationInfo result = null;
			if (HttpContext.Current != null)
				result = (RelationInfo)HttpContext.Current.Cache["RelationInfo_" + relation.ParentTable + "_" + relation.ParentField];
			if (result != null)
			{
				relation.ChildClassName = result.ChildClassName;
				relation.ChildField = result.ChildField;
				relation.ChildTable = result.ChildTable;
				return;
			}
			result = relation;
			SqlCommand cmd = GetCommand("spRelationInfo", CommandType.StoredProcedure, user);
			cmd.Parameters.AddWithValue("@tableName", result.ParentTable);
			cmd.Parameters.AddWithValue("@foreingColumnName", result.ParentField);
			cmd.Connection.Open();
			try
			{
				SqlDataReader dr = cmd.ExecuteReader();
				if (dr.Read())
				{
					result.ChildClassName = dr["className"] != DBNull.Value ? (string)dr["className"] : null;
					result.ChildTable = (string)dr["TABLE_NAME"];
					result.ChildField = (string)dr["COLUMN_NAME"];
				}
				else
				{
					throw new Exception("ForeignRelation not defined for table " + relation.ParentTable + " and column " + relation.ParentField);
				}
			}
			finally
			{
				cmd.Connection.Close();
				if (HttpContext.Current != null)
					HttpContext.Current.Cache.Insert("RelationInfo_" + relation.ParentTable + "_" + relation.ParentField, result, null, DateTime.MaxValue, TimeSpan.FromMinutes(20));
			}
		}
		private static DataType GetDataType(string dt)
		{
			switch (dt)
			{
				case "uniqueidentifier":
					return DataType.Uniqueidentifier;
				case "varchar":
				case "char":
					return DataType.String;
				case "int":
				case "bigint":
				case "tinyint":
					return DataType.Int;
				case "datetime":
					return DataType.DateTime;
				case "bit":
					return DataType.Boolean;
				case "money":
				case "decimal":
					return DataType.Money;
				case "float":
					return DataType.Float;
				case "image":
					return DataType.Binary;
				default:
					return DataType.String;
			}
		}
		private static int GetMaximumLength(XAttribute elem, string dt)
		{
			if (elem != null && dt != "image" && dt != "text") return int.Parse(elem.Value);
			return 0;
		}

		private static string InnerBuildGetObject(MetadataInfo info, string fieldNames, string multiProps)
		{
			string sql = null;
			var key = "ObjectSQL_" + info.Classes[0].CID + (fieldNames ?? "") + (multiProps ?? "");
			if (HttpContext.Current != null)
			{
				sql = (string)HttpContext.Current.Cache[key];
			}
			if (sql != null) return sql;

			bool hasBinaryField = false;
			if (fieldNames != null && fieldNames.IndexOf("<") != -1) // признак наличи¤ xml документа
			{
				sql = InnerBuildGetObjectExt(info, fieldNames);
			}
			else
			{
				var sbSelect = new StringBuilder();
				string[] fields = null;
				string[] multies = null;
				if (!string.IsNullOrEmpty(fieldNames) && fieldNames.ToUpper().IndexOf("CID") == -1) fieldNames += ",CID";
				if (fieldNames != null) fields = fieldNames.ToLower().Split(",; ".ToCharArray());
				if (multiProps != null) multies = multiProps.ToLower().Split(",; ".ToCharArray());
				var search = new Hashtable();
				var searchMulty = new Hashtable();
				var showAll = (fieldNames != null);
				var showAllMulties = (multiProps != null);
				if (fields != null)
				{
					foreach (var field in fields.Where(field => field != "" && field != "oid"))
					{
						search[field] = true;
						showAll = false;
					}
				}
				if (multies != null)
				{
					foreach (var field in multies.Where(field => field != ""))
					{
						searchMulty[field] = true;
						showAllMulties = false;
					}
				}
				sbSelect.Append(@"
SELECT
	Tag = 1,
	Parent = null,
	[item!1!OID] = [t_Object].[OID]");
				var i = 1;
				foreach (var ci in info.Classes)
				{
					foreach (var pi in ci.Fields)
					{
						if (pi.PropName.ToLower() != "oid" && (showAll || search.ContainsKey(pi.PropName.ToLower())))
						{
							if (pi.ColumnType == DataType.Binary) hasBinaryField = true;
							//propName
							sbSelect.Append(@",
	[item!1!");
							sbSelect.Append(pi.PropName);
							sbSelect.Append("!element");
							sbSelect.Append("] = ");
							sbSelect.Append("[" + ci.TableName + "].[" + pi.PropName + "]");
							if (pi.ColumnType == DataType.Uniqueidentifier && pi.IsRefer)
							{
								//NK
								sbSelect.Append(@",
	[item!1!");
								sbSelect.Append(pi.PropName + "_NK!element] = ");
								sbSelect.Append("dbo.NK([" + ci.TableName + "].[" + pi.PropName + "])");
								//className
								sbSelect.Append(@",
	[item!1!");
								sbSelect.Append(pi.PropName + "_className!element] = ");
								sbSelect.Append("dbo.className([" + ci.TableName + "].[" + pi.PropName + "])");
							}
						}
					}
					if (ci.MultiFields != null)
					{
						foreach (MultiPropInfo pi in ci.MultiFields)
						{
							if (showAllMulties || searchMulty.ContainsKey(pi.PropName.ToLower()))
							{
								i++;
								if (pi.WithOrder)
								{
									sbSelect.Append(@",
	[" + pi.PropName + "!" + i.ToString() + "!ordValue!element] = null");
									if (pi.FieldOrd.ColumnType == DataType.Uniqueidentifier)
									{
										//NK
										sbSelect.Append(@",
	[" + pi.PropName + "!" + i.ToString() + "!ordValue_NK!element] = null");
										//className
										sbSelect.Append(@",
	[" + pi.PropName + "!" + i.ToString() + "!ordValue_className!element] = null");
									}
								}
								sbSelect.Append(@",
	[" + pi.PropName + "!" + i.ToString() + "!propValue!element] = null");
								if (pi.FieldProp.ColumnType == DataType.Uniqueidentifier && pi.FieldProp.IsRefer)
								{
									//NK
									sbSelect.Append(@",
	[" + pi.PropName + "!" + i.ToString() + "!propValue_NK!element] = null");
									//className
									sbSelect.Append(@",
	[" + pi.PropName + "!" + i.ToString() + "!propValue_className!element] = null");
								}
							}
						}
					}
				}
				sbSelect.Append(@"
FROM");
				string prevTable = "";
				foreach (ClassInfo ci in info.Classes)
				{
					if (prevTable == "")
					{
						sbSelect.Append(@"
	[" + ci.TableName + "]");
					}
					else
					{
						sbSelect.Append(@"
	inner join [" + ci.TableName + "] on [" + prevTable + "].[OID] = [" + ci.TableName + "].[OID]");
					}
					prevTable = ci.TableName;
				}
				sbSelect.Append(@"
WHERE 
	[t_Object].[OID] = @OID");
				i = 1;
				foreach (ClassInfo ci in info.Classes)
				{
					if (ci.MultiFields != null)
					{
						foreach (MultiPropInfo mpi in ci.MultiFields)
						{
							if (showAllMulties || searchMulty.ContainsKey(mpi.PropName.ToLower()))
							{
								i++;
								sbSelect.Append(@"
UNION ALL
SELECT
	" + i.ToString() + @",
	1,
	[" + ci.TableName + "].[OID]");
								foreach (ClassInfo ci1 in info.Classes)
								{
									foreach (PropInfo pi1 in ci1.Fields)
									{
										if (pi1.PropName.ToLower() != "oid" && (showAll || search.ContainsKey(pi1.PropName.ToLower())))
										{
											sbSelect.Append(@",
	null");
											if (pi1.ColumnType == DataType.Uniqueidentifier && pi1.IsRefer)
											{
												//NK
												sbSelect.Append(@",
	null");
												//className
												sbSelect.Append(@",
	null");
											}
										}
									}
									if (ci1 == ci)
									{
										if (ci1.MultiFields != null)
										{
											foreach (MultiPropInfo mpi1 in ci1.MultiFields)
											{
												if (showAllMulties || searchMulty.ContainsKey(mpi1.PropName.ToLower()))
												{
													if (mpi1 == mpi)
													{
														if (mpi1.WithOrder)
														{
															sbSelect.Append(@",
	[" + mpi1.TableName + "].[" + mpi1.FieldOrd.PropName + "]");
															if (mpi1.FieldOrd.ColumnType == DataType.Uniqueidentifier)
															{
																//NK
																sbSelect.Append(@",
	dbo.NK([" + mpi1.TableName + "].[" + mpi1.FieldOrd.PropName + "])");
																//className
																sbSelect.Append(@",
	dbo.className([" + mpi1.TableName + "].[" + mpi1.FieldOrd.PropName + "])");
															}
														}
														sbSelect.Append(@",
	[" + mpi1.TableName + "].[" + mpi1.FieldProp.PropName + "]");
														if (mpi1.FieldProp.ColumnType == DataType.Uniqueidentifier && mpi1.FieldProp.IsRefer)
														{
															//NK
															sbSelect.Append(@",
	dbo.NK([" + mpi1.TableName + "].[" + mpi1.FieldProp.PropName + "])");
															//className
															sbSelect.Append(@",
	dbo.className([" + mpi1.TableName + "].[" + mpi1.FieldProp.PropName + "])");
														}
													}
													else
													{
														if (mpi1.WithOrder)
														{
															sbSelect.Append(@",
	null");
															if (mpi1.FieldOrd.ColumnType == DataType.Uniqueidentifier)
															{
																//NK
																sbSelect.Append(@",
	null");
																//className
																sbSelect.Append(@",
	null");
															}
														}
														sbSelect.Append(@",
	null");
														if (mpi1.FieldProp.ColumnType == DataType.Uniqueidentifier && mpi1.FieldProp.IsRefer)
														{
															//NK												
															sbSelect.Append(@",
	null");
															//className
															sbSelect.Append(@",
	null");
														}
													}
												}
											}
										}
									}
									else
									{
										if (ci1.MultiFields != null)
										{
											foreach (MultiPropInfo mpi1 in ci1.MultiFields)
											{
												if (showAllMulties || searchMulty.ContainsKey(mpi1.PropName.ToLower()))
												{
													if (mpi1.WithOrder)
													{
														sbSelect.Append(@",
	null");
														if (mpi1.FieldOrd.ColumnType == DataType.Uniqueidentifier)
														{
															//NK
															sbSelect.Append(@",
	null");
															//className
															sbSelect.Append(@",
	null");
														}
													}
													sbSelect.Append(@",
	null");
													if (mpi1.FieldProp.ColumnType == DataType.Uniqueidentifier && mpi1.FieldProp.IsRefer)
													{
														//NK
														sbSelect.Append(@",
	null");
														//className
														sbSelect.Append(@",
	null");
													}
												}
											}
										}
									}
								}
								sbSelect.Append(@"
FROM
	[" + ci.TableName + @"]
	inner join [" + mpi.TableName + "] on [" + ci.TableName + @"].[OID] = [" + mpi.TableName + "].[" + mpi.FieldOID + "]");
								sbSelect.Append(@"
WHERE
	[" + ci.TableName + @"].[OID] = @OID");
							}
						}
					}
				}
				sbSelect.Append(@"
FOR XML EXPLICIT");
				if (hasBinaryField) sbSelect.Append(", BINARY BASE64");
				sql = sbSelect.ToString();
			}
			if (HttpContext.Current != null)
				HttpContext.Current.Cache.Insert(key, sql, null, DateTime.MaxValue, TimeSpan.FromMinutes(20));
			return sql;
		}
		private static string InnerBuildGetObject(MetadataInfo info, string fieldNames)
		{
			string sql = null;
			var key = "ObjectSQL_" + info.Classes[0].CID + (fieldNames ?? "");
			if (HttpContext.Current != null)
			{
				sql = (string)HttpContext.Current.Cache[key];
			}
			return sql ?? InnerBuildGetObjectExt(info, fieldNames);
		}

		private static string InnerBuildGetObjectDS(MetadataInfo info, string fieldNames)
		{
			string sql = null;   
			var key = "ObjectSQL_DS_" + info.Classes[0].CID + (fieldNames ?? "");
			if (HttpContext.Current != null)
			{
				sql = (string)HttpContext.Current.Cache[key];
			}
			if (sql != null) return sql;
			var doc = new XmlDocument();
			var build = new GetObjectBuilder(info);
			if (fieldNames != null) doc.LoadXml(fieldNames);
			var nl = doc.DocumentElement.SelectNodes("single/field");
			foreach (XmlNode n in nl)
			{
				var fieldName = n.Attributes["name"].Value;
				string columnName = null;
				if (n.Attributes["columnName"] != null) columnName = n.Attributes["columnName"].Value;
				build.AddSingleField(fieldName, columnName);
			}
			nl = doc.DocumentElement.SelectNodes("multi/field");
			foreach (XmlNode n in nl)
			{
				var fieldName = n.Attributes["name"].Value;
				string columnName = null;
				if (n.Attributes["columnName"] != null) columnName = n.Attributes["columnName"].Value;
				string order = null;
				if (n.Attributes["order"] != null) order = n.Attributes["order"].Value;
				build.AddMultiField(fieldName, columnName, order);
			}
			sql = build.BuildMTQuery();

			if (HttpContext.Current != null)
				HttpContext.Current.Cache.Insert(key, sql, null, DateTime.MaxValue, TimeSpan.FromMinutes(20));
			return sql;
		}

		private static string InnerBuildQueryObjects(MetadataInfo info, int numInBatch, int pageNum, string keyFields, string addFields, FilterCollection filter, OrderInfo[] order)
		{
			if (addFields.IndexOf("<field") != -1)
			{
				return InnerBuildQueryObjectsExt(info, numInBatch, pageNum, keyFields, addFields, filter, order);
			}
			bool found;
			var sbFrom = new StringBuilder();
			var sbSelect = new StringBuilder();
			if (numInBatch != 0)
			{
				sbSelect.Append(@"
SELECT TOP " + (pageNum != 0 ? numInBatch.ToString() : "0") + @"
	pageCount = @pageCount,
	itemCount = @itemCount,
	OID = [t_Object].[OID]");
			}
			else
			{
				sbSelect.Append(@"
SELECT pageCount = 1, OID = [t_Object].[OID]");
			}
			var tables = new Hashtable {{info.Classes[0].TableName.ToLower(), info.Classes[0]}};
			tables["t_object"] = info.Classes[info.Classes.Length - 1];
			//keyFields - пол¤ по которым, производитс¤ постранична¤ разбивка
			var keyFieldsArray = keyFields.Split(',');
			foreach (var keyField in keyFieldsArray)
			{
				if (addFields.IndexOf(keyField) == -1)
				{
					addFields += "," + keyField;
				}
			}
			if (addFields == "") sbSelect.Append(@",
	NK = dbo.nk([t_Object].[OID])");
			else
			{
				var addF = addFields.Split(",; ".ToCharArray());
				foreach (var item in addF)
				{
					if (item == "" || item.ToUpper() == "OID") continue;
					var columnName = item.Substring(item.LastIndexOf(".", StringComparison.Ordinal) + 1).ToLower();
					var columnSuffix = "";
					SplitColumn(ref columnName, ref columnSuffix);
					found = false;
					foreach (var ci in info.Classes)
					{
						foreach (var pi in ci.Fields.Where(pi => pi.PropName.ToLower() == columnName))
						{
							if (columnSuffix == "")
							{
								sbSelect.Append(@",
	" + pi.PropName + " = [" + ci.TableName + "].[" + pi.PropName + "]");
								tables[ci.TableName.ToLower()] = ci;
								found = true;
								break;
							}
							if (columnSuffix != "nk") continue;
							sbSelect.Append(@",
	" + pi.PropName + "_NK = dbo.NK([" + ci.TableName + "].[" + pi.PropName + "])");
							tables[ci.TableName.ToLower()] = ci;
							found = true;
							break;
						}
						if (found) break;
					}
				}
			}
			if (order == null)
			{
				var oi = new OrderInfo {ColumnName = "dateCreate", order = OrderDir.Desc};
				order = new[] { oi };
			}
			var orderFields = new ArrayList();
			var sbOrder = new StringBuilder();
			sbOrder.Append(@"
ORDER BY");
			for (var i = 0; i < order.Length; i++)
			{
				var item = order[i];
				if (item.TableName == null)
				{
					var columnName = item.ColumnName.ToLower();
					found = false;
					foreach (var ci in info.Classes)
					{
						if (ci.Fields.Any(pi => pi.PropName.ToLower() == columnName))
						{
							item.TableName = ci.TableName;
							tables[ci.TableName.ToLower()] = ci;
							found = true;
						}
						if (found) break;
					}
				}
				else
				{
					foreach (var ci in info.Classes.Where(ci => ci.TableName.ToLower() == item.TableName.ToLower()))
					{
						tables[ci.TableName.ToLower()] = ci;
						break;
					}
				}
				if (item.TableName == null) continue;
				sbOrder.Append(@"
	[" + item.TableName + "].[" + item.ColumnName + "] " + (item.order == OrderDir.Asc ? "ASC" : "DESC") + (i == order.Length - 1 ? "" : ", "));
				orderFields.Add(new TableField(item.TableName, item.ColumnName));
			}

			//ќЅ–јЅќ“ј≈ћ ‘»Ћ№“–џ
			var sbWhere = new StringBuilder(); 
			if (filter != null && filter.Filters != null && filter.Filters.Length > 0)
			{
				foreach (FilterInfo item in filter.Filters)
				{
					if (item.TableName == null)
					{
						var columnName = item.ColumnName.ToLower();
						found = false;
						foreach (var ci in info.Classes)
						{
							if (ci.Fields.Any(pi => pi.PropName.ToLower() == columnName))
							{
								item.TableName = ci.TableName;
								tables[ci.TableName.ToLower()] = ci;
								found = true;
							}
							if (found) break;
						}
					}
					else
					{
						var item1 = item;
						foreach (var ci in info.Classes.Where(ci => ci.TableName.ToLower() == item1.TableName.ToLower()))
						{
							tables[ci.TableName.ToLower()] = ci;
							break;
						}
					}
				}
				var where = filter.BuildFilter();
				if (where.Trim() != "")//если дл¤ таких фильтров были найдены таблицы
				{
					sbWhere.Append(@"
WHERE
	" + filter.BuildFilter());
				}
			}

			//«јѕќЋЌ»ћ FROM
			sbFrom.Append(@"
FROM 
	[t_Object] ");
			foreach (ClassInfo ci in from DictionaryEntry entry in tables where (string)entry.Key != "t_object" select (ClassInfo)entry.Value)
			{
				sbFrom.Append(@"
	inner join [" + ci.TableName + "] on [t_Object].[OID] = [" + ci.TableName + "].[OID]");
			}
			var sb = new StringBuilder();
			if (numInBatch != 0)
			{
				sb.Append(@"
Declare
@pageCount int,
@itemCount int
SELECT 
@pageCount=Ceiling(Count(*)/" + numInBatch + @".0), @itemCount = Count(*)
");
				sb.Append(sbFrom.ToString());
				sb.Append(sbWhere.ToString());
			}
			sb.Append(sbSelect.ToString());
			sb.Append(sbFrom.ToString());
			var strFields = "";
			var strOrderFields = "";
			if (numInBatch != 0 && pageNum > 1)
			{
				if (keyFields == "")
				{
					sb.Append(sbWhere.ToString());
					if (filter != null && filter.Filters != null) sb.Append(" AND ");
					else sb.Append(@"
WHERE");

					sb.Append(@"
	NOT [t_Object].[OID] in (SELECT TOP " + ((pageNum - 1) * numInBatch).ToString() + @" [t_Object].[OID]");
					sb.Append(sbFrom.ToString());
					sb.Append(sbWhere.ToString());
					sb.Append(sbOrder.ToString());
					sb.Append(@")");
				}
				else
				{
					var keyFieldsList = new ArrayList();
					foreach (var keyField in keyFieldsArray)
					{
						var keyFound = false;
						foreach (var ci in info.Classes)
						{
							var field = keyField;
							var ci1 = ci;
							foreach (var tf in from pi in ci.Fields where pi.PropName.ToLower() == field.ToLower() select new TableField(ci1.TableName, pi.PropName))
							{
								keyFieldsList.Add(tf);
								strFields += "[" + tf.tblAlias + "].[" + tf.fieldName + "],";
								var isInOrderAlready = orderFields.Cast<TableField>().Any(_tf => _tf.tblAlias == tf.tblAlias && _tf.fieldName == tf.fieldName);
								if (!isInOrderAlready)
									strOrderFields += "[" + tf.tblAlias + "].[" + tf.fieldName + "],";
								keyFound = true;
								break;
							}
							if (keyFound) break;
						}
					}

					strFields = strFields.Remove(strFields.Length - 1, 1);
					strOrderFields = strOrderFields.Remove(strOrderFields.Length - 1, 1);
					var leftSelect = @"
	LEFT JOIN (SELECT TOP " + ((pageNum - 1) * numInBatch).ToString() + " " + strFields;

					var _sb = new StringBuilder();
					_sb.Append(leftSelect);
					_sb.Append(sbFrom.ToString());
					_sb.Append(sbWhere.ToString());
					_sb.Append(sbOrder.ToString());
					if (strOrderFields != "")
						_sb.Append("," + strOrderFields);

					_sb.Append(@") lj 

ON ");
					var firstField = true;
					foreach (TableField tf in keyFieldsList)
					{
						if (firstField)
						{
							_sb.Append("[" + tf.tblAlias + "].[" + tf.fieldName + "]=lj.[" + tf.fieldName + "] ");
							firstField = false;
						}
						else
						{
							_sb.Append(@" 
		AND (([" + tf.tblAlias + "].[" + tf.fieldName + "]=lj.[" + tf.fieldName + "]) OR ([" + tf.tblAlias + "].[" + tf.fieldName + "] IS NULL AND lj.[" + tf.fieldName + "] IS NULL)) ");
						}
					}

					sb.Append(_sb.ToString());
					sb.Append(sbWhere.ToString());
					if (filter != null && filter.Filters != null)
						sb.Append(" AND ");
					else sb.Append(@"
WHERE");
					sb.Append(@" lj.[" + ((TableField)keyFieldsList[0]).fieldName + "] IS NULL");

				}
			}
			else
				sb.Append(sbWhere.ToString());

			sb.Append(sbOrder.ToString());
			if (strOrderFields != "")
				sb.Append("," + strOrderFields);
			return sb.ToString();
		}

		private static string InnerBuildQueryObjectsExt(MetadataInfo info, int numInBatch, int pageNum, string keyFieldsString, string addFields, FilterCollection filter, OrderInfo[] order)
		{
			var re = new Regex("<field(\\s+columnName=\"([a-zA-Z0-9_]*)\")?\\s+name=\"([^\"]*)\"", RegexOptions.IgnoreCase);
			if (order == null)
			{
				order = new[] { new OrderInfo("dateCreate", OrderDir.Desc) };
			}
			var build = new QueryObjectsBuilder(info, numInBatch, pageNum, filter);
			var mtchs = re.Matches(addFields);

			var keyFieldsStringArray = keyFieldsString.Split(',');
			var keyFieldsArray = new List<FieldExpr>();
			var fieldName = "OID";
			var columnName = "OID";
			var field = build.AddField(fieldName, columnName, FieldContext.List, null);
			if (field != null)
			{
				if (keyFieldsStringArray.Any(str => str.Trim().ToUpper() == "OID"))
				{
					keyFieldsArray.Add(field);
				}
			}

			var fields = new ArrayList {"OID"};
			foreach (Match m in mtchs)
			{
				fieldName = m.Groups[3].Value;
				columnName = m.Groups[2].Value;
				field = build.AddField(fieldName, columnName, FieldContext.List, null);
				fields.Add(fieldName);
				//добавил field.Field != null иначе функции типа GetDate() не пашут
				if (field != null && field.Fields != null && field.Fields.Count() == 1 && field.Fields.First().FieldName.Trim().ToUpper() != "OID")
				{
					keyFieldsArray.AddRange(from strFields in keyFieldsStringArray where fieldName == strFields select field);
				}
			}

			foreach (string t in keyFieldsStringArray)
			{
				if (t == "")
					continue;

				var fieldFound = fields.Cast<string>().Any(strField => strField == t);
				if (fieldFound) continue;
				field = build.AddField(t, "", FieldContext.List, null);
				if (field != null)
					keyFieldsArray.Add(field);
			}
			build.keyFields = keyFieldsArray;

			foreach (var ord in order)
			{
				fieldName = ord.ColumnName;
				build.AddField(fieldName, fieldName, FieldContext.Order, ord);
				//if(field != null)field.ContextObject = ord;
			}
			if (filter != null && filter.Filters != null)
			{
				foreach (var flt in filter.Filters)
				{
					fieldName = flt.ColumnName;
					columnName = flt.FilterName;
					build.AddField(fieldName, columnName, FieldContext.Filter, flt);
					//if(field != null)field.ContextObject = flt;
				}
			}
			return build.BuildQuery();
		}
		public static string BuildGetTreeContent(MetadataInfo info, string addFields, int nodeID, bool withViewDates)
		{
			var sbFrom = new StringBuilder();
			var sbSelect = new StringBuilder();
			var sbOrder = new StringBuilder();
			var sbWhere = new StringBuilder();
			sbSelect.Append(@"
SELECT
	n.nodeID,
	n.nodeName,
	n.nodeURL,
	OID = n.object,
	n.parentNode,
	n.lft,
	n.rgt,
	c.className,
	c.label");
			sbFrom.Append(@"
FROM
	t_Nodes n
	left join t_Object on n.object = t_Object.OID
	left join t_className c on t_Object.CID = c.CID");
			sbWhere.Append(@"
WHERE
	n.tree = @OID
");
			sbOrder.Append(@"
ORDER BY
	n.lft");
			if (withViewDates)
			{
				sbSelect.Append(@",
	nv.dateBegin,
	nv.dateEnd");
				sbFrom.Append(@"
	left join t_NodeView nv on n.nodeID = nv.nodeID
");
			}
			if (nodeID != 0)
			{
				sbFrom.Append(@"
	inner join t_Nodes n1 on n.tree = n1.tree And n.lft > n1.lft and n.lft < n1.rgt
");
				sbWhere.Append(@"
	and n1.nodeID = @nodeID
");
			}
			if (info == null || addFields == null || addFields == "")
			{
				sbSelect.Append(@",
	NK = dbo.nk(t_Object.OID)");
			}
			else
			{
				var tables = new Hashtable {{"t_object", info.Classes[info.Classes.Length - 1].Fields[0]}};
				var addF = addFields.Split(",; ".ToCharArray());
				foreach (string item in addF)
				{
					if (item == "" || item.ToUpper() == "OID") continue;
					string columnName = item.Substring(item.LastIndexOf(".", StringComparison.Ordinal) + 1).ToLower();
					string columnSuffix = "";
					SplitColumn(ref columnName, ref columnSuffix);
					var pi = (PropInfo)info.FindProp(columnName, 1);
					if (pi == null) continue;
					switch (columnSuffix)
					{
						case "":
							sbSelect.Append(@",
	" + pi.PropName + " = [" + pi.TableName + "].[" + pi.PropName + "]");
							tables[pi.TableName.ToLower()] = pi;
							break;
						case "nk":
							sbSelect.Append(@",
	" + pi.PropName + "_NK = dbo.NK([" + pi.TableName + "].[" + pi.PropName + "])");
							tables[pi.TableName.ToLower()] = pi;
							break;
						case "classname":
							sbSelect.Append(@",
	" + pi.PropName + "_ClassName = dbo.ClassName([" + pi.TableName + "].[" + pi.PropName + "])");
							tables[pi.TableName.ToLower()] = pi;
							break;
					}
				}
				foreach (var pi in from DictionaryEntry prop in tables where (string) prop.Key != "t_object" select (PropInfo)prop.Value)
				{
					sbFrom.Append(@"
	left join [" + pi.TableName + "] on [t_Object].[OID] = [" + pi.TableName + "].[OID]");
				}
			}
			var sb = new StringBuilder();
			sb.Append(sbSelect.ToString());
			sb.Append(sbFrom.ToString());
			sb.Append(sbWhere.ToString());
			sb.Append(sbOrder.ToString());
			return sb.ToString();
		}
		public static string BuildGetTreeContent(MetadataInfo info, string addFields, int nodeID, bool withViewDates, bool withLabels)
		{
			var sbFrom = new StringBuilder();
			var sbSelect = new StringBuilder();
			var sbOrder = new StringBuilder();
			var sbWhere = new StringBuilder();
			sbSelect.Append(@"
SELECT
	n.nodeID,
	n.nodeName,
	n.nodeURL,
	OID = n.object,
	n.parentNode,
	n.lft,
	n.rgt,
	c.className,
	c.label");
			sbFrom.Append(@"
FROM
	t_Nodes n
	left join t_Object on n.object = t_Object.OID
	left join t_className c on t_Object.CID = c.CID");
			sbWhere.Append(@"
WHERE
	n.tree = @OID
");
			sbOrder.Append(@"
ORDER BY
	n.lft");
			if (withViewDates)
			{
				sbSelect.Append(@",
	nv.dateBegin,
	nv.dateEnd");
				sbFrom.Append(@"
	left join t_NodeView nv on n.nodeID = nv.nodeID
");
			}
			if (withLabels)
			{
				sbSelect.Append(@",
	nl.ordValue,
	nl.value,
	nl.url");
				sbFrom.Append(@"
	left join t_NodeLabels nl on n.nodeID = nl.nodeID
");
			}
			if (nodeID != 0)
			{
				sbFrom.Append(@"
	inner join t_Nodes n1 on n.tree = n1.tree And n.lft > n1.lft and n.lft < n1.rgt
");
				sbWhere.Append(@"
	and n1.nodeID = @nodeID
");
			}
			if (info == null || addFields == null || addFields == "")
			{
				sbSelect.Append(@",
	NK = dbo.nk(t_Object.OID)");
			}
			else
			{
				var tables = new Hashtable {{"t_object", info.Classes[info.Classes.Length - 1].Fields[0]}};
				var addF = addFields.Split(",; ".ToCharArray());
				foreach (string item in addF)
				{
					if (item == "" || item.ToUpper() == "OID") continue;
					var columnName = item.Substring(item.LastIndexOf(".", StringComparison.Ordinal) + 1).ToLower();
					var columnSuffix = "";
					SplitColumn(ref columnName, ref columnSuffix);
					var pi = (PropInfo)info.FindProp(columnName, 1);
					if (pi == null) continue;
					switch (columnSuffix)
					{
						case "":
							sbSelect.Append(@",
	" + pi.PropName + " = [" + pi.TableName + "].[" + pi.PropName + "]");
							tables[pi.TableName.ToLower()] = pi;
							break;
						case "nk":
							sbSelect.Append(@",
	" + pi.PropName + "_NK = dbo.NK([" + pi.TableName + "].[" + pi.PropName + "])");
							tables[pi.TableName.ToLower()] = pi;
							break;
						case "classname":
							sbSelect.Append(@",
	" + pi.PropName + "_ClassName = dbo.ClassName([" + pi.TableName + "].[" + pi.PropName + "])");
							tables[pi.TableName.ToLower()] = pi;
							break;
					}
				}
				foreach (DictionaryEntry prop in tables)
				{
					if ((string) prop.Key == "t_object") continue;
					var pi = (PropInfo)prop.Value;
					sbFrom.Append(@"
	left join [" + pi.TableName + "] on [t_Object].[OID] = [" + pi.TableName + "].[OID]");
				}
			}
			var sb = new StringBuilder();
			sb.Append(sbSelect.ToString());
			sb.Append(sbFrom.ToString());
			sb.Append(sbWhere.ToString());
			sb.Append(sbOrder.ToString());
			return sb.ToString();
		}
		public static string BuildGetTreeContent(MetadataInfo info, string addFields, int nodeID, bool withViewDates, bool withLabels, bool withParent)
		{
			var sbFrom = new StringBuilder();
			var sbSelect = new StringBuilder();
			var sbOrder = new StringBuilder();
			var sbWhere = new StringBuilder();
			sbSelect.Append(@"
SELECT
	n.nodeID,
	n.nodeName,
	n.nodeURL,
	OID = n.object,
	n.parentNode,
	n.lft,
	n.rgt,
	c.className,
	c.label");
			sbFrom.Append(@"
FROM
	t_Nodes n
	left join t_Object on n.object = t_Object.OID
	left join t_className c on t_Object.CID = c.CID");
			sbWhere.Append(@"
WHERE
	n.tree = @OID
");
			sbOrder.Append(@"
ORDER BY
	n.lft");
			if (withViewDates)
			{
				sbSelect.Append(@",
	nv.dateBegin,
	nv.dateEnd");
				sbFrom.Append(@"
	left join t_NodeView nv on n.nodeID = nv.nodeID
");
			}
			if (withLabels)
			{
				sbSelect.Append(@",
	nl.ordValue,
	nl.value,
	nl.url");
				sbFrom.Append(@"
	left join t_NodeLabels nl on n.nodeID = nl.nodeID
");
			}
			if (nodeID != 0)
			{
				if (!withParent)
				{
					sbFrom.Append(@"
	inner join t_Nodes n1 on n.tree = n1.tree And n.lft > n1.lft and n.lft < n1.rgt
");
				}
				else
				{
					sbFrom.Append(@"
	inner join t_Nodes n1 on n.tree = n1.tree And n.lft BETWEEN n1.lft and n1.rgt
");
				}
				sbWhere.Append(@"
	and n1.nodeID = @nodeID
");
			}
			if (info == null || addFields == null || addFields == "")
			{
				sbSelect.Append(@",
	NK = dbo.nk(t_Object.OID)");
			}
			else
			{
				var tables = new Hashtable {{"t_object", info.Classes[info.Classes.Length - 1].Fields[0]}};
				var addF = addFields.Split(",; ".ToCharArray());
				foreach (string item in addF)
				{
					if (item == "" || item.ToUpper() == "OID") continue;
					var columnName = item.Substring(item.LastIndexOf(".", StringComparison.Ordinal) + 1).ToLower();
					var columnSuffix = "";
					SplitColumn(ref columnName, ref columnSuffix);
					var pi = (PropInfo)info.FindProp(columnName, 1);
					if (pi == null) continue;
					switch (columnSuffix)
					{
						case "":
							sbSelect.Append(@",
	" + pi.PropName + " = [" + pi.TableName + "].[" + pi.PropName + "]");
							tables[pi.TableName.ToLower()] = pi;
							break;
						case "nk":
							sbSelect.Append(@",
	" + pi.PropName + "_NK = dbo.NK([" + pi.TableName + "].[" + pi.PropName + "])");
							tables[pi.TableName.ToLower()] = pi;
							break;
						case "classname":
							sbSelect.Append(@",
	" + pi.PropName + "_ClassName = dbo.ClassName([" + pi.TableName + "].[" + pi.PropName + "])");
							tables[pi.TableName.ToLower()] = pi;
							break;
					}
				}
				foreach (DictionaryEntry prop in tables)
				{
					if ((string)prop.Key != "t_object")
					{
						var pi = (PropInfo)prop.Value;
						sbFrom.Append(@"
	left join [" + pi.TableName + "] on [t_Object].[OID] = [" + pi.TableName + "].[OID]");
					}
				}
			}
			var sb = new StringBuilder();
			sb.Append(sbSelect.ToString());
			sb.Append(sbFrom.ToString());
			sb.Append(sbWhere.ToString());
			sb.Append(sbOrder.ToString());
			return sb.ToString();
		}
		private static string InnerBuildGetMulti(MetadataInfo info, int OIDCount, string propName, string multiClass, int numInBatch, int pageNum, string addFields, FilterCollection filter, OrderInfo[] order)
		{
			if (addFields.IndexOf("<field") != -1)
			{
				return InnerBuildGetMultiExt(info, OIDCount, propName, multiClass, numInBatch, pageNum, addFields, filter, order);
			}
			var infoMulti = BuildMetadataInfo(multiClass ?? "CObject");
			var mpi = (MultiPropInfo)info.FindProp(propName, 2);

			bool found;
			var sbFrom = new StringBuilder();
			var sbSelect = new StringBuilder();
			if (numInBatch != 0)
			{
				sbSelect.Append(@"
SELECT TOP " + (pageNum != 0 ? numInBatch.ToString() : "0") + @"
	pageCount = @pageCount,
	itemCount = @itemCount,
	OID = mpi.[" + mpi.FieldProp.PropName + @"]");
			}
			else
			{
				sbSelect.Append(@"
SELECT pageCount = 1, OID = mpi.[" + mpi.FieldProp.PropName + @"]");
			}
			var tables = new Hashtable {{infoMulti.Classes[0].TableName.ToLower(), infoMulti.Classes[0]}};
			if (addFields == "") sbSelect.Append(@",
	NK = dbo.nk(mpi.[" + mpi.FieldProp.PropName + @"])");
			else
			{
				var addF = addFields.Split(",; ".ToCharArray());
				foreach (string item in addF)
				{
					if (item == "" || item.ToUpper() == "OID") continue;
					var columnName = item.Substring(item.LastIndexOf(".", StringComparison.Ordinal) + 1).ToLower();
					var columnSuffix = "";
					SplitColumn(ref columnName, ref columnSuffix);
					found = false;
					foreach (var ci in infoMulti.Classes)
					{
						foreach (var pi in ci.Fields.Where(pi => pi.PropName.ToLower() == columnName))
						{
							if (columnSuffix == "")
							{
								sbSelect.Append(@",
	" + pi.PropName + " = [" + ci.TableName + "].[" + pi.PropName + "]");
								tables[ci.TableName.ToLower()] = ci;
								found = true;
								break;
							}
							if (columnSuffix == "nk")
							{
								sbSelect.Append(@",
	" + pi.PropName + "_NK = dbo.NK([" + ci.TableName + "].[" + pi.PropName + "])");
								tables[ci.TableName.ToLower()] = ci;
								found = true;
								break;
							}
						}
						if (found) break;
					}
				}
			}
			if (order == null)
			{
				var oi = new OrderInfo {ColumnName = "dateCreate", order = OrderDir.Desc};
				order = new[] { oi };
			}
			var sbOrder = new StringBuilder();
			sbOrder.Append(@"
ORDER BY");
			for (var i = 0; i < order.Length; i++)
			{
				var item = order[i];
				if (item.TableName == null)
				{
					var columnName = item.ColumnName.ToLower();
					found = false;
					foreach (ClassInfo ci in infoMulti.Classes)
					{
						if (ci.Fields.Any(pi => pi.PropName.ToLower() == columnName))
						{
							item.TableName = ci.TableName;
							tables[ci.TableName.ToLower()] = ci;
							found = true;
						}
						if (found) break;
					}
				}
				else
				{
					foreach (ClassInfo ci in infoMulti.Classes)
					{
						if (ci.TableName.ToLower() == item.TableName.ToLower())
						{
							tables[ci.TableName.ToLower()] = ci;
							break;
						}
					}
				}
				if (item.TableName != null) sbOrder.Append(@"
	[" + item.TableName + "].[" + item.ColumnName + "] " + (item.order == OrderDir.Asc ? "ASC" : "DESC") + (i == order.Length - 1 ? "" : ", "));
			}
			var sbWhere = new StringBuilder();
			if (filter != null && filter.Filters != null)
			{
				sbWhere.Append(@"
WHERE ");
				foreach (FilterInfo item in filter.Filters)
				{
					if (item.TableName == null)
					{
						var columnName = item.ColumnName.ToLower();
						found = false;
						foreach (var ci in infoMulti.Classes)
						{
							if (ci.Fields.Any(pi => pi.PropName.ToLower() == columnName))
							{
								item.TableName = ci.TableName;
								tables[ci.TableName.ToLower()] = ci;
								found = true;
							}
							if (found) break;
						}
					}
					else
					{
						var item1 = item;
						foreach (var ci in infoMulti.Classes.Where(ci => ci.TableName.ToLower() == item1.TableName.ToLower()))
						{
							tables[ci.TableName.ToLower()] = ci;
							break;
						}
					}
				}
				sbWhere.Append(@"
	" + filter.BuildFilter());
			}
			sbFrom.Append(@"
FROM 
	[" + mpi.TableName + @"] mpi ");
			var OIDString = new StringBuilder();
			for (int i = 1; i <= OIDCount; i++)
			{
				OIDString.Append("@OID" + i.ToString() + ", ");
			}
			var OIDs = OIDString.ToString().Substring(0, OIDString.ToString().LastIndexOf(",", StringComparison.Ordinal));
			foreach (var ci in from DictionaryEntry entry in tables select (ClassInfo)entry.Value)
			{
				sbFrom.Append(@"
inner join [" + ci.TableName + "] on mpi.[" + mpi.FieldProp.PropName + @"] = [" + ci.TableName + "].[OID] And mpi.[" + mpi.FieldOID + @"] in (" + OIDs + ")");
			}
			var sb = new StringBuilder();
			if (numInBatch != 0)
			{
				sb.Append(@"
Declare
	@pageCount int,
	@itemCount int
	SELECT 
		@pageCount=Ceiling(Count(*)/" + numInBatch + @".0), @itemCount = Count(*)
");
				sb.Append(sbFrom.ToString());
				sb.Append(sbWhere.ToString());
			}
			sb.Append(sbSelect.ToString());
			sb.Append(sbFrom.ToString());
			sb.Append(sbWhere.ToString());
			if (numInBatch != 0 && pageNum > 1)
			{
				if (filter != null && filter.Filters != null) sb.Append(" AND ");
				else sb.Append(@"
WHERE");
				sb.Append(@"
	NOT mpi.[" + mpi.FieldProp.PropName + @"] in (SELECT TOP " + ((pageNum - 1) * numInBatch).ToString() + @" [t_Object].[OID]");
				sb.Append(sbFrom.ToString());
				sb.Append(sbWhere.ToString());
				sb.Append(sbOrder.ToString());
				sb.Append(@")");
			}
			sb.Append(sbOrder.ToString());
			return sb.ToString();
		}

		private static string InnerBuildGetMultiExt(MetadataInfo info, int OIDCount, string propName, string multiClass, int numInBatch, int pageNum, string addFields, FilterCollection filter, OrderInfo[] order)
		{
			var infoMulti = BuildMetadataInfo(multiClass ?? "CObject");
			var mpi = (MultiPropInfo)info.FindProp(propName, 2);

			bool found;
			var sbFrom = new StringBuilder();
			var sbSelect = new StringBuilder();
			if (numInBatch != 0)
			{
				sbSelect.Append(@"
SELECT TOP " + (pageNum != 0 ? numInBatch.ToString() : "0") + @"
	pageCount = @pageCount,
	itemCount = @itemCount,
	OID = mpi.[" + mpi.FieldProp.PropName + @"]");
			}
			else
			{
				sbSelect.Append(@"
SELECT pageCount = 1, OID = mpi.[" + mpi.FieldProp.PropName + @"]");
			}
			var tables = new Hashtable {{infoMulti.Classes[0].TableName.ToLower(), infoMulti.Classes[0]}};
			if (string.IsNullOrEmpty(addFields)) sbSelect.Append(@",
	NK = dbo.nk(mpi.[" + mpi.FieldProp.PropName + @"])");
			else
			{
				var re = new Regex("<field(\\s+columnName=\"([a-zA-Z0-9_]*)\")?\\s+name=\"([^\"]*)\"", RegexOptions.IgnoreCase);
				var mtchs = re.Matches(addFields);

				foreach (Match m in mtchs)
				{
					var fieldName = m.Groups[2].Value;
					var item = m.Groups[3].Value;
					if (fieldName == "") fieldName = item;

					if (fieldName.ToUpper() == "OID") continue;
					var columnName = item.Substring(item.LastIndexOf(".", StringComparison.Ordinal) + 1).ToLower();
					var columnSuffix = "";
					SplitColumn(ref columnName, ref columnSuffix);
					found = false;
					foreach (var ci in infoMulti.Classes)
					{
						foreach (PropInfo pi in ci.Fields.Where(pi => pi.PropName.ToLower() == columnName))
						{
							if (columnSuffix == "")
							{
								sbSelect.Append(@",
	" + fieldName + " = [" + ci.TableName + "].[" + pi.PropName + "]");
								tables[ci.TableName.ToLower()] = ci;
								found = true;
								break;
							}
							sbSelect.Append(@",
	" + fieldName + " = dbo." + columnSuffix + "([" + ci.TableName + "].[" + pi.PropName + "])");
							tables[ci.TableName.ToLower()] = ci;
							found = true;
							break;
						}
						if (found) break;
					}
				}
			}
			if (order == null)
			{
				var oi = new OrderInfo {ColumnName = "dateCreate", order = OrderDir.Desc};
				order = new[] { oi };
			}
			var sbOrder = new StringBuilder();
			sbOrder.Append(@"
ORDER BY");
			for (var i = 0; i < order.Length; i++)
			{
				var item = order[i];
				if (item.TableName == null)
				{
					var columnName = item.ColumnName.ToLower();
					found = false;
					foreach (var ci in infoMulti.Classes)
					{
						if (ci.Fields.Any(pi => pi.PropName.ToLower() == columnName))
						{
							item.TableName = ci.TableName;
							tables[ci.TableName.ToLower()] = ci;
							found = true;
						}
						if (found) break;
					}
				}
				else
				{
					foreach (ClassInfo ci in infoMulti.Classes)
					{
						if (ci.TableName.ToLower() == item.TableName.ToLower())
						{
							tables[ci.TableName.ToLower()] = ci;
							break;
						}
					}
				}
				if (item.TableName != null) sbOrder.Append(@"
	[" + item.TableName + "].[" + item.ColumnName + "] " + (item.order == OrderDir.Asc ? "ASC" : "DESC") + (i == order.Length - 1 ? "" : ", "));
			}
			var sbWhere = new StringBuilder();
			if (filter != null && filter.Filters != null)
			{
				sbWhere.Append(@"
WHERE ");
				foreach (var item in filter.Filters)
				{
					if (item.TableName == null)
					{
						var columnName = item.ColumnName.ToLower();
						found = false;
						foreach (var ci in infoMulti.Classes)
						{
							if (ci.Fields.Any(pi => pi.PropName.ToLower() == columnName))
							{
								item.TableName = ci.TableName;
								tables[ci.TableName.ToLower()] = ci;
								found = true;
							}
							if (found) break;
						}
					}
					else
					{
						var item1 = item;
						foreach (var ci in infoMulti.Classes.Where(ci => ci.TableName.ToLower() == item1.TableName.ToLower()))
						{
							tables[ci.TableName.ToLower()] = ci;
							break;
						}
					}
				}
				sbWhere.Append(@"
	" + filter.BuildFilter());
			}
			sbFrom.Append(@"
FROM 
	[" + mpi.TableName + @"] mpi ");
			var OIDString = new StringBuilder();
			for (var i = 1; i <= OIDCount; i++)
			{
				OIDString.Append("@OID" + i + ", ");
			}
			var OIDs = OIDString.ToString().Substring(0, OIDString.ToString().LastIndexOf(",", StringComparison.Ordinal));
			foreach (ClassInfo ci in from DictionaryEntry entry in tables select (ClassInfo)entry.Value)
			{
				sbFrom.Append(@"
inner join [" + ci.TableName + "] on mpi.[" + mpi.FieldProp.PropName + @"] = [" + ci.TableName + "].[OID] And mpi.[" + mpi.FieldOID + @"] in (" + OIDs + ")");
			}
			var sb = new StringBuilder();
			if (numInBatch != 0)
			{
				sb.Append(@"
Declare
	@pageCount int,
	@itemCount int
	SELECT 
		@pageCount=Ceiling(Count(*)/" + numInBatch + @".0), @itemCount = Count(*)
");
				sb.Append(sbFrom.ToString());
				sb.Append(sbWhere.ToString());
			}
			sb.Append(sbSelect.ToString());
			sb.Append(sbFrom.ToString());
			sb.Append(sbWhere.ToString());
			if (numInBatch != 0 && pageNum > 1)
			{
				if (filter != null && filter.Filters != null) sb.Append(" AND ");
				else sb.Append(@"
WHERE");
				sb.Append(@"
	NOT mpi.[" + mpi.FieldProp.PropName + @"] in (SELECT TOP " + ((pageNum - 1) * numInBatch).ToString() + @" [t_Object].[OID]");
				sb.Append(sbFrom.ToString());
				sb.Append(sbWhere.ToString());
				sb.Append(sbOrder.ToString());
				sb.Append(@")");
			}
			sb.Append(sbOrder.ToString());
			return sb.ToString();
		}

		private static string InnerBuildGetObjectExt(MetadataInfo info, string fieldDescription)
		{
			var doc = new XmlDocument();
			var build = new GetObjectBuilder(info);
			doc.LoadXml(fieldDescription);
			var nl = doc.DocumentElement.SelectNodes("single/field");
			foreach (XmlNode n in nl)
			{
				var fieldName = n.Attributes["name"].Value;
				string columnName = null;
				if (n.Attributes["columnName"] != null) columnName = n.Attributes["columnName"].Value;
				build.AddSingleField(fieldName, columnName);
			}
			nl = doc.DocumentElement.SelectNodes("multi/field");
			foreach (XmlNode n in nl)
			{
				string fieldName = n.Attributes["name"].Value;
				string columnName = null;
				if (n.Attributes["columnName"] != null) columnName = n.Attributes["columnName"].Value;
				string order = null;
				if (n.Attributes["order"] != null) order = n.Attributes["order"].Value;
				build.AddMultiField(fieldName, columnName, order);
			}
			return build.BuildQuery();
		}

		private static void SplitColumn(ref string columnNameFull, ref string columnSuffix)
		{
			int i = columnNameFull.IndexOf("_");
			if (i != -1)
			{
				string columnName = columnNameFull.Substring(0, i);
				columnSuffix = columnNameFull.Substring(i + 1, columnNameFull.Length - i - 1).ToLower();
				columnNameFull = columnName;
			}
		}
		public static string BuildLookupValues(string className)
		{
			var info = (MetadataInfo)HttpContext.Current.Cache["MetaDataInfo_" + className];
			if (info == null)
			{
				info = BuildMetadataInfo(className);
				HttpContext.Current.Cache.Insert("MetaDataInfo_" + className, info, null, DateTime.MaxValue, TimeSpan.FromMinutes(20));
			}
			return InnerBuildLookupValues(info);
		}
		public static string BuildLookupValues(string tableName, string fieldID, string fieldName)
		{
			return InnerBuildLookupValues(tableName, fieldID, fieldName);
		}
		private static string InnerBuildLookupValues(MetadataInfo info)
		{
			var sb = new StringBuilder();
			var firstClass = true;
			var findLookup = false;
			foreach (var lpi in info.Classes.Where(ci => ci.LookupFields != null).SelectMany(ci => ci.LookupFields))
			{
				findLookup = true;
				if (!firstClass)
				{
					sb.Append(@"
UNION ALL");
				}
				firstClass = false;
				sb.Append(@"
SELECT
	Tag = 1,
	Parent = null,
	[lookupField!1!propName] = '" + lpi.PropName + @"', 
	[lookupField!1!boundProp] = '" + lpi.FieldID + @"', 
	[item!2!ID] = null,
	[item!2!] = null
UNION ALL
SELECT
	2,
	1,
	'" + lpi.PropName + @"',
	null, 
	[" + lpi.TableName + "].[" + lpi.FieldID + @"],
	[" + lpi.TableName + "].[" + lpi.FieldName + @"]
FROM
	[" + lpi.TableName + @"]");
			}
			if (findLookup)
			{
				sb.Append(@"
ORDER BY
	[lookupField!1!propName], Tag
FOR XML EXPLICIT");
			}

			return sb.ToString();
		}
		private static string InnerBuildLookupValues(string tableName, string fieldID, string fieldName)
		{
			var sb = new StringBuilder();
			sb.Append(@"
SELECT
	Tag = 1,
	Parent = null,
	[lookupField!1!propName] = '" + tableName + @"', 
	[item!2!ID] = null,
	[item!2!] = null
UNION ALL
SELECT
	2,
	1,
	'" + tableName + @"',
	[" + tableName + "].[" + fieldID + @"],
	[" + tableName + "].[" + fieldName + @"]
FROM
	[" + tableName + @"]
ORDER BY
	[lookupField!1!propName], Tag
FOR XML EXPLICIT");
			return sb.ToString();
						
		}
		public static string BuildDetachMulti(string className, string propName, Guid OID, Guid detachedOID)
		{
			var info = BuildMetadataInfo(className);
			var sb = new StringBuilder();
			foreach (var mpi in from ci in info.Classes from mpi in ci.MultiFields where mpi.PropName.ToLower() == propName.ToLower() select mpi)
			{
				if (!mpi.One2Many) throw new Exception("use DeleteMulti not DetachMulti");
				if (mpi.IsPersistent)
				{
					sb.Append(@"
UPDATE [" + mpi.TableName + "] SET [" + mpi.FieldOID + "] = null WHERE [" +
					          mpi.FieldProp.PropName + @"] = '" + detachedOID.ToString() + @"' And
[" + mpi.FieldOID + "] = '" + OID.ToString() + "'");
				}
				else
				{
					foreach (var delCl in info.Classes)
					{
						sb.Append(@"
DELETE FROM [" + delCl.TableName + "] WHERE [" + mpi.FieldProp.PropName + "] = '" + detachedOID.ToString() + "'");
					}
				}
			}
			if (sb.ToString().Length == 0) throw new Exception("propName not found");
			return sb.ToString();
		}

		public static string BuildAttachMulti(string className, string propName, Guid OID, Guid detachedOID)
		{
			var info = BuildMetadataInfo(className);
			var sb = new StringBuilder();
			foreach (var mpi in from ci in info.Classes from mpi in ci.MultiFields where mpi.PropName.ToLower() == propName.ToLower() select mpi)
			{
				if (!mpi.One2Many) throw new Exception("use AddMulti not AttachMulti");
				sb.Append(@"
UPDATE [" + mpi.TableName + "] SET [" + mpi.FieldOID + "] = '" + OID.ToString() + "' WHERE [" +
				          mpi.FieldProp.PropName + @"] = '" + detachedOID.ToString() + @"'");
			}
			if (sb.ToString().Length == 0) throw new Exception("propName not found");
			return sb.ToString();
		}

	}

	class TableField
	{
		public string tblAlias = "";
		public string fieldName = "";

		public TableField() { }
		public TableField(string tblAlias, string fieldName)
		{
			this.tblAlias = tblAlias;
			this.fieldName = fieldName;
		}
	}
}