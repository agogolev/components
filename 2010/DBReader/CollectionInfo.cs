using System;


namespace DBReader
{
	public class CollectionInfo : TableInfo
	{
		public string ClassName;
		public string PropName;
		public string FieldOID;
	}
}
