using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using MetaData;

namespace DBReader
{
	/// <summary>
	/// Summary description for QueryObjectsBuilder.
	/// </summary>
	public class QueryObjectsBuilder : ObjectQueryParser
	{
		private readonly Joins _fields;
		public List<FieldExpr> keyFields = new List<FieldExpr>();
		private readonly int _numInBatch;
		private readonly int _pageNum;
		private readonly FilterCollection _filter;

		public QueryObjectsBuilder(MetadataInfo info, int numInBatch, int pageNum, FilterCollection filter)
			: base(info)
		{
			_fields = new Joins();
			_numInBatch = numInBatch;
			_pageNum = pageNum;
			_filter = filter;
		}
		public FieldExpr AddField(string fieldName, string columnName, FieldContext context, object contextObject)
		{
			FieldExpr field = null;
			if (contextObject is ExpressionFilterInfo)
			{
				field = BuildConstField(fieldName);
			}
			else
			{
				_lex = new LexAn(fieldName);
				_slidingInfo = _info;
				var t = _lex.Next();
				switch (t.Tok)
				{
					case LexAn.FUNC:
						field = BuildFunction((string)t.TokenVal);
						break;
					case LexAn.ID:
						field = BuildField((string)t.TokenVal);
						break;
					case LexAn.PARAM:
					case LexAn.STRING:
					case LexAn.NUM:
						field = BuildConstField((string)t.TokenVal);
						break;
					//////////////////// TODO: —делать обработку *
					case LexAn.ALL:
						break;
					//////////////////// TODOEND: —делать обработку *
					default:
						Error(t, "AddField");
						break;
				}
			}
			if (field != null)
			{
				field.Context = context;
				field.ContextObject = contextObject;
				_fields.Add(field);
				if (!string.IsNullOrEmpty(columnName)) field.FullFieldName = columnName;
			}
			return field;
		}
		public override string BuildQuery()
		{
			var sb = new StringBuilder();
			var select = BuildSelectClause();
			var from = BuildFromClause();
			var where = BuildWhereClause();
			var orderFields = new ArrayList();
			var order = BuildOrderClause(orderFields);
			if (_numInBatch != 0)
			{
				sb.Append(@"
Declare
	@pageCount int,
	@itemCount int
	SELECT 
		@pageCount= Ceiling(Count(*)/" + _numInBatch + @".0), @itemCount = Count(*)
");
				sb.Append(from);
				sb.Append(where);
			}
			sb.Append(select);
			sb.Append(from);
			var strFields = "";
			var strOrderFields = "";
			//если указаны пол¤, по которым проводить постраничную разбивку
			if (_pageNum > 1)
			{
				if (keyFields == null || keyFields.Count == 0)
				{
					sb.Append(where);

					if (_filter != null && _filter.Filters != null) sb.Append(" AND ");
					else sb.Append(@" WHERE");
					sb.Append(@" NOT " + FullKeyField() + " in (SELECT TOP " + ((_pageNum - 1) * _numInBatch).ToString() + @" " + FullKeyField());
					sb.Append(from);
					sb.Append(where);
					sb.Append(order);
					sb.Append(@")");
				}
				else
				{
					var firstKeyField = keyFields[0];
					foreach (var fe in keyFields)
					{
						foreach (var fi in fe.Fields)
						{
							var field = fi.FieldName;

							var tblAlias = fi.TableNameAlias;

							strFields += "[" + tblAlias + "].[" + field + "],";

							if (firstKeyField == null)
								firstKeyField = fe;
							var isInOrderAlready = orderFields.Cast<TableField>().Any(_tf => _tf.tblAlias == tblAlias && _tf.fieldName == field);
							if (!isInOrderAlready)
								strOrderFields += "[" + tblAlias + "].[" + field + "],";
						}
					}
					strFields = strFields.Remove(strFields.Length - 1, 1);
					strOrderFields = strOrderFields.Remove(strOrderFields.Length - 1, 1);
					var leftSelect = @" LEFT JOIN (SELECT TOP " + ((_pageNum - 1) * _numInBatch).ToString() + " " + strFields;
					var _sb = new StringBuilder();
					_sb.Append(leftSelect);
					_sb.Append(from);
					_sb.Append(where);
					_sb.Append(order);
					if (strOrderFields != "")
						_sb.Append("," + strOrderFields);

					_sb.Append(@") lj 

ON ");
					var firstField = true;
					foreach (var fe in keyFields)
					{
						if (fe.Fields == null) break;
						foreach (var fi in fe.Fields)
						{
							if (firstField)
							{
								_sb.Append("[" + fi.TableNameAlias + "].[" + fi.FieldName + "]=lj.[" + fi.FieldName + "] ");
								firstField = false;
							}
							else
							{
								_sb.Append(@" 
		AND (([" + fi.TableNameAlias + "].[" + fi.FieldName + "]=lj.[" + fi.FieldName + "]) OR ([" + fi.TableNameAlias + "].[" + fi.FieldName + "] IS NULL AND lj.[" + fi.FieldName + "] IS NULL)) ");
							}
						}

					}

					sb.Append(_sb.ToString());
					sb.Append(where);
					if (_filter != null && _filter.Filters != null) sb.Append(" AND ");
					else sb.Append(@"
WHERE");
					var f = keyFields[0];
					sb.Append(@" lj.[" + f.Fields[0].FieldName + "] IS NULL");
				}
			}
			else
				sb.Append(where);

			if (order.ToLower().Trim() != "order")
			{
				sb.Append(order);
			}
			if (strOrderFields != "")
				sb.Append("," + strOrderFields);

			return sb.ToString();
		}

		private string BuildSelectClause()
		{
			var sb = new StringBuilder();
			if (_numInBatch != 0)
			{
				sb.Append(@"
SELECT TOP " + (_pageNum != 0 ? _numInBatch.ToString() : "0") + @" 
	PageCount = @pageCount,
	ItemCount = @itemCount,");
			}
			else
			{
				sb.Append(@"
SELECT PageCount = 1,");
			}
			foreach (var fe in _fields.Where(fe => fe.Context == FieldContext.List))
			{
				sb.Append(@"
	[" + fe.FullFieldName + @"] = " + fe.Expr() + @",");
			}
			sb.Length -= 1;
			return sb.ToString();
		}
		private string BuildFromClause()
		{
			foreach (var fi in from field in _fields where field.Fields != null from fi in field.Fields where fi != null select fi)
			{
				LinkToBackbone(fi);
			}
			return _fields.BuildFromClause();
		}

		private string BuildWhereClause()
		{
			var findfilter = false;
			var sb = new StringBuilder();
			sb.Append(@"
WHERE");
			if (string.IsNullOrEmpty(_filter.FilterString))
			{
				foreach (var fe in _fields.Where(fe => fe.Context == FieldContext.Filter))
				{
					findfilter = true;
					var fi = (FilterInfo)fe.ContextObject;
					sb.Append(@"
	" + fi.Expr(fe.Expr()) + (_filter.CombinationType == FilterCombinationType.AND ? " And " : " OR "));
				}
				sb.Length -= 4;
			}
			else
			{
				var noNameFilters = new ArrayList();
				var flt = _filter.FilterString.ToLower();
				foreach (var fe in _fields.Where(fe => fe.Context == FieldContext.Filter))
				{
					findfilter = true;
					var fi = (FilterInfo)fe.ContextObject;
					if (fi.FilterName != null)
					{
						var repl = new Regex(string.Concat(@"\b", fi.FilterName, @"\b"), RegexOptions.IgnoreCase);
						flt = repl.Replace(flt, fi.Expr(fe.Expr()));
					}
					else
					{
						noNameFilters.Add(fi.Expr(fe.Expr()));
					}
				}
				if (noNameFilters.Count > 0)
				{
					flt = "(" + flt + ")";
					flt = noNameFilters.Cast<string>().Aggregate(flt, (current, strFilter) => current + (" AND " + strFilter));
				}
				sb.Append(@"
	" + flt);
			}
			return !findfilter ? "" : sb.ToString();
		}

		private string BuildOrderClause(ArrayList orderFields)
		{
			var sb = new StringBuilder();
			sb.Append(@"
ORDER BY");
			foreach (var fe in _fields)
			{
				if (fe.Context != FieldContext.Order) continue;
				var oi = (OrderInfo)fe.ContextObject;
				sb.Append(@"
	" + oi.Expr(fe.Expr()) + @", ");
				//добавил fe.Field != null иначе функции типа GetDate() не пашут
				if(fe.Fields == null) continue;
				foreach (var fi in fe.Fields.Where(fi => orderFields != null))
				{
					orderFields.Add(new TableField(fi.TableNameAlias, fi.Fields[0].FieldName));
				}
			}
			sb.Length -= 2;
			return sb.ToString();
		}
	}
}
