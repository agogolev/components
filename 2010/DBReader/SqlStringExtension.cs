using System;
using System.Collections.Generic;
using System.Data;
using System.Collections;
using MetaData;

namespace DBReader
{
	public static class SqlStringExtension
	{
		public static IEnumerable<DataRow> ExecSql(this string sqlString, Object prms = null, bool isSproc = false)
		{
			Hashtable prmsHash = new Hashtable();
			if (prms != null)
			{
				var prmsPis = prms.GetType().GetProperties();
				foreach (var prmsPi in prmsPis) prmsHash.Add(prmsPi.Name, prmsPi.GetValue(prms, null));
			}
			DataSetISM ds = DBHelper.GetDataSetISMSql(sqlString, null, prmsHash, isSproc, null);
			return ds.Table.AsEnumerable();
		}

		public static IEnumerable<DataRow> ExecSql(this string sqlString, Dictionary<string, string> prms, bool isSproc = false)
		{
			Hashtable prmsHash = new Hashtable();
			if (prms != null)
			{
				foreach (var prm in prms) prmsHash.Add(prm.Key, prm.Value);
			}
			DataSetISM ds = DBHelper.GetDataSetISMSql(sqlString, null, prmsHash, isSproc, null);
			return ds.Table.AsEnumerable();
		}

        public static DataTableCollection ExecMSql(this string sqlString, Object prms = null, bool isSproc = false)
        {
            Hashtable prmsHash = new Hashtable();
            if (prms != null)
            {
                var prmsPis = prms.GetType().GetProperties();
                foreach (var prmsPi in prmsPis) prmsHash.Add(prmsPi.Name, prmsPi.GetValue(prms, null));
            }
            DataSetISM ds = DBHelper.GetDataSetISMSql(sqlString, null, prmsHash, isSproc, null);
            return ds.Tables;
        }

        public static DataTableCollection ExecMSql(this string sqlString, Dictionary<string, string> prms, bool isSproc = false)
        {
            Hashtable prmsHash = new Hashtable();
            if (prms != null)
            {
                foreach (var prm in prms) prmsHash.Add(prm.Key, prm.Value);
            }
            DataSetISM ds = DBHelper.GetDataSetISMSql(sqlString, null, prmsHash, isSproc, null);
            return ds.Tables;
        }
	}
}
