using System;
using MetaData;
using System.Xml;
using System.Text;
using System.Collections;
using System.Globalization;
using System.Data;
using System.Data.SqlClient;

namespace DBReader
{
	/// <summary>
	/// Summary description for TableAdapter.
	/// </summary>
	public class TableAdapter
	{
		private XmlDocument _doc;
		private string _tableName;
		private TableInfo _info;
		private Hashtable _fields;
		private SqlConnection _conn;
		private SqlTransaction _trans;

		public TableAdapter(string tableName, string xmlContent) {
			_tableName = tableName;
			_doc = new XmlDocument();
			_doc.LoadXml(xmlContent);
			_fields = new Hashtable();
			_conn = null;
			_trans = null;
		}
		public TableAdapter(string tableName, string xmlContent, SqlConnection conn, SqlTransaction trans)//для поддержки транзакций
		{
			_tableName = tableName;
			_doc = new XmlDocument();
			_doc.LoadXml(xmlContent);
			_fields = new Hashtable();
			_conn = conn;
			_trans = trans;
		}
		public void AcceptChanges() {
			if(_doc.DocumentElement.ChildNodes.Count == 0) return;
			_info = QueryConstructor.BuildTableInfo(_tableName);
			object[] keyValues = new object[_info.PrimaryKey.Length];
			XmlNodeList nDelete = _doc.DocumentElement.SelectNodes("delete");
			if(nDelete.Count != 0) {
				StringBuilder sqlB = new StringBuilder();
				sqlB.Append(@"
DELETE FROM ["+_info.TableName+@"] 
WHERE ");
				int i = 0;
				foreach(string key in _info.PrimaryKey) {
					sqlB.Append(@"
	["+key+"] = @param"+(i++).ToString()+" And ");
				}
				sqlB.Length -= 4;
				string sql = sqlB.ToString();
				Hashtable prms = new Hashtable();
				foreach(XmlNode n in nDelete) {
					i = 0;
					foreach(string key in _info.PrimaryKey) {
						prms["@param"+(i++).ToString()] = Convert(key, n.SelectSingleNode(key).InnerText);
					}
					if(_conn != null && _trans != null)
						DBHelper.ExecuteCommand(sql, prms, false, _conn, _trans);
					else DBHelper.ExecuteCommand(sql, prms, false);
				}
			}
			XmlNodeList nAdd = _doc.DocumentElement.SelectNodes("add");
			if(nAdd.Count != 0) {
				StringBuilder sqlB = new StringBuilder();
				Hashtable fields = new Hashtable();
				sqlB.Append(@"
INSERT INTO ["+_info.TableName+@"] (");
				foreach(PropInfo pi in _info.Fields) {
					// мега заглушка для auto-increment поля
					if (_info.TableName == "t_SubscriptionPeriods" && pi.PropName == "ID")
						continue;
					if(nAdd[0].SelectSingleNode(pi.PropName) != null) {
						sqlB.Append(pi.PropName+", ");
						fields[pi.PropName] = 1;
					}
				}
				sqlB.Length -= 2;
				sqlB.Append(@")
VALUES(");
				int i = 0;
				foreach(PropInfo pi in _info.Fields) {
					if(fields[pi.PropName] != null) sqlB.Append(CastVariable(pi, "@param"+(i++).ToString())+", ");
				}
				sqlB.Length -= 2;
				sqlB.Append(@")");
				string sql = sqlB.ToString();
				Hashtable prms = new Hashtable();
				foreach(XmlNode n in nAdd) {
					i = 0;
					foreach(PropInfo pi in _info.Fields) {
						if(fields[pi.PropName] != null) prms["@param"+(i++).ToString()] = Convert(pi.PropName, n.SelectSingleNode(pi.PropName).InnerText);
					}
					if(_conn != null && _trans != null)
						DBHelper.ExecuteCommand(sql, prms, false, _conn, _trans);
					else DBHelper.ExecuteCommand(sql, prms, false);
				}
			}
			XmlNodeList nModify = _doc.DocumentElement.SelectNodes("modify");
			if(nModify.Count != 0) {
				StringBuilder sqlB = new StringBuilder();
				ArrayList fields = new ArrayList();
				sqlB.Append(@"
UPDATE ["+_info.TableName+@"] SET");
				int i = 0;
				foreach(PropInfo pi in _info.Fields) {
					if(!_info.IsPrimaryKey(pi.PropName) && nModify[0].SelectSingleNode(pi.PropName) != null) {
						sqlB.Append(@"
"+pi.PropName+" = "+CastVariable(pi, "@param"+(i++).ToString())+", ");
						fields.Add(pi.PropName);
					}
				}
				sqlB.Length -= 2;
				sqlB.Append(@"
WHERE");
				foreach(string key in _info.PrimaryKey) {
					sqlB.Append(@"
	["+key+"] = @param"+(i++).ToString()+" And ");
				}			
				sqlB.Length -= 4;
				string sql = sqlB.ToString();
				Hashtable prms = new Hashtable();
				foreach(XmlNode n in nModify) {
					i = 0;
					foreach(string fName in fields) {
						prms["@param"+(i++).ToString()] = Convert(fName, n.SelectSingleNode(fName).InnerText);
					}
					foreach(string key in _info.PrimaryKey) {
						prms["@param"+(i++).ToString()] = Convert(key, n.SelectSingleNode(key).InnerText);
					}
					if(_conn != null && _trans != null)
						DBHelper.ExecuteCommand(sql, prms, false, _conn, _trans);
					else DBHelper.ExecuteCommand(sql, prms, false);
				}
			}
		}
		public NumberFormatInfo NumberFormat {
			get {
				NumberFormatInfo nfi = new CultureInfo("en-US").NumberFormat;
				nfi.NumberDecimalSeparator = ".";
				return nfi;
			}
		}
		public DateTimeFormatInfo DateTimeFormat {
			get {
				DateTimeFormatInfo dtfi = new CultureInfo("ru-RU").DateTimeFormat;
				return dtfi;
			}
		}
		private object Convert(string field, string value) {
			PropInfo pi = (PropInfo)_fields[field.ToLower()];
			if(pi == null) {
				pi = _info.GetField(field);
				_fields[field.ToLower()] = pi;
			}
			switch(pi.ColumnType) {
				case DataType.String:
					return (value == "\0" ? DBNull.Value : (object) value);
				case DataType.Uniqueidentifier: 
					Guid g = (value == "\0" ? Guid.Empty : new Guid(value));
					return (g == Guid.Empty ? DBNull.Value : (object) g);
				case DataType.DateTime:
					return (value == "\0" ? DBNull.Value : (object)System.Convert.ToDateTime(value, DateTimeFormat));
				case DataType.Float:
					return (value == "\0" ? DBNull.Value : (object)System.Convert.ToDouble(value, NumberFormat));
				case DataType.Money:
					return (value == "\0" ? DBNull.Value : (object)System.Convert.ToDecimal(value, NumberFormat));
				case DataType.Int:
					return (value == "\0" ? DBNull.Value : (object)System.Convert.ToInt32(value));
				case DataType.Boolean:
					return (value == "\0" ? DBNull.Value : (object)System.Convert.ToBoolean(value));
				default:
					return null;
			}
		}
		private string CastVariable(PropInfo prop, string var) {
			switch(prop.ColumnType) {
				case DataType.Float:
					return "convert(decimal, "+var+")";
				case DataType.Money:
					return "convert(money, "+var+")";
				default:
					return var;
			}
		}
	}
}
