using System;
using System.Linq;
using System.Collections;
using System.Text;
using MetaData;

namespace DBReader
{
	public abstract class FieldExpr {
		public abstract string Expr();
		public abstract string FullFieldName{
			get;
			set;
		}
		public abstract FieldInfo[] Fields {
			get;
		}
		public FieldContext Context;
		/// может быть только null Context = List
		/// OrderInfo	Context = Order
		/// FilterInfo Context = Filter
		public object ContextObject; 
		public string TruncFieldName(int level) {
			int j = 0;
			while(level-- != 0) {
				int i = FullFieldName.IndexOf("_", j);
				if (i == -1) break;
				j = i + 1;
			}
			return FullFieldName.Substring(j);
		}
		public bool isOrdered;
		public string OrderDir = "ASC";
	}
	public class ConstFieldInfo : FieldExpr {
		private string _fullFieldName;
		private string _expr;
		public override string FullFieldName {
			get {
				return _fullFieldName;
			}
			set {
				_fullFieldName = value;
			}
		}
		public override FieldInfo[] Fields {
			get {
				return null;
			}
		}
		public ConstFieldInfo(string expr) {
			_expr = expr;
		}
		public override string Expr() {
			return _expr;
		}
	}
	public class FieldInfo : FieldExpr {
		private string _fullFieldName;
		public override string FullFieldName {
			get {
				return _fullFieldName;
			}
			set {
				_fullFieldName = value;
			}
		}
		public override FieldInfo[] Fields {
			get {
				return new[] { this };
			}
		}
		public string FieldName;
		public string TableName;
		public string TableNameAlias;
		public DataType FieldType;
		private ArrayList _relations;
		private ArrayList _props;
		public ArrayList Props {
			get {
				return _props;
			}
		}
		public RelationInfo[] Relations {
			get {
				RelationInfo[] rel = new RelationInfo[_relations.Count];
				for(int i = 0; i < _relations.Count; i++) {
					rel[i] = (RelationInfo)_relations[i];
				}
				return rel;
			}
		}
		public FieldInfo(string fieldName) {
			FieldName = fieldName;
			_fullFieldName = fieldName;
			_relations = new ArrayList();
			_props = new ArrayList();
		}
		public void AddRelation(RelationInfo rel) {
			_relations.Add(rel);
			TableName = rel.ChildTable;
		}
		public void InsertHead(RelationInfo rel) {
			if(_relations.Count == 0) {
				rel.ChildTable = TableName;
				_relations.Add(rel);
			}
			else {
				rel.ChildTable = Relations[0].ParentTable;
				rel.ChildTableAlias = Relations[0].ParentTableAlias;
				_relations.Insert(0, rel);
			}
		}
		public override string Expr() {
			return TableNameAlias+".["+FieldName+"]";
		}
		public bool ContainRelation(FieldInfo field) {
			if(field.Relations.Length > this.Relations.Length) return false;
			for(int i = 0; i < field.Relations.Length; i++)	{
				if(!field.Relations[i].Equals(this.Relations[i])) return false;				
			}
			return true;
		}
	}
	public class FunctionInfo : FieldExpr {
		private readonly string _functionName;
		private string _columnName;
		private readonly ArrayList _args;

		public bool Reserved;
		public bool hasNotFoundedFields;
		public override string FullFieldName {
			get
			{
				return string.IsNullOrEmpty(_columnName) ? 
					Fields.Aggregate(new StringBuilder(), (sb, field) => sb.AppendFormat("{0}_", field.FullFieldName), sb => sb.Append(_functionName).ToString()) 
					: _columnName;
			}
			set {
				_columnName = value;
			}
		}

		public override FieldInfo[] Fields
		{
			get { return _args.OfType<FieldInfo>().ToArray(); } 
		}


		public string FunctionName {
			get{return _functionName;}
		}
		public ArrayList Args {
			get {return _args;}
		}
		public FunctionInfo(string functionName) {
			_functionName = functionName;
			_args = new ArrayList();
		}
		//public FieldInfo FieldArg {
		//    get {
		//        for(int i = 0; i < _args.Count; i++) {
		//            object o = _args[i];
		//            if(o is FieldInfo) {
		//                return (FieldInfo)o;
		//            }
		//        }
		//        return null;
		//    }
		//}
		public override string Expr() {
			//bool findField = false;
			var sb = new StringBuilder();
			sb.Append((Reserved ? "":"dbo.")+_functionName+"(");
			for(var i = 0; i < _args.Count; i++) {
				var o = _args[i];
				if(o is FieldExpr) {
					//if(findField) throw new Exception("duplicate field definition in function "+_functionName);
					sb.Append(((FieldExpr)o).Expr());
					//if(o is FieldInfo) findField = true;
				}
				else sb.Append(o.ToString());
				if(i != _args.Count-1) sb.Append(",");
			}
			sb.Append(")");
			return sb.ToString();
		}

	}
	public class RelationInfo {
		public string ParentTable;
		public string ParentTableAlias;
		public string ChildTable;
		public string ChildTableAlias;
		public string ParentField;
		public string ChildField;
		public string ChildClassName;
		public bool IsInner;
		public string JoinExpr() {
			return @"
"+(IsInner ? "\tinner " : "\tleft ") + " join ["+ChildTable+"].["+ChildField+"] on ["+ParentTable+"].["+ParentField+"] = ["+ChildTable+"].["+ChildField+"] ";
		}
		public override bool Equals(object obj) {
			if(!(obj is RelationInfo)) return false;
			RelationInfo rel = (RelationInfo) obj;
			return rel.ParentTable.ToUpper().Equals(this.ParentTable.ToUpper()) &&
				rel.ParentField.ToUpper().Equals(this.ParentField.ToUpper()) &&
				rel.ChildTable.ToUpper().Equals(this.ChildTable.ToUpper()) &&
				rel.ChildField.ToUpper().Equals(this.ChildField.ToUpper()) &&
				rel.IsInner.Equals(this.IsInner);
		}
		public override int GetHashCode() {
			return ParentTable.GetHashCode()*ParentField.GetHashCode()*ChildTable.GetHashCode()*ChildField.GetHashCode()*IsInner.GetHashCode();
		}

	}
	public enum FieldContext {List, Order, Filter};
}
