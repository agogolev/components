using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MetaData;

namespace DBReader
{
	/// <summary>
	/// Summary description for ObjectQueryParser.
	/// </summary>
	public abstract class ObjectQueryParser
	{
		protected MetadataInfo _info;
		protected MetadataInfo _slidingInfo;
		private TableDesc[] _backboneAliases;
		protected LexAn _lex;
		public ObjectQueryParser(MetadataInfo info)
		{
			_info = info;
			_backboneAliases = new TableDesc[info.Classes.Length];
		}
		protected void Error(Token t, string functionName)
		{
			throw new Exception("invalid token ID = " + t.Tok.ToString() + " value = " + t.TokenVal.ToString() + " in function " + functionName);
		}
		private void Match(int tokVal, Token t)
		{
			if ((char)t.TokenVal == tokVal) return;
			else Error(t, "Match");
		}
		protected FunctionInfo BuildFunction(string functionName)
		{
			FunctionInfo funct = new FunctionInfo(functionName);
			if (_lex.Symbols.ContainsKey(functionName.ToUpper())) funct.Reserved = true;
			ParseFunction(funct);
			//если функция от несуществующего поля - то ничего не возвращаем
			if (funct.hasNotFoundedFields) return null;
			return funct;
		}
		protected FieldExpr BuildField(string fieldName)
		{
			Prop p = _info.FindProp(fieldName, 3);
			FieldExpr field;
			if (p != null)
			{
				field = new FieldInfo(fieldName);
				ParseField((FieldInfo)field, p);
			}
			else field = new ConstFieldInfo(fieldName);
			//if(p == null) return null; //throw new Exception("Field "+field.FieldName+" not found in "+_info.Classes[0].ClassName);
			return field;
		}
		protected FieldExpr BuildConstField(string constantExpr)
		{
			ConstFieldInfo field = new ConstFieldInfo(constantExpr);
			return field;
		}
		private RelationInfo BuildRelation(FieldInfo field, Prop p, char tokenVal)
		{
			RelationInfo rel = new RelationInfo();
			if (p is PropInfo)
			{
				rel.ParentField = field.FieldName;
				rel.ParentTable = field.TableName;
				QueryConstructor.BuildForeignRelationInfo(rel, null);
			}
			else if (p is MultiPropInfo)
			{
				rel.ParentField = field.FieldName;
				rel.ParentTable = field.TableName;
				rel.ChildField = (p as MultiPropInfo).FieldOID;
				rel.ChildTable = p.TableName;
			}
			if (tokenVal == '.') rel.IsInner = true;
			else if (tokenVal == '?') rel.IsInner = false;
			else throw new Exception("invalid relation '" + tokenVal + "'");
			return rel;
		}
		protected virtual void ParseField(FieldInfo field, Prop p)
		{
			field.FullFieldName = field.FieldName;
			field.Props.Add(p);
			if (p is PropInfo)
			{
				field.TableName = p.TableName;
			}
			else if (p is LinkedProp)
			{
				field.FieldName = (p as LinkedProp).ParentFieldName;
				field.TableName = (p as LinkedProp).ParentTableName;
			}
			while (true)
			{
				Token t = _lex.Next();
				switch (t.Tok)
				{
					case LexAn.RELATION:
						RelationInfo rel = BuildRelation(field, p, (char)t.TokenVal);
						field.AddRelation(rel);
						break;
					case LexAn.ID:
						string fieldName = (string)t.TokenVal;
						rel = field.Relations[field.Relations.Length - 1];
						if (p is PropInfo)
						{
							if (rel.ChildClassName != null)
								_slidingInfo = QueryConstructor.BuildMetadataInfo(rel.ChildClassName);
							else
								_slidingInfo = QueryConstructor.BuildTableData(rel.ChildTable, null);
						}
						else if (p is MultiPropInfo)
						{
							if ((p as MultiPropInfo).One2Many)
							{
								_slidingInfo = QueryConstructor.BuildMetadataByTable(rel.ChildTable, null);
							}
							else if (t.Attributes["ClassName"] != null)
							{
								_slidingInfo = QueryConstructor.BuildMetadataInfo((string)t.Attributes["ClassName"]);
							}
							else
							{
								_slidingInfo = QueryConstructor.BuildTableData(rel.ChildTable, null);
							}
						}
						Prop pi = _slidingInfo.FindProp(fieldName, 3);
						if (pi == null) return; //throw new Exception("Field "+fieldName+" not found in "+_slidingInfo.Classes[0].ClassName);
						field.FullFieldName += "_" + fieldName;
						field.Props.Add(pi);
						if (pi is PropInfo)
						{
							field.FieldName = fieldName;
							if (pi.TableName != rel.ChildTable)
							{
								RelationInfo ri = new RelationInfo();
								ri.IsInner = rel.IsInner;
								ri.ParentTable = rel.ChildTable;
								ri.ParentField = rel.ChildField;
								ri.ChildTable = pi.TableName;
								if (pi is PropInfo)
								{
									ri.ChildField = "OID";
								}
								else if (pi is LinkedProp)
								{
									if (t.Attributes["ClassName"] != null) ri.ChildField = "OID";
									else ri.ChildField = pi.PropName;
									if (t.Attributes["ClassName"] != null) field.FieldName = pi.PropName;
								}
								field.AddRelation(ri);
							}
							else
							{
								field.FieldName = fieldName;
								field.TableName = pi.TableName;
							}
						}
						else if (pi is MultiPropInfo)
						{
							field.FieldName = (pi as MultiPropInfo).FieldOID;
							field.TableName = pi.TableName;
						}
						else
						{
							field.FieldName = fieldName;
							field.TableName = pi.TableName;
						}
						p = pi;
						break;
					//////////////////// TODO: Сделать обработку *
					case LexAn.ALL:
						return;
					//////////////////// TODOEND: Сделать обработку *
					default:
						_lex.PutBack();
						return;
				}
			}
		}
		private void ParseFunction(FunctionInfo funct)
		{
			Match('(', _lex.Next());
			ParseArgs(funct);
			Match(')', _lex.Next());
		}
		private void ParseArgs(FunctionInfo funct)
		{
			while (true)
			{
				Token t = _lex.Next();
				switch (t.Tok)
				{
					case LexAn.ID:
						FieldExpr field = BuildField((string)t.TokenVal);
						funct.Args.Add(field);
						//if (field == null)
						//  funct.hasNotFoundedFields = true;//есть несуществующие поля
						//else 
						break;
					case LexAn.NUM:
					case LexAn.PARAM:
					case LexAn.STRING:
						funct.Args.Add(t.TokenVal);
						break;
					case LexAn.FUNC:
						FunctionInfo funct1 = BuildFunction((string)t.TokenVal);
						funct.Args.Add(funct1);
						break;
					case ',':
						break;
					default:
						_lex.PutBack();
						return;
				}
			}
		}
		public abstract string BuildQuery();
		public void LinkToBackbone(FieldInfo field)
		{
			TableDesc tbDesc = null;
			if (field.Relations.Length == 0) tbDesc = new TableDesc(field.TableName, field.TableNameAlias);
			else tbDesc = new TableDesc(field.Relations[0].ParentTable, field.Relations[0].ParentTableAlias);
			int pos = -1;
			foreach (ClassInfo item in _info.Classes)
			{
				pos++;
				if (item.TableName.ToUpper() == tbDesc.TableName.ToUpper()) break;
			}
			if (pos == _info.Classes.Length) throw new Exception("Error LinkToBackbone");
			if (_backboneAliases[pos] == null)
			{
				_backboneAliases[pos] = tbDesc;
				for (int i = pos - 1; i >= 0; i--)
				{
					if (_backboneAliases[i] != null)
					{
						RelationInfo rel = new RelationInfo();
						rel.IsInner = true;
						rel.ParentField = "OID";
						rel.ParentTable = _backboneAliases[i].TableName;
						rel.ParentTableAlias = _backboneAliases[i].TableAlias;
						rel.ChildField = "OID";
						rel.ChildTable = tbDesc.TableName;
						rel.ChildTableAlias = tbDesc.TableAlias;
						field.InsertHead(rel);
					}
				}
			}
		}
		public string FullKeyField()
		{
			for (int i = 0; i < _backboneAliases.Length; i++)
			{
				if (_backboneAliases[i] != null) return " [" + _backboneAliases[i].TableAlias + "].[OID] ";
			}
			throw new Exception("table not found");
		}

	}
	public class Joins : IList<FieldExpr>
	{
		private List<FieldExpr> innerList = new List<FieldExpr>();

		public Hashtable PropHierarhy = new Hashtable();
		public object[] DistinctMulti()
		{
			var arr = new object[MaxNumber() + 1];
			foreach (DictionaryEntry de in PropHierarhy)
			{
				var i = (int)((object[])de.Value)[1];
				var prms = new[] { de.Key, ((object[])de.Value)[0], ((object[])de.Value)[1], ((object[])de.Value)[2] };
				arr[i] = prms;
			}
			return arr.Where(obj => obj != null).ToArray();
		}
		public void BuildPropHierarhy(int i)
		{
			foreach (var fe in this)
			{
				foreach (var fi in fe.Fields)
				{
					var prevProp = "";
					var propName = "";
					foreach (var pr in fi.Props.OfType<MultiPropInfo>())
					{
						propName += pr.PropName + "_";
						if (PropHierarhy[propName] == null)
						{
							var parentProp = PropHierarhy[prevProp] == null ? i : (int)((object[])PropHierarhy[prevProp])[1];
							PropHierarhy[propName] = new object[] { parentProp, i++, fi };
						}
						prevProp = propName;
					}
				}

			}
		}
		public int MaxNumber()
		{
			int i = 0;
			foreach (DictionaryEntry de in PropHierarhy)
			{
				i = Math.Max(i, (int)((object[])de.Value)[1]);
			}
			return i;
		}
		public object[] GetObjectFieldPrefix(FieldExpr field)
		{
			if (field.Fields == null || field.Fields.Length == 0) return null;

			var fi = field.Fields[0];
			var propName = "";
			var lastProp = "";
			foreach (MultiPropInfo pr in fi.Props.OfType<MultiPropInfo>())
			{
				propName += pr.PropName + "_";
				lastProp = pr.PropName;
			}
			return new[] { lastProp, ((object[])PropHierarhy[propName])[1] };
		}
		private int _tableCount;
		public bool FieldInCollection(FieldInfo field, string collName)
		{
			var name = field.Props.OfType<MultiPropInfo>().Aggregate(new StringBuilder(), (sb, pr) => sb.AppendFormat("{0}_", pr.PropName), sb => sb.ToString());
			return name == collName;
		}
		public string BuildFromClause(string multiCollection)
		{
			var usedAlias = new Hashtable();
			var sb = new StringBuilder();
			if (Count != 0 && FieldInCollection(this[0].Fields[0], multiCollection))
			{
				var f = this[0].Fields[0];
				if (f.Relations.Length != 0)
				{
					sb.Append(@"
FROM
	[" + f.Relations[0].ParentTable + "] " + f.Relations[0].ParentTableAlias);
					usedAlias[f.Relations[0].ParentTableAlias] = true;
				}
				else
				{
					sb.Append(@"
FROM
	[" + f.TableName + "] " + f.TableNameAlias);
					usedAlias[f.TableNameAlias] = true;
				}
			}
			else
			{
				sb.Append(@"
FROM
	[t_Object] o");
			}
			foreach (RelationInfo ri in from fExpr in this where fExpr.Fields != null from fi in fExpr.Fields where fi != null && FieldInCollection(fi, multiCollection) from ri in fi.Relations where !usedAlias.ContainsKey(ri.ChildTableAlias) select ri)
			{
				sb.Append(@"
	" + (ri.IsInner ? "inner" : "left") + " join [" + ri.ChildTable + "] " + ri.ChildTableAlias + " on " + ri.ParentTableAlias + ".[" + ri.ParentField + "] = " + ri.ChildTableAlias + ".[" + ri.ChildField + "]");
				usedAlias[ri.ChildTableAlias] = true;
			}
			return sb.ToString();
		}
		public string BuildFromClause()
		{
			var usedAlias = new Hashtable();
			var sb = new StringBuilder();
			if (Count != 0)
			{
				var f = this[0].Fields[0];
				if (f.Relations.Length != 0)
				{
					sb.Append(@"
FROM
	[" + f.Relations[0].ParentTable + "] " + f.Relations[0].ParentTableAlias);
					usedAlias[f.Relations[0].ParentTableAlias] = true;
				}
				else
				{
					sb.Append(@"
FROM
	[" + f.TableName + "] " + f.TableNameAlias);
					usedAlias[f.TableNameAlias] = true;
				}
			}
			else
			{
				sb.Append(@"
FROM
	[t_Object] o");
			}
			foreach (var ri in from fExpr in this where fExpr.Fields != null from fi in fExpr.Fields from ri in fi.Relations where !usedAlias.ContainsKey(ri.ChildTableAlias) select ri)
			{
				sb.Append(@"
	" + (ri.IsInner ? "inner" : "left") + " join [" + ri.ChildTable + "] " + ri.ChildTableAlias + " on " + ri.ParentTableAlias + ".[" + ri.ParentField + "] = " + ri.ChildTableAlias + ".[" + ri.ChildField + "]");
				usedAlias[ri.ChildTableAlias] = true;
			}
			return sb.ToString();
		}
		private void BuildAliases(FieldInfo field)
		{
			if (string.IsNullOrEmpty(field.TableNameAlias))
			{
				foreach (var fExpr in this.Where(fExpr => fExpr.Fields != null))
				{
					foreach (var fi in fExpr.Fields)
					{
						//Заплатка для фильтров и ордеров - иногда для полей, по которым идёт фильтрация не надо 
						//делать ещё один JOIN, а использовать существующий (если поле было запрошено ранее)
						if ((field.Context == FieldContext.Filter || field.Context == FieldContext.Order)
							&& field.TableName == fi.TableName && field.Relations.Length == fi.Relations.Length)
						{//естественно равенство названий таблиц и количество отношений мало - надо сравнить сами отношения
							bool allRelEqual = true;
							for (int i = 0; i < field.Relations.Length; i++)
							{
								if (!field.Relations[i].Equals(fi.Relations[i]))
								{
									allRelEqual = false;
									break;
								}
							}

							if (allRelEqual)//нашли эквивалентную колонку
								field.TableNameAlias = fi.TableNameAlias;
						}

						if (fi.Relations.Length != 0)
						{
							if (field.Relations.Length == 0)
							{
								if (field.TableName == fi.Relations[0].ParentTable)
								{
									field.TableNameAlias = fi.Relations[0].ParentTableAlias;
									break;
								}
								continue;
							}
							for (int i = 0; i < field.Relations.Length; i++)
							{
								var newRel = field.Relations[i];
								if (fi.Relations.Length <= i) break;
								var oldRel = fi.Relations[i];
								if (!newRel.Equals(oldRel))
									break;
								newRel.ParentTableAlias = oldRel.ParentTableAlias;
								newRel.ChildTableAlias = oldRel.ChildTableAlias;
							}
						}
						else
						{
							if (field.Relations.Length == 0)
							{
								if (field.TableName == fi.TableName)
								{
									field.TableNameAlias = fi.TableNameAlias;
									break;
								}
								continue;
							}
							if (field.Relations[0].ParentTable == fi.TableName)
							{
								field.Relations[0].ParentTableAlias = fi.TableNameAlias;
								//break; - из-за этого брейка не работал проход по всем полям, хотя не факт что мы дальше не найдём алиас
							}
						}
					}
				}
			}

			for (var i = 0; i < field.Relations.Length; i++)
			{
				var ri = field.Relations[i];
				if (ri.ParentTableAlias == null)
				{
					if (i == 0) ri.ParentTableAlias = "i" + (_tableCount++).ToString();
					else ri.ParentTableAlias = field.Relations[i - 1].ChildTableAlias;
				}
				if (ri.ChildTableAlias == null) ri.ChildTableAlias = "i" + (_tableCount++).ToString();
			}

			if (string.IsNullOrEmpty(field.TableNameAlias))
			{
				if (field.Relations.Length != 0) field.TableNameAlias = field.Relations[field.Relations.Length - 1].ChildTableAlias;
				else field.TableNameAlias = "i" + (_tableCount++).ToString();
			}
		}

		public int IndexOf(FieldExpr item)
		{
			return innerList.IndexOf(item);
		}

		public void Insert(int index, FieldExpr item)
		{
			throw new NotImplementedException();
		}

		public void RemoveAt(int index)
		{
			innerList.RemoveAt(index);
		}

		public FieldExpr this[int index]
		{
			get { return innerList[index]; }
			set
			{
				throw new NotImplementedException();
			}
		}

		public void Add(FieldExpr item)
		{
			if (item.Fields != null)
			{
				foreach (var fi in item.Fields)
				{
					if ((item.ContextObject is FilterInfo) && (item.ContextObject as FilterInfo).TableAlias != "")
					{
						var flt = (FilterInfo)item.ContextObject;
						if (fi != null)
						{
							if (fi.Relations.Length != 0) fi.Relations[0].ChildTableAlias = flt.TableAlias;
							else fi.TableNameAlias = flt.TableAlias;
						}
					}
					if (fi != null)
					{
						BuildAliases(fi);
					}
				}
			}

			innerList.Add(item);
		}


		public void Clear()
		{
			innerList.Clear();
		}

		public bool Contains(FieldExpr item)
		{
			return innerList.Contains(item);
		}

		public void CopyTo(FieldExpr[] array, int arrayIndex)
		{
			throw new NotImplementedException();
		}

		public int Count
		{
			get { return innerList.Count; }
		}

		public bool IsReadOnly
		{
			get { return false; }
		}

		public bool Remove(FieldExpr item)
		{
			return innerList.Remove(item);
		}

		public IEnumerator<FieldExpr> GetEnumerator()
		{
			return innerList.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return innerList.GetEnumerator();
		}
	}
	public class FieldContainer : ICollection
	{
		private Hashtable _items = new Hashtable();
		public void Add(string root, FieldExpr field)
		{
			if (!_items.ContainsKey(root)) _items[root] = new Joins();
			Joins arr = (Joins)_items[root];
			arr.Add(field);
		}
		public int Count
		{
			get { return _items.Count; }
		}
		public bool IsSynchronized
		{
			get { return _items.IsSynchronized; }
		}
		public object SyncRoot
		{
			get { return _items.SyncRoot; }
		}
		public void CopyTo(Array array, int index)
		{
			_items.CopyTo(array, index);
		}
		public IEnumerator GetEnumerator()
		{
			return _items.GetEnumerator();
		}
	}
	public class TableDesc
	{
		public string TableName;
		public string TableAlias;
		public TableDesc(string tableName, string tableAlias)
		{
			TableName = tableName;
			TableAlias = tableAlias;
		}
	}
}
