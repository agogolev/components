using System;
using System.Collections;
using MetaData;

namespace DBReader
{
	/// <summary>
	/// Summary description for LexAn.
	/// </summary>
	public class LexAn {
		public const int EOF = 256;
		public const int NUM = 257;
		public const int ID = 258;
		public const int ALL = 259;
		public const int RELATION = 260;
		public const int FUNC = 261;
		public const int STRING = 262;
		public const int PARAM = 263;

		private string _parseStr;
		private int _tokenBegin;
		private int _current;
		private char t;
		public Hashtable Symbols;
		public LexAn(string parseStr) {
			_parseStr = parseStr;
			_tokenBegin = 0;
			_current = 0;
			Symbols = new Hashtable();
			InitSymbols();
		}
		private void InitSymbols() {
			// Add Reserved words to Symbol table;
			Symbols["GETDATE"] = true;
			Symbols["CONVERT"] = true;
			Symbols["DATALENGTH"] = true;
		}
		public Token Next() {
			_tokenBegin = _current;
			while(true) {
				NextNonBlank();
				if(t == (char)EOF) {
					return new Token(EOF, EOF);
				}
				else if(IsNumberBegin()) {
					bool isInt = true;
					do{
						NextChar();
						if (IsDecimal()) isInt = false;
					}while(IsDigit() || IsDecimal());
					PutBack();
					string value = ValueStr.Trim();
					return new Token(NUM, (isInt ? Int32.Parse(value) : Decimal.Parse(value, System.Globalization.CultureInfo.InvariantCulture.NumberFormat)));
				}
				else if(IsAlpha()) {
					do{
						NextChar();
					}while(IsAlphaNum());
					if(IsBlank()) NextNonBlank();
					if (t == '(') {
						PutBack();
						return new Token(FUNC, ValueStr.Trim());
					}
					else if(t == '[') {
						PutBack();
						Token toc = new Token(ID, ValueStr.Trim());
						AddClassAttr(toc);
						return toc;
					}
					else {
						PutBack();
						return new Token(ID, ValueStr.Trim());
					}
				}
				else if(t == '.' || t == '?') {
					char rel = t;
					return new Token(RELATION, rel);
				}
				else if(t == '@') {
					do{
						NextChar();
					}while(IsAlphaNum());
					PutBack();
					string value = ValueStr.Trim();
					return new Token(PARAM, value);
				}
				else if(t == '\''){
					do {
						NextChar();
					}while(!IsStringEnd());
					_current++;
					string value = ValueStr.Trim();
					return new Token(STRING, value);
				}
				else if(t == '*') {
					PutBack();
					return new Token(ALL, null);
				}
				else return new Token(t, t);
				
			}
		}
		private void AddClassAttr(Token toc) {
			_tokenBegin = ++_current;
			while(true) {
				NextNonBlank();
				if(t == ']') {
					PutBack();
					toc.Attributes["ClassName"] = ValueStr.Trim();
					//_current += 2;
					return;
				}
			}
		}
		/*private char NChar {
			get {
				if (_current == _parseStr.Length) return (char)EOF;
				return _parseStr[_current++]; <--- ÍÅËÜÇß ÑÎÇÄÀÂÀÒÜ ÏÐÎÏÅÐÒß Ñ ÈÇÌÅÍÅÍÈÅÌ ÄÐÓÃÈÕ ÄÀÍÍÛÕ ÂÍÓÒÐÈ !!!! (ÏÓÑÒÜ ÄÀÆÅ ÏÐÀÉÂÅÒ)
			}
		}*/
		private void NextChar() {
			if (_current == _parseStr.Length)
				t = (char)EOF;
			else
				t = _parseStr[_current++];
		} 
		private string ValueStr {
			get {return _parseStr.Substring(_tokenBegin, _current-_tokenBegin);}
		}
		public void PutBack() {
			if(t != (char)LexAn.EOF) _current--;
		}
		private void NextNonBlank() {
			do {
				NextChar();
			}
			while(t == ' ' || t == '\t' || t == '\n' || t == '\r');
		}
		private bool IsBlank() {
			if (t == ' ' || t == '\t' || t == '\n' || t == '\r') return true;
			else return false;
		}
		private bool IsDigit() {
			if (t >= '0' && t <='9') return true;
			else return false;
		}
		private bool IsSign() {
			if (t == '+' || t == '-') return true;
			else return false;
		}
		private bool IsDecimal() {
			if (t == '.') return true;
			else return false;
		}
		private bool IsNumberBegin() {
			bool result = IsDigit();
			if (result) return true;
			result = IsSign();
			if (result) return true;
			else return false;
		}
		private bool IsAlpha() {
			if((t >='a' && t<='z') || (t >='A' && t<='Z') || t == '_') return true;
			else return false;
		}
		private bool IsAlphaNum() {
			bool result = IsDigit();
			if (result) return true;
			result = IsAlpha();
			if (result) return true;
			else return false;
		}

		private bool IsStringEnd() {
			if(t == '\'') {
				int slashNumber = 0;
				int i = --_current;
				while(_parseStr[i--] == '\\') {
					slashNumber++;
				}
				if((slashNumber / 2) == 0) return true;
				else return false;
			}
			else return false;
		}
	}


	public class Token {
		private int _token;
		private Hashtable _attr;
		public Hashtable Attributes {
			get {
				if (_attr == null) _attr = new Hashtable();
				return _attr;
			}
		}
		public int Tok {
			get{ return _token;}
		}
		private object _tokenVal;
		public object TokenVal {
			get{ return _tokenVal;}
		}
		public Token(int token, object tokenVal) {
			_token = token;
			_tokenVal = tokenVal;
		}
	}
}
