using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using MetaData;

namespace DBReader
{
	/// <summary>
	/// Summary description for GetObjectBuilder.
	/// </summary>
	public class GetObjectBuilder : ObjectQueryParser
	{
		public Joins SingleFields { get; private set; }

		public FieldContainer MultiFields { get; private set; }

		public GetObjectBuilder(MetadataInfo info)
			: base(info)
		{
			SingleFields = new Joins();
			MultiFields = new FieldContainer();
		}
		public void AddSingleField(string fieldName, string columnName)
		{
			_lex = new LexAn(fieldName);
			_slidingInfo = _info;
			FieldExpr field = null;
			Token t = _lex.Next();
			switch (t.Tok)
			{
				case LexAn.FUNC:
					field = BuildFunction((string)t.TokenVal);
					break;
				case LexAn.ID:
					field = BuildField((string)t.TokenVal);
					break;
				case LexAn.STRING:
				case LexAn.NUM:
				case LexAn.PARAM:
					field = BuildConstField((string)t.TokenVal);
					break;
				case LexAn.ALL:
					break;
				default:
					Error(t, "AddSingleField");
					break;
			}
			if (field != null)
			{
				SingleFields.Add(field);
				if (!string.IsNullOrEmpty(columnName)) field.FullFieldName = columnName;
			}
		}
		public void AddMultiField(string fieldName, string columnName, string order)
		{
			_lex = new LexAn(fieldName);
			FieldExpr field = null;
			string root = "";
			Token t = _lex.Next();
			switch (t.Tok)
			{
				case LexAn.FUNC:
					field = BuildFunction((string)t.TokenVal);
					root = MultiRoot(field.Fields);//.FullFieldName;
					break;
				case LexAn.ID:
					field = BuildField((string)t.TokenVal);
					if (!(field is FieldInfo)) throw new Exception("error resolve multi field: " + t.TokenVal);
					root = MultiRoot(new[] { (FieldInfo)field });
					break;
				case LexAn.STRING:
				case LexAn.NUM:
				case LexAn.PARAM:
					field = BuildConstField((string)t.TokenVal);
					break;
				case LexAn.ALL:
					break;
				default:
					Error(t, "AddMultiField");
					break;
			}
			if (field != null)
			{
				if (!string.IsNullOrEmpty(order))
				{
					field.isOrdered = true;
					field.OrderDir = order;
				}
				MultiFields.Add(root, field);
				if (!string.IsNullOrEmpty(columnName)) field.FullFieldName = columnName;
			}
		}
		private string MultiRoot(IEnumerable<FieldInfo> fields)
		{
			if (fields == null) return "";
			foreach (var field in fields)
			{
				foreach (Prop pi in field.Props)
				{
					if (pi is MultiPropInfo)
					{
						return pi.PropName;
					}
				}
			}

			return "";
		}
		protected override void ParseField(FieldInfo field, Prop p)
		{
			base.ParseField(field, p);
			var riStart = new RelationInfo
			              	{
			              		ParentTable = "t_Object",
			              		ParentField = "OID",
			              		ParentTableAlias = "o",
			              		ChildField = "OID",
			              		IsInner = true
			              	};
			field.InsertHead(riStart);
		}

		public override string BuildQuery()
		{
			var _hasBinaryField = false;
			var order = new StringBuilder();
			var sb = new StringBuilder();
			sb.Append(@"
SELECT
	Tag = 1,
	Parent = null,
	[item!1!OID] = o.[OID]");
			foreach (var field in SingleFields)
			{
				sb.Append(@",
	[item!1!" + field.FullFieldName + "!element] = " + field.Expr());
				if (field is FieldInfo && ((FieldInfo)field).FieldType == DataType.Binary) _hasBinaryField = true;
			}
			var iterate = MultiFields.GetEnumerator();
			var i = 1;
			while (iterate.MoveNext())
			{
				i++;
				var entry = (DictionaryEntry)iterate.Current;
				var fields = (Joins)entry.Value;
				fields.BuildPropHierarhy(i);
				//FindKeyFields(fields);
				foreach (var field in fields)
				{
					var prms = fields.GetObjectFieldPrefix(field);
					sb.Append(@",
	[" + prms[0] + "!" + ((int)prms[1]) + "!" + field.FullFieldName + "!element] = null");
					if (field.isOrdered)
					{
						order.Append("[" + prms[0] + "!" + ((int)prms[1]) + "!" + field.FullFieldName + "!element] " + field.OrderDir + ", ");
					}
				}
				i = fields.MaxNumber();
			}
			sb.Append(SingleFields.BuildFromClause());
			sb.Append(@"
WHERE
	o.[OID] = @OID");
			iterate.Reset();
			i = 1;
			while (iterate.MoveNext())
			{
				i++;
				var entry = (DictionaryEntry)iterate.Current;
				var fields = (Joins)entry.Value;
				var multi = fields.DistinctMulti();
				foreach (var obj in multi)
				{
					var collName = (string)((object[])obj)[0];
					var parentNumber = (int)((object[])obj)[1];
					var number = (int)((object[])obj)[2];
					if (parentNumber == number) parentNumber = 1;
					var parentTableAlias = fields[0].Fields[0].Relations[0].ParentTableAlias;
					var parentField = fields[0].Fields[0].Relations[0].ParentField;
					sb.Append(@"
UNION ALL
SELECT
	" + number + @",
	" + parentNumber + @",
	" + parentTableAlias + ".[" + parentField + "]");
					for (var k = 0; k < SingleFields.Count; k++)
					{
						sb.Append(@",
	null");
					}
					var innerIterate = MultiFields.GetEnumerator();
					while (innerIterate.MoveNext())
					{
						var innerFields = (Joins)((DictionaryEntry)innerIterate.Current).Value;
						if (fields != innerFields)
						{
							for (i = 0; i < innerFields.Count; i++)
							{
								sb.Append(@",
	null");
							}
						}
						else
						{
							foreach (var field in innerFields)
							{
								if (innerFields.FieldInCollection(field.Fields[0], collName))
								{
									sb.Append(@",
	" + field.Expr());
								}
								else
								{
									if (field.isOrdered)
									{
										sb.Append(@",
	" + field.Expr());
									}
									else
									{
										sb.Append(@",
	null");
									}
								}
							}
						}
					}
					sb.Append(fields.BuildFromClause(collName));
					sb.Append(@"
WHERE
	" + parentTableAlias + ".[" + parentField + "] = @OID");
				}
			}
			sb.Append(@"
ORDER BY
	" + order + @" Tag
FOR XML EXPLICIT");
			if (_hasBinaryField) sb.Append(", BINARY BASE64");
			return sb.ToString();
		}
		public string BuildMTQuery()
		{
			var sb = new StringBuilder();
			sb.Append(@"
SELECT
	o.[OID]");
			foreach (var field in SingleFields)
			{
				sb.Append(@",
	" + field.FullFieldName + " = " + field.Expr());
			}
			var iterate = MultiFields.GetEnumerator();
			var i = 1;
			while (iterate.MoveNext())
			{
				i++;
				var entry = (DictionaryEntry)iterate.Current;
				var fields = (Joins)entry.Value;
				fields.BuildPropHierarhy(i);
				i = fields.MaxNumber();
			}
			sb.Append(SingleFields.BuildFromClause());
			sb.Append(@"
WHERE
	o.[OID] = @OID");
			iterate.Reset();
			i = 1;
			while (iterate.MoveNext())
			{
				i++;
				var entry = (DictionaryEntry)iterate.Current;
				var fields = (Joins)entry.Value;
				var multi = fields.DistinctMulti();
				foreach (var obj in multi)
				{
					var collName = (string)((object[])obj)[0];
					var parentTableAlias = fields[0].Fields[0].Relations[0].ParentTableAlias;
					var parentField = fields[0].Fields[0].Relations[0].ParentField;
					sb.Append(@"

SELECT
	" + parentTableAlias + ".[" + parentField + "]");
					var innerIterate = MultiFields.GetEnumerator();
					while (innerIterate.MoveNext())
					{
						var innerFields = (Joins)((DictionaryEntry)innerIterate.Current).Value;
						if (fields != innerFields) continue;
						foreach (var field in innerFields)
						{
							if (field.Fields == null) break;
							foreach (var fi in field.Fields)
							{
								if (innerFields.FieldInCollection(fi, collName))
								{
									sb.Append(@",
	" + field.FullFieldName + " = " + field.Expr());
								}
								else
								{
									if (field.isOrdered)
									{
										sb.Append(@",
	" + field.FullFieldName + " = " + field.Expr());
									}
								}
							}

						}
					}
					sb.Append(fields.BuildFromClause(collName));
					sb.Append(@"
WHERE
	" + parentTableAlias + ".[" + parentField + "] = @OID");
				}
			}
			return sb.ToString();
		}
		public string BuildDataSetSQL()
		{
			var sb = new StringBuilder();
			sb.Append(@"
SELECT");
			if (MultiFields.Count != 1) throw new Exception("Invalid number of collections");
			var iterate = MultiFields.GetEnumerator();
			if (iterate.MoveNext())
			{
				var entry = (DictionaryEntry)iterate.Current;
				var fields = (Joins)entry.Value;
				var parentTableAlias = fields[0].Fields[0].Relations[0].ParentTableAlias;
				var parentField = fields[0].Fields[0].Relations[0].ParentField;
				foreach (var field in fields)
				{
					sb.Append(@"
	[" + field.TruncFieldName(1) + "] = " + field.Expr() + ",");
				}
				sb.Length -= 1;
				sb.Append(fields.BuildFromClause());
				sb.Append(@"
WHERE
	" + parentTableAlias + ".[" + parentField + "] = @OID");
			}
			return sb.ToString();
		}
	}
}

