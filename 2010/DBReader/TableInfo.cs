using System;
using MetaData;

namespace DBReader
{
	#region Классы для построения обработчика коллекций
	public class TableInfo
	{
		public string TableName;
		public string[] PrimaryKey;
		public PropInfo[] Fields;
		public PropInfo GetField(string name)
		{
			foreach (PropInfo pi in Fields)
			{
				if (pi.PropName.ToLower() == name.ToLower())
				{
					return pi;
				}
			}
			return null;
		}
		public bool IsPrimaryKey(string name)
		{
			foreach (string key in PrimaryKey)
			{
				if (key.ToLower() == name.ToLower())
				{
					return true;
				}
			}
			return false;
		}
	}
	#endregion
}
