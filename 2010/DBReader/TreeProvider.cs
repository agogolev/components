using System;
using System.Data;
using System.Data.SqlClient;
using System.Xml;
using System.Collections;
using System.Text;

namespace DBReader
{
	/// <summary>
	/// Summary description for TreeProvider.
	/// </summary>
	public class TreeProvider
	{
		public static DataSet GetTreeContent(string xml, UserInfo usr)
		{
			XmlDocument doc = new XmlDocument();
			string sql;
			Hashtable prms = new Hashtable();
			doc.LoadXml(xml);
			Guid treeOID = Guid.Empty;
			int nodeID = 0;
			XmlAttribute attr = doc.DocumentElement.Attributes["OID"];
			if (attr == null)
			{
				attr = doc.DocumentElement.Attributes["treeName"];
				if (attr == null)
				{
					attr = doc.DocumentElement.Attributes["nodeID"];
					if (attr == null) throw new Exception("tree not defined");
					nodeID = int.Parse(attr.Value);
					sql = "SELECT tree FROM t_Nodes WHERE nodeID = @nodeID";
					prms["@nodeID"] = nodeID;
					treeOID = (Guid)DBHelper.ExecuteScalar(sql, prms, false);
				}
				else
				{
					string treeName = attr.Value.Trim();
					sql = "SELECT OID FROM t_Tree WHERE treeName = @treeName";
					prms["@treeName"] = treeName;
					treeOID = (Guid)DBHelper.ExecuteScalar(sql, prms, false);
				}
			}
			else
			{
				treeOID = new Guid(attr.Value);
			}
			attr = doc.DocumentElement.Attributes["nodeID"];
			if (attr != null) nodeID = Convert.ToInt32(attr.Value);
			bool withViewDates = false;
			attr = doc.DocumentElement.Attributes["withViewDates"];
			if (attr != null) withViewDates = Convert.ToBoolean(attr.Value);
			bool withNodeLabels = false;
			attr = doc.DocumentElement.Attributes["withNodeLabels"];
			if (attr != null) withNodeLabels = Convert.ToBoolean(attr.Value);
			bool withParent = false;
			attr = doc.DocumentElement.Attributes["withParent"];
			if (attr != null) withParent = Convert.ToBoolean(attr.Value);
			MetaData.MetadataInfo info = null;
			attr = doc.DocumentElement.Attributes["className"];
			string fieldNames = null;
			if (attr != null)
			{
				string className = attr.Value;
				info = QueryConstructor.BuildMetadataInfo(className);
				XmlNode n = doc.DocumentElement.SelectSingleNode("fieldNames");
				if (n != null) fieldNames = n.InnerText;
			}
			sql = QueryConstructor.BuildGetTreeContent(info, fieldNames, nodeID, withViewDates, withNodeLabels, withParent);
			prms.Clear();
			prms["@OID"] = treeOID;
			if (nodeID != 0) prms["@nodeID"] = nodeID;
			//return sql;
			return DBHelper.GetDataSetSql(sql, "table", prms, false, usr);
		}
		public static int CreateNode(string xml)
		{
			XmlDocument doc = new XmlDocument();
			doc.LoadXml(xml);
			Guid treeOID = Guid.Empty;
			XmlAttribute attr = doc.DocumentElement.Attributes["treeOID"];
			if (attr != null) treeOID = new Guid(attr.Value);
			int parentNode = 0;
			attr = doc.DocumentElement.Attributes["parentNode"];
			if (attr != null) parentNode = Convert.ToInt32(attr.Value);
			int leftSibling = 0;
			attr = doc.DocumentElement.Attributes["leftSibling"];
			if (attr != null) leftSibling = Convert.ToInt32(attr.Value);
			string nodeName = null;
			attr = doc.DocumentElement.Attributes["nodeName"];
			if (attr != null) nodeName = attr.Value;
			string nodeUrl = null;
			attr = doc.DocumentElement.Attributes["nodeUrl"];
			if (attr != null) nodeUrl = attr.Value;
			Guid objectOID = Guid.Empty;
			attr = doc.DocumentElement.Attributes["object"];
			if (attr != null) objectOID = new Guid(attr.Value);
			DateTime dateBegin = DateTime.MinValue, dateEnd = DateTime.MinValue;
			attr = doc.DocumentElement.Attributes["dateBegin"];
			if (attr != null) dateBegin = Convert.ToDateTime(new System.Globalization.CultureInfo("ru-RU").DateTimeFormat);
			attr = doc.DocumentElement.Attributes["dateEnd"];
			if (attr != null) dateEnd = Convert.ToDateTime(new System.Globalization.CultureInfo("ru-RU").DateTimeFormat);
			SqlCommand cmd = QueryConstructor.GetCommand("spInnerCreateNode", CommandType.StoredProcedure, null);
			cmd.Parameters.AddWithValue("@tree", (treeOID == Guid.Empty ? DBNull.Value : (object)treeOID));
			cmd.Parameters.AddWithValue("@parentNode", (parentNode == 0 ? DBNull.Value : (object)parentNode));
			cmd.Parameters.AddWithValue("@leftSibling", (leftSibling == 0 ? DBNull.Value : (object)leftSibling));
			cmd.Parameters.AddWithValue("@nodeName", (nodeName == null ? DBNull.Value : (object)nodeName));
			cmd.Parameters.AddWithValue("@nodeURL", (nodeUrl == null ? DBNull.Value : (object)nodeUrl));
			cmd.Parameters.AddWithValue("@object", (objectOID == Guid.Empty ? DBNull.Value : (object)objectOID));
			SqlParameter p = cmd.Parameters.Add("@nodeID", SqlDbType.Int);
			p.Direction = ParameterDirection.Output;
			int nodeID;
			cmd.Connection.Open();
			try
			{
				cmd.ExecuteNonQuery();
				nodeID = (int)p.Value;
				cmd.CommandType = CommandType.Text;
				if (dateBegin != DateTime.MinValue || dateEnd != DateTime.MinValue)
				{
					string sql = @"
INSERT INTO t_NodeView (nodeID, dateBegin, dateEnd)
VALUES (@nodeID, @dateBegin, @dateEnd)";
					cmd.CommandText = sql;
					cmd.Parameters.Clear();
					cmd.Parameters.AddWithValue("@nodeID", nodeID);
					cmd.Parameters.AddWithValue("@dateBegin", (dateBegin == DateTime.MinValue ? DBNull.Value : (object)dateBegin));
					cmd.Parameters.AddWithValue("@dateEnd", (dateEnd == DateTime.MinValue ? DBNull.Value : (object)dateEnd));
					cmd.ExecuteNonQuery();
				}
				XmlNodeList nl = doc.DocumentElement.SelectNodes("add");
				foreach (XmlNode n in nl)
				{
					string sql = @"INSERT INTO t_NodeLabels VALUES(@nodeID, @ordValue, @value, @url)";
					cmd.CommandText = sql;
					cmd.Parameters.Clear();
					cmd.Parameters.AddWithValue("@nodeID", nodeID);
					cmd.Parameters.AddWithValue("@ordValue", n.Attributes["ordValue"].Value);
					cmd.Parameters.AddWithValue("@value", n.Attributes["value"].Value);
					cmd.Parameters.AddWithValue("@url", n.Attributes["url"].Value);
					cmd.ExecuteNonQuery();
				}
			}
			finally
			{
				cmd.Connection.Close();
			}
			return nodeID;
		}
		public static void UpdateNode(string xml)
		{
			//StringBuilder fieldNames = new StringBuilder();
			//StringBuilder fieldValues = new StringBuilder();
			XmlDocument fieldsXml = new XmlDocument();
			fieldsXml.LoadXml("<?xml version=\"1.0\"?><fields></fields>");
			XmlElement fieldNode;
			XmlElement innerNode;

			XmlDocument doc = new XmlDocument();
			doc.LoadXml(xml);

			int nodeID = 0;
			XmlAttribute attr = doc.DocumentElement.Attributes["nodeID"];
			if (attr != null) nodeID = Convert.ToInt32(attr.Value);
			else throw new Exception("node not defined");

			string nodeName = null;
			attr = doc.DocumentElement.Attributes["nodeName"];
			if (attr != null) nodeName = attr.Value;

			string nodeUrl = null;
			attr = doc.DocumentElement.Attributes["nodeUrl"];
			if (attr != null) nodeUrl = attr.Value;

			Guid objectOID = Guid.Empty;
			bool isEmptyObject = false;
			attr = doc.DocumentElement.Attributes["object"];
			if (attr != null)
			{
				objectOID = new Guid(attr.Value);
				if (objectOID == Guid.Empty) isEmptyObject = true;
			}

			DateTime dateBegin = DateTime.MinValue, dateEnd = DateTime.MinValue;
			attr = doc.DocumentElement.Attributes["dateBegin"];
			if (attr != null) dateBegin = Convert.ToDateTime(new System.Globalization.CultureInfo("ru-RU").DateTimeFormat);

			attr = doc.DocumentElement.Attributes["dateEnd"];
			if (attr != null) dateEnd = Convert.ToDateTime(new System.Globalization.CultureInfo("ru-RU").DateTimeFormat);

			if (nodeName != null)
			{
				/*fieldNames.Append("nodeName,");
				fieldValues.Append(DBHelper.ToOurString(nodeName));
				fieldValues.Append("'',''");*/
				fieldNode = fieldsXml.CreateElement("field");
				innerNode = fieldsXml.CreateElement("name");
				innerNode.InnerText = "nodeName";
				fieldNode.AppendChild(innerNode);
				innerNode = fieldsXml.CreateElement("value");
				innerNode.InnerText = DBHelper.ToOurString(nodeName);
				fieldNode.AppendChild(innerNode);
				fieldsXml.DocumentElement.AppendChild(fieldNode);
			}
			if (nodeUrl != null)
			{
				/*fieldNames.Append("nodeUrl,");
				fieldValues.Append(DBHelper.ToOurString(nodeUrl));
				fieldValues.Append("'',''");*/
				fieldNode = fieldsXml.CreateElement("field");
				innerNode = fieldsXml.CreateElement("name");
				innerNode.InnerText = "nodeUrl";
				fieldNode.AppendChild(innerNode);
				innerNode = fieldsXml.CreateElement("value");
				innerNode.InnerText = DBHelper.ToOurString(nodeUrl);
				fieldNode.AppendChild(innerNode);
				fieldsXml.DocumentElement.AppendChild(fieldNode);
			}
			if (objectOID != Guid.Empty || isEmptyObject)
			{
				/*fieldNames.Append("object,");
				fieldValues.Append((!isEmptyObject ? DBHelper.ToOurString(objectOID) : "null"));
				fieldValues.Append("'',''");*/
				fieldNode = fieldsXml.CreateElement("field");
				innerNode = fieldsXml.CreateElement("name");
				innerNode.InnerText = "object";
				fieldNode.AppendChild(innerNode);
				innerNode = fieldsXml.CreateElement("value");
				innerNode.InnerText = (!isEmptyObject ? DBHelper.ToOurString(objectOID) : "null");
				fieldNode.AppendChild(innerNode);
				fieldsXml.DocumentElement.AppendChild(fieldNode);
			}

			SqlCommand cmd = QueryConstructor.GetCommand("", CommandType.Text, null);
			cmd.Connection.Open();
			try
			{
				/*if(fieldNames.Length != 0) 
				{
					fieldNames.Length -= 1;
					fieldValues.Length -= 5;
					string sql = "spUpdateNode "+nodeID.ToString()+", '"+
						fieldNames.ToString()+"', '"+
						fieldValues.ToString()+"'";
					cmd.CommandText = sql;
					cmd.ExecuteNonQuery();
				}*/
				if (fieldsXml.DocumentElement.ChildNodes.Count > 0)
				{
					string sql = "spUpdateNode " + nodeID.ToString() + ", null, '" + fieldsXml.OuterXml + "'";
					cmd.CommandText = sql;
					cmd.ExecuteNonQuery();
				}

				if (dateBegin != DateTime.MinValue || dateEnd != DateTime.MinValue)
				{
					string sql = @"
DELETE FROM t_NodeView WHERE nodeID = @nodeID
INSERT INTO t_NodeView (nodeID, dateBegin, dateEnd)
VALUES (@nodeID, @dateBegin, @dateEnd)";
					cmd.CommandText = sql;
					cmd.Parameters.Clear();
					cmd.Parameters.AddWithValue("@nodeID", nodeID);
					cmd.Parameters.AddWithValue("@dateBegin", (dateBegin == DateTime.MinValue ? DBNull.Value : (object)dateBegin));
					cmd.Parameters.AddWithValue("@dateEnd", (dateEnd == DateTime.MinValue ? DBNull.Value : (object)dateEnd));
					cmd.ExecuteNonQuery();
				}
				else
				{
					string sql = @"DELETE FROM t_NodeView WHERE nodeID = @nodeID";
					cmd.CommandText = sql;
					cmd.Parameters.Clear();
					cmd.Parameters.AddWithValue("@nodeID", nodeID);
					cmd.ExecuteNonQuery();
				}
				XmlNodeList nl = doc.DocumentElement.SelectNodes("delete");
				foreach (XmlNode n in nl)
				{
					string sql = @"DELETE FROM t_NodeLabels WHERE nodeID = @nodeID And ordValue = @ordValue";
					cmd.CommandText = sql;
					cmd.Parameters.Clear();
					cmd.Parameters.AddWithValue("@nodeID", nodeID);
					cmd.Parameters.AddWithValue("@ordValue", n.Attributes["ordValue"].Value);
					cmd.ExecuteNonQuery();
				}
				nl = doc.DocumentElement.SelectNodes("add");
				foreach (XmlNode n in nl)
				{
					string sql = @"INSERT INTO t_NodeLabels VALUES(@nodeID, @ordValue, @value, @url)";
					cmd.CommandText = sql;
					cmd.Parameters.Clear();
					cmd.Parameters.AddWithValue("@nodeID", nodeID);
					cmd.Parameters.AddWithValue("@ordValue", n.Attributes["ordValue"].Value);
					cmd.Parameters.AddWithValue("@value", n.Attributes["value"].Value);
					cmd.Parameters.AddWithValue("@url", n.Attributes["url"].Value);
					cmd.ExecuteNonQuery();
				}
				nl = doc.DocumentElement.SelectNodes("update");
				foreach (XmlNode n in nl)
				{
					string sql = @"UPDATE t_NodeLabels SET value = @value, url = @url WHERE nodeID = @nodeID And ordValue = @ordValue";
					cmd.CommandText = sql;
					cmd.Parameters.Clear();
					cmd.Parameters.AddWithValue("@nodeID", nodeID);
					cmd.Parameters.AddWithValue("@ordValue", n.Attributes["ordValue"].Value);
					cmd.Parameters.AddWithValue("@value", n.Attributes["value"].Value);
					cmd.Parameters.AddWithValue("@url", n.Attributes["url"].Value);
					cmd.ExecuteNonQuery();
				}
			}
			finally
			{
				cmd.Connection.Close();
			}
			return;
		}
	}
}
