using System;
using System.Web;
using System.Collections;
using System.Configuration;
using System.Net;
using System.IO;
using System.Text;

namespace DBReader
{
	/// <summary>
	/// Summary description for WebServiceExtensions.
	/// </summary>
	public abstract class WebServiceExtensions
	{
		private WebServiceExtensions()
		{ }
		public static Guid ServiceTicket = new Guid("7CDDC52B-1ED2-4F4B-8B5F-A56B509122E5");
		public static void ClearWebCache(string Prefix)
		{
			string urlsString = ConfigurationManager.AppSettings["cacheControlUrl"];
			if (urlsString == null) return;
			string key = ConfigurationManager.AppSettings["cacheControl"];
			string[] urls = urlsString.Split(',');
			foreach (string url in urls)
			{
			    var newUrl = url;
			    if (!newUrl.StartsWith("http", StringComparison.InvariantCultureIgnoreCase))
			    {
                    newUrl = "http://" + url;
			    }
				ConnectToAsync(newUrl + "/cache_control.aspx?key=" + key + "&prefix=" + Prefix);
			}
		}

		public static void ClearCurrentCache(string Prefix)
		{
			if (HttpContext.Current != null && HttpContext.Current.Cache.Count > 0)
			{
				ArrayList toRemove = new ArrayList();

				if (!string.IsNullOrEmpty(Prefix))
				{
					foreach (DictionaryEntry de in HttpContext.Current.Cache)
						if (((string)de.Key).StartsWith(Prefix))
							toRemove.Add(de.Key);
				}
				else
				{
					foreach (DictionaryEntry de in HttpContext.Current.Cache)
						toRemove.Add(de.Key);
				}

				if (toRemove.Count > 0)
				{
					for (int i = 0; i < toRemove.Count; i++)
						HttpContext.Current.Cache.Remove((string)toRemove[i]);
				}
			}
		}

		public static void ConnectTo(string Url)
		{
			HttpWebRequest request;
			HttpWebResponse response = null;

            try
			{
				request = (HttpWebRequest)WebRequest.Create(Url);
				// Set some reasonable limits on resources used by this request
				request.MaximumAutomaticRedirections = 4;
				request.MaximumResponseHeadersLength = 4;
				// Set credentials to use for this request.
				request.Credentials = CredentialCache.DefaultCredentials;
				response = (HttpWebResponse)request.GetResponse();
			}
			catch { }
			finally
			{
				if (response != null)
					response.Close();
			}
		}

		public static void ConnectToAsync(string Url)
		{
			try
			{
				HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Url);
				// Set some reasonable limits on resources used by this request
				request.MaximumAutomaticRedirections = 4;
				request.MaximumResponseHeadersLength = 4;
				// Set credentials to use for this request.
				request.Credentials = CredentialCache.DefaultCredentials;

				// Start the asynchronous request.
				IAsyncResult result =
					(IAsyncResult)request.BeginGetResponse(new AsyncCallback(RespCallback), request);

			}
			catch { }
		}

		private static void RespCallback(IAsyncResult asynchronousResult)
		{
			HttpWebResponse response = null;
			try
			{
				// State of request is asynchronous.
				HttpWebRequest request = (HttpWebRequest)asynchronousResult.AsyncState;
				response = (HttpWebResponse)request.EndGetResponse(asynchronousResult);
			}
			catch { }
			finally
			{
				// Release the HttpWebResponse resource.
				if (response != null)
					response.Close();
			}
		}

	}
}
