using System;
using System.Data;
using System.Web.Services;
using System.Web.Services.Protocols;
using MetaData;
using NLog;

namespace DBReader
{
    /// <summary>
    ///     Summary description for AuthentificatedService.
    /// </summary>
    public class AuthentificatedService : WebService
    {
        private static readonly Logger log = LogManager.GetCurrentClassLogger();
        public SecurityHeader CurrentUser;
        protected UserInfo Usr;

        protected virtual string CheckLogin(string login, string password) //Uncatched
        {
            var cmd = QueryConstructor.GetCommand("upRegisterPeople", CommandType.StoredProcedure, null);
            cmd.Parameters.AddWithValue("@loginID", login);
            cmd.Parameters.AddWithValue("@password", password);
            cmd.Parameters.AddWithValue("@backend", 1);
            cmd.Connection.Open();
            try
            {
                var dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    Usr = new UserInfo
                    {
                        People = (Guid) dr[0],
                        FirstName = dr[1].ToString(),
                        MiddleName = dr[2].ToString(),
                        LastName = dr[3].ToString(),
                        Ticket = (Guid) dr[4]
                    };
                }
                else throw new LoginException();
                dr.Close();
                Context.Cache.Insert(Usr.Ticket.ToString(), Usr, null, DateTime.MaxValue, TimeSpan.FromMinutes(20));
                return Usr.Ticket + "," + Usr.People + "," + Usr.FirstName + " " + Usr.LastName;
            }
            catch (Exception ex)
            {
                log.Error("error login message: {0}\r\nlogin: {1}\r\npassword: {2}", ex.Message, login, password);
                return "";
            }
            finally
            {
                cmd.Connection.Close();
            }
        }

        protected virtual UserInfo CheckSession() //Uncatched
        {
            if (CurrentUser == null) throw new Exception("Autentification absent");
            if (new Guid(CurrentUser.Ticket) == WebServiceExtensions.ServiceTicket) return null;
            Usr = (UserInfo) Context.Cache[CurrentUser.Ticket];
            if (Usr == null)
            {
                var cmd = QueryConstructor.GetCommand("upGetSession", CommandType.StoredProcedure, null);
                cmd.Parameters.AddWithValue("@sessionOID", new Guid(CurrentUser.Ticket));
                cmd.Connection.Open();
                try
                {
                    var dr = cmd.ExecuteReader();
                    if (dr.Read())
                    {
                        Usr = new UserInfo();
                        Usr.People = (Guid) dr[0];
                        Usr.FirstName = dr[1].ToString();
                        Usr.MiddleName = dr[2].ToString();
                        Usr.LastName = dr[3].ToString();
                        Usr.Ticket = (Guid) dr[4];
                    }
                    else throw new Exception("Autentification failed");
                    dr.Close();
                    Context.Cache.Insert(Usr.Ticket.ToString(), Usr, null, DateTime.MaxValue, TimeSpan.FromMinutes(20));
                }
                finally
                {
                    cmd.Connection.Close();
                }
            }
            return Usr;
        }
    }

    public class SecurityHeader : SoapHeader
    {
        public string Ticket;
    }
}