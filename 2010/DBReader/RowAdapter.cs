using System;
using System.Globalization;
using System.Xml;
using System.Reflection;

namespace DBReader
{
	/// <summary>
	/// Summary description for RowAdapter.
	/// </summary>
	public class RowAdapter
	{
		public static NumberFormatInfo NumberFormat {
			get {
				NumberFormatInfo nfi = new CultureInfo("en-US").NumberFormat;
				nfi.NumberDecimalSeparator = ".";
				return nfi;
			}
		}
		public static DateTimeFormatInfo DateTimeFormat {
			get {
				DateTimeFormatInfo dtfi = new CultureInfo("ru-RU").DateTimeFormat;
				dtfi.FullDateTimePattern += ".fff";
				dtfi.ShortDatePattern = "dd/MM/yyyy";
				return dtfi;
			}
		}		
		public static string FormatRowValue(string rowXml, Type classType) {
			XmlDocument doc = new XmlDocument();
			PropertyInfo[] infos = classType.GetProperties();
			doc.LoadXml(rowXml);
			//classType
			XmlNode singleProp = doc.DocumentElement.SelectSingleNode("singleProp");
			if(singleProp != null) {
				foreach(XmlNode n in singleProp.ChildNodes) {
					string propName = n.Name;
					string propValue = n.InnerText;
					if(propValue == "\0") {
						foreach(PropertyInfo info in infos) {
							if(string.Compare(propName, info.Name, true) == 0) {
								if (info.PropertyType == typeof(DateTime)) {
									n.InnerText = DateTime.MinValue.ToString(DateTimeFormat.FullDateTimePattern);
									break;
								}
								else if(info.PropertyType == typeof(string)) {
									break;
								}
								else if(info.PropertyType == typeof(int)) {
									n.InnerText = int.MinValue.ToString();
									break;
								}
								else if(info.PropertyType == typeof(double)) {
									n.InnerText = double.MinValue.ToString(NumberFormat);
									break;
								}
								else if(info.PropertyType == typeof(decimal)) {
									n.InnerText = decimal.MinValue.ToString(NumberFormat);
									break;
								}
								else if(info.PropertyType == typeof(float)) {
									n.InnerText = float.MinValue.ToString(NumberFormat);
									break;
								}
								else if(info.PropertyType == typeof(bool)) {
									n.InnerText = false.ToString();
									break;
								}
								else if(info.PropertyType == typeof(Guid)) {
									n.InnerText = Guid.Empty.ToString();
									break;
								}
							}
						}
					}
				}
			}
			return doc.OuterXml;
		}
	}
}
