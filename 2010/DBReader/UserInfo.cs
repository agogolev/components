using System;

namespace DBReader
{
	/// <summary>
	/// Summary description for UserInfo.
	/// </summary>
	public class UserInfo 
	{
		public string LastName;
		public string FirstName;
		public string MiddleName;
		public Guid People;
		public Guid User;//не используется
		public string UserRole;//не используется
		public Guid Ticket;
	}
}
