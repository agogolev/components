using System;
using System.Linq;
using System.Xml;
using System.Text;
using System.Data;
using System.Collections;
using MetaData;
using System.Xml.Linq;
using NLog;


namespace DBReader
{
	/// <summary>
	/// Summary description for ListProvider.
	/// </summary>
	public class ListProvider
	{
		private static readonly Logger log = LogManager.GetCurrentClassLogger();
		private static void ParseFiltersOrders(XmlDocument doc, ref FilterCollection filters, ref OrderInfo[] orders)
		{
			var nf = doc.SelectSingleNode("/list/filters");
			if (nf != null)
			{
				filters = new FilterCollection();
				if (nf.Attributes["filterString"] != null && nf.Attributes["filterString"].Value != String.Empty) filters.FilterString = nf.Attributes["filterString"].Value;
				if (nf.Attributes["combinationType"] == null || (nf.Attributes["combinationType"] != null && nf.Attributes["combinationType"].Value.ToUpper() == "AND")) filters.CombinationType = FilterCombinationType.AND;
				else filters.CombinationType = FilterCombinationType.OR;
				var nodes = nf.SelectNodes("filter");
				if (nodes.Count > 0)
				{
					filters.Filters = new FilterInfo[nodes.Count];
					var i = 0;
					foreach (XmlNode n in nodes)
					{
						var type = n.Attributes["Type"].Value;
						var vals = n.SelectNodes("Value");
						if (type.IndexOf("Integer") != -1)
						{
							var filter = new IntegerFilterInfo();
							if (vals.Count > 0)
							{
								filter.Values = new int[vals.Count];
								var j = 0;
								foreach (XmlNode val in vals)
									filter.Values[j++] = Convert.ToInt32(val.InnerText);
							}
							filters.Filters[i] = filter;
						}
						else if (type.IndexOf("Double") != -1)
						{
							var filter = new DoubleFilterInfo();
							if (vals.Count > 0)
							{
								filter.Values = new double[vals.Count];
								int j = 0;
								foreach (XmlNode val in vals)
									filter.Values[j++] = Convert.ToDouble(val.InnerText);
							}
							filters.Filters[i] = filter;
						}
						else if (type.IndexOf("String") != -1)
						{
							var filter = new StringFilterInfo();
							if (vals.Count > 0)
							{
								filter.Values = new string[vals.Count];
								var j = 0;
								foreach (XmlNode val in vals)
									filter.Values[j++] = val.InnerText;
							}
							filters.Filters[i] = filter;
						}
						else if (type.IndexOf("Object") != -1)
						{
							var filter = new ObjectFilterInfo();
							if (vals.Count > 0)
							{
								filter.Values = new Guid[vals.Count];
								var j = 0;
								foreach (XmlNode val in vals)
									filter.Values[j++] = new Guid(val.InnerText);
							}
							filters.Filters[i] = filter;
						}
						else if (type.IndexOf("DateTime") != -1)
						{
							var filter = new DateTimeFilterInfo();
							if (vals.Count > 0)
							{
								filter.Values = new DateTime[vals.Count];
								var j = 0;
								foreach (XmlNode val in vals)
									filter.Values[j++] = DateTime.Parse(val.InnerText, filter.DateTimeFormat);

							}
							filters.Filters[i] = filter;
						}
						else if (type.IndexOf("Expression") != -1)
						{
							var filter = new ExpressionFilterInfo();
							filters.Filters[i] = filter;
						}



						filters.Filters[i].ColumnName = n.Attributes["ColumnName"].Value;
						if (n.Attributes["TableName"] != null && n.Attributes["TableName"].Value != "")
							filters.Filters[i].TableName = n.Attributes["TableName"].Value;
						if (n.Attributes["TableAlias"] != null && n.Attributes["TableAlias"].Value != "")
							filters.Filters[i].TableAlias = n.Attributes["TableAlias"].Value;
						if (n.Attributes["FilterName"] != null && n.Attributes["FilterName"].Value != "")
							filters.Filters[i].FilterName = n.Attributes["FilterName"].Value;
						if (n.Attributes["Negation"] != null && n.Attributes["Negation"].Value == "True")
							filters.Filters[i].Negation = true;
						else
							filters.Filters[i].Negation = false;

						switch (n.Attributes["Verb"].Value)
						{
							case "Equal":
								filters.Filters[i].Verb = FilterVerb.Equal;
								break;
							case "Like":
								filters.Filters[i].Verb = FilterVerb.Like;
								break;
							case "Between":
								filters.Filters[i].Verb = FilterVerb.Between;
								break;
							case "Greater":
								filters.Filters[i].Verb = FilterVerb.Greater;
								break;
							case "GreaterOrEqual":
								filters.Filters[i].Verb = FilterVerb.GreaterOrEqual;
								break;
							case "Less":
								filters.Filters[i].Verb = FilterVerb.Less;
								break;
							case "LessOrEqual":
								filters.Filters[i].Verb = FilterVerb.LessOrEqual;
								break;
							case "In":
								filters.Filters[i].Verb = FilterVerb.In;
								break;
							case "IsNull":
								filters.Filters[i].Verb = FilterVerb.IsNull;
								break;
						}

						i++;
					}
				}
			}
			XmlNodeList nodes1 = doc.SelectNodes("/list/orders/order");
			if (nodes1.Count > 0)
			{
				orders = new OrderInfo[nodes1.Count];

				int i = 0;
				foreach (XmlNode n in nodes1)
				{
					orders[i] = new OrderInfo();
					if (n.Attributes["TableName"] != null && n.Attributes["TableName"].Value != "")
						orders[i].TableName = n.Attributes["TableName"].Value;
					orders[i].ColumnName = n.Attributes["ColumnName"].Value;
					if (n.Attributes["order"] != null && string.Compare(n.Attributes["order"].Value, "Desc", true) == 0)
						orders[i].order = OrderDir.Desc;
					else
						orders[i].order = OrderDir.Asc;
					i++;
				}
			}
		}
		private static Hashtable ParseParams(XmlDocument doc)
		{
			var prms = new Hashtable();
			var nl = doc.SelectNodes("/list/params/param");
			foreach (XmlNode n in nl)
			{
				DBHelper.AddParam(prms, n);
			}
			return prms;
		}

		/// <summary>
		/// Возвращает датасет с полями объектов (возвращает все объекты)
		/// </summary>
		/// <param name="xml"></param>
		/// <param name="usr"></param>
		/// <returns></returns>
		public static DataSet GetList(string xml, UserInfo usr)
		{
			var xmlDoc = new XmlDocument();
			xmlDoc.LoadXml(xml);
			var node = xmlDoc.SelectSingleNode("/list");

			if ((node.Attributes["OID"]) != null)
			{
				var classContainer = node.Attributes["classContainer"] != null ? node.Attributes["classContainer"].Value : null;
				var className = node.Attributes["class"] != null ? node.Attributes["class"].Value : null;
				var propName = node.Attributes["propName"].Value;
				var OIDs = node.Attributes["OID"].Value;
				var numInBatch = Convert.ToInt32(node.Attributes["numInBatch"].Value);
				var pageNum = Convert.ToInt32(node.Attributes["pageNum"].Value);

				var filters = new FilterCollection();
				OrderInfo[] orders = null;

				ParseFiltersOrders(xmlDoc, ref filters, ref orders);

				//				ShowOnlyNotDeleted(ref filters);

				node = xmlDoc.SelectSingleNode("/list/fieldNames");
				var fieldNames = node != null ? node.InnerXml : null;

				return GetMulti(OIDs, propName, classContainer, className, numInBatch, pageNum, fieldNames, filters, orders, null);
			}
			else
			{
				var className = node.Attributes["class"].Value;

				var numInBatch = Convert.ToInt32(node.Attributes["numInBatch"].Value);
				var pageNum = Convert.ToInt32(node.Attributes["pageNum"].Value);
				var keyFieldsNode = node.Attributes["keyFields"];
				var keyFields = "";
				if (keyFieldsNode != null) keyFields = node.Attributes["keyFields"].Value;

				var filters = new FilterCollection();
				OrderInfo[] orders = null;

				ParseFiltersOrders(xmlDoc, ref filters, ref orders);

				var prms = ParseParams(xmlDoc);
				node = xmlDoc.SelectSingleNode("/list/fieldNames");
				var fieldNames = node != null ? node.InnerXml : null;

				return GetList(className, numInBatch, pageNum, keyFields, fieldNames, filters, orders, prms, usr);
			}
		}

		private static void ShowOnlyNotDeleted(ref FilterCollection filters)
		{
			var len = 1;
			if (filters.Filters != null)
			{
				len += filters.Filters.Length;
				//1 если есть сторка фильтров (FilterString), то необходимо добавить в конец фильтр на isDeleted
				if (!string.IsNullOrEmpty(filters.FilterString))
				{
					filters.FilterString = "(" + filters.FilterString + ") AND GetList_IsDeleted";
				}
				//2 если строки нет, а комбинация (CombinationType) указана как OR, то необходимо задать строку фильтров
				else if (filters.CombinationType == FilterCombinationType.OR)
				{
					foreach (var t in filters.Filters)
					{
						filters.FilterString += t.ColumnName + " OR ";
						t.FilterName = t.ColumnName;
					}
					filters.FilterString = "(" + filters.FilterString.Substring(0, filters.FilterString.Length - 4) + ") AND GetList_IsDeleted";
				}
			}

			var fnew = new FilterInfo[len];

			for (var i = 0; i < len - 1; i++)
				fnew[i] = filters.Filters[i];

			var ifi = new IntegerFilterInfo { ColumnName = "IsDeleted", FilterName = "GetList_IsDeleted", Values = new[] { 1 }, Negation = true };
			fnew[len - 1] = ifi;
			filters.Filters = fnew;
		}

		/// <summary>
		/// Возвращает датасет с полями объектов, есть возможность получить объекты, помеченные как удалённые
		/// </summary>
		/// <param name="xml"></param>
		/// <param name="usr"></param>
		/// <param name="showAll">Показывать ли объекты, помеченные как удалённые</param>
		/// <returns></returns>
		public static DataSet GetList(string xml, bool showAll, UserInfo usr)
		{
			var xmlDoc = new XmlDocument();
			xmlDoc.LoadXml(xml);
			var node = xmlDoc.SelectSingleNode("/list");

			if ((node.Attributes["OID"]) != null)
			{
				var classContainer = node.Attributes["classContainer"] != null ? node.Attributes["classContainer"].Value : null;
				var className = node.Attributes["class"] != null ? node.Attributes["class"].Value : null;
				var propName = node.Attributes["propName"].Value;
				var OIDs = node.Attributes["OID"].Value;
				var numInBatch = Convert.ToInt32(node.Attributes["numInBatch"].Value);
				var pageNum = Convert.ToInt32(node.Attributes["pageNum"].Value);

				var filters = new FilterCollection();
				OrderInfo[] orders = null;

				ParseFiltersOrders(xmlDoc, ref filters, ref orders);

				if (!showAll)
					ShowOnlyNotDeleted(ref filters);

				node = xmlDoc.SelectSingleNode("/list/fieldNames");
				var fieldNames = node != null ? node.InnerXml : null;

				return GetMulti(OIDs, propName, classContainer, className, numInBatch, pageNum, fieldNames, filters, orders, null);
			}
			else
			{
				string className = node.Attributes["class"].Value;

				int numInBatch = Convert.ToInt32(node.Attributes["numInBatch"].Value);
				int pageNum = Convert.ToInt32(node.Attributes["pageNum"].Value);
				XmlNode keyFieldsNode = node.Attributes["keyFields"];
				string keyFields = "";
				if (keyFieldsNode != null)
					keyFields = node.Attributes["keyFields"].Value;

				var filters = new FilterCollection();
				OrderInfo[] orders = null;

				ParseFiltersOrders(xmlDoc, ref filters, ref orders);

				if (!showAll)
					ShowOnlyNotDeleted(ref filters);

				var prms = ParseParams(xmlDoc);
				node = xmlDoc.SelectSingleNode("/list/fieldNames");
				string fieldNames = node != null ? node.InnerXml : null;

				return GetList(className, numInBatch, pageNum, keyFields, fieldNames, filters, orders, prms, usr);
			}
		}

		public static DataSet GetList(string className, int numInBatch, int pageNum, string fieldNames, FilterCollection filters, OrderInfo[] orders, Hashtable prms, UserInfo usr)
		{
			return GetList(className, numInBatch, pageNum, "", fieldNames, filters, orders, prms, usr);
		}

		public static DataSet GetList(string className, int numInBatch, int pageNum, string keyFields, string fieldNames, FilterCollection filters, OrderInfo[] orders, Hashtable prms, UserInfo usr)
		{
			string sql = null;
			try
			{
				sql = QueryConstructor.BuildQueryObjects(className, numInBatch, pageNum, keyFields, fieldNames, filters, orders, usr);
				return DBHelper.GetDataSetSql(sql, "table", prms, false, usr);
			}
			catch (Exception e)
			{
				log.Error("sql = {0}\r\nerror: {1}", sql, e.Message);
				throw;
			}

		}

		public static DataSet GetMulti(string OIDs, string propName, string className, string multiClass, int numInBatch, int pageNum, string fieldNames, FilterCollection filters, OrderInfo[] orders, UserInfo usr)
		{
			var arrOID = OIDs.Split(",; ".ToCharArray());
			var sbParams = new StringBuilder();
			var prms = new Hashtable();
			var i = 0;
			foreach (string OIDItem in arrOID.Where(OIDItem => OIDItem != ""))
			{
				i++;
				prms["@OID" + i.ToString()] = new Guid(OIDItem);
				sbParams.Append("@OID" + i.ToString() + " uniqueidentifier, ");
			}
			sbParams.Length -= 2;
			var sParams = sbParams.ToString();
			if (i == 0) return null;
			if (className == null)
			{
				var prmsClass = new Hashtable();
				prmsClass["@OID"] = prms["@OID1"];
				className = (string)DBHelper.ExecuteScalar(@"
SELECT c.className 
FROM t_Object o
	inner join t_className c on o.CID = c.CID
WHERE o.OID = @OID", prmsClass, false);
			}
			var sql = QueryConstructor.BuildGetMulti(i, propName, className, multiClass, numInBatch, pageNum, fieldNames, filters, orders, null);
			prms["@stmt"] = sql;
			prms["@params"] = sParams;
			sql = "sp_executesql";
			return DBHelper.GetDataSetSql(sql, "table", prms, true, usr);
		}
		public static string GetLookupValues(string className)
		{
			var sql = QueryConstructor.BuildLookupValues(className);
			var elem = DBHelper.GetXmlData(sql, null, "root", false);
			var attr = new XAttribute("className", className);
			elem.Add(attr);
			return elem.ToString();
		}
		public static string GetLookupValues(string tableName, string fieldID, string fieldName)
		{
			var sql = QueryConstructor.BuildLookupValues(tableName, fieldID, fieldName);
			var elem = DBHelper.GetXmlData(sql, null, "root", false);
			return elem.ToString();
		}
		//For DataSetISM
		public static DataSetISM GetDataSetISM(string xml, UserInfo usr)
		{
			var xmlDoc = new XmlDocument();
			xmlDoc.LoadXml(xml);
			var node = xmlDoc.SelectSingleNode("/list");

			if ((node.Attributes["OID"]) != null)
			{
				var classContainer = node.Attributes["classContainer"] != null ? node.Attributes["classContainer"].Value : null;
				var className = node.Attributes["class"] != null ? node.Attributes["class"].Value : null;
				var propName = node.Attributes["propName"].Value;
				var OIDs = node.Attributes["OID"].Value;
				var numInBatch = Convert.ToInt32(node.Attributes["numInBatch"].Value);
				var pageNum = Convert.ToInt32(node.Attributes["pageNum"].Value);


				FilterCollection filters = null;
				OrderInfo[] orders = null;

				ParseFiltersOrders(xmlDoc, ref filters, ref orders);

				node = xmlDoc.SelectSingleNode("/list/fieldNames");
				string fieldNames = node != null ? node.InnerText : null;

				return GetMultiDataSetISM(OIDs, propName, classContainer, className, numInBatch, pageNum, fieldNames, filters, orders, null);
			}
			else
			{
				string className = node.Attributes["class"].Value;

				int numInBatch = Convert.ToInt32(node.Attributes["numInBatch"].Value);
				int pageNum = Convert.ToInt32(node.Attributes["pageNum"].Value);

				FilterCollection filters = null;
				OrderInfo[] orders = null;

				ParseFiltersOrders(xmlDoc, ref filters, ref orders);

				node = xmlDoc.SelectSingleNode("/list/fieldNames");
				string fieldNames = node != null ? node.InnerXml : null;

				return GetDataSetISM(className, numInBatch, pageNum, fieldNames, filters, orders, usr);
				//				string sql = QueryConstructor.BuildQueryObjects(className, numInBatch, pageNum, fieldNames, filters, orders, usr);
				//				return GetDataSetSql(sql, usr);

			}

		}

		public static DataSetISM GetDataSetISM(string className, int numInBatch, int pageNum, string fieldNames, FilterCollection filters, OrderInfo[] orders, UserInfo usr)
		{
			string sql = QueryConstructor.BuildQueryObjects(className, numInBatch, pageNum, fieldNames, filters, orders, usr);
			return DBHelper.GetDataSetISMSql(sql, "table", usr);
		}

		public static DataSetISM GetMultiDataSetISM(string OIDs, string propName, string className, string multiClass, int numInBatch, int pageNum, string fieldNames, FilterCollection filters, OrderInfo[] orders, UserInfo usr)
		{
			var arrOID = OIDs.Split(",; ".ToCharArray());
			var sbParams = new StringBuilder();
			var prms = new Hashtable();
			var i = 0;
			foreach (var OIDItem in arrOID.Where(OIDItem => OIDItem != ""))
			{
				i++;
				prms["@OID" + i.ToString()] = new Guid(OIDItem);
				sbParams.Append("@OID" + i.ToString() + " uniqueidentifier, ");
			}
			var sParams = sbParams.ToString().Substring(0, sbParams.ToString().LastIndexOf(",", StringComparison.Ordinal));
			if (i == 0) return null;
			if (className == null)
			{
				var prmsClass = new Hashtable();
				prmsClass["@OID"] = prms["@OID1"];
				className = (string)DBHelper.ExecuteScalar(@"
SELECT c.className 
FROM t_Object o
	inner join t_className c on o.CID = c.CID
WHERE o.OID = @OID", prmsClass, false);
			}
			var sql = QueryConstructor.BuildGetMulti(i, propName, className, multiClass, numInBatch, pageNum, fieldNames, filters, orders, null);
			prms["@stmt"] = sql;
			prms["@params"] = sParams;
			sql = "sp_executesql";
			return DBHelper.GetDataSetISMSql(sql, "table", prms, true, usr);
		}
	}
}
