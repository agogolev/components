using System;
using MetaData;
using System.Xml;
using System.Text;
using System.Collections;
using System.Globalization;
using System.Data;
using System.Data.SqlClient;

namespace DBReader
{
	/// <summary>
	/// Summary description for CollectionAdapter.
	/// </summary>
	public class CollectionAdapter
	{
		private XmlDocument _doc;
		private Guid _OID;
		private CollectionInfo _ci;
		private string _className;
		private string _propName;
		private Hashtable _fields;
		public CollectionAdapter(string OID, string className, string propName, string xmlContent)
		{
			_OID = new Guid(OID);
			_className = className;
			_propName = propName;
			_doc = new XmlDocument();
			_doc.LoadXml(xmlContent);
			_fields = new Hashtable();
		}
		public void AcceptChanges()
		{
			AcceptChanges(null, null);
		}

		/// <summary>
		/// выполн¤етс¤ в транзакции
		/// </summary>

		public void AcceptChanges(SqlConnection conn, SqlTransaction trans)
		{
			if (_doc.DocumentElement.ChildNodes.Count == 0) return;
			_ci = QueryConstructor.BuildCollectionInfo(_className, _propName);
			XmlNodeList nDelete = _doc.DocumentElement.SelectNodes("delete");
			if (nDelete.Count != 0)
			{
				StringBuilder sqlB = new StringBuilder();
				sqlB.Append(String.Format(@"
										DELETE FROM [{0}] 
										WHERE ", _ci.TableName));
				int i = 0;
				foreach (string key in _ci.PrimaryKey)
				{
					sqlB.Append(@"
	[" + key + "] = @param" + (i++).ToString() + " And ");
				}
				sqlB.Length -= 4;
				string sql = sqlB.ToString();
				Hashtable prms = new Hashtable();
				foreach (XmlNode n in nDelete)
				{
					i = 0;
					foreach (string key in _ci.PrimaryKey)
					{

						if (key.ToLower() == _ci.FieldOID.ToLower())
							prms["@param" + (i++).ToString()] = _OID;
						else
						{
							if (n.SelectSingleNode(key) == null) throw new Exception(string.Format("Ёлемент {0} не найден в xml", key));
							prms["@param" + (i++).ToString()] = Convert(key, n.SelectSingleNode(key).InnerText);
						}

					}
					DBHelper.ExecuteCommand(sql, prms, false, conn, trans);
				}
			}
			XmlNodeList nAdd = _doc.DocumentElement.SelectNodes("add");
			if (nAdd.Count != 0)
			{
				StringBuilder sqlB = new StringBuilder();
				Hashtable fields = new Hashtable();
				sqlB.Append(String.Format(@"
										INSERT INTO [{0}] (", _ci.TableName));
				foreach (PropInfo pi in _ci.Fields)
				{

					if (pi.PropName.ToLower() == _ci.FieldOID.ToLower())
					{
						sqlB.Append(String.Format("[{0}], ", _ci.FieldOID));
					}
					else if (nAdd[0].SelectSingleNode(pi.PropName) != null && nAdd[0].SelectSingleNode(pi.PropName).InnerText != null)
					{
						sqlB.Append(String.Format("[{0}], ", pi.PropName));
						fields[pi.PropName] = 1;
					}
				}
				sqlB.Length -= 2;
				sqlB.Append(@")
VALUES(");
				int i = 0;
				foreach (PropInfo pi in _ci.Fields)
				{
					if (pi.PropName.ToLower() == _ci.FieldOID.ToLower())
					{
						sqlB.Append(CastVariable(pi, "@param" + (i++).ToString()) + ", ");
					}
					else if (fields[pi.PropName] != null) sqlB.Append(CastVariable(pi, "@param" + (i++).ToString()) + ", ");
				}
				sqlB.Length -= 2;
				sqlB.Append(@")");
				string sql = sqlB.ToString();
				Hashtable prms = new Hashtable();
				foreach (XmlNode n in nAdd)
				{
					i = 0;
					foreach (PropInfo pi in _ci.Fields)
					{
						if (pi.PropName.ToLower() == _ci.FieldOID.ToLower())
						{
							prms["@param" + (i++).ToString()] = _OID;
						}
						else if (fields[pi.PropName] != null)
						{
							if (n.SelectSingleNode(pi.PropName) == null) throw new Exception(string.Format("Ёлемент {0} не найден в xml", pi.PropName));
							prms["@param" + (i++).ToString()] = Convert(pi.PropName, n.SelectSingleNode(pi.PropName).InnerText);
						}
					}
					DBHelper.ExecuteCommand(sql, prms, false, conn, trans);
				}
			}
			XmlNodeList nModify = _doc.DocumentElement.SelectNodes("modify");
			if (nModify.Count != 0)
			{
				StringBuilder sqlB = new StringBuilder();
				ArrayList fields = new ArrayList();
				sqlB.Append(String.Format(@"
										UPDATE [{0}] SET", _ci.TableName));
				int i = 0;
				foreach (PropInfo pi in _ci.Fields)
				{
					if (!_ci.IsPrimaryKey(pi.PropName) && nModify[0].SelectSingleNode(pi.PropName) != null)
					{
						sqlB.Append(String.Format(@"
												[{0}] = {1}, ", pi.PropName, CastVariable(pi, "@param" + (i++).ToString())));
						fields.Add(pi.PropName);
					}
				}
				sqlB.Length -= 2;
				sqlB.Append(@"
WHERE");
				foreach (string key in _ci.PrimaryKey)
				{
					sqlB.Append(String.Format(@"
												[{0}] = @param{1} And ", key, i++));
				}
				sqlB.Length -= 4;
				string sql = sqlB.ToString();
				Hashtable prms = new Hashtable();
				foreach (XmlNode n in nModify)
				{
					i = 0;
					foreach (string fName in fields)
					{
						if (n.SelectSingleNode(fName) == null) throw new Exception(string.Format("Ёлемент {0} не найден в xml", fName));
						prms["@param" + (i++).ToString()] = Convert(fName, n.SelectSingleNode(fName).InnerText);
					}
					foreach (string key in _ci.PrimaryKey)
					{
						if (key.ToLower() == _ci.FieldOID.ToLower())
							prms["@param" + (i++).ToString()] = _OID;
						else
						{
							if (n.SelectSingleNode(key) == null) throw new Exception(string.Format("Ёлемент {0} не найден в xml", key));
							prms["@param" + (i++).ToString()] = Convert(key, n.SelectSingleNode(key).InnerText);
						}
							
					}
					DBHelper.ExecuteCommand(sql, prms, false, conn, trans);
				}
			}
		}
		private static string ConstructField(string propName, string fieldName)
		{
			int i = fieldName.IndexOf("(");
			if (i == -1)
			{
				return String.Format("{0}.{1}", propName, fieldName);
			}
			else
			{
				string beginStr = fieldName.Substring(0, i + 1);
				string endStr = fieldName.Substring(i + 1);
				return String.Format("{0}{1}.{2}", beginStr, propName, endStr);
			}
		}
		public DataSetISM GetDataSet()
		{
			var info = QueryConstructor.BuildMetadataInfo(_className);
			if (info == null) throw new Exception("class not found");
			var mpi = (MultiPropInfo)info.FindProp(_propName, 2);
			if (mpi == null) throw new Exception("Collection not found");
			var build = new GetObjectBuilder(info);
			var nl = _doc.DocumentElement.SelectNodes("multi/field");
			foreach (XmlNode n in nl)
			{
				var fieldName = n.Attributes["name"].Value;
				string columnName = null;
				if (n.Attributes["columnName"] != null) columnName = n.Attributes["columnName"].Value;
				string order = null;
				if (n.Attributes["order"] != null) order = n.Attributes["order"].Value;
				build.AddMultiField(ConstructField(_propName, fieldName), columnName, order);
			}
			var prms = new Hashtable();
			prms["@OID"] = _OID;
			nl = _doc.DocumentElement.SelectNodes("params/param");
			if (nl.Count != 0)
			{
				foreach (XmlNode n in nl)
				{
					DBHelper.AddParam(prms, n);
				}
			}
			var ds = DBHelper.GetDataSetISMSql(build.BuildDataSetSQL(), "table", prms, false, null);
			XmlAttribute attr = _doc.DocumentElement.Attributes["createPK"];
			if (attr == null || attr.Value == "1")
			{
				var dt = ds.Tables["table"];
				DataColumn[] cols;
				var i = 0;
				DataColumn dc;
				if (dt.Columns.Contains(mpi.FieldOID))
				{
					cols = new DataColumn[2];
					dc = dt.Columns[mpi.FieldOID];
					if (dc.AllowDBNull) dc.AllowDBNull = false;
					cols[i++] = dc;
				}
				else cols = new DataColumn[1];
				string keyField;
				keyField = mpi.WithOrder ? mpi.FieldOrd.PropName : mpi.FieldProp.PropName;
				if (dt.Columns.Contains(keyField))
				{
					dc = dt.Columns[keyField];
					if (dc.AllowDBNull) dc.AllowDBNull = false;
					cols[i++] = dc;
				}
				if (i == cols.Length)
				{
					var cnst = new UniqueConstraint(cols, true);
					dt.Constraints.Add(cnst);
				}
			}
			return ds;
		}
		public static NumberFormatInfo NumberFormat
		{
			get
			{
				NumberFormatInfo nfi = new CultureInfo("en-US").NumberFormat;
				nfi.NumberDecimalSeparator = ".";
				return nfi;
			}
		}
		public static DateTimeFormatInfo DateTimeFormat
		{
			get
			{
				DateTimeFormatInfo dtfi = new CultureInfo("ru-RU").DateTimeFormat;
				return dtfi;
			}
		}
		private object Convert(string field, string value)
		{
			PropInfo pi = (PropInfo)_fields[field.ToLower()];
			if (pi == null)
			{
				pi = _ci.GetField(field);
				_fields[field.ToLower()] = pi;
			}
			switch (pi.ColumnType)
			{
				case DataType.String:
					return (value == "\0" ? DBNull.Value : (object)value);
				case DataType.Uniqueidentifier:
					Guid g = (value == "\0" ? Guid.Empty : new Guid(value));
					return (g == Guid.Empty ? DBNull.Value : (object)g);
				case DataType.DateTime:
					return (value == "\0" ? DBNull.Value : (object)System.Convert.ToDateTime(value, DateTimeFormat));
				case DataType.Float:
					return (value == "\0" ? DBNull.Value : (object)System.Convert.ToDouble(value, NumberFormat));
				case DataType.Money:
					return (value == "\0" ? DBNull.Value : (object)System.Convert.ToDecimal(value, NumberFormat));
				case DataType.Int:
					return (value == "\0" ? DBNull.Value : (object)System.Convert.ToInt32(value));
				case DataType.Boolean:
					return (value == "\0" ? DBNull.Value : (object)System.Convert.ToBoolean(value));
				case DataType.Binary:
					return null;
			}
			return null;
		}
		private static string CastVariable(PropInfo prop, string var)
		{
			switch (prop.ColumnType)
			{
				//				case DataType.DateTime:
				//					return (value == String.Empty ? DBNull.Value : (object)System.Convert.ToDateTime(value, DateTimeFormat));
				case DataType.Float:
					return String.Format("convert(decimal, {0})", var);
				case DataType.Money:
					return String.Format("convert(money, {0})", var);
				default:
					return var;
			}
		}
	}

}
