﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Configuration;
using System.Drawing;
using System.Drawing.Imaging;
using System.Web.SessionState;

namespace Captcha
{
	public class CaptchaHandler : IHttpHandler, IReadOnlySessionState
	{
		bool IHttpHandler.IsReusable
		{
			get { return true; }
		}
		void IHttpHandler.ProcessRequest(HttpContext context)
		{
			// Create a CAPTCHA image using the text stored in the Session object.
			if (HttpContext.Current.Session["CaptchaImageText"] != null)
			{
				CaptchaImage ci = new CaptchaImage(HttpContext.Current.Session["CaptchaImageText"].ToString(), 200, 50, "Century Schoolbook");

				// Change the response headers to output a JPEG image.
				HttpContext.Current.Response.Clear();
				HttpContext.Current.Response.ContentType = "image/jpeg";

				// Write the image to the response stream in JPEG format.
				ci.Image.Save(HttpContext.Current.Response.OutputStream, ImageFormat.Jpeg);

				// Dispose of the CAPTCHA image object.
				ci.Dispose();
			}
		}

		public static object GetValue(string name)
		{
			string str = ConfigurationManager.AppSettings[name];
			if (str == null)
				return "";
			else return str;
		}
	}
}
