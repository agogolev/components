using System;
using System.Windows.Forms;

namespace WinComponents
{
	/// <summary>
	/// Summary description for ExtMenuItem.
	/// </summary>
	public class ExtMenuItem : MenuItem
	{
		private bool visibleStatic = true;
		private bool visibleDynamic = true;

		public bool VisibleStatic 
		{
			get { return visibleStatic; }
			set 
			{ 
				visibleStatic = value; 
				this.Visible = visibleStatic && visibleDynamic;				
			}
		}

		public bool VisibleDynamic 
		{
			get { return visibleDynamic; }
			set 
			{
				visibleDynamic = value;
				this.Visible = visibleStatic && visibleDynamic;				 
			}
		}
	}
}
