using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

namespace WinComponents
{
	/// <summary>
	/// Summary description for UserControl1.
	/// </summary>
	public class MenuItemGuarded : MenuItem
	{
		private System.ComponentModel.Container components = null;
		private Guid _tag;
		[Bindable(true), 
		DefaultValue(typeof(Guid), "00000000-0000-0000-0000-000000000000")] 
		public Guid GuidTag 
        {
			get {return _tag;}
			set {_tag = value;}
		}

		public MenuItemGuarded()
		{
			InitializeComponent();
		}

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if( components != null )
					components.Dispose();
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
		}
		#endregion
	}
}
