using System;
using System.Windows.Forms;

namespace WinComponents
{
	/// <summary>
	/// Summary description for ExtButton.
	/// </summary>
	public class ExtButton : Button
	{
		private bool enabledStatic = true;
		private bool enabledDynamic = true;

		public bool EnabledStatic 
		{
			get { return enabledStatic; }
			set 
			{ 
				this.Enabled = enabledStatic && enabledDynamic;
				enabledStatic = value; 
			}
		}

		public bool EnabledDynamic 
		{
			get { return enabledDynamic; }
			set 
			{
				this.Enabled = enabledStatic && enabledDynamic;
				enabledDynamic = value; 
			}
		}
	}
}
