using System;
using System.IO;
using System.Runtime.InteropServices;


namespace WinComponents
{
	/// <summary>
	/// Summary description for MimeTypeUtil.
	/// </summary>
	public class MimeTypeUtil
	{
		[DllImport("urlmon.dll", CharSet=CharSet.Auto)]
		static extern int FindMimeFromData(IntPtr pBC, IntPtr pwzUrl,byte[] pBuffer, int cbSize,
			IntPtr pwzMimeProposed,int dwMimeFlags, out IntPtr ppwzMimeOut, int dwReserved );

		public static string CheckType(string filePath) 
		{
			byte[] buffer = new byte[256];

			// grab the first 256 bytes on the file
			using (FileStream fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read)) 
			{
				if (fileStream.Length >=256)
					fileStream.Read(buffer,0,256);
				else
					fileStream.Read(buffer,0,(int)fileStream.Length);
			}

			try 
			{
				IntPtr suggestPtr = IntPtr.Zero, outPtr = IntPtr.Zero;
				IntPtr filePtr = Marshal.StringToBSTR(filePath);
				System.Int32 returnValue = FindMimeFromData(IntPtr.Zero, filePtr, buffer, 256, suggestPtr, 0, out outPtr, 0);
				return Marshal.PtrToStringUni(outPtr);
			}

			catch(Exception ex) 
			{
				return ex.Message;
			}
		}   

//		public static string CheckType(string filePath) 
//		{
//			using (FileStream fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read)) 
//			{
//				return CheckType(fileStream);
//			}
//		}
//
//		public static string CheckType(Stream stream)
//		{
//			byte[] buffer = new byte[256];
//
//			if (stream.Length >=256)
//				stream.Read(buffer,0,256);
//			else
//				stream.Read(buffer,0,(int)stream.Length);
//
//			try 
//			{
//				IntPtr suggestPtr = IntPtr.Zero, outPtr = IntPtr.Zero;
//				IntPtr filePtr = Marshal.StringToBSTR(filePath);
//				System.Int32 returnValue = FindMimeFromData(IntPtr.Zero, filePtr, buffer, 256, suggestPtr, 0, out outPtr, 0);
//				return Marshal.PtrToStringUni(outPtr);
//			}
//
//			catch(Exception ex) 
//			{
//				return ex.Message;
//			}
//		}
	}
}
