﻿using System;
using System.Text;
using System.Web;
using PostSharp.Aspects;

namespace Core.Aspects
{
	[Serializable]
	public class CacheAttribute : MethodInterceptionAspect, IAspect
	{
		private string defaultPrefix;
		private bool absoluteExpiration;
		private int minutes = 20;
		//public CacheAttribute()
		//  : this("www_")
		//{
		//}

		//public CacheAttribute(bool absoluteExpiration)
		//  : this("www_")
		//{
		//  this.absoluteExpiration = absoluteExpiration;
		//}

		//public CacheAttribute(string prefix)
		//{
		//  this.defaultPrefix = prefix;
		//}

		public CacheAttribute(bool absoluteExpiration = false, int minutes = 20, string prefix = "www_")
		{
			this.defaultPrefix = prefix;
			this.minutes = minutes;
			this.absoluteExpiration = absoluteExpiration;
		}

		public override void OnInvoke(MethodInterceptionArgs eventArgs)
		{
			var sb = new StringBuilder();
			sb.AppendFormat("{0}Method: {1}.{2}.", defaultPrefix, eventArgs.Method.DeclaringType.FullName, eventArgs.Method.Name);
			if (eventArgs.Arguments.Count > 0)
			{
				sb.Append(" Params: ");
				foreach (object obj in eventArgs.Arguments)
					if (obj != null)
						sb.AppendFormat("[{0}:{1}]", obj.GetType(), obj.GetHashCode());
					else
						sb.Append("[null]");
			}

			string key = sb.ToString();

            if (HttpContext.Current != null)
		    {
		        object result = HttpContext.Current.Cache.Get(key);
		        if (result == null)
		        {
		            lock (this)
		            {
		                result = HttpContext.Current.Cache.Get(key);
		                if (result == null)
		                {
		                    eventArgs.Proceed();
		                    if (eventArgs.ReturnValue != null)
		                    {
		                        result = eventArgs.ReturnValue;
		                        if (!absoluteExpiration)
		                            HttpContext.Current.Cache.Insert(key, result, null,
		                                System.Web.Caching.Cache.NoAbsoluteExpiration, TimeSpan.FromMinutes(minutes));
		                        else
		                            HttpContext.Current.Cache.Insert(key, result, null, DateTime.Now.AddMinutes(minutes),
		                                System.Web.Caching.Cache.NoSlidingExpiration);
		                    }
		                    return;
		                }
		            }
		        }
		        eventArgs.ReturnValue = result;
		    }
		    else
		    {
                eventArgs.Proceed();
		    }
		}
	}
}
