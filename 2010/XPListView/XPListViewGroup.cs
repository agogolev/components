﻿using System;
using System.ComponentModel; 
using System.Runtime.InteropServices;

namespace XPListview
{
	[TypeConverter(typeof(XPListViewGroupConverter))] 
	public class XPListViewGroup { 
		private string _text; 
		private int _index; 
		private XPListViewItemCollection items_;
		private XPListView _owner; 
		private ListViewAPI.LVGROUP _apiGroup;

		public XPListViewGroup() { 
		} 

		public XPListViewGroup(string text, int index) { 
			_text = text; 
			_index = index; 
		} 

		public XPListViewGroup(XPListView owner,string text, int index) 
		{ 
			_owner = owner;
			items_ = new XPListViewItemCollection(_owner);
			int ptrRetVal;

			try
			{
				if(_owner == null){ return ; }

				_apiGroup = new ListViewAPI.LVGROUP();
				_apiGroup.mask = ListViewAPI.LVGF_GROUPID | ListViewAPI.LVGF_HEADER | ListViewAPI.LVGF_STATE;
				_apiGroup.pszHeader = text;
				_apiGroup.cchHeader = _apiGroup.pszHeader.Length;
				_apiGroup.iGroupId = index;
				_apiGroup.stateMask = ListViewAPI.LVGS_NORMAL;
				_apiGroup.state = ListViewAPI.LVGS_NORMAL;
				_apiGroup.cbSize = Marshal.SizeOf(typeof(ListViewAPI.LVGROUP));

				ptrRetVal = (int)ListViewAPI.SendMessage(_owner.Handle, ListViewAPI.LVM_INSERTGROUP, -1, ref _apiGroup);
			}
			catch(Exception ex)
			{
				throw new System.Exception("An exception in ListViewAPI.AddListViewGroup occured: " + ex.Message);
			}

			_text = text; 
			_index = index; 
		} 

		public XPListViewItemCollection Items
		{
			get { return items_;}
		}

		public XPListViewGroup(string text) 
		{ 
			_text = text; 
		} 

		public string GroupText { 
			get { 
				return _text; 
			} 
			set { 
				_text = value; 
				_apiGroup.pszHeader = _text;
				_apiGroup.cchHeader = _apiGroup.pszHeader.Length;
				_apiGroup.stateMask = ListViewAPI.LVGS_NORMAL;
				_apiGroup.state = ListViewAPI.LVGS_NORMAL;
				ListViewAPI.SendMessage(_owner.Handle, ListViewAPI.LVM_SETGROUPINFO, _apiGroup.iGroupId, ref _apiGroup);
			} 
		} 

		public int GroupIndex { 
			get { 
				return _index; 
			} 
			set { 
				_index = value; 
			} 
		} 

		public override string ToString() { 
			return _text; 
		} 
	}
}
