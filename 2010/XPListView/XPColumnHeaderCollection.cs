﻿using System;
using System.Windows.Forms;
using System.Collections;

namespace XPListview
{
	/// <summary>
	/// Summary description for XPColumnHeaderCollection.
	/// </summary>
	public class XPColumnHeaderCollection : System.Windows.Forms.ListView.ColumnHeaderCollection
	{
		public XPColumnHeaderCollection(XPListView owner): base(owner as ListView)
		{
		}

		public XPColumnHeader this[int index]
		{
			get{ return base[index] as XPColumnHeader;}
		}

		public int Add(XPColumnHeader header)
		{
			return base.Add(header);
		}
	}
}
