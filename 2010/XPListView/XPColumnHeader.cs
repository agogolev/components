﻿using System;
using System.Windows.Forms;

namespace XPListview
{
		/// <summary>
	/// Summary description for XPColumnHeader.
	/// </summary>
	public class XPColumnHeader : ColumnHeader
	{
		private string columnName_;
		public XPColumnHeader() : base()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public string ColumnName
		{
			get{ return columnName_; }
			set{ columnName_ = value;} 
		}
	}
}
