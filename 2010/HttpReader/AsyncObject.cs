using System.IO;
using System.Net;
using System.Text;

namespace HttpReader
{
	public class AsyncObject
	{
		const int BUFFER_SIZE = 1024;
		public StringBuilder DataBuilder;
		public Stream BinaryStream;
		public byte[] Buffer;
		public HttpWebRequest Request;
		public HttpWebResponse Response;
		public Stream ResponseStream;
		public AsyncObject()
		{
			Buffer = new byte[BUFFER_SIZE];
			DataBuilder = new StringBuilder();
			Request = null;
			Response = null;
		}
	}
}