﻿using System;
using System.Text;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Net;
using System.IO;

namespace HttpReader
{
	public class DigestHttpReader 
	{
		public string Login { get; set; }
		public string Password { get; set; }
		public string Host { get; set; }
		public string Dir { get; set; }
		public string Realm { get; internal set; }
		public string Nonce { get; internal set; }
		public string Qop { get; internal set; }
		public string CNonce { get; internal set; }
		public DateTime CNonceDate { get; internal set; }
		public int Nc { get; internal set; }
		private static string CalculateMd5Hash(
				string input)
		{
			var inputBytes = Encoding.ASCII.GetBytes(input);
			var hash = MD5.Create().ComputeHash(inputBytes);
			var sb = new StringBuilder();
			foreach (var b in hash)
				sb.Append(b.ToString("x2"));
			return sb.ToString();
		}

		private static string GrabHeaderVar(
				string varName,
				string header)
		{
			var regHeader = new Regex(string.Format(@"{0}=""([^""]*)""", varName));
			var matchHeader = regHeader.Match(header);
			if (matchHeader.Success)
				return matchHeader.Groups[1].Value;
			throw new ApplicationException(string.Format("Header {0} not found", varName));
		}
		private string GetDigestHeader(string dir)
		{
			Nc = Nc + 1;

			var ha1 = CalculateMd5Hash(string.Format("{0}:{1}:{2}", Login, Realm, Password));
			var ha2 = CalculateMd5Hash(string.Format("{0}:{1}", "GET", dir));
			var digestResponse =
					CalculateMd5Hash(string.Format("{0}:{1}:{2:00000000}:{3}:{4}:{5}", ha1, Nonce, Nc, CNonce, Qop, ha2));

			return string.Format("Digest username=\"{0}\", realm=\"{1}\", nonce=\"{2}\", uri=\"{3}\", " +
					"algorithm=MD5, response=\"{4}\", qop={5}, nc={6:00000000}, cnonce=\"{7}\"",
					Login, Realm, Nonce, dir, digestResponse, Qop, Nc, CNonce);
		}
		public string Request()
		{
			var url = Host + Dir;
			var uri = new Uri(url);

			var request = (HttpWebRequest)WebRequest.Create(uri);

			// If we've got a recent Auth header, re-use it!
			if (!string.IsNullOrEmpty(CNonce) &&
					DateTime.Now.Subtract(CNonceDate).TotalHours < 1.0)
			{
				request.Headers.Add("Authorization", GetDigestHeader(Dir));
			}

			HttpWebResponse response;
			try
			{
				response = (HttpWebResponse)request.GetResponse();
			}
			catch (WebException ex)
			{
				// Try to fix a 401 exception by adding a Authorization header
				if (ex.Response == null || ((HttpWebResponse)ex.Response).StatusCode != HttpStatusCode.Unauthorized)
					throw;

				var wwwAuthenticateHeader = ex.Response.Headers["WWW-Authenticate"];
				Realm = GrabHeaderVar("realm", wwwAuthenticateHeader);
				Nonce = GrabHeaderVar("nonce", wwwAuthenticateHeader);
				Qop = GrabHeaderVar("qop", wwwAuthenticateHeader);

				Nc = 0;
				CNonce = new Random().Next(123400, 9999999).ToString();
				CNonceDate = DateTime.Now;

				var request2 = (HttpWebRequest)WebRequest.Create(uri);
				request2.Headers.Add("Authorization", GetDigestHeader(Dir));
				response = (HttpWebResponse)request2.GetResponse();
			}
			var reader = new StreamReader(response.GetResponseStream());
			return reader.ReadToEnd();
		}

	}
}
