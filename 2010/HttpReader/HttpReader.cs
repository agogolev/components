using System;
using System.Collections;
using System.Threading;
using System.Text;
using System.Net;
using System.IO;
using System.Configuration;

namespace HttpReader
{
	/// <summary>
	/// Summary description for HTTPReader.
	/// </summary>
	public class HTTPReader
	{
		public ICredentials Credentials { get; set; }

		public ReaderMode Mode { get; set; }

		private readonly ArrayList headers = new ArrayList();
		public void AddHeader(string header)
		{
			headers.Add(header);
		}
		public void ClearHeaders()
		{
			headers.Clear();
		}

		public int Timeout { get; set; }

		public ProxyRec Proxy;
		public string Response;
		public string UserAgent = "Unknown";
		public HttpWebResponse ResponseHandler { get; set; }
		private Stream binaryStream;
		public Stream BinaryStream
		{
			get
			{
				if (binaryStream == null) return new MemoryStream();
				return binaryStream;
			}
			set
			{
				binaryStream = value;
			}
		}
		public string Login { get; set; }
		public string Password { get; set; }
		public string Url { get; set; }
		public string PostData { get; set; }

		public string ContentType { get; set; }

		private Exception _ex;
		private readonly Encoding _encTo;
		private readonly Encoding _encFrom;
		private readonly ManualResetEvent workDone = new ManualResetEvent(false);

		public HTTPReader(string url, string login, string password)
            : this(url, login, password, Encoding.UTF8, Encoding.UTF8)
		{
		}

		public HTTPReader(string url, string login, string password, Encoding encodingTo, Encoding encodingFrom)
		{
			Url = url;
			Login = login;
			Password = password;
			_encTo = encodingTo;
			_encFrom = encodingFrom;
		}

		public HTTPReader(string url, string postData, string login, string password)
            : this(url, login, password, Encoding.UTF8, Encoding.UTF8)
		{
			PostData = postData;
		}

		public HTTPReader(string url, string postData, string login, string password, Encoding encodingTo, Encoding encodingFrom)
		{
			Url = url;
			Login = login;
			Password = password;
			_encTo = encodingTo;
			_encFrom = encodingFrom;
			PostData = postData;
		}

		public void Request()
		{
			var request = (HttpWebRequest)WebRequest.Create(new Uri(Url));
			request.ContentType = ContentType;
			foreach (string header in headers)
			{
				if (header.ToLower().IndexOf("content-type") != -1)
				{
					request.ContentType = header.Split(":".ToCharArray())[1];
				}
				else if (header.ToLower().IndexOf("keep-alive") != -1)
				{
					request.KeepAlive = true;
				}
				else
				{
					request.Headers.Add(header);
				}
			}

			if (Timeout != 0) 
			request.Timeout = Timeout * 1000;
			if (Credentials != null) request.Credentials = Credentials;
			else if (Login != "") request.Credentials = new NetworkCredential(Login, Password);

			var proxy = ConfigurationManager.AppSettings["proxy"];
			if (proxy != null)
			{
				var pr = proxy == "default" ? WebRequest.DefaultWebProxy : new WebProxy(proxy, true);
				bool useDefault = (string.Compare(ConfigurationManager.AppSettings["proxyAuth"], "integrated", true) == 0);
				if (useDefault) pr.Credentials = CredentialCache.DefaultCredentials;
				else
				{
					string proxyLogin = ConfigurationManager.AppSettings["proxyLogin"];
					string proxyPassword = ConfigurationManager.AppSettings["proxyPassword"];
					string proxyDomain = ConfigurationManager.AppSettings["proxyDomain"];
					pr.Credentials = new NetworkCredential(proxyLogin, proxyPassword, proxyDomain);
				}
				request.Proxy = pr;
			}

			if (!string.IsNullOrEmpty(PostData))
			{
				request.Method = "POST";
				byte[] post = _encTo.GetBytes(PostData);
				if (request.ContentType == null)
				{
					request.ContentType = "application/x-www-form-urlencoded";
				}
				request.ContentLength = post.Length;
				using (Stream newStream = request.GetRequestStream())
				{
					newStream.Write(post, 0, post.Length);
				}
			}


			if (Proxy != null)
			{
				var nc = new NetworkCredential(Proxy.Login, Proxy.Password, Proxy.Domain);
				var pr = new WebProxy(Proxy.Address, true, null, nc);
				request.Proxy = pr;
			}
			var obj = new AsyncObject { Request = request, BinaryStream = BinaryStream };
			request.BeginGetResponse(ResponseCallback, obj);
			workDone.WaitOne();

			if (obj.Response != null)
				obj.Response.Close();

			if (_ex != null)
				throw _ex;//��� ���������� ���������� ����� ���������� � ������ ������

		}

		public void ResponseCallback(IAsyncResult result)
		{
			try
			{
				var obj = (AsyncObject)result.AsyncState;
				var request = obj.Request;
				ResponseHandler = (HttpWebResponse)request.EndGetResponse(result);
				obj.Response = ResponseHandler;
				obj.ResponseStream = ResponseHandler.GetResponseStream();
				obj.ResponseStream.BeginRead(obj.Buffer, 0, obj.Buffer.Length, ReadCallback, obj);
			}
			catch (Exception ex)
			{
				workDone.Set();
				//				throw;//���� throw ���������� � ������ ������ - � � �������� ���-������ �� ���-������ Object reference not set ...
				_ex = ex;
			}
		}

		public void ReadCallback(IAsyncResult result)
		{
			try
			{
				var obj = (AsyncObject)result.AsyncState;
				var read = obj.ResponseStream.EndRead(result);
				if (read > 0)
				{
					obj.BinaryStream.Write(obj.Buffer, 0, read);
					obj.ResponseStream.BeginRead(obj.Buffer, 0, obj.Buffer.Length, ReadCallback, obj);
				}
				else
				{
					if (Mode == ReaderMode.Text)
					{
						obj.BinaryStream.Position = 0;
						byte[] message;
						using (var reader = new BinaryReader(obj.BinaryStream))
						{
							message = reader.ReadBytes((int)obj.BinaryStream.Length);
						}
						obj.DataBuilder.Append(_encFrom.GetString(message, 0, message.Length));
						Response = obj.DataBuilder.ToString();
					}
					workDone.Set();
				}
			}
			catch (Exception ex)
			{
				workDone.Set();
				//				throw;//���� throw ���������� � ������ ������ - � � �������� ���-������ �� ���-������ Object reference not set ...
				_ex = ex;
			}
		}
	}
}
