using System;
using NLog;

namespace SMSHelper
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	public class SMSSenderHelper
	{
		private static Logger logger = LogManager.GetCurrentClassLogger();
		private const string xmlSMS = @"<?xml version=""1.0"" encoding=""utf-8"" ?>
<package login=""{0}"" password=""{1}"">
	<message>
		<default sender=""{2}"" />
		<msg recipient=""{3}"" sender=""{2}"" type=""0"">{4}</msg>
	</message>
</package>
";
		public static string SendMessage(string phone, string bodyText, string login, string password, string sender) {
			string body;
			try {
				body = string.Format(xmlSMS, new object[]{login, password, sender, phone, bodyText});
				var read = new HttpReader.HTTPReader("http://service.smsconsult.ru/", body, "", "", System.Text.Encoding.UTF8, System.Text.Encoding.UTF8);
				read.Request();
				body = read.Response;
			}
			catch(Exception ex) {
				return "������� SMS ����������� �������. ������: "+ex.Message;
			}
			return body;
		}
	}
}
