using System;
using System.Web.SessionState;
using System.Data;
using System.Data.SqlClient;
using System.Xml;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections;

namespace HtmlEditor
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	public class WebPage
	{
		protected string _connStr;
		protected user _user;
		private XmlDocument _pageData;
		protected SqlCommand _cmd;

		public XmlDocument PageData 
		{
			get {return _pageData;}
		}
		
		public string Xml 
		{
			get {return _pageData.InnerXml;}
		}
		
		public WebPage(string ConnStr) 
		{
			_connStr = ConnStr;
		}
		
		public WebPage(HttpSessionState session) 
		{
			_user = (user)session["user"];
			_connStr = _user.connString;
		}

		public void LoadData () 
		{
			InitCommand();
			try 
			{
				doLoadData();
			}
			finally 
			{
				CloseCommand();
			}
		}

		protected virtual void doLoadData() 
		{
			_pageData = new XmlDocument();
			//_pageData.LoadXml("<?xml version=\"1.0\" encoding=\"Windows-1251\"?><root></root>");
		}
		
		public void InitCommand () 
		{
			_cmd = new SqlCommand("", new SqlConnection(_connStr));
			_cmd.Connection.Open();
		}

		public void CloseCommand() 
		{
			if(_cmd != null) _cmd.Connection.Close();
		}

		public XmlNode AppendXml(XmlNode element, string rootTag, string sql) 
		{
			_cmd.CommandText = sql;
			XmlReader xr = _cmd.ExecuteXmlReader();
			XmlDocument child = new XmlDocument();
			child.Load(xr);
			XmlNode newItem = element.OwnerDocument.ImportNode(child.DocumentElement, true);
			element.AppendChild(newItem);
			return newItem;
		}
		public XmlNode AppendXmlStr(XmlNode element, string rootTag, string xml) 
		{
			XmlDocument child = new XmlDocument();
			child.LoadXml("<"+rootTag+">"+EscapeAmp(xml)+"</"+rootTag+">");
			XmlNode newItem = element.OwnerDocument.ImportNode(child.DocumentElement, true);
			return element.AppendChild(newItem);
		}

		public string record2Xml(string sql, string recordTag) 
		{
			StringWriter sw = new StringWriter();
			XmlTextWriter w = new XmlTextWriter(sw);
			_cmd.CommandText = sql;
			SqlDataReader dr = _cmd.ExecuteReader();
			try 
			{
				while(dr.Read()) 
				{
					if (recordTag != null) w.WriteStartElement(recordTag);
					for(int i = 0; i < dr.FieldCount; i++) 
					{
						string t = dr.GetName(i);
						if (t.Equals("")) w.WriteStartElement("name"+i.ToString());
						else w.WriteStartElement(dr.GetName(i));
						w.WriteString(dr[i].ToString());
						w.WriteEndElement();
					}
					if (recordTag != null) w.WriteEndElement();
				}
			}
			finally 
			{
				dr.Close();
			}
			return sw.ToString();
		}
		public string record2Xml(SqlDataReader dr, string recordTag) 
		{
			StringWriter sw = new StringWriter();
			XmlTextWriter w = new XmlTextWriter(sw);
			if(dr.Read()) 
			{
				if (recordTag != null) w.WriteStartElement(recordTag);
				for(int i = 0; i < dr.FieldCount; i++) 
				{
					w.WriteStartElement(dr.GetName(i));
					w.WriteString(dr[i].ToString());
					w.WriteEndElement();
				}
				if (recordTag != null) w.WriteEndElement();
			}
			return sw.ToString();
		}
		
		public string EscapeSQL (string value) 
		{
			return "'"+value.Replace("'", "''")+"'";
		}

		public SqlDataReader execSQL(string sql) 
		{
			_cmd.CommandText = sql;
			SqlDataReader dr = _cmd.ExecuteReader();
			return dr;
		}

		public int executeNoQuery(string sql) 
		{
			_cmd.CommandText = sql;
			return _cmd.ExecuteNonQuery();
		}

		// ===============================================
		// string function escapeAmp(str)
		// ��������� ������, ���������� �������� ������, 
		// � ������� ���������� (&) �������� �� &amp; �
		// ��� ������, ����� ��� �� �������� ������ entity
		// (����� ��� &#1049;, &quot; � �.�.)
		// ===============================================
		public string EscapeAmp(string source) 
		{
			Regex re = new Regex("&(?!(([a-z]+|#[0-9]+);))", RegexOptions.IgnoreCase);
			if(source != null)	return re.Replace(source, "&amp;");
			else return "";
		}
	}


	// ========================================================================
	// class themeRecord 
	// ���������, ���������� ��������, OID ������� � ���-�� ��������� ������
	// ========================================================================
	public class themeRecord 
	{
		public string themeName;
		public Guid themeOID;
		public int itemCount;

		public themeRecord(string name, Guid OID, int count)
		{
			themeName = name;
			themeOID = OID;
			itemCount = count;
		}
	}
}
