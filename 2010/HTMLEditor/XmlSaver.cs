using System;
using System.Xml;
using System.Web;
using System.Collections;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using TidyNet;

namespace HtmlEditor
{
	/// <summary>
	/// Summary description for XmlSaver.
	/// </summary>
	public class XmlSaver
	{
		public string NewInlineTags { get; set; }
		public bool NotNeedRoot { get; set; }
		private bool isWord;
		public void Save(Guid OID, string fieldName, string fieldValue, string className, bool tidyOutput = true)
		{
			Regex re = new Regex("^<doc>(.*)</doc>$", RegexOptions.Singleline | RegexOptions.IgnoreCase);
			if (tidyOutput)
			{
				fieldValue = prepareBody(prepareXML(fieldValue));
			}
			else
			{
				fieldValue = prepareText(fieldValue);
			}
			if (NotNeedRoot) {
				Match match = re.Match(fieldValue);
				if (match.Success) fieldValue = match.Groups[1].Value;
			}
			//��������� �� ���������� ������� ����
			MetaData.MetadataInfo m = DBReader.QueryConstructor.BuildMetadataInfo(className);
			MetaData.PropInfo p = (MetaData.PropInfo)m.FindProp(fieldName, 1); //7 - ������ �� ����
			if (p == null) throw new Exception(string.Format("���� {0} �� �������", fieldName));
			if (p.MaximumLength == 0 || fieldValue.Length <= p.MaximumLength)
			{
				// ��������� �� ���������� ����������, ������� ��� ������ �������� �� ���� ',' ����� ��������� ���� ����� ������������ ParseInput ����� sp_xml_preparedocument
				Hashtable prms = new Hashtable();
				prms["@OID"] = OID;
				prms["@className"] = className;
				if (fieldValue.IndexOf("','") == -1)
				{
					prms["@FieldNames"] = fieldName;
					prms["@FieldValues"] = fieldValue;
				}
				else
				{
					prms["@FieldNames"] = DBNull.Value;
					string xml = "<?xml version=\"1.0\" encoding=\"Windows-1251\" ?><fields><field><name>" + fieldName + "</name><value></value></field></fields>";
					XmlDocument doc = new XmlDocument();
					doc.LoadXml(xml);
					doc.SelectSingleNode("fields/field/value").InnerText = fieldValue;
					prms["@FieldValues"] = doc.OuterXml;
				}

				DBReader.DBHelper.ExecuteCommand("spUpdateObject", prms, true);
				DBReader.WebServiceExtensions.ClearWebCache("www_");
				//Regex re = new Regex("^<doc>(.*)</doc>$", RegexOptions.Singleline | RegexOptions.IgnoreCase);
				//Match match = re.Match(fieldValue);
				//if (match.Success) return match.Groups[1].Value;
				//else return fieldValue;
			}
			else throw new Exception("����� �� ��������, ��� ��� ��� ����� ��������� ������� ���� � ����");
		}
		private string prepareXML(string text)
		{
			text = ClearWordTags(text);
			text = FixIFrame(text);
			string result = TidyXML(text);
			result = ReturnIFrame(result);

			ModifyXmlEventArgs e = new ModifyXmlEventArgs(result);
			OnModifyXml(e);
			XmlDocument doc = new XmlDocument();
			doc.LoadXml(e.XmlBody);
			return e.XmlBody;
		}
		// ����������� ������������ ��������� IFRAME
		private string FixIFrame(string text)
		{
			Regex re = new Regex(@"\<IFRAME([^\>]*)\>\s*\</IFRAME\>", RegexOptions.Singleline | RegexOptions.IgnoreCase);
			return re.Replace(text, "<IFRAME$1 />");
		}
		private string ReturnIFrame(string text)
		{
			Regex re = new Regex(@"\<IFRAME([^\>]*)/\>", RegexOptions.Singleline | RegexOptions.IgnoreCase);
			return re.Replace(text, "<IFRAME$1></IFRAME>");
		}
		// �������� �� ����� word
		private string ClearWordTags(string text)
		{
			text = ClearStrangeNamespaces(text);
			Regex re = new Regex("[a-zA-Z]+=\"?mso", RegexOptions.Singleline | RegexOptions.IgnoreCase);
			if (re.IsMatch(text))
			{
				this.isWord = true;
				Regex mso = new Regex("([a-zA-Z]+=\"[^\"]*mso[^\"]+\"|[a-zA-Z]+=mso[^\\s]+)", RegexOptions.Singleline | RegexOptions.IgnoreCase);
				Regex margin = new Regex("[a-zA-Z]+=\"MARGIN[^\"]+\"", RegexOptions.Singleline | RegexOptions.IgnoreCase);
				Regex font = new Regex("(<FONT[^>]*>|</FONT>)", RegexOptions.Singleline | RegexOptions.IgnoreCase);
				Regex span = new Regex("(<SPAN[^>]*>|</SPAN>)", RegexOptions.Singleline | RegexOptions.IgnoreCase);
				Regex lang = new Regex("lang=(\"[^\"]+\"|[^\\s]+)", RegexOptions.Singleline | RegexOptions.IgnoreCase);
				text = mso.Replace(text, " ");
				text = margin.Replace(text, " ");
				text = font.Replace(text, " ");
				text = span.Replace(text, " ");
				text = lang.Replace(text, " ");
			}
			return text;
		}
		// ����� �������� ���������� namespace
		private string ClearStrangeNamespaces(string text)
		{
			ArrayList prefixes = new ArrayList();
			Regex re = new Regex("<\\?xml:namespace[^>]*prefix\\s*=\\s*\"?\\s*([a-zA-Z0-9]+)?[^>]*>", RegexOptions.Singleline | RegexOptions.IgnoreCase);
			MatchCollection matches = re.Matches(text);
			foreach (Match m in matches)
			{
				string prefix = m.Groups[1].Value;
				prefixes.Add(prefix);
			}
			re = new Regex("<\\?xml:namespace[^>]*>", RegexOptions.Singleline | RegexOptions.IgnoreCase);
			text = re.Replace(text, "");
			foreach (string prefix in prefixes)
			{
				re = new Regex("<" + prefix + ":", RegexOptions.Singleline | RegexOptions.IgnoreCase);
				text = re.Replace(text, "<");
				re = new Regex("</" + prefix + ":", RegexOptions.Singleline | RegexOptions.IgnoreCase);
				text = re.Replace(text, "</");
				re = new Regex(prefix + ":[a-zA-Z0-9]+=\"[^\"]*\"", RegexOptions.Singleline | RegexOptions.IgnoreCase);
				text = re.Replace(text, "");
			}
			return text;
		}
		public string TidyXML(string text)
		{
			text = string.Format("<div>{0}</div>", text);
			string result = "";
			Tidy tDoc = new Tidy();
			TidyOptions opt = tDoc.Options;
			opt.Xhtml = true;
			opt.CharEncoding = CharEncoding.UTF8;
			opt.DocType = DocType.Omit;
			opt.DropEmptyParas = true;
			opt.EncloseText = true;
			opt.LogicalEmphasis = true;
			opt.QuoteNbsp = false;
			opt.TidyMark = false;
			//opt.ParseBlockTagNames("doc figure script", null);
			//if (NewInlineTags != null)
			//{
			//    string[] tags = NewInlineTags.Split(" ".ToCharArray());
			//    foreach (string tag in tags)
			//    {
			//        if (tag != "") tDoc.DefineInlineTag(tag);
			//    }
			//}
			if (this.isWord) opt.Word2000 = true;
			byte[] array = Encoding.UTF8.GetBytes(text);
			MemoryStream input = new MemoryStream(array);
			MemoryStream output = new MemoryStream();
			TidyMessageCollection mc = new TidyMessageCollection();
			tDoc.Parse(input, null, output, mc);
			//Let's check each message
			foreach (TidyMessage message in mc)
			{
				//If an error has been thrown, we want to trap for it
				if (message.Level == MessageLevel.Error)
				{
					//Throw a simple ApplicationException
					throw new ApplicationException(String.Format("{0} at line {1} column {2}",
				 message.Message, message.Line,
					message.Column));
				}
			}
			result = Encoding.UTF8.GetString(output.GetBuffer());
			result = NormalizeStr(result);
			if (result.ToUpper().IndexOf("<DOC") == -1)
				result = "<doc>" + result + "</doc>";
			else
			{
				int firstDoc = result.ToUpper().IndexOf("<DOC>");
				int lastDoc = result.ToUpper().LastIndexOf("</DOC>");
				result = result.Substring(firstDoc, lastDoc + 6 - firstDoc);
			}
			return result;
		}

		public static string NormalizeStr(string source)
		{
			source = source.Replace("\0", "");
			source = source.Replace("<\\/SCRIPT>", "</script>");
			Regex re = new Regex(@"<body>\s*<div>(.*)</div>\s*</body>", RegexOptions.Singleline | RegexOptions.IgnoreCase);
			Match m = re.Match(source);
			if (m.Success)
				source = m.Groups[1].Value.Trim();
			return source.Replace("\r\n", " ");
		}

		private string prepareBody(string body)
		{
			if (body == null) return null;
			XmlDocument doc = new XmlDocument();
			doc.LoadXml(body);
			ProcessForms(doc);
			ProcessArticles(doc);
			ModifyAnchor(doc, "a", "href");
			ModifyAnchor(doc, "img", "src");
			return doc.DocumentElement.OuterXml;
		}

		private string prepareText(string text)
		{
			if (text == null) return null;
			text = ModifyAnchor(text, "a", "href");
			text = ModifyAnchor(text, "img", "src");
			return text;
		}

		//������� ��� ����� � ������� ���� �����
		private void ProcessForms(XmlDocument doc)
		{
			XmlNodeList nl = doc.SelectNodes("//img");
			Regex re = new Regex(@"formExample.gif\?OID=([0-9A-F]{8,8}-[0-9A-F]{4,4}-[0-9A-F]{4,4}-[0-9A-F]{4,4}-[0-9A-F]{12,12})", RegexOptions.IgnoreCase | RegexOptions.Singleline);
			foreach (XmlNode n in nl)
			{
				if (n.Attributes["src"] != null)
				{
					Match m = re.Match(n.Attributes["src"].Value);
					if (m.Success)
					{
						XmlNode form = doc.CreateElement("linkForm");
						form.Attributes.Append(doc.CreateAttribute("OID")).Value = m.Groups[1].Value.ToUpper();
						n.ParentNode.ReplaceChild(form, n);
					}
				}
			}
		}

		//������� ��� ������� � ������� ���� �����
		private void ProcessArticles(XmlDocument doc)
		{
			XmlNodeList nl = doc.SelectNodes("//div[@name='linkedArticle']");
			Regex re = new Regex(@"linkedArticle_([0-9A-F]{8,8}-[0-9A-F]{4,4}-[0-9A-F]{4,4}-[0-9A-F]{4,4}-[0-9A-F]{12,12})", RegexOptions.IgnoreCase | RegexOptions.Singleline);
			foreach (XmlNode n in nl)
			{
				if (n.Attributes["id"] != null)
				{
					Match m = re.Match(n.Attributes["id"].Value);
					if (m.Success)
					{
						XmlNode form = doc.CreateElement("linkArticle");
						form.Attributes.Append(doc.CreateAttribute("OID")).Value = m.Groups[1].Value.ToUpper();
						n.ParentNode.ReplaceChild(form, n);
					}
				}
			}
		}

		//���������� ��������� ������ http://localhost:114 � ������ <a href � <img src
		private void ModifyAnchor(XmlDocument doc, string tagName, string atrName)
		{
			XmlNodeList nl = doc.SelectNodes("//" + tagName + "[@" + atrName + "]");
			foreach (XmlNode n in nl)
			{
				string link = n.Attributes[atrName].Value;
				if (link.ToLower().IndexOf("http://") != -1)
				{
					Uri back = HttpContext.Current.Request.Url;
					Uri linkUri = new Uri(link);
					if (back.Authority == linkUri.Authority)
					{
						string path = linkUri.AbsolutePath.Substring(0, linkUri.AbsolutePath.LastIndexOf('/'));
						string backPath = back.AbsolutePath.Substring(0, back.AbsolutePath.LastIndexOf('/'));

						if (string.Compare(path, backPath, true) == 0)
						{
							link = linkUri.AbsolutePath.Substring(linkUri.AbsolutePath.LastIndexOf('/') + 1) + linkUri.Query + linkUri.Fragment;
						}
						else
						{
							link = linkUri.AbsolutePath + linkUri.Query + linkUri.Fragment;
						}
						n.Attributes[atrName].Value = link;
					}
				}
			}
		}

		//���������� ��������� ������ http://localhost:114 � ������ <a href � <img src
		private string ModifyAnchor(string text, string tagName, string atrName)
		{
			Regex findAttr = new Regex("\\<" + tagName + "[^>]*" + atrName + "=['\"]([^'\"]*)['\"][^>]*\\>", RegexOptions.IgnoreCase | RegexOptions.Singleline);
			return findAttr.Replace(text, ModifyAnchorEvaluator);
		}

		private string ModifyAnchorEvaluator(Match match)
		{
			string tag = match.Value;
			string link = match.Groups[1].Value;
			if (link.IndexOf("http://", StringComparison.InvariantCultureIgnoreCase) != -1)
			{
				Uri back = HttpContext.Current.Request.Url;
				Uri linkUri = new Uri(link);
				if (back.Authority == linkUri.Authority)
				{
					string path = linkUri.AbsolutePath.Substring(0, linkUri.AbsolutePath.LastIndexOf('/'));
					string backPath = back.AbsolutePath.Substring(0, back.AbsolutePath.LastIndexOf('/'));

					if (String.Compare(path, backPath, StringComparison.InvariantCultureIgnoreCase) == 0)
					{
						link = linkUri.AbsolutePath.Substring(linkUri.AbsolutePath.LastIndexOf('/') + 1) + linkUri.Query +
						       linkUri.Fragment;
					}
					else
					{
						link = linkUri.AbsolutePath + linkUri.Query + linkUri.Fragment;
					}
					return string.Format("{0}{1}{2}", tag.Substring(0, match.Groups[1].Index), link,
					                     tag.Substring(match.Groups[1].Index + match.Groups[1].Length));
				}
			}
			return tag;
		}

		public event ModifyXmlEventHandler ModifyXml;
		protected virtual void OnModifyXml(ModifyXmlEventArgs e)
		{
			if (ModifyXml != null) ModifyXml(this, e);
		}
	}
	public class ModifyXmlEventArgs : EventArgs
	{
		private string _xmlBody;
		public string XmlBody
		{
			get
			{
				return _xmlBody;
			}
			set
			{
				_xmlBody = value;
			}
		}
		public ModifyXmlEventArgs(string body)
		{
			_xmlBody = body;
		}
	}
	public delegate void ModifyXmlEventHandler(object sender, ModifyXmlEventArgs e);
}
