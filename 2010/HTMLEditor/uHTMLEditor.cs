using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.Xml.Xsl;
using System.Xml;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Reflection;
using DBReader;
using MetaData;
using System.Collections;
using System.Data;
using System.Data.SqlClient;

namespace HtmlEditor
{
	/// <summary>
	/// Summary description for WebCustomControl1.
	/// </summary>
	[ToolboxData("<{0}:Editor runat=server></{0}:Editor>")]
	public class Editor : System.Web.UI.WebControls.WebControl
	{
		//		private EditDescrPage _page;
		private XmlDocument PageData;

		[Bindable(true),
		DefaultValue("")]
		public string ConnectionString
		{
			get
			{
				return ViewState["ConnectionString"] != null ? (string)ViewState["ConnectionString"] : "";
			}

			set
			{
				ViewState["ConnectionString"] = value;
			}
		}

		[Bindable(true),
		DefaultValue("")]
		public string FieldString
		{
			get
			{
				return ViewState["FieldString"] != null ? (string)ViewState["FieldString"] : "";
			}

			set
			{
				ViewState["FieldString"] = value;
			}
		}
		[Bindable(true),
		DefaultValue("")]
		public string MultiField
		{
			get
			{
				return ViewState["MultiField"] != null ? (string)ViewState["MultiField"] : "";
			}

			set
			{
				ViewState["MultiField"] = value;
			}
		}
		[Bindable(true),
		DefaultValue(typeof(Guid), "00000000-0000-0000-0000-000000000000")]
		public Guid OrdValue
		{
			get
			{
				return ViewState["OrdValue"] != null ? (Guid)ViewState["OrdValue"] : Guid.Empty;
			}
			set
			{
				ViewState["OrdValue"] = value;
			}
		}

		[Bindable(true),
		DefaultValue("")]
		public string ClassName
		{
			get
			{
				return ViewState["ClassName"] != null ? (string)ViewState["ClassName"] : "";
			}

			set
			{
				ViewState["ClassName"] = value;
			}
		}

		[Bindable(true),
		DefaultValue(typeof(Guid), "00000000-0000-0000-0000-000000000000")]
		public Guid OID
		{
			get
			{
				return ViewState["OID"] != null ? (Guid)ViewState["OID"] : Guid.Empty;
			}
			set
			{
				ViewState["OID"] = value;
			}
		}

		[Bindable(true),
		DefaultValue("")]
		public int SiteID
		{
			get
			{
				return ViewState["SiteID"] != null ? (int)ViewState["SiteID"] : 0;
			}

			set
			{
				ViewState["SiteID"] = value;
			}
		}

		protected override void Render(HtmlTextWriter output)
		{
			if (OID == Guid.Empty)
			{
				return;
			}

			XslCompiledTransform tr = new XslCompiledTransform();
			tr.Load(Page.Server.MapPath("~/xslt/editDescription.xslt"));
			tr.Transform(PageData, null, output);
		}


		public override void DataBind()
		{
			if (OID != Guid.Empty)
			{
				LoadData();
				base.DataBind();
			}
		}

		public void LoadData() 
		{
			if (OID != Guid.Empty)
			{//1
				if (FieldString != "")
				{
					if (ClassName == "CArticle")//���� ������ ������
					{
						XslCompiledTransform xslt = new XslCompiledTransform();
						if (FieldString.ToLower() == "body")
							xslt.Load(Page.Server.MapPath("~/xslt/From_XmlGetArticle_view_to_current_view.xslt"));
						else
							xslt.Load(Page.Server.MapPath("~/xslt/From_XmlGetArticle_view_to_current_viewE.xslt"));

						//������� ������
						string sql = "xmlGetArticle '"+OID+"'";
						SqlCommand cmd = DBReader.QueryConstructor.GetCommand(sql, CommandType.Text, null); //UserInfo
						cmd.Connection.Open();
						StringBuilder sb = new StringBuilder();
						try 
						{
							SqlDataReader dr = cmd.ExecuteReader();
							while (dr.Read()) 
							{
								sb.Append((string)dr[0]);
							}
						}
						finally 
						{
							cmd.Connection.Close();
						}

						XmlDocument doc = new XmlDocument();
						doc.LoadXml(sb.ToString());
						PageData = new XmlDocument();
									
						//����������� ���������� XML �� ���� XmlGetArticle � ���, ������� ��� ��������
												//XmlReader rdr = null;
						System.Text.StringBuilder sbXml = new System.Text.StringBuilder();
						StringWriter sw = new StringWriter(sbXml);
						XsltArgumentList args = null;
						xslt.Transform(doc, args, sw);
						PageData.LoadXml(sw.ToString());
						
						//�������� �����
						PageData.DocumentElement.InnerXml += BuildStyles();

						//��������� preview
						XmlNodeList nl = DBReader.DBHelper.AppSettings("previewList/"+ClassName+"/preview[@siteID="+SiteID+"]");
						foreach(XmlNode n in nl) 
						{
							XmlNode nodePreview = PageData.DocumentElement.AppendChild(PageData.CreateElement("preview"));
							nodePreview.InnerText = n.Attributes["value"].Value.Replace("%OID%",OID.ToString());
							nodePreview.Attributes.Append(PageData.CreateAttribute("name")).Value = n.Attributes["name"].Value;
						}
					}
					else//���� ������ �� ������
					{
						//������� ������
						string sql = QueryConstructor.BuildGetObject(OID, ClassName, FieldString, "images", null);
						SqlCommand cmd = DBReader.QueryConstructor.GetCommand(sql, CommandType.Text, null); //UserInfo
						cmd.Parameters.AddWithValue("@OID", OID);
						cmd.Connection.Open();
						StringBuilder sb = new StringBuilder();
						try 
						{
							SqlDataReader dr = cmd.ExecuteReader();
							while (dr.Read()) 
							{
								sb.Append((string)dr[0]);
							}
						}
						finally 
						{
							cmd.Connection.Close();
						}

						XmlDocument doc = new XmlDocument();
						doc.LoadXml(sb.ToString());
						PageData = new XmlDocument();

						//����������� ���������� ���� (��� ����� ���� ���������� xml) - ���� �������� � <doc></doc>
						XmlNode node = doc.SelectSingleNode("item/"+FieldString);
						string body="";
						if (node != null) {
							XmlSaver saver = new XmlSaver();
							body = saver.TidyXML(node.InnerText);
						}
						else body = "<doc></doc>";

						//���������� ��������� images, ����������� � �������
						XmlNodeList nodes = doc.SelectNodes("item/images");
						XmlNode child;
				
						StringBuilder images = new StringBuilder();
						foreach(XmlNode n in nodes)
						{
							child = n.SelectSingleNode("propValue");
							if(child!=null)
							{
								images.Append("<linkimages OID=\"" + child.FirstChild.Value +"\"");
								child = n.SelectSingleNode("propValue_NK");
								if(child!=null)
									images.Append(" title=\"" + removeTags(child.FirstChild.Value.Replace("\"", "&quot;"), " ") +"\"");
								images.Append(" />");
							}
						}

						//�������� �����
						string styles = BuildStyles();

						body = "<?xml version=\"1.0\" encoding=\"utf-8\"?><root>" + 
							body + images + styles + "</root>";
						PageData.LoadXml(body);
					}
				}
				else
				{
					string sql = @"
SELECT 
	mf.tableName,
	mf.ordField,
	mf.fieldName
FROM t_ClassName cn
INNER JOIN t_MultiFields mf ON mf.CID=cn.CID AND mf.propName='"+MultiField+@"'
WHERE cn.className='"+ClassName+"'";
					DataSet ds = DBHelper.GetDataSetSql(sql, "table", null);
					if (ds.Tables["table"].Rows.Count > 0)
					{
						string tableName = (string)ds.Tables["table"].Rows[0]["tableName"];
						string ordField = (string)ds.Tables["table"].Rows[0]["ordField"];
						string fieldName = (string)ds.Tables["table"].Rows[0]["fieldName"];

						sql = @"
SELECT 
	"+fieldName+@"
FROM "+tableName+@" gd
WHERE OID='"+OID+"' AND "+ordField+"='"+OrdValue+"'";
						object val = DBHelper.ExecuteScalar(sql, null, false);
						string body;
						if(val != null)
						{
							body = (string)val;
							//����� ��� �������� <doc></doc>
							if(body.Length >= 11)
							{
								if (body.Substring(0,5) != "<doc>")
								{
									body = "<doc>"+body+"</doc>";
								}
							}
							else body = "<doc>"+body+"</doc>";
						}
						else body = "<doc></doc>";
						body = "<?xml version=\"1.0\" encoding=\"Windows-1251\"?><root>" + 
							body + "</root>";
						PageData.LoadXml(body);
					}
				}
			}//1
		}

		private static string removeTags(string text, string replaceText)
		{
			Regex re = new Regex("<[^>]*>", RegexOptions.Singleline);
			return re.Replace(text, replaceText);
		}

		private string BuildStyles()
		{
			//Styles
			StringBuilder styles = new StringBuilder();
			styles.Append(@"<styles>");
			XmlNodeList nl = DBReader.DBHelper.AppSettings("styles/style[@siteID=" + SiteID + "]");
			foreach (XmlNode n in nl)
			{
				styles.Append(@"<item>" + n.InnerText + @"</item>");
			}
			styles.Append(@"</styles>");
			return styles.ToString();
		}

	}
}
