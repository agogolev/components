﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Data;
using System.Xml;
using System.Xml.Xsl;
using System.IO;
using DBReader;
using System.Text.RegularExpressions;

namespace HtmlEditor
{
	public class TextBoxEditor : System.Web.UI.WebControls.TextBox
	{
		public TextBoxEditor()
		{
			TextMode = TextBoxMode.MultiLine;
		}
		private XmlDocument PageData;

		[Bindable(true),
		DefaultValue("")]
		public string ConnectionString
		{
			get
			{
				return ViewState["ConnectionString"] != null ? (string)ViewState["ConnectionString"] : "";
			}

			set
			{
				ViewState["ConnectionString"] = value;
			}
		}

		[Bindable(true),
		DefaultValue("")]
		public string FieldString
		{
			get
			{
				return ViewState["FieldString"] != null ? (string)ViewState["FieldString"] : "";
			}

			set
			{
				ViewState["FieldString"] = value;
			}
		}
		[Bindable(true),
		DefaultValue("")]
		public string ClassName
		{
			get
			{
				return ViewState["ClassName"] != null ? (string)ViewState["ClassName"] : "";
			}

			set
			{
				ViewState["ClassName"] = value;
			}
		}

		[Bindable(true),
		DefaultValue(typeof(Guid), "00000000-0000-0000-0000-000000000000")]
		public Guid OID
		{
			get
			{
				return ViewState["OID"] != null ? (Guid)ViewState["OID"] : Guid.Empty;
			}
			set
			{
				ViewState["OID"] = value;
			}
		}
		public override void DataBind()
		{
			if (OID != Guid.Empty)
			{
				LoadData();
				base.DataBind();
			}
		}
		public void LoadData()
		{
			StringBuilder sb = new StringBuilder();
			if (OID != Guid.Empty)
			{//1
				if (ClassName == "CArticle")//если объект статья
				{
					XslCompiledTransform xslt = new XslCompiledTransform();
					xslt.Load(Page.Server.MapPath("~/xslt/From_XmlGetArticle_view_to_current_view.xslt"));

					//получим данные
					string sql = "xmlGetArticle '" + OID + "'";
					SqlCommand cmd = DBReader.QueryConstructor.GetCommand(sql, CommandType.Text, null); //UserInfo
					cmd.Connection.Open();

					try
					{
						SqlDataReader dr = cmd.ExecuteReader();
						while (dr.Read())
						{
							sb.Append((string)dr[0]);
						}
					}
					finally
					{
						cmd.Connection.Close();
					}

					XmlDocument doc = new XmlDocument();
					doc.LoadXml(sb.ToString());
					PageData = new XmlDocument();

					//перобразуем полученный XML из вида XmlGetArticle в вид, который ждёт редактор
					//XmlReader rdr = null;
					sb.Length = 0;
					StringWriter sw = new StringWriter(sb);
					XsltArgumentList args = null;
					xslt.Transform(doc, args, sw);
					PageData.LoadXml(sb.ToString());

				}
				else//если объект не статья
				{
					//получим данные
					string sql = QueryConstructor.BuildGetObject(OID, ClassName, FieldString, "images", null);
					SqlCommand cmd = DBReader.QueryConstructor.GetCommand(sql, CommandType.Text, null); //UserInfo
					cmd.Parameters.AddWithValue("@OID", OID);
					cmd.Connection.Open();
					try
					{
						SqlDataReader dr = cmd.ExecuteReader();
						while (dr.Read())
						{
							sb.Append((string)dr[0]);
						}
					}
					finally
					{
						cmd.Connection.Close();
					}

					XmlDocument doc = new XmlDocument();
					doc.LoadXml(sb.ToString());
					PageData = new XmlDocument();

					//преобразуем содержание поля (оно может быть невалидным xml) - надо обернуть в <doc></doc>
					XmlNode node = doc.SelectSingleNode("item/" + FieldString);
					string body = "";
					if (node != null)
					{
						XmlSaver saver = new XmlSaver();
						body = saver.TidyXML(node.InnerText);
					}
					else body = "<doc></doc>";

					//обработаем коллекцию images, привязанных к объекту
					XmlNodeList nodes = doc.SelectNodes("item/images");
					XmlNode child;

					StringBuilder images = new StringBuilder();
					foreach (XmlNode n in nodes)
					{
						child = n.SelectSingleNode("propValue");
						if (child != null)
						{
							images.Append("<linkimages OID=\"" + child.FirstChild.Value + "\"");
							child = n.SelectSingleNode("propValue_NK");
							if (child != null)
								images.Append(" title=\"" + removeTags(child.FirstChild.Value.Replace("\"", "&quot;"), " ") + "\"");
							images.Append(" />");
						}
					}

					body = "<?xml version=\"1.0\" encoding=\"utf-8\"?><root>" +
						body + images + "</root>";
					PageData.LoadXml(body);
				}
				XslCompiledTransform tr = new XslCompiledTransform();
				tr.Load(Page.Server.MapPath("~/xslt/editDescription.xslt"));
				sb.Length = 0;
				StringWriter writer = new StringWriter(sb);
				tr.Transform(PageData, null, writer);
				Text = sb.ToString();
			}//1
		}
		private static string removeTags(string text, string replaceText)
		{
			Regex re = new Regex("<[^>]*>", RegexOptions.Singleline);
			return re.Replace(text, replaceText);
		}
	}
}
