using System;
using System.Text;
using System.Xml;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;
using DBReader;

namespace HtmlEditor
{
	/// <summary>
	/// Summary description for WebCustomControl1.
	/// </summary>
	[ToolboxData("<{0}:ScriptBlock runat=server></{0}:ScriptBlock>")]
	public class ScriptBlock : WebControl {
		private StringBuilder _build;
		private int _siteID;
		public int SiteID {
			get {
				return _siteID;
			}
			set {
				_siteID = value;
			}
		}
		public override void DataBind() {
			_build = new StringBuilder();
			XmlNodeList nl = DBReader.DBHelper.AppSettings("cssFiles/css[@siteID="+SiteID+"]");
			foreach(XmlNode n in nl) {
				_build.Append(@"
		<link rel='stylesheet' href='"+n.InnerText+"' type='text/css' />");
			}			
			base.DataBind ();
		}

		protected override void Render(HtmlTextWriter writer) {
			if(_build != null) writer.Write(_build.ToString());
		}
	}
}
