using System;
using System.Data;
using System.Xml;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;
using ExcelUtil;

namespace MetaData
{

	[Serializable]
	public class DataSetISM : DataSet
	{
		public NumberFormatInfo NumberFormat;//нужно для парса
		public DateTimeFormatInfo DateTimeFormat;//нужно для парса
		/// <summary>
		/// Надо ли в методах GetChangesXML возвращать строки с ошибками
		/// </summary>
		public bool OnlyRowsWithoutErrors { get; set; }

		public DataSetISM()
		{
			NumberFormat = new CultureInfo("en-US").NumberFormat;
			NumberFormat.NumberDecimalSeparator = ".";

			DateTimeFormat = new CultureInfo("ru-RU").DateTimeFormat;
			DateTimeFormat.FullDateTimePattern += ".fff";
			DateTimeFormat.ShortDatePattern = "dd/MM/yyyy";
		}


		public DataSetISM(string xml, bool acceptChanges = true)
			: this()
		{
			if(!string.IsNullOrWhiteSpace(xml)) LoadFromString(xml, acceptChanges);
		}
		protected DataSetISM(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
			: base(info, context)
		{

		}
		protected DataSetISM(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context, bool ConstructSchema)
			: base(info, context, ConstructSchema)
		{

		}


		public DataTable Table
		{
			get { return Tables["table"]; }
		}

		public bool IsModified
		{
			get
			{

				if (Tables.Count > 0)
				{
					bool isModified = false;
					foreach (DataTable dt in Tables)
					{
						DataTable diff = dt.GetChanges();
						isModified = diff == null ? false : true;
						if (isModified) break;
					}
					return isModified;
				}
				else return false;
			}
		}

		/// <summary>
		/// Все строки
		/// </summary>
		private void DiffGram(XmlElement root)
		{
			DiffGram("", 0, root, null, null);
		}

		/// <summary>
		/// Все строки, но только указанные колонки (ВАЖНО: объязательно передавайте колонки, которые входят в первичный ключ)
		/// </summary>
		private void DiffGram(XmlElement root, string[] columns, string[] columnMapping)
		{
			DiffGram("", 0, root, columns, columnMapping);
		}

		/// <summary>
		/// Все строки, но только указанные колонки (ВАЖНО: объязательно передавайте колонки, которые входят в первичный ключ)
		/// </summary>
		private void DiffGram(string TableName, int TableIndex, XmlElement root, string[] columns, string[] columnMapping)
		{
			DataTable dtAdded, dtDeleted, dtModified;
			if (TableName != null && TableName != "")
			{
				dtAdded = Tables[TableName].GetChanges(DataRowState.Added);
				dtDeleted = Tables[TableName].GetChanges(DataRowState.Deleted);
				dtModified = Tables[TableName].GetChanges(DataRowState.Modified);
			}
			else
			{
				dtAdded = Tables[TableIndex].GetChanges(DataRowState.Added);
				dtDeleted = Tables[TableIndex].GetChanges(DataRowState.Deleted);
				dtModified = Tables[TableIndex].GetChanges(DataRowState.Modified);
			}

			//XmlElement prop = (XmlElement)root.AppendChild(root.OwnerDocument.CreateElement(_propName));
			if (dtAdded != null)
			{
				for (int i = 0; i < dtAdded.Rows.Count; i++)
				{
					DataRow dr = dtAdded.Rows[i];

					if (OnlyRowsWithoutErrors && dr.HasErrors)
						continue;

					XmlElement add = (XmlElement)root.AppendChild(root.OwnerDocument.CreateElement("add"));
					LoadXmlContent(add, dr, columns, columnMapping);

					if (OnAddedElement != null)//может понадобиться спец обработка, например, добавить несуществующие колонки
						OnAddedElement(add, new ProcessEventArgs(i));
				}
			}

			if (dtDeleted != null)
			{
				dtDeleted.RejectChanges();
				for (int i = 0; i < dtDeleted.Rows.Count; i++)
				{
					DataRow dr = dtDeleted.Rows[i];

					if (OnlyRowsWithoutErrors && dr.HasErrors)
						continue;

					XmlElement del = (XmlElement)root.AppendChild(root.OwnerDocument.CreateElement("delete"));
					LoadXmlContent(del, dr, columns, columnMapping);

					if (OnDeletedElement != null)
						OnDeletedElement(del, new ProcessEventArgs(i));
				}
			}

			if (dtModified != null)
			{
				for (int i = 0; i < dtModified.Rows.Count; i++)
				{
					DataRow dr = dtModified.Rows[i];

					if (OnlyRowsWithoutErrors && dr.HasErrors)
						continue;

					XmlElement mod = (XmlElement)root.AppendChild(root.OwnerDocument.CreateElement("modify"));
					LoadXmlContent(mod, dr, columns, columnMapping);

					if (OnModifiedElement != null)
						OnModifiedElement(mod, new ProcessEventArgs(i));
				}
			}
		}

		/// <summary>
		/// Только добавленные и удаленные строки
		/// </summary>		
		private void DiffGramAddDel(XmlElement root)
		{
			DataTable dtAdded = Tables[0].GetChanges(DataRowState.Added);
			DataTable dtDeleted = Tables[0].GetChanges(DataRowState.Deleted);

			//XmlElement prop = (XmlElement)root.AppendChild(root.OwnerDocument.CreateElement(_propName));
			if (dtAdded != null)
			{
				foreach (DataRow dr in dtAdded.Rows)
				{
					if (OnlyRowsWithoutErrors && dr.HasErrors)
						continue;

					XmlElement add = (XmlElement)root.AppendChild(root.OwnerDocument.CreateElement("add"));
					LoadXmlContent(add, dr, null, null);
				}
			}

			if (dtDeleted != null)
			{
				dtDeleted.RejectChanges();
				foreach (DataRow dr in dtDeleted.Rows)
				{
					if (OnlyRowsWithoutErrors && dr.HasErrors)
						continue;

					XmlElement del = (XmlElement)root.AppendChild(root.OwnerDocument.CreateElement("delete"));
					LoadXmlContent(del, dr, null, null);
				}
			}
		}

		private void DiffGramDel(XmlElement root, string[] columns, string[] columnMapping)
		{
			DataTable dtDeleted = Tables[0].GetChanges(DataRowState.Deleted);

			if (dtDeleted != null)
			{
				dtDeleted.RejectChanges();
				foreach (DataRow dr in dtDeleted.Rows)
				{
					if (OnlyRowsWithoutErrors && dr.HasErrors)
						continue;

					XmlElement del = (XmlElement)root.AppendChild(root.OwnerDocument.CreateElement("delete"));
					LoadXmlContent(del, dr, columns, columnMapping);
				}
			}
		}

		public delegate void ProcessAddedElement(XmlElement curEl, ProcessEventArgs Args);
		public event ProcessAddedElement OnAddedElement;
		public delegate void ProcessModifiedElement(XmlElement curEl, ProcessEventArgs Args);
		public event ProcessModifiedElement OnModifiedElement;
		public delegate void ProcessDeletedElement(XmlElement curEl, ProcessEventArgs Args);
		public event ProcessDeletedElement OnDeletedElement;

		/// <summary>
		/// Функция выгрузки строк
		/// </summary>
		private void LoadXmlContent(XmlElement elem, DataRow dr, string[] columns, string[] columnMapping)
		{
			DataTable curTable = dr.Table;

			if (columns == null)//отсылаем все колонки
			{
				int count = curTable.Columns.Count;
				for (int i = 0; i < count; i++)
				{
					//if(dr[i] != DBNull.Value)
					//{
					XmlNode node = elem.AppendChild(elem.OwnerDocument.CreateElement(curTable.Columns[i].ColumnName));
					if (dr[i] == DBNull.Value)
						node.InnerText = "\0";
					else if (dr[i].GetType() == typeof(DateTime))
						//node.InnerText = ((DateTime)dr[i]).ToString("dd.MM.yyyy HH:mm:ss'.'fff");
						node.InnerText = ((DateTime)dr[i]).ToString("dd MMMM yyyy HH:mm:ss.fff", System.Globalization.CultureInfo.InvariantCulture.DateTimeFormat); //DateTimeFormat.FullDateTimePattern);
					else if (dr[i].GetType() == typeof(decimal))
					{
						//NumberFormatInfo nfi = new CultureInfo("en-US").NumberFormat;
						//nfi.NumberDecimalSeparator = ".";
						node.InnerText = ((decimal)dr[i]).ToString(NumberFormat);
					}
					else node.InnerText = dr[i].ToString();
					//}
					//else continue;
				}
			}
			else//отсылаем только указанные колонки
			{
				for (int i = 0; i < columns.Length; i++)
				{
					string columnName = columns[i];
					string elementName = columnName;
					if (!curTable.Columns.Contains(columnName))
						throw new Exception("Таблица не содержит такой колонки: " + columnName);
					if (columnMapping != null && columnMapping.Length >= i) elementName = columnMapping[i];
					XmlNode node = elem.AppendChild(elem.OwnerDocument.CreateElement(elementName));
					if (dr[columnName] == DBNull.Value)
						node.InnerText = "\0";
					else if (dr[columnName].GetType() == typeof(DateTime))
						//node.InnerText = ((DateTime)dr[str]).ToString("dd.MM.yyyy HH:mm:ss'.'fff");
						node.InnerText = ((DateTime)dr[columnName]).ToString("dd MMMM yyyy HH:mm:ss.fff", System.Globalization.CultureInfo.InvariantCulture.DateTimeFormat); //DateTimeFormat.FullDateTimePattern);
					else if (dr[columnName].GetType() == typeof(decimal))
					{
						//NumberFormatInfo nfi = new CultureInfo("en-US").NumberFormat;
						//nfi.NumberDecimalSeparator = ".";
						node.InnerText = ((decimal)dr[columnName]).ToString(NumberFormat);
					}
					else node.InnerText = dr[columnName].ToString();
				}
			}
		}


		/// <summary>
		/// Метод для обычного сохранения
		/// </summary>
		/// <returns>XML с изменениями в виде строки</returns>
		public string GetChangesXml()
		{
			XmlDocument doc = new XmlDocument();
			XmlElement root = doc.CreateElement("root");

			DiffGram(root);
			doc.AppendChild(root);
			XmlDeclaration xmldecl = doc.CreateXmlDeclaration("1.0", "Windows-1251", null);
			doc.InsertBefore(xmldecl, root);

			return doc.OuterXml;

		}

		/// <summary>
		/// Метод для обычного сохранения, но только указанные колонки
		/// </summary>
		/// <returns>XML с изменениями в виде строки</returns>
		public string GetChangesXml(string[] columns)
		{
			return GetChangesXml(columns, null);
		}
		/// <summary>
		/// Метод для обычного сохранения, но только указанные колонки
		/// </summary>
		/// <returns>XML с изменениями в виде строки</returns>
		public string GetChangesXml(string[] columns, string[] columnMapping)
		{
			if (columns == null || columns.Length < 0)
				throw new Exception("Не передали массив с именами колонок!");

			XmlDocument doc = new XmlDocument();
			XmlElement root = doc.CreateElement("root");

			DiffGram(root, columns, columnMapping);
			doc.AppendChild(root);
			XmlDeclaration xmldecl = doc.CreateXmlDeclaration("1.0", "Windows-1251", null);
			doc.InsertBefore(xmldecl, root);

			return doc.OuterXml;
		}

		/// <summary>
		/// Метод для расширенного сохранения (сохранения мультиколлекций вместе с объектом)
		/// </summary>
		/// <param name="MulticollectionName">Имя коллекции - имя корневого листа</param>
		/// <returns>XML с изменениями в виде строки</returns>
		public string GetChangesXml(string MulticollectionName)
		{
			XmlDocument doc = new XmlDocument();
			XmlElement root = doc.CreateElement(MulticollectionName);
			DiffGram(root);
			doc.AppendChild(root);
			return doc.OuterXml;
		}
		/// <summary>
		/// Метод для расширенного сохранения (сохранения мультиколлекций вместе с объектом), но только указанные колонки
		/// </summary>
		/// <param name="MulticollectionName">Имя коллекции - имя корневого листа</param>
		/// <returns>XML с изменениями в виде строки</returns>
		public string GetChangesXml(string MulticollectionName, string[] columns)
		{
			return GetChangesXml(MulticollectionName, columns, null);
		}
		/// <summary>
		/// Метод для расширенного сохранения (сохранения мультиколлекций вместе с объектом), но только указанные колонки
		/// </summary>
		/// <param name="MulticollectionName">Имя коллекции - имя корневого листа</param>
		/// <returns>XML с изменениями в виде строки</returns>
		public string GetChangesXml(string MulticollectionName, string[] columns, string[] columnMapping)
		{
			if (columns == null || columns.Length < 0)
				throw new Exception("Не передали массив с именами колонок!");

			XmlDocument doc = new XmlDocument();
			XmlElement root = doc.CreateElement(MulticollectionName);
			DiffGram(root, columns, columnMapping);
			doc.AppendChild(root);
			return doc.OuterXml;
		}
		/// <summary>
		/// Метод для расширенного сохранения (сохранения мультиколлекций вместе с объектом), но только указанные колонки
		/// </summary>
		/// <param name="tableName">Имя таблицы</param>
		/// <param name="MulticollectionName">Имя коллекции - имя корневого листа</param>
		/// <returns>XML с изменениями в виде строки</returns>
		public string GetChangesXml(string TableName, string MulticollectionName)
		{

			XmlDocument doc = new XmlDocument();
			XmlElement root = doc.CreateElement(MulticollectionName);
			DiffGram(TableName, 0, root, null, null);
			doc.AppendChild(root);
			return doc.OuterXml;
		}
		/// <summary>
		/// Метод для расширенного сохранения (сохранения мультиколлекций вместе с объектом), но только указанные колонки
		/// </summary>
		/// <param name="tableName">Имя таблицы</param>
		/// <param name="MulticollectionName">Имя коллекции - имя корневого листа</param>
		/// <returns>XML с изменениями в виде строки</returns>
		public string GetChangesXml(string TableName, string MulticollectionName, string[] columns)
		{
			return GetChangesXml(TableName, MulticollectionName, columns, null);
		}
		/// <summary>
		/// Метод для расширенного сохранения (сохранения мультиколлекций вместе с объектом), но только указанные колонки
		/// в указанные элементы xml
		/// </summary>
		/// <param name="tableName">Имя таблицы</param>
		/// <param name="MulticollectionName">Имя коллекции - имя корневого листа</param>
		/// <returns>XML с изменениями в виде строки</returns>
		public string GetChangesXml(string TableName, string MulticollectionName, string[] columns, string[] columnMapping)
		{
			//			if (columns == null || columns.Length < 0)
			//				throw new Exception("Не передали массив с именами колонок!");

			XmlDocument doc = new XmlDocument();
			XmlElement root = doc.CreateElement(MulticollectionName);
			DiffGram(TableName, 0, root, columns, columnMapping);
			doc.AppendChild(root);
			return doc.OuterXml;
		}
		/// <summary>
		/// Метод для расширенного сохранения (сохранения мультиколлекций вместе с объектом) 
		/// Только добавленные и удаленные строки
		/// </summary>
		/// <param name="MulticollectionName">Имя коллекции - имя корневого листа</param>
		/// <returns>XML с изменениями в виде строки</returns>
		public string GetChangesAddDelXml(string MulticollectionName)
		{
			XmlDocument doc = new XmlDocument();
			XmlElement root = doc.CreateElement(MulticollectionName);
			DiffGramAddDel(root);
			doc.AppendChild(root);
			return doc.OuterXml;
		}
		/// <summary>
		/// Метод для расширенного сохранения (сохранения мультиколлекций вместе с объектом) 
		/// Только удаленные строки
		/// </summary>
		/// <param name="MulticollectionName">Имя коллекции - имя корневого листа</param>
		/// <returns>XML с изменениями в виде строки</returns>
		public string GetChangesDelXml(string MulticollectionName)
		{
			return GetChangesDelXml(MulticollectionName, null, null);
		}

		public string GetChangesDelXml(string MulticollectionName, string[] columns, string[] columnMapping)
		{
			XmlDocument doc = new XmlDocument();
			XmlElement root = doc.CreateElement(MulticollectionName);
			DiffGramDel(root, columns, columnMapping);
			doc.AppendChild(root);
			return doc.OuterXml;
		}

		//сохранение и загрузка схемы и данных
		public string SaveToString()
		{
			using (StringWriter sw = new StringWriter())
			{
				WriteXml(sw, XmlWriteMode.WriteSchema);
				return sw.ToString();
			}
		}

		public void LoadFromString(string xml)
		{
			LoadFromString(xml, true);
		}

		public void LoadFromString(string xml, bool acceptChanges)
		{
			string rp = @"(?<DATE>\d{4}-\d{2}-\d{2})(?<TIME>T\d{2}:\d{2}:\d{2}\+)(?<HOUR>\d{2})(?<LAST>:\d{2})";
			//Replace UTC offset value
			string fixedString = Regex.Replace(xml, rp,
				new MatchEvaluator(getHourOffset));
			StringReader sr = new StringReader(fixedString);
			this.ReadXml(sr, XmlReadMode.ReadSchema);
			if (acceptChanges)
				this.AcceptChanges();
		}

		private static string getHourOffset(Match m)
		{
			// Need to also account for Daylights Savings 

			// Time when calculating UTC offset value

			DateTime dtLocal = DateTime.Parse(m.Result("${DATE}"));
			DateTime dtUTC = dtLocal.ToUniversalTime();
			int hourLocalOffset = (dtLocal - dtUTC).Hours;
			string newHour = hourLocalOffset.ToString("00");
			string retString = m.Result("${DATE}" + "${TIME}" +
				newHour + "${LAST}");

			return retString;
		}
		//сохранение и загрузка только схемы
		public string SaveToStringSchema()
		{
			using (StringWriter sw = new StringWriter())
			{
				WriteXmlSchema(sw);
				return sw.ToString();
			}
		}

		public void LoadFromStringSchema(string xml)
		{
			using (StringReader sr = new StringReader(xml))
			{
				ReadXmlSchema(sr);
			}
			AcceptChanges();
		}

		/// <summary>
		/// Возвращает все текущие строки (все кроме удалённых)
		/// </summary>
		/// <param name="tableIndex">Индекс таблицы</param>
		/// <returns>Таблица со строками</returns>
		public DataTable GetCurrentRows(int tableIndex)
		{
			DataTable dt = Tables[tableIndex].Clone();

			foreach (DataRow dr in Tables[tableIndex].Rows)
			{
				if (dr.RowState != DataRowState.Deleted)//&& dr.RowState != DataRowState.Detached)
				{
					DataRow row = dt.NewRow();
					foreach (DataColumn dc in Tables[tableIndex].Columns)
						row[dc.ColumnName] = dr[dc.ColumnName];
					dt.Rows.Add(row);
				}
			}

			return dt;
		}

		/// <summary>
		/// Возвращает все текущие строки (все кроме удалённых)
		/// </summary>
		/// <param name="tableName">Имя таблицы</param>
		/// <returns>Таблица со строками</returns>
		public DataTable GetCurrentRows(string tableName)
		{
			DataTable dt = Tables[tableName].Clone();

			foreach (DataRow dr in Tables[tableName].Rows)
			{
				if (dr.RowState != DataRowState.Deleted)//&& dr.RowState != DataRowState.Detached)
				{
					DataRow row = dt.NewRow();
					foreach (DataColumn dc in Tables[tableName].Columns)
						row[dc.ColumnName] = dr[dc.ColumnName];
					dt.Rows.Add(row);
				}
			}

			return dt;
		}

		/// <summary>
		/// Возвращает все текущие строки (все кроме удалённых) таблицы по индексу 0
		/// </summary>
		/// <returns>Коллекцию строк</returns>
		public DataRowCollection GetCurrentRows()
		{
			DataTable dt = Tables[0].Clone();

			foreach (DataRow dr in Tables[0].Rows)
			{
				if (dr.RowState != DataRowState.Deleted)//&& dr.RowState != DataRowState.Detached)
				{
					DataRow row = dt.NewRow();
					foreach (DataColumn dc in Tables[0].Columns)
						row[dc.ColumnName] = dr[dc.ColumnName];
					dt.Rows.Add(row);
				}
			}

			return dt.Rows;
		}

		/// <summary>
		/// Удаляет все строки
		/// </summary>
		/// <param name="tableIndex">Индекс таблицы</param>
		public void DeleteCurrentRows(int tableIndex)
		{
			int x = Tables[tableIndex].Rows.Count;
			for (int i = x - 1; i >= 0; i--)
			{
				if (Tables[tableIndex].Rows[i].RowState != DataRowState.Deleted)//&& this.Tables[tableIndex].Rows[i].RowState != DataRowState.Detached)
					Tables[tableIndex].Rows[i].Delete();
			}
		}

		/// <summary>
		/// Удаляет все строки
		/// </summary>
		/// <param name="tableName">Имя таблицы</param>
		public void DeleteCurrentRows(string tableName)
		{
			int x = Tables[tableName].Rows.Count;
			for (int i = x - 1; i >= 0; i--)
			{
				if (Tables[tableName].Rows[i].RowState != DataRowState.Deleted)//&& this.Tables[tableName].Rows[i].RowState != DataRowState.Detached)
					Tables[tableName].Rows[i].Delete();
			}
		}

		/// <summary>
		/// Сбрасывает статус всех текущих строк (кроме уже удалённых) в DataRowState.Added
		/// </summary>
		/// <param name="tableName">Имя таблицы</param>
		public void SetAddedDataRowState(string tableName)
		{
			DataTable dt = Tables[tableName].Clone();
			foreach (DataRow dr in Tables[tableName].Rows)
			{
				if (dr.RowState != DataRowState.Deleted)//&& dr.RowState != DataRowState.Detached)
				{
					DataRow row = dt.NewRow();
					foreach (DataColumn dc in Tables[tableName].Columns)
						row[dc.ColumnName] = dr[dc.ColumnName];
					dt.Rows.Add(row);
				}
			}

			Tables.Remove(tableName);
			Tables.Add(dt);
		}

		/// <summary>
		/// Загружает строки в таблицу - этот метод используется, например, когда полученны строки мультиколлекции
		/// через GetObject вместе с объектом и другими мультиколлекциями
		/// </summary>
		/// <param name="table">Имя таблицы, в которую загружаются данные</param>
		/// <param name="doc">XML документ - фактически ответ GetObject</param>
		/// <param name="MultiColName">Имя мультиколлекции, которая будет загружаться в таблицу</param>
		public void LoadRowsFromXML(string TableName, XmlDocument Doc, string MultiColName, bool AcceptChange)
		{
			XmlNodeList nl = Doc.DocumentElement.SelectNodes(MultiColName);
			foreach (XmlNode n in nl)//для каждой записи в XML
			{
				if (!n.HasChildNodes)
					continue;
				DataRow dr = Tables[TableName].NewRow();
				foreach (DataColumn dc in Tables[TableName].Columns)//для каждой колонки в таблице
				{
					XmlNode nitem = n.SelectSingleNode(dc.ColumnName);
					if (nitem != null)
						dr[dc] = DataSetISM.GetValueFromString(dc.DataType, nitem.InnerText, NumberFormat, DateTimeFormat);
				}
				Tables[TableName].Rows.Add(dr);
			}
			if (AcceptChange)
				Tables[TableName].AcceptChanges();
		}

		public void LoadRowsFromXML(string TableName, XmlDocument Doc, string MultiColName)
		{
			LoadRowsFromXML(TableName, Doc, MultiColName, true);
		}
		public void LoadRowsFromXML(string TableName, string xml, string MultiColName, bool AcceptChange)
		{
			if (!string.IsNullOrEmpty(xml))
			{//если объект новый то сюда передасться пустая строка
				XmlDocument Doc = new XmlDocument();
				Doc.LoadXml(xml);
				LoadRowsFromXML(TableName, Doc, MultiColName, AcceptChange);
			}
		}
		public void LoadRowsFromXML(string TableName, string xml, string MultiColName)
		{
			LoadRowsFromXML(TableName, xml, MultiColName, true);
		}

		public static object GetValueFromString(Type DType, string DValue, NumberFormatInfo nfi, DateTimeFormatInfo dfi)
		{
			object result;

			if (DValue == "\0")//наш Null
				result = DBNull.Value;
			else
			{
				string type = DType.ToString();

				switch (type)
				{
					case "System.Guid":
					case "System.Object":
						result = new Guid(DValue);
						break;
					case "System.Int32":
						result = Convert.ToInt32(DValue);
						break;
					case "System.Int64":
						result = Convert.ToInt64(DValue);
						break;
					case "System.String":
						result = DValue;
						break;
					case "System.Decimal":
						result = Convert.ToDecimal(DValue, nfi);
						break;
					case "System.Double":
						result = Convert.ToDouble(DValue, nfi);
						break;
					case "System.Single":
						result = Convert.ToSingle(DValue, nfi);
						break;
					case "System.DateTime":
						result = Convert.ToDateTime(DValue, dfi);
						break;
					case "System.Boolean":
						result = (DValue == "0" ? false : true);
						break;
					default:
						throw new Exception("Wow, dude, undefined type here!");
					//break;
				}
			}

			return result;
		}

		public string SaveToExcel()
		{
			return WorkbookEngine.CreateWorkbook(this);
		}

		//		public void LoadTableXml(string xml, string table_name)
		//		{
		//			//Создадим структуру таблицы
		//			XmlDocument doc = new XmlDocument();
		//			doc.LoadXml(xml);
		//			XmlNode child = doc.DocumentElement.FirstChild;
		//			//XmlNodeList nl = doc.DocumentElement.FirstChild.SelectNodes(child.Name);
		//			DataTable dt = new DataTable();
		//
		//			dt.TableName = (table_name != "" || table_name != null) ? table_name : "table";
		//
		//			foreach(XmlAttribute attr in child.Attributes)
		//			{
		//
		//			}
		//						
		////			foreach(XmlNode n in nl) 
		////			{
		////				string fieldName = n.Attributes["name"].Value;
		////				fieldName = fieldName.Replace("?",".");
		////				dt.Columns.Add(fieldName, Type.GetType("System.String"));
		////			}
		////			dt.PrimaryKey = new DataColumn[] {dt.Columns["items.OID"]};
		////			dataSet2.Tables.Add(dt);
		////			dataGrid1.DataSource = dataSet2.Tables[0];
		//		}

	}
}
