using System;

namespace MetaData
{
	/// <summary>
	/// Summary description for GuidProperty.
	/// </summary>
	public class GuidProperty
	{
		private Guid _prop;
		private Guid _shadowProp;
		public Guid Prop 
		{
			get 
			{
				return _prop;
			}
			set 
			{
				_prop = value;
			}
		}
		public GuidProperty(Guid prop)
		{
			_prop = prop;
			_shadowProp = _prop;
		}
		public bool IsModified 
		{
			get 
			{
				return _prop != _shadowProp;
			}
		}
		public void AcceptChange() 
		{
			_shadowProp = _prop;
		}
	}
}
