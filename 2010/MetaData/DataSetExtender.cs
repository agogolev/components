using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Xml;
using System.Xml.Linq;
using System.Globalization;

namespace MetaData
{
	public static class DataSetExtender
	{
		private static readonly NumberFormatInfo numberFormat;
		private static readonly DateTimeFormatInfo dateTimeFormat;
		static DataSetExtender()
		{
			numberFormat = new CultureInfo("en-US").NumberFormat;
			numberFormat.NumberDecimalSeparator = ".";

			dateTimeFormat = new CultureInfo("ru-RU").DateTimeFormat;
			dateTimeFormat.FullDateTimePattern += ".fff";
			dateTimeFormat.ShortDatePattern = "dd/MM/yyyy";
		}
		public static void LoadRowsFromXml(this DataSet item, string tableName, string xml, string multiColName, bool acceptChandes = true)
		{
			if (xml != "")
			{//если объект новый то сюда передастся пустая строка
				var doc = XElement.Parse(xml);
				DoLoad(item, tableName, doc, multiColName, acceptChandes);
			}
		}
		public static void LoadRowsFromXml(this DataSet item, string tableName, XElement doc, string multiColName, bool acceptChandes = true)
		{
			DoLoad(item, tableName, doc, multiColName, acceptChandes);
		}

		//public static string GetChangesXml(this DataSet item, string[] columns = null, string[] columnMapping = null)
		//{
		//    var root = XElement.Parse("<root />");

		//    DiffGram(item, null, 0, root, columns, columnMapping);
		//    return root.ToString();
		//}


		//public static string GetChangesXml(this DataSet item, string MulticollectionName = "root", string[] columns = null, string[] columnMapping = null)
		//{
		//    var root = XElement.Parse(string.Format("<{0} />", MulticollectionName));

		//    DiffGram(item, null, 0, root, columns, columnMapping);
		//    return root.ToString();
		//}

		public static string GetChangesXml(this DataSet item, string tableName = null, string MulticollectionName = "root", string[] columns = null, string[] columnMapping = null)
		{
			var root = XElement.Parse(string.Format("<{0} />", MulticollectionName));

			DiffGram(item, tableName, 0, root, columns, columnMapping);
			return root.ToString();
		}

		private static void DiffGram(DataSet item, string tableName, int tableIndex, XContainer root, string[] columns, string[] columnMapping)
		{
			DataTable dtAdded, dtDeleted, dtModified;
			if (!string.IsNullOrEmpty(tableName))
			{
				dtAdded = item.Tables[tableName].GetChanges(DataRowState.Added);
				dtDeleted = item.Tables[tableName].GetChanges(DataRowState.Deleted);
				dtModified = item.Tables[tableName].GetChanges(DataRowState.Modified);
			}
			else
			{
				dtAdded = item.Tables[tableIndex].GetChanges(DataRowState.Added);
				dtDeleted = item.Tables[tableIndex].GetChanges(DataRowState.Deleted);
				dtModified = item.Tables[tableIndex].GetChanges(DataRowState.Modified);
			}

			//XmlElement prop = (XmlElement)root.AppendChild(root.OwnerDocument.CreateElement(_propName));
			if (dtAdded != null)
			{
				foreach (DataRow dr in dtAdded.Rows)
				{
					LoadXmlContent(root, "add", dr, columns, columnMapping);
				}
			}

			if (dtDeleted != null)
			{
				dtDeleted.RejectChanges();
				foreach (DataRow dr in dtDeleted.Rows)
				{
					LoadXmlContent(root, "delete", dr, columns, columnMapping);
				}
			}

			if (dtModified != null)
			{
				foreach (DataRow dr in dtModified.Rows)
				{
					LoadXmlContent(root, "modify", dr, columns, columnMapping);
				}
			}
		}
		private static string ToXmlString(this object rowAtCol)
		{
			if (rowAtCol == DBNull.Value)
				return "\0";
			if (rowAtCol is DateTime)
				return ((DateTime)rowAtCol).ToString("dd MMMM yyyy HH:mm:ss.fff", CultureInfo.InvariantCulture.DateTimeFormat);
			if (rowAtCol is decimal)
				return ((decimal)rowAtCol).ToString(numberFormat);
			return rowAtCol.ToString();
		}

		private static string GetColumnMapping(this string item, string[] columns, string[] columnMapping)
		{
			var i = columns.IndexOf(item);
			if (columnMapping == null || columnMapping.Length <= i) return item;
			return columnMapping[i];
		}

		private static int IndexOf(this string[] item, string column)
		{
			for (var i = 0; i < item.Length; i++)
			{
				if (string.Compare(item[i], column, true) == 0) return i;
			}
			return -1;
		}

		private static void LoadXmlContent(XContainer elem, string rootName, DataRow dr, string[] columns, string[] columnMapping)
		{
			DataTable curTable = dr.Table;

			if (columns == null)//отсылаем все колонки
			{
				elem.Add(new XElement(rootName, curTable.Columns.Cast<DataColumn>().Select(dc => new XElement(dc.ColumnName, dr[dc.ColumnName].ToXmlString()))));
			}
			else//отсылаем только указанные колонки
			{
				elem.Add(new XElement(rootName, curTable.Columns.Cast<DataColumn>().Where(dc => columns.Any(col => string.Compare(col, dc.ColumnName, true) == 0)).Select(dc =>  new XElement(dc.ColumnName.GetColumnMapping(columns, columnMapping), dr[dc.ColumnName].ToXmlString()))));
			}
		}
		private static void DoLoad(DataSet item, string tableName, XContainer doc, string multiColName, bool acceptChandes)
		{
			doc.Elements(multiColName)
				.ToList()
				.ForEach(e =>
				{
					if (e.Elements().Any())
					{
						var dr = item.Tables[tableName].NewRow();
						foreach (DataColumn dc in item.Tables[tableName].Columns)//для каждой колонки в таблице
						{
							var element = e.Elements(dc.ColumnName).SingleOrDefault();
							if (element != null)
								dr[dc] = GetValueFromString(dc.DataType, element.Value, numberFormat, dateTimeFormat);
						}
						item.Tables[tableName].Rows.Add(dr);
					}
				});
			if (acceptChandes)
				item.Tables[tableName].AcceptChanges();
		}
		private static object GetValueFromString(Type DType, string DValue, NumberFormatInfo nfi, DateTimeFormatInfo dfi)
		{
			if (nfi == null) throw new ArgumentNullException("nfi");
			object result;

			if (DValue == "\0")//наш Null
				result = DBNull.Value;
			else
			{
				string type = DType.ToString();

				switch (type)
				{
					case "System.Guid":
					case "System.Object":
						result = new Guid(DValue);
						break;
					case "System.Int32":
						result = Convert.ToInt32(DValue);
						break;
					case "System.Int64":
						result = Convert.ToInt64(DValue);
						break;
					case "System.String":
						result = DValue;
						break;
					case "System.Decimal":
						result = Convert.ToDecimal(DValue, nfi);
						break;
					case "System.Double":
						result = Convert.ToDouble(DValue, nfi);
						break;
					case "System.Single":
						result = Convert.ToSingle(DValue, nfi);
						break;
					case "System.DateTime":
						result = Convert.ToDateTime(DValue, dfi);
						break;
					case "System.Boolean":
						result = (DValue == "0" ? false : true);
						break;
					default:
						throw new Exception("Wow, dude, undefined type here!");
					//break;
				}
			}

			return result;
		}
		public static bool IsModified(this DataSet item, string tableName = null)
		{
			if (item.Tables.Count > 0)
			{
				if (string.IsNullOrEmpty(tableName))
				{
					return item.Tables.Cast<DataTable>().Select(tbl => tbl.GetChanges()).Any(diff => diff != null);
					//return (from DataTable dt in item.Tables select dt.GetChanges()).Any(diff => diff != null);
				}
				var table = item.Tables[tableName];
				if (table != null && table.GetChanges() != null) return true;
			}
			return false;
		}
	}
}
