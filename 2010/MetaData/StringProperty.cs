using System;

namespace MetaData
{
	/// <summary>
	/// Summary description for StringProperty.
	/// </summary>
	public class StringProperty
	{
		private string _prop;
		private string _shadowProp;
		public string Prop {
			get {
				return _prop;
			}
			set {
				_prop = value;
			}
		}
		public StringProperty(string prop)
		{
			_prop = prop;
			_shadowProp = _prop;
		}
		public bool IsModified {
			get {
				return _prop != _shadowProp;
			}
		}
		public void AcceptChange() {
			_shadowProp = _prop;
		}
	}
}
