using System;
using System.Data;
using System.Xml;
using System.Collections;
using System.Globalization;
using System.IO;
using System.Text;

namespace MetaData
{
	/// <summary>
	/// Summary description for MultiContainerExt.
	/// </summary>
	public class MultiContainer : DataTable
	{
		private string _propName;
		public string PropName
		{
			get
			{
				return _propName;
			}
			set
			{
				_propName = value;
			}
		}
		public MultiContainer()
		{
			TableName = "table";
		}

		public MultiContainer(string propName)
		{
			TableName = "table";
			PropName = propName;
		}

		public MultiContainer(DataTable table)
		{
			TableName = "table";
			MemoryStream stream = new MemoryStream();
			table.WriteXml(stream, XmlWriteMode.WriteSchema);
			stream.Position = 0;
			this.ReadXml(stream);
		}

		//создание структуры таблицы
		public void LoadSchemaFromString(string xmlSchema)
		{
			XmlDocument schema = new XmlDocument();
			string xmlSchemaTmp;

			//если сначала стоит <?xml version="1.0" encoding="utf-8" ?> (или что-то подобное) - то режем нафиг
			if (xmlSchema.Substring(0, 2) == "<?")
			{
				char[] array = xmlSchema.ToCharArray();
				int i = 0;
				for (; i < xmlSchema.Length; i++)
				{
					if (array[i] == '>')
						break;
				}
				xmlSchemaTmp = xmlSchema.Substring(i + 1);
			}
			else
			{
				xmlSchemaTmp = xmlSchema;
			}
			schema.LoadXml(xmlSchemaTmp);

			//загружаем имя коллекции
			foreach (XmlAttribute attr in schema.DocumentElement.Attributes)
			{
				if (attr.Name == "tableName")
				{
					this.TableName = attr.Value;
					break;
				}
			}

			//забиваем структуру таблицы по переданному xml
			XmlNodeList nodes = schema.DocumentElement.SelectNodes("fieldNames/field");
			ArrayList al = new ArrayList();
			foreach (XmlNode node in nodes)
			{

				DataColumn dc = new DataColumn();
				string columnName = "";
				string fieldName = "";
				bool pk = false;
				foreach (XmlAttribute attr in node.Attributes)
				{
					switch (attr.Name)
					{
						case "columnName":
							columnName = attr.Value;
							break;
						case "name":
							fieldName = attr.Value;
							break;
						case "type":
							dc.DataType = Type.GetType(attr.Value);
							break;
						case "PK":
							pk = Convert.ToBoolean(attr.Value);
							break;
						default:
							break;
					}
				}
				prepareName(ref fieldName);
				dc.ColumnName = (columnName == "" ? fieldName : columnName);
				if (pk)
					al.Add(dc);
				this.Columns.Add(dc);
			}

			DataColumn[] PK = (DataColumn[])al.ToArray(typeof(DataColumn));
			this.PrimaryKey = PK;
		}
		//Вспомогательные функции, служащие для получения правильных имён сложных колонок, если не задан ColumnName
		private void prepareName(ref string fieldName)
		{
			char[] a = fieldName.ToCharArray();
			int argStart = 0;
			int argEnd = 0;
			//сначала надо понять функция это или нет - ищем скобки, запятые
			for (int i = 0; i < fieldName.Length; i++)
			{
				if (a[i] == '(')
					argStart = i;	//записали позицию открывающей скобки
				else if (a[i] == ',' || a[i] == ')')
				{
					argEnd = i;		//записали позиции закрывающей скобки или запятой
					break;
				}
			}
			//если не функция надо заменить все . и ? на _ и всё
			if (argStart == 0 && argEnd == 0)
				clearString(ref fieldName);
			else
			{
				string func = fieldName.Substring(0, argStart);
				clearString(ref func);	//мало ли потом будут составные имена функций
				string arg = fieldName.Substring(argStart + 1, argEnd - argStart - 1);
				clearString(ref arg);
				fieldName = arg + '_' + func;
			}
		}

		private void clearString(ref string str)
		{
			str = str.Replace('.', '_');
			str = str.Replace('?', '_');
		}
		//Просто полезные функции
		public DataTable GetCurrentRows()
		{
			DataTable dt = this.Clone();

			foreach (DataRow dr in this.Rows)
			{
				if (dr.RowState != DataRowState.Deleted && dr.RowState != DataRowState.Detached)
				{
					DataRow row = dt.NewRow();
					foreach (DataColumn dc in this.Columns)
					{
						row[dc.ColumnName] = dr[dc.ColumnName];
					}
					dt.Rows.Add(row);
				}
			}

			return dt;
		}

		public void DeleteCurrentRows()
		{
			int x = this.Rows.Count;
			for (int i = x - 1; i >= 0; i--)
			{
				if (this.Rows[i].RowState != DataRowState.Deleted
					&& this.Rows[i].RowState != DataRowState.Detached)
					this.Rows[i].Delete();
			}
		}

		public bool IsModified
		{
			get
			{
				DataTable diff = this.GetChanges();
				return diff == null ? false : true;
			}
		}
		//Получение данных, которые были изменены
		public void DiffGram(XmlElement root)
		{
			DataTable dtAdded = this.GetChanges(DataRowState.Added);
			DataTable dtDeleted = this.GetChanges(DataRowState.Deleted);
			DataTable dtModified = this.GetChanges(DataRowState.Modified);

			XmlElement prop = (XmlElement)root.AppendChild(root.OwnerDocument.CreateElement(_propName));
			if (dtAdded != null)
			{
				foreach (DataRow dr in dtAdded.Rows)
				{
					XmlElement add = (XmlElement)prop.AppendChild(root.OwnerDocument.CreateElement("add"));
					LoadXmlContent(add, dr);
				}
			}
			if (dtDeleted != null)
			{
				dtDeleted.RejectChanges();
				foreach (DataRow dr in dtDeleted.Rows)
				{
					XmlElement del = (XmlElement)prop.AppendChild(root.OwnerDocument.CreateElement("delete"));
					LoadXmlContent(del, dr);
				}
			}
			if (dtModified != null)
			{
				foreach (DataRow dr in dtModified.Rows)
				{
					XmlElement mod = (XmlElement)prop.AppendChild(root.OwnerDocument.CreateElement("modify"));
					LoadXmlContent(mod, dr);
				}
			}
		}

		private void LoadXmlContent(XmlElement elem, DataRow dr)
		{
			int count = this.Columns.Count;
			for (int i = 0; i < count; i++)
			{
				//if(dr[i] != DBNull.Value)
				//{
				XmlNode node = elem.AppendChild(elem.OwnerDocument.CreateElement(this.Columns[i].ColumnName));
				if (dr[i] == DBNull.Value)
					node.InnerText = "\0";
				else if (dr[i].GetType() == typeof(DateTime))
					node.InnerText = ((DateTime)dr[i]).ToString("dd.MM.yyyy HH:mm:ss'.'fff");
				else if (dr[i].GetType() == typeof(decimal))
				{
					NumberFormatInfo nfi = new CultureInfo("en-US").NumberFormat;
					nfi.NumberDecimalSeparator = ".";
					node.InnerText = ((decimal)dr[i]).ToString(nfi);
				}
				else node.InnerText = dr[i].ToString();
				//}
				//else continue;
			}
		}

		public string GetChangesXml()
		{
			XmlDocument doc = new XmlDocument();
			XmlElement root = doc.CreateElement("root");

			DiffGram(root);

			doc.AppendChild(root);
			XmlDeclaration xmldecl = doc.CreateXmlDeclaration("1.0", "Windows-1251", null);
			doc.InsertBefore(xmldecl, root);
			return doc.OuterXml;

		}
		public string GetChangesXml(bool ignoreRoot)
		{
			XmlDocument doc = new XmlDocument();
			string xml = GetChangesXml();
			if (ignoreRoot)
			{
				doc.LoadXml(xml);
				return doc.DocumentElement.InnerXml;
			}
			else return xml;
		}
		//Загрузка данных
		public void LoadRows(string xml)
		{
			if (this.Columns.Count <= 0)
				throw new Exception("There are no Columns in MultiContainerExt! So, i cant load rows! ... stupid human ...");
			if (this.TableName == null || this.TableName == "")
				throw new Exception("And what is supposed to be loaded?! Where is TableName?! ... stupid human ...");

			XmlDocument doc = new XmlDocument();
			doc.LoadXml(xml);

			XmlNodeList nodes = doc.DocumentElement.SelectNodes(this.TableName);

			foreach (XmlNode node in nodes)
			{
				DataRow dr = this.NewRow();
				foreach (DataColumn dc in this.Columns)
				{
					XmlNode child = node.SelectSingleNode(dc.ColumnName);
					if (child != null)
						dr[dc.ColumnName] = child.InnerText;
					else dr[dc.ColumnName] = DBNull.Value;
				}
				this.Rows.Add(dr);
			}
			this.AcceptChanges();
		}

	}
}
