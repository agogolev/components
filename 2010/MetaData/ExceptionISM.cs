using System;
using System.Xml.Serialization;

namespace MetaData
{
	/// <summary>
	/// Summary description for ExceptionISM.
	/// </summary>
	
	public class ExceptionISM //контейнер
	{
		public ExceptionISM() {
			this.Message = "";
			this.StackTrace = "";
			this.ExceptionType = "";
		}
		public ExceptionISM(string message, string stackTrace, string exceptionType) 
		{
			this.Message = message;
			this.StackTrace = stackTrace;
			this.ExceptionType = exceptionType;
		}
		public ExceptionISM(string message, string exceptionType) 
		{
			this.Message = message;
			this.ExceptionType = exceptionType;
		}
		public ExceptionISM(Exception e) {
			this.Message = e.Message;
			this.StackTrace = e.StackTrace;
			this.ExceptionType = e.GetType().ToString();
		}
		// Both types of attributes can be applied. Depending on which type
		// the method used, either one will affect the call.
		[SoapElement(ElementName = "EncodedMessage")]
		[XmlElement(ElementName = "LiteralMessage")]
		public string Message;

		[SoapElement(ElementName = "EncodedStackTrace")]
		[XmlElement(ElementName = "LiteralStackTrace")]
		public string StackTrace;

		[SoapElement(ElementName = "EncodedExceptionType")]
		[XmlElement(ElementName = "LiteralExceptionType")]
		public string ExceptionType;
	}

	public class SystemOIDException : Exception
	{
		public SystemOIDException() : base("OID is using by System! You dont have permissions to destroy it!")
		{
		}

//		public static string GetType()
//		{
//			return "SystemOIDException";
//		}
	}

	public class BusinessException : Exception
	{
		public BusinessException() : base("Business procedure not found!")
		{
		}

//		public static string GetType()
//		{
//			return "BusinessException";
//		}
	}
	public class LoginException : Exception
	{
		public LoginException() : base("Неверное сочетание логина\\пароля")
		{
		}

//		public static string GetType()
//		{
//			return "LoginException";
//		}
	}
	public class CCVException : Exception
	{
		public CCVException() : base("Несовпадение версий")
		{
		}

//		public static string GetType()
//		{
//			return "CCVException";
//		}
	}
}
