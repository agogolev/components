using System;
using System.IO;
using System.Runtime.InteropServices;


namespace MetaData
{
	/// <summary>
	/// Summary description for MimeTypeUtil.
	/// </summary>
	public class MimeTypeUtil
	{
	    private const int bufferSize = 560;
		[DllImport(@"urlmon.dll", CharSet = CharSet.Auto)]
		private extern static UInt32 FindMimeFromData(
            IntPtr pBC,
            [MarshalAs(UnmanagedType.LPWStr)] String pwzUrl,
            [MarshalAs(UnmanagedType.LPArray, ArraySubType = UnmanagedType.I1, SizeParamIndex = 3)] byte[] pBuffer,
            int cbSize,
			[MarshalAs(UnmanagedType.LPWStr)] String pwzMimeProposed,
            int dwMimeFlags,
            /*[MarshalAs(UnmanagedType.U4)]*/ out IntPtr ppwzMimeOut,
            int dwReserverd
		);

		public static string CheckType(string filename)
		{
			if (!File.Exists(filename))
			{
				return GetMimeFromRegistry(filename);
			}

            var buffer = new byte[bufferSize];

			using (var fs = new FileStream(filename, FileMode.Open))
			{
                if (fs.Length >= bufferSize)
                    fs.Read(buffer, 0, bufferSize);
				else
					fs.Read(buffer, 0, (int)fs.Length);
			}

			try
			{
				var mime = CheckType(buffer);
				if (string.IsNullOrWhiteSpace(mime) ||
					mime == "text/plain" || mime == "application/octet-stream")
				{
					return GetMimeFromRegistry(filename);
				}
				return mime;
			}
			catch (Exception)
			{
				return GetMimeFromRegistry(filename);
			}
		}

		public static string CheckType(Stream stream)
		{
            var buffer = new byte[bufferSize];
			try
			{
				stream.Position = 0;
                if (stream.Length >= bufferSize)
                    stream.Read(buffer, 0, bufferSize);
				else
					stream.Read(buffer, 0, (int)stream.Length);
			}
			finally
			{
				stream.Position = 0;
			}
			
			return CheckType(buffer);
		}

		public static string CheckType(byte[] buffer)
		{
			try
			{
				var mimeType = CheckFlash(buffer);
				if (mimeType != null) return mimeType;
                IntPtr outPtr;
				int mimeFlag = 0x20;
                FindMimeFromData(IntPtr.Zero, null, buffer, bufferSize, null, mimeFlag, out outPtr, 0);
				//var mimeTypePtr = new IntPtr(outPtr);
                var mime = Marshal.PtrToStringUni(outPtr);
                Marshal.FreeCoTaskMem(outPtr);
                if (mime != null && 
                    (mime.IndexOf("xml", StringComparison.InvariantCultureIgnoreCase) != -1 ||
                    mime.IndexOf("octet", StringComparison.InvariantCultureIgnoreCase) != -1 ||
                    mime.IndexOf("plain", StringComparison.InvariantCultureIgnoreCase) != -1))
                {
                    // новый алгоритм в классе detective
                    var fileType = buffer.GetFileType();
                    return fileType.Mime;
                }
				return mime;
			}

			catch (Exception)
			{
				return null;
			}
		}

		public static string CheckFlash(byte[] buffer)
		{
			if ((buffer[0] == 'F' || buffer[0] == 'C') && (buffer[1] == 'W') && (buffer[2] == 'S'))
				return "application/x-shockwave-flash";
			else return null;
		}

		private static string GetMimeFromRegistry(string Filename)
		{
			var mime = "application/octetstream";
			var ext = Path.GetExtension(Filename).ToLower();
			var rk = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(ext);
			if (rk != null && rk.GetValue("Content Type") != null)
				mime = rk.GetValue("Content Type").ToString();
			return mime;
		}
	}
}
