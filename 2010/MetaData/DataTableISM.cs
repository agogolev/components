using System;
using System.Data;
using System.Xml;
using System.Globalization;

namespace MetaData
{
	/// <summary>
	/// Summary description for DataTableISM.
	/// </summary>
	
//	[Serializable]
	public class DataTableISM : DataTable//, ISerializable
	{
		public DataTableISM()
		{
			
		}
		public DataTableISM(string tableName)
			: base(tableName)
		{
			
		}
		public DataTableISM(string tableName, string tableNamespace)
			: base(tableName, tableNamespace)
		{
			
		}
		protected DataTableISM(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
			: base(info, context)
		{
			
		}

		#region FOR SERIALIZATION
//		public DataTableISM(SerializationInfo info, StreamingContext context)
//		{
//			this.Clear();
//
//			string[] columnNames = (string[]) info.GetValue("ColumnNames", typeof(string[]));
//			string[] columnTypes = (string[]) info.GetValue("ColumnTypes", typeof(string[]));
//			//Type[] columnTypes = (Type[]) info.GetValue("ColumnTypes", typeof(Type[]));
//			object[,] data = (object[,]) info.GetValue("Data", typeof(object[,]));
//
//			for(int c = 0; c < columnNames.Length; c++)
//			{
//				this.Columns.Add(columnNames[c], Type.GetType(columnTypes[c]));
//				//this.Columns.Add(columnNames[c], columnTypes[c]);
//			}
//
//			for(int r = 0; r < data.GetLength(0); r++)
//			{
//				DataRow row = this.NewRow();
//				for(int c = 0; c < data.GetLength(1); c++)
//					row[c] = data[r, c];
//				this.Rows.Add(row);
//			}
//		}
//
//		public void GetObjectData(SerializationInfo info, StreamingContext context)
//		{
//			string[] columnNames = new string[this.Columns.Count];
//			//Type[] columnTypes = new Type[this.Columns.Count];
//			string[] columnTypes = new string[this.Columns.Count];
//			object [,] data = new object[this.Rows.Count, this.Columns.Count];
//
//			for(int c = 0; c < this.Columns.Count; c++)
//			{
//				columnNames[c] = this.Columns[c].ColumnName;
//				columnTypes[c] = this.Columns[c].DataType.ToString();
//			}
//        
//			//≈сли не добавить эту строку то, при десериализации в columnTypes будет массив с null значени¤ми, если добваить - как положено.
//			//info.AddValue("Data1", this.Columns[2].DataType); 
//        
//			for(int r = 0; r < this.Rows.Count; r++)
//				for(int c = 0; c < this.Columns.Count; c++)
//					data[r, c] = this.Rows[r][c];
//
//			info.AddValue("ColumnNames", columnNames);
//			info.AddValue("ColumnTypes", columnTypes);
//			info.AddValue("Data", data);
//		}
	#endregion

		public void DiffGram(XmlElement root)
		{
			DataTable dtAdded = GetChanges(DataRowState.Added);
			DataTable dtDeleted = GetChanges(DataRowState.Deleted);
			DataTable dtModified = GetChanges(DataRowState.Modified);

			//XmlElement prop = (XmlElement)root.AppendChild(root.OwnerDocument.CreateElement(_propName));
			if(dtAdded != null) 
			{
				foreach(DataRow dr in dtAdded.Rows) 
				{
					XmlElement add = (XmlElement)root.AppendChild(root.OwnerDocument.CreateElement("add"));
					LoadXmlContent(add, dr);
				}
			}
			if(dtDeleted != null) 
			{
				dtDeleted.RejectChanges();
				foreach(DataRow dr in dtDeleted.Rows) 
				{
					XmlElement del = (XmlElement)root.AppendChild(root.OwnerDocument.CreateElement("delete"));
					LoadXmlContent(del, dr);
				}
			}
			if(dtModified != null) 
			{
				foreach(DataRow dr in dtModified.Rows) 
				{
					XmlElement mod = (XmlElement)root.AppendChild(root.OwnerDocument.CreateElement("modify"));
					LoadXmlContent(mod, dr);
				}
			}
		}

		private void LoadXmlContent(XmlElement elem, DataRow dr) 
		{
			int count = Columns.Count;
			for(int i=0; i < count; i++)
			{
				XmlNode node = elem.AppendChild(elem.OwnerDocument.CreateElement(Columns[i].ColumnName));
				if(dr[i].GetType() == typeof(DateTime))
					//node.InnerText = ((DateTime)dr[i]).ToString("dd.MM.yyyy HH:mm:ss'.'fff");
					node.InnerText = ((DateTime)dr[i]).ToString("dd MMMM yyyy HH:mm:ss.fff", System.Globalization.CultureInfo.InvariantCulture.DateTimeFormat); //DateTimeFormat.FullDateTimePattern);
				else if (dr[i].GetType() == typeof(decimal)) 
				{
					NumberFormatInfo nfi = new CultureInfo("en-US").NumberFormat;
					nfi.NumberDecimalSeparator = ".";
					node.InnerText = ((decimal)dr[i]).ToString(nfi);
				}
				else node.InnerText = dr[i].ToString();

			}
		}

		public string GetXml() 
		{
			XmlDocument doc = new XmlDocument();
			XmlElement root = doc.CreateElement("root");

			DiffGram(root);

			XmlDeclaration xmldecl = doc.CreateXmlDeclaration("1.0", "Windows-1251", null);
			doc.InsertBefore(xmldecl, root);

			return doc.OuterXml;
		}	

		public void LoadTable(DataTable dt)
		{
			Clear();
			foreach(DataColumn dc in dt.Columns)
				Columns.Add(dc);
			foreach(DataRow dr in dt.Rows)
				Rows.Add(dr);
		}
	}
}
