﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetaData
{
	public class LambdaSorter<T> : IComparer<T>
	{
		private readonly Func<T, T, int> _lambdaComparer;

		public LambdaSorter(Func<T, T, int> lambdaComparer)
		{
			if (lambdaComparer == null)
				throw new ArgumentNullException("lambdaComparer");

			_lambdaComparer = lambdaComparer;
		}


		public int Compare(T x, T y)
		{
			return _lambdaComparer(x, y);
		}

	}

	public static class LambdaSorter
	{
		public static LambdaSorter<T> CreateSorter<T>(Func<T, T, int> lambdaComparer)
		{
			return new LambdaSorter<T>(lambdaComparer);
		}
		public static LambdaSorter<T> CreateSorterForElements<T>(this IEnumerable<T> items, Func<T, T, int> lambdaComparer)
		{
			return new LambdaSorter<T>(lambdaComparer);
		}
	}
}
