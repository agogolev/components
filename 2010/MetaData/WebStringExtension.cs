﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace MetaData
{
	public static class WebStringExtension // нужен из-за особенностей сериализации значений в web сервисах когда съедается CR в комбинации CRLF
	{
		static readonly Regex re = new Regex("\n(?<!\r)");
		static readonly Regex reXml = new Regex("&#x0D;\n", RegexOptions.Singleline | RegexOptions.IgnoreCase);
		public static string RestoreCr(this string item)
		{
			return re.Replace(item, "\r\n");
		}
		public static string PrepareXml(this string item)
		{
			//item = item.Replace("&#x0D;\n", );
			return reXml.Replace(item, "\r\n");
		}
		public static object StringToObject(this string item)
		{
			if (item == null || string.Compare(item.Trim(), "null", true) == 0) return DBNull.Value;
			else return item;
		}
	}
}
