using System;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.IO;

namespace MetaData
{
	public class Sizer
	{
		public static Size GetSize(string fileName, string mimeType)
		{
			string mime = mimeType.ToLower();
			var size = new Size(0, 0);
			try {
				if(mime.IndexOf("image", StringComparison.Ordinal) != -1) {
					if(mime.IndexOf("gif", StringComparison.Ordinal) != -1) {
						var gif = new GIFSizer();
						var fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
						var buffer = new byte[10];
						fs.Read(buffer, 0, 10);
						fs.Close();
						size = gif.ImageSize(buffer);
					}
					else if(mime.IndexOf("png", StringComparison.Ordinal) != -1) {
						var png = new PNGSizer();
						var fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
						var buffer = new byte[24];
						fs.Read(buffer, 0, 24);
						fs.Close();
						size = png.ImageSize(buffer);
					}
					else if(mime.IndexOf("jpeg", StringComparison.Ordinal) != -1 || mime.IndexOf("jpg", StringComparison.Ordinal) != -1) {
						var jpg = new JPEGSizer();
						var fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
						var buffer = new byte[fs.Length];
						fs.Read(buffer, 0, (int)fs.Length);
						fs.Close();
						size = jpg.ImageSize(buffer);
					}
				}
			}
			catch {
				MessageBox.Show("ќшибка загрузки размеров");
			}
			return size;
		}

		public static Size GetSize(byte[] buffer, string mimeType)
		{
			var mime = mimeType.ToLower();
			var size = new Size(0, 0);
			if(mime.IndexOf("image", StringComparison.Ordinal) != -1) 
			{
				if(mime.IndexOf("gif", StringComparison.Ordinal) != -1) 
				{
					var gif = new GIFSizer();
					size = gif.ImageSize(buffer);
				}
				else if(mime.IndexOf("png", StringComparison.Ordinal) != -1) 
				{
					var png = new PNGSizer();
					size = png.ImageSize(buffer);
				}
				else if(mime.IndexOf("jpeg", StringComparison.Ordinal) != -1 || mime.IndexOf("jpg", StringComparison.Ordinal) != -1) 
				{
					var jpg = new JPEGSizer();
					size = jpg.ImageSize(buffer);
				}
			}
			return size;
		}
		public static Size GetSize(Stream stream, string mimeType)
		{
			var mime = mimeType.ToLower();
			var size = new Size(0, 0);
			if (mime.IndexOf("image", StringComparison.Ordinal) != -1)
			{
				if (mime.IndexOf("gif", StringComparison.Ordinal) != -1)
				{
					var gif = new GIFSizer();
					size = gif.ImageSize(stream);
				}
				else if (mime.IndexOf("png", StringComparison.Ordinal) != -1)
				{
					var png = new PNGSizer();
					size = png.ImageSize(stream);
				}
				else if (mime.IndexOf("jpeg", StringComparison.Ordinal) != -1 || mime.IndexOf("jpg", StringComparison.Ordinal) != -1)
				{
					var jpg = new JPEGSizer();
					var bytes = new byte[stream.Length];
					stream.Read(bytes, 0, (int)stream.Length);
					//size = jpg.ImageSize(stream);
					size = jpg.ImageSize(bytes);
					stream.Position = 0;
				}
			}
			return size;
		}
	}
	internal abstract class ImageSizer
	{
		public abstract Size ImageSize(byte[] data);
		public Size ImageSize(Stream stream)
		{
			var buffer = new byte[256];
			stream.Read(buffer, 0, buffer.Length);
			stream.Position = 0;
			return ImageSize(buffer);
		}
	}
	internal class GIFSizer : ImageSizer
	{
		public override Size ImageSize(byte[] data)
		{
			var width = data[7] << 8;
			width += data[6];
			var height = data[9] << 8;
			height += data[8];
			return new Size(width, height);
		}
	}
	internal class PNGSizer : ImageSizer
	{
		public override Size ImageSize(byte[] data)
		{
			var width = data[16] << 24;
			width += data[17] << 16;
			width += data[18] << 8;
			width += data[19];
			var height = data[20] << 24;
			height += data[21] << 16;
			height += data[22] << 8;
			height += data[23];
			return new Size(width, height);
		}
	}
	internal class JPEGSizer : ImageSizer
	{
		private MemoryStream _mem;
		private int _currentTag;
		private int _tagSize;
        public override Size ImageSize(byte[] data)
        {
            _mem = new MemoryStream(data);
            if (!StartTag()) throw new Exception("missing start JPEG tag");
            do
            {
                NextTag();
                if ((_currentTag >= 0xC0 && _currentTag < 0xC4) || // SOFn markers
                    (_currentTag >= 0xC5 && _currentTag < 0xC8) ||
                    (_currentTag >= 0xC9 && _currentTag < 0xCC) ||
                    (_currentTag >= 0xCD && _currentTag <= 0xCF))
                {
                    _mem.Position -= _tagSize - 3;
                    int height = _mem.ReadByte();
                    height = height << 8;
                    height += _mem.ReadByte();
                    int width = _mem.ReadByte();
                    width = width << 8;
                    width += _mem.ReadByte();
                    return new Size(width, height);
                }
            } while (_currentTag != -1);
            while (NextTag() != -1) ;
            return new Size(0, 0);
        }

        //public override Size ImageSize(byte[] data)
        //{
        //    _mem = new MemoryStream(data);
        //    return GetJpegDimensions(_mem);
        //}

        //public static Size GetJpegDimensions(Stream fs)
        //{
        //    if (!fs.CanSeek) throw new ArgumentException("Stream must be seekable");
        //    var buf = new byte[4];
        //    fs.Read(buf, 0, 4);
        //    if (buf.SequenceEqual(new byte[] { 0xff, 0xd8, 0xff, 0xe0 }))
        //    {
        //        var blockStart = fs.Position;
        //        fs.Read(buf, 0, 2);
        //        var blockLength = ((buf[0] << 8) + buf[1]);
        //        fs.Read(buf, 0, 4);
        //        if (Encoding.ASCII.GetString(buf, 0, 4) == "JFIF"
        //            && fs.ReadByte() == 0)
        //        {
        //            blockStart += blockLength;
        //            while (blockStart < fs.Length)
        //            {
        //                fs.Position = blockStart;
        //                fs.Read(buf, 0, 4);
        //                blockLength = ((buf[2] << 8) + buf[3]);
        //                if (blockLength >= 7 && buf[0] == 0xff && buf[1] == 0xc0)
        //                {
        //                    fs.Position += 1;
        //                    fs.Read(buf, 0, 4);
        //                    var height = (buf[0] << 8) + buf[1];
        //                    var width = (buf[2] << 8) + buf[3];
        //                    return new Size(width, height);
        //                }
        //                blockStart += blockLength + 2;
        //            }
        //        }
        //    }
        //    return new Size(0, 0);
        //}

		private bool StartTag() {
			if(_mem.ReadByte() == 0xFF && _mem.ReadByte() == 0xD8) return true;
			else return false;
		}
		private int NextTag() {
			if(_mem.ReadByte() != 0xFF) return -1;
			_currentTag = _mem.ReadByte();
			int temp = _mem.ReadByte();
			temp = temp << 8;
			temp += _mem.ReadByte();
			_tagSize = temp;
			_mem.Position += _tagSize-2;
			return _currentTag;
		}
	}
}
