using System;

namespace MetaData
{
	/// <summary>
	/// Summary description for ObjectItem.
	/// </summary>
	public sealed class ObjectItem : IComparable
	{
		private Guid _OID;
		private string _className;
		private string _nk;
		public ObjectItem(Guid oid, string nk, string className)
		{
			_OID = oid;
			_nk = nk;
			_className = className;
		}

		public static ObjectItem Empty
		{
			get { return new ObjectItem(Guid.Empty, "", ""); }
		}

		public Guid OID
		{
			get { return _OID; }
		}

		public string NK
		{
			get { return _nk; }
			set { _nk = value; }
		}

		public string ClassName
		{
			get { return _className; }
			set { _className = value; }
		}

		public override bool Equals(object obj)
		{
			if (!(obj is ObjectItem)) return false;
			return _OID.Equals(((ObjectItem)obj).OID);
		}

		public override int GetHashCode()
		{
			return _OID.GetHashCode();
		}

		public override string ToString()
		{
			return _nk;
		}

		public int CompareTo(object obj)
		{
			if (!(obj is ObjectItem)) throw new Exception("Uncomparable types");
			return _OID.CompareTo(((ObjectItem)obj).OID);
		}

		public static bool operator ==(ObjectItem obj1, ObjectItem obj2)
		{
			if ((((object)obj1) == null && ((object)obj2) != null)
				|| (((object)obj1) != null && ((object)obj2) == null))
				return false;
			else if (((object)obj1) == null && ((object)obj2) == null)
				return true;
			else return obj1.OID == obj2.OID;
		}

		public static bool operator !=(ObjectItem obj1, ObjectItem obj2)
		{
			return !(obj1 == obj2);
		}
	}

	public class ObjectItemExt
	{
		public ObjectItemExt(ObjectItem ObjectItem)
		{
			_objectItem = objectItem = ObjectItem;
		}

		public ObjectItemExt(Guid OID, string NK, string ClassName)
		{
			objectItem = new ObjectItem(OID, NK, ClassName);
			_objectItem = objectItem;
		}

		private ObjectItem _objectItem;
		private ObjectItem objectItem;
		public ObjectItem ObjectItem
		{
			get { return objectItem; }
			set { objectItem = value; }
		}

		public bool IsModified
		{
			get
			{
				if (_objectItem != objectItem)
					return true;
				else return false;
			}
		}
		public void AcceptChange()
		{
			_objectItem = objectItem;
		}

		public Guid OID
		{
			get
			{
				if (objectItem != null)
					return objectItem.OID;
				else return Guid.Empty;
			}
		}
		public string NK
		{
			get
			{
				if (objectItem != null)
					return objectItem.NK;
				else return "";
			}
		}
		public string ClassName
		{
			get
			{
				if (objectItem != null)
					return objectItem.ClassName;
				else return "";
			}
		}
		public void Refresh()
		{
			_objectItem = objectItem;
		}
	}
}
