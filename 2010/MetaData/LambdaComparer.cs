﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetaData
{
	public class LambdaComparer<T> : IEqualityComparer<T>
	{
		private readonly Func<T, T, bool> _lambdaComparer;
		private readonly Func<T, int> _lambdaHash;

		public LambdaComparer(Func<T, T, bool> lambdaComparer) :
			this(lambdaComparer, o => 0)
		{
		}

		public LambdaComparer(Func<T, T, bool> lambdaComparer, Func<T, int> lambdaHash)
		{
			if (lambdaComparer == null)
				throw new ArgumentNullException("lambdaComparer");
			if (lambdaHash == null)
				throw new ArgumentNullException("lambdaHash");

			_lambdaComparer = lambdaComparer;
			_lambdaHash = lambdaHash;
		}

		public bool Equals(T x, T y)
		{
			return _lambdaComparer(x, y);
		}

		public int GetHashCode(T obj)
		{
			return _lambdaHash(obj);
		}
	}

	public static class LambdaComparer
	{
		public static LambdaComparer<T> CreateComparer<T>(Func<T, T, bool> lambdaComparer)
		{
			return new LambdaComparer<T>(lambdaComparer);
		}
		public static LambdaComparer<T> CreateComparer<T>(Func<T, T, bool> lambdaComparer, Func<T, int> lambdaHash)
		{
			return new LambdaComparer<T>(lambdaComparer, lambdaHash);
		}
		public static LambdaComparer<T> CreateComparerForElements<T>(this IEnumerable<T> items, Func<T, T, bool> lambdaComparer)
		{
			return new LambdaComparer<T>(lambdaComparer);
		}
		public static LambdaComparer<T> CreateComparerForElements<T>(this IEnumerable<T> items, Func<T, T, bool> lambdaComparer, Func<T, int> lambdaHash)
		{
			return new LambdaComparer<T>(lambdaComparer, lambdaHash);
		}
	}
}
