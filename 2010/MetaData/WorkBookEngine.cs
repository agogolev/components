using System.Data;
using System.IO;
using System.Reflection;
using System.Xml;
using System.Xml.Xsl;

namespace ExcelUtil
{
	public static class WorkbookEngine
	{
	// you could have other overloads if you want to get creative...
		public static string CreateWorkbook(this DataSet ds)
		{
			XmlDocument doc = new XmlDocument();
			doc.LoadXml(ds.GetXml());
			//XmlDataDocument xmlDataDoc = new XmlDataDocument(ds);
			var xt = new XslCompiledTransform();
			StreamReader reader =
				new StreamReader(Assembly.GetExecutingAssembly().GetManifestResourceStream("MetaData.Excel.xsl"), System.Text.Encoding.UTF8);
			XmlTextReader xRdr = new XmlTextReader(reader);
			xt.Load(xRdr, null, null);
			StringWriter sw = new StringWriter();
			XmlTextWriter wrt = new XmlTextWriter(sw);
			xt.Transform(doc, null, wrt, null);
			return sw.ToString();
		}
	}
}