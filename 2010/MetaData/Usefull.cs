using System;
using System.Drawing;
using System.Windows.Forms;

namespace MetaData
{
	/// <summary>
	/// Summary description for Usefull.
	/// </summary>
	public static class Usefull
	{
		/// <summary>
		/// Дизаблит все кнотролы, принадлежащие переданному контролу (форме)
		/// </summary>
		public static void DisableControls(this Control cntrl)
		{
			if (cntrl.Controls.Count == 0 && !(cntrl is ScrollBar))
				if (cntrl is TextBoxBase)
				{
					if (cntrl.BackColor == SystemColors.Window) cntrl.BackColor = Color.White;
					(cntrl as TextBoxBase).ReadOnly = true;
				}
					
				else cntrl.Enabled = false;
			else
			{
				foreach (Control c in cntrl.Controls)
					DisableControls(c);
			}
		}
	}
}
