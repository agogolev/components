using System;
using System.Linq;
using System.Text;
using System.Xml;
using System.Globalization;

namespace MetaData
{
	public abstract class FilterInfo
	{

		public DateTimeFormatInfo DateTimeFormat
		{
			get
			{
				var dtfi = new CultureInfo("ru-RU").DateTimeFormat;
				dtfi.FullDateTimePattern += ".fff";
				dtfi.ShortDatePattern = "dd/MM/yyyy";
				return dtfi;
			}
		}
		abstract protected void createValueNodes(XmlNode filterNode);
		public string TableName;
		public string TableAlias;
		public string ColumnName;
		public string FilterName;
		public FilterVerb Verb;
		public bool Negation;
		public virtual string Expr(string fullFieldName)
		{
			var result = new StringBuilder();
			result.Append((Negation ? "NOT (" : " ") + fullFieldName);
			switch (Verb)
			{
				case FilterVerb.Equal:
					result.Append(" = ");
					break;
				case FilterVerb.Like:
					result.Append(" LIKE ");
					break;
				case FilterVerb.Between:
					result.Append(" BETWEEN ");
					break;
				case FilterVerb.Greater:
					result.Append(" > ");
					break;
				case FilterVerb.GreaterOrEqual:
					result.Append(" >= ");
					break;
				case FilterVerb.Less:
					result.Append(" < ");
					break;
				case FilterVerb.LessOrEqual:
					result.Append(" <= ");
					break;
				case FilterVerb.In:
					result.Append(" in (");
					break;
				case FilterVerb.IsNull:
					result.Append(" IS NULL ");
					break;

			}
			return result.ToString();
		}
		public override string ToString()
		{
			if (TableName == null) return "";
			return Expr("[" + TableName + "].[" + ColumnName + "]");
		}

		public void CreateXmlNode(XmlNode node)
		{
			XmlDocument xml = node.OwnerDocument;

			XmlNode filterNode = xml.CreateNode(XmlNodeType.Element, "filter", "");
			node.AppendChild(filterNode);

			XmlAttribute attr = xml.CreateAttribute("Type");
			attr.Value = GetType().ToString();
			filterNode.Attributes.Append(attr);

			attr = xml.CreateAttribute("TableName");
			attr.Value = TableName;
			filterNode.Attributes.Append(attr);

			attr = xml.CreateAttribute("ColumnName");
			attr.Value = ColumnName;
			filterNode.Attributes.Append(attr);

			if (!string.IsNullOrEmpty(FilterName))
			{
				attr = xml.CreateAttribute("FilterName");
				attr.Value = FilterName;
				filterNode.Attributes.Append(attr);
			}

			attr = xml.CreateAttribute("Verb");
			attr.Value = Verb.ToString();
			filterNode.Attributes.Append(attr);

			attr = xml.CreateAttribute("Negation");
			attr.Value = Negation.ToString();
			filterNode.Attributes.Append(attr);

			createValueNodes(filterNode);
		}

		public static FilterInfo ReadXmlNode(XmlNode node)
		{
			if (node == null)
				return null;

			try
			{
				var type = node.Attributes["Type"].Value;
				var vals = node.SelectNodes("Value");

				FilterInfo filter;
				if (type.IndexOf("Integer") != -1)
				{
					var _filter = new IntegerFilterInfo();

					if (vals.Count > 0)
					{
						_filter.Values = new int[vals.Count];
						int j = 0;
						foreach (XmlNode val in vals)
							_filter.Values[j++] = Convert.ToInt32(val.InnerText);
					}
					filter = _filter;
				}
				else if (type.IndexOf("Double") != -1)
				{
					var _filter = new DoubleFilterInfo();
					if (vals.Count > 0)
					{
						_filter.Values = new double[vals.Count];
						var j = 0;
						foreach (XmlNode val in vals)
							_filter.Values[j++] = Convert.ToDouble(val.InnerText);
					}
					filter = _filter;
				}
				else if (type.IndexOf("String") != -1)
				{
					var _filter = new StringFilterInfo();
					if (vals.Count > 0)
					{
						_filter.Values = new string[vals.Count];
						var j = 0;
						foreach (XmlNode val in vals)
							_filter.Values[j++] = val.InnerText;
					}
					filter = _filter;
				}
				else if (type.IndexOf("Object") != -1)
				{
					var _filter = new ObjectFilterInfo();
					if (vals.Count > 0)
					{
						_filter.Values = new Guid[vals.Count];
						var j = 0;
						foreach (XmlNode val in vals)
							_filter.Values[j++] = new Guid(val.InnerText);
					}
					filter = _filter;
				}
				else if (type.IndexOf("DateTime") != -1)
				{
					var _filter = new DateTimeFilterInfo();
					if (vals.Count > 0)
					{
						_filter.Values = new DateTime[vals.Count];
						int j = 0;
						foreach (XmlNode val in vals)
							_filter.Values[j++] = DateTime.Parse(val.InnerText, _filter.DateTimeFormat);
					}
					filter = _filter;
				}
				else
					throw new ApplicationException("FilterInfo.ReadXmlNode: Wrong filter!");

				XmlAttribute attr;

				filter.ColumnName = node.Attributes["ColumnName"].Value;

				if ((attr = node.Attributes["TableName"]) != null)
					filter.TableName = attr.Value;
				if ((attr = node.Attributes["FilterName"]) != null)
					filter.FilterName = attr.Value;
				if ((attr = node.Attributes["Negation"]) != null && attr.Value == "True")
					filter.Negation = true;
				else
					filter.Negation = false;

				switch (node.Attributes["Verb"].Value)
				{
					case "Equal":
						filter.Verb = FilterVerb.Equal;
						break;
					case "Like":
						filter.Verb = FilterVerb.Like;
						break;
					case "Between":
						filter.Verb = FilterVerb.Between;
						break;
					case "Greater":
						filter.Verb = FilterVerb.Greater;
						break;
					case "GreaterOrEqual":
						filter.Verb = FilterVerb.GreaterOrEqual;
						break;
					case "Less":
						filter.Verb = FilterVerb.Less;
						break;
					case "LessOrEqual":
						filter.Verb = FilterVerb.LessOrEqual;
						break;
					case "In":
						filter.Verb = FilterVerb.In;
						break;
					case "IsNull":
						filter.Verb = FilterVerb.IsNull;
						break;
				}
				return filter;
			}
			catch
			{
				return null;
			}
		}

	}
	public enum FilterCombinationType { AND, OR };
	public class FilterCollection
	{
		public string FilterString;
		public FilterCombinationType CombinationType;
		public FilterInfo[] Filters;
		public string BuildFilter()
		{
			if (Filters == null || Filters.Length == 0) return "";
			var sb = new StringBuilder();
			if (string.IsNullOrEmpty(FilterString))
			{
				for (var i = 0; i < Filters.Length; i++)
				{
					var item = Filters[i];
					if (item.ToString() == "") continue;
					sb.Append(@"
	" + item + (i == Filters.Length - 1 ? "" : (CombinationType == FilterCombinationType.AND ? " AND " : " OR ")));
				}
			}
			else
			{
				var flt = FilterString.ToLower();
				foreach (var fi in Filters)
				{
					var filterName = fi.FilterName.ToLower();
					flt = flt.Replace(filterName, fi.ToString());
				}
				sb.Append(flt);
			}
			return sb.ToString();
		}
	}
	public class StringFilterInfo : FilterInfo
	{
		public string[] Values;
		public override string Expr(string fullFieldName)
		{
			var result = new StringBuilder();
			result.Append(base.Expr(fullFieldName));
			switch (Verb)
			{
				case FilterVerb.Like:
					result.Append("'" + Values[0].Replace("'", "''").Replace("*", "%") + "'");
					break;
				case FilterVerb.Between:
					result.Append("'" + Values[0].Replace("'", "''") + "' AND '" + Values[1].Replace("'", "''") + "'");
					break;
				case FilterVerb.In:
					for (int i = 0; i < Values.Length; i++)
					{
						string item = Values[i];
						result.Append("'" + item.Replace("'", "''") + "'");
						if (i != Values.Length - 1) result.Append(", ");
					}
					result.Append(")");
					break;
				case FilterVerb.IsNull:
					break;
				default:
					result.Append("'" + Values[0].Replace("'", "''") + "'");
					break;

			}
			result.Append((Negation ? ")" : ""));
			return result.ToString();
		}
		protected override void createValueNodes(XmlNode filterNode)
		{
			if (Values != null)
			{
				var xml = filterNode.OwnerDocument;
				foreach (string val in Values)
				{
					var valueNode = xml.CreateNode(XmlNodeType.Element, "Value", "");
					valueNode.InnerText = val;
					filterNode.AppendChild(valueNode);
				}
			}
		}

	}
	public class IntegerFilterInfo : FilterInfo
	{
		public int[] Values;
		public override string Expr(string fullFieldName)
		{
			var result = new StringBuilder();
			result.Append(base.Expr(fullFieldName));
			switch (Verb)
			{
				case FilterVerb.Between:
					result.Append(Values[0].ToString() + " AND " + Values[1].ToString());
					break;
				case FilterVerb.In:
					for (var i = 0; i < Values.Length; i++)
					{
						var item = Values[i].ToString();
						result.Append(item);
						if (i != Values.Length - 1) result.Append(", ");
					}
					result.Append(")");
					break;
				case FilterVerb.IsNull:
					break;
				default:
					result.Append(Values[0].ToString());
					break;
			}
			result.Append((Negation ? ")" : ""));
			return result.ToString();
		}
		protected override void createValueNodes(XmlNode filterNode)
		{
			var xml = filterNode.OwnerDocument;
			foreach (var val in Values)
			{
				var valueNode = xml.CreateNode(XmlNodeType.Element, "Value", "");
				valueNode.InnerText = val.ToString();
				filterNode.AppendChild(valueNode);
			}
		}
	}
	public class DoubleFilterInfo : FilterInfo
	{
		public double[] Values;
		public override string Expr(string fullFieldName)
		{
			var result = new StringBuilder();
			result.Append(base.Expr(fullFieldName));
			var prov = new NumberFormatInfo {NumberDecimalSeparator = "."};
			switch (Verb)
			{
				case FilterVerb.Between:
					result.Append(Values[0].ToString(prov) + " AND " + Values[1].ToString(prov));
					break;
				case FilterVerb.In:
					for (int i = 0; i < Values.Length; i++)
					{
						string item = Values[i].ToString(prov);
						result.Append(item);
						if (i != Values.Length - 1) result.Append(", ");
					}
					result.Append(")");
					break;
				case FilterVerb.IsNull:
					break;
				default:
					result.Append(Values[0].ToString(prov));
					break;

			}
			result.Append((Negation ? ")" : ""));
			return result.ToString();
		}
		protected override void createValueNodes(XmlNode filterNode)
		{
			var xml = filterNode.OwnerDocument;
			foreach (double val in Values)
			{
				var valueNode = xml.CreateNode(XmlNodeType.Element, "Value", "");
				valueNode.InnerText = val.ToString();
				filterNode.AppendChild(valueNode);
			}
		}
	}
	public class DateTimeFilterInfo : FilterInfo
	{
		public DateTime[] Values;
		public override string Expr(string fullFieldName)
		{
			var result = new StringBuilder();
			result.Append(base.Expr(fullFieldName));
			switch (Verb)
			{
				case FilterVerb.Between:
					result.Append("'" + Values[0].ToString("yyyyMMdd HH:mm:ss.fff") + "' AND '" + Values[1].ToString("yyyyMMdd HH:mm:ss.fff") + "'");
					break;
				case FilterVerb.In:
					for (var i = 0; i < Values.Length; i++)
					{
						var item = "'" + Values[i].ToString("yyyyMMdd HH:mm:ss.fff") + "'";
						result.Append(item);
						if (i != Values.Length - 1) result.Append(", ");
					}
					result.Append(")");
					break;
				case FilterVerb.IsNull:
					break;
				default:
					result.Append("'" + Values[0].ToString("yyyyMMdd HH:mm:ss.fff") + "'");
					break;
			}
			result.Append((Negation ? ")" : ""));
			return result.ToString();
		}

		protected override void createValueNodes(XmlNode filterNode)
		{
			var xml = filterNode.OwnerDocument;
			foreach (DateTime val in Values)
			{
				var valueNode = xml.CreateNode(XmlNodeType.Element, "Value", "");
				valueNode.InnerText = val.ToString(DateTimeFormat.FullDateTimePattern);
				filterNode.AppendChild(valueNode);
			}
		}
	}
	public class ObjectFilterInfo : FilterInfo
	{
		public Guid[] Values { get; set; }

		public override string Expr(string fullFieldName)
		{
			var result = new StringBuilder();
			result.Append(base.Expr(fullFieldName));
			switch (Verb)
			{
				case FilterVerb.In:
					for (var i = 0; i < Values.Length; i++)
					{
						var item = Values[i].ToString();
						result.Append("'" + item + "'");
						if (i != Values.Length - 1) result.Append(", ");
					}
					result.Append(")");
					break;
				case FilterVerb.IsNull:
					break;
				default:
					result.Append("'" + Values[0].ToString() + "'");
					break;

			}
			result.Append((Negation ? ")" : ""));
			return result.ToString();
		}
		protected override void createValueNodes(XmlNode filterNode)
		{
			var xml = filterNode.OwnerDocument;
			foreach (Guid val in Values)
			{
				var valueNode = xml.CreateNode(XmlNodeType.Element, "Value", "");
				valueNode.InnerText = val.ToString();
				filterNode.AppendChild(valueNode);
			}
		}
	}
	public class ExpressionFilterInfo : FilterInfo
	{
		public override string Expr(string fullFieldName)
		{
			return fullFieldName;
		}
		protected override void createValueNodes(XmlNode filterNode) { }
	}
	public enum FilterVerb { Equal, Like, Between, Greater, GreaterOrEqual, Less, LessOrEqual, In, IsNull };
	public enum OrderDir { Asc, Desc };
	public class OrderInfo
	{
		public string TableName { get; set; }
		public string ColumnName { get; set; }
		public OrderDir order { get; set; }

	    public OrderInfo NextOrder { get; set; }
	    public bool IsDefault { get; set; }

		public OrderInfo() { }
		public OrderInfo(string columnName)
		{
			ColumnName = columnName;
		}
		public OrderInfo(string columnName, OrderDir dir)
		{
			ColumnName = columnName;
			order = dir;
		}
		public OrderInfo(string columnName, OrderDir dir, string tableName)
		{
			ColumnName = columnName;
			TableName = tableName;
			order = dir;
		}
		public void CreateXmlNode(XmlNode node)
		{
			XmlDocument xml = node.OwnerDocument;
			XmlNode orderNode = xml.CreateNode(XmlNodeType.Element, "order", "");

			node.AppendChild(orderNode);

			XmlAttribute attr = xml.CreateAttribute("TableName");
			attr.Value = TableName;
			orderNode.Attributes.Append(attr);

			attr = xml.CreateAttribute("ColumnName");
			attr.Value = ColumnName;
			orderNode.Attributes.Append(attr);

			attr = xml.CreateAttribute("order");
			attr.Value = order.ToString();
			orderNode.Attributes.Append(attr);

		    NextOrder?.CreateXmlNode(node);
		}
		public static OrderInfo ReadXmlNode(XmlNode node)
		{
			if (node == null)
				return null;
			var order = new OrderInfo();

			XmlAttribute attr;
			if ((attr = node.Attributes["TableName"]) != null)
				order.TableName = attr.Value;
			if ((attr = node.Attributes["ColumnName"]) != null)
				order.ColumnName = attr.Value;
			if ((attr = node.Attributes["order"]) != null)
			{
				if (attr.Value == "Asc")
					order.order = OrderDir.Asc;
				else if (attr.Value == "Desc")
					order.order = OrderDir.Desc;
			}
			return order;
		}
		public string Expr(string fullFieldName)
		{
			return fullFieldName + " " + (order == OrderDir.Desc ? "DESC" : "");
		}
	}
	public class MetadataInfo
	{
		public ClassInfo[] Classes;
		public Prop FindProp(string fieldName, int mask)
		{
			Prop finded = null;
			bool inSingle = (mask & 1) != 0;
			bool inMulti = (mask & 2) != 0;
			bool inLookup = (mask & 4) != 0;
			foreach (ClassInfo cl in Classes)
			{
				if (inSingle)
				{
					foreach (var fl in cl.Fields)
					{
						if (fl.PropName.ToUpper() != fieldName.ToUpper()) continue;
						finded = fl;
						break;
					}
				}
				if (inMulti && finded == null && cl.MultiFields != null)
				{
					foreach (var fl in cl.MultiFields.Where(fl => fl.PropName.ToUpper() == fieldName.ToUpper()))
					{
						finded = fl;
						break;
					}
				}
				if (inLookup && finded == null && cl.LookupFields != null)
				{
					foreach (var fl in cl.LookupFields.Where(fl => fl.PropName.ToUpper() == fieldName.ToUpper()))
					{
						finded = fl;
						break;
					}
				}
				if (finded != null) break;
			}
			return finded;
		}
	}
	public class ClassInfo
	{
		public Guid CID;
		public string ClassName;
		public string TableName;
		public PropInfo[] Fields;
		public LookupPropInfo[] LookupFields;
		public MultiPropInfo[] MultiFields;
	}
	public enum DataType { String, Int, Uniqueidentifier, DateTime, Money, Float, Boolean, Binary }
	public class Prop
	{
		public string PropName;
		public string TableName;
	}
	public class LinkedProp : Prop
	{
		public string ParentFieldName;
		public string ParentTableName;
	}
	public class PropInfo : Prop
	{
		public DataType ColumnType;
		public int MaximumLength;
		public bool IsRefer;
	}
	public class LookupPropInfo : LinkedProp
	{
		public string FieldName;
		public string FieldID;
	}
	public class MultiPropInfo : LinkedProp
	{
		public bool WithOrder;
		public bool IsPersistent;
		public bool One2Many;
		public string ClassName;
		public Guid CID = Guid.Empty;
		public string FieldOID;
		public PropInfo FieldProp;
		public PropInfo FieldOrd;
	}
}
