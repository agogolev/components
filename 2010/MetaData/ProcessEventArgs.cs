using System;

namespace MetaData
{
	/// <summary>
	/// Summary description for DataSetISM.
	/// </summary>

	public class ProcessEventArgs : EventArgs
	{
		public ProcessEventArgs(int Row_Index)
		{
			RowIndex = Row_Index;
		}

		public int RowIndex;
	}
}
