﻿using System.Linq;

namespace MetaData
{
    /// <summary>
    ///     Little data structure to hold information about file types.
    ///     Holds information about binary header at the start of the file
    /// </summary>
    public class FileType
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="FileType" /> class.
        ///     Default construction with the header offset being set to zero by default
        /// </summary>
        /// <param name="header">Byte array with header.</param>
        /// <param name="extension">String with extension.</param>
        /// <param name="mime">The description of MIME.</param>
        public FileType(byte?[] header, string extension, string mime)
        {
            this.Header = header;
            this.Extension = extension;
            this.Mime = mime;
            HeaderOffset = 0;
        }


        /// <summary>
        ///     Initializes a new instance of the <see cref="FileType" /> struct.
        ///     Takes the details of offset for the header
        /// </summary>
        /// <param name="header">Byte array with header.</param>
        /// <param name="offset">The header offset - how far into the file we need to read the header</param>
        /// <param name="extension">String with extension.</param>
        /// <param name="mime">The description of MIME.</param>
        public FileType(byte?[] header, int offset, string extension, string mime)
        {
            this.Header = null;
            this.Header = header;
            HeaderOffset = offset;
            this.Extension = extension;
            this.Mime = mime;
        }

        public byte?[] Header { get; private set; }
        // most of the times we only need first 8 bytes, but sometimes extend for 16
        public int HeaderOffset { get; private set; }
        public string Extension { get; private set; }
        public string Mime { get; private set; }

        protected bool Equals(FileType other)
        {
            return Header.SequenceEqual(other.Header) && HeaderOffset == other.HeaderOffset && string.Equals(Extension, other.Extension) && string.Equals(Mime, other.Mime);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((FileType) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = (Header != null ? Header.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ HeaderOffset;
                hashCode = (hashCode*397) ^ (Extension != null ? Extension.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (Mime != null ? Mime.GetHashCode() : 0);
                return hashCode;
            }
        }

        public override string ToString()
        {
            return Extension;
        }
    }
}