@echo off

echo +------------------------------+
echo +   WebSupergoo ABCpdf .NET    +
echo +------------------------------+
echo Registering type library ...
echo.

rem find the ABCpdf Common folder
cd ..\Common\
if not exist "ABCpdf.dll" (
	cd %ProgramFiles%\WebSupergoo
	cd ABCpdf*
	cd Common
)
if not exist "%CD%\ABCpdf.dll" (
	echo Script unable to find ABCpdf.dll
	echo %CD%
	goto end
)
if not exist "%CD%\..\ABCpdf.tlb" (
	echo Script unable to find ABCpdf.tlb Type Library.
	echo %CD%
	goto end
)



set REGASM32=%SystemRoot%\Microsoft.NET\Framework\v2.0.50727\regasm.exe
set REGTLIB32=%SystemRoot%\Microsoft.NET\Framework\v2.0.50727\regtlibv12.exe
rem set REGTLIB=%SystemRoot%\regtlib.exe

set REGASM64=%SystemRoot%\Microsoft.NET\Framework64\v2.0.50727\regasm.exe
set REGTLIB64=%SystemRoot%\Microsoft.NET\Framework64\v2.0.50727\regtlibv12.exe
rem set REGTLIB=%SystemRoot%\regtlib.exe



if exist "%REGASM32%" (
	echo.
	echo Registering ABCpdf under x86 for 32 bit apps.
	%REGASM32% "%CD%\ABCpdf.dll" /tlb:"%CD%\..\ABCpdf.tlb" /codebase /nologo
)
if exist "%REGASM64%" (
	echo.
	echo Registering ABCpdf under x64 for 64 bit apps.
	%REGASM64% "%CD%\ABCpdf.dll" /tlb:"%CD%\..\ABCpdf.tlb" /codebase /nologo
)
goto end



rem ----Note: Server versions of Windows such as Windows Server 2003 may have
rem ----permission issues when using .NET objects exposed as COM objects through COM-Interop.
rem ----To avoid some of the permission issues, make sure ABCpdf is installed with the
rem ----installation package and use regasm.exe without /codebase switch

rem ----.NET Framework 3.0 is installed, and ABCpdf is installed with the installation package
rem %REGASM32% "%CD%\ABCpdf.dll" /tlb:"%CD%\..\ABCpdf.tlb" /nologo
rem %REGASM64% "%CD%\ABCpdf.dll" /tlb:"%CD%\..\ABCpdf.tlb" /nologo
rem ----.NET Framework 3.0 is installed, and ABCpdf is manually installed
rem %REGASM32% "%CD%\ABCpdf.dll" /tlb:"%CD%\..\ABCpdf.tlb" /codebase /nologo
rem %REGASM64% "%CD%\ABCpdf.dll" /tlb:"%CD%\..\ABCpdf.tlb" /codebase /nologo
rem ----Without .NET Framework 3.0
rem if not exist "%REGTLIB32%" (
rem	echo %REGTLIB32% not found.
rem	goto end
rem )
rem if not exist "%REGTLIB64%" (
rem	echo %REGTLIB64% not found.
rem	goto end
rem )
rem %REGTLIB32% "%CD%\..\ABCpdf.tlb"
rem %REGTLIB64% "%CD%\..\ABCpdf.tlb"
rem ----Without .NET Framework 3.0, and ABCpdf is installed with the installation package
rem %REGASM32% "%CD%\ABCpdf.dll" /registered /nologo
rem %REGASM64% "%CD%\ABCpdf.dll" /registered /nologo
rem ----Without .NET Framework 3.0, and ABCpdf is manually installed
rem %REGASM32% "%CD%\ABCpdf.dll" /registered /codebase /nologo
rem %REGASM64% "%CD%\ABCpdf.dll" /registered /codebase /nologo

if errorlevel 1 (
	echo Operation failed.
	goto end
)



:end
echo.
pause
