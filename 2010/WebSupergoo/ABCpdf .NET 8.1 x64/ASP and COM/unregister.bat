@echo off

echo +------------------------------+
echo +   WebSupergoo ABCpdf .NET    +
echo +------------------------------+
echo Unregistering type library ...
echo.

rem find the ABCpdf Common folder
cd ..\Common\
if not exist "ABCpdf.dll" (
	cd %ProgramFiles%\WebSupergoo
	cd ABCpdf*
	cd Common
)
if not exist "%CD%\ABCpdf.dll" (
	echo Script unable to find ABCpdf.dll
	echo %CD%
	goto end
)
if not exist "%CD%\..\ABCpdf.tlb" (
	echo Script unable to find ABCpdf.tlb Type Library.
	echo %CD%
	goto end
)



set REGASM32=%SystemRoot%\Microsoft.NET\Framework\v2.0.50727\regasm.exe
set REGTLIB32=%SystemRoot%\Microsoft.NET\Framework\v2.0.50727\regtlibv12.exe
rem set REGTLIB=%SystemRoot%\regtlib.exe

set REGASM64=%SystemRoot%\Microsoft.NET\Framework64\v2.0.50727\regasm.exe
set REGTLIB64=%SystemRoot%\Microsoft.NET\Framework64\v2.0.50727\regtlibv12.exe
rem set REGTLIB=%SystemRoot%\regtlib.exe



if exist "%REGASM32%" (
	echo.
	echo Unregistering ABCpdf under x86 for 32 bit apps.
	%REGASM32% "%CD%\ABCpdf.dll" /tlb:"%CD%\..\ABCpdf.tlb" /u /nologo
)
if exist "%REGASM64%" (
	echo.
	echo Unregistering ABCpdf under x64 for 64 bit apps.
	%REGASM64% "%CD%\ABCpdf.dll" /tlb:"%CD%\..\ABCpdf.tlb" /u /nologo
)
goto end



rem ----Without .NET Framework 3.0
rem if not exist "%REGTLIB%" (
rem	echo %REGTLIB% not found.
rem	goto end
rem )
rem %REGTLIB% "%ABCDIR%\ABCpdf.tlb"
rem %REGASM% "%ABCDIR%\ABCpdf.dll" /registered /u /nologo

if errorlevel 1 (
	echo Operation failed.
	goto end
)



:end
echo.
pause

