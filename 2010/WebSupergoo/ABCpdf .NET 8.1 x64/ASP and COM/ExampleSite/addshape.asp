<% @Language="VBScript" %>
<%
Response.Buffer = True
%>

<%
Set theDoc = Session("doc")
theDoc.Width = Request.Form("Width")
theDoc.Color = Request.Form("Color")
theDoc.Rect.SetRect Request.Form("x"), Request.Form("y"), Request.Form("w"), Request.Form("h")

theOp = Request.Form("op")
x1 = CInt(Request.Form("x"))
x2 = x1 + CInt(Request.Form("w"))
y1 = CInt(Request.Form("y"))
y2 = y1 + CInt(Request.Form("h"))

If theOp = "fill" Then
	theDoc.FillRect
ElseIf theOp = "frame" Then
	theDoc.FrameRect
ElseIf theOp = "linetlbr" Then
	theDoc.AddLine x1, y2, x2, y1
Else
	theDoc.AddLine x1, y1, x2, y2
End If

Response.Redirect "default.asp"
%>


            
