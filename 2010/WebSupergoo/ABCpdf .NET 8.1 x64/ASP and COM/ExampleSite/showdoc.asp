<% @Language="VBScript" %>
<%
Response.Clear
Response.Buffer = True
Response.Expires = -1000
Set theDoc = Session("doc")
theData = theDoc.GetData()
Response.ContentType = "application/pdf"
' IIS 7.5 may cause problems if content-length is specified.
' It is unknown whether it is a problem with IIS 7.5
' or content-length is not supposed to be specified at all.
'Response.AddHeader "content-length", UBound(theData) - LBound(theData) + 1
If Request.QueryString("attachment") <> "" Then
	Response.AddHeader "content-disposition", "attachment; filename=ExampleSite.pdf"
Else
	Response.Addheader "content-disposition", "inline; filename=ExampleSite.pdf"
End If
Response.BinaryWrite theData
Response.End
%>
