<html>
<head>
<title>ABCpdf Example Site</title>
<link rel="stylesheet" href="mystyles.css">

<script language="javascript">
<!--
function OpenPreview() {
	theFeats = "height=320,width=320,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,toolbar=no";
	window.open("showdoc.asp", "Preview", theFeats);
}
//-->
</script>

<link rel="stylesheet" href="mystyles.css">
</head>

<body>
<h1><img src="images/goo.gif" width="58" height="60" align="right"></h1>
<h1>ABCpdf Example Site</h1>
<p><b>
<%
On Error Resume Next

Set doc = Session("doc")
If Err <> 0 Then
	Response.Write("<b>WARNING: global.asa failed to create ABCpdf document. ")
	Response.Write("Creating session scope document manually.<br><br></b>")
	Err.Clear
	Set doc = Server.CreateObject("ABCpdf8.Doc")
	If Err <> 0 Then
		Response.Write("<b>Could not create ABCpdf object.<b><br><br>")
		Response.Write("Error: <i>" & Err.Description & "</i><br><br>")
		Response.Write("This error is most likely a result of an installation problem. ")
		Response.Write("Have you run the ABCpdf Installer? Were you logged on as Administrator ")
		Response.Write("when you installed? Check the Application Event Log for any installation errors. ")
		Response.Write("If you need help please see ")
		Response.Write("<a href='http://www.websupergoo.com/support.htm'>our support pages.</a><br><br>")
		Response.End
	End If
	Set Session("doc") = doc
	If Err <> 0 Then
		Response.Write("<b>Could not access session state.<b><br><br>")
		Response.Write("Error: <i>" & Err.Description & "</i><br><br>")
		Response.Write("This error is most likely a result of an IIS configuration problem. ")
		Response.Write("It is specific to the way that the example pages work rather than any ")
		Response.Write("issue with ABCpdf itself. These example pages store an ABCpdf document object in ")
		Response.Write("session state so that you can add items and the state ")
		Response.Write("of your document is preserved as you navigate the site. You need ")
		Response.Write("to have session state enabled for these pages to work. ")
		Response.Write("If you need help please see ")
		Response.Write("<a href='http://www.websupergoo.com/support.htm'>our support pages.</a><br><br>")
		Response.End
	End If
End If

Set doc = Session("doc")
theLicense = doc.License
If Err <> 0 then
	Response.Write("ABCpdf Not Installed.<br><br>")
	Response.End
ElseIf InStr(theLicense, "Trial") Then
	Response.Write("ABCpdf " & theLicense & "<br><br>")
End If


%>
</b></p>
<p>Using the links below you can add text, images and other graphics to your very 
  own PDF document. You can choose to view the document in a browser window or
  download it to your computer for viewing. The actual document itself 
  is dynamically generated and sent direct to your web browser - there are no 
  files saved anywhere.</p>
<p>In this example web site we limit the document to a few pages to make it 
  easy to see the changes you're making. However if you want to create large multi-page 
  documents it's only a few more lines of code!</p>
<ul>
  <li><a href="addtext.htm">Add Text</a> - add a paragraph of text.</li>
  <li><a href="addurl.htm">Add Web Page</a> - draw a web page.</li>
  <li><a href="addimage.htm">Add Image</a> - draw a selected image.</li>
  <li><a href="addshape.htm">Add Shape</a> - draw a rectangle or line.</li>
</ul><ul>
  <li><a href="#" OnClick="OpenPreview()">Show PDF</a> - show the PDF in a browser window.</li>
  <li><a href="showdoc.asp?attachment=true">Download PDF</a> - download the PDF to your computer.</li>
</ul><ul>
  <li><a href="cleardoc.asp">Clear Document</a> - delete all document content.</li>
</ul>
<p>If you're on a slow connection - do remember that as you add more and more 
  to your document it will get larger. This is particularly true when you add 
  images. As the document gets larger it will take longer to refresh. This doesn't 
  mean that ABCpdf is taking longer - just that it's taking longer to send the 
  document down the line to your browser.</p>
</body>
</html>
