<% @Language="VBScript" %>
<%
Response.Buffer = True
%>

<%
Set theDoc = Session("doc")
theDoc.Rect.SetRect Request.Form("x"), Request.Form("y"), Request.Form("w"), Request.Form("h")
theDoc.AddText Request.Form("text")
theURL = Request.Form("url")
If Left(theURL, 4) <> "http" Then theURL = "http://www.google.com/"

theID = theDoc.AddImageUrl(theURL, True, 0, False)
For i = 1 To 3 ' add up to 3 pages
  If theDoc.GetInfo(theID, "Truncated") <> "1" Then Exit For
  theDoc.Page = theDoc.AddPage()
  theID = theDoc.AddImageToChain(theID)
Next
theDoc.PageNumber = 1

Response.Redirect "default.asp"
%>

