<% @Language="VBScript" %>
<%
Response.Buffer = False
Response.Expires = -1000
%>

<HTML>
<link rel="stylesheet" href="mystyles.css" type="text/css">

<BODY>
<h1>Document Structure</h1>
<%

Set theDoc = Session("doc")
i = 0

Do
	i = i + 1
	If theDoc.GetInfo(i, "ID") = "" Then Exit Do
    theType = theDoc.GetInfo(i, "type")

	Response.Write "<p>"
    Response.Write "ID      = " & theDoc.GetInfo(i, "id") & "<br>"
    Response.Write "Type    = " & theDoc.GetInfo(i, "type") & "<br>"
    Response.Write "Active  = " & theDoc.GetInfo(i, "active") & "<br>"
    If theType = "cata" Then
        Response.Write "Pages   = " & theDoc.GetInfo(i, "pages") & "<br>"
        Response.Write "Outlines= " & theDoc.GetInfo(i, "outlines") & "<br>"
    End If
    If theType = "pags" Then
        theCount = theDoc.GetInfo(i, "page count")
        Response.Write "Page No.= " & theCount & "<br>"
        For j = 1 To theCount
            Response.Write "Page " & j & "  = " & theDoc.GetInfo(i, "page " & j) & "<br>"
        Next
    End If
    If theType = "page" Then
        theCount = theDoc.GetInfo(i, "content count")
        Response.Write "Cont No.= " & theCount & "<br>"
        For j = 1 To theCount
            Response.Write "Cont " & j & "  = " & theDoc.GetInfo(i, "content " & j) & "<br>"
        Next
    End If
    If theType = "jpeg" Then
        Response.Write "Width   = " & theDoc.GetInfo(i, "width") & "<br>"
        Response.Write "Height  = " & theDoc.GetInfo(i, "height") & "<br>"
        Response.Write "Comps   = " & theDoc.GetInfo(i, "components") & "<br>"
    End If
    If theType = "imag" Then
        Response.Write "XObject = " & theDoc.GetInfo(i, "xobject") & "<br>"
    End If
    If theType = "text" Then
        Response.Write "EndPos  = " & theDoc.GetInfo(i, "endpos") & "<br>"
        Response.Write "Chars   = " & theDoc.GetInfo(i, "characters") & "<br>"
        Response.Write "Lines   = " & theDoc.GetInfo(i, "lines") & "<br>"
    End If
	Response.Write "</p>"
Loop

%>

</body>
</html>

            
