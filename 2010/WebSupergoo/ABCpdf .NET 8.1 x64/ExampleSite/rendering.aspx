<%@ Page %>
<%@ Assembly Name="ABCpdf" %>
<%@ Import Namespace="WebSupergoo.ABCpdf8" %>
<%@ Import Namespace="WebSupergoo.ABCpdf8" %>

<!-- #include File = validation.aspx -->
<%
Doc	theDoc = (Doc)Session["doc"];

double w = ValidateDouble(Request.Form["w"], false, "Incorrect rectangle width.");
double h = ValidateDouble(Request.Form["h"], false, "Incorrect rectangle height.");
double x = ValidateDouble(Request.Form["x"], false, "Incorrect Left coordinate.");
double y = ValidateDouble(Request.Form["y"], false, "Incorrect Bottom coordinate.");

theDoc.Rendering.AntiAliasImages = Request.Form["AntiAliasImages"] == "on";
theDoc.Rendering.AntiAliasPolygons = Request.Form["AntiAliasPolygons"] == "on";
theDoc.Rendering.AntiAliasText = Request.Form["AntiAliasText"] == "on";
theDoc.Rendering.AntiAliasScene = Request.Form["AntiAliasScene"] == "on";
theDoc.Rendering.DrawAnnotations = Request.Form["DrawAnnotations"] == "on";

try {
	theDoc.Rendering.ColorSpace = (XRendering.ColorSpaceType)Enum.Parse(typeof(XRendering.ColorSpaceType), Request.Form["Colorspace"], true);
} catch {
	theDoc.Rendering.ColorSpace = XRendering.ColorSpaceType.Rgb;
}
theDoc.Rendering.BitsPerChannel = Convert.ToInt32(Request.Form["Precision"]);
theDoc.Rendering.DotsPerInch = ValidateDouble(Request.Form["DotsPerInch"], true, "Incorrect dots per inch value.");
theDoc.Rendering.SaveQuality = (int)ValidateRangeDouble(Request.Form["SaveQuality"], 0, 100, "Incorrect save quality value. Should be a number between 0 and 100.");

theDoc.Rect.SetRect(x, y, w, h);

string ext = Request.Form["FileFormat"];
string imageName = "1."+ext;

try  {
	byte[] theData = theDoc.Rendering.GetData(imageName);
	Response.Expires = -1000;
	Response.ContentType = "image/" + ext;
	Response.AddHeader("content-length", theData.Length.ToString());
	if ((ext != "PNG") && (ext != "JPG")) 
		Response.AddHeader("content-disposition", "attachment; filename =" + imageName);
	else
		Response.AddHeader("content-disposition", "inline; filename =" + imageName);
	Response.BinaryWrite(theData);
	Response.Flush();
}
catch (Exception e) {
	Session["warning"] = "<p>Unable to render image. </p><p>" + e.ToString() + "</p>";
	Response.Redirect("warning.aspx");
}
%>
