<%@ Page %>
<%@ Import Namespace="WebSupergoo.ABCUpload6" %>

<% 

Progress theProgress = new Progress(Request.QueryString["ProgressID"]);
string theID = theProgress.ID.ToString();
string theMins = ((int)theProgress.SecondsLeft / 60).ToString();
string theSecs = ((int)theProgress.SecondsLeft % 60).ToString();
string theMeta = "<meta http-equiv=\"refresh\" content=\"2,progressbar.aspx?ProgressID=" + theID + "\">";
string thePercent = theProgress.PercentDone.ToString();
string theKbps = Math.Round(theProgress.BytesPerSecondCurrent / 1024, 1).ToString();
string theKbdone = Math.Round((double)theProgress.BytesDone / 1024, 1).ToString();
string theKbtotal = Math.Round((double)theProgress.BytesTotal / 1024, 1).ToString();
string theNote = theProgress.Note;
string theFileName = theProgress.FileName;
if (theProgress.Finished) theMeta = "";

%>
<HTML>
	<HEAD>
		<title>Progress...</title>
		<meta http-equiv="expires" content="Tue, 01 Jan 1981 01:00:00 GMT">
		<% = theMeta %>
		<script language="javascript">
<!--
if (<% = thePercent %> >= 100) top.close();
//-->
		</script>
	</HEAD>
	<body bgcolor="#cccccc">
		<table border="0" width="100%">
			<tr>
				<td><font face="Verdana, Arial, Helvetica, sans-serif" size="2"><b>Uploading:</b></font></td>
			</tr>
			<tr bgcolor="#999999">
				<td>
					<table border="0" width="<% = thePercent %>%" cellspacing="1" bgcolor="#0033ff">
						<tr>
							<td><font size="1">&nbsp;</font></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<table border="0" width="100%">
						<tr>
							<td><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Estimated&nbsp;Time&nbsp;Left:</font></td>
							<td><font face="Verdana, Arial, Helvetica, sans-serif" size="1">
									<% = theMins %> min
									<% = theSecs %> secs 
									(<% = theKbdone %> KB of
									<% = theKbtotal %> KB uploaded)
							</font></td>
						</tr>
						<tr>
							<td><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Transfer&nbsp;Rate:</font></td>
							<td><font face="Verdana, Arial, Helvetica, sans-serif" size="1"><% = theKbps %> KB/sec</font></td>
						</tr>
						<tr>
							<td><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Information:</font></td>
							<td><font face="Verdana, Arial, Helvetica, sans-serif" size="1"><% = theNote %></font></td>
						</tr>
						<tr>
							<td><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Uploading&nbsp;File:</font></td>
							<td><font face="Verdana, Arial, Helvetica, sans-serif" size="1"><% = theFileName %></font></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
			</tr>
		</table>
	</body>
</HTML>
