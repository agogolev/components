<%@ Page %>

<%@ Assembly Name="ABCpdf" %>
<%@ Import Namespace="WebSupergoo.ABCpdf8" %>

<!-- #include File = validation.aspx -->
<%
Doc	theDoc = (Doc)Session["doc"];
double w = ValidateDouble(Request.Form["w"], false, "Incorrect rectangle width");
double h = ValidateDouble(Request.Form["h"], false, "Incorrect rectangle height");
double x = ValidateDouble(Request.Form["x"], false, "Incorrect Left coordinate");
double y = ValidateDouble(Request.Form["y"], false, "Incorrect Bottom coordinate");

string theURL = Request.Form["url"];
if (!theURL.StartsWith("http"))
	theURL = "http://" + theURL;

bool pagedOutput = Request.Form["PagedOutput"] == "on";
theDoc.HtmlOptions.PageCacheEnabled = false;
theDoc.HtmlOptions.AddForms = Request.Form["AddForms"] == "on";
theDoc.HtmlOptions.AddLinks = Request.Form["AddLinks"] == "on";
theDoc.HtmlOptions.AddMovies = Request.Form["AddMovies"] == "on";
theDoc.HtmlOptions.FontEmbed = Request.Form["EmbedFonts"] == "on";
theDoc.HtmlOptions.UseResync = Request.Form["UseResync"] == "on";
theDoc.HtmlOptions.UseVideo = Request.Form["UseVideo"] == "on";
//theDoc.HtmlOptions.UseJava = Request.Form["UseJava"] == "on";
//theDoc.HtmlOptions.UseActiveX = Request.Form["UseActiveX"] == "on";
theDoc.HtmlOptions.UseScript = Request.Form["UseScript"] == "on";
theDoc.HtmlOptions.HideBackground = Request.Form["HideBackground"] == "on";
theDoc.HtmlOptions.Timeout = ValidateRangeInt(Request.Form["Timeout"], 1000, 100000, "Incorrect timeout value");
theDoc.HtmlOptions.LogonName = Request.Form["UserName"];
theDoc.HtmlOptions.LogonPassword = Request.Form["Password"];

if (Request.Form["SetBrowserWidth"] == "on")
	theDoc.HtmlOptions.BrowserWidth = ValidateInt(Request.Form["BrowserWidth"], true, "Incorrect browser width value. Should be a positive integer.");
else
	theDoc.HtmlOptions.BrowserWidth = 0;
	
if (Request.Form["CompressImages"] == "on")
	theDoc.HtmlOptions.ImageQuality = (int)ValidateRangeDouble(Request.Form["ImageQuality"], 0, 100, "Incorrect image quality value. Should be a number between 0 and 100.");
else
	theDoc.HtmlOptions.ImageQuality = 101;


theDoc.Rect.SetRect(x, y, w, h);

// Add url to document.
try {
	int theID = theDoc.AddImageUrl(theURL);
	if (pagedOutput) { // Add up to 3 pages.
		for (int i = 1; i <= 3; i++) { 
			if (!theDoc.Chainable(theID))
				break;
			theDoc.Page = theDoc.AddPage();
			theID = theDoc.AddImageToChain(theID);
		}
		theDoc.PageNumber = 1;
	}
}
catch {
	Session["warning"] = "<p>Web page is inaccessible. Please try another URL.</p>";
	Response.Redirect("warning.aspx");
}

Response.Redirect("addurl.htm");
%>
