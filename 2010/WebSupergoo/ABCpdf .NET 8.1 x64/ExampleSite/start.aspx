<%@ Import Namespace="System.Web" %>
<%@ Import Namespace="WebSupergoo.ABCpdf8" %>
<%@ Import Namespace="WebSupergoo.ABCUpload6" %>
<%@ Assembly Name="ABCpdf" %>

<%@ Page %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" >
<head>
	<title></title>
	<link rel="stylesheet" href="mystyles.css" type="text/css" />
</head>
<body>
	<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr valign="middle">
			<td>
				<a href='http://www.websupergoo.com/' onclick='window.open(this.href); return false;'><img src='images/logo.gif' width='272' height='120' alt='Logo' /></a>
			</td>
			<td align="right">
<%
	bool abcPdfWarn = false;
	try {
		Doc theDoc = new Doc();
		string theLic = theDoc.License;
		if (theLic.StartsWith("No License")) {
			theLic = "license unavailable or expired";
			abcPdfWarn = true;
		}
		Response.Write("<b>ABCpdf .NET " + theLic + "</b><br />");
	}
	catch (Exception ex) {
		Response.Write("<b>ABCpdf Not Installed.  " + ex.Message + "</b><br />");
	}

	bool abcUploadWarn = false;
	try {
	    string theLic = Registration.License;
	    if ((theLic.IndexOf("Trial") >= 0) && (theLic.IndexOf("Expired") >= 0)) {
	        theLic = "license unavailable or expired";
	        abcUploadWarn = true;
	    }
		Response.Write("<b>ABCUpload .NET " + theLic + "</b><br />");
	}
	catch ( Exception ex ) {
		Response.Write("<b>Could not access ABCUpload .NET. <br />");
		Response.Write(ex.Message + "<br />");
		Response.Write("Please install ABCUpload .NET and then restart your computer.</b><br />");
	}
%>
			</td>
		</tr>
	</table>
	<h1>ABCpdf Example Site</h1>
<%
	Doc doc = null;

	// Check that there is a document in session scope.
	try {
		doc = (Doc)Session["doc"];
	}
	catch {
	}
	if (doc == null) {
		Response.Write("<p><b>WARNING: global.asax failed to create ABCpdf document. ");
		Response.Write("Creating session scope document manually.<br /><br /></b></p>");
		try {
			doc = new Doc();
			Session["doc"] = doc;
		}
		catch (Exception ex) {
			Response.Write("<p><b>Could not create ABCpdf object.<br />");
			Response.Write("Error: <i>" + ex.Message + "</i></b><br /><br />");
			Response.Write("<i>This error is most likely a result of an installation problem. ");
			Response.Write("Have you run the ABCpdf Installer? Were you logged on as Administrator ");
			Response.Write("when you installed? Check the Application Event Log for any installation errors. ");
			Response.Write("If you need help please see ");
			Response.Write("<a href='http://www.websupergoo.com/support.htm'>our support pages.</a></i></p><hr />");
			HttpContext.Current.ApplicationInstance.CompleteRequest();
			return;
		}
	}

	// try and access session state
	string error = "Unknown Error";
	try {
		doc = (Doc)Session["doc"];
	}
	catch (Exception ex) {
		error = ex.Message;
	}
	if (doc == null) {
		Response.Write("<p><b>Could not access session state.<br />");
		Response.Write("Error: <i>" + error + "</i></b><br /><br />");
		Response.Write("<i>This error is most likely a result of an IIS configuration problem. ");
		Response.Write("It is specific to the way that the example pages work rather than any ");
		Response.Write("issue with ABCpdf itself. These example pages store an ABCpdf document object in ");
		Response.Write("session state so that you can add items and the state ");
		Response.Write("of your document is preserved as you navigate the site. You need ");
		Response.Write("to have session state enabled for these pages to work. ");
		Response.Write("If you need help please see ");
		Response.Write("<a href='http://www.websupergoo.com/support.htm'>our support pages.</a></i></p><hr />");
		HttpContext.Current.ApplicationInstance.CompleteRequest();
		return;
	}

	if (abcPdfWarn == true) {
	    Response.Write("<p><b>A valid ABCpdf .NET license is required. ");
	    Response.Write("Please open the PDFSettings application located under the ABCpdf menu item ");
	    Response.Write("- this will automatically insert a trial license. ");
	    Response.Write("Then simply refresh this page to ensure that the license has been picked up.</b></p><hr />");
	}
	
	if (abcUploadWarn == true) {
	    Response.Write("<p><b>ABCUpload .NET is required for the upload image page. ");
	    Response.Write("If it is not available then the upload image page will not work.");
	    Response.Write("To install a trial license open the ABCSettings Application ");
	    Response.Write("located in the bin directory of this web site ");
	    Response.Write("- this will automatically insert a trial license. ");
	    Response.Write("Then simply refresh this page to ensure that the license has been picked up.</b></p><hr />");
	}
%>
	<p>
		Using the links to the left you can add text, images and other graphics to your
		very own PDF document. You can choose to view the document in a browser window or
		download it to your computer for viewing. The actual document itself is dynamically
		generated and sent direct to your web browser - there are no files saved anywhere.</p>
	<p>
		In this example web site we limit the document to few pages to make it easy to see
		the changes you're making. However if you want to create larger documents it's only
		a few more lines of code!</p>
	<p>
		If you're on a slow connection - do remember that as you add more and more to your
		document it will get larger. This is particularly true when you add images. As the
		document gets larger it will take longer to refresh. This doesn't mean that ABCpdf
		is taking longer - just that it's taking longer to send the document down the line
		to your browser.</p>
	<p>
		Click twice on the page area in the frame to the left to define a rectangle.
		The coordinates below will be updated to reflect the position and dimensions of
		your area.
	</p>
	<p>
		Note that this feature may not be available for all browsers. However you can always
		enter text positions and dimensions in the text boxes below.
	</p>
</body>
</html>
