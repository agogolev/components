using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using WebSupergoo.ABCpdf8;

namespace ExampleSite {
	public class Global : System.Web.HttpApplication {
		protected void Session_Start(Object sender, EventArgs e) {
			try {
				Session["doc"] = new Doc();
			}
			catch {
			}
		}

		protected void Session_End(Object sender, EventArgs e) {
			try {
				if (Session["doc"] != null) {
					((Doc)Session["doc"]).Clear();
					Session["doc"] = null;
				}
			}
			catch {
			}
		}
	}
}

