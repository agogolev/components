<%@ Import Namespace="WebSupergoo.ABCpdf8" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Security.Principal" %>
<%@ Assembly Name="ABCpdf" %>
<%@ Page %>
<HTML>
	<HEAD>
		<title>ABCpdf Diagnostics</title>
		<link rel="stylesheet" href="mystyles.css">
	</HEAD>
	<body>
	<img src="images/logo.gif" width="272" height="120" alt="Logo" border="0" />
	<h1>ABCpdf Diagnostics</h1>
	<p>
<%
			
Response.Write("<table width=\"100%\">");
Response.Write("<tr><td>Current User:</td><td>&quot;" + WindowsIdentity.GetCurrent().Name + "&quot;</td></tr>");
Response.Write("<tr><td>Temp Directory:</td><td>&quot;" + Path.GetTempPath() + "&quot;</td></tr>");
Response.Write("<tr><td>&nbsp;</td><td>&nbsp;</td></tr>");

// try reading Arial font
try {
	FileStream fs = new FileStream("C:\\WINDOWS\\Fonts\\arial.ttf", FileMode.Open, FileAccess.Read, FileShare.Read);
	fs.Close();
	Response.Write("<tr><td>Read Arial Font:</td><td>OK</td></tr>");
}
catch (Exception ex) {
	Response.Write("<tr><td>Read Arial Font:</td><td>" + ex.Message + "</td></tr>");
}

// try write in temp directory I
try {
	string thePath = Path.GetTempPath() + "abcpdftest.txt";
	FileStream fs = new FileStream(thePath, FileMode.Create, FileAccess.ReadWrite, FileShare.None);
	fs.Close();
	File.Delete(thePath);
	Response.Write("<tr><td>Write Temp Directory:</td><td>OK</td></tr>");
}
catch (Exception ex) {
	Response.Write("<tr><td>Write Temp Directory:</td><td>" + ex.Message + "</td></tr>");
}

// try write in temp directory II
try {
	string thePath = Path.GetTempPath() + "ABCpdf\\abcpdftest.txt";
	FileStream fs = new FileStream(thePath, FileMode.Create, FileAccess.ReadWrite, FileShare.None);
	fs.Close();
	File.Delete(thePath);
	Response.Write("<tr><td>Write ABCpdf Temp Directory:</td><td>OK</td></tr>");
}
catch (Exception ex) {
	Response.Write("<tr><td>Write ABCpdf Temp Directory:</td><td>" + ex.Message + "</td></tr>");
}

string theLocal = Directory.GetParent(Path.GetTempPath()).Parent.Parent.FullName + "\\";

try {
	string theName = "Local Settings\\Temporary Internet Files";
	Response.Write("<tr><td>Write " + theName + ":</td><td>");
	string thePath = theLocal + theName + "\\abcpdftest.txt";
	FileStream fs = new FileStream(thePath, FileMode.Create, FileAccess.ReadWrite, FileShare.None);
	fs.Close();
	File.Delete(thePath);
	Response.Write("OK</td></tr>");
}
catch (Exception ex) {
	Response.Write(ex.Message + "</td></tr>");
}


try {
	string theName = "Local Settings\\History";
	Response.Write("<tr><td>Write " + theName + ":</td><td>");
	string thePath = theLocal + theName + "\\abcpdftest.txt";
	FileStream fs = new FileStream(thePath, FileMode.Create, FileAccess.ReadWrite, FileShare.None);
	fs.Close();
	File.Delete(thePath);
	Response.Write("OK</td></tr>");
}
catch (Exception ex) {
	Response.Write(ex.Message + "</td></tr>");
}


try {
	string theName = "Cookies";
	Response.Write("<tr><td>Write " + theName + ":</td><td>");
	string thePath = theLocal + theName + "\\abcpdftest.txt";
	FileStream fs = new FileStream(thePath, FileMode.Create, FileAccess.ReadWrite, FileShare.None);
	fs.Close();
	File.Delete(thePath);
	Response.Write("OK</td></tr>");
}
catch (Exception ex) {
	Response.Write(ex.Message + "</td></tr>");
}

try {
	string theName = "Local Settings\\Temporary Internet Files\\Content.IE5";
	Response.Write("<tr><td>Write " + theName + ":</td><td>");
	string thePath = theLocal + theName + "\\abcpdftest.txt";
	FileStream fs = new FileStream(thePath, FileMode.Create, FileAccess.ReadWrite, FileShare.None);
	fs.Close();
	File.Delete(thePath);
	Response.Write("OK</td></tr>");
}
catch (Exception ex) {
	Response.Write(ex.Message + "</td></tr>");
}

try {
	string theName = "Local Settings\\History\\History.IE5";
	Response.Write("<tr><td>Write " + theName + ":</td><td>");
	string thePath = theLocal + theName + "\\abcpdftest.txt";
	FileStream fs = new FileStream(thePath, FileMode.Create, FileAccess.ReadWrite, FileShare.None);
	fs.Close();
	File.Delete(thePath);
	Response.Write("OK</td></tr>");
}
catch (Exception ex) {
	Response.Write(ex.Message + "</td></tr>");
}


Response.Write("</table>");
	
%>
	</p>
</body>
</HTML>
