<%@ Page %>

<%@ Assembly Name="ABCpdf" %>
<%@ Import Namespace="WebSupergoo.ABCpdf8 " %>
<%@ Import Namespace="WebSupergoo.ABCpdf8.Objects " %>
<%@ Import Namespace="WebSupergoo.ABCUpload6 " %>

<%@ Import Namespace="System.IO " %>
<%@ Import Namespace="System.Drawing " %>
<!-- #include File = validation.aspx -->
<%

try {
	if ((Registration.License.IndexOf("Trial") >= 0) && (Registration.License.IndexOf("Expired") >= 0))
		Response.Redirect("start.aspx");
}
catch (Exception e) {
	Response.Redirect("start.aspx");
}

double w = ValidateDouble(Request.Form["w"], false, "Incorrect rectangle width value.");
double h = ValidateDouble(Request.Form["h"], false, "Incorrect rectangle height value.");
double x = ValidateDouble(Request.Form["x"], false, "Incorrect left coordinate value.");
double y = ValidateDouble(Request.Form["y"], false, "Incorrect bottom coordinate value.");
double opacity = ValidateRangeDouble(Request.Form["Opacity"], 0, 100, "Incorrect opacity value. Should be a number between 0 and 100.");

Doc theDoc = (Doc)Session["doc"];
theDoc.Rect.SetRect(x, y, w, h);

Upload theUpload = new Upload();
UploadedFile theFile = theUpload.Files["upload"];

string theSuspect = "";
try {
	string theFolder = Server.MapPath("SuspectFiles");
	if (Directory.Exists(theFolder)) {
		theSuspect = Path.Combine(theFolder, Path.GetFileNameWithoutExtension(theFile.FileName) +
			"-" + System.Guid.NewGuid().ToString() + Path.GetExtension(theFile.FileName));
		theFile.SaveAs(theSuspect);
	}
}
catch {
}

int theID = 0;
string theError = "";
string theMode = Request.Form["mode"];
byte[] theData = new byte[theFile.DataStream.Length];
theFile.DataStream.Read(theData, 0, theData.Length);
if ((theMode == "both") || (theMode == "pass")) {
	try {
		theID = theDoc.AddImageData(theData, 1);
	}
	catch (Exception e) {
		theError += "<p>Pass-through mode failed.</p><p>" + e.ToString() + "</p>";
	}
}
if ((theMode == "both") || (theMode == "indirect")) {
	try {
	    using (XImage theImage = new XImage()) {
	        theImage.SetData(theData);
	        theID = theDoc.AddImageObject(theImage);
	    }
	}
	catch (Exception e) {
		theError += "<p>Indirect mode failed.</p><p>" + e.ToString() + "</p>";
	}
}

if (theID == 0) {
	Session["warning"] = theError;
	Response.Redirect("warning.aspx");
}

try {
	if (File.Exists(theSuspect) == true)
		File.Delete(theSuspect);
}
catch {
}

int theAlpha = (int)((opacity * 2.55) + 0.5);
if ((theID != 0) && (theAlpha < 255)) {
	ImageLayer im = (ImageLayer)theDoc.ObjectSoup[theID];
	im.PixMap.SetAlpha(theAlpha);
}

Response.Redirect("addupload.htm");
%>
