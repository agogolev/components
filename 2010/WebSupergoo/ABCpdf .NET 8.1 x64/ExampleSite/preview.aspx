<%@ Page %>

<%@ Assembly Name="ABCpdf" %>
<%@ Import Namespace="WebSupergoo.ABCpdf8" %>
<!-- #include File = validation.aspx -->
<%
Doc	theDoc = (Doc)Session["doc"];

theDoc.Rendering.AntiAliasImages = true;
theDoc.Rendering.AntiAliasPolygons = true;
theDoc.Rendering.AntiAliasText = true;
theDoc.Rendering.AntiAliasScene = false;
theDoc.Rendering.DrawAnnotations = true;

theDoc.Rendering.ColorSpace = XRendering.ColorSpaceType.Rgb;
theDoc.Rendering.DotsPerInch = 18.0;
theDoc.Rect.String = theDoc.MediaBox.String;

string filename = "dummy.png";

try {
	if (theDoc.PageCount <= 0){
		theDoc.Page = theDoc.AddPage();
		theDoc.Rect.String = theDoc.MediaBox.String;
	}
	byte[] theData = theDoc.Rendering.GetData(filename);
	
	Response.Clear();
	Response.Expires = -1000;
	Response.ContentType = "image/png";
	Response.AddHeader("content-length", theData.Length.ToString());
	Response.AddHeader("content-disposition", "inline; filename =" + filename);
	Response.BinaryWrite(theData);
}
catch {
}
Response.Flush();
%>
