<%@ Page ValidateRequest="false" %>

<%@ Assembly Name="ABCpdf" %>
<%@ Import Namespace="WebSupergoo.ABCpdf8" %>
<!-- #include File = validation.aspx -->
<%
Doc	theDoc = (Doc)Session["doc"];

// Save current state of TextStyle in a string.
string originalTextStyle = theDoc.TextStyle.ToString();

double w = ValidateDouble(Request.Form["w"], false, "Incorrect rectangle width");
double h = ValidateDouble(Request.Form["h"], false, "Incorrect rectangle height");
double x = ValidateDouble(Request.Form["x"], false, "Incorrect Left coordinate");
double y = ValidateDouble(Request.Form["y"], false, "Incorrect Bottom coordinate");

string theText = Request.Form["text"];
string theFont = Request.Form["font"];
int theSize = ValidateInt(Request.Form["FontSize"], true, "Incorrect font size");
string theColor = Request.Form["Color"];
double theHPos = Convert.ToDouble(Request.Form["HPos"], System.Globalization.CultureInfo.InvariantCulture);
double theVPos = Convert.ToDouble(Request.Form["VPos"], System.Globalization.CultureInfo.InvariantCulture);

theDoc.Rect.SetRect(x, y, w, h);
theDoc.Font = theDoc.AddFont(theFont);
theDoc.FontSize = theSize;
theDoc.Color.String = theColor;
theDoc.HPos = theHPos;
theDoc.VPos = theVPos;
int opacity = (int)((ValidateRangeDouble(Request.Form["Opacity"], 0, 100, "Incorrect opacity value. Should be a number between 0 and 100.") * 2.55) + 0.5);
theDoc.Color.Alpha = opacity;

theDoc.TextStyle.CharSpacing = ValidateDouble(Request.Form["CharSpacing"], false, "Incorrect character spacing value.");
theDoc.TextStyle.WordSpacing = ValidateDouble(Request.Form["WordSpacing"], false, "Incorrect word spacing value.");
theDoc.TextStyle.LineSpacing = ValidateDouble(Request.Form["LineSpacing"], false, "Incorrect line spacing value.");
theDoc.TextStyle.ParaSpacing = ValidateDouble(Request.Form["ParaSpacing"], false, "Incorrect paragraph spacing value.");
theDoc.TextStyle.Indent = ValidateDouble(Request.Form["Indent"], true, "Incorrect paragraph indent value.");
theDoc.TextStyle.Outline = ValidateDouble(Request.Form["Outline"], false, "Incorrect character outline value.");

theDoc.TextStyle.Bold = Request.Form["Bold"] == "on";
theDoc.TextStyle.Italic = Request.Form["Italic"] == "on";
theDoc.TextStyle.Strike = Request.Form["Strike"] == "on";
theDoc.TextStyle.Strike2 = Request.Form["Strike2"] == "on";
theDoc.TextStyle.Underline = Request.Form["Underline"] == "on";
theDoc.TextStyle.Justification = Request.Form["Justification"] == "on" ? 1 : 0;

if (Request.Form["UseHtml"] == "on")
	theDoc.AddHtml(theText);
else
	theDoc.AddText(theText);

// Restore the original TextStyle state.
theDoc.TextStyle.String = originalTextStyle;

Response.Redirect("addtext.htm");
%>
