<%@ Page %>

<%@ Assembly Name="ABCpdf" %>
<%@ Import Namespace="WebSupergoo.ABCpdf8 " %>
<%@ Import Namespace="WebSupergoo.ABCpdf8.Objects " %>
<%@ Import Namespace="System.IO " %>
<%@ Import Namespace="System.Drawing " %>
<!-- #include File = validation.aspx -->
<%
Doc	theDoc = (Doc)Session["doc"];
double w = ValidateDouble(Request.Form["w"], false, "Incorrect rectangle width value.");
double h = ValidateDouble(Request.Form["h"], false, "Incorrect rectangle height value.");
double x = ValidateDouble(Request.Form["x"], false, "Incorrect left coordinate value.");
double y = ValidateDouble(Request.Form["y"], false, "Incorrect bottom coordinate value.");
double opacity = ValidateRangeDouble(Request.Form["Opacity"], 0, 100, "Incorrect opacity value. Should be a number between 0 and 100.");

string thePath = "images/" + Request.Form["image"];

theDoc.Rect.SetRect(x, y, w, h);

int theID = 0;
try {
	Bitmap bm = new Bitmap(Server.MapPath(thePath));
	theID = theDoc.AddImageBitmap(bm, true);
	bm.Dispose();
}
catch (Exception e) {
    Session["warning"] = "<p>Unable to add image. </p><p>" + e.ToString() + "</p>";
    Response.Redirect("warning.aspx");
}

int theAlpha = (int)((opacity * 2.55) + 0.5);
if ((theID != 0) && (theAlpha < 255)) {
	ImageLayer im = (ImageLayer)theDoc.ObjectSoup[theID];
	im.PixMap.SetAlpha(theAlpha);
}

Response.Redirect("addimage.htm");

%>
