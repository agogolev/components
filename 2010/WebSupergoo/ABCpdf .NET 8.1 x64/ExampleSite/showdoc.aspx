<%@ Page %>
<%@ Import Namespace="System.Web" %>
<%@ Assembly Name="ABCpdf" %>
<%@ Import Namespace="WebSupergoo.ABCpdf8" %>
<%

Response.ClearHeaders();
Response.ClearContent();
Response.Expires = -1000;
Doc theDoc = (Doc)Session["doc"];

// Stamp the PDF with the AddHtml function.
if (theDoc.GetInfo(theDoc.Root, "/ABCpdfStamp") == "") {
	theDoc.Rect.String = theDoc.MediaBox.String;
	theDoc.Rect.Inset(18, 18);
	theDoc.FontSize = 18;
	theDoc.Font = theDoc.AddFont("Helvetica");
	string stamp = "<p align=\"right\">HTML to PDF conversion by ABCpdf .NET <a href=\"http://www.abcpdf.com/\">http://www.abcpdf.com/</a></p>";
	int id = theDoc.AddHtml(stamp);
	theDoc.SetInfo(theDoc.Root, "/ABCpdfStamp:Num", id.ToString());
}

byte[] theData = theDoc.GetData();
Response.ContentType = "application/pdf";
Response.AddHeader("content-length", theData.Length.ToString());
if (Request.QueryString["attachment"] != null)
	Response.AddHeader("content-disposition", "attachment; filename=ExampleSite.pdf");
else
	Response.AddHeader("content-disposition", "inline; filename=ExampleSite.pdf");
Response.BinaryWrite(theData);
HttpContext.Current.ApplicationInstance.CompleteRequest();

%>
