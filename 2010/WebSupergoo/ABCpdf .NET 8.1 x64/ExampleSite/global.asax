<%@ Assembly Name="ABCpdf" %>
<%@ Application Language="C#" %>
<%@ Import Namespace="WebSupergoo.ABCpdf8" %>

<script runat="server">

		protected void Session_Start(Object sender, EventArgs e) {
			try {
				Session["doc"] = new Doc();
			}
			catch {
			}
		}

		protected void Session_End(Object sender, EventArgs e)
		{
			try {
				if (Session["doc"] != null) {
					((Doc)Session["doc"]).Clear();
					Session["doc"] = null;
				}
			}
			catch {
			}
		}

</script>
