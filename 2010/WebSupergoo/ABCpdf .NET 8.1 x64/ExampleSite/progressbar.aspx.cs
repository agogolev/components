using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WebSupergoo.ABCUpload6;


namespace ABCUpload
{
    public partial class _Default : System.Web.UI.Page
    {
        public string theID;
        public string theMins;
        public string theSecs;
        public string theMeta;
        public string thePercent;
        public string theKbps;
        public string theKbdone;
        public string theKbtotal;
        public string theNote;
        public string theFileName;

        public void UpdateProgressBar()
        {
            Progress theProgress = new Progress(Request.QueryString["ProgressID"]);
            theID = theProgress.ID.ToString();
            theMins = ((int)theProgress.SecondsLeft / 60).ToString();
            theSecs = ((int)theProgress.SecondsLeft % 60).ToString();
            theMeta = "<meta http-equiv=\"refresh\" content=\"2,progressbar.aspx?ProgressID=" + theID + "\">";
            thePercent = theProgress.PercentDone.ToString();
            theKbps = Math.Round(theProgress.BytesPerSecondCurrent / 1024, 1).ToString();
            theKbdone = Math.Round((double)theProgress.BytesDone / 1024, 1).ToString();
            theKbtotal = Math.Round((double)theProgress.BytesTotal / 1024, 1).ToString();
            theNote = theProgress.Note;
            theFileName = theProgress.FileName;
            if (theProgress.Finished) theMeta = "";
        }
    }
}
