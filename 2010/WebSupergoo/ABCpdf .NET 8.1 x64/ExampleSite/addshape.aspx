<%@ Page %>

<%@ Assembly Name="ABCpdf" %>
<%@ Import Namespace="WebSupergoo.ABCpdf8" %>
<!-- #include File = validation.aspx -->
<%
Doc	theDoc = (Doc)Session["doc"];
double w = ValidateDouble(Request.Form["w"], false, "Incorrect rectangle width value.");
double h = ValidateDouble(Request.Form["h"], false, "Incorrect rectangle height value.");
double x = ValidateDouble(Request.Form["x"], false, "Incorrect left coordinate value.");
double y = ValidateDouble(Request.Form["y"], false, "Incorrect bottom coordinate value.");

int theWidth = ValidateInt(Request.Form["Width"], true, "Incorrect line width value.");
string theColor = Request.Form["Color"];
string theOp = Request.Form["op"];

double opacity = ValidateRangeDouble(Request.Form["Opacity"], 0, 100, "Incorrect opacity value. Should be a number between 0 and 100.");
int theAlpha = (int)((opacity * 2.55) + 0.5);

theDoc.Rect.SetRect(x, y, w, h);
theDoc.Width = theWidth;
theDoc.Color.String = theColor;

theDoc.Color.Alpha = theAlpha;

switch (theOp) {
	case "Rectangle":
		double rx = 0;
		double ry = 0;
		if (Request.Form["RoundedCorners"] == "on") {
			rx = ValidateDouble(Request.Form["HorizontalRadius"], false, "Incorrect horizontal radius value.");
			ry = ValidateDouble(Request.Form["VerticalRadius"], false, "Incorrect vertical radius value.");
		}
		if (Request.Form["FillRect"] == "on")
			theDoc.FillRect(rx, ry);
		else
			theDoc.FrameRect(rx, ry);
		break;
	case "Pie":
		double startAngle = ValidateDouble(Request.Form["StartAngle"], false, "Incorrect start angle value.");
		double endAngle = ValidateDouble(Request.Form["EndAngle"], false, "Incorrect end angle value.");
		theDoc.AddPie(startAngle, endAngle, Request.Form["FillPie"] == "on");
		break;
	case "Line":
		string direction = Convert.ToString(Request.Form["LineDirection"]);
		if (direction == "tlbr")
			theDoc.AddLine(x, y + h, x + w, y);
		else
			theDoc.AddLine(x, y, x + w, y + h);
		break;
	case "Oval":
		theDoc.AddOval(Request.Form["FillOval"] == "on");
		break;
}
	
Response.Redirect("addshape.htm");

%>
