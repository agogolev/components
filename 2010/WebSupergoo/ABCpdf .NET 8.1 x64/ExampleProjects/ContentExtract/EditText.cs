using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using WebSupergoo.ABCpdf8;
using WebSupergoo.ABCpdf8.Objects;

namespace WebSupergoo.ContentExtract {
	public partial class EditText : Form {
		public EditText(MainForm parent, Doc doc, ObjectExtractor obj) {
			InitializeComponent();
			_parent = parent;
			_doc = doc;
			_obj = obj;
		}

		private void EditText_Load(object sender, EventArgs e) {
			if(!_obj.IsAscii) {
				const string message = "This object appears to contain data rather than text.\r\n\r\nWould you like to ASCII 85 encode it for editing?";
				if(MessageBox.Show(message, "Warning", MessageBoxButtons.YesNo) == DialogResult.Yes) {
					StreamObject so = _obj.Object as StreamObject;
					if(so != null)
						so.CompressAscii85();
				}
			}
			_text = _obj.GetValue(_doc);
			textBox1.Text = _text;
		}

		private MainForm _parent;
		private Doc _doc;
		private ObjectExtractor _obj;
		string _text;

		private void ok_Click(object sender, EventArgs e) {
			if (textBox1.Text != _text) {
				_obj.SetValue(_doc, textBox1.Text);
				_parent.UpdateContent(_obj);
				_parent.UpdateImage(_obj, true);
				_parent.Dirty = true;
			}
			Close();
		}

		private void cancel_Click(object sender, EventArgs e) {
			Close();
		}
	}
}