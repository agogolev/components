// ===========================================================================
//	�2007 WebSupergoo. All rights reserved.
// ===========================================================================

using System;
using System.Drawing;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.IO;

using System.Drawing.Imaging;
using WebSupergoo.ABCpdf8;
using WebSupergoo.ABCpdf8.Objects;

namespace WebSupergoo.ContentExtract
{
	/// <summary>
	/// Summary description for MainForm.
	/// </summary>
	public class MainForm : System.Windows.Forms.Form {
		private sealed class ImageLoadHandler {
			private const int _maxThreadCount = 2;
			private MainForm _form;
			private bool _disposed;
			private int _pendingCount;
			private List<ObjectExtractor> _waitingList;

			public ImageLoadHandler(MainForm form) {
				_form = form;
				_waitingList = new List<ObjectExtractor>();
			}

			public void DisposeDoc(Doc doc) {
				if(doc!=null && _pendingCount<=0)
					doc.Dispose();
				_disposed = true;
			}

			public void BeginLoadImage(ObjectExtractor extractor) {
				if(_pendingCount>=_maxThreadCount) {
					_waitingList.Add(extractor);
					return;
				}
				extractor.BeginLoadImage(ImageLoadComplete);
				++_pendingCount;
			}

			private void ImageLoadComplete(ObjectExtractor extractor, ImageAndState imageState)
			{
				if(!_form.InvokeRequired) {
					ImageLoadCompleteOnUIThread(extractor, imageState);
					return;
				}
				IAsyncResult result = _form.BeginInvoke(
					new ObjectExtractor.ImageLoadCompleteDelegate(
					ImageLoadCompleteOnUIThread), extractor, imageState);
			}

			private void ImageLoadCompleteOnUIThread(ObjectExtractor extractor, ImageAndState imageState)
			{
				--_pendingCount;
				if(_disposed) {
					if(imageState.Image!=null)
						imageState.Image.Dispose();
					if(_pendingCount<=0)
						extractor.Object.Doc.Dispose();
					return;
				}
				_form.ImageLoadComplete(extractor, imageState);
				if(_waitingList.Count>0) {
					extractor = _waitingList[0];
					_waitingList.RemoveAt(0);
					try {
						extractor.BeginLoadImage(ImageLoadComplete);
					} catch {
						_form.ImageLoadComplete(extractor,
							new ImageAndState(null, ImageState.Error));
					}
					++_pendingCount;
				}
			}
		}

		private Doc _doc;
		private string _filePath;
		private string _tempPath;
		private bool _dirty;
		private ObjectExtractor _selectedExtractor;
		private ImageLoadHandler _imageLoadHandler;
		private ObjectExtractor _cachedExtractor;
		private ObjectExtractor[] _cachedImageExtractorList;
		private ImageAndState[] _cachedImageList;
		private bool _imageIsCached;
		private TabPage _preferredPage;
		private bool _internalPageChanging;
		private Find _findDialog;
		private ListView lst;
		private OpenFileDialog dlgOpenFile;
		private FontDialog dlgSelectFont;
		private ContextMenuStrip cmenuTextBox;
		private SplitContainer split;
		private ColumnHeader cheaderID;
		private ColumnHeader cheaderType;
		private ColumnHeader cheaderName;
		private ColumnHeader cheaderInfo1;
		private ColumnHeader cheaderInfo2;
		private TabControl tabInfo;
		private TabPage pageAtom;
		private RichTextBox txtAtom;
		private TabPage pageContent;
		private RichTextBox txtContent;
		private TabPage pageImage;
		private PictureBox pict;
		private ToolStripMenuItem mitemFont;
		private MenuStrip mstrip;
		private ToolStripMenuItem mitemFile;
		private ToolStripMenuItem mitemOpen;
		private ToolStripSeparator mitemSep1;
		private ToolStripMenuItem mitemExit;
		private ImageList imglist;
		private Panel pnlPicture;
		private ToolStripMenuItem mitemWordWrap;
		private ContextMenuStrip cmenuIndirectObject;
		private ToolStripMenuItem mitemDecompress;
		private ToolStripMenuItem mItemMakeAscii85;
		private ToolStripMenuItem mItemCompressFlate;
		private ToolStripMenuItem mItemCompressJpeg75;
		private ToolStripMenuItem mItemCompressJpeg50;
		private ToolStripMenuItem mItemCompressJpeg25;
		private ToolStripMenuItem mitemSaveAs;
		private ToolStripMenuItem mitemSave;
		private SaveFileDialog dlgSaveFile;
		private ToolStripMenuItem mItemEdit;
		private ToolStripMenuItem mItemRefresh;
		private ToolStripMenuItem mItemMakeAsciiHex;
		private ToolStripMenuItem editToolStripMenuItem;
		private ToolStripMenuItem findToolStripMenuItem;
		private ToolStripMenuItem selectAllToolStripMenuItem;
		private ToolStripMenuItem viewToolStripMenuItem;
		private ToolStripMenuItem showAllToolStripMenuItem;
		private ToolStripMenuItem selectedToolStripMenuItem;
		private ToolStripMenuItem unselectedToolStripMenuItem;
		private ToolStripMenuItem compactToolStripMenuItem;
		private IContainer components;

		public MainForm() {
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			lst.ListViewItemSorter = new ListViewItemComparer();
			_cachedImageExtractorList = new ObjectExtractor[10];
			_cachedImageList = new ImageAndState[10];
			_preferredPage = pageImage;
			Clear();
			string[] args = Environment.GetCommandLineArgs();
			if (args.Length > 1) {
				try {
					Open(args[1]);
				}
				catch (Exception exc) {
					MessageBox.Show(exc.Message);
				}
			}
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing) {
			if(disposing) {
				Clear();
				if(components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
			this.dlgOpenFile = new System.Windows.Forms.OpenFileDialog();
			this.cmenuTextBox = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.mitemWordWrap = new System.Windows.Forms.ToolStripMenuItem();
			this.mitemFont = new System.Windows.Forms.ToolStripMenuItem();
			this.dlgSelectFont = new System.Windows.Forms.FontDialog();
			this.split = new System.Windows.Forms.SplitContainer();
			this.lst = new System.Windows.Forms.ListView();
			this.cheaderID = new System.Windows.Forms.ColumnHeader();
			this.cheaderType = new System.Windows.Forms.ColumnHeader();
			this.cheaderName = new System.Windows.Forms.ColumnHeader();
			this.cheaderInfo1 = new System.Windows.Forms.ColumnHeader();
			this.cheaderInfo2 = new System.Windows.Forms.ColumnHeader();
			this.cmenuIndirectObject = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.mitemDecompress = new System.Windows.Forms.ToolStripMenuItem();
			this.mItemMakeAscii85 = new System.Windows.Forms.ToolStripMenuItem();
			this.mItemMakeAsciiHex = new System.Windows.Forms.ToolStripMenuItem();
			this.mItemCompressFlate = new System.Windows.Forms.ToolStripMenuItem();
			this.mItemCompressJpeg75 = new System.Windows.Forms.ToolStripMenuItem();
			this.mItemCompressJpeg50 = new System.Windows.Forms.ToolStripMenuItem();
			this.mItemCompressJpeg25 = new System.Windows.Forms.ToolStripMenuItem();
			this.mItemRefresh = new System.Windows.Forms.ToolStripMenuItem();
			this.mItemEdit = new System.Windows.Forms.ToolStripMenuItem();
			this.tabInfo = new System.Windows.Forms.TabControl();
			this.pageImage = new System.Windows.Forms.TabPage();
			this.pnlPicture = new System.Windows.Forms.Panel();
			this.pict = new System.Windows.Forms.PictureBox();
			this.pageAtom = new System.Windows.Forms.TabPage();
			this.txtAtom = new System.Windows.Forms.RichTextBox();
			this.pageContent = new System.Windows.Forms.TabPage();
			this.txtContent = new System.Windows.Forms.RichTextBox();
			this.imglist = new System.Windows.Forms.ImageList(this.components);
			this.mstrip = new System.Windows.Forms.MenuStrip();
			this.mitemFile = new System.Windows.Forms.ToolStripMenuItem();
			this.mitemOpen = new System.Windows.Forms.ToolStripMenuItem();
			this.mitemSave = new System.Windows.Forms.ToolStripMenuItem();
			this.mitemSaveAs = new System.Windows.Forms.ToolStripMenuItem();
			this.mitemSep1 = new System.Windows.Forms.ToolStripSeparator();
			this.mitemExit = new System.Windows.Forms.ToolStripMenuItem();
			this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.findToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.selectAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.showAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.selectedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.unselectedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.dlgSaveFile = new System.Windows.Forms.SaveFileDialog();
			this.compactToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.cmenuTextBox.SuspendLayout();
			this.split.Panel1.SuspendLayout();
			this.split.Panel2.SuspendLayout();
			this.split.SuspendLayout();
			this.cmenuIndirectObject.SuspendLayout();
			this.tabInfo.SuspendLayout();
			this.pageImage.SuspendLayout();
			this.pnlPicture.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pict)).BeginInit();
			this.pageAtom.SuspendLayout();
			this.pageContent.SuspendLayout();
			this.mstrip.SuspendLayout();
			this.SuspendLayout();
			// 
			// dlgOpenFile
			// 
			this.dlgOpenFile.DefaultExt = "pdf";
			this.dlgOpenFile.Filter = "PDF Document (*.pdf)|*.pdf";
			this.dlgOpenFile.Title = "Open a PDF Document";
			// 
			// cmenuTextBox
			// 
			this.cmenuTextBox.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitemWordWrap,
            this.mitemFont});
			this.cmenuTextBox.Name = "textBoxContextMenu";
			this.cmenuTextBox.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
			this.cmenuTextBox.ShowCheckMargin = true;
			this.cmenuTextBox.ShowImageMargin = false;
			this.cmenuTextBox.Size = new System.Drawing.Size(135, 48);
			this.cmenuTextBox.Text = "cmenuTextBox";
			this.cmenuTextBox.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.cmenuTextBox_ItemClicked);
			this.cmenuTextBox.Opening += new System.ComponentModel.CancelEventHandler(this.cmenuTextBox_Opening);
			// 
			// mitemWordWrap
			// 
			this.mitemWordWrap.Name = "mitemWordWrap";
			this.mitemWordWrap.Size = new System.Drawing.Size(134, 22);
			this.mitemWordWrap.Text = "&Word Wrap";
			// 
			// mitemFont
			// 
			this.mitemFont.Name = "mitemFont";
			this.mitemFont.Size = new System.Drawing.Size(134, 22);
			this.mitemFont.Text = "&Font...";
			// 
			// split
			// 
			this.split.Dock = System.Windows.Forms.DockStyle.Fill;
			this.split.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
			this.split.Location = new System.Drawing.Point(0, 24);
			this.split.Name = "split";
			// 
			// split.Panel1
			// 
			this.split.Panel1.Controls.Add(this.lst);
			// 
			// split.Panel2
			// 
			this.split.Panel2.Controls.Add(this.tabInfo);
			this.split.Size = new System.Drawing.Size(837, 542);
			this.split.SplitterDistance = 455;
			this.split.TabIndex = 3;
			// 
			// lst
			// 
			this.lst.AllowDrop = true;
			this.lst.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.cheaderID,
            this.cheaderType,
            this.cheaderName,
            this.cheaderInfo1,
            this.cheaderInfo2});
			this.lst.ContextMenuStrip = this.cmenuIndirectObject;
			this.lst.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lst.FullRowSelect = true;
			this.lst.HideSelection = false;
			this.lst.Location = new System.Drawing.Point(0, 0);
			this.lst.Name = "lst";
			this.lst.Size = new System.Drawing.Size(455, 542);
			this.lst.TabIndex = 0;
			this.lst.UseCompatibleStateImageBehavior = false;
			this.lst.View = System.Windows.Forms.View.Details;
			this.lst.SelectedIndexChanged += new System.EventHandler(this.lst_SelectedIndexChanged);
			this.lst.DoubleClick += new System.EventHandler(this.lst_DoubleClick);
			this.lst.DragDrop += new System.Windows.Forms.DragEventHandler(this.lst_DragDrop);
			this.lst.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.lst_ColumnClick);
			this.lst.DragOver += new System.Windows.Forms.DragEventHandler(this.lst_DragOver);
			// 
			// cheaderID
			// 
			this.cheaderID.Text = "ID";
			// 
			// cheaderType
			// 
			this.cheaderType.Text = "Type";
			this.cheaderType.Width = 86;
			// 
			// cheaderName
			// 
			this.cheaderName.Text = "Name";
			this.cheaderName.Width = 117;
			// 
			// cheaderInfo1
			// 
			this.cheaderInfo1.Text = "Info 1";
			this.cheaderInfo1.Width = 77;
			// 
			// cheaderInfo2
			// 
			this.cheaderInfo2.Text = "Info 2";
			this.cheaderInfo2.Width = 300;
			// 
			// cmenuIndirectObject
			// 
			this.cmenuIndirectObject.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitemDecompress,
            this.mItemMakeAscii85,
            this.mItemMakeAsciiHex,
            this.mItemCompressFlate,
            this.mItemCompressJpeg75,
            this.mItemCompressJpeg50,
            this.mItemCompressJpeg25,
            this.mItemRefresh,
            this.mItemEdit});
			this.cmenuIndirectObject.Name = "textBoxContextMenu";
			this.cmenuIndirectObject.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
			this.cmenuIndirectObject.ShowCheckMargin = true;
			this.cmenuIndirectObject.ShowImageMargin = false;
			this.cmenuIndirectObject.Size = new System.Drawing.Size(245, 202);
			this.cmenuIndirectObject.Text = "cmenuIndirectObject";
			// 
			// mitemDecompress
			// 
			this.mitemDecompress.Name = "mitemDecompress";
			this.mitemDecompress.Size = new System.Drawing.Size(244, 22);
			this.mitemDecompress.Text = "&Decompress";
			this.mitemDecompress.Click += new System.EventHandler(this.mitemDecompress_Click);
			// 
			// mItemMakeAscii85
			// 
			this.mItemMakeAscii85.Name = "mItemMakeAscii85";
			this.mItemMakeAscii85.Size = new System.Drawing.Size(244, 22);
			this.mItemMakeAscii85.Text = "&Make ASCII85";
			this.mItemMakeAscii85.Click += new System.EventHandler(this.mItemMakeAscii_Click);
			// 
			// mItemMakeAsciiHex
			// 
			this.mItemMakeAsciiHex.Name = "mItemMakeAsciiHex";
			this.mItemMakeAsciiHex.Size = new System.Drawing.Size(244, 22);
			this.mItemMakeAsciiHex.Text = "&Make ASCII Hex";
			this.mItemMakeAsciiHex.Click += new System.EventHandler(this.mItemMakeAsciiHex_Click);
			// 
			// mItemCompressFlate
			// 
			this.mItemCompressFlate.Name = "mItemCompressFlate";
			this.mItemCompressFlate.Size = new System.Drawing.Size(244, 22);
			this.mItemCompressFlate.Text = "Compress Flate";
			this.mItemCompressFlate.Click += new System.EventHandler(this.mItemCompressFlate_Click);
			// 
			// mItemCompressJpeg75
			// 
			this.mItemCompressJpeg75.Name = "mItemCompressJpeg75";
			this.mItemCompressJpeg75.Size = new System.Drawing.Size(244, 22);
			this.mItemCompressJpeg75.Text = "Compress JPEG High Quality";
			this.mItemCompressJpeg75.Click += new System.EventHandler(this.mItemCompressJpeg75_Click);
			// 
			// mItemCompressJpeg50
			// 
			this.mItemCompressJpeg50.Name = "mItemCompressJpeg50";
			this.mItemCompressJpeg50.Size = new System.Drawing.Size(244, 22);
			this.mItemCompressJpeg50.Text = "Compress JPEG Medium Quality";
			this.mItemCompressJpeg50.Click += new System.EventHandler(this.mItemCompressJpeg50_Click);
			// 
			// mItemCompressJpeg25
			// 
			this.mItemCompressJpeg25.Name = "mItemCompressJpeg25";
			this.mItemCompressJpeg25.Size = new System.Drawing.Size(244, 22);
			this.mItemCompressJpeg25.Text = "Compress JPEG Low Quality";
			this.mItemCompressJpeg25.Click += new System.EventHandler(this.mItemCompressJpeg25_Click);
			// 
			// mItemRefresh
			// 
			this.mItemRefresh.Name = "mItemRefresh";
			this.mItemRefresh.Size = new System.Drawing.Size(244, 22);
			this.mItemRefresh.Text = "Refresh";
			this.mItemRefresh.Click += new System.EventHandler(this.mItemRefresh_Click);
			// 
			// mItemEdit
			// 
			this.mItemEdit.Name = "mItemEdit";
			this.mItemEdit.Size = new System.Drawing.Size(244, 22);
			this.mItemEdit.Text = "Edit Item...";
			this.mItemEdit.Click += new System.EventHandler(this.mItemEdit_Click);
			// 
			// tabInfo
			// 
			this.tabInfo.Controls.Add(this.pageImage);
			this.tabInfo.Controls.Add(this.pageAtom);
			this.tabInfo.Controls.Add(this.pageContent);
			this.tabInfo.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabInfo.ImageList = this.imglist;
			this.tabInfo.Location = new System.Drawing.Point(0, 0);
			this.tabInfo.Name = "tabInfo";
			this.tabInfo.SelectedIndex = 0;
			this.tabInfo.Size = new System.Drawing.Size(378, 542);
			this.tabInfo.TabIndex = 2;
			this.tabInfo.SelectedIndexChanged += new System.EventHandler(this.tabInfo_SelectedIndexChanged);
			// 
			// pageImage
			// 
			this.pageImage.Controls.Add(this.pnlPicture);
			this.pageImage.ImageIndex = 0;
			this.pageImage.Location = new System.Drawing.Point(4, 23);
			this.pageImage.Name = "pageImage";
			this.pageImage.Size = new System.Drawing.Size(370, 515);
			this.pageImage.TabIndex = 0;
			this.pageImage.Text = "Image";
			this.pageImage.UseVisualStyleBackColor = true;
			// 
			// pnlPicture
			// 
			this.pnlPicture.AutoScroll = true;
			this.pnlPicture.BackColor = System.Drawing.SystemColors.Window;
			this.pnlPicture.Controls.Add(this.pict);
			this.pnlPicture.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pnlPicture.Location = new System.Drawing.Point(0, 0);
			this.pnlPicture.Margin = new System.Windows.Forms.Padding(0);
			this.pnlPicture.Name = "pnlPicture";
			this.pnlPicture.Size = new System.Drawing.Size(370, 515);
			this.pnlPicture.TabIndex = 1;
			this.pnlPicture.Click += new System.EventHandler(this.pict_Click);
			// 
			// pict
			// 
			this.pict.Location = new System.Drawing.Point(0, 0);
			this.pict.Name = "pict";
			this.pict.Size = new System.Drawing.Size(120, 272);
			this.pict.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pict.TabIndex = 0;
			this.pict.TabStop = false;
			this.pict.Click += new System.EventHandler(this.pict_Click);
			// 
			// pageAtom
			// 
			this.pageAtom.Controls.Add(this.txtAtom);
			this.pageAtom.ImageIndex = 0;
			this.pageAtom.Location = new System.Drawing.Point(4, 23);
			this.pageAtom.Name = "pageAtom";
			this.pageAtom.Size = new System.Drawing.Size(370, 515);
			this.pageAtom.TabIndex = 1;
			this.pageAtom.Text = "Atom";
			this.pageAtom.UseVisualStyleBackColor = true;
			// 
			// txtAtom
			// 
			this.txtAtom.ContextMenuStrip = this.cmenuTextBox;
			this.txtAtom.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtAtom.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtAtom.Location = new System.Drawing.Point(0, 0);
			this.txtAtom.Name = "txtAtom";
			this.txtAtom.ReadOnly = true;
			this.txtAtom.Size = new System.Drawing.Size(370, 515);
			this.txtAtom.TabIndex = 0;
			this.txtAtom.Text = "";
			this.txtAtom.WordWrap = false;
			// 
			// pageContent
			// 
			this.pageContent.Controls.Add(this.txtContent);
			this.pageContent.ImageIndex = 0;
			this.pageContent.Location = new System.Drawing.Point(4, 23);
			this.pageContent.Name = "pageContent";
			this.pageContent.Size = new System.Drawing.Size(370, 515);
			this.pageContent.TabIndex = 2;
			this.pageContent.Text = "Stream Content";
			this.pageContent.UseVisualStyleBackColor = true;
			// 
			// txtContent
			// 
			this.txtContent.ContextMenuStrip = this.cmenuTextBox;
			this.txtContent.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtContent.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtContent.Location = new System.Drawing.Point(0, 0);
			this.txtContent.Name = "txtContent";
			this.txtContent.ReadOnly = true;
			this.txtContent.Size = new System.Drawing.Size(370, 515);
			this.txtContent.TabIndex = 0;
			this.txtContent.Text = "";
			this.txtContent.WordWrap = false;
			// 
			// imglist
			// 
			this.imglist.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imglist.ImageStream")));
			this.imglist.TransparentColor = System.Drawing.Color.Transparent;
			this.imglist.Images.SetKeyName(0, "cross.ico");
			this.imglist.Images.SetKeyName(1, "tick.ico");
			// 
			// mstrip
			// 
			this.mstrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitemFile,
            this.editToolStripMenuItem,
            this.viewToolStripMenuItem});
			this.mstrip.Location = new System.Drawing.Point(0, 0);
			this.mstrip.Name = "mstrip";
			this.mstrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
			this.mstrip.Size = new System.Drawing.Size(837, 24);
			this.mstrip.TabIndex = 4;
			this.mstrip.Text = "mstrip";
			// 
			// mitemFile
			// 
			this.mitemFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitemOpen,
            this.mitemSave,
            this.mitemSaveAs,
            this.mitemSep1,
            this.mitemExit});
			this.mitemFile.Name = "mitemFile";
			this.mitemFile.Size = new System.Drawing.Size(37, 20);
			this.mitemFile.Text = "&File";
			// 
			// mitemOpen
			// 
			this.mitemOpen.Name = "mitemOpen";
			this.mitemOpen.Size = new System.Drawing.Size(152, 22);
			this.mitemOpen.Text = "&Open...";
			this.mitemOpen.Click += new System.EventHandler(this.mitemOpen_Click);
			// 
			// mitemSave
			// 
			this.mitemSave.Name = "mitemSave";
			this.mitemSave.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
			this.mitemSave.Size = new System.Drawing.Size(152, 22);
			this.mitemSave.Text = "&Save";
			this.mitemSave.Click += new System.EventHandler(this.mitemSave_Click);
			// 
			// mitemSaveAs
			// 
			this.mitemSaveAs.Name = "mitemSaveAs";
			this.mitemSaveAs.Size = new System.Drawing.Size(152, 22);
			this.mitemSaveAs.Text = "&Save As...";
			this.mitemSaveAs.Click += new System.EventHandler(this.mitemSaveAs_Click);
			// 
			// mitemSep1
			// 
			this.mitemSep1.Name = "mitemSep1";
			this.mitemSep1.Size = new System.Drawing.Size(149, 6);
			// 
			// mitemExit
			// 
			this.mitemExit.Name = "mitemExit";
			this.mitemExit.Size = new System.Drawing.Size(152, 22);
			this.mitemExit.Text = "E&xit";
			this.mitemExit.Click += new System.EventHandler(this.mitemExit_Click);
			// 
			// editToolStripMenuItem
			// 
			this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.findToolStripMenuItem,
            this.selectAllToolStripMenuItem,
            this.compactToolStripMenuItem});
			this.editToolStripMenuItem.Name = "editToolStripMenuItem";
			this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
			this.editToolStripMenuItem.Text = "&Edit";
			// 
			// findToolStripMenuItem
			// 
			this.findToolStripMenuItem.Name = "findToolStripMenuItem";
			this.findToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F)));
			this.findToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
			this.findToolStripMenuItem.Text = "&Find All...";
			this.findToolStripMenuItem.Click += new System.EventHandler(this.findToolStripMenuItem_Click);
			// 
			// selectAllToolStripMenuItem
			// 
			this.selectAllToolStripMenuItem.Name = "selectAllToolStripMenuItem";
			this.selectAllToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
			this.selectAllToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
			this.selectAllToolStripMenuItem.Text = "Select &All";
			this.selectAllToolStripMenuItem.Click += new System.EventHandler(this.selectAllToolStripMenuItem_Click);
			// 
			// viewToolStripMenuItem
			// 
			this.viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showAllToolStripMenuItem,
            this.selectedToolStripMenuItem,
            this.unselectedToolStripMenuItem});
			this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
			this.viewToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
			this.viewToolStripMenuItem.Text = "View";
			// 
			// showAllToolStripMenuItem
			// 
			this.showAllToolStripMenuItem.Name = "showAllToolStripMenuItem";
			this.showAllToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
			this.showAllToolStripMenuItem.Text = "All";
			this.showAllToolStripMenuItem.Click += new System.EventHandler(this.allToolStripMenuItem_Click);
			// 
			// selectedToolStripMenuItem
			// 
			this.selectedToolStripMenuItem.Name = "selectedToolStripMenuItem";
			this.selectedToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
			this.selectedToolStripMenuItem.Text = "Selected Only";
			this.selectedToolStripMenuItem.Click += new System.EventHandler(this.selectedToolStripMenuItem_Click);
			// 
			// unselectedToolStripMenuItem
			// 
			this.unselectedToolStripMenuItem.Name = "unselectedToolStripMenuItem";
			this.unselectedToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
			this.unselectedToolStripMenuItem.Text = "Unselected Only";
			this.unselectedToolStripMenuItem.Click += new System.EventHandler(this.unselectedToolStripMenuItem_Click);
			// 
			// dlgSaveFile
			// 
			this.dlgSaveFile.DefaultExt = "pdf";
			this.dlgSaveFile.FileName = "Untitled";
			this.dlgSaveFile.Filter = "PDF Document (*.pdf)|*.pdf";
			this.dlgSaveFile.Title = "Save a PDF Document";
			// 
			// compactToolStripMenuItem
			// 
			this.compactToolStripMenuItem.Name = "compactToolStripMenuItem";
			this.compactToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
			this.compactToolStripMenuItem.Text = "Compact";
			this.compactToolStripMenuItem.Click += new System.EventHandler(this.compactToolStripMenuItem_Click);
			// 
			// MainForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(837, 566);
			this.Controls.Add(this.split);
			this.Controls.Add(this.mstrip);
			this.MainMenuStrip = this.mstrip;
			this.Name = "MainForm";
			this.Text = "Content Extract";
			this.Load += new System.EventHandler(this.MainForm_Load);
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
			this.cmenuTextBox.ResumeLayout(false);
			this.split.Panel1.ResumeLayout(false);
			this.split.Panel2.ResumeLayout(false);
			this.split.ResumeLayout(false);
			this.cmenuIndirectObject.ResumeLayout(false);
			this.tabInfo.ResumeLayout(false);
			this.pageImage.ResumeLayout(false);
			this.pnlPicture.ResumeLayout(false);
			this.pnlPicture.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.pict)).EndInit();
			this.pageAtom.ResumeLayout(false);
			this.pageContent.ResumeLayout(false);
			this.mstrip.ResumeLayout(false);
			this.mstrip.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() {
			Application.EnableVisualStyles();
			Application.Run(new MainForm());
		}

		private void Clear() {
			ResetItemView();
			if(pict.Image!=null && !_imageIsCached) {
				pict.Image.Dispose();
				pict.Image = null;
			}
			_imageIsCached = false;
			_cachedExtractor = null;
			lst.Items.Clear();
			for(int i = 0; i<_cachedImageList.Length; ++i) {
				if(_cachedImageList[i].Image!=null)
					_cachedImageList[i].Image.Dispose();
			}
			Array.Clear(_cachedImageList, 0, _cachedImageList.Length);
			Array.Clear(_cachedImageExtractorList, 0, _cachedImageExtractorList.Length);
			if(_imageLoadHandler!=null)
				_imageLoadHandler.DisposeDoc(_doc);
			_imageLoadHandler = new ImageLoadHandler(this);
		}

		private void ResetItemView() {
			txtAtom.Clear();
			txtContent.Clear();
			pict.Visible = false;

			pageImage.ImageIndex = 0;
			pageAtom.ImageIndex = 0;
			pageContent.ImageIndex = 0;
		}

		public void UpdateContent(ObjectExtractor item) {
			// first atom...
			txtAtom.Text = item.GetAtom();
			pageAtom.ImageIndex = 1;
			// ... then content
			string content = item.GetContent();
			if (content == null) {
				txtContent.Clear();
				pageContent.ImageIndex = 0;
			}
			else {
				SetTextBoxText(content, txtContent);
				pageContent.ImageIndex = 1;
			}
		}

		public void UpdateImage(ObjectExtractor extractor, bool reload) {
			ImageAndState imageState;
			int i = Array.LastIndexOf(_cachedImageExtractorList, extractor);
			if ((i >= 0) && (!reload)) {
				if (SetImage(_cachedImageList[i])) {
					_imageIsCached = true;
					_cachedExtractor = extractor;
				}
				int j = _cachedImageExtractorList.Length - 1;
				if (i < j) {
					Array.Copy(_cachedImageExtractorList, i + 1,
						_cachedImageExtractorList, i, j - i);
					_cachedImageExtractorList[j] = extractor;
					imageState = _cachedImageList[i];
					Array.Copy(_cachedImageList, i + 1,
						_cachedImageList, i, j - i);
					_cachedImageList[j] = imageState;
				}
			}
			else if ((extractor == _cachedExtractor) && (!reload)) {
				pict.Visible = true;
				pageImage.ImageIndex = 1;
			}
			else {
				try {
					imageState = extractor.GetImage();
					if (SetImage(imageState))
						_cachedExtractor = extractor;
					if (imageState.State == ImageState.Loading) {
						i = _cachedImageExtractorList.Length - 1;
						if (_cachedImageExtractorList[i] == null) {
							i = Array.IndexOf(_cachedImageExtractorList, null);
						}
						else {
							Array.Copy(_cachedImageExtractorList, 1,
								_cachedImageExtractorList, 0, i);
							if (_cachedImageList[0].Image != null) {
								if (_cachedImageList[0].Image != pict.Image)
									_cachedImageList[0].Image.Dispose();
								else
									_imageIsCached = false;
							}
							Array.Copy(_cachedImageList, 1, _cachedImageList, 0, i);
							_cachedImageList[i] = new ImageAndState();
						}
						_cachedImageExtractorList[i] = extractor;
						_cachedImageList[i] = imageState;
						_imageLoadHandler.BeginLoadImage(extractor);
					}
				}
				catch (Exception exc) {
					if (SetImage(new ImageAndState(null, ImageState.Error)))
						_cachedExtractor = extractor;
					MessageBox.Show(exc.Message);
				}
			}
			if (_preferredPage.ImageIndex == 1)
				SetPage(_preferredPage);
			else if (tabInfo.SelectedTab.ImageIndex == 0)
				SetPage(pageAtom);
		}

		public bool Dirty { get { return _dirty; } set { _dirty = value; } }
		public Find FindDialog { get { return _findDialog; } set { _findDialog = value; } }

		public void Find(string search) {
			if ((_doc == null) || (search == null) || (search == ""))
				return;
			foreach (ListViewItem item in lst.Items) {
				ObjectExtractor extractor = item.Tag as ObjectExtractor;
				string val = extractor != null ? extractor.GetValue(_doc) : "";
				item.Selected = (val.IndexOf(search) >= 0);
			}
			lst.Focus();
		}

		private void Open(string filePath) {
			if (!CloseFile())
				return;
			string oldText = Text;
			Doc doc = null;
			try {
				Text = "Content Extract - "+filePath;
				Update();
				doc = new Doc();
				XReadOptions ro = new XReadOptions();
				ro.ExtraChecks = true;
				doc.Read(filePath, ro);
			} catch {
				if(doc!=null)
					doc.Dispose();
				Text = oldText;
				throw;
			}
			Clear();
			_doc = doc;
			_filePath = filePath;
			_dirty = false;
			lst.BeginUpdate();
			try {
				lst.Items.AddRange(MakeListItems(doc));
			} finally {
				lst.EndUpdate();
			}
		}

		private static ListViewItem[] MakeListItems(Doc doc) {
			ObjectSoup soup = doc.ObjectSoup;
			int count = soup.Count;
			ListViewItem[] items;
			ObjectExtractor[] extractorList = new ObjectExtractor[count];
			string[] errorList = new string[count];
			ExtractorContext context = new ExtractorContext();
			for (int i = 0; i < count; ++i) {
				IndirectObject obj = soup[i];
				if (obj == null)
					continue;

				try {
					extractorList[i] = ObjectExtractor.FromIndirectObject(obj, context);
				}
				catch (Exception exc) {
					extractorList[i] = new ObjectExtractor(obj);
					errorList[i] = exc.Message;
				}
			}
			foreach(int id in context.CMaps) {
				StreamObjectExtractor streamExtractor
					= unchecked((uint)id) >= unchecked((uint)count) ? null :
					extractorList[id] as StreamObjectExtractor;
				if(streamExtractor != null)
					streamExtractor.StreamType = "CMap";
			}
			foreach (KeyValuePair<int, string> pair in context.Xfa) {
				StreamObjectExtractor streamExtractor
					= unchecked((uint)pair.Key) >= unchecked((uint)count) ? null :
					extractorList[pair.Key] as StreamObjectExtractor;
				if (streamExtractor != null) {
					streamExtractor.StreamType = pair.Value == null ? "XML/XFA" :
						string.Format("XML/XFA[{0}]", pair.Value);
				}
			}
			string[] scriptTypeList = { "JavaScript",
				"JavaScript[{0}]",
				"JavaScript[PageID {1}]",
				"JavaScript[{0},PageID {1}]",
				"JavaScript[AnnotationID {2}]",
				"JavaScript[{0},AnnotationID {2}]",
				"JavaScript[PageID {1},AnnotationID {2}]",
				"JavaScript[{0},PageID {1},AnnotationID {2}]"
			};
			foreach (KeyValuePair<int, ScriptType> pair in context.Scripts) {
				StreamObjectExtractor streamExtractor
					= unchecked((uint)pair.Key) >= unchecked((uint)count) ? null :
					extractorList[pair.Key] as StreamObjectExtractor;
				if (streamExtractor != null) {
					int formatIndex = 0;
					if (pair.Value.DocumentName != null)
						formatIndex |= 1;
					if (pair.Value.PageName != null)
						formatIndex |= 2;
					if (pair.Value.AnnotationName != null)
						formatIndex |= 4;
					streamExtractor.StreamType = string.Format(
						scriptTypeList[formatIndex], pair.Value.DocumentName,
						pair.Value.PageName, pair.Value.AnnotationName);
				}
			}
			foreach(KeyValuePair<int, string> pair in context.Type3Glyphs) {
				StreamObjectExtractor streamExtractor
					= unchecked((uint)pair.Key) >= unchecked((uint)count) ? null :
					extractorList[pair.Key] as StreamObjectExtractor;
				if(streamExtractor != null) {
					streamExtractor.StreamType = string.Format(
						"Type 3 Glyph[{0}]", pair.Value);
				}
			}
			foreach (int id in context.PageContents) {
				StreamObjectExtractor streamExtractor
					= unchecked((uint)id) >= unchecked((uint)count) ? null :
					extractorList[id] as StreamObjectExtractor;
				if (streamExtractor != null)
					streamExtractor.StreamType = "Page Content";
			}
			List<ListViewItem> itemList = new List<ListViewItem>(count);
			for (int i = 0; i < count; ++i) {
				ObjectExtractor extractor = extractorList[i];
				if (extractor != null) {
					string[] info;
					if (errorList[i] != null)
						info = extractor.GetErrorInfo(errorList[i]);
					else {
						try {
							info = extractor.GetInfo();
						}
						catch (Exception exc) {
							extractor = new ObjectExtractor(extractor.Object);
							info = extractor.GetErrorInfo(exc.Message);
						}
					}

					ListViewItem item = new ListViewItem(info);
					item.Tag = extractor;
					itemList.Add(item);
				}
			}
			items = itemList.ToArray();
			return items;
		}

		private bool CloseFile() {
			bool ok = true;
			if (_dirty) {
				string text = string.Format("Do you want to save changes to {0} ?",
					Path.GetFileNameWithoutExtension(_filePath));
				switch (MessageBox.Show(text, Application.ProductName, MessageBoxButtons.YesNoCancel)) {
					case DialogResult.Yes :
						_dirty = false;
						Save();
						_doc.Clear();
						break;
					case DialogResult.No :
						break;
					case DialogResult.Cancel :
						ok = false;
						break;
				}
			}
			return ok;
		}

		private void Save() {
			bool swap = File.Exists(_filePath);
			string path = swap ? _filePath + ".tmp" : _filePath;
			// when we save we try to keep the object ids constant
			_doc.SaveOptions.Linearize = false;
			_doc.SaveOptions.Remap = false;
			_doc.Save(path);
			_doc.Clear();
			if (swap) {
				File.Delete(_filePath);
				File.Move(path, _filePath);
			}
			_dirty = false;
			Open(_filePath);
			TempFile = null;
		}

		private string TempFile {
			set {
				try {
					if ((_tempPath != null) && (File.Exists(_tempPath)))
						File.Delete(_tempPath);
				}
				catch {
				}
				_tempPath = value;
			}
			get { return _tempPath; }
		}

		private void ImageLoadComplete(ObjectExtractor extractor, ImageAndState imageState)
		{
			int i = Array.LastIndexOf(_cachedImageExtractorList, extractor);
			if(i<0)
				return;

			_cachedImageList[i] = imageState;
			ListViewItem item = GetSelectedItem();
			if(item==null || item.Tag!=extractor)
				return;

			if(SetImage(imageState)) {
				_imageIsCached = true;
				_cachedExtractor = extractor;
			}
		}

		public void SetItemValue(int id, string text) {
			_doc.SetInfo(id, "value", text);
		}

		private ListViewItem GetSelectedItem() {
			ListViewItem item = lst.FocusedItem;
			if(item!=null && item.Selected)
				return item;
			if(lst.SelectedItems.Count>0)
				return lst.SelectedItems[0];

			return null;
		}

		private bool SetImage(ImageAndState imageState) {
			if(imageState.Image!=null) {
				if(pict.Image!=null && !_imageIsCached)
					pict.Image.Dispose();
				pict.Image = imageState.Image;
				_imageIsCached = false;
				pict.Visible = true;
				pageImage.ImageIndex = 1;
				return true;
			}
			switch(imageState.State) {
			case ImageState.Loading:
				if(pict.Image!=pict.InitialImage) {
					if(pict.Image!=null && !_imageIsCached)
						pict.Image.Dispose();
					pict.Image = pict.InitialImage;
					_imageIsCached = true;
				}
				pict.Visible = true;
				pageImage.ImageIndex = 1;
				return true;
			case ImageState.Error:
				if(pict.Image!=pict.ErrorImage) {
					if(pict.Image!=null && !_imageIsCached)
						pict.Image.Dispose();
					pict.Image = pict.ErrorImage;
					_imageIsCached = true;
				}
				pict.Visible = true;
				pageImage.ImageIndex = 1;
				return true;
			}
			pict.Visible = false;
			pageImage.ImageIndex = 0;
			return false;
		}

		private void SetTextBoxText(string text, RichTextBox textBox) {
			textBox.Clear();
			int i = text.IndexOf('\0');
			if(i<0) {
				textBox.Text = text;
				return;
			}

			textBox.Text = text.Replace('\0', '\ufeff');
			text = textBox.Text;
			textBox.Text = text.Replace('\ufeff', ' ');
			Color backColor = Color.FromKnownColor(KnownColor.Control);
			Color highlight = Color.FromKnownColor(KnownColor.Highlight);
			highlight = Color.FromArgb(highlight.A, (highlight.R+backColor.R)/2,
				(highlight.G+backColor.G)/2, (highlight.B+backColor.B)/2);
			// some character combinations cause mismatches between Select and text positions
			while((i = text.IndexOf('\ufeff', i))>=0) {
				int j = i;
				do {
					++i;
				} while(i<text.Length && text[i]=='\ufeff');
				textBox.Select(j, i-j);
				textBox.SelectionBackColor = highlight;
			}
			textBox.Select(0, 0);
			textBox.SelectionBackColor = new Color();
		}

		private void SetPage(TabPage page) {
			_internalPageChanging = true;
			tabInfo.SelectedTab = page;
			_internalPageChanging = false;
		}

		private void mitemExit_Click(object sender, EventArgs e) {
			CloseFile();
			Close();
		}

		private void mitemOpen_Click(object sender, EventArgs e) {
			if(dlgOpenFile.ShowDialog()!=DialogResult.OK)
				return;

			try {
				Open(dlgOpenFile.FileName);
			} catch(Exception exc) {
				MessageBox.Show(exc.Message);
			}
		}

		private void cmenuTextBox_ItemClicked(object sender, ToolStripItemClickedEventArgs e) {
			RichTextBox txt = tabInfo.SelectedTab==pageAtom? txtAtom:
				tabInfo.SelectedTab==pageContent? txtContent: null;
			if(txt==null)
				return;

			if(e.ClickedItem==mitemWordWrap) {
				txt.WordWrap = !mitemWordWrap.Checked;
			} else if(e.ClickedItem==mitemFont) {
				ContextMenuStrip cmenu = sender as ContextMenuStrip;
				if(cmenu!=null)
					cmenu.Close();
				if(dlgSelectFont.ShowDialog()==DialogResult.OK)
					txt.Font = dlgSelectFont.Font;
			}
		}

		private void cmenuTextBox_Opening(object sender, CancelEventArgs e) {
			RichTextBox txt = tabInfo.SelectedTab==pageAtom? txtAtom:
				tabInfo.SelectedTab==pageContent? txtContent: null;
			mitemWordWrap.Checked = txt!=null && txt.WordWrap;
		}

		private void lst_SelectedIndexChanged(object sender, EventArgs e) {
			ListViewItem item = GetSelectedItem();
			ObjectExtractor extractor = item==null? null: item.Tag as ObjectExtractor;
			if(_selectedExtractor==extractor)
				return;

			_selectedExtractor = extractor;
			if(extractor==null) {
				ResetItemView();
				return;
			}

			UpdateContent(extractor);
			UpdateImage(extractor, false);
		}

		private void lst_DoubleClick(object sender, EventArgs e) {
			ObjectExtractor extractor = _selectedExtractor;
			if ((extractor == null) || (extractor.Object == null)) return;
			EditText theEdit = new EditText(this, _doc, extractor);
			theEdit.Show();
		}

		private void lst_ColumnClick(object sender, ColumnClickEventArgs e) {
			ListViewItemComparer comparer = lst.ListViewItemSorter as ListViewItemComparer;
			if(comparer!=null){
				if(e.Column==comparer.Column){
					if(comparer.Order==SortOrder.Ascending)
						comparer.Order = SortOrder.Descending;
					else
						comparer.Order = SortOrder.Ascending;
				}else{
					comparer.Column = e.Column;
					comparer.Order = SortOrder.Ascending;
				}
			}
			lst.Sort();
			ListViewItem item = lst.FocusedItem;
			if(item!=null)
				item.EnsureVisible();
		}

		private void lst_DragOver(object sender, DragEventArgs e) {
			if (e.Data.GetDataPresent(DataFormats.FileDrop))
				e.Effect = DragDropEffects.Move;
		}

		private void lst_DragDrop(object sender, DragEventArgs e) {
			if (e.Data.GetDataPresent(DataFormats.FileDrop)) {
				string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
				if (files.Length > 0) {
					try {
						Open(files[0]);
					}
					catch (Exception exc) {
						MessageBox.Show(exc.Message);
					}
				}
			}
		}

		private void pict_Click(object sender, EventArgs e) {
			pnlPicture.Focus();
		}

		private void tabInfo_SelectedIndexChanged(object sender, EventArgs e) {
			if(!_internalPageChanging)
				_preferredPage = tabInfo.SelectedTab;
		}

		private void mitemDecompress_Click(object sender, EventArgs e) {
			ObjectExtractor extractor = _selectedExtractor;
			if (extractor == null) return;
			StreamObject stream = extractor.Object as StreamObject;
			if (stream == null) return;
			stream.Decompress();
			extractor.Invalidate();
			UpdateContent(extractor);
		}

		private void mItemMakeAscii_Click(object sender, EventArgs e) {
			ObjectExtractor extractor = _selectedExtractor;
			if (extractor == null) return;
			StreamObject stream = extractor.Object as StreamObject;
			if (stream == null) return;
			stream.CompressAscii85();
			extractor.Invalidate();
			UpdateContent(extractor);
		}

		private void mItemMakeAsciiHex_Click(object sender, EventArgs e) {
			ObjectExtractor extractor = _selectedExtractor;
			if (extractor == null) return;
			StreamObject stream = extractor.Object as StreamObject;
			if (stream == null) return;
			stream.CompressAsciiHex();
			extractor.Invalidate();
			UpdateContent(extractor);
		}

		private void mItemCompressFlate_Click(object sender, EventArgs e) {
			ObjectExtractor extractor = _selectedExtractor;
			if (extractor == null) return;
			StreamObject stream = extractor.Object as StreamObject;
			if (stream == null) return;
			stream.CompressFlate();
			extractor.Invalidate();
			UpdateContent(extractor);
		}

		private void mItemCompressJpeg75_Click(object sender, EventArgs e) {
			ObjectExtractor extractor = _selectedExtractor;
			if (extractor == null) return;
			PixMap pixMap = extractor.Object as PixMap;
			if (pixMap == null) return;
			pixMap.Decompress();
			pixMap.CompressJpeg(75);
			extractor.Invalidate();
			UpdateContent(extractor);
		}

		private void mItemCompressJpeg50_Click(object sender, EventArgs e) {
			ObjectExtractor extractor = _selectedExtractor;
			if (extractor == null) return;
			PixMap pixMap = extractor.Object as PixMap;
			if (pixMap == null) return;
			pixMap.Decompress();
			pixMap.CompressJpeg(50);
			extractor.Invalidate();
			UpdateContent(extractor);
		}

		private void mItemCompressJpeg25_Click(object sender, EventArgs e) {
			ObjectExtractor extractor = _selectedExtractor;
			if (extractor == null) return;
			PixMap pixMap = extractor.Object as PixMap;
			if (pixMap == null) return;
			pixMap.Decompress();
			pixMap.CompressJpeg(25);
			extractor.Invalidate();
			UpdateContent(extractor);
		}

		private void mItemRefresh_Click(object sender, EventArgs e) {
			ObjectExtractor extractor = _selectedExtractor;
			if (extractor == null) return;
			extractor.Invalidate();
			UpdateContent(extractor);
			UpdateImage(extractor, true);
		}

		private void mItemEdit_Click(object sender, EventArgs e) {
			ObjectExtractor extractor = _selectedExtractor;
			if ((extractor == null) || (extractor.Object == null)) return;
			EditText theEdit = new EditText(this, _doc, extractor);
			theEdit.Show();
		}

		private void mitemSave_Click(object sender, EventArgs e) {
			Save();
		}

		private void mitemSaveAs_Click(object sender, EventArgs e) {
			if (dlgSaveFile.ShowDialog() == DialogResult.OK) {
				_filePath = dlgSaveFile.FileName;
				Save();
			}
		}

		private void selectAllToolStripMenuItem_Click(object sender, EventArgs e) {
			Control control = ActiveControl;
			while(control!=null) {
				ContainerControl container = control as ContainerControl;
				if(container==null) {
					TextBoxBase txt = control as TextBoxBase;
					if(txt!=null) {
						txt.SelectAll();
						return;
					}
					break;
				}
				control = container.ActiveControl;
			}

			foreach (ListViewItem item in lst.Items)
				item.Selected = true;
		}

		private void findToolStripMenuItem_Click(object sender, EventArgs e) {
			if (_findDialog == null)
				_findDialog = new Find(this);
			_findDialog.Show();
			_findDialog.BringToFront();
		}

		private void compactToolStripMenuItem_Click(object sender, EventArgs e) {
			// remove any redundant objects
			string path = Path.GetTempPath() + Guid.NewGuid().ToString() + ".pdf";
			_doc.SaveOptions.Linearize = false;
			_doc.SaveOptions.Remap = true;
			_doc.Save(path);
			_doc.Clear();
			string oldText = Text;
			string oldPath = _filePath;
			_dirty = false;
			Open(path);
			_dirty = true;
			_filePath = oldPath;
			Text = oldText;
			TempFile = path;
		}

		private void allToolStripMenuItem_Click(object sender, EventArgs e) {
			if (_doc == null)
				return;
			lst.BeginUpdate();
			try {
				lst.Items.Clear();
				lst.Items.AddRange(MakeListItems(_doc));
			}
			finally {
				lst.EndUpdate();
			}
		}

		private void selectedToolStripMenuItem_Click(object sender, EventArgs e) {
			ShowSelected(true);
		}

		private void unselectedToolStripMenuItem_Click(object sender, EventArgs e) {
			ShowSelected(false);
		}

		private void ShowSelected(bool truth) {
			List<ListViewItem> itemList = new List<ListViewItem>();
			foreach (ListViewItem item in lst.Items) {
				if (item.Selected == truth)
					itemList.Add(item);
			}

			lst.BeginUpdate();
			try {
				lst.Items.Clear();
				lst.Items.AddRange(itemList.ToArray());
			}
			finally {
				lst.EndUpdate();
			}
		}

		private void MainForm_Load(object sender, EventArgs e) {
		}

		private void MainForm_FormClosing(object sender, FormClosingEventArgs e) {
			if (CloseFile()) {
				if (_doc != null) {
					_doc.Dispose();
					_doc = null;
				}
				TempFile = null;
			}
			else
				e.Cancel = true;
		}
	}
}
