// ===========================================================================
//	�2007 WebSupergoo. All rights reserved.
// ===========================================================================

using System;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Text;
using System.Threading;
using WebSupergoo.ABCpdf8;
using WebSupergoo.ABCpdf8.Objects;
using WebSupergoo.ABCpdf8.Atoms;
using WebSupergoo.ABCpdf8.Operations;

namespace WebSupergoo.ContentExtract
{
	public sealed class ExtractorContext {
		private static readonly string[] _aActionEntryList = {
			"O", "C", "K", "F", "V", "C",
			"E", "X", "D", "U", "Fo", "Bl", "PO", "PC", "PV", "PI",
			"WC", "WS", "DS", "WP", "DP"
		};
		private static readonly string[] _aActionSearchList;

		static ExtractorContext() {
			_aActionSearchList = new string[_aActionEntryList.Length];
			for(int i = 0; i<_aActionEntryList.Length; ++i)
				_aActionSearchList[i] = ','+_aActionEntryList[i]+',';
		}

		private List<int> _pageContentList;
		private List<int> _cmapList;
		private SortedDictionary<int, string> _xfaDict;
		private SortedDictionary<int, ScriptType> _scriptDict;
		private SortedDictionary<int, string> _type3GlyphDict;

		public ExtractorContext() {
			_pageContentList = new List<int>();
			_cmapList = new List<int>();
			_xfaDict = new SortedDictionary<int, string>();
			_scriptDict = new SortedDictionary<int, ScriptType>();
			_type3GlyphDict = new SortedDictionary<int, string>();
		}

		public List<int> PageContents { get { return _pageContentList; } }
		public List<int> CMaps { get { return _cmapList; } }
		public SortedDictionary<int, string> Xfa { get { return _xfaDict; } }
		public SortedDictionary<int, ScriptType> Scripts { get { return _scriptDict; } }
		public SortedDictionary<int, string> Type3Glyphs { get { return _type3GlyphDict; } }

		public void AddXfa(int id, string name) {
			string oldName;
			if(_xfaDict.TryGetValue(id, out oldName)) {
				if(oldName==name)
					return;
				if(name!=null) {
					if(oldName=="")
						return;
					name = "";
				}
			}
			_xfaDict[id] = name;
		}

		public void AddScript(int id, ScriptType type) {
			ScriptType oldType;
			if(_scriptDict.TryGetValue(id, out oldType))
				oldType.Add(type);
			else
				_scriptDict[id] = type;
		}

		public void AddAction(Doc doc, int id, ScriptType type) {
			int jsId = doc.GetInfoInt(id, "/A*/JS:Ref");
			if(jsId!=0 && doc.GetInfo(id, "/A*/S*:Name")=="JavaScript")
				AddScript(jsId, type);
		}

		public void AddAdditionalAction(Doc doc, int id, ScriptType type) {
			string keys = doc.GetInfo(id, "/AA*:Keys");
			if(keys=="")
				return;
			keys = ','+keys+',';
			for(int i = 0; i<_aActionSearchList.Length; ++i) {
				if(keys.IndexOf(_aActionSearchList[i])>=0) {
					string path = "/AA*/"+_aActionEntryList[i];
					int jsId = doc.GetInfoInt(id, path+"*/JS:Ref");
					if(jsId!=0 && doc.GetInfo(id, path+"*/S*:Name")=="JavaScript")
						AddScript(jsId, type);
				}
			}
		}

		public void AddType3Glyph(Doc doc, int id) {
			string s = doc.GetInfo(id, "/CharProcs*:Keys");
			if(s!="") {
				string[] keys = s.Split(new char[] { ',' },
					StringSplitOptions.RemoveEmptyEntries);
				foreach(string name in keys) {
					int glyphId = doc.GetInfoInt(id, "/CharProcs*/"+name+":Ref");
					if(glyphId!=0) {
						string oldName;
						if(_type3GlyphDict.TryGetValue(glyphId, out oldName)) {
							if(oldName==name || oldName=="")
								continue;
							_type3GlyphDict[glyphId] = "";
						} else
							_type3GlyphDict[glyphId] = name;
					}
				}
			}
		}
	}

	public enum ImageState { None, Loading, Error }

	[StructLayout(LayoutKind.Auto)]
	public struct ImageAndState {
		public Image Image;
		public ImageState State;

		public ImageAndState(Image image, ImageState state) {
			Image = image;
			State = state;
		}
	}

	public sealed class ScriptType {
		public string DocumentName;
		public string PageName;
		public string AnnotationName;

		public static ScriptType NewDocument(string name) {
			ScriptType v = new ScriptType();
			v.DocumentName = name;
			return v;
		}
		public static ScriptType NewPage(string name) {
			ScriptType v = new ScriptType();
			v.PageName = name;
			return v;
		}
		public static ScriptType NewAnnotation(string name) {
			ScriptType v = new ScriptType();
			v.AnnotationName = name;
			return v;
		}

		public void Add(ScriptType v) {
			if(DocumentName!=v.DocumentName)
				DocumentName = DocumentName==null? v.DocumentName: "";
			if(PageName!=v.PageName)
				PageName = PageName==null? v.PageName: "";
			if(AnnotationName!=v.AnnotationName)
				AnnotationName = AnnotationName==null? v.AnnotationName: "";
		}
	}

	public class ObjectExtractor {
		public delegate void ImageLoadCompleteDelegate(
			ObjectExtractor extractor, ImageAndState imageState);

		public static ObjectExtractor FromIndirectObject(IndirectObject obj, ExtractorContext context) {
			if(obj==null)
				throw new ArgumentNullException("obj", "IndirectObject obj cannot be null.");

			if(obj is StreamObject)
				return StreamObjectExtractor.FromStreamObject((StreamObject)obj);
			if(obj is Annotation)
				return new AnnotationExtractor((Annotation)obj, context);
			if(obj is Page)
				return new PageExtractor((Page)obj, context);
			if(obj is Pages)
				return new PagesExtractor((Pages)obj);
			if(obj is FontObject)
				return new FontObjectExtractor((FontObject)obj, context);
			if(obj is Field)
				return FieldExtractor.FromField((Field)obj);
			if(obj is GraphicsState)
				return new GraphicsStateExtractor((GraphicsState)obj);
			if(obj is Bookmark)
				return BookmarkExtractor.FromBookmark((Bookmark)obj);
			if(obj is ColorSpace)
				return new ColorSpaceExtractor((ColorSpace)obj);
			if(obj is Catalog)
				return new CatalogExtractor((Catalog)obj, context);

			return new ObjectExtractor(obj);
		}

		private IndirectObject _obj;

		public ObjectExtractor(IndirectObject obj) {
			_obj = obj;
		}

		public IndirectObject Object { get { return _obj; } }

		public virtual bool IsAscii { get { return true; } }

		public virtual void Invalidate() {}

		public string GetValue(Doc doc) {
			if ((doc == null) || (_obj == null))
				return "";
			return doc.GetInfo(_obj.ID, "value");
		}

		public virtual void SetValue(Doc doc, string value) {
			if ((doc == null) || (_obj == null))
				return;
			int id = _obj.ID;
			doc.SetInfo(id, "value", value);
			_obj = doc.ObjectSoup[id]; 
			Invalidate();
		}

		public virtual string[] GetInfo() {
			return new string[] { GetIDString(), GetTypeName() };
		}

		public string[] GetErrorInfo(string message) {
			return new string[] { GetIDString(), GetTypeName(), "(Error!)", "", message };
		}

		protected string GetIDString() { return _obj.ID.ToString(); }
		protected string GetTypeName() { return _obj.GetType().Name; }

		public virtual ImageAndState GetImage() {
			return new ImageAndState(GetBitmap(), ImageState.None);
		}
		public virtual void BeginLoadImage(ImageLoadCompleteDelegate imageLoadComplete) {
			throw new NotSupportedException("BeginLoadImage is not supported.");
		}
		public virtual Bitmap GetBitmap() { return null; }
		public virtual string GetAtom() { return Object.Atom.ToString(); }
		public virtual string GetContent() { return null; }

		protected static string FormatList(string[] list) {
			if(list==null || list.Length<=0)
				return "";

			StringBuilder builder = new StringBuilder();
			builder.Append(list[0]);
			for(int i = 1; i<list.Length; ++i)
				builder.Append(", ").Append(list[i]);

			return builder.ToString();
		}
	}

	public class CatalogExtractor: ObjectExtractor {
		public CatalogExtractor(Catalog obj, ExtractorContext context) : base(obj) {
			Doc doc = obj.Doc;
			int id = obj.ID;
			int treeId = doc.GetInfoInt(id, "/Names*/JavaScript:Ref");
			if(treeId!=0)
				AddScriptInTree(doc, context, treeId);

			int count = doc.GetInfoInt(id, "/AcroForm*/XFA*:Count");
			if(count>0) {
				for(int i = 0; i<count; i += 2) {
					context.AddXfa(doc.GetInfoInt(id,
						"/AcroForm*/XFA*["+(i+1).ToString()+"]:Ref"),
						doc.GetInfo(id, "/AcroForm*/XFA*["+i.ToString()+"]*:Text"));
				}
			} else {
				int xfaId = doc.GetInfoInt(id, "/AcroForm*/XFA:Ref");
				if(xfaId!=0)
					context.AddXfa(xfaId, null);
			}
		}
		private static void AddScriptInTree(Doc doc, ExtractorContext context, int nodeId) {
			int count = doc.GetInfoInt(nodeId, "/Kids*:Count");
			if(count>0) {
				for(int i = 0; i<count; ++i) {
					int childNodeId = doc.GetInfoInt(nodeId, "/Kids*["+i.ToString()+"]:Ref");
					if(childNodeId!=0)
						AddScriptInTree(doc, context, childNodeId);    // recur on childNodeID
				}
				return;
			}

			count = doc.GetInfoInt(nodeId, "/Names*:Count");
			for(int i = 0; i<count; i += 2) {
				string path = "/Names*["+(i+1).ToString();
				int jsId = doc.GetInfoInt(nodeId, path+"]*/JS:Ref");
				if(jsId!=0 && doc.GetInfo(nodeId, path+"]*/S*:Name")=="JavaScript") {
					context.AddScript(jsId, ScriptType.NewDocument(
						doc.GetInfo(nodeId, "/Names*["+i.ToString()+"]*:Text")));
				}
			}
		}

		public new Catalog Object { get { return (Catalog)base.Object; } }

		public override string[] GetInfo() {
			Catalog obj = Object;
			Outline outline = obj.Outline;
			Pages pages = obj.Pages;
			return new string[]{ GetIDString(), GetTypeName(), "", "",
				string.Format("Outline ID:[{0}] Pages ID:[{1}]",
					outline==null? "": outline.ID.ToString(),
					pages==null? "": pages.ID.ToString()) };
		}
	}

	public class ColorSpaceExtractor: ObjectExtractor {
		public ColorSpaceExtractor(ColorSpace obj): base(obj) { }

		public new ColorSpace Object { get { return (ColorSpace)base.Object; } }

		public override string[] GetInfo() {
			ColorSpace obj = Object;
			IccProfile iccProfile = obj.IccProfile;
			return new string[]{ GetIDString(), GetTypeName(), obj.Name,
				obj.ColorSpaceType.ToString(),
				string.Format("Components:[{0}] IccProfile ID:[{1}]",
					obj.Components, iccProfile==null? "": iccProfile.ID.ToString()) };
		}
	}

	public class BookmarkExtractor: ObjectExtractor {
		public static BookmarkExtractor FromBookmark(Bookmark obj) {
			if(obj is Outline)
				return new OutlineExtractor((Outline)obj);

			return new BookmarkExtractor(obj);
		}

		public BookmarkExtractor(Bookmark obj): base(obj) { }

		public new Bookmark Object { get { return (Bookmark)base.Object; } }

		public override string[] GetInfo() {
			Bookmark obj = Object;
			Bookmark parent = obj.Parent;
			return new string[]{ GetIDString(), GetTypeName(), "", obj.Title,
				string.Format("PageID:[{0}] Parent ID:[{1}] Count:[{2}] Open:[{3}]",
				obj.PageID, parent==null? "": parent.ID.ToString(),
				obj.Count, obj.Open) };
		}
	}

	public class OutlineExtractor: BookmarkExtractor {
		public OutlineExtractor(Outline obj): base(obj) { }

		public new Outline Object { get { return (Outline)base.Object; } }
	}

	public class GraphicsStateExtractor: ObjectExtractor {
		public GraphicsStateExtractor(GraphicsState obj) : base(obj) { }

		public new GraphicsState Object { get { return (GraphicsState)base.Object; } }
	}

	public class FieldExtractor: ObjectExtractor {
		public static FieldExtractor FromField(Field obj) {
			if(obj is Signature)
				return new SignatureExtractor((Signature)obj);

			return new FieldExtractor(obj);
		}

		public FieldExtractor(Field obj): base(obj) { }

		public new Field Object { get { return (Field)base.Object; } }

		public override string[] GetInfo() {
			Field obj = Object;
			return new string[]{ GetIDString(), GetTypeName(),
				obj.Name, obj.Value,
				string.Format("FieldType:[{0}] Format:[{1}] MultiSelect:[{2}] Options[{3}]",
					obj.FieldType, obj.Format, obj.MultiSelect,
					FormatList(obj.Options)) };
		}
	}

	public class SignatureExtractor: FieldExtractor {
		public SignatureExtractor(Signature obj): base(obj) { }

		public new Signature Object { get { return (Signature)base.Object; } }

		public override string[] GetInfo() {
			Signature obj = Object;
			return new string[]{ GetIDString(), GetTypeName(), "",
				obj.Signer, string.Format(
				"IsModified:[{0}] SigningUtcTime:[{1}] Location:[{2}] Reason[{3}]",
				obj.IsModified, obj.SigningUtcTime, obj.Location, obj.Reason) };
		}
	}

	public class FontObjectExtractor: ObjectExtractor
	{
		public FontObjectExtractor(FontObject obj, ExtractorContext context) : base(obj) {
			Doc doc = obj.Doc;
			int id = obj.ID;
			int cmapId = doc.GetInfoInt(id, "/ToUnicode:Ref");
			if(cmapId!=0)
				context.CMaps.Add(cmapId);
			context.AddType3Glyph(doc, id);
		}

		public new FontObject Object{ get{ return (FontObject)base.Object; } }
	}

	public class AnnotationExtractor: ObjectExtractor {
		public AnnotationExtractor(Annotation obj, ExtractorContext context) : base(obj) {
			Doc doc = obj.Doc;
			int id = obj.ID;
			ScriptType scryptType = ScriptType.NewAnnotation(id.ToString());
			context.AddAction(doc, id, scryptType);
			context.AddAdditionalAction(doc, id, scryptType);
		}

		public new Annotation Object { get { return (Annotation)base.Object; } }

		public override string[] GetInfo() {
			Annotation obj = Object;
			return new string[]{ GetIDString(), GetTypeName(),
				obj.FullName, obj.SubType,
				string.Format("FieldType:[{0}] FieldValue:[{1}] Contents:[{2}]",
					obj.FieldType, obj.FieldValue, obj.Contents) };
		}
	}

	public class PageExtractor: ObjectExtractor {
		private string _atom;
		private int _pageNum;

		public PageExtractor(Page obj, ExtractorContext context): base(obj) {
			_atom = obj.Atom.ToString();
			Doc doc = obj.Doc;
			int id = obj.ID;
			doc.Page = id;
			if(doc.Page==id)
				_pageNum = doc.PageNumber;

			context.AddAdditionalAction(doc, id, ScriptType.NewPage(id.ToString()));

			int count = doc.GetInfoInt(id, "/Contents*:Count");
			if(count>0) {
				for(int i = 0; i<count; ++i)
					context.PageContents.Add(doc.GetInfoInt(id,
						"/Contents*["+i.ToString()+"]:Ref"));
			} else {
				int contentId = doc.GetInfoInt(id, "/Contents:Ref");
				if(contentId!=0)
					context.PageContents.Add(contentId);
			}
		}

		public new Page Object { get { return (Page)base.Object; } }

		public override string[] GetInfo() {
			Page obj = Object;
			Doc doc = obj.Doc;
			int id = obj.ID;
			doc.Page = id;
			Pages parent = obj.Parent;
			return new string[] { 
				GetIDString(), 
				GetTypeName(),
				_pageNum==0? "": "Page "+_pageNum.ToString(), "",
				string.Format("Rotation:[{0}] Parent ID:[{1}]", obj.Rotation, parent==null? "": parent.ID.ToString()) 
			};
		}

		public override ImageAndState GetImage() {
			return new ImageAndState(null, _pageNum==0? ImageState.None: ImageState.Loading);
		}

		public override void BeginLoadImage(ImageLoadCompleteDelegate imageLoadComplete) {
			Page obj = Object;
			Doc doc = obj.Doc;
			int id = obj.ID;
			doc.Page = id;
			doc.Rect.SetRect(doc.MediaBox);
			double dimension = Math.Max(doc.MediaBox.Width, doc.MediaBox.Height);
			doc.Rendering.DotsPerInch = dimension<850? 72: 72*850/dimension;
			RenderOperation operation = null;
			try {
				operation = new RenderOperation(doc);
				ThreadPool.QueueUserWorkItem(delegate(object state) {
					Image image = null;
					try {
						image = operation.GetBitmap();
					} catch {
					} finally {
						operation.Dispose();
					}
					imageLoadComplete(this, new ImageAndState(image, image==null? ImageState.Error: ImageState.None));
				});
			} catch {
				if(operation!=null)
					operation.Dispose();
				throw;
			}
		}

		public override Bitmap GetBitmap() {
			Page obj = Object;
			Doc doc = obj.Doc;
			int id = obj.ID;
			doc.Page = id;
			if(doc.Page!=id)
				return null;

			doc.Rect.SetRect(doc.MediaBox);
			return doc.Rendering.GetBitmap();
		}

		public override string GetAtom() { return _atom; }
	}

	public class PagesExtractor: ObjectExtractor {
		public PagesExtractor(Pages obj): base(obj) { }

		public new Pages Object { get { return (Pages)base.Object; } }

		public override string[] GetInfo() {
			Pages obj = Object;
			Pages parent = obj.Parent;
			return new string[]{ GetIDString(), GetTypeName(), "", "",
				string.Format("Count:[{0}] Parent ID:[{1}]", obj.Count, parent==null? "": parent.ID.ToString()) };
		}
	}

	public class StreamObjectExtractor: ObjectExtractor {
		public static StreamObjectExtractor FromStreamObject(StreamObject obj) {
			if(obj is PixMap)
				return new PixMapExtractor((PixMap)obj);
			if(obj is Layer)
				return LayerExtractor.FromLayer((Layer)obj);
			if(obj is IccProfile)
				return new IccProfileExtractor((IccProfile)obj);

			return new StreamObjectExtractor(obj);
		}

		private string _atom;
		private string _streamType;	// only if is text

		public StreamObjectExtractor(StreamObject obj): base(obj) { }

		public new StreamObject Object { get { return (StreamObject)base.Object; } }

		public override bool IsAscii { 
			get {
				StreamObject so = Object;
				if (so == null) return true;
				byte[] data = so.GetData();
				for (int i = 0; i < data.Length; i++) {
					if ((data[i] >= 32) && (data[i] < 128))
						continue;
					if ((data[i] != '\r') && (data[i] != '\n') && (data[i] != '\t'))
						return false;
				}
				return base.IsAscii; 
			} 
		}

		public string StreamType {
			get { return _streamType; }
			set { _streamType = value; }
		}

		public override void Invalidate() {
			_atom = null;
			_streamType = null;
			base.Invalidate();
		}

		public override string[] GetInfo() {
			UpdateStreamType();
			StreamObject obj = Object;
			return new string[]{ GetIDString(), GetTypeName(), _streamType,
				obj.Compression.ToString(),
				string.Format("Length:[{0}]", obj.Length) };
		}

		private void UpdateStreamType() {
			if(_streamType!=null)
				return;

			StreamObject obj = Object;
			Doc doc = obj.Doc;
			int id = obj.ID;
			string subtype = doc.GetInfo(id, "/Subtype*:Name");
			if(subtype=="Form" || subtype=="PS") {
				string type = subtype=="PS"
					|| doc.GetInfo(id, "/Subtype2*:Name")=="PS"?
					"PostScript XObject": "Form XObject";
				string name = doc.GetInfo(id, "/Name*:Name");
				_streamType = name==""? type:
					string.Format("{0}[{1}]", type, name);
			} else if(subtype=="XML")
				_streamType = doc.GetInfo(id, "/Type*:Name")=="Metadata"?
					"XML/Metadata" : "XML";
			else if(doc.GetInfo(id, "/PatternType")!="")
				_streamType = "Pattern";
			else if(doc.GetInfoInt(id, "/FunctionType*:Num")==4)
				_streamType = "Function/Type-4";
			else
				_streamType = "";
		}

		private bool ContainsText() {
			StreamObject obj = Object;
			CompressionType[] comps = obj.Compressions;
			CompressionType comp = comps.Length > 0 ? comps[0] : CompressionType.None;
			return (comp == CompressionType.AsciiHex) || (comp == CompressionType.Ascii85);
		}

		public override string GetAtom() {
			if(_atom==null)
				_atom = base.GetAtom();
			return _atom;
		}

		public override string GetContent() {
			UpdateStreamType();
			if (_streamType != "") {
				if (_atom == null)
					_atom = base.GetAtom();
				StreamObject obj = Object;
				obj.Decompress();
				return obj.GetText();
			}
			if (ContainsText()) {
				return Object.GetText();
			}
			return base.GetContent();
		}
	}

	public class IccProfileExtractor: StreamObjectExtractor {
		public IccProfileExtractor(IccProfile obj): base(obj) { }

		public new IccProfile Object { get { return (IccProfile)base.Object; } }
	}

	public class PixMapExtractor: StreamObjectExtractor {
		private static readonly string[] _colorSpaceFormat = {
			"{0}\u2014{1} comp \xd7 {2} bit",
			"{0}\u2014{1} comp \xd7 {2} bits",
			"{0}\u2014{1} comps \xd7 {2} bit",
			"{0}\u2014{1} comps \xd7 {2} bits"
		};

		public PixMapExtractor(PixMap obj): base(obj) { }

		public new PixMap Object { get { return (PixMap)base.Object; } }

		public override string[] GetInfo() {
			PixMap obj = Object;
			int components = obj.Components;
			int bitsPerComponent = obj.BitsPerComponent;
			return new string[]{ GetIDString(), GetTypeName(),
				obj.Doc.GetInfo(obj.ID, "/Name*:Name"),
				string.Format("{0}\xd7{1} {2}", obj.Width, obj.Height,
					obj.Compression),
				string.Format(_colorSpaceFormat[
					(components<=1? 0: 2)+(bitsPerComponent<=1? 0: 1)],
					obj.ColorSpaceType, components, bitsPerComponent) };
		}

		public override Bitmap GetBitmap() {
			return Object.GetBitmap();
		}
	}

	public class LayerExtractor: StreamObjectExtractor {
		public static LayerExtractor FromLayer(Layer obj) {
			return new LayerExtractor(obj);
		}

		public LayerExtractor(Layer obj): base(obj) { }

		public new Layer Object { get { return (Layer)base.Object; } }

		public override string[] GetInfo() {
			Layer obj = Object;
			return new string[]{ GetIDString(), GetTypeName(), "",
				obj.Compression.ToString(),
				string.Format("Length:[{0}] Rect:[{1}]",
					obj.Length, obj.Rect.String) };
		}
	}
}
