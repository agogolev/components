using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace WebSupergoo.ContentExtract {
	public partial class Find : Form {
		public Find(MainForm parent) {
			InitializeComponent();
			_parent = parent;
		}

		private MainForm _parent;

		static string _lastSearch = "";

		private void findNext_Click(object sender, EventArgs e) {
			_parent.Find(findText.Text);
			_parent.FindDialog = null;
			Close();
		}

		private void Cancel_Click(object sender, EventArgs e) {
			_parent.FindDialog = null;
			Close();
		}

		private void Find_Load(object sender, EventArgs e) {
			findText.Text = _lastSearch;
		}

		private void Find_Shown(object sender, EventArgs e) {
		}

		private void findText_TextChanged(object sender, EventArgs e) {
		}

		private void Find_FormClosing(object sender, FormClosingEventArgs e) {
			_lastSearch = findText.Text;
		}
	}
}