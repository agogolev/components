// ===========================================================================
//	�2006 WebSupergoo. All rights reserved.
// ===========================================================================

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace WebSupergoo.ABCpdf8.Drawing {
	#region PageCollection
	/// <summary>
	/// Represents a collection of Page objects for a PDF document.
	/// </summary>
	public sealed class PageCollection : ReadOnlyCollection<Page> {

		public PageCollection(IList<Page> sourceList) : base(sourceList) { }
	}
	#endregion
}
