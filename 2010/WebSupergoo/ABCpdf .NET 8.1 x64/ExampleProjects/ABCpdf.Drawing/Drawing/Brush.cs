// ===========================================================================
//	�2006 WebSupergoo. All rights reserved.
// ===========================================================================

using System;

namespace WebSupergoo.ABCpdf8.Drawing {
	public interface Brush {
		WebSupergoo.ABCpdf8.Drawing.Color Color {
			get;
			set;
		}
	}
	
	#region SolidBrush
	/// <summary>
	/// Used to fill the interiors of graphical shapes such as rectangles, ellipses, pies, polygons, and paths.
	/// </summary>
	public sealed class SolidBrush : Brush {
		#region Declare variables
		private Color _color;
		#endregion

		#region Properties
		/// <summary>
		/// Gets or sets the color of this Brush object.  
		/// </summary>
		public Color Color {
			get { return _color; }
			set { _color = value; }
		}
		#endregion

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the Brush class with 
		/// the specified Color property.
		/// </summary>
		/// <param name="color">The color of this System.Drawing.Pen object.</param>
		public SolidBrush(Color color) {
			_color = color;
		}
		#endregion

		public override bool Equals(object obj) {
			Brush b = obj as Brush;
			if (b != null)
				return b.Color.Equals(Color);
			else
				return false;
		}

		public override int GetHashCode() {
			return base.GetHashCode ();
		}


	}
	#endregion
}
