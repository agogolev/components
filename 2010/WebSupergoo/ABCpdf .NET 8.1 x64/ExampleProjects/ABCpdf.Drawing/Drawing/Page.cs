// ===========================================================================
//	�2006 WebSupergoo. All rights reserved.
// ===========================================================================

using System;

namespace WebSupergoo.ABCpdf8.Drawing {
	#region Page
	/// <summary>
	/// A visible page within the document.
	/// </summary>
	public sealed class Page {
		#region Declare variables
		internal PDFDocument Document = null;
		private Drawing.Graphics _graphics = null;
		private int _id = -1;
		#endregion
	
		#region Properties  
		/// <summary>
		/// The ID of the PDF page.
		/// </summary>
		public int ID {
			get { return _id; }
		} 

		/// <summary>
		/// The Graphics object for the page.
		/// </summary>
		public Drawing.Graphics Graphics {
			get { 
				Document.doc.Page = _id;
				return _graphics; 
			}
		} 

		/// <summary>
		/// The Width of the page.
		/// </summary>
		public int Width {
			get { 
				Document.doc.Page = _id;
				return (int)Document.doc.MediaBox.Width; 
			}
		} 

		/// <summary>
		/// The Height of the page.
		/// </summary>
		public int Height {
			get { 
				Document.doc.Page = _id;
				return (int)Document.doc.MediaBox.Height; 
			}
		} 


		#endregion
		
		#region Constructors
		/// <summary>
		/// Page constructor.
		/// </summary>
		/// <param name="page">The PDFDocument instance.</param>
		internal Page(PDFDocument doc) {
			if (doc == null || doc.doc == null)
				throw new ApplicationException("Invalid document ptr");

			Document = doc;

			_graphics = new Drawing.Graphics(this);
			_id = Document.doc.AddPage();
			
			Document.doc.Page = _id;
			Document.doc.Rect.String = Document.doc.MediaBox.String;
			Document.doc.Color.String = "0 0 0";
		}
		
		/// <summary>
		/// Page constructor.
		/// </summary>
		/// <param name="index">The zero-based index at which Page should be inserted.</param>
		/// <param name="doc">The PDFDocument instance.</param>
		internal Page(int index, PDFDocument doc) {
			if (doc == null || doc.doc == null)
				throw new ApplicationException("Invalid document ptr");

			Document = doc;

			_graphics = new Drawing.Graphics(this);
			_id = Document.doc.AddPage(index);
			
			Document.doc.Page = _id;
			Document.doc.Rect.String = Document.doc.MediaBox.String;
			Document.doc.Color.String = "0 0 0";
		}
		#endregion
	}
	#endregion
}
