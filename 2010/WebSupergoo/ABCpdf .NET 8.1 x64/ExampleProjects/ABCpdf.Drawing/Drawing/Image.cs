// ===========================================================================
//	�2006 WebSupergoo. All rights reserved.
// ===========================================================================

using System;
using System.IO;
using System.Text;

namespace WebSupergoo.ABCpdf8.Drawing {
	#region Image
	public class Image : IDisposable {
		internal XImage xImage = new XImage();
		internal Stream xStream;

		/// <summary>
		/// Gets the width, in pixels, of this Image
		/// </summary>
		/// <value>The width, in pixels, of this Image.</value>
		public int Width {
			get { 
				return xImage.Width;
			}
		}

		/// <summary>
		/// Gets the height, in pixels, of this Image
		/// </summary>
		/// <value>The height, in pixels, of this Image.</value>
		public int Height {
			get { 
				return xImage.Height;
			}
		}

		/// <summary>
		/// Creates a Image from the specified file.
		/// </summary>
		/// <param name="filename">A path to the image file.</param>
		/// <returns>The Image this method creates.</returns>
		public static Image FromFile(string filename) {
			if (filename == null || filename == string.Empty)
				return null;
			if (!System.IO.File.Exists(filename))
				return null;
			return FromStream(new FileStream(filename, FileMode.Open));
		}
		
		/// <summary>
		/// Creates a Image from the specified data stream.
		/// </summary>
		/// <param name="stream">A System.IO.Stream that contains the image data.</param>
		/// <returns>The Image this method create.</returns>
		public static Image FromStream(System.IO.Stream stream) {
			Image image = new Image();
			image.xStream = stream;
			image.xImage.SetStream(stream);
			return image;
		}

		public override int GetHashCode() {
			return xStream.GetHashCode();
		}

		public override bool Equals(object obj) {
			Image image = obj as Image;
			if (obj != null) {
				return image.GetHashCode() == GetHashCode(); 
			}
			else {
				return false;
			}
		}

		public override string ToString() {
			return base.ToString ();
		}

		#region IDisposable Members

		public void Dispose() {
			if (xStream != null) {
				xStream.Close();
			}
		}

		#endregion
	}
	#endregion
}
