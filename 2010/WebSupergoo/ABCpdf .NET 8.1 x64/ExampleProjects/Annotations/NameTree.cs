// ===========================================================================
//	�2010 WebSupergoo. All rights reserved.
// ===========================================================================
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.IO;

using WebSupergoo.ABCpdf8;
using WebSupergoo.ABCpdf8.Objects;

namespace WebSupergoo.Annotations {
	/// <summary>Base class for name tree.</summary>
	public abstract class NameTree {
		protected Doc _doc;
		private int _treeId;

		/// <summary>NameTree constructor.</summary>
		/// <param name="doc">A document.</param>
		protected NameTree(Doc doc) {
			_doc = doc;
		}

		/// <summary>The name of name tree.</summary>
		protected abstract string TreeName { get; }

		/// <summary>The ID of name tree.</summary>
		public int Id {
			get {
				InitializeId();
				return _treeId;
			}
		}

		/// <summary>The maximum branching factor of name tree.</summary>
		private int MaxKidCount { get { return 20; } }

		/// <summary>Initialize the ID of name tree.</summary>
		/// <returns>Whether the name tree is newly created.</returns>
		private bool InitializeId() {
			if(_treeId!=0)
				return false;

			string treePath = null;
			string treeName = TreeName;
			if(treeName!=null) {
				treePath = "/Names*/"+treeName+":Ref";
				_treeId = _doc.GetInfoInt(_doc.Root, treePath);
				if(_treeId!=0)
					return false;
			}

			_treeId = _doc.AddObject("<</Names []>>");
			if(treePath!=null)
				_doc.SetInfo(_doc.Root, treePath, _treeId);
			return true;
		}

		/// <summary>Compares a key with an existing key in the tree.</summary>
		/// <param name="key">A key for comparison.</param>
		/// <param name="nodeId">The ID of the node containing an existing key.</param>
		/// <param name="path">The object path of an existing key from the node.</param>
		/// <returns>An integer whose sign indicates the lexical relationship.</returns>
		private int CompareKeys(string key, int nodeId, string path) {
			return string.CompareOrdinal(key, _doc.GetInfo(nodeId, path));
		}

		/// <summary>Finds the leaf descendant node.</summary>
		/// <param name="nodePath">(Output) The list of IDs of ancestor nodes of the search.</param>
		/// <param name="nodeId">The ID of the root node of the search.</param>
		/// <param name="upperLimit">Whether to find the leaf closest to the upper limit (otherwise, to the lower limit).</param>
		/// <param name="outCount">The child count or the key-value pair count of the leaf descendant.</param>
		/// <returns>The ID of the leaf descendant node.</returns>
		private int GetExtremeDescendant(List<int> nodePath, int nodeId, bool upperLimit, out int outCount) {
			outCount = _doc.GetInfoInt(nodeId, "/Kids*:Count");
			if(outCount<=0) {
				outCount = _doc.GetInfoInt(nodeId, "/Names*:Count")/2;
				return nodeId;
			}
			while(true) {
				int childId = _doc.GetInfoInt(nodeId,
					upperLimit? "/Kids*["+(outCount-1).ToString()+"]:Ref": "/Kids*[0]:Ref");
				int count = _doc.GetInfoInt(childId, "/Kids*:Count");
				if(count<=0) {
					count = _doc.GetInfoInt(childId, "/Names*:Count")/2;
					if(count<=0) {
						if(nodePath.Count>0)
							nodePath.RemoveAt(nodePath.Count-1);
						return nodeId;
					}
					nodePath.Add(nodeId);
					outCount = count;
					return childId;
				}
				nodePath.Add(nodeId);
				outCount = count;
				nodeId = childId;
			}
		}

		/// <summary>Finds the node that contains/should contain a key.</summary>
		/// <param name="nodePath">(Output) The list of IDs of ancestor nodes of the search.</param>
		/// <param name="nodeId">The ID of the root node of the search.</param>
		/// <param name="key">A key as the search target.</param>
		/// <param name="outIndex">The index into Kids of the returned node where
		/// the key is found, or the one's complement of the index into Kids of the
		/// returned node where the key should be added. Special internal values
		/// for recursion: Int32.MinValue and Int32.MinValue+1.</param>
		/// <returns>The ID of the node that contains/should contain a key. Special internal
		/// value for recursion: 0.</returns>
		private int FindNode(List<int> nodePath, int nodeId, string key, out int outIndex) {
			if(nodeId!=_treeId) {
				if(CompareKeys(key, nodeId, "/Limits*[1]*:Text")>0) {
					outIndex = int.MinValue;
					return 0;	// return to recursive caller for checking at (*) and (**)
				}
				if(CompareKeys(key, nodeId, "/Limits*[0]*:Text")<0) {
					outIndex = int.MinValue+1;
					return 0;	// return to recursive caller for checking at (*) and (**)
				}
			}
			int count = _doc.GetInfoInt(nodeId, "/Kids*:Count");
			if(count>0) {	// intermediate node
				int nodePathCount = nodePath.Count;
				nodePath.Add(nodeId);
				int prevChildId = 0;
				for(int i = 0; i<count; ++i) {
					int childId = _doc.GetInfoInt(nodeId, "/Kids*["+i.ToString()+"]:Ref");
					if(childId!=0) {
						int v = FindNode(nodePath, childId, key, out outIndex);
						if(v!=0)	// here is (*)
							return v;
						if(outIndex==int.MinValue)	// here is (**)
							prevChildId = childId;
						else {
							if(prevChildId!=0) {
								int prevCount;
								int prevV = GetExtremeDescendant(nodePath, prevChildId, true, out prevCount);
								if(prevCount>0 && prevCount<MaxKidCount) {
									outIndex = ~prevCount;
									return prevV;
								}
								nodePath.RemoveRange(nodePathCount+1, nodePath.Count-(nodePathCount+1));
							}
							v = GetExtremeDescendant(nodePath, childId, false, out count);
							if(count>0) {
								outIndex = ~0;
								return v;
							}
							nodePath.RemoveRange(nodePathCount, nodePath.Count-nodePathCount);
							outIndex = ~i;
							return nodeId;
						}
					}
				}
				if(prevChildId!=0) {
					int prevCount;
					int prevV = GetExtremeDescendant(nodePath, prevChildId, true, out prevCount);
					if(prevCount>0 && prevCount<MaxKidCount) {
						outIndex = ~prevCount;
						return prevV;
					}
				}
				nodePath.RemoveRange(nodePathCount, nodePath.Count-nodePathCount);
				outIndex = ~count;
				return nodeId;
			}
			// leaf node
			count = _doc.GetInfoInt(nodeId, "/Names*:Count");
			for(int i = 0; i<count; i += 2) {
				int compare = CompareKeys(key, nodeId, "/Names*["+i.ToString()+"]*:Text");
				if(compare<=0) {
					outIndex = compare<0? ~(i/2): i/2;
					return nodeId;
				}
			}
			outIndex = ~(count/2);
			return nodeId;
		}

		/// <summary>Extends the limits of all nodes in the node path to contain a key.</summary>
		/// <param name="nodePath">The list of IDs of ancestor nodes.</param>
		/// <param name="nodePathCount">The count of nodePath considered.</param>
		/// <param name="nodeId">The ID of a node whose limits are to be extended.
		/// It is not included in nodePath.</param>
		/// <param name="key">A key.</param>
		/// <param name="upperLimit">Whether to extend upper limits (otherwise, lower limits).</param>
		private void ExtendLimit(List<int> nodePath, int nodePathCount,
			int nodeId, string key, bool upperLimit)
		{
			while(true) {
				Debug.Assert((nodePathCount<=0)==(nodeId==_treeId));
				if(nodePathCount<=0)
					return;

				if(upperLimit) {
					if(CompareKeys(key, nodeId, "/Limits*[1]*:Text")<=0)
						return;
					_doc.SetInfo(nodeId, "/Limits*[1]:Del", "");
					_doc.SetInfo(nodeId, "/Limits*[1]:Text", key);
				} else {
					if(CompareKeys(key, nodeId, "/Limits*[0]*:Text")>=0)
						return;
					_doc.SetInfo(nodeId, "/Limits*[0]:Del", "");
					_doc.SetInfo(nodeId, "/Limits*[0]:Text", key);
				}
				nodeId = nodePath[--nodePathCount];
			}
		}

		/// <summary>Splits an internal node (and its ancestors) to two if the branching factor is above the maximum.</summary>
		/// <param name="nodePath">The list of IDs of ancestor nodes.</param>
		/// <param name="nodePathCount">The count of nodePath considered.</param>
		/// <param name="nodeId">The ID of a node to be splitted.
		/// It is not included in nodePath.</param>
		private void SplitInternalNode(List<int> nodePath, int nodePathCount, int nodeId) {
recur:
			int count = _doc.GetInfoInt(nodeId, "/Kids*:Count");
			if(count<=MaxKidCount)
				return;

			int id = _doc.AddObject("<</Kids [] /Limits []>>");
			int index = count-(MaxKidCount+1)/2;
			string limit = _doc.GetInfo(nodeId, "/Kids*["+index.ToString()+"]*/Limits*[0]");
			_doc.SetInfo(id, "/Limits*[]", limit);

			Debug.Assert((nodePathCount<=0)==(nodeId==_treeId));
			if(nodePathCount<=0) {
				limit = _doc.GetInfo(nodeId, "/Kids*["+(count-1).ToString()+"]*/Limits*[1]");
				_doc.SetInfo(id, "/Limits*[]", limit);

				int id0 = _doc.AddObject("<</Kids [] /Limits []>>");
				limit = _doc.GetInfo(nodeId, "/Kids*[0]*/Limits[0]");
				_doc.SetInfo(id0, "/Limits*[]", limit);
				limit = _doc.GetInfo(nodeId, "/Kids*["+(index-1).ToString()+"]*/Limits*[1]");
				_doc.SetInfo(id0, "/Limits*[]", limit);

				for(int i = 0; i<index; ++i) {
					string kid = _doc.GetInfo(nodeId, "/Kids*["+i.ToString()+"]");
					_doc.SetInfo(id0, "/Kids*[]", kid);
				}
				for(int i = index; i<count; ++i) {
					string kid = _doc.GetInfo(nodeId, "/Kids*["+i.ToString()+"]");
					_doc.SetInfo(id, "/Kids*[]", kid);
				}
				_doc.SetInfo(nodeId, "/Kids:Del", "");
				_doc.SetInfo(nodeId, "/Kids*[]:Ref", id0);
				_doc.SetInfo(nodeId, "/Kids*[]:Ref", id);
			} else {
				limit = _doc.GetInfo(nodeId, "/Limits*[1]");
				_doc.SetInfo(id, "/Limits*[]", limit);

				limit = _doc.GetInfo(nodeId, "/Kids*["+(index-1).ToString()+"]*/Limits*[1]");
				_doc.SetInfo(nodeId, "/Limits*[1]:Del", "");
				_doc.SetInfo(nodeId, "/Limits*[1]", limit);

				for(int i = index; i<count; ++i) {
					string kid = _doc.GetInfo(nodeId, "/Kids*["+i.ToString()+"]");
					_doc.SetInfo(id, "/Kids*[]", kid);
				}
				for(int i = count; i>index; ) {
					--i;
					_doc.SetInfo(nodeId, "/Kids*[-1]:Del", "");
				}
				int parentId = nodePath[nodePathCount-1];
				count = _doc.GetInfoInt(parentId, "/Kids*:Count");
				for(int i = count; i>0; ) {
					--i;
					if(_doc.GetInfoInt(parentId, "/Kids*["+i.ToString()+"]:Ref")==nodeId) {
						_doc.SetInfo(parentId, "/Kids*["+(i+1).ToString()+"]:Ref", id);
						--nodePathCount;
						nodeId = parentId;
						goto recur;
					}
				}
				Debug.Fail("Child is not found in parent.");
			}
		}

		/// <summary>Splits a leaf node (and its ancestors) to two if the branching factor is above the maximum.</summary>
		/// <param name="nodePath">The list of IDs of ancestor nodes.</param>
		/// <param name="nodeId">The ID of a node to be splitted.
		/// It is not included in nodePath.</param>
		private void SplitLeafNode(List<int> nodePath, int nodeId) {
			int count = _doc.GetInfoInt(nodeId, "/Names*:Count");
			if(count<=2*MaxKidCount+1)
				return;

			int id = _doc.AddObject("<</Names [] /Limits []>>");
			int index = count-(MaxKidCount+MaxKidCount%2);
			string limit = _doc.GetInfo(nodeId, "/Names*["+index.ToString()+"]");
			_doc.SetInfo(id, "/Limits*[]", limit);

			Debug.Assert((nodePath.Count<=0)==(nodeId==_treeId));
			if(nodePath.Count<=0) {
				limit = _doc.GetInfo(nodeId, "/Names*["+(count-count%2-2).ToString()+"]");
				_doc.SetInfo(id, "/Limits*[]", limit);

				int id0 = _doc.AddObject("<</Names [] /Limits []>>");
				limit = _doc.GetInfo(nodeId, "/Names*[0]");
				_doc.SetInfo(id0, "/Limits*[]", limit);
				limit = _doc.GetInfo(nodeId, "/Names*["+(index-2).ToString()+"]");
				_doc.SetInfo(id0, "/Limits*[]", limit);

				for(int i = 0; i<index; ++i) {
					string name = _doc.GetInfo(nodeId, "/Names*["+i.ToString()+"]");
					_doc.SetInfo(id0, "/Names*[]", name);
				}
				for(int i = index; i<count; ++i) {
					string name = _doc.GetInfo(nodeId, "/Names*["+i.ToString()+"]");
					_doc.SetInfo(id, "/Names*[]", name);
				}
				_doc.SetInfo(nodeId, "/Names:Del", "");
				_doc.SetInfo(nodeId, "/Kids*[]:Ref", id0);
				_doc.SetInfo(nodeId, "/Kids*[]:Ref", id);
			} else {
				limit = _doc.GetInfo(nodeId, "/Limits*[1]");
				_doc.SetInfo(id, "/Limits*[]", limit);

				limit = _doc.GetInfo(nodeId, "/Names*["+(index-2).ToString()+"]");
				_doc.SetInfo(nodeId, "/Limits*[1]:Del", "");
				_doc.SetInfo(nodeId, "/Limits*[1]", limit);

				for(int i = index; i<count; ++i) {
					string name = _doc.GetInfo(nodeId, "/Names*["+i.ToString()+"]");
					_doc.SetInfo(id, "/Names*[]", name);
				}
				for(int i = count; i>index; ) {
					--i;
					_doc.SetInfo(nodeId, "/Names*[-1]:Del", "");
				}
				int parentId = nodePath[nodePath.Count-1];
				count = _doc.GetInfoInt(parentId, "/Kids*:Count");
				for(int i = count; i>0; ) {
					--i;
					if(_doc.GetInfoInt(parentId, "/Kids*["+i.ToString()+"]:Ref")==nodeId) {
						_doc.SetInfo(parentId, "/Kids*["+(i+1).ToString()+"]:Ref", id);
						SplitInternalNode(nodePath, nodePath.Count-1, parentId);
						return;
					}
				}
				Debug.Fail("Child is not found in parent.");
			}
		}

		private delegate T GetInfoDelegate<T>(int id, string type);
		private bool RemoveNameTreeValue<T>(string key, GetInfoDelegate<T> getInfo,
			string namesPathSuffix, ref T outV)
		{
			if(InitializeId())
				return false;

			List<int> nodePath = new List<int>();
			int index;
			int nodeId = FindNode(nodePath, _treeId, key, out index);
			if(index<0)
				return false;

			// name already exists
			string sIndex = (2*index).ToString();
			string pathDel = "/Names*["+sIndex+"]:Del";
			_doc.SetInfo(nodeId, pathDel, "");
			outV = getInfo(nodeId, "/Names*["+sIndex+namesPathSuffix);
			_doc.SetInfo(nodeId, pathDel, "");

			UpdateNameTreeAfterRemoval(nodePath, nodeId, index);
			return true;
		}
		private void UpdateNameTreeAfterRemoval(List<int> nodePath, int nodeId, int index) {
			Debug.Assert(nodePath.Count<=0 || nodePath[0]==_treeId);
			int i = nodePath.Count;
			int childId = nodeId;	// the node for getting limits
			int count = _doc.GetInfoInt(nodeId, "/Names*:Count")/2;
			if(count<=0) {	// delete empty node and empty ancestors
				while(i>0) {
					childId = nodeId;
					nodeId = nodePath[--i];
					count = _doc.GetInfoInt(nodeId, "/Kids*:Count");
					string sIndex = null;
					for(index = 0; index<count; ++index) {
						sIndex = index.ToString();
						if(_doc.GetInfoInt(nodeId, "/Kids*["+sIndex+"]:Ref")==childId)
							break;
					}
					if(index>=count) {	// impossible
						Debug.Fail("Child is not found in parent.");
						return;
					}
					_doc.SetInfo(nodeId, "/Kids*["+sIndex+"]:Del", "");
					if(count>1)	// not empty
						break;
				}
				if(nodeId!=_treeId && (index<=0 || index+1>=count)) {
					// prepare for the update of limits
					nodePath.RemoveRange(i, nodePath.Count-i);
					childId = GetExtremeDescendant(
						nodePath, nodeId, index>0, out count);
					if(childId==nodeId || count<=0)
						return;
					count = _doc.GetInfoInt(childId, "/Names*:Count")/2;
					if(count<=0)
						return;
					if(index>0)
						index = count;
				}
			}
			if(nodeId==_treeId)	// does not have /Limits
				return;
			string key;
			if(index<=0) {	// set lower limit
				key = _doc.GetInfo(childId, "/Names*[0]*:Text");
				do {
					_doc.SetInfo(nodeId, "/Limits*[0]:Del", "");
					_doc.SetInfo(nodeId, "/Limits*[0]:Text", key);
					if(i<=0)
						break;
					childId = nodeId;
					nodeId = nodePath[--i];
				} while(nodeId!=_treeId && _doc.GetInfoInt(
					nodeId, "/Kids*[0]:Ref")==childId);
			} else if(index>=count) {	// set upper limit
				key = _doc.GetInfo(childId, "/Names*["
					+(2*(index-1)).ToString()+"]*:Text");
				do {
					_doc.SetInfo(nodeId, "/Limits*[1]:Del", "");
					_doc.SetInfo(nodeId, "/Limits*[1]:Text", key);
					if(i<=0)
						break;
					childId = nodeId;
					nodeId = nodePath[--i];
				} while(nodeId!=_treeId && _doc.GetInfoInt(
					nodeId, "/Kids*[-1]:Ref")==childId);
			}
		}

		/// <summary>Removes an entry.</summary>
		/// <param name="key">A key that uniquely identifies a value in the name tree.</param>
		/// <returns>The value assigned to the key.</returns>
		protected string RemoveNameTreeValue(string key) {
			string v = null;
			RemoveNameTreeValue(key, _doc.GetInfo, "]", ref v);
			return v;
		}

		/// <summary>Removes an entry.</summary>
		/// <param name="key">A key that uniquely identifies a value in the name tree.</param>
		/// <returns>The ID of an object whose reference is assigned to the key.</returns>
		protected int? RemoveNameTreeId(string key) {
			int v = 0;
			return RemoveNameTreeValue(key, _doc.GetInfoInt, "]:Ref", ref v)? v: new int?();
		}

		private delegate void SetInfoDelegate<T>(int id, string type, T info);
		private void SetNameTreeValue<T>(string key, T value,
			SetInfoDelegate<T> setInfo, string addNamesPath, string namesPathSuffix)
		{
			if(InitializeId()) {
				_doc.SetInfo(_treeId, "/Names*[]:Text", key);
				setInfo(_treeId, addNamesPath, value);
				return;
			}
			string sIndex;
			List<int> nodePath = new List<int>();
			int index;
			int nodeId = FindNode(nodePath, _treeId, key, out index);
			if(index>=0) {	// name already exists
				sIndex = (2*index+1).ToString();
				_doc.SetInfo(nodeId, "/Names*["+sIndex+"]:Del", "");
				setInfo(nodeId, "/Names*["+sIndex+namesPathSuffix, value);
				return;
			}

			index = ~index;
			if(_doc.GetInfo(nodeId, "/Kids")=="") {	// leaf node
				sIndex = (2*index).ToString();
				setInfo(nodeId, "/Names*["+sIndex+namesPathSuffix, value);
				_doc.SetInfo(nodeId, "/Names*["+sIndex+"]:Text", key);
				ExtendLimit(nodePath, nodePath.Count, nodeId, key, index>0);
				SplitLeafNode(nodePath, nodeId);
				return;
			}

			// internal node
			int childId = _doc.AddObject("<</Names [] /Limits []>>");
			_doc.SetInfo(childId, "/Names*[]:Text", key);
			setInfo(childId, addNamesPath, value);
			_doc.SetInfo(childId, "/Limits*[]:Text", key);
			_doc.SetInfo(childId, "/Limits*[]:Text", key);
			_doc.SetInfo(nodeId, "/Kids*["+index.ToString()+"]:Ref", childId);
			ExtendLimit(nodePath, nodePath.Count, nodeId, key, index>0);
			SplitInternalNode(nodePath, nodePath.Count-1, nodeId);
		}

		/// <summary>Sets the value of a key.</summary>
		/// <param name="key">A key that uniquely identifies a value in the name tree.</param>
		/// <param name="value">The value to be assigned.</param>
		protected void SetNameTreeValue(string key, string value) {
			SetNameTreeValue(key, value, _doc.SetInfo, "/Names*[]", "]");
		}

		/// <summary>Sets the value of a key to a reference.</summary>
		/// <param name="key">A key that uniquely identifies a value in the name tree.</param>
		/// <param name="value">The ID of an object whose reference is to be assigned.</param>
		protected void SetNameTreeId(string key, int value) {
			SetNameTreeValue(key, value, _doc.SetInfo, "/Names*[]:Ref", "]:Ref");
		}

		/// <summary>Embeds a file.</summary>
		/// <param name="info">A file info.</param>
		/// <returns>The ID of the embedded file stream.</returns>
		protected int EmbedFile(FileInfo info) {
			string filePath = info.FullName;
			byte[] buffer = File.ReadAllBytes(filePath);
			int streamId = _doc.AddObject("<<>>stream\r\nendstream\r\n");
			StreamObject obj = (StreamObject)_doc.ObjectSoup[streamId];
			obj.SetData(buffer);

			if(buffer.Length>64)
				_doc.GetInfo(streamId, "Compress");
			_doc.SetInfo(streamId, "/Type:Name", "EmbeddedFile");
			string contentType = GetContentType(filePath);
			if(contentType!=null)
				_doc.SetInfo(streamId, "/Subtype:Name", contentType);
			_doc.SetInfo(streamId, "/Params/Size:Num", (int)info.Length);
			_doc.SetInfo(streamId, "/Params/ModDate:Text", info.LastWriteTimeUtc);
			_doc.SetInfo(streamId, "/Params/CreationDate:Text", info.CreationTimeUtc);
			return streamId;
		}

		/// <summary>Gets the content type of a file.</summary>
		/// <param name="fileName">A file name or path.</param>
		/// <returns>The content type.</returns>
		protected static string GetContentType(string fileName) {
			if(fileName.EndsWith(".pdf", StringComparison.OrdinalIgnoreCase))
				return "application/pdf";
			if(fileName.EndsWith(".swf", StringComparison.OrdinalIgnoreCase))
				return "application/x-shockwave-flash";
			if(fileName.EndsWith(".wmv", StringComparison.OrdinalIgnoreCase))
				return "video/x-ms-wmv";
			if(fileName.EndsWith(".mpg", StringComparison.OrdinalIgnoreCase))
				return "video/mpeg";
			if(fileName.EndsWith(".avi", StringComparison.OrdinalIgnoreCase))
				return "video/avi";
			return null;
		}
	}

	/// <summary>The name tree for embedded file streams.</summary>
	public class EmbeddedFileTree: NameTree {
		/// <summary>EmbeddedFileTree constructor.</summary>
		/// <param name="doc">A document.</param>
		public EmbeddedFileTree(Doc doc) : base(doc) { }

		/// <summary>The name of name tree.</summary>
		protected override string TreeName { get { return "EmbeddedFiles"; } }

		/// <summary>Embeds a file and adds it to the name tree.</summary>
		/// <param name="key">A key that uniquely identifies an embedded file in the name tree.</param>
		/// <param name="filePath">A file path.</param>
		/// <param name="description">A description.</param>
		public void EmbedFile(string key, string filePath, string description) {
			FileInfo info = new FileInfo(filePath);
			int streamId = EmbedFile(info);
			int fileSpec = _doc.AddObject("<</Type /Filespec>>");
			_doc.SetInfo(fileSpec, "/F:Text", info.Name);
			_doc.SetInfo(fileSpec, "/EF/F:Ref", streamId);
			if(description!=null)
				_doc.SetInfo(fileSpec, "/Desc:Text", description);

			SetNameTreeId(key, fileSpec);
		}
	}

	/// <summary>The name tree for document-level JavaScript actions.</summary>
	public class JavaScriptTree: NameTree {
		/// <summary>JavaScriptTree constructor.</summary>
		/// <param name="doc">A document.</param>
		public JavaScriptTree(Doc doc) : base(doc) { }

		/// <summary>The name of name tree.</summary>
		protected override string TreeName { get { return "JavaScript"; } }

		/// <summary>Adds a JavaScript action to the name tree.</summary>
		/// <param name="key">A key that uniquely identifies a JavaScript action in the name tree.</param>
		/// <param name="script">JavaScript code.</param>
		public void AddScript(string key, string script) {
			AddScript(key, script, script.Length>128);
		}

		/// <summary>Adds a JavaScript action to the name tree.</summary>
		/// <param name="key">A key that uniquely identifies a JavaScript action in the name tree.</param>
		/// <param name="script">JavaScript code.</param>
		/// <param name="useStream">Whether to use a stream (otherwise, a string) to store JavaScript code.</param>
		public void AddScript(string key, string script, bool useStream) {
			int action = _doc.AddObject("<</Type /Action /S /JavaScript>>");
			if(!useStream)
				_doc.SetInfo(action, "/JS:Text", script);
			else {
				int streamId = _doc.AddObject("<<>>stream\r\nendstream\r\n");
				_doc.SetInfo(streamId, "Stream", script);
				if(script.Length>64)
					_doc.GetInfo(streamId, "Compress");
				_doc.SetInfo(action, "/JS:Ref", streamId);
			}

			SetNameTreeId(key, action);
		}
	}
}
