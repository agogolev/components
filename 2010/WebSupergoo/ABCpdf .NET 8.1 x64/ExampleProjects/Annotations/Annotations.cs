// ===========================================================================
//	©2006 WebSupergoo. All rights reserved.
// ===========================================================================

using System;
using System.Globalization;
using System.Text;
using System.IO;
using System.Security.Cryptography.X509Certificates;

using WebSupergoo.ABCpdf8;
using WebSupergoo.ABCpdf8.Objects;


namespace WebSupergoo.Annotations
{
	/// <summary>
	/// Base class for all types of annotations
	/// </summary>
	public class Annotation
	{
		/// <summary>
		/// Constructor. Creates annotation and registers it in the page Annots[] array
		/// </summary>
		/// <param name="doc">Document to add annotation to</param>
		protected Annotation(Doc doc)
		{
			System.Diagnostics.Debug.Assert(doc.Page != 0);
			mDoc = doc;
			mId = mDoc.AddObject("<</Type /Annot /F 4>>");
			InteractiveForm.AddToPageAnnots(mDoc, mId);
			mDoc.SetInfo(mId, "/P:Ref", mDoc.Page);
		}

		/// <summary>
		/// Construct annotation from the existing object id
		/// </summary>
		/// <param name="fieldId">Annotation ID</param>
		/// <param name="doc">Document in which annotation exists</param>
		internal Annotation(int fieldId, Doc doc)
		{
			mId = fieldId;
			mDoc = doc;
		}

		/// <summary>
		/// Internal routine for assigning color
		/// </summary>
		/// <param name="id">ID of object</param>
		/// <param name="delPath">Path ending with ":Del" for deletion of the entry</param>
		/// <param name="itemPath">Path ending with "[]:Num" for adding a color component</param>
		/// <param name="colorString">Color description in the same format as XColor.String</param>
		internal void SetColor(int id, string delPath, string itemPath, string colorString)
		{
			XColor color = new XColor();
			color.String = colorString;
			mDoc.SetInfo(id, delPath, "");
			mDoc.SetInfo(id, itemPath, (double)color.Red / 255);
			mDoc.SetInfo(id, itemPath, (double)color.Green / 255);
			mDoc.SetInfo(id, itemPath, (double)color.Blue / 255);
		}

		/// <summary>
		/// Border width of the annotation
		/// </summary>
		public double BorderWidth
		{
			set { mDoc.SetInfo(mId, "/BS/W:Num", value); }
		}

		/// <summary>
		/// Border style of the annotation. Possible values are Solid, Dashed, Beveled, Inset and Underline
		/// </summary>
		public string BorderStyle
		{
			set 
			{
				if (value == "Solid" || value == "Dashed" || value == "Beveled"
					|| value == "Inset" || value == "Underline")
					mDoc.SetInfo(mId, "/BS/S:Name", value.Substring(0, 1));
			}
		}

		/// <summary>
		/// Dash pattern of the Border. Used when Borderstyle is "Dashed"
		/// </summary>
		public string BorderDash
		{
			set { mDoc.SetInfo(mId, "/BS/D", value); }
		}

		/// <summary>
		/// Bounding rectangle of the annotation
		/// </summary>
		public string Rect
		{
			set { mDoc.SetInfo(mId, "/Rect:Rect", value); }
		}

		/// <summary>
		/// Color of the annotation. This color is used for the following purposes
		/// - the background of the annotationís icon when closed 
		/// - The title bar of the annotationís pop-up window 
		/// - The border of a link annotation 
		/// </summary>
		public string Color
		{
			set { SetColor(mId, "/C:Del", "/C[]:Num", value); }
		}

		/// <summary>
		/// Text to be displayed for the annotation 
		/// </summary>
		public string Contents
		{
			set	{ mDoc.SetInfo(mId, "/Contents:Text", value); }
		}

		/// <summary>
		/// Internal routine for file embedding
		/// </summary>
		/// <param name="info">File info</param>
		/// <returns>ID of the newly created stream</returns>
		protected int EmbedFile(FileInfo info) {
			string filePath = info.FullName;
			byte[] buffer = File.ReadAllBytes(filePath);
			int streamId = mDoc.AddObject("<<>>stream\nendstream\n");
			StreamObject obj = (StreamObject)mDoc.ObjectSoup[streamId];
			obj.SetData(buffer);

			if(buffer.Length>64)
				mDoc.GetInfo(streamId, "Compress");
			mDoc.SetInfo(streamId, "/Type:Name", "EmbeddedFile");
			string contentType = GetContentType(filePath);
			if(contentType!=null)
				mDoc.SetInfo(streamId, "/Subtype:Name", contentType);
			mDoc.SetInfo(streamId, "/Params/Size:Num", (int)info.Length);
			mDoc.SetInfo(streamId, "/Params/ModDate:Text", info.LastWriteTimeUtc);
			mDoc.SetInfo(streamId, "/Params/CreationDate:Text", info.CreationTimeUtc);
			return streamId;
		}

		/// <summary>
		/// Internal routine for getting content type
		/// </summary>
		/// <param name="fileName">File name or path</param>
		/// <returns>Content type</returns>
		protected static string GetContentType(string fileName) {
			return GetContentType(fileName, fileName.Length);
		}
		/// <summary>
		/// Internal routine for getting content type
		/// </summary>
		/// <param name="fileName">File name or path</param>
		/// <param name="end">End index of the file name or path</param>
		/// <returns>Content type</returns>
		protected static string GetContentType(string fileName, int end) {
			if(end<4)
				return null;
			int index = end-4;
			if(string.Compare(fileName, index, ".pdf", 0, 4, StringComparison.OrdinalIgnoreCase)==0)
				return "application/pdf";
			if(string.Compare(fileName, index, ".swf", 0, 4, StringComparison.OrdinalIgnoreCase)==0)
				return "application/x-shockwave-flash";
			if(string.Compare(fileName, index, ".wmv", 0, 4, StringComparison.OrdinalIgnoreCase)==0)
				return "video/x-ms-wmv";
			if(string.Compare(fileName, index, ".mpg", 0, 4, StringComparison.OrdinalIgnoreCase)==0)
				return "video/mpeg";
			if (string.Compare(fileName, index, ".avi", 0, 4, StringComparison.OrdinalIgnoreCase) == 0)
				return "video/avi";
			if (string.Compare(fileName, index, ".mp4", 0, 4, StringComparison.OrdinalIgnoreCase) == 0)
				return "video/mp4";
			return null;
		}

		/// <summary>
		/// Update the appearance stream for the annotation.
		/// </summary>
		public void UpdateAppearance() {
			IndirectObject io = mDoc.ObjectSoup[mId];
			if (io is ABCpdf8.Objects.Annotation) {
				ABCpdf8.Objects.Annotation a = (ABCpdf8.Objects.Annotation)io;
				a.UpdateAppearance();
			}
		}

		/// <summary>
		/// ID of the annotation object
		/// </summary>
		public int Id
		{
			get {return mId;}
		}

		protected int mId;
		protected Doc mDoc;
	}

	/// <summary>
	/// Form field annotation
	/// </summary>
	public class FormField: Annotation
	{
		/// <summary>
		/// Flags which can be used for different form fields
		/// </summary>
		public enum FieldFlag
		{
			//Buttons
			NoToggleToOff = 15,
			Radio = 16,
			Pushbutton = 17, 
			RadiosInUnison = 26,
			//Text
			Multiline = 13,
			Password = 14,
			FileSelect = 21,
			DoNotSpellCheck = 23,
			DoNotScroll = 24,
			Comb = 25,
			RichText = 26,
			//Choice fields
			Combo = 18,
			Edit = 19,
			Sort = 20,
			MultiSelect = 22,
			CommitOnSelChange = 27
		}

		/// <summary>
		/// Form field constructor
		/// </summary>
		/// <param name="fieldId">Field ID</param>
		/// <param name="doc">Parent document</param>
		internal FormField(int fieldId, Doc doc) : base(fieldId, doc) { }

		/// <summary>
		/// This property is specific for the choice filed objects
		/// </summary>
		/// <param name="options">Array of possible option strings</param>
		public void AddOptions(string[] options)
		{
			mDoc.SetInfo(mId, "/Opt:Del", "");
			foreach (string item in options)
				mDoc.SetInfo(mId, "/Opt[]:Text", item);
		}
		/// <summary>
		/// Set annotation flag
		/// </summary>
		/// <param name="flag">See FieldFlag enum for description of the flags</param>
		public void SetFlag(FieldFlag flag)
		{
			int flags = mDoc.GetInfoInt(mId, "/Ff:Num");
			flags |= 1 << ((int)flag - 1);
			Flags = flags;
		}

		/// <summary>
		/// Annotation flags
		/// </summary>
		public int Flags
		{
			set { mDoc.SetInfo(mId, "/Ff:Num", value); }
			get { return mDoc.GetInfoInt(mId, "/Ff:Num"); }
		}

		/// <summary>
		/// The default appearance string containing a sequence of valid
		/// page-content graphics or text state operators that define such 
		/// properties as the field text size and color
		/// </summary>
		public string DefaultAppearance
		{
			set { mDoc.SetInfo(mId, "/DA:Text", value);}
		}
	
		/// <summary>
		/// Border color of the form field
		/// </summary>
		public string BorderColor
		{
			set { SetColor(mId, "/MK/BC:Del", "/MK/BC[]:Num", value); }
		}
		/// <summary>
		/// Fill color of the form field
		/// </summary>
		public string FillColor
		{
			set { SetColor(mId, "/MK/BG:Del", "/MK/BG[]:Num", value); }
		}

		/// <summary>
		/// Text align for the form field
		/// </summary>
		public string TextAlign 
		{
			set 
			{
				if (value == "Left")
					mDoc.SetInfo(mId, "/Q:Num", "0");
				else if (value == "Center")
					mDoc.SetInfo(mId, "/Q:Num", "1");
				else if (value == "Right")
					mDoc.SetInfo(mId, "/Q:Num", "2");
			}
		}

		/// <summary>
		/// The number of degrees by which the form field annotation is rotatedcounterclockwise
		///  relative to the page. The value must be a multiple of 90.Default value: 0 
		/// </summary>
		public int Rotate
		{
			set { mDoc.SetInfo(mId, "/MK/R:Num", value); }
		}
	}

	/// <summary>
	/// Interactive form class. This class is used to add different types of the form fields
	/// </summary>
	public class InteractiveForm
	{
		private delegate void SignMethod(Signature sig);

		/// <summary>
		/// Interactive form class constructor. Adds AcroForm key to the document
		/// </summary>
		/// <param name="inDoc">Parent doc</param>
		public InteractiveForm(Doc inDoc)
		{
			mDoc = inDoc;
			int eid = mDoc.GetInfoInt(mDoc.Root, "/AcroForm:Ref");
			if (eid == 0) {
				string theVal = mDoc.GetInfo(mDoc.Root, "/AcroForm");
				eid = mDoc.AddObject(theVal != ""? theVal: "<</Fields []>>");
				mDoc.SetInfo(mDoc.Root, "/AcroForm:Ref", eid);
				if (theVal != "")
					MakeDirect(mDoc, eid, "/Fields", "/Fields:Ref", "/Fields:Del");
			}
			//add font for text fields
			int theFont = mDoc.AddFont("Times-Roman");
			mDoc.SetInfo(eid, "/DR/Font/TimesRoman:Ref", theFont);
		}

		/// <summary>
		/// Add check box form field
		/// </summary>
		/// <param name="inRect">Rectangle of the form field</param>
		/// <param name="inName">Name of the form field</param>
		/// <param name="inValue">Initial value</param>
		/// <returns>Form field object</returns>
		public FormField AddCheckbox(string inRect, string inName, bool inValue)
		{
			MakeDefaultCheckBoxAppearance();
			int fieldId = mDoc.AddObject("<</Type /Annot /Subtype /Widget /F 4 /FT /Btn>>");
			RegisterField(fieldId, inName, inRect);
			mDoc.SetInfo(fieldId, "/AP/N/Yes:Ref", mYesCheckBoxAppearance);
			mDoc.SetInfo(fieldId, "/AP/N/Off:Ref", mOffCheckBoxAppearance);
			mDoc.SetInfo(fieldId, "/MK/CA:Text", mYesCheckBoxCharacter);
			string state = inValue ? "Yes" : "Off";
			mDoc.SetInfo(fieldId, "/V:Name", state);
			mDoc.SetInfo(fieldId, "/AS:Name", state);
			return new FormField(fieldId, mDoc);
		}

		/// <summary>
		/// Add choice form field
		/// </summary>
		/// <param name="type">Type of the choice field. Possible values are Combobox and Listbox</param>
		/// <param name="inRect">Rectangle of the form field</param>
		/// <param name="inName">Name of the form field</param>
		/// <returns>Form field object</returns>
		public FormField AddChoiceField(string type, string inRect, string inName)
		{
			if (type == "ComboBox" || type == "ListBox")
			{
				int fieldId = mDoc.AddObject("<</Type /Annot /Subtype /Widget /F 4 /FT /Ch"
					+ " /Ff 0 /DA (/TimesRoman 12 Tf 0 g) /Opt [] /I [0]"
					+ " /MK <</BG [1] /BC [0 0 0]>>>>");
				if (type == "ComboBox")
					mDoc.SetInfo(fieldId, "/Ff:Num", "131072");
				RegisterField(fieldId, inName, inRect);
				return new FormField(fieldId, mDoc);
			}
			return null;
		}

		/// <summary>
		/// Add text field
		/// </summary>
		/// <param name="inRect">Rectangle of the form field</param>
		/// <param name="inName">Name of the form field</param>
		/// <param name="inText">Initial value</param>
		/// <returns>Form field object</returns>
		public FormField AddTextField(string inRect, string inName, string inText)
		{
			int fieldId = mDoc.AddObject("<</Type /Annot /Subtype /Widget /F 4 /FT /Tx /Ff 4096 /Q 1>>");
			mDoc.SetInfo(fieldId, "/V:Text", inText);
			RegisterField(fieldId, inName, inRect);
			return new FormField(fieldId, mDoc);
		}

		/// <summary>
		/// Whether to certify the document when a signature is added.
		/// In a document certified with any signature,
		/// only the last signed signature is valid,
		/// especially after clicking on the signatures.
		/// </summary>
		public bool Certify {
			get { return mCertify; }
			set { mCertify = value; }
		}

		/// <summary>
		/// Whether to show the validity icon and message.
		/// </summary>
		public bool ShowSignatureValidity {
			get { return mShowSignatureValidity; }
			set { mShowSignatureValidity = value; }
		}

		/// <summary>
		/// Commit the latest signature. Saving the document automatically
		/// commits the signature. AddSignature calls this method appropriately.
		/// You need not call this method directly unless you plan to sign
		/// another signature outside the InteractiveForm class, or you plan
		/// to add contents after signing and want to make sure that the
		/// signature does not sign the later added contents.
		/// </summary>
		public bool CommitSignature() {
			if (mSig == null)
				return false;
			mSig.Commit();
			mSig = null;
			return true;
		}

		/// <summary>
		/// Add signature field
		/// </summary>
		/// <param name="inRect">Rectangle of the form field</param>
		/// <param name="inName">Name of the form field</param>
		/// <param name="inKeyPath">Path to the X.509 file containing the private key</param>
		/// <param name="inKeyPassword">Password required to use the private key</param>
		/// <param name="inReason">Reason why the signature is being signed (may be null)</param>
		/// <param name="inLocation">Location at which the signing is taking place (may be null)</param>
		/// <returns>Form field object</returns>
		public FormField AddSignature(string inRect, string inName, string inKeyPath, 
			string inKeyPassword, string inReason, string inLocation)
		{
			return AddSignature(inRect, inName, inKeyPath, inKeyPassword, inReason, inLocation, null);
		}

		/// <summary>
		/// Add signature field
		/// </summary>
		/// <param name="inRect">Rectangle of the form field</param>
		/// <param name="inName">Name of the form field</param>
		/// <param name="inKeyPath">Path to the X.509 file containing the private key</param>
		/// <param name="inKeyPassword">Password required to use the private key</param>
		/// <param name="inReason">Reason why the signature is being signed (may be null)</param>
		/// <param name="inLocation">Location at which the signing is taking place (may be null)</param>
		/// <param name="inAppearanceTextFormat">Format string of the Text (may be null). {0} is the Signer, {1} is the Date/Time (UTC), {2} is the Reason, and {3} is the Location.</param>
		/// <returns>Form field object</returns>
		public FormField AddSignature(string inRect, string inName, string inKeyPath, string inKeyPassword, 
			string inReason, string inLocation, string inAppearanceTextFormat)
		{
			return AddSignature(inRect, inName, delegate(Signature sig) { sig.Sign(inKeyPath, inKeyPassword); },
				inReason, inLocation, inAppearanceTextFormat);
		}

		/// <summary>
		/// Add signature field
		/// </summary>
		/// <param name="inRect">Rectangle of the form field</param>
		/// <param name="inName">Name of the form field</param>
		/// <param name="inKey">X.509 certificate containing the private key</param>
		/// <param name="inKeySilent">Whether to suppress prompting the user to use the private key</param>
		/// <param name="inReason">Reason why the signature is being signed (may be null)</param>
		/// <param name="inLocation">Location at which the signing is taking place (may be null)</param>
		/// <returns>Form field object</returns>
		public FormField AddSignature(string inRect, string inName, X509Certificate2 inKey, 
			bool inKeySilent, string inReason, string inLocation)
		{
			return AddSignature(inRect, inName, inKey, inKeySilent, inReason, inLocation, null);
		}

		/// <summary>
		/// Add signature field
		/// </summary>
		/// <param name="inRect">Rectangle of the form field</param>
		/// <param name="inName">Name of the form field</param>
		/// <param name="inKey">X.509 certificate containing the private key</param>
		/// <param name="inKeySilent">Whether to suppress prompting the user to use the private key</param>
		/// <param name="inReason">Reason why the signature is being signed (may be null)</param>
		/// <param name="inLocation">Location at which the signing is taking place (may be null)</param>
		/// <param name="inAppearanceTextFormat">Format string of the Text (may be null). {0} is the Signer, {1} is the Date/Time (UTC), {2} is the Reason, and {3} is the Location.</param>
		/// <returns>Form field object</returns>
		public FormField AddSignature(string inRect, string inName, X509Certificate2 inKey, 
			bool inKeySilent, string inReason, string inLocation, string inAppearanceTextFormat)
		{
			return AddSignature(inRect, inName, delegate(Signature sig) { sig.Sign(inKey, inKeySilent); },
				inReason, inLocation, inAppearanceTextFormat);
		}

		private FormField AddSignature(string inRect, string inName, SignMethod sign, 
			string inReason, string inLocation, string inAppearanceTextFormat)
		{
			CommitSignature();

			// NB If you don't want your signature to print then set the /F flag to 0
			int fieldId = mDoc.AddObject("<</Type /Annot /Subtype /Widget /F 4 /FT /Sig"
				+ " /DA (/TimesRoman 0 Tf 0 g)>>");
			int sigDictId = mDoc.AddObject("<</Type /Sig /Filter /Adobe.PPKLite /SubFilter /adbe.pkcs7.detached>>");
			mDoc.SetInfo(fieldId, "/V:Ref", sigDictId);
			RegisterField(fieldId, inName, inRect);

			mDoc.Form.Refresh();
			mSig = (Signature)mDoc.Form[inName];
			sign(mSig);
			if (inReason != null)
				mDoc.SetInfo(sigDictId, "/Reason:Text", inReason);
			if (inLocation != null)
				mDoc.SetInfo(sigDictId, "/Location:Text", inLocation);
			if (mSig.Signer != null)
				mDoc.SetInfo(sigDictId, "/Name:Text", mSig.Signer);


			if (mCertify) {
				// make it PDF 1.6, see the second point under "Validating MDP signatures" in the PDF reference
				mDoc.SetInfo(sigDictId, "version", "6");
				// make signature MDP
				mDoc.SetInfo(sigDictId, "/Reference", "[<</Type /SigRef /TransformMethod /DocMDP"
					+ " /TransformParams <</Type /TransformParams /P 2 /V /1.2>>>>]");
				mDoc.SetInfo(mDoc.Root, "/Perms*/DocMDP:Ref", sigDictId);
			}
			if (inAppearanceTextFormat != null) {
				XRect rect = new XRect();
				rect.String = mDoc.GetInfo(fieldId, "Rect");
				rect.Pin = XRect.Corner.TopLeft;
				rect.Width = 200;	// Change this to fit the Text
				rect.Height = 60;	// Change this to fit the Text
				mDoc.SetInfo(fieldId, "/Rect:Rect", rect.String);
				rect.String = mDoc.GetInfo(fieldId, "Rect");
				rect.Pin = 0;
				rect.Position(0, 0);

				double validityMessageHeight = 0;
				int fontSize = 12;
				if (mShowSignatureValidity) {
					validityMessageHeight = Math.Min(0.3 * rect.Height, 0.1 * rect.Width);
					fontSize = 8;
				}

				string theRect = mDoc.Rect.String;
				int theFont = mDoc.Font;
				int theFontSize = mDoc.FontSize;
				double charSpacing = mDoc.TextStyle.CharSpacing;
				double lineSpacing = mDoc.TextStyle.LineSpacing;
				int pageId = mDoc.Page;

				mDoc.Rect.String = rect.String;
				if (validityMessageHeight != 0)
					mDoc.Rect.Top -= validityMessageHeight;
				int fontId = mDoc.AddFont("Times-Roman");
				mDoc.Font = fontId;
				mDoc.FontSize = fontSize;
				mDoc.TextStyle.CharSpacing = 0;
				mDoc.TextStyle.LineSpacing = 2;
				// Use a detached page to avoid changes to the current page
				// or to the page tree for a new page
				mDoc.Page = mDoc.AddObject("<</Type /Page>>");
				mDoc.SetInfo(mDoc.Page, "/MediaBox:Rect", rect.String);
				string text = string.Format(inAppearanceTextFormat,
					mSig.Signer, mSig.SigningUtcTime, inReason, inLocation);
				int textId = mDoc.AddText(text);
				string textStream = mDoc.GetInfo(textId, "Stream");

				mDoc.Delete(textId);
				mDoc.Delete(mDoc.Page);
				mDoc.Page = pageId;
				mDoc.Rect.String = theRect;
				mDoc.Font = theFont;
				mDoc.FontSize = theFontSize;
				mDoc.TextStyle.CharSpacing = charSpacing;
				mDoc.TextStyle.LineSpacing = lineSpacing;

				int l1 = textStream.IndexOf("/Fabc", 0, textStream.Length);
				int l2 = textStream.IndexOf(" ", l1, textStream.Length - l1);
				string fontResName = textStream.Substring(l1, l2 - l1);

				theRect = rect.String;
				const string xobjString = "<</Type /XObject /Subtype /Form"
					+ " /Resources <</ProcSet [/PDF /Text /ImageB /ImageC /ImageI]>>>>stream\nendstream\n";
				int apId = mDoc.AddObject(xobjString);
				mDoc.SetInfo(apId, "/BBox:Rect", theRect);
				mDoc.SetInfo(apId, "/Resources/Font" + fontResName + ":Ref", fontId);
				mDoc.SetInfo(apId, "Stream", textStream);
				if(textStream.Length > 64)
					mDoc.GetInfo(apId, "Compress");

				if (mShowSignatureValidity) {
					if (mBlankAppearance < 0) {
						mBlankAppearance = mDoc.AddObject("<</Type /XObject /Subtype /Form"
							+ " /BBox [0 0 100 100]>>stream\nendstream\n");
					}

					int validityId = mDoc.AddObject(xobjString);
					mDoc.SetInfo(validityId, "/BBox:Del", "");
					mDoc.SetInfo(validityId, "/BBox[]:Num", rect.Left);
					mDoc.SetInfo(validityId, "/BBox[]:Num", rect.Top - validityMessageHeight);
					mDoc.SetInfo(validityId, "/BBox[]:Num", rect.Right);
					mDoc.SetInfo(validityId, "/BBox[]:Num", rect.Top);
					mDoc.SetInfo(validityId, "/Resources/Font" + fontResName + ":Ref", fontId);

					int frmId = mDoc.AddObject(xobjString);
					mDoc.SetInfo(frmId, "/BBox:Rect", theRect);
					mDoc.SetInfo(frmId, "/Resources/XObject/n0:Ref", mBlankAppearance);
					mDoc.SetInfo(frmId, "/Resources/XObject/n1:Ref", mBlankAppearance);
					mDoc.SetInfo(frmId, "/Resources/XObject/n2:Ref", apId);
					mDoc.SetInfo(frmId, "/Resources/XObject/n3:Ref", mBlankAppearance);
					mDoc.SetInfo(frmId, "/Resources/XObject/n4:Ref", validityId);
					double side = 0.9 * Math.Min(rect.Width, rect.Height);
					mDoc.SetInfo(frmId, "Stream", string.Format(NumberFormatInfo.InvariantInfo,
						"q 1 0 0 1 0 0 cm /n0 Do Q"
						+ "\r\nq {0:0.#####} 0 0 {0:0.#####} {1:0.#####} {2:0.#####} cm /n1 Do Q"
						+ "\r\nq 1 0 0 1 0 0 cm /n2 Do Q"
						+ "\r\nq {0:0.#####} 0 0 {0:0.#####} {1:0.#####} {2:0.#####} cm /n3 Do Q"
						+ "\r\nq 1 0 0 1 0 0 cm /n4 Do Q"
						+ "\r\n", side / 100, (rect.Width - side) / 2, (rect.Height - side) / 2));

					apId = mDoc.AddObject(xobjString);
					mDoc.SetInfo(apId, "/BBox:Rect", theRect);
					mDoc.SetInfo(apId, "/Resources/XObject/FRM:Ref", frmId);
					mDoc.SetInfo(apId, "Stream", "q 1 0 0 1 0 0 cm /FRM Do Q\r\n");
				}

				mDoc.SetInfo(fieldId, "/AP*/N:Ref", apId);
			}

			int eid = mDoc.GetInfoInt(mDoc.Root, "/AcroForm:Ref");
			mDoc.SetInfo(eid, "/SigFlags:Num", "3");
			return new FormField(fieldId, mDoc);
		}

		/// <summary>
		/// Add unsigned signature field
		/// </summary>
		/// <param name="inRect">Rectangle of the form field</param>
		/// <param name="inName">Name of the form field</param>
		/// <returns>Form field object</returns>
		public FormField AddSignature(string inRect, string inName) {
			// NB If you don't want your signature to print then set the /F flag to 0
			int fieldId = mDoc.AddObject("<</Type /Annot /Subtype /Widget /F 4 /FT /Sig"
				+ " /DA (/TimesRoman 0 Tf 0 g)>>");
			int sigDictId = mDoc.AddObject("<</Type /Sig /Filter /Adobe.PPKLite /SubFilter /adbe.pkcs7.detached>>");
			mDoc.SetInfo(fieldId, "/V:Ref", sigDictId);
			RegisterField(fieldId, inName, inRect);

			if (mSig == null)
				mDoc.Form.Refresh();
			else {
				string sigName = mSig.Name;
				mDoc.Form.Refresh();
				mSig = (Signature)mDoc.Form[sigName];
			}

			if (mCertify) {
				// make it PDF 1.6, see the second point under "Validating MDP signatures" in the PDF reference
				mDoc.SetInfo(sigDictId, "Version", "6");
				// make signature MDP
				mDoc.SetInfo(sigDictId, "/Reference", "[<</Type /SigRef /TransformMethod /DocMDP"
					+ " /TransformParams <</Type /TransformParams /P 1 /V /1.2>>>>]");
				mDoc.SetInfo(mDoc.Root, "/Perms*/DocMDP:Ref", sigDictId);
			}
			
			int eid = mDoc.GetInfoInt(mDoc.Root, "/AcroForm:Ref");
			mDoc.SetInfo(eid, "/SigFlags:Num", "3");
			return new FormField(fieldId, mDoc);
		}

		/// <summary>
		/// Add button field
		/// </summary>
		/// <param name="inRect">Rectangle of the form field</param>
		/// <param name="inName">Name of the form field</param>
		/// <param name="inCaption">Caption of the button</param>
		/// <returns>Form field object</returns>
		public FormField AddButton(string inRect, string inName, string inCaption)
		{
			int fieldId = mDoc.AddObject("<</Type /Annot /Subtype /Widget /F 4 /FT /Btn"
				+ " /Ff 65536 /DA (/HeBo 18 Tf 0 g) /MK <</BG [0.752930]>>>>");
			mDoc.SetInfo(fieldId, "/MK/CA:Text", inCaption);
			RegisterField(fieldId, inName, inRect);
			return new FormField(fieldId, mDoc);
		}

		/// <summary>
		/// Add radio button group
		/// </summary>
		/// <param name="inRects">Radio buttons rectangles</param>
		/// <param name="inName">Name of the radio button form field</param>
		/// <param name="inSelectedButton">Number of initially selected button</param>
		/// <returns>Form field object</returns>
		public FormField AddRadioButtonGroup(string[] inRects, string inName, int inSelectedButton)
		{
			MakeDefaultRadioBtnAppearance();
			int fieldId = mDoc.AddObject("<</FT /Btn /Ff 49152 /Kids [] /DA (/ZaDb 0 Tf 0 g)>>");

			mDoc.SetInfo(fieldId, "/T:Text", inName);
			mDoc.SetInfo(fieldId, "/V:Name", inSelectedButton);

			int eid = mDoc.GetInfoInt(mDoc.Root, "/AcroForm:Ref");
			mDoc.SetInfo(eid, "/Fields*[]:Ref", fieldId);

			for (int i=0; i < inRects.Length; i++)
			{
				string buttonName = i.ToString();
				int button = mDoc.AddObject("<</Type /Annot /Subtype /Widget /F 4>>");
				mDoc.SetInfo(button, "/Parent:Ref", fieldId);
				if (i == inSelectedButton)
					mDoc.SetInfo(button, "/AS:Name", buttonName);
				else
					mDoc.SetInfo(button, "/AS:Name", "Off");
				mDoc.SetInfo(button, "/P:Ref", mDoc.Page);
				mDoc.SetInfo(button, "/Rect:Rect", inRects[i]);

				mDoc.SetInfo(button, "/AP/N/" + buttonName + ":Ref", mYesRadioBtnAppearance);
				mDoc.SetInfo(button, "/AP/N/Off:Ref", mOffRadioBtnAppearance);
				AddToPageAnnots(mDoc, button);
				mDoc.SetInfo(fieldId, "/Kids[]:Ref", button);
			}
			return new FormField(fieldId, mDoc);
		}

		/// <summary>
		/// Convert indirect object into direct object
		/// </summary>
		/// <param name="doc">Document</param>
		/// <param name="id">ID of object</param>
		/// <param name="path">Path for setting the value of the entry</param>
		/// <param name="refPath">Path ending with ":Ref" for getting the ID of the reference</param>
		/// <param name="delPath">Path ending with ":Del" for deletion of the entry</param>
		private static void MakeDirect(Doc doc, int id, string path, string refPath, string delPath)
		{
			int target = doc.GetInfoInt(id, refPath);
			if(target==0)
				return;
			if(doc.GetInfoInt(target, "Active")==0) {
				doc.SetInfo(id, delPath, "");
				return;
			}
			string s = doc.GetInfo(target, "*");
			if(s=="")
				doc.SetInfo(id, delPath, "");
			else
				doc.SetInfo(id, path, s);
		}

		/// <summary>
		/// Register annotation in the page Annots array
		/// </summary>
		/// <param name="doc">Document</param>
		/// <param name="annotId">ID of annotation</param>
		internal static void AddToPageAnnots(Doc doc, int annotId) {
			MakeDirect(doc, doc.Page, "/Annots", "/Annots:Ref", "/Annots:Del");
			doc.SetInfo(doc.Page, "/Annots*[]:Ref", annotId);
		}

		/// <summary>
		/// Register form field in the AcroForm dictionary and in the page Annots array
		/// </summary>
		/// <param name="fieldId"></param>
		/// <param name="fieldName"></param>
		/// <param name="fieldRect"></param>
		private void RegisterField(int fieldId, string fieldName,  string fieldRect)
		{
			mDoc.SetInfo(fieldId, "/T:Text", fieldName);
			AddToPageAnnots(mDoc, fieldId);
			mDoc.SetInfo(fieldId, "/P:Ref", mDoc.Page);
			int eid = mDoc.GetInfoInt(mDoc.Root, "/AcroForm:Ref");
			mDoc.SetInfo(eid, "/Fields*[]:Ref", fieldId);
			mDoc.SetInfo(fieldId, "/Rect:Rect", fieldRect);
		}

		/// <summary>
		/// Create default appearances for check box fields
		/// </summary>
		public void MakeDefaultCheckBoxAppearance()
		{
			if (mYesCheckBoxAppearance < 0 && mOffCheckBoxAppearance < 0)
			{
				const string xobjString = "<</Type /XObject /Subtype /Form /BBox [0 0 20 20] /Resources <</ProcSet [/PDF /Text]>>>>stream\nendstream\n";
				const string stream = "q 2 w 1 g 0 0 20 20 re f 0 g 0 0 20 20 re s BT /ZaDb 20 Tf 3 3 Td ({0}) Tj ET Q\r\n"; // with border
				//const string stream = "q 1 w 1 g 0 0 20 20 re f 0 g 1 1 10 10 re s BT /ZaDb 18 Tf 3 4 Td ({0}) Tj ET Q\r\n"; // with inset border
				int theFont = mDoc.AddFont("ZapfDingbats");

				mYesCheckBoxAppearance = mDoc.AddObject(xobjString);
				mDoc.SetInfo(mYesCheckBoxAppearance, "/Resources/Font/ZaDb:Ref", theFont);
				mDoc.SetInfo(mYesCheckBoxAppearance, "Stream", string.Format(stream, mYesCheckBoxCharacter));

				mOffCheckBoxAppearance = mDoc.AddObject(xobjString);
				mDoc.SetInfo(mOffCheckBoxAppearance, "/Resources/Font/ZaDb:Ref", theFont);
				mDoc.SetInfo(mOffCheckBoxAppearance, "Stream", string.Format(stream, " "));
			}
		}

		/// <summary>
		/// Create default appearances for radio button fields
		/// </summary>
		public void MakeDefaultRadioBtnAppearance()
		{
			if (mYesRadioBtnAppearance < 0 && mOffRadioBtnAppearance < 0)
			{
				string theRect = mDoc.Rect.String;
				mDoc.Rect.String = "1 1 19 19";
				int id = mDoc.AddOval(false);
				string circle = mDoc.GetInfo(id, "Stream");
				mDoc.Delete(id);
				string theColor = mDoc.Color.String;
				mDoc.Color.String = "0 0 0";
				mDoc.Rect.String = "5 5 15 15";
				id = mDoc.AddOval(true);
				string pupil = mDoc.GetInfo(id, "Stream");
				mDoc.Delete(id);
				mDoc.Color.String = theColor;
				mDoc.Rect.String = theRect;

				const string xobjString = "<</Type /XObject /Subtype /Form /BBox [0 0 20 20]"
					+ " /Resources <</ProcSet [/PDF /Text]>>>>stream\nendstream\n";
				mYesRadioBtnAppearance = mDoc.AddObject(xobjString);
				mDoc.SetInfo(mYesRadioBtnAppearance, "Stream", circle + pupil);
				mOffRadioBtnAppearance = mDoc.AddObject(xobjString);
				mDoc.SetInfo(mOffRadioBtnAppearance, "Stream", circle);
			}
		}


		private string mYesCheckBoxCharacter = "3"; // 6 cross, 3 tick; see Zapf Dingbats character set for details
		private int mYesCheckBoxAppearance = -1;
		private int mOffCheckBoxAppearance = -1;

		private int mYesRadioBtnAppearance = -1;
		private int mOffRadioBtnAppearance = -1;

		private int mBlankAppearance = -1;

		private Signature mSig = null;
		private bool mCertify = false;
		private bool mShowSignatureValidity = false;

		private Doc mDoc;
	}

	/// <summary>
	/// Polyline annotation
	/// </summary>
	public class PolylineAnnotation : Annotation
	{
		/// <summary>
		/// Add polyline annotation to the current page of the doc
		/// </summary>
		/// <param name="doc">Parent doc</param>
		/// <param name="polygon">Polyline</param>
		/// <param name="borderColor">Line color</param>
		public PolylineAnnotation(Doc doc, string polygon, string borderColor) : base(doc)
		{
			mDoc.SetInfo(mId, "/Subtype:Name", "PolyLine");
			mDoc.SetInfo(mId, "/Vertices", "[" + polygon + "]");
			mDoc.SetInfo(mId, "/Rect:Rect", GetBoundingRect(polygon).String);
			SetColor(mId, "/C:Del", "/C[]:Num", borderColor);
		}

		/// <summary>
		/// Get bounding rectangle of the polyline
		/// </summary>
		/// <param name="verticesString">Vertices of the polyline</param>
		/// <returns>Bounding rectangle</returns>
		protected XRect GetBoundingRect(string verticesString)
		{
			string[] vertices = verticesString.Split(new char[] {' '});
			XRect rect = new XRect();
			rect.Left = double.MaxValue;
			rect.Right = double.MinValue;
			rect.Top = double.MinValue;
			rect.Bottom = double.MaxValue;
			
			for (int i = 0; i < vertices.Length; i++)
			{
				double v = double.Parse(vertices[i], NumberFormatInfo.InvariantInfo);
				if (i%2 == 0)
				{
					if (v < rect.Left)
						rect.Left = v;
					else if (v > rect.Right)
						rect.Right = v;
				}
				else
				{
					if (v < rect.Bottom)
						rect.Bottom = v;
					else if (v > rect.Top)
						rect.Top = v;
				}
			}
			return rect;
		}

		/// <summary>
		/// Line ending styles. Must be a string containing line ending styles
		///  for the beginning and end of the line, e.g. "OpenArrow OpenArrow".
		///  Possible values for lineending styles are: Square, Circle, Diamond,
		///  OpenArrow, ClosedArrow, None, Butt, ROpenArrow, RClosedArrow, Slash
		/// </summary>
		public string LineEndingsStyle
		{
			set 
			{
				string[] styles = value.Split(new char[] {' '});
				if (styles.Length == 2){
					mDoc.SetInfo(mId, "/LE:Del", "");
					mDoc.SetInfo(mId, "/LE[]:Name", styles[0]);
					mDoc.SetInfo(mId, "/LE[]:Name", styles[1]);
				}
			}
		}
	}

	/// <summary>
	/// Polygon annotation
	/// </summary>
	public class PolygonAnnotation : PolylineAnnotation
	{
		/// <summary>
		/// Add polygon annotation to the current page of the doc
		/// </summary>
		/// <param name="doc">Parent doc</param>
		/// <param name="polygon">Polygon vertices</param>
		/// <param name="borderColor">Border color</param>
		/// <param name="fillColor">Fill color</param>
		public PolygonAnnotation(Doc doc, string polygon, string borderColor,
			string fillColor) : base(doc, polygon, borderColor)
		{
			mDoc.SetInfo(mId, "/Subtype:Name", "Polygon");
			SetColor(mId, "/IC:Del", "/IC[]:Num", fillColor);
		}

		/// <summary>
		/// Intensity of the cloudy border effect. Default value: 0
		/// </summary>
		public double CloudyEffect
		{
			set 
			{
				if (value > 0)
				{
					mDoc.SetInfo(mId, "/BE/S:Name", "C");
					mDoc.SetInfo(mId, "/BE/I:Num", value);
				}
			}
		}
	}


	/// <summary>
	/// Stamp annotation
	/// </summary>
	public class StampAnnotation : Annotation
	{
		/// <summary>
		/// Add stamp annotation to the current page of the doc
		/// </summary>
		/// <param name="doc">Parent doc</param>
		/// <param name="rect">Annotation rectangle</param>
		/// <param name="type">Name of the predefined stamp annotation</param>
		public StampAnnotation(Doc doc, string rect, string caption, string color): base(doc)
		{
			GenerateAppearance(caption, color);
			mDoc.SetInfo(mId, "/AP/N:Ref", mAppearance);
			mDoc.SetInfo(mId, "/Subtype:Name", "Stamp");
			mDoc.SetInfo(mId, "/Contents:Text", caption);
			XRect newRect = new XRect();
			newRect.String = rect;
			newRect.Width = newRect.Height * mWidthToHeight;
			mDoc.SetInfo(mId, "/Rect:Rect", newRect.String);
		}

		/// <summary>
		/// Generates appearance stream for the annotation
		/// </summary>
		/// <param name="text">Text to be displayed on the stamp</param>
		/// <param name="color">Stamp color</param>
		public void GenerateAppearance(string text, string color)
		{
			if (mAppearance < 0)
			{
				string theRect = mDoc.Rect.String;
				double theWidth = mDoc.Width;
				string theColor = mDoc.Color.String;
				int theFont = mDoc.Font;
				int fontSize = mDoc.FontSize;
				double charSpacing = mDoc.TextStyle.CharSpacing;
				int alpha= mDoc.Color.Alpha;

				mDoc.Rect.String = "0 0 600 44";
				mDoc.Color.String = color;
				mDoc.Font = mDoc.AddFont("Helvetica-BoldOblique");
				mDoc.FontSize = 28;
				mDoc.Pos.String = "10 35";
				mDoc.TextStyle.CharSpacing = -1;
				int textId = mDoc.AddText(text);
				string caption = mDoc.GetInfo(textId, "Stream");

				int l1 = caption.IndexOf("/Fabc", 0, caption.Length);
				int l2 = caption.IndexOf(" ", l1, caption.Length - l1);
				string fontResName = caption.Substring(l1, l2 - l1);

				XRect rect = new XRect();
				rect.String = mDoc.GetInfo(textId, "Rect");
				mDoc.Delete(textId);
				mDoc.Rect.Width = 23 + rect.Width;

				int rectId = mDoc.FrameRect(10, 10, true);
				string frameRect = mDoc.GetInfo(rectId, "Stream");
				mDoc.Delete(rectId);
			
				mDoc.Color.Alpha = 25;
				rectId = mDoc.FillRect(10, 10);
				mDoc.Color.Alpha = alpha;
				string fillRect = mDoc.GetInfo(rectId, "Stream");
				l1 = fillRect.IndexOf("/Gabc", 0, fillRect.Length);
				l2 = fillRect.IndexOf(" ", l1, fillRect.Length - l1);
				string gsResName = fillRect.Substring(l1, l2 - l1);
				mDoc.Delete(rectId);

				
				mWidthToHeight = mDoc.Rect.Width / mDoc.Rect.Height;
				string xobjString = "<</Type /XObject /Subtype /Form"
					+ " /Resources <</ProcSet [/PDF /Text]>>>>stream\nendstream\n";
				mAppearance = mDoc.AddObject(xobjString);
				mDoc.SetInfo(mAppearance, "/BBox:Rect", mDoc.Rect.String);
				mDoc.SetInfo(mAppearance, "/Resources/Font" + fontResName + ":Ref", mDoc.Font);
				mDoc.SetInfo(mAppearance, "/Resources/ExtGState" + gsResName,
					mDoc.GetInfo(mDoc.Page, "/Resources/ExtGState" + gsResName));
				mDoc.SetInfo(mAppearance, "Stream", caption + frameRect + fillRect);
				mDoc.SetInfo(mDoc.Page, "/Resources/ExtGState" + gsResName + ":Del", "");
				
				mDoc.Color.String = theColor;
				mDoc.Rect.String = theRect;
				mDoc.Width = theWidth;
				mDoc.FontSize = fontSize;
				mDoc.Font = theFont;
				mDoc.TextStyle.CharSpacing = charSpacing;
				mDoc.Color.Alpha = alpha;
			}
		}

		private int mAppearance = -1;
		private double mWidthToHeight = 0;
	}
	/// <summary>
	/// Line annotation
	/// </summary>
	public class LineAnnotation: Annotation
	{
		/// <summary>
		/// Add line annotation to the current page of the doc
		/// </summary>
		/// <param name="doc">Parent doc</param>
		/// <param name="line">Line start and end points</param>
		/// <param name="color">Color of the line</param>
		public LineAnnotation(Doc doc, string line, string color): base(doc)
		{
			mDoc.SetInfo(mId, "/Subtype:Name", "Line");
			mDoc.SetInfo(mId, "/L:Rect", line);
			mDoc.SetInfo(mId, "/Rect:Rect", line);
			SetColor(mId, "/C:Del", "/C[]:Num", color);
		}

		/// <summary>
		/// Line ending styles. Must be a string containing line ending styles
		///  for the beginning and end of the line, e.g. "OpenArrow OpenArrow".
		///  Possible values for lineending styles are: Square, Circle, Diamond,
		///  OpenArrow, ClosedArrow, None, Butt, ROpenArrow, RClosedArrow, Slash
		/// </summary>
		public string LineEndingsStyle
		{
			set 
			{
				string[] styles = value.Split(new char[] {' '});
				if (styles.Length == 2){
					mDoc.SetInfo(mId, "/LE:Del", "");
					mDoc.SetInfo(mId, "/LE[]:Name", styles[0]);
					mDoc.SetInfo(mId, "/LE[]:Name", styles[1]);
				}
			}
		}

		/// <summary>
		/// Line Caption
		/// </summary>
		public string Caption
		{
			set 
			{
				mDoc.SetInfo(mId, "/Cap:Bool", "true");
				mDoc.SetInfo(mId, "/Contents:Text", value);
			}
		}

		/// <summary>
		/// Line caption in rich text format
		/// </summary>
		public string RichTextCaption
		{
			set 
			{
				mDoc.SetInfo(mId, "/Cap:Bool", "true");
				mDoc.SetInfo(mId, "/RC:Text", value);
			}
		}

		/// <summary>
		/// Line fill color
		/// </summary>
		public string FillColor
		{
			set { SetColor(mId, "/IC:Del", "/IC[]:Num", value); }
		}
	}

	/// <summary>
	/// Square annotation
	/// </summary>
	public class SquareAnnotation: Annotation
	{
		/// <summary>
		/// Add square annotation to the current page of the doc
		/// </summary>
		/// <param name="doc">Parent doc</param>
		/// <param name="rect">Annotation rectangle</param>
		/// <param name="borderColor">Border color</param>
		/// <param name="fillColor">Fill color</param>
		public SquareAnnotation(Doc doc, string rect, string borderColor, string fillColor): base(doc)
		{
			mDoc.SetInfo(mId, "/Subtype:Name", "Square");
			mDoc.SetInfo(mId, "/Rect:Rect", rect);
			SetColor(mId, "/C:Del", "/C[]:Num", borderColor);
			SetColor(mId, "/IC:Del", "/IC[]:Num", fillColor);
		}
	}

	/// <summary>
	/// Circle Annotation
	/// </summary>
	public class CircleAnnotation: Annotation
	{
		/// <summary>
		/// Add circle annotation to the current page of the doc
		/// </summary>
		/// <param name="doc">Parent doc</param>
		/// <param name="rect">Annotation rectangle</param>
		/// <param name="borderColor">Border color</param>
		/// <param name="fillColor">Fill color</param>
		public CircleAnnotation(Doc doc, string rect, string borderColor, string fillColor): base(doc)
		{
			mDoc.SetInfo(mId, "/Subtype:Name", "Circle");
			mDoc.SetInfo(mId, "/Rect:Rect", rect);
			SetColor(mId, "/C:Del", "/C[]:Num", borderColor);
			SetColor(mId, "/IC:Del", "/IC[]:Num", fillColor);
		}
	}
	/// <summary>
	/// Movie annotation
	/// Please note that the Adobe PDF Specification now states,
	/// "The features described in this sub-clause are obsolescent and their use is no longer recommended."
	/// The recommended replacement is the SimpleRichMediaAnnotation.
	/// </summary>
	public class MovieAnnotation : Annotation
	{
		/// <summary>
		/// Add movie annotation to the current page of the doc
		/// </summary>
		/// <param name="doc">Parent doc</param>
		/// <param name="rect">Annotation rectangle</param>
		/// <param name="filePathOrUrl">Movie file path or URL</param>
		public MovieAnnotation(Doc doc, string rect, string filePathOrUrl)
			: this(doc, rect, filePathOrUrl, null) { }

		/// <summary>
		/// Add movie annotation to the current page of the doc
		/// </summary>
		/// <param name="doc">Parent doc</param>
		/// <param name="rect">Annotation rectangle</param>
		/// <param name="filePathOrUrl">Movie file path or URL</param>
		/// <param name="contentType">Movie content type</param>
		public MovieAnnotation(Doc doc, string rect, string filePathOrUrl, string contentType)
			: base(doc)
		{
			bool isUrl = filePathOrUrl.StartsWith("http://", StringComparison.OrdinalIgnoreCase)
				|| filePathOrUrl.StartsWith("https://", StringComparison.OrdinalIgnoreCase)
				|| filePathOrUrl.StartsWith("rtmp://", StringComparison.OrdinalIgnoreCase);
			if(contentType==null) {
				int end = -1;
				if(isUrl)
					end = filePathOrUrl.IndexOfAny(new char[] { '?', '#' });
				if(end<0)
					end = filePathOrUrl.Length;
				contentType = GetContentType(filePathOrUrl, end);
				if(contentType == null)
					throw new NotSupportedException("Movie content type is not supported.");
			}

			mDoc.SetInfo(mId, "/Subtype:Name", "Screen");
			mDoc.SetInfo(mId, "/Rect:Rect", rect);

			int fileSpec;
			if(isUrl) {
				fileSpec = mDoc.AddObject("<</Type /Filespec /FS /URL>>");
				mDoc.SetInfo(fileSpec, "/F:Text", filePathOrUrl);
			} else {
				FileInfo fileInfo = new FileInfo(filePathOrUrl);
				int streamId = EmbedFile(fileInfo);
				fileSpec = mDoc.AddObject("<</Type /Filespec>>");
				mDoc.SetInfo(fileSpec, "/F:Text", fileInfo.Name);
				mDoc.SetInfo(fileSpec, "/EF/F:Ref", streamId);
			}

			int mediaClip = mDoc.AddObject("<</S /MCD /P <</TF (TEMPACCESS)>>>>");
			mDoc.SetInfo(mediaClip, "/CT:Text", contentType);
			mDoc.SetInfo(mediaClip, "/D:Ref", fileSpec);

			int rendition = mDoc.AddObject("<</S /MR>>");
			mDoc.SetInfo(rendition, "/C:Ref", mediaClip);

			int pageVisibleAction = mDoc.AddObject("<</S /Rendition /OP 4>>");
			mDoc.SetInfo(pageVisibleAction, "/AN:Ref", mId);
			mDoc.SetInfo(pageVisibleAction, "/R:Ref", rendition);

			mDoc.SetInfo(mId, "/AA*/PV:Ref", pageVisibleAction);
		}
	}

	/// <summary>
	/// File attachment annotation
	/// </summary>
	public class FileAttachmentAnnotation : Annotation
	{
		/// <summary>
		/// Add file attachment annotation to the current page of the doc
		/// </summary>
		/// <param name="doc">Parent doc</param>
		/// <param name="rect">Annotation rectangle</param>
		/// <param name="filePath">File path</param>
		public FileAttachmentAnnotation(Doc doc, string rect, string filePath): base(doc)
		{
			mDoc.SetInfo(mId, "/Subtype:Name", "FileAttachment");
			mDoc.SetInfo(mId, "/Rect:Rect", rect);
			FileInfo fileInfo = new FileInfo(filePath);
			int streamId = EmbedFile(fileInfo);
			int fileSpec = mDoc.AddObject("<</Type /Filespec>>");
			mDoc.SetInfo(fileSpec, "/F:Text", fileInfo.Name);
			mDoc.SetInfo(fileSpec, "/EF/F:Ref", streamId);

			mDoc.SetInfo(mId, "/FS:Ref", fileSpec);
		}
	}
	
	/// <summary>
	/// Popup annotation
	/// </summary>
	public class PopupAnnotation : Annotation
	{
		/// <summary>
		/// Add popup annotation to the current page of the doc
		/// </summary>
		/// <param name="doc">Parent doc</param>
		/// <param name="rect">Annotation rectangle</param>
		/// <param name="parentId">ID of the parent annotation</param>
		public PopupAnnotation(Doc doc, string rect, int parentId) : base(doc)
		{
			mDoc.SetInfo(mId, "/Subtype:Name", "Popup");
			mDoc.SetInfo(mId, "/Parent:Ref", parentId);
			mDoc.SetInfo(mId, "/Rect:Rect", rect);
			mDoc.SetInfo(mId, "/Open:Bool", "true");
		}
	}

	/// <summary>
	/// Text annotation ("Sticky note")
	/// </summary>
	public class TextAnnotation : Annotation
	{
		/// <summary>
		/// Add text annotation to the current page of the doc
		/// </summary>
		/// <param name="doc">Parent doc</param>
		/// <param name="rect1">Annotation rectangle</param>
		/// <param name="rect2">Rectangle of the "sticky note" popup</param>
		/// <param name="contents">Contents of the text annotation</param>
		public TextAnnotation(Doc doc, string rect1, string rect2, string contents) : base(doc)
		{
			mDoc.SetInfo(mId, "/Subtype:Name", "Text");
			mDoc.SetInfo(mId, "/Name:Name", "Comment");

			mDoc.SetInfo(mId, "/Contents:Text", contents);
			mDoc.SetInfo(mId, "/Rect:Rect", rect1);
			if (rect2 != "")
			{
				PopupAnnotation popup = new PopupAnnotation(doc, rect2, mId);
				mDoc.SetInfo(mId, "/Popup:Ref", popup.Id);
			}
		}
	}

	/// <summary>
	/// Text markup annotation
	/// </summary>
	public class TextMarkupAnnotation : Annotation
	{
		/// <summary>
		/// Add text markup annotation to the current page of the doc
		/// </summary>
		/// <param name="doc">Parent doc</param>
		/// <param name="quadPoints">Quad points of the text.</param>
		/// <param name="markupType">Type of the markup annotation. Possible values are Highlight, Underline, Squiggly and StrikeOut</param>
		/// <param name="color">Markup color</param>
		public TextMarkupAnnotation(Doc doc, string quadPoints, string markupType, string color): base(doc)
		{
			mDoc.SetInfo(mId, "/Subtype:Name", markupType);
			mDoc.SetInfo(mId, "/QuadPoints", "[" + quadPoints + "]");
			Color = color;
		}

		/// <summary>
		/// Add text markup annotation to the current page of the doc
		/// </summary>
		/// <param name="doc">Parent doc</param>
		/// <param name="id">ID of the Text object</param>
		/// <param name="markupType">Type of the markup annotation. Possible values are Highlight, Underline, Squiggly and StrikeOut</param>
		/// <param name="color">Markup color</param>
		public TextMarkupAnnotation(Doc doc, int id, string markupType, string color)
			: base(doc) {
			XRect rect = new XRect();
			rect.String = doc.GetInfo(id, "Rect");
			mDoc.SetInfo(mId, "/Subtype:Name", markupType);
			mDoc.SetInfo(mId, "/QuadPoints[]:Num", rect.Left);
			mDoc.SetInfo(mId, "/QuadPoints[]:Num", rect.Top);
			mDoc.SetInfo(mId, "/QuadPoints[]:Num", rect.Right);
			mDoc.SetInfo(mId, "/QuadPoints[]:Num", rect.Top);
			mDoc.SetInfo(mId, "/QuadPoints[]:Num", rect.Left);
			mDoc.SetInfo(mId, "/QuadPoints[]:Num", rect.Bottom);
			mDoc.SetInfo(mId, "/QuadPoints[]:Num", rect.Right);
			mDoc.SetInfo(mId, "/QuadPoints[]:Num", rect.Bottom);
			mDoc.SetInfo(mId, "/Rect:Rect", rect.String);
			Color = color;
		}

		/// <summary>
		/// Add text markup annotation to the current rect of the current page of the doc
		/// </summary>
		/// <param name="doc">Parent doc</param>
		/// <param name="markupType">Type of the markup annotation. Possible values are Highlight, Underline, Squiggly and StrikeOut</param>
		/// <param name="color">Markup color</param>
		public TextMarkupAnnotation(Doc doc, string markupType, string color)
			: base(doc) {
			XRect rect = new XRect();
			rect.String = doc.Rect.String;
			mDoc.SetInfo(mId, "/Subtype:Name", markupType);
			mDoc.SetInfo(mId, "/QuadPoints[]:Num", rect.Left);
			mDoc.SetInfo(mId, "/QuadPoints[]:Num", rect.Top);
			mDoc.SetInfo(mId, "/QuadPoints[]:Num", rect.Right);
			mDoc.SetInfo(mId, "/QuadPoints[]:Num", rect.Top);
			mDoc.SetInfo(mId, "/QuadPoints[]:Num", rect.Left);
			mDoc.SetInfo(mId, "/QuadPoints[]:Num", rect.Bottom);
			mDoc.SetInfo(mId, "/QuadPoints[]:Num", rect.Right);
			mDoc.SetInfo(mId, "/QuadPoints[]:Num", rect.Bottom);
			mDoc.SetInfo(mId, "/Rect:Rect", rect.String);
			Color = color;
		}
	}

	/// <summary>Simple rich media annotation</summary>
	public class SimpleRichMediaAnnotation: Annotation {
		/// <summary>Adds rich media to the current page of the doc</summary>
		/// <param name="doc">Parent doc</param>
		/// <param name="rect">Annotation rectangle</param>
		/// <param name="filePathOrUrl">Media file path or URL</param>
		/// <param name="mediaType">Media type</param>
		public SimpleRichMediaAnnotation(Doc doc, string rect, string filePathOrUrl, string mediaType)
			: base(doc)
		{
			if(mediaType!="Flash" && mediaType!="Video" && mediaType!="Sound" && mediaType!="3D")
				throw new ArgumentException("Invalid media type.", "mediaType");

			mDoc.SetInfo(mId, "/Subtype:Name", "RichMedia");
			mDoc.SetInfo(mId, "/Rect:Rect", rect);

			int fileSpec;
			string fileName;
			if(filePathOrUrl.StartsWith("http://", StringComparison.OrdinalIgnoreCase)
				|| filePathOrUrl.StartsWith("https://", StringComparison.OrdinalIgnoreCase)
				|| filePathOrUrl.StartsWith("rtmp://", StringComparison.OrdinalIgnoreCase))
			{
				fileSpec = mDoc.AddObject("<</Type /Filespec /FS /URL>>");
				fileName = FixUrl(filePathOrUrl);
			} else {
				FileInfo fileInfo = new FileInfo(filePathOrUrl);
				int streamId = EmbedFile(fileInfo);
				fileSpec = mDoc.AddObject("<</Type /Filespec>>");
				fileName = EncodeName(fileInfo.Name);
				mDoc.SetInfo(fileSpec, "/EF/F:Ref", streamId);
			}
			mDoc.SetInfo(fileSpec, "/F:Text", fileName);
			mDoc.SetInfo(fileSpec, "/UF:Text", fileName);

			mDoc.SetInfo(mId, "AdbeExtLevel", "7,3");
			int content = mDoc.AddObject("<</Type /RichMediaContent>>");
			int assets = mDoc.AddObject("<</Names []>>");
			mDoc.SetInfo(assets, "/Names*[]:Text", fileName);
			mDoc.SetInfo(assets, "/Names*[]:Ref", fileSpec);
			mDoc.SetInfo(content, "/Assets:Ref", assets);

			int configuration = mDoc.AddObject("<</Type /RichMediaConfiguration>>");
			mDoc.SetInfo(configuration, "/Subtype:Name", mediaType);
			int instance = mDoc.AddObject("<</Type /RichMediaInstance"
				+ " /Params <</Type /RichMediaParams /Binding /Foreground>>>>");
			mDoc.SetInfo(instance, "/Subtype:Name", mediaType);
			mDoc.SetInfo(instance, "/Asset:Ref", fileSpec);
			mDoc.SetInfo(configuration, "/Instances*[]:Ref", instance);

			mDoc.SetInfo(content, "/Configurations*[]:Ref", configuration);
			mDoc.SetInfo(mId, "/RichMediaContent:Ref", content);

			int settings = mDoc.AddObject("<</Type /RichMediaSettings"
				+ " /Activation <</Type /RichMediaActivation>>>>");
			mDoc.SetInfo(settings, "/Activation*/Configuration:Ref", configuration);
			mDoc.SetInfo(mId, "/RichMediaSettings:Ref", settings);
		}

		/// <summary>Encodes the file name as the asset name tree requires</summary>
		/// <param name="fileName">File name</param>
		/// <returns>Encoded file name</returns>
		private static string EncodeName(string fileName) {
			fileName = System.Web.HttpUtility.UrlPathEncode(fileName);
			int length = fileName.Length;
			if(length>255)
				length = 255;
			while(fileName[length-1]=='.') {
				if(--length<=0)
					return "_";
			}
			if(fileName[length-1]=='%')
				--length;
			else if(fileName[length-2]=='%')
				length -= 2;
			if(length<fileName.Length)
				fileName = fileName.Substring(0, length);
			return fileName;
		}

		private static bool IsHexDigit(char ch) {
			return ch>='0' && ch<='9' || ch>='A' && ch<='F' || ch>='a' && ch<='f';
		}

		/// <summary>Fixes URL</summary>
		/// <param name="url">URL</param>
		/// <returns>Corrected URL</returns>
		private static string FixUrl(string url) {
			int query = url.IndexOf('?');
			if(query<0)
				return System.Web.HttpUtility.UrlPathEncode(url);

			++query;
			string originalData = url.Substring(query);
			string data = Uri.EscapeDataString(originalData);
			if(data.Length<=originalData.Length)
				return System.Web.HttpUtility.UrlPathEncode(url);

			string path = System.Web.HttpUtility.UrlPathEncode(url.Substring(0, query));
			StringBuilder builder = null;
			int iLast = 0;
			for(int i = 0; (i = data.IndexOf("%25", i, StringComparison.Ordinal))>=0
				&& i+4<data.Length; i += 3)
			{
				if(IsHexDigit(data[i+3]) && IsHexDigit(data[i+4])) {
					if(builder==null)
						builder = new StringBuilder(path);
					builder.Append(data.Substring(iLast, i+1-iLast));
					iLast = i+3;
					i += 2;
				}
			}
			if(builder==null)
				return path+data;
			builder.Append(data.Substring(iLast, data.Length-iLast));
			return builder.ToString();
		}

		/// <summary>Activation condition</summary>
		public string ActivationCondition {
			set {
				if(string.IsNullOrEmpty(value))
					mDoc.SetInfo(mId, "/RichMediaSettings*/Activation*/Condition:Del", "");
				else if(value=="XA" || value=="PO" || value=="PV")	// explicit activation, page open, page visible
					mDoc.SetInfo(mId, "/RichMediaSettings*/Activation*/Condition:Name", value);
				else
					throw new ArgumentException("Invalid activation condition.", "value");
			}
		}

		/// <summary>Deactivation condition</summary>
		public string DeactivationCondition {
			set {
				if(string.IsNullOrEmpty(value))
					mDoc.SetInfo(mId, "/RichMediaSettings*/Deactivation*/Condition:Del", "");
				else if(value=="XD" || value=="PC" || value=="PI") {	// explicit deactivation, page close, page invisible
					if(mDoc.GetInfo(mId, "/RichMediaSettings*/Deactivation*/Type")=="")
						mDoc.SetInfo(mId, "/RichMediaSettings*/Deactivation*/Type:Name", "RichMediaDeactivation");
					mDoc.SetInfo(mId, "/RichMediaSettings*/Deactivation*/Condition:Name", value);
				} else
					throw new ArgumentException("Invalid deactivation condition.", "value");
			}
		}

		/// <summary>FlashVars parameter</summary>
		public string FlashVars {
			set {
				string pathBase = "/RichMediaSettings*/Activation*/Configuration*/Instances*[0]*/Params*/FlashVars";
				if(mDoc.GetInfo(mId, "/RichMediaSettings*/Activation*/Configuration")=="")
					pathBase = "/RichMediaContent*/Configurations*[0]*/Instances*[0]*/Params*/FlashVars";
				if(string.IsNullOrEmpty(value))
					mDoc.SetInfo(mId, pathBase + ":Del", "");
				else if(value.Length<=128)
					mDoc.SetInfo(mId, pathBase + ":Text", value);
				else {
					int streamId = mDoc.AddObject("<<>>stream\nendstream\n");
					mDoc.SetInfo(streamId, "Stream", value);
					mDoc.GetInfo(streamId, "Compress");
					mDoc.SetInfo(mId, pathBase + ":Ref", streamId);
				}
			}
		}
	}
}
