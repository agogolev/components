// ===========================================================================
//	�2006 WebSupergoo. All rights reserved.
// ===========================================================================

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Windows.Forms;

using WebSupergoo.ABCpdf8;
using WebSupergoo.ABCpdf8.Objects;


namespace WebSupergoo.Annotations
{
	public class Test
	{
		class Server
		{
			public static string MapPath(string fileName)
			{
				string theBase = Directory.GetCurrentDirectory();
				return Directory.GetParent(theBase).Parent.FullName + "\\" + fileName;
			}
		}
		public static void Main()
		{
			const string msg = "This project will prompt you for a signature. In order to provide the correct signature you will need to first install the JohnSmith.pfx certificate.\r\n\r\n" +
				"You can do this by right clicking on the PFX file and selecting \'Install PFX\'. The password for the certificate is \'1234\'.\r\n\r\n" +
				"Please note that this is a console application and that as such, any notifications or errors will be written to the console window.";
			if (MessageBox.Show(msg, "Important Note", MessageBoxButtons.OKCancel) == DialogResult.OK) {
				AnnotationsTest(Server.MapPath("Annotations.pdf"));
				VerifyFileAndMakeReport(Server.MapPath("Annotations.pdf"), Server.MapPath("VerificationReport.pdf"));
			}
		}

		// <summary>Ask the current user to select a certificate from the his/her
		// personal store.</summary>
		private static X509Certificate2 PromptUserForCert() {
			X509Store store = new X509Store(StoreName.My, StoreLocation.CurrentUser);
			store.Open(OpenFlags.ReadOnly);

			try {
				X509Certificate2Collection certs =
					X509Certificate2UI.SelectFromCollection(
					store.Certificates, "Digital Identity",
					"Select a private key to sign",
					X509SelectionFlag.SingleSelection);

				// Return the first cert in the selection
				foreach (X509Certificate2 cert in certs)
					return cert;

				return null;
			}
			finally {
				store.Close();
			}
		}

		// <returns>Friendly name of the cert, using canonical name as fallback.</returns>
		private static string DisplayNameFromCert(X509Certificate2 cert) {
			if(!string.IsNullOrEmpty(cert.FriendlyName))
				return cert.FriendlyName;

			return cert.Subject.Split(',')[0].Split('=')[1];
		}

		public static void AnnotationsTest(string outputFile)
		{
			try 
			{
				Doc theDoc = new Doc();
				theDoc.Font = theDoc.AddFont("Helvetica");
				theDoc.FontSize = 36;

				EmbeddedFileTree fileTree = new EmbeddedFileTree(theDoc);
				fileTree.EmbedFile("MyFile1", Server.MapPath("ABCpdf.swf"), "attachment without annotation");
				theDoc.SetInfo(theDoc.Root, "/PageMode:Name", "UseAttachments");

				// Create interactive form
				InteractiveForm form = new InteractiveForm(theDoc);
				theDoc.Pos.X = 40;
				theDoc.Pos.Y = theDoc.MediaBox.Top - 40;
				theDoc.AddText("Interactive Form annotations");

				// Radio buttons
				form.AddRadioButtonGroup(new string[2] { "40 610 80 650", "40 660 80 700" }, "RadioGroupField", 0);
				theDoc.Pos.String = "100 696";
				theDoc.AddText("RadioButton 1");
				theDoc.Pos.String = "100 646";
				theDoc.AddText("RadioButton 2");

				// Text fields
				FormField text = form.AddTextField("40 530 300 580", "TextField1", "Hello World!");
				text.DefaultAppearance = "/TimesRoman 36 Tf 0 0 1 rg";
				text.BorderColor = "0 0 0";
				text.FillColor = "220 220 220";
				text.TextAlign = "Left";

				text = form.AddTextField("40 460 300 510", "TextField2", "Text Field");
				text.BorderColor = "0 0 0";
				text.DefaultAppearance = "/TimesRoman 36 Tf 0 0 1 rg";
				text.TextAlign = "Left";
				text.SetFlag(FormField.FieldFlag.Password);

				text = form.AddTextField("320 460 370 580", "TextField3", "Vertical");
				text.BorderColor = "0 0 0";
				text.DefaultAppearance = "/TimesRoman 36 Tf 0 0 0 rg";
				text.Rotate = 90;

				// Combobox field
				FormField combo = form.AddChoiceField("ComboBox", "40 390 300 440", "ComboBoxField");
				combo.DefaultAppearance = "/TimesRoman 24 Tf 0 0 0 rg";
				combo.AddOptions(new string[] { "ComboBox Item 1", "ComboBox Item 2", "ComboBox Item 3" });

				// Listbox field
				FormField listbox = form.AddChoiceField("ListBox", "40 280 300 370", "ListBoxField");
				listbox.DefaultAppearance = "/TimesRoman 24 Tf 0 0 0 rg";
				listbox.AddOptions(new string[] { "ListBox Item 1", "ListBox Item 2", "ListBox Item 3" });

				// Checkbox field
				form.AddCheckbox("40 220 80 260", "CheckBoxField", true);
				theDoc.Pos.String = "100 256";
				theDoc.AddText("Check Box");

				// Pushbutton field
				FormField button = form.AddButton("40 160 200 200", "ButtonField", "Button");
				button.BorderColor = "0 0 0";
				button.BorderStyle = "Beveled";

				// Markup annotations
				theDoc.Page = theDoc.AddPage();
				theDoc.Pos.X = 40;
				theDoc.Pos.Y = theDoc.MediaBox.Top - 40;
				theDoc.AddText("Markup annotations");

				SquareAnnotation square = new SquareAnnotation(theDoc, "40 560 300 670", "255 0 0", "0 0 255");
				square.BorderWidth = 8;

				LineAnnotation line = new LineAnnotation(theDoc, "100 565 220 665", "255 0 0");
				line.BorderWidth = 12;
				line.RichTextCaption = "<span style= \"font-size:36pt; color:#FF0000\">Line</span>";

				theDoc.FontSize = 24;
				theDoc.Pos.String = "400 670";
				int id = theDoc.AddText("Underline");
				TextMarkupAnnotation markup = new TextMarkupAnnotation(theDoc, id, "Underline", "0 255 0");

				theDoc.Pos.String = "400 640";
				id = theDoc.AddText("Highlight");
				markup = new TextMarkupAnnotation(theDoc, id, "Highlight", "255 255 0");

				theDoc.Pos.String = "400 610";
				id = theDoc.AddText("StrikeOut");
				markup = new TextMarkupAnnotation(theDoc, id, "StrikeOut", "255 0 0");

				theDoc.Pos.String = "400 580";
				id = theDoc.AddText("Squiggly");
				markup = new TextMarkupAnnotation(theDoc, id, "Squiggly", "0 0 255");

				CircleAnnotation circle = new CircleAnnotation(theDoc, "80 320 285 525", "255 255 0", "255 128 0");
				circle.BorderWidth = 20;
				circle.BorderStyle = "Dashed";
				circle.BorderDash = "[3 2]";

				LineAnnotation arrowLine = new LineAnnotation(theDoc, "385 330 540 520", "255 0 0");
				arrowLine.LineEndingsStyle = "ClosedArrow ClosedArrow";
				arrowLine.BorderWidth = 6;
				arrowLine.FillColor = "255 0 0";

				PolygonAnnotation polygon = new PolygonAnnotation(theDoc, "100 70 50 120 50 220 100 270 200 270 250 220 250 120 200 70", "255 0 0", "0 255 0");
				PolygonAnnotation cloudyPolygon = new PolygonAnnotation(theDoc, "400 70 350 120 350 220 400 270 500 270 550 220 550 120 500 70", "255 0 0", "64 85 255");
				cloudyPolygon.CloudyEffect = 1;

				// Movie annotations
				// WMV is courtesy of NASA - http://www.nasa.gov/wmv/30873main_cardiovascular_300.wmv
				theDoc.Page = theDoc.AddPage();
				theDoc.Pos.X = 40;
				theDoc.Pos.Y = theDoc.MediaBox.Top - 40;
				theDoc.AddText("Multimedia features");

				theDoc.FontSize = 24;

				theDoc.Pos.String = "40 690";
				theDoc.AddText("Flash movie:");
				MovieAnnotation movie1 = new MovieAnnotation(theDoc, "40 420 300 650", Server.MapPath("ABCpdf.swf"));

				theDoc.Pos.String = "312 690";
				theDoc.AddText("Flash rich media:");
				SimpleRichMediaAnnotation media1 = new SimpleRichMediaAnnotation(theDoc, "312 420 572 650", Server.MapPath("ABCpdf.swf"), "Flash");

				theDoc.Pos.String = "40 400";
				theDoc.AddText("Video File:");
				MovieAnnotation movie2 = new MovieAnnotation(theDoc, "80 40 520 360", Server.MapPath("video.wmv"));

				theDoc.Page = theDoc.AddPage();
				theDoc.FontSize = 36;
				theDoc.Pos.X = 40;
				theDoc.Pos.Y = theDoc.MediaBox.Top - 40;
				theDoc.AddText("Other types of annotations");

				// Sticky note annotation
				theDoc.FontSize = 24;
				theDoc.Pos.String = "40 680";
				theDoc.AddText("Text annotation");
				TextAnnotation textAnnotation = new TextAnnotation(theDoc, "340 660 360 680", "550 650 600 750", "6 sets of 13 pages. Trim to 5X7.");

				// File attachment annotation
				theDoc.Pos.String = "40 640";
				theDoc.AddText("File Attachment annotation");
				FileAttachmentAnnotation fileAttachment = new FileAttachmentAnnotation(theDoc, "340 620 360 640", Server.MapPath("video.WMV"));

				// StampAnnotations
				theDoc.Pos.String = "40 600";
				theDoc.AddText("Stamp annotations");
				StampAnnotation stamp1 = new StampAnnotation(theDoc, "340 560 540 600", "DRAFT", "0 0 128");
				StampAnnotation stamp2 = new StampAnnotation(theDoc, "340 505 540 545", "FINAL", " 0 128 0");
				StampAnnotation stamp3 = new StampAnnotation(theDoc, "340 450 540 490", "NOT APPROVED", "128 0 0");

				theDoc.PageNumber = 1;

				// Signature fields
				// Add signature fields last so that entire document is signed

				// Note: For maximum portability, it is recommended
				// that you create all the signature fields before 
				// signing any one of them (as is demonstrated below).
				//
				// The reason is that adding a signature field 
				// changes the document. Although this is legal, not all 
				// versions of Acrobat are happy with all types of updates.
				// As such, they may report existing valid signatures to be 
				// invalid.
				//
				// In the past, the "SigFlags" entry allowed contents to be 
				// appended to signed documents using incremental updates.
				// However, the entry's treatment seems to have changed in 
				// Adobe Reader X. For example, Adobe Reader X will 
				// report the signatures in `SignedThenAppended.pdf` to 
				// be invalid whereas Acrobat 8 will report 
				// "signatures are valid, but document has been changed 
				// since it was signed." This is a document created using 
				// Acrobat Professional 8.


				// We add the first signature field unsigned
				form.AddSignature("40 100 240 150", "Signature1");

				// And the second signature field unsigned (to be signed 
				// below)
				form.AddSignature("340 100 540 150", "Signature2");
				X509Certificate2 userCert = PromptUserForCert();
				if (userCert != null) {
					Signature sig2 = (Signature)theDoc.Form.Fields["Signature2"];
					sig2.Signer = DisplayNameFromCert(userCert);

					// We need not call sig2.Commit() as sig2 is not signed yet.
					//sig2.Commit();
				}

				// We can add the last signature field signed.
				// Note: Certifying the document makes all but the last signed
				// signature invalid, especially after clicking on the signatures.
				form.Certify = true;
				form.ShowSignatureValidity = true;
				form.AddSignature("340 160 540 220", "Signature3",
					Server.MapPath("JohnSmith.pfx"), "1234", "I am the author",
					"New York", "Digitally signed by {0}\nReason: {2}\n" +
					"Location: {3}\nDate: {1:yyyy.MM.dd}");
				form.Certify = false;

				// Go back and sign the second signature placeholder
				// if interactive user has supplied the signing key
				if (userCert != null) {
					// We are about to sign a signature outside the
					// InteractiveForm class. Commit the previous signature.
					form.CommitSignature();

					// We need to obtain the field afresh because
					// Signature.Commit invalidates all IndirectObject's
					Signature sig2 = (Signature)theDoc.Form.Fields["Signature2"];
					sig2.Sign(userCert, true);
					// This is the last signature so we need not commit it
					// using sig2.Commit();
				}

				theDoc.Save(outputFile);
			}
			catch (Exception e) {
				Console.WriteLine("An exception occurred during document signing:");
				Console.WriteLine("Message: " + e.Message);
				Console.WriteLine("Source: " + e.Source);
			}
		}

		public static void VerifyFileAndMakeReport(string fileName, string reportFileName)
		{
			try
			{
				Doc reportDoc = new Doc();
				reportDoc.Rect.Inset(20, 20);

				reportDoc.FontSize = 20;
				reportDoc.AddText("Signature verification report\n");
				reportDoc.Pos.Y -= 20;
				reportDoc.FontSize = 16;
				reportDoc.AddText("File name: " + fileName + "\n");

				Doc subjectDoc = new Doc();
				subjectDoc.Read(fileName);

				// certificates are used for validating the X.509 signature
				// you may wish to obtain root certificates from a trusted authority
				string[] certs = Server.MapPath("JohnSmith.cer").Split(new char[] { ';' });

				int sigCount = 0;
				foreach (Field theField in subjectDoc.Form.Fields) {
					if (theField is Signature) {
						sigCount++;
						if (sigCount != 1) {
							reportDoc.Page = reportDoc.AddPage();
						}
						Signature theSig = (Signature)theField;
						string reportHtml = null;
						if (theSig.Signer != null && theSig.Signer.Length > 0) {
							bool certificateValid = theSig.Validate(certs);
							reportHtml = "Signature name: " + theSig.Name + "<BR>" +
								"Signed by: " + theSig.Signer + "<BR>" +
								"Reason: " + theSig.Reason + "<BR>" +
								"Date (UTC Time): " + theSig.SigningUtcTime + "<BR>" +
								"Location: " + theSig.Location + "<BR>" +
								"Is document modified: " + theSig.IsModified.ToString() + "<BR>" +
								"Is certificate valid: " + certificateValid.ToString() + "<BR>" +
								"Is document valid: " + (certificateValid && !theSig.IsModified).ToString() + "<BR>";
						}
						else {
							reportHtml = "Signature name: " + theSig.Name + "<BR>" +
								"This signature has not been signed.<BR>";
						}
						reportDoc.TextStyle.LineSpacing = 10;
						reportDoc.AddHtml(reportHtml);

						foreach (byte[] certData in theSig.GetCertificates()) {
							X509Certificate2 theCert = new X509Certificate2(certData);
							AddCertificateDetails(reportDoc, theCert);
						}
					}
				}

				if ( sigCount == 0 )
					reportDoc.AddText("The document is not signed.");
				reportDoc.Save(reportFileName);
			}
			catch (Exception e)
			{
				Console.WriteLine("An exception occured during document validation:");
				Console.WriteLine("Message: " + e.Message);
				Console.WriteLine("Source: " + e.Source);
			}
		}

		private static void AddCertificateDetails(Doc inDoc, X509Certificate2 inCert) {

			StringBuilder reportHtml = new StringBuilder();
			reportHtml.Append("Certificate details:" + "<BR>");
			reportHtml.Append("Subject: " + inCert.Subject + "<BR>");
			reportHtml.Append("Issued by: " + inCert.IssuerName.Name + "<BR>");
			reportHtml.Append("From: " + inCert.NotBefore.ToString() + " To: " + inCert.NotAfter.ToString() + "<BR>");
			reportHtml.Append("Serial Number:" + inCert.GetSerialNumberString() + "<BR>");
			reportHtml.Append("Version: " + inCert.Version.ToString() + "<BR>");
			reportHtml.Append("Algorithm: " + inCert.SignatureAlgorithm.FriendlyName + "<BR>");

			foreach (X509Extension e in inCert.Extensions) {
				reportHtml.Append(e.Oid.FriendlyName + ": " + e.Format(false) + "<BR>");
			}

			reportHtml.Append("Public Key: " + inCert.PublicKey.Key.KeyExchangeAlgorithm + "<BR>");
			reportHtml.Append("Public Key Data " + inCert.GetPublicKeyString() + "<BR>");

			inDoc.AddHtml(reportHtml.ToString());
		}
	}
}

