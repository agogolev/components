using System.Configuration;
namespace HtmlEditor.Repository
{
    public class BaseRepository
    {
        protected static string ConnectionString
        {
            get { return ConfigurationManager.AppSettings["dbdata.connection"]; }
        }
    }
}
