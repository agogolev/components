using System.Web;

namespace HtmlEditor.Repository.Converter
{
    public class PostedFileToByteArrayConverter
    {
        public HttpPostedFileBase PostedFile { get; private set; }

        public PostedFileToByteArrayConverter(HttpPostedFileBase postedFile)
        {
            PostedFile = postedFile;
        }
        
        public byte[] GetData()
        {
            if (PostedFile == null) return new byte[0];
            var stream = PostedFile.InputStream; //initialise new stream
            var buf = new byte[stream.Length];
            stream.Read(buf, 0, buf.Length);
            return buf;
        }
    }
}
