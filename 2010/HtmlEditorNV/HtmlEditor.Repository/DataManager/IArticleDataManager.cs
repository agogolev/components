using System;
using System.Collections.Generic;
using System.Web;
using HtmlEditor.Domain;

namespace HtmlEditor.Repository.DataManager
{
    public interface IArticleDataManager
    {
        void SetArticleImage(Guid articleOID, HttpPostedFileBase file);

    	IList<Image> GetImagesByArticle(Guid articleOID);

    	void DelArticleImage(Guid articleOID, string ordValue);
    }
}
