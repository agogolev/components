using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using DBReader;
using HtmlEditor.Domain;
using HtmlEditor.Domain.Classes;
using HtmlEditor.Repository.ArticleRep;
using HtmlEditor.Repository.BinaryDataRep;
using System.Text.RegularExpressions;
using HtmlEditor.Repository.Converter;

namespace HtmlEditor.Repository.DataManager
{
	public class ArticleDataManager : BaseRepository, IArticleDataManager
	{
		public IBinaryDataRepository binaryDataRepository;

		public ArticleDataManager(IBinaryDataRepository binaryDataRepository)
		{
			this.binaryDataRepository = binaryDataRepository;
		}

		public void SetArticleImage(Guid articleOID, HttpPostedFileBase file)
		{
			if (file == null || file.ContentLength == 0) throw new ArgumentNullException("file");

			var fileName = Path.GetFileName(file.FileName);
			var data = (new PostedFileToByteArrayConverter(file)).GetData();
			var ordValue = GetCurrentOrdValue(articleOID);

			SetArticleImage(articleOID, fileName, data, ordValue);
		}

		private void SetArticleImage(Guid articleOID, string fileName, byte[] data, string ordValue)
		{
			var binaryData = binaryDataRepository.SetBinaryData(fileName, data);

			var cArticle = new CArticle(ConnectionString, articleOID);
			cArticle.AddMulti("images", binaryData.OID, ordValue);
			//cArticle.Save();
		}

		public void DelArticleImage(Guid articleOID, string ordValue)
		{
			var cArticle = new CArticle(ConnectionString, articleOID);
			cArticle.DeleteMulti("images", null, ordValue);
		}


		public IList<Image> GetImagesByArticle(Guid articleOID)
		{
			const string sql =
				@"
SELECT
	b.OID,
	oi.ordValue, 
	b.name,
	b.fileName,
	b.description
FROM 
	t_BinaryData b
	inner join t_ObjectImage oi on oi.binData = b.OID
WHERE
	oi.OID = @articleOID
ORDER BY oi.ordValue";
			return sql.ExecSql(new { articleOID })
				.Select(row => new Image
				               	{
				               		OID = row.Field<Guid>("OID"),
				               		FileName = row.Field<string>("fileName"),
									Name = row.Field<string>("ordValue"),
				               		Description = row.Field<string>("description"),
				               	})
				.ToList();
		}

		private static string GetCurrentOrdValue(Guid articleOID)
		{
			var re = new Regex(@"^image\s+(\d+)$");
			const string sql = @"
SELECT
	ordValue
FROM 
	t_ObjectImage oi
WHERE
	oi.OID = @OID";
			var maxOrd =
				sql.ExecSql(new {OID = articleOID}).Where(row => re.IsMatch(row.Field<string>("ordValue"))).Select(
					row => int.Parse(re.Match(row.Field<string>("ordValue")).Groups[1].Value)).DefaultIfEmpty(0).Max();
			return string.Format("image {0}", maxOrd + 1);
		}

	}
}
