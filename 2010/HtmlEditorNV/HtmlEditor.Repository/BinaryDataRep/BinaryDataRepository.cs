using System;
using System.Drawing;
using System.IO;
using MetaData;
using HtmlEditor.Domain.Classes;

namespace HtmlEditor.Repository.BinaryDataRep
{
	public class BinaryDataRepository : BaseRepository, IBinaryDataRepository
	{
		public CBinaryData SetBinaryData(string fileName, byte[] data)
		{
			var name = Path.GetFileNameWithoutExtension(fileName);
			return SetBinaryData(fileName, data, name);
		}
		
		public CBinaryData SetBinaryData(string fileName, byte[] data, string name)
		{
			var mimeType = MimeTypeUtil.CheckType(data);
			int width, height;
			FillSize(mimeType, data, out width, out height);

			var binaryData = new CBinaryData(ConnectionString);
			binaryData.FileName = fileName;
			binaryData.Name = name;
			binaryData.MimeType = mimeType;
			binaryData.Width = width;
			binaryData.Height = height;
			binaryData.Data = data;
			binaryData.Save();

			return binaryData;
			//CArticle a = new CArticle(ConnectionString, new Guid());
			//a.AddMulti("images", img.OID, 1);
		}

		private static void FillSize(string mimeType, byte[] data, out int width, out int height)
		{
			width = 0;
			height = 0;
			var mime = mimeType.ToLower();
			//var size = new Size(0, 0);
			var buffer = data;
			var size = Sizer.GetSize(buffer, mimeType);
			//if (mime.IndexOf("image") != -1)
			//{
			//    if (mime.IndexOf("gif") != -1)
			//    {
			//        var gif = new GIFSizer();
			//        //var fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
			//        //var buffer = new byte[10];
			//        //fs.Read(buffer, 0, 10);
			//        //fs.Close();
			//        size = gif.ImageSize(buffer);
			//    }
			//    else if (mime.IndexOf("png") != -1)
			//    {
			//        var png = new PNGSizer();
			//        //var fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
			//        //byte[] buffer = new byte[24];
			//        //fs.Read(buffer, 0, 24);
			//        //fs.Close();
			//        size = png.ImageSize(buffer);
			//    }
			//    else if (mime.IndexOf("jpeg") != -1 || mime.IndexOf("jpg") != -1)
			//    {
			//        var jpg = new JPEGSizer();
			//        //var fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
			//        //var buffer = new byte[fs.Length];
			//        //fs.Read(buffer, 0, (int)fs.Length);
			//        //fs.Close();
			//        size = jpg.ImageSize(buffer);
			//    }
			//}
			if (size.Width != 0) width = size.Width;
			if (size.Height != 0) height = size.Height;
		}

		public void DelBinaryData(Guid oid)
		{
			if (oid == Guid.Empty) throw new ArgumentNullException("oid");
			var binaryData = new CBinaryData(ConnectionString, oid);
			binaryData.Delete();
		}
	}
}
