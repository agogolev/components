using System;
using HtmlEditor.Domain.Classes;

namespace HtmlEditor.Repository.BinaryDataRep
{
    public interface IBinaryDataRepository
    {
        CBinaryData SetBinaryData(string fileName, byte[] data);
        CBinaryData SetBinaryData(string fileName, byte[] data, string name);
        void DelBinaryData(Guid oid);
    }
}
