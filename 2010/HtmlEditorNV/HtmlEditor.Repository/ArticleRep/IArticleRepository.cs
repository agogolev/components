using System;
using System.Collections.Generic;

namespace HtmlEditor.Repository.ArticleRep
{
    public interface IArticleRepository
    {
        Domain.Article GetArticleById(int id);

        Domain.Article GetArticleByOid(Guid oid);
    }
}
