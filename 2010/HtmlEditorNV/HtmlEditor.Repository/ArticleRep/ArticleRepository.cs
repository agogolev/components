using System;
using System.Data;
using DBReader;
using System.Linq;
using HtmlEditor.Domain.Classes;
using HtmlEditor.Domain;

namespace HtmlEditor.Repository.ArticleRep
{
	public class ArticleRepository : BaseRepository, IArticleRepository
	{
		public Article GetArticleById(int id)
		{
			const string sql = @"
SELECT
	a.OID, 
	o.objectID,
	a.articleType,
	a.header,
	a.annotation,
	a.body,
	a.pubDate,
	o.title,
	o.keywords,
	o.pageDescription
FROM 
	t_Article a
	JOIN t_object o ON a.OID = o.OID
WHERE 
	o.objectID = @id";

			return sql.ExecSql(new {id})
				.Select(row => new Article
				{
					OID = row.Field<Guid>("OID"),
					Id = row.Field<int>("objectID"),
					Body = row.Field<string>("body")
				})
				.FirstOrDefault();
		}

		public Article GetArticleByOid(Guid oid)
		{
			const string sql = @"
SELECT
	a.OID, 
	o.objectID,
	a.body
FROM 
	t_Article a
	JOIN t_object o ON a.OID = o.OID
WHERE 
	o.OID = @oid";

			return sql.ExecSql(new { oid })
				.Select(row => new Article
				               	{
				               		OID = row.Field<Guid>("OID"),
									Id = row.Field<int>("objectID"),
									Body = row.Field<string>("body")
				               	})
				.FirstOrDefault();
		}
	}
}
