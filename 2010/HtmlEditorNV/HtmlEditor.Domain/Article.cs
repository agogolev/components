using System;

namespace HtmlEditor.Domain
{
	public class Article
	{
        public Guid OID { get; set; }
		public int Id { get; set; }
		public string Body { get; set; }

	}
}
