using System;

namespace HtmlEditor.Domain
{
    public class Image
    {
        public Guid OID { get; set; }
        public Guid ObjectId { get; set; }
		public string Name { get; set; }
		public string FileName { get; set; }
		public string Description { get; set; }
        public byte[] Data { get; set; }
    }
}
