using System;

namespace HtmlEditor {
	/// <summary>
	/// Summary description for user.
	/// </summary>
	
	[Serializable]
	public class user {
		private string _userLogin;
		private string _userPassword;
		private string _catalog;
		private string _dbMachine;
		private string _siteLogin; // логин пользователя на сайте
		private string _sitePeopleName; // псевдоним пользователя на сайте
		private Guid _sitePeopleOID; // OID пользователя на сайте
		private string _lastURL;
		private string _currentURL;
		
    public user(string UserLogin, string UserPassword, string catalog, string dbmachine) {
			_userLogin = UserLogin;
			_userPassword = UserPassword;
			_catalog = catalog;
			_dbMachine = dbmachine;
		}

		public string connString {
			get {return "data source="+_dbMachine+";initial catalog="+_catalog+";password="+_userPassword+";persist security info=True;user id="+_userLogin;}
		}

		public string Login
		{
			get {return _siteLogin;}
			set {_siteLogin = value;}
		}

		public string PeopleName
		{
			get {return _sitePeopleName;}
			set {_sitePeopleName = value;}
		}

		public Guid PeopleOID
		{
			get {return _sitePeopleOID;}
			set {_sitePeopleOID = value;}
		}
		public string LastURL {
			get {return _lastURL;}
			set {_lastURL = value;}
		}
		public string CurrentURL 
		{
			get {return _currentURL;}
			set {_currentURL = value;}
		}
	}
}
