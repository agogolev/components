using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using HtmlEditor.Repository.DataManager;
using HtmlEditor.Domain;
using System.Web;
using HtmlEditor.Repository.BinaryDataRep;
using Unity;
using Unity.Lifetime;

namespace HtmlEditor
{
	public class BinDataManager : IBinDataManager
	{
		private static readonly UnityContainer container = new UnityContainer();
		private readonly IArticleDataManager articleDataManager;
		static BinDataManager()
		{
			container.RegisterType<IBinaryDataRepository, BinaryDataRepository>(new ContainerControlledLifetimeManager());
			container.RegisterType<IArticleDataManager, ArticleDataManager>(new ContainerControlledLifetimeManager());
		}
		public BinDataManager()
		{
			articleDataManager = container.Resolve<IArticleDataManager>();
		}
		public BinDataManager(IArticleDataManager articleDataManager)
		{
			this.articleDataManager = articleDataManager;
		}
		public void SaveImageForArticle(Guid articleOID, HttpPostedFileBase file)
		{
			articleDataManager.SetArticleImage(articleOID, file);
		}
		public void DelArticleImage(Guid articleOID, string ordValue)
		{
			articleDataManager.DelArticleImage(articleOID, ordValue);
		}
		public IList<Image> GetImagesByArticle(Guid articleOID)
		{
			return articleDataManager.GetImagesByArticle(articleOID);
		}

	    public IList<Image> GetImagesByFolder(string folderName, string urlFolder)
	    {
	        var di = new DirectoryInfo(folderName);
	        return di.GetFiles().Select(fi => new Image
	        {
	            Name = Path.GetFileNameWithoutExtension(fi.Name),
	            FileName = string.Format("/{0}/{1}", urlFolder, Path.GetFileName(fi.Name))
	        }).ToList();
	    }

        public void SaveImageToFolder(string folderName, HttpPostedFileWrapper postedFile)
        {
            var fileName = string.Format(@"{0}\{1}", folderName, Path.GetFileName(postedFile.FileName));
            var stream = postedFile.InputStream;
            using (var fileStream = File.Create(fileName))
            {
                stream.CopyTo(fileStream);
            }
	    }
	}
}
