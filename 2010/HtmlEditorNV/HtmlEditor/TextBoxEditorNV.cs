using System;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.Data;
using System.Xml.Linq;
using DBReader;

namespace HtmlEditor
{
	public sealed class TextBoxEditorNV : TextBox
	{
		public TextBoxEditorNV()
		{
			TextMode = TextBoxMode.MultiLine;
		}

		[Bindable(true),
		DefaultValue("")]
		public string ConnectionString
		{
			get
			{
				return ViewState["ConnectionString"] != null ? (string)ViewState["ConnectionString"] : "";
			}

			set
			{
				ViewState["ConnectionString"] = value;
			}
		}

		[Bindable(true),
		DefaultValue("")]
		public string FieldString
		{
			get
			{
				return ViewState["FieldString"] != null ? (string)ViewState["FieldString"] : "";
			}

			set
			{
				ViewState["FieldString"] = value;
			}
		}

	    [Bindable(true),
	     DefaultValue("")]
	    public string OrdValueString
	    {
	        get
	        {
	            return ViewState["OrdValueString"] != null ? (string)ViewState["OrdValueString"] : "";
	        }

	        set
	        {
	            ViewState["OrdValueString"] = value;
	        }
	    }

        [Bindable(true),
		DefaultValue("")]
		public string TableName
		{
			get
			{
				return ViewState["TableName"] != null ? (string)ViewState["TableName"] : "";
			}

			set
			{
				ViewState["TableName"] = value;
			}
		}

	    [Bindable(true),
	     DefaultValue("")]
	    public string OrdValue
	    {
	        get
	        {
	            return ViewState["OrdValue"] != null ? (string)ViewState["OrdValue"] : "";
	        }

	        set
	        {
	            ViewState["OrdValue"] = value;
	        }
	    }

        [Bindable(true),
		DefaultValue(typeof(Guid), "00000000-0000-0000-0000-000000000000")]
		public Guid OID
		{
			get
			{
				return ViewState["OID"] != null ? (Guid)ViewState["OID"] : Guid.Empty;
			}
			set
			{
				ViewState["OID"] = value;
			}
		}
		public override void DataBind()
		{
			if (OID != Guid.Empty)
			{
				LoadData();
				base.DataBind();
			}
		}
		public void LoadData()
		{
			var sb = new StringBuilder();
			if (OID == Guid.Empty) return;
			//получим данные
			var sql = string.Format("SELECT {0} FROM {1} WHERE OID = '{2}'", FieldString, TableName, OID);
			if (!string.IsNullOrEmpty(OrdValue))
			{
			    sql = string.Format("{0} and {1} = '{2}'", sql, OrdValueString, OrdValue);
			}
			var cmd = QueryConstructor.GetCommand(sql, CommandType.Text, null); //UserInfo
			cmd.Connection.Open();

			try
			{
				var dr = cmd.ExecuteReader();
				while (dr.Read())
				{
					sb.Append((string)dr[0]);
				}
			}
			finally
			{
				cmd.Connection.Close();
			}

			var doc = XElement.Parse(sb.ToString());

			Text = sb.ToString();
		}
	}
}
