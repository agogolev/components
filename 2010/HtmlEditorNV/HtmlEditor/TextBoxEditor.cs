using System;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.Data;
using System.Xml.Linq;
using DBReader;

namespace HtmlEditor
{
	public sealed class TextBoxEditor : TextBox
	{
		public TextBoxEditor()
		{
			TextMode = TextBoxMode.MultiLine;
		}

		[Bindable(true),
		DefaultValue("")]
		public string ConnectionString
		{
			get
			{
				return ViewState["ConnectionString"] != null ? (string)ViewState["ConnectionString"] : "";
			}

			set
			{
				ViewState["ConnectionString"] = value;
			}
		}

		[Bindable(true),
		DefaultValue("")]
		public string FieldString
		{
			get
			{
				return ViewState["FieldString"] != null ? (string)ViewState["FieldString"] : "";
			}

			set
			{
				ViewState["FieldString"] = value;
			}
		}
		[Bindable(true),
		DefaultValue("")]
		public string ClassName
		{
			get
			{
				return ViewState["ClassName"] != null ? (string)ViewState["ClassName"] : "";
			}

			set
			{
				ViewState["ClassName"] = value;
			}
		}

		[Bindable(true),
		DefaultValue(typeof(Guid), "00000000-0000-0000-0000-000000000000")]
		public Guid OID
		{
			get
			{
				return ViewState["OID"] != null ? (Guid)ViewState["OID"] : Guid.Empty;
			}
			set
			{
				ViewState["OID"] = value;
			}
		}
		public override void DataBind()
		{
			if (OID != Guid.Empty)
			{
				LoadData();
				base.DataBind();
			}
		}
		public void LoadData()
		{
			var sb = new StringBuilder();
			if (OID == Guid.Empty) return;
			if (ClassName == "CArticle")//если объект статья
			{
				//получим данные
				var sql = "upGetArticle '" + OID + "'";
				var cmd = QueryConstructor.GetCommand(sql, CommandType.Text, null); //UserInfo
				cmd.Connection.Open();

				try
				{
					var dr = cmd.ExecuteReader();
					while (dr.Read())
					{
						sb.Append((string)dr[0]);
					}
				}
				finally
				{
					cmd.Connection.Close();
				}

				var doc = XElement.Parse(sb.ToString());

				Text = doc.Descendants("body").First().Value;

			}
			else//если объект не статья
			{
				//получим данные
				var sql = QueryConstructor.BuildGetObject(OID, ClassName, FieldString, "", null);
				var cmd = QueryConstructor.GetCommand(sql, CommandType.Text, null); 
				cmd.Parameters.AddWithValue("@OID", OID);
				cmd.Connection.Open();
				try
				{
					var dr = cmd.ExecuteReader();
					while (dr.Read())
					{
						sb.Append((string)dr[0]);
					}
				}
				finally
				{
					cmd.Connection.Close();
				}

				var doc = XElement.Parse(sb.ToString());
					
				Text = doc.DescendantsAndSelf("item").First().Elements(FieldString).Select(e => e.Value).SingleOrDefault();
			}
		}
	}
}
