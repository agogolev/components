using System;
using System.Web;
using System.Collections.Generic;
using HtmlEditor.Domain;

namespace HtmlEditor
{
	public interface IBinDataManager
	{
		void SaveImageForArticle(Guid articleOID, HttpPostedFileBase file);
		void DelArticleImage(Guid articleOID, string ordValue);
		IList<Image> GetImagesByArticle(Guid articleOID);
        IList<Image> GetImagesByFolder(string folderName, string urlFolder);
	}
}