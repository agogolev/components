using System;
using System.Xml;
using System.Web;
using System.Collections;
using System.Data.SqlClient;
using System.Globalization;
using System.Text.RegularExpressions;
using DBReader;
using HtmlEditor.Domain.Classes;
using MetaData;

namespace HtmlEditor
{
    /// <summary>
    /// Summary description for XmlSaver.
    /// </summary>
    public class XmlSaver
    {
        public void Save(Guid OID, string fieldName, string fieldValue, string className)
        {
            fieldValue = prepareText(fieldValue);
            var e = new ModifyXmlEventArgs(fieldValue);
            OnModifyXml(e);
            fieldValue = e.XmlBody;
            var m = QueryConstructor.BuildMetadataInfo(className);
            if (string.Compare(className, "CArticle", StringComparison.InvariantCultureIgnoreCase) == 0 &&
                string.Compare(fieldName, "Body", StringComparison.InvariantCultureIgnoreCase) == 0)
            {
                CArticle art = new CArticle(System.Configuration.ConfigurationManager.AppSettings["dbdata.connection"], OID);
                art.Body = fieldValue;
                art.Save();
            }
            else
            {
                var p = (PropInfo)m.FindProp(fieldName, 1);
                if (p == null) throw new Exception(string.Format("Field {0} not found", fieldName));
                if (p.MaximumLength == 0 || fieldValue.Length <= p.MaximumLength)
                {
                    var prms = new Hashtable();
                    prms["@OID"] = OID;
                    prms["@className"] = className;
                    if (fieldValue.IndexOf("','", StringComparison.Ordinal) == -1)
                    {
                        prms["@FieldNames"] = fieldName;
                        prms["@FieldValues"] = fieldValue;
                    }
                    else
                    {
                        prms["@FieldNames"] = DBNull.Value;
                        string xml = "<?xml version=\"1.0\"?><fields><field><name>" + fieldName + "</name><value></value></field></fields>";
                        var doc = new XmlDocument();
                        doc.LoadXml(xml);
                        var selectSingleNode = doc.SelectSingleNode("fields/field/value");
                        if (selectSingleNode != null)
                            selectSingleNode.InnerText = fieldValue;
                        prms["@FieldValues"] = doc.OuterXml;
                    }

                    DBHelper.ExecuteCommand("spUpdateObject", prms, true);
                    
                }
                else throw new Exception("Текст не сохранён, так как его длина превышает размеры поля в базе");
            }

            WebServiceExtensions.ClearWebCache("www_");
        }

        private string prepareText(string text)
        {
            if (text == null) return null;
            text = ModifyAnchor(text, "a", "href");
            text = ModifyAnchor(text, "img", "src");
            return text;
        }

        private string ModifyAnchor(string text, string tagName, string atrName)
        {
            var findTag = new Regex("\\<" + tagName + "[^>]*" + atrName + "=['\"]([^'\"]*)['\"][^>]*\\>",
                                    RegexOptions.IgnoreCase | RegexOptions.Singleline);
            return findTag.Replace(text, ModifyAnchorEvaluator);
        }

        private string ModifyAnchorEvaluator(Match match)
        {
            var tag = match.Value;
            var link = match.Groups[1].Value;
            if (link.IndexOf("http://", StringComparison.InvariantCultureIgnoreCase) == -1) return tag;
            var back = HttpContext.Current.Request.Url;
            var linkUri = new Uri(link);
            if (back.Authority != linkUri.Authority) return tag;
            var path = linkUri.AbsolutePath.Substring(0, linkUri.AbsolutePath.LastIndexOf('/'));
            var backPath = back.AbsolutePath.Substring(0, back.AbsolutePath.LastIndexOf('/'));

            if (string.Compare(path, backPath, StringComparison.InvariantCultureIgnoreCase) == 0)
            {
                link = linkUri.AbsolutePath.Substring(linkUri.AbsolutePath.LastIndexOf('/') + 1) + linkUri.Query + linkUri.Fragment;
            }
            else
            {
                link = linkUri.AbsolutePath + linkUri.Query + linkUri.Fragment;
            }
            return string.Format("{0}{1}{2}", tag.Substring(0, match.Groups[1].Index - match.Index), link,
                                 tag.Substring(match.Groups[1].Index - match.Index + match.Groups[1].Length));
        }

        public event ModifyXmlEventHandler ModifyXml;
        protected virtual void OnModifyXml(ModifyXmlEventArgs e)
        {
            if (ModifyXml != null) ModifyXml(this, e);
        }
    }
    public class ModifyXmlEventArgs : EventArgs
    {
        public string XmlBody { get; set; }

        public ModifyXmlEventArgs(string body)
        {
            XmlBody = body;
        }
    }
    public delegate void ModifyXmlEventHandler(object sender, ModifyXmlEventArgs e);
}
