using System;
using System.Text;
using System.Collections;
using System.Xml;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;
using BinHandler;
using System.Globalization;
using System.Xml.Linq;

namespace GeneralControls
{
	/// <summary>
	/// Summary description for Article.
	/// </summary>
	[ParseChildren(true)]
	[ToolboxData("<{0}:Article runat=server></{0}:Article>")]
	public class Article : PlaceHolder, INamingContainer
	{
		
		public bool ProcessImages
		{
			get
			{
				object o = ViewState["ProcessImages"];
				return o == null ? true : (bool)o;
			}
			set
			{
				ViewState["ProcessImages"] = value;
			}
		}
		public bool IsLoaded { get; set; }
		public XElement ArticleBody { get; set; }

		private RepeaterItem _headerItem;
		private RepeaterItem _footerItem;
		public RepeaterItem HeaderItem
		{
			get
			{
				EnsureChildControls();
				return _headerItem;
			}
		}
		public RepeaterItem FooterItem
		{
			get
			{
				EnsureChildControls();
				return _footerItem;
			}
		}
		private string _header = "", _annotation = "", _title = "", _keywords = "", _description = "";
		private XElement _doc;
		public Guid ArticleOID
		{
			get
			{
				object o = ViewState["ArticleOID"];
				return o == null ? Guid.Empty : (Guid)o;
			}
			set
			{
				ViewState["ArticleOID"] = value;
			}
		}
		public string ArticleControls
		{
			get
			{
				object o = ViewState["ArticleControls"];
				return o == null ? string.Empty : (string)o;
			}
			set
			{
				ViewState["ArticleControls"] = value;
			}
		}
		public string Header
		{
			get
			{
				return _header;
			}
			set
			{
				_header = value;
			}
		}
		public DateTime PubDate { get; set; }
		public string Annotation
		{
			get
			{
				return _annotation;
			}
			set
			{
				_annotation = value;
			}
		}
		public string Title
		{
			get
			{
				return _title;
			}
			set
			{
				_title = value;
			}
		}
		public string Keywords
		{
			get
			{
				return _keywords;
			}
			set
			{
				_keywords = value;
			}
		}
		public string Description
		{
			get
			{
				return _description;
			}
			set
			{
				_description = value;
			}
		}
		[Browsable(false),
		DefaultValue(null),
		Description("The template property"),
		PersistenceMode(PersistenceMode.InnerProperty),
		TemplateContainer(typeof(RepeaterItem))]
		public virtual ITemplate HeaderTemplate { get; set; }
		[Browsable(false),
		DefaultValue(null),
		Description("The template property"),
		PersistenceMode(PersistenceMode.InnerProperty),
		TemplateContainer(typeof(RepeaterItem))]
		public virtual ITemplate FooterTemplate { get; set; }
		public override ControlCollection Controls
		{
			get
			{
				EnsureChildControls();
				return base.Controls;
			}
		}
		protected override void CreateChildControls()
		{
			Controls.Clear();
			BuildControlHierarhy(false);
		}

		public override void DataBind()
		{
			if (!IsLoaded)
			{
				IsLoaded = true;
				OnDataBinding(EventArgs.Empty);
				Controls.Clear();
				ClearChildViewState();
				if (ArticleOID != Guid.Empty)
				{
					LoadArticle(ArticleOID);
					BuildControlHierarhy(true);
				}
				ChildControlsCreated = true;
				base.DataBind();
			}
		}

		private void LoadArticle(Guid OID)
		{
			string xmlContent;
			_doc = null;
			string cacheName = "www_Article_" + ArticleOID;
			if (Context.Cache[cacheName] == null)
			{
				if (ArticleBody == null)//��� ��������� ���� �������
				{
					var prms = new Hashtable();
					prms["@OID"] = OID;
					_doc = DBReader.DBHelper.GetXmlData("xmlGetArticle", prms, null, true);
				}
				else _doc = ArticleBody;

				ProcessArticle();
				xmlContent = _doc.ToString();
				Context.Cache.Insert(cacheName, xmlContent, null, DateTime.MaxValue, TimeSpan.FromMinutes(20));
			}
			else
			{
				xmlContent = (string)Context.Cache[cacheName];
				_doc = XElement.Parse(xmlContent);
			}

			var n = _doc.Element("header");
			if (n != null) Header = n.Value;
			n = _doc.Element("annotation");
			if (n != null) Annotation = n.Value;
			n = _doc.Element("pubDate");
			if (n != null) PubDate = DateTime.Parse (n.Value, CultureInfo.CreateSpecificCulture("ru"));
			n = _doc.Element("title");
			if (n != null) Title = n.Value;
			n = _doc.Element("keywords");
			if (n != null) Keywords = n.Value;
			n = _doc.Element("pageDescription");
			if (n != null) Description = n.Value;
		}

		public void ProcessArticle()
		{//string element, 
			#region ��������� img
			if (ProcessImages)
			{
				var nodes = _doc.Element("body").Elements("img"); 
				foreach (XElement node in nodes)
				{
					string imageStyle = "";
					if (node.Attribute("align") == null)
					{
						imageStyle += "border:0;float:left;margin-right:10px;";
					}
					if (imageStyle != "") node.Add(new XAttribute("style", imageStyle));
				}
			}
			#endregion
		}
		protected void BuildControlHierarhy(bool useDataSource)
		{
			_headerItem = new RepeaterItem(0, ListItemType.Header);
			if (HeaderTemplate != null)
			{
				_headerItem.DataItem = Header;
				HeaderTemplate.InstantiateIn(_headerItem);
				Controls.Add(_headerItem);
			}
			if (useDataSource)
			{
				ControlItem items = new ControlItem("Article", ID);

				string body = "";
				var n = _doc.Element("body");
				if (n != null)
				{
					body = n.InnerXml();
				}
				//				Regex re = new Regex("<linkForm\\s*OID=\"([0-9A-F]{8,8}-[0-9A-F]{4,4}-[0-9A-F]{4,4}-[0-9A-F]{4,4}-[0-9A-F]{12,12})\"\\s*/>", RegexOptions.IgnoreCase | RegexOptions.Singleline);
				//				MatchCollection ms = re.Matches(body);
				int start = 0;
				//				foreach(Match m in ms) {
				//					if(m.Index != start) {
				//						string literalText = body.Substring(start, m.Index - start);
				//						Literal li = new Literal();
				//						Controls.Add(li);
				//						items.Child.Add(new ControlItem("Literal", li.ID));
				//						li.Text = literalText;
				//						GenericFormGenerator.FormConstructor frm = new GenericFormGenerator.FormConstructor();
				//						Controls.Add(frm);
				//						items.Child.Add(new ControlItem("FormConstructor", frm.ID));
				//						frm.FormOID = new Guid(m.Groups[1].Value);
				//					}
				//					start = m.Index + m.Length;
				//				}
				if (start != body.Length)
				{
					string literalText = body.Substring(start, body.Length - start);
					Literal li = new Literal();
					Controls.Add(li);
					items.Child.Add(new ControlItem("Literal", li.ID));
					li.Text = literalText;
				}
				ArticleControls = items.ToString();
			}
			else
			{
				ControlItem articleItem = ArticleControls;
				foreach (ControlItem item in articleItem.Child)
				{
					if (item.ClassName == "Literal")
					{
						Literal li = new Literal { ID = item.ID };
						Controls.Add(li);
					}
					//					else {
					//						GenericFormGenerator.FormConstructor frm = new GenericFormGenerator.FormConstructor();
					//						frm.ID = item.ID;
					//						Controls.Add(frm);
					//					}
				}
			}
			_footerItem = new RepeaterItem(0, ListItemType.Header);
			if (FooterTemplate != null)
			{
				_footerItem.DataItem = Header;
				FooterTemplate.InstantiateIn(_footerItem);
				Controls.Add(_footerItem);
			}
		}
	}

	public static class Linq2XmlExtension
	{
		public static string InnerXml(this XElement element)
		{
			if (element != null)
			{
				var reader = element.CreateReader();
				reader.MoveToContent();
				return reader.ReadInnerXml();
			}
			return string.Empty;
		}
	}
	
}
