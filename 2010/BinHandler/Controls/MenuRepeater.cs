using System;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Data;
using MetaData;


namespace GeneralControls
{
	/// <summary>
	/// Summary description for MenuRepeater.
	/// </summary>
	[ToolboxData("<{0}:MenuRepeater runat=server></{0}:MenuRepeater>")]
	[ParseChildren(false)]
	[ControlBuilder(typeof(MenuRepeaterControlBuilder))]
	public class MenuRepeater : PlaceHolder, INamingContainer
	{
		private Hashtable _templates = new Hashtable();
		private string _defaultTemplateName = "default";
		private string _nodeIDField = "nodeID";
		private DataSetISM _dataSource;

		public ManuRepeaterDetermineTemplateDelegate DetermineTemplate;
		public event RepeaterItemEventHandler ItemCreated;
		public event RepeaterItemEventHandler ItemDataBound;
		public event RepeaterCommandEventHandler ItemCommand;

		#region Properties
		public int SelectedNode {
			get {
				object o = ViewState["SelectedNode"];
				return o == null ? 0 : (int) o;
			}
			set {
				ViewState["SelectedNode"] = value;
			}
		}
		private int SelectedItem {
			get {
				object o = ViewState["SelectedItem"];
				return o == null ? -1 : (int) o;
			}
			set {
				ViewState["SelectedItem"] = value;
			}
		}
		private int ItemCount {
			get {
				object o = ViewState["ItemCount"];
				return o == null ? 0 : (int)o;}
			set {ViewState["ItemCount"] = value;}
		}

		public string DefaultTemplate {
			get	{return _defaultTemplateName;}
			set	{_defaultTemplateName = value;}
		}
		
		public string NodeIDField {
			get	{return _nodeIDField;}
			set	{_nodeIDField = value;}
		}

		public virtual DataSetISM DataSource {
			get	{return _dataSource;}
			set	{_dataSource = value;}
		}
		public string DataMember {
			get {
				object o = ViewState["DataMember"];
				return o == null ? "table" : (string)o;}
			set {ViewState["DataMember"] = value;}
		}

		public ControlCollection Items {
			get	{return this.Controls;}
		}
		#endregion

		#region Methods
		public void OnItemDataBound(RepeaterItemEventArgs e) {
			if(this.ItemDataBound != null)
				this.ItemDataBound(this, e);
		}

		public void OnItemCreated(RepeaterItemEventArgs e) {
			if(this.ItemCreated != null)
				this.ItemCreated(this, e);
		}

		public void OnItemCommand(RepeaterCommandEventArgs e) {
			if(this.ItemCommand != null)
				this.ItemCommand(this, e);
		}

		public void ClearCurState() {
			SelectedItem = -1;
			ItemCount = 0;
		}

		public override void DataBind() {
			// Controls with a data-source property perform their 
			// custom data binding by overriding DataBind to
			// evaluate any data-binding expressions on the control    
			// itself.
			base.OnDataBinding(EventArgs.Empty);

			//Reset tempates in ViewState - ��������������, �� �� ��� ��������������
			// Reset the control's state.
			Controls.Clear();
			ClearChildViewState();
			ClearCurState();

			// Create the control hierarchy using the data source.
			CreateControlHierarchy(true);
			ChildControlsCreated = true;

			TrackViewState();
		}

		protected override void CreateChildControls() {
			Controls.Clear();

			if (ItemCount > 0) {
				// Create the control hierarchy using the view state, 
				// not the data source.
				CreateControlHierarchy(false);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="useDataSource">True to create the hierarchy from the DataSource, False to create it from the ViewState.</param>
		private void CreateControlHierarchy(bool useDataSource) {
			int totalIndex = 0;
			bool _prevSelected = false, _nextSelected = false, _selected = false;
			
			if (!useDataSource) {
				// ViewState must have a non-null value for ItemCount because this is checked 
				//  by CreateChildControls.
				if (ItemCount > 0) {
					_dataSource = new DataSetISM();
					DataTable dt = new DataTable(DataMember);
					_dataSource.Tables.Add(dt);
					for (int i = 0; i < ItemCount; i++)
						dt.Rows.Add(new object []{});
				}
			}

			CreateItem(totalIndex, ListItemType.Header, useDataSource, null, "");
			totalIndex++;

			if (_dataSource != null) {
				for (int index = 0; index < _dataSource.Tables[DataMember].DefaultView.Count; index++) {
					DataRowView dataItem = _dataSource.Tables[DataMember].DefaultView[index];
					if(!useDataSource) { // ����� ��� ���������� ����������� �����������
						if(index == SelectedItem - 1) {
							_prevSelected = false;
							_selected = false;
							_nextSelected = true;
						}
						else if(index == SelectedItem) {
							_prevSelected = false;
							_selected = true;
							_nextSelected = false;
						}
						else if(index == SelectedItem + 1) {
							_prevSelected = true;
							_selected = false;
							_nextSelected = false;
						}
					}
					else { // ��� ��������� ���� � ���������� ����� ���� ������ _dataSource
						if(_selected) {
							_prevSelected = true;
							_selected = false;
							_nextSelected = false;
						}
						else if(_nextSelected) {
							_prevSelected = false;
							_selected = true;
							_nextSelected = false;
							SelectedItem = index;
						}
						if(_prevSelected) {
							_prevSelected = false;
							_selected = false;
							_nextSelected = false;
						}
						else {
							if(index != _dataSource.Tables[DataMember].DefaultView.Count - 1) {
								DataRowView nextItem = _dataSource.Tables[DataMember].DefaultView[index+1];
								if((int)nextItem[NodeIDField] == SelectedNode) {
									_prevSelected = false;
									_selected = false;
									_nextSelected = true;
								}
							}
						}
						if(index == 0 && (int)dataItem[NodeIDField] == SelectedNode) {
							_prevSelected = false;
							_selected = true;
							_nextSelected = false;
							SelectedItem = index;
						}
						else if(_dataSource.Tables[DataMember].DefaultView.Count == 1 && (int)dataItem[NodeIDField] == SelectedNode) { // ���� _nextSelected �� ��������� ��-�� ������ ��-�� �����
							_prevSelected = false;
							_selected = true;
							_nextSelected = false;
							SelectedItem = index;
						}
					}
//					if(SelectedItem == -1) {
//						if(useDataSource && (int)dataItem[NodeIDField] == SelectedNode) {
//							SelectedItem = index;
//						}
//					}
					if (index != SelectedItem) {
						if(index == 0) {
							if(CreateItem(totalIndex, ListItemType.Item, useDataSource, dataItem, "first") == null) {
								CreateItem(totalIndex, ListItemType.Item, useDataSource, dataItem, "");
							}
						}
						else if(index == _dataSource.Tables[DataMember].DefaultView.Count - 1) {
							if(CreateItem(totalIndex, ListItemType.Item, useDataSource, dataItem, "last") == null) {
								CreateItem(totalIndex, ListItemType.Item, useDataSource, dataItem, "");
							}
						}
						else {
							CreateItem(totalIndex, ListItemType.Item, useDataSource, dataItem, "");
						}
						totalIndex++;
					}
					else {
						CreateItem(totalIndex, ListItemType.Item, useDataSource, dataItem, "selected");
						totalIndex++;
					}
					if(index != _dataSource.Tables[DataMember].DefaultView.Count - 1) { // ������������ ����������
						if(_nextSelected) {
							if (CreateItem(totalIndex, ListItemType.Separator, useDataSource, dataItem, "selected_next") == null) 
								CreateItem(totalIndex, ListItemType.Separator, useDataSource, null, "");
						}
						else if(_selected) {
							if (CreateItem(totalIndex, ListItemType.Separator, useDataSource, dataItem, "selected") == null) 
								CreateItem(totalIndex, ListItemType.Separator, useDataSource, null, "");
						}
						else if(_prevSelected) {
							if (CreateItem(totalIndex, ListItemType.Separator, useDataSource, dataItem, "selected_prev") == null) 
								CreateItem(totalIndex, ListItemType.Separator, useDataSource, null, "");
						}
						else {
							CreateItem(totalIndex, ListItemType.Separator, useDataSource, null, "");
						}
						totalIndex++;
					}
				}
			}

			CreateItem(totalIndex, ListItemType.Footer, useDataSource, null, "");
			totalIndex++;

			if (useDataSource) {
				// Save the number of items contained for use in round trips.
				ItemCount = ((_dataSource != null) ? _dataSource.Tables[DataMember].DefaultView.Count : 0);
			}
		}

		private RepeaterItem CreateItem(int itemIndex, ListItemType itemType, bool dataBind, object dataItem, string TemplateName) {
			var item = new RepeaterItem(itemIndex, itemType);

			var e = new RepeaterItemEventArgs(item);

			//decide which template to use.
			string templateName = null;
			
			if(dataBind) {
				if (TemplateName == "")
					templateName = this.DefaultTemplate;
				else templateName = TemplateName;
				
				ViewState["templateName" + itemIndex.ToString()] = templateName;
			}
			else {
				//determine template to use from viewState;
				templateName = (string)ViewState["templateName" + itemIndex.ToString()];
			}

			if (itemType == ListItemType.Header)
				templateName = "header_"+templateName;
			else if (itemType == ListItemType.Footer)
				templateName = "footer_"+templateName;
			else if (itemType == ListItemType.Separator)
				templateName = "separator_"+templateName;

			var dynamicTemplate = (MyRepeaterTemplate)_templates[templateName];

			if(dynamicTemplate == null) return null;
			dynamicTemplate.ItemTemplate.InstantiateIn(item);
			
			if (dataBind) {
				item.DataItem = dataItem;
			}
			OnItemCreated(e);
			this.Controls.Add(item);

			if (dataBind) {
				item.DataBind();
				OnItemDataBound(e);

				item.DataItem = null;
			}

			return item;
		}

		protected override bool OnBubbleEvent(object source, EventArgs e) {
			// Handle events raised by children by overriding OnBubbleEvent.

			bool handled = false;

			if (e is CommandEventArgs) {
				RepeaterCommandEventArgs ce = (RepeaterCommandEventArgs)e;

				OnItemCommand(ce);
				handled = true;
			}

			return handled;
		}


		/// <summary>
		/// This member overrides Control.AddParsedSubObject.
		/// it catches the contents of each item.
		/// </summary>
		protected override void AddParsedSubObject(object obj) {
			if(obj is ObjectTemplate) {	
				ObjectTemplate template = (ObjectTemplate)obj;
				_templates.Add(template.Name, template);
				return;
			}
			else if (obj is HeaderTemplate) {
				HeaderTemplate template = (HeaderTemplate)obj;
				_templates.Add("header_"+template.Name, template);
				return;
			}
			else if (obj is FooterTemplate) {
				FooterTemplate template = (FooterTemplate)obj;
				_templates.Add("footer_"+template.Name, template);
				return;
			}
			else if (obj is SeparatorTemplate) {
				SeparatorTemplate template = (SeparatorTemplate)obj;
				_templates.Add("separator_"+template.Name, template);
				return;
			}
			else {
				if(!(obj is LiteralControl))
					throw new Exception("Undefined template in ObjectRepeater");
			}
			base.AddParsedSubObject(obj);
		}


		#endregion
	}
	public delegate string ManuRepeaterDetermineTemplateDelegate(object sender, object dataItem);

	#region Control Builder
	/// <summary>
	/// Interacts with the parser to build a PanelBar control.
	/// </summary>
	public class MenuRepeaterControlBuilder : ControlBuilder {
		/// <summary>
		/// This member overrides ControlBuilder.GetChildControlType.
		/// </summary>
		public override Type GetChildControlType(string tagName, IDictionary attributes) {
			// check is the tag is an TabStripPanelItem tag
			if (tagName.ToLower().IndexOf("objecttemplate") >= 0) {
				//System.Diagnostics.Trace.WriteLine(tagName);

				// yes, so return TabStripPanelItem type
				return typeof(ObjectTemplate);
			}
			else if (tagName.ToLower().IndexOf("headertemplate") >= 0) {
				//System.Diagnostics.Trace.WriteLine(tagName);

				// yes, so return TabStripPanelItem type
				return typeof(HeaderTemplate);
			}
			else if (tagName.ToLower().IndexOf("footertemplate") >= 0) {
				//System.Diagnostics.Trace.WriteLine(tagName);

				// yes, so return TabStripPanelItem type
				return typeof(FooterTemplate);
			}
			else if (tagName.ToLower().IndexOf("separatortemplate") >= 0) {
				//System.Diagnostics.Trace.WriteLine(tagName);

				// yes, so return TabStripPanelItem type
				return typeof(SeparatorTemplate);
			}
			return null;
		}

		public override void AppendLiteralString(string s) {
			//empty implementation to drop literal content.
			//System.Diagnostics.Trace.WriteLine(s);
		}

	}
	#endregion

	[ParseChildren(true)]
	public class MyRepeaterTemplate : Control {
		private string _name;
		private ITemplate _itemTemplate;

		[Browsable(false)]
		[TemplateContainer(typeof(RepeaterItem))]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[DefaultValue(typeof(ITemplate), "")]
		public ITemplate ItemTemplate {
			get { return _itemTemplate; }
			set { _itemTemplate = value; }
		}

		public string Name {
			get { return _name; }
			set { _name = value; }
		}
	}

	[ParseChildren(true)]
	public class ObjectTemplate : MyRepeaterTemplate {
	}

	[ParseChildren(true)]
	public class HeaderTemplate : MyRepeaterTemplate {
	}

	[ParseChildren(true)]
	public class FooterTemplate : MyRepeaterTemplate {
	}

	[ParseChildren(true)]
	public class SeparatorTemplate : MyRepeaterTemplate {
	}
}
