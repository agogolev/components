using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Data;
using MetaData;


namespace GeneralControls
{
	/// <summary>
	/// Summary description for HVRepeater.
	/// </summary>
	[ToolboxData("<{0}:HVRepeater runat=server></{0}:HVRepeater>")]
	[ParseChildren(false)]
	[ControlBuilder(typeof(MenuRepeaterControlBuilder))]
	public class HVRepeater : PlaceHolder, INamingContainer
	{
		private Hashtable fields = new Hashtable();
		private Hashtable _templates = new Hashtable();
		private string _defaultTemplateName = "default";
		private DataSetISM _modDataSource;

		public ManuRepeaterDetermineTemplateDelegate DetermineTemplate;
		public event RepeaterItemEventHandler ItemCreated;
		public event RepeaterItemEventHandler ItemDataBound;
		public event RepeaterCommandEventHandler ItemCommand;

		#region Properties
		private int ItemCount {
			get {
				object o = ViewState["ItemCount"];
				return o == null ? 0 : (int)o;}
			set {ViewState["ItemCount"] = value;}
		}

		public string DefaultTemplate {
			get	{return _defaultTemplateName;}
			set	{_defaultTemplateName = value;}
		}

		public virtual DataSetISM DataSource { get; set; }
		public string DataMember {
			get {
				object o = ViewState["DataMember"];
				return o == null ? "table" : (string)o;}
			set {ViewState["DataMember"] = value;}
		}

		public int HorItemCount {
			get {
				object o = ViewState["HorItemCount"];
				return o == null ? 1 : (int)o;}
			set {ViewState["HorItemCount"] = value;}
		}
		public ControlCollection Items {
			get	{return Controls;}
		}
		#endregion

		#region Methods
		public void OnItemDataBound(RepeaterItemEventArgs e) {
			if(ItemDataBound != null)
				ItemDataBound(this, e);
		}

		public void OnItemCreated(RepeaterItemEventArgs e) {
			if(ItemCreated != null)
				ItemCreated(this, e);
		}

		public void OnItemCommand(RepeaterCommandEventArgs e) {
			if(ItemCommand != null)
				ItemCommand(this, e);
		}

		public void ClearCurState() {
			ItemCount = 0;
		}

		public override void DataBind() {
			// Controls with a data-source property perform their 
			// custom data binding by overriding DataBind to
			// evaluate any data-binding expressions on the control    
			// itself.
			base.OnDataBinding(EventArgs.Empty);

			//Reset tempates in ViewState - ��������������, �� �� ��� ��������������
			// Reset the control's state.
			Controls.Clear();
			ClearChildViewState();

			// Create the control hierarchy using the data source.
			CreateControlHierarchy(true);
			ChildControlsCreated = true;

			TrackViewState();
		}

		protected override void CreateChildControls() {
			Controls.Clear();

			if (ItemCount > 0) {
				// Create the control hierarchy using the view state, 
				// not the data source.
				CreateControlHierarchy(false);
			}
		}

		private string ModifyExpression(string expression, int i) {
			if(expression == "") return "";
			foreach(DictionaryEntry de in fields) {
				expression = expression.ToLower().Replace(((string)de.Key).ToLower(), String.Format("{0}_{1}", ((string)de.Key).ToLower(), i));
			}
			return expression;
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="useDataSource">True to create the hierarchy from the DataSource, False to create it from the ViewState.</param>
		private void CreateControlHierarchy(bool useDataSource) {
			int totalIndex = 0;
			
			if (!useDataSource) {
				// ViewState must have a non-null value for ItemCount because this is checked 
				//  by CreateChildControls.
				if (ItemCount > 0) {
					_modDataSource = new DataSetISM();
					DataTable dt = new DataTable(DataMember);
					_modDataSource.Tables.Add(dt);
					for (int i = 0; i < ItemCount; i++)
						dt.Rows.Add(new object []{});
				}
			}
			else {
				if (DataSource == null || DataSource.Tables[DataMember].Rows.Count == 0) return;
				if(HorItemCount != 1) {
					int rem = (HorItemCount - (DataSource.Tables[DataMember].Rows.Count % HorItemCount)) % HorItemCount;
					for (int i = 0; i < rem; i++)
					{
						DataRow dr = DataSource.Tables[DataMember].NewRow();
						DataSource.Tables[DataMember].Rows.Add(dr);
					}
					_modDataSource = new DataSetISM();
					DataTable dt = new DataTable(DataMember);
					_modDataSource.Tables.Add(dt);
					for(int i = 0; i < HorItemCount; i++) {
            foreach(DataColumn dc in DataSource.Tables[DataMember].Columns) {
							DataColumn column = new DataColumn(String.Format("{0}_{1}", dc.ColumnName, i), dc.DataType, ModifyExpression(dc.Expression, i)) { AllowDBNull = dc.AllowDBNull };
							dt.Columns.Add(column);	
							fields[dc.ColumnName] = 1;									 
						}
						
					}
					object [] values = new object[DataSource.Tables[DataMember].Columns.Count * HorItemCount];
					for(int i = 0; i < DataSource.Tables[DataMember].Rows.Count; i += HorItemCount) {
						int pos = 0;
						for(int k = 0; k < HorItemCount; k++){
							DataRow dr = DataSource.Tables[DataMember].Rows[i+k];
							for(int j = 0; j < DataSource.Tables[DataMember].Columns.Count; j++) {
								object value = dr[j];
								values[pos] = value;
								pos++;
							}
						}
						dt.Rows.Add(values);
					} 
				}
				else _modDataSource = DataSource;
			}

			CreateItem(totalIndex, ListItemType.Header, useDataSource, null, "");
			totalIndex++;

			if (_modDataSource != null) {
				for (int index = 0; index < _modDataSource.Tables[DataMember].DefaultView.Count; index++) {
					DataRowView dataItem = _modDataSource.Tables[DataMember].DefaultView[index];
					if(index == 0) {
						if(CreateItem(totalIndex, ListItemType.Item, useDataSource, dataItem, "first") == null) {
							CreateItem(totalIndex, ListItemType.Item, useDataSource, dataItem, "");
						}
					}
					else if(index == _modDataSource.Tables[DataMember].DefaultView.Count - 1) {
						if(CreateItem(totalIndex, ListItemType.Item, useDataSource, dataItem, "last") == null) {
							CreateItem(totalIndex, ListItemType.Item, useDataSource, dataItem, "");
						}
					}
					else {
						CreateItem(totalIndex, ListItemType.Item, useDataSource, dataItem, "");
					}
					totalIndex++;
					if(index != _modDataSource.Tables[DataMember].DefaultView.Count - 1) {
						CreateItem(totalIndex, ListItemType.Separator, useDataSource, null, "");
						totalIndex++;
					}
				}
			}

			CreateItem(totalIndex, ListItemType.Footer, useDataSource, null, "");
			totalIndex++;

			if (useDataSource) {
				// Save the number of items contained for use in round trips.
				ItemCount = ((_modDataSource != null) ? _modDataSource.Tables[DataMember].DefaultView.Count : 0);
			}
		}

		private RepeaterItem CreateItem(int itemIndex, ListItemType itemType, bool dataBind, object dataItem, string TemplateName) {
			RepeaterItem item = new RepeaterItem(itemIndex, itemType);

			RepeaterItemEventArgs e = new RepeaterItemEventArgs(item);

			//decide which template to use.
			string templateName = null;
			
			if(dataBind) {
				if (TemplateName == "")
					templateName = DefaultTemplate;
				else templateName = TemplateName;
				
				ViewState["templateName" + itemIndex.ToString()] = templateName;
			}
			else {
				//determine template to use from viewState;
				templateName = (string)ViewState["templateName" + itemIndex.ToString()];
			}

			if (itemType == ListItemType.Header)
				templateName = "header_"+templateName;
			else if (itemType == ListItemType.Footer)
				templateName = "footer_"+templateName;
			else if (itemType == ListItemType.Separator)
				templateName = "separator_"+templateName;

			MyRepeaterTemplate dynamicTemplate = (MyRepeaterTemplate)_templates[templateName];

			if(dynamicTemplate == null) return null;
			dynamicTemplate.ItemTemplate.InstantiateIn(item);
			
			if (dataBind) {
				item.DataItem = dataItem;
			}
			OnItemCreated(e);
			Controls.Add(item);

			if (dataBind) {
				item.DataBind();
				OnItemDataBound(e);

				item.DataItem = null;
			}

			return item;
		}

		protected override bool OnBubbleEvent(object source, EventArgs e) {
			// Handle events raised by children by overriding OnBubbleEvent.

			bool handled = false;

			if (e is CommandEventArgs) {
				RepeaterCommandEventArgs ce = (RepeaterCommandEventArgs)e;

				OnItemCommand(ce);
				handled = true;
			}

			return handled;
		}


		/// <summary>
		/// This member overrides Control.AddParsedSubObject.
		/// it catches the contents of each item.
		/// </summary>
		protected override void AddParsedSubObject(object obj) {
			if(obj is ObjectTemplate) {	
				ObjectTemplate template = (ObjectTemplate)obj;
				_templates.Add(template.Name, template);
				return;
			}
			else if (obj is HeaderTemplate) {
				HeaderTemplate template = (HeaderTemplate)obj;
				_templates.Add("header_"+template.Name, template);
				return;
			}
			else if (obj is FooterTemplate) {
				FooterTemplate template = (FooterTemplate)obj;
				_templates.Add("footer_"+template.Name, template);
				return;
			}
			else if (obj is SeparatorTemplate) {
				SeparatorTemplate template = (SeparatorTemplate)obj;
				_templates.Add("separator_"+template.Name, template);
				return;
			}
			else {
				if(!(obj is LiteralControl))
					throw new Exception("Undefined template in ObjectRepeater");
			}
			base.AddParsedSubObject(obj);
		}


		#endregion
	}
}
