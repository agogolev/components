using System;
using System.Xml;
using System.Xml.Xsl;
using BinHandler;
using System.Web;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using MetaData;
using System.Collections;
using System.Configuration;
using System.Web.UI;
using System.Net.Mail;
using System.Collections.Generic;

namespace GeneralControls
{
	/// <summary>
	/// Summary description for BaseGenericPage.
	/// </summary>
	public class BaseGenericPage : Page
	{
		/// <summary>
		/// ���������� ��� ��������� ������ ���������� �������� �� ���� body t_Article ��� ����� xmlGetArticle
		/// </summary>
		/// <param name="_doc">XmlDocument, � ������� �� �����, � ���� �� �������</param>
		public static void ProcessArticle(XmlDocument _doc)//string element, 
		{
			XmlElement root = _doc.DocumentElement;
			#region ��������� linkImage
			XmlNodeList nodes = root.SelectNodes("//linkimages");//element+
			foreach (XmlNode node in nodes)
			{
				XmlNode trueParentNode = node.ParentNode;
				XmlNode replacedNode = node;
				string ImageOID = node.Attributes["oid"].Value;
				string ImageLink = "bin.aspx?ID=";
				ImageLink += ImageOID;

				XmlNode newImgNode = null;
				XmlNode newNode = _doc.CreateElement("img");
				XmlNode trueImgNode = newNode;

				//�������� ���� ��� ��������
				if (ImageOID != "")
				{
					if (node.Attributes["alt"] == null)
					{
						XmlNode title = root.SelectSingleNode(String.Format("//titleImage[text()='{0}']/@title", ImageOID));
						if (title != null)
							newNode.Attributes.Append(_doc.CreateAttribute("alt")).Value = title.Value;
					}
					else newNode.Attributes.Append(_doc.CreateAttribute("alt")).Value = node.Attributes["alt"].Value;
				}

				string imageStyle = "";
				if (node.Attributes["style"] != null)
				{
					imageStyle = node.Attributes["style"].Value;
				}

				newNode.Attributes.Append(_doc.CreateAttribute("src")).Value = ImageLink;
				XmlNodeList nlAttr = node.SelectNodes("@*[local-name() != 'oid' and local-name() != 'style' and local-name() != 'align' and local-name() != 'mode']");
				foreach (XmlNode nAttr in nlAttr)
				{
					newNode.Attributes.Append(_doc.CreateAttribute(nAttr.LocalName)).Value = nAttr.Value;
				}
				if (node.Attributes["align"] != null)
				{
					if (node.Attributes["align"].Value == "left")
					{
						imageStyle += "border:0;float:left;margin-right:10px;";
					}
					else if (node.Attributes["align"].Value == "center")
					{
						newImgNode = _doc.CreateElement("table");
						newImgNode.Attributes.Append(_doc.CreateAttribute("align")).Value = "center";
						newImgNode.Attributes.Append(_doc.CreateAttribute("cellpadding")).Value = "0";
						newImgNode.Attributes.Append(_doc.CreateAttribute("cellspacing")).Value = "0";
						newImgNode.Attributes.Append(_doc.CreateAttribute("border")).Value = "0";
						XmlNode tr = newImgNode.AppendChild(_doc.CreateElement("tr"));
						XmlNode td = tr.AppendChild(_doc.CreateElement("td"));
						if (node.ParentNode.LocalName.ToLower() == "a")
						{
							// a ���� ��������� ������ table
							trueParentNode = node.ParentNode.ParentNode;
							replacedNode = node.ParentNode;
							XmlNode anchorNode = _doc.CreateElement("a");
							foreach (XmlAttribute atrAnc in node.ParentNode.Attributes)
							{
								anchorNode.Attributes.Append(_doc.CreateAttribute(atrAnc.LocalName)).Value = atrAnc.Value;
							}
							anchorNode.AppendChild(newNode);
							newNode = anchorNode;
						}
						td.AppendChild(newNode);
					}
					else
					{
						imageStyle += "border:0;float:right;margin-left:10px;";
					}
				}
				if (imageStyle != "") trueImgNode.Attributes.Append(_doc.CreateAttribute("style")).Value = imageStyle;
				if (newImgNode == null) newImgNode = newNode;
				trueParentNode.ReplaceChild(newImgNode, replacedNode);
			}
			#endregion
			#region ��������� a
			nodes = root.SelectNodes("//a");//element+
			foreach (XmlNode node in nodes)
			{
				string href = node.Attributes["href"].Value;
				if (href.IndexOf("http://") != -1)//���� ������� �����
				{
					if (node.Attributes["target"] == null)
						node.Attributes.Append(_doc.CreateAttribute("target")).Value = "_blank";
				}
				else if (href.IndexOf("mailto") != -1)//���� mail
				{
					string decoded = HttpUtility.UrlDecode(href, Encoding.UTF8);//�������� ���������� �������
					node.Attributes["href"].Value = decoded;
				}
				else if (href[0] == '/')//���� ���������� ����� - ������� ������ � ����������� �� �������������/��������������� Mode Rewrite
					node.Attributes["href"].Value = Rewrite.Eval(href);

				XmlNodeList insideImg = node.SelectNodes(".//img");
				foreach (XmlNode img in insideImg)
					if (img.Attributes["border"] == null) img.Attributes.Append(_doc.CreateAttribute("border")).Value = "0";
			}
			#endregion
		}

		/// <summary>
		/// ���������� ��� ��������� ������� ������� (������������ ���)
		/// </summary>
		/// <param name="parentNode">������������ ��� ��� �������</param>
		public static DataSetISM GetMenuDS(int parentNode)
		{
			DataSetISM ds;
			string cacheName = "www_GetMenuDS_" + parentNode;
			if (HttpContext.Current.Cache[cacheName] == null)
			{
				string sql = @"
SELECT 
	nodeID = n.nodeID, 
	nodeName = REPLACE(n.nodeName, CHAR(10), '<br />'), 
	alt = n.nodeName, 
	nodeUrl = n.nodeUrl,
	object = n.object
FROM t_Nodes n
WHERE
	n.parentNode = @parentNode
ORDER BY n.lft";
				Hashtable prms = new Hashtable();
				prms["@parentNode"] = parentNode;
				ds = DBReader.DBHelper.GetDataSetISMSql(sql, "table", prms, false, null);
				HttpContext.Current.Cache.Insert(cacheName, ds, null, DateTime.MaxValue, TimeSpan.FromMinutes(20));
			}
			else
			{
				ds = (DataSetISM)HttpContext.Current.Cache[cacheName];
			}

			return ds;
		}

		public static DataSetISM GetSomethingCache(string sql, string cacheID)
		{
			DataSetISM ds;
			string cacheName = "www_GetSomethingCache_" + cacheID;
			if (HttpContext.Current.Cache[cacheName] == null)
			{
				ds = DBReader.DBHelper.GetDataSetISMSql(sql, "table", null, false, null);
				HttpContext.Current.Cache.Insert(cacheName, ds, null, DateTime.MaxValue, TimeSpan.FromMinutes(20));
			}
			else
			{
				ds = (DataSetISM)HttpContext.Current.Cache[cacheName];
			}

			return ds;
		}

		public static Guid GetNodeObject(int nodeID)
		{
			Guid objectOID = Guid.Empty;
			string cacheName = "www_GetNodeObject_" + nodeID;
			if (HttpContext.Current.Cache[cacheName] == null)
			{
				string sql =
@"SELECT n.object
FROM t_Nodes n
WHERE nodeID = @nodeID";
				Hashtable prms = new Hashtable();
				prms["@nodeID"] = nodeID;
				object res = DBReader.DBHelper.ExecuteScalar(sql, prms, false);

				if (res is Guid)
					objectOID = (Guid)res;

				HttpContext.Current.Cache.Insert(cacheName, objectOID, null, DateTime.MaxValue, TimeSpan.FromMinutes(20));
			}
			else
			{
				objectOID = (Guid)HttpContext.Current.Cache[cacheName];
			}

			return objectOID;
		}
		public static string GetNodeName(int nodeID)
		{
			string result = "";
			string cacheName = "www_GetNodeName_" + nodeID;
			if (HttpContext.Current.Cache[cacheName] == null)
			{
				string sql =
@"SELECT n.nodeName
FROM t_Nodes n
WHERE nodeID = @nodeID";
				Hashtable prms = new Hashtable();
				prms["@nodeID"] = nodeID;
				object o = DBReader.DBHelper.ExecuteScalar(sql, prms, false);
				if (o != null) result = (string)o;

				HttpContext.Current.Cache.Insert(cacheName, result, null, DateTime.MaxValue, TimeSpan.FromMinutes(20));
			}
			else
			{
				result = (string)HttpContext.Current.Cache[cacheName];
			}

			return result;
		}

		public static bool UseAuth()
		{
			foreach (string key in ConfigurationManager.AppSettings.Keys)
			{
				if (key.ToLower() == "smtplogin") return true;
			}
			return false;
		}

		public string SendTransformedMail(string fromAddress, string fromName, string subject, string addrsTo, XmlDocument doc, string xslt, string bodyType, string paramNames, string paramValues)
		{
			try
			{
				string[] parNames = null;
				string[] parValues = null;
				if (paramNames != null) parNames = paramNames.Split(",;".ToCharArray());
				if (paramValues != null) parValues = paramValues.Split(",;".ToCharArray());
				using (var smtpClient = new SmtpClient())
				{
					smtpClient.EnableSsl = smtpClient.Port == 587 || smtpClient.Port == 465;
					using (var msg = new MailMessage())
					{
						msg.From = new MailAddress(fromAddress, fromName);
						msg.To.Add(addrsTo.Replace(";", ","));
						if (bodyType == null || bodyType == "" || bodyType.ToLower() == "html")
							msg.IsBodyHtml = true;
						else
							msg.IsBodyHtml = false;
						msg.Subject = subject;
						XslCompiledTransform xsl = new XslCompiledTransform();
						string fileName = Context.Server.MapPath(xslt);
						xsl.Load(fileName);
						StringBuilder sb = new StringBuilder();
						using (StringWriter sw = new StringWriter(sb))
						{
							XsltArgumentList args = null;
							if (parNames != null && parNames.Length != 0)
							{
								args = new XsltArgumentList();
								for (int i = 0; i < parNames.Length; i++)
									args.AddParam(parNames[i], "", parValues[i]);
							}
							xsl.Transform(doc, args, sw);
						}
						msg.Body = sb.ToString();
						smtpClient.Send(msg);
					}
				}
			}
			catch (Exception ex)
			{
				return ex.Message;
			}
			return "";
		}

		public static bool SendMail(string fromAddress, string fromName, string subject, string addrsTo, string body, string format)
		{
			using (var smtpClient = new SmtpClient())
			{
				MailAddress from = new MailAddress(fromAddress, fromName);
				MailAddress to = new MailAddress(addrsTo.Replace(",", ";"), "");
				smtpClient.EnableSsl = smtpClient.Port == 587 || smtpClient.Port == 465;
				using (var msg = new MailMessage(from, to))
				{
					if (format == "" || format.ToLower().Trim() == "html")
						msg.IsBodyHtml = true;
					else
						msg.IsBodyHtml = false;
					msg.BodyEncoding = Encoding.UTF8;
					msg.Subject = subject;
					msg.Body = body;
					try
					{
						smtpClient.Send(msg);
					}
					catch
					{
						return false;
					}
				}
			}
			return true;
		}

		public static bool SendMail(string fromAddress, string fromName, string subject, string addrsTo, string body)
		{
			using (var smtpClient = new SmtpClient())
			{
				MailAddress from = new MailAddress(fromAddress, fromName);
				MailAddress to = new MailAddress(addrsTo.Replace(",", ";"), "");
				smtpClient.EnableSsl = smtpClient.Port == 587 || smtpClient.Port == 465;
				using (var msg = new MailMessage(from, to))
				{
					msg.IsBodyHtml = false;
					msg.BodyEncoding = Encoding.UTF8;
					msg.Subject = subject;
					msg.Body = body;
					try
					{
						smtpClient.Send(msg);
					}
					catch
					{
						return false;
					}
				}
			}
			return true;
		}

		public static bool SendMail(string fromAddress, string fromName, string subject, string addrsTo, string body, List<Attachment> attachCollection)
		{
			using (var smtpClient = new SmtpClient())
			{
				MailAddress from = new MailAddress(fromAddress, fromName);
				MailAddress to = new MailAddress(addrsTo.Replace(",", ";"), "");
				smtpClient.EnableSsl = smtpClient.Port == 587 || smtpClient.Port == 465;
				using (var msg = new MailMessage(from, to))
				{
					msg.IsBodyHtml = false;
					msg.BodyEncoding = Encoding.UTF8;
					msg.Subject = subject;
					msg.Body = body;
					if (attachCollection != null)
					{
						foreach (Attachment attach in attachCollection) msg.Attachments.Add(attach);
					}
					try
					{
						smtpClient.Send(msg);
					}
					catch
					{
						return false;
					}
				}
			}
			return true;
		}

		public HttpCookie GetSomeCookie(string CookieName)
		{
			HttpCookie c = Page.Request.Cookies[CookieName];
			if (c == null)
			{
				c = new HttpCookie(CookieName);
				c.Expires = DateTime.Now.Add(new TimeSpan(30, 0, 0, 0, 0));
				Response.Cookies.Set(c);
			}
			return c;
		}

		public void SetSomeCookie(string CookieName, string Key, string Value)
		{
			if (Key != null && Value != null)
			{
				HttpCookie c = Response.Cookies[CookieName];
				if (c.Expires <= DateTime.Now)//��������� ���� ���������
					c.Expires = DateTime.Now.Add(new TimeSpan(30, 0, 0, 0, 0));

				c.Values[Key] = Value;

				Response.Cookies.Set(c);
			}
		}

		public void ShowError(string message)
		{
			ClientScriptManager cs = Page.ClientScript;
			Type cstype = GetType();
			string csname = "error";
			if (!cs.IsClientScriptBlockRegistered(cstype, csname))
			{
				message = message.Replace("\r", " ").Replace("\n", "").Replace("'", "\"");
				Page.ClientScript.RegisterClientScriptBlock(cstype, csname, String.Format("<script language='JavaScript'>alert('{0}');</script>", message), false);
			}
		}
		public string GetParam(string ParamName)
		{
#if(ISAPI_Rewrite)
			Regex r = new Regex(String.Format("/{0}_(?<value>[^/]*)", ParamName), RegexOptions.IgnoreCase);
			Match m = r.Match(Request.Headers["X-Rewrite-URL"]);
			return m.Groups["value"].Value != "" ? m.Groups["value"].Value : Request[ParamName];
#else
			return Request.Params[ParamName];
#endif
		}
	}
}
