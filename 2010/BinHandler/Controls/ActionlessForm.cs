using System;
using System.Web.UI;

namespace GeneralControls
{
	public class ActionlessForm : System.Web.UI.HtmlControls.HtmlForm
	{
		public string action="";

		protected override void RenderAttributes(HtmlTextWriter writer)
		{
			writer.WriteAttribute("name", Name);
			base.Attributes.Remove("name");

			writer.WriteAttribute("method", Method);
			base.Attributes.Remove("method");

			if (action != "")
				writer.WriteAttribute("action", action);
			base.Attributes.Remove("action");

			Attributes.Render(writer);

			if (base.ID != null)
				writer.WriteAttribute("id", base.ClientID);
		}
	}

}
