using System;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using System.Text;

namespace BinHandler
{
	/// <summary>
	/// Summary description for BinHandler.
	/// </summary>
	public class BinHandler : IHttpHandler
	{
		bool IHttpHandler.IsReusable 
		{
			get { return true; }
		}
		
		void IHttpHandler.ProcessRequest(HttpContext context)
		{
			string binHandlerType = (string)GetValue("binHandlerType");//тип используемого бин хэндлера

			HttpRequest Request = context.Request;
			HttpResponse Response = context.Response;

			string ID = "";
			if(Request["ID"] != null) ID = Request["ID"];
			else if(Rewrite.GetParam("ID", Request) != null) ID = Rewrite.GetParam("ID", Request);
			using (SqlConnection myConn = new SqlConnection((string)GetValue("dbdata.connection")))
			{
				SqlCommand myCmd = new SqlCommand("select data, mimetype, fileName from t_BinaryData where OID = @ID", myConn);
				myCmd.Parameters.AddWithValue("@ID", ID);
				try
				{
					myConn.Open();
					SqlDataReader rdr = myCmd.ExecuteReader();
					try
					{
						if (rdr.Read())
						{
							string mimeType = ((string)rdr.GetValue(1)).ToLower().Trim();
							string fileName = rdr.GetValue(2).ToString().Trim();
							if (mimeType.IndexOf("image") != -1 || mimeType.IndexOf("flash") != -1)
								Response.ContentType = mimeType;
							else
								if (mimeType.IndexOf("msword") != -1 || mimeType.IndexOf("ms-excel") != -1)
								{
									Response.ContentType = mimeType;
									//Response.ContentEncoding = Encoding.UTF8; 
									binHandlerType = "1";
								}
								else
									if (mimeType.IndexOf("pdf") != -1)
									{
										Response.ContentType = mimeType;
										binHandlerType = "1";
									}
									else
									{
										Response.AppendHeader("Content-Type", "application/download");
										binHandlerType = "1"; //неизвестные файлы полюбому предлагаем сохранять
									}
							if (binHandlerType == "1" || Request.Path.ToLower().IndexOf("binsave") != -1)
								Response.AppendHeader("content-disposition", "attachment;filename=" + HttpUtility.UrlEncode(fileName));
							Response.Cache.AppendCacheExtension("post-check=900,pre-check=3600");
							Response.OutputStream.Write(rdr.GetSqlBinary(0).Value, 0, rdr.GetSqlBinary(0).Length);
							Response.OutputStream.Close();
						}
					}
					finally
					{
						rdr.Close();
					}
				}
				catch (Exception ex)
				{
					Response.Write(ex.Message);
				}
				finally
				{
					myConn.Close();
				}
			}
		}
		
		public static object GetValue(string name)
		{
			string str = ConfigurationManager.AppSettings[name];
			if (str == null)
				return "";
			else return str;
		}
	}
}
