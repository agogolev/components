using System;
using System.Text;
using System.Web;
using System.Text.RegularExpressions;

namespace BinHandler 
{
	/// <summary>
	/// Summary description for Rewrite.
	/// </summary>
	public class Rewrite 
	{
		/// <summary>
		/// Функция преобразования URL в зависимости от использования/неиспользования Mode Rewrite
		/// Пример: из default.aspx?par1=1&par2=2 делает default/par1_1/par2_2
		/// </summary>
		/// <param name="sourceUrl">Исходный URL</param>
		/// <returns>Преобразованный URL</returns>
		public static string Eval(string sourceUrl) 
		{
#if (ISAPI_Rewrite)
			if (sourceUrl.Length == 0)
				return "";

			string[] parts = sourceUrl.Split("?&".ToCharArray());
			StringBuilder url = new StringBuilder();
			string addr = parts[0];

			//уберём ~ из адресов
			if (addr[0] == '~')
				addr = addr.Substring(1);
			//пути делаем от корня - иначе при использовании виртуальных папок будут траблы
			if (addr[0] != '/')
				addr = "/"+addr;
			//уюерём точку и расширение .aspx
			int comma = addr.IndexOf('.');
			if (comma != -1) 
				url.Append(addr.Substring(0, comma)+'/');
			else url.Append(addr+'/');
			//преобразуем параметры
			for(int i = 1; i < parts.Length; i++) {
				url.Append(parts[i].Replace('=', '_')+'/');
			}
			url.Length -= 1;

			return url.ToString();
#else
			return sourceUrl;
#endif
		}

		/// <summary>
		/// Функция получения значений параметров из QueryString в зависимости от использования/неиспользования Mode Rewrite
		/// </summary>
		/// <param name="ParamName">Имя параметра</param>
		/// <param name="CurRequest">Текущий HttpRequest</param>
		/// <returns>Значение параметра</returns>
		public static string GetParam(string ParamName, HttpRequest CurRequest)
		{
#if(ISAPI_Rewrite)
			Regex r = new Regex(String.Format(".*/{0}_(?<value>[a-zA-Z0-9,\\.-]+)/?.*", ParamName), RegexOptions.IgnoreCase);
			Match m = r.Match(CurRequest.Headers["X-Rewrite-URL"]);
			return m.Groups["value"].Value != "" ? m.Groups["value"].Value : null;
#else
      return Request[ParamName];
#endif
		}

/*		/// <summary>
		/// Функция предназначена для выкусывания пути до имени самой страницы
		/// </summary>
		/// <param name="sourceURL">Исходный URL</param>
		/// <returns>Преобразованный URL</returns>
		public static string SpellPath(string sourceURL)
		{
//TODO: доделать!!!
			return sourceURL;
		}*/
	}
}
