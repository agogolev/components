using System;
using System.Collections;
using System.Text;
using System.Text.RegularExpressions;

namespace BinHandler
{
	/// <summary>
	/// Summary description for ControlItem.
	/// </summary>
	public class ControlItem {
		public string ClassName;
		public string ID;
		public ArrayList Child;
		public ControlItem(string className, string id) {
			ClassName = className;
			ID = id;
			Child = new ArrayList();
		}
		public ControlItem() {
			Child = new ArrayList();
		}
		public override string ToString() {
			StringBuilder sb = new StringBuilder();
			sb.Append(String.Format("{{\"{0}\",\"{1}\",[", ClassName, ID));
			if(Child.Count != 0) {
				foreach(ControlItem ctrl in Child) {
					sb.Append(ctrl.ToString()+",");
				}
				sb.Length -= 1;
			}
			sb.Append("]}");
			return sb.ToString();
		}
		public static implicit operator ControlItem(string value)	{
			ControlItem item = new ControlItem();
			Regex re = new Regex("^{\"([^\"]*)\",\"([^\"]*)\",\\[(.*)\\]}$");
			Match m = re.Match(value);
			if(m.Success) {
				item.ClassName = m.Groups[1].Value;
				item.ID = m.Groups[2].Value;
				string array = m.Groups[3].Value;
				int k = 0, start = 0;
				for(int i = 0; i < array.Length; i++) {
					char a = array[i];
					switch(a) {
						case '{':
							k++;
							break;
						case '}':
							k--;
							break;
						default:
							break;
					}
					if(k == 0) {
						string subStr = array.Substring(start, i - start + 1);
						ControlItem ctrl = subStr;
						item.Child.Add(ctrl);
						if(i < array.Length-2) {
							start = i + 2;
							i += 1;
						}
					}
				}
			}
			return item;
		}
	}
}
