using System;
using System.Web;

namespace BinHandler
{
	/// <summary>
	/// Summary description for BinHandler.
	/// </summary>
	public class BarCodeHandler : IHttpHandler
	{
		bool IHttpHandler.IsReusable 
		{
			get { return true; }
		}
		
		void IHttpHandler.ProcessRequest(HttpContext context)
		{
			HttpRequest Request = context.Request;
			HttpResponse Response = context.Response;

			string ID;
			if (Request["ID"] != null)
				ID = Request["ID"];
			else
				ID = "";
			System.Drawing.Color bgColor = System.Drawing.Color.White;
			if (Request["bgColor"] != null)
			{
				System.ComponentModel.TypeConverter converter = System.ComponentModel.TypeDescriptor.GetConverter(bgColor);
				bgColor = (System.Drawing.Color) converter.ConvertFromString(Request["bgColor"]);
			}
			int width;
			if (Request["width"] != null)
				width = int.Parse(Request["width"]);
			else
				width = 600;
			BarcodeLib.Barcode code = new BarcodeLib.Barcode { IncludeLabel = true, Height = 30, Width = width, BackColor = bgColor }; 
			code.Encode(BarcodeLib.TYPE.CODE93, ID);
			Response.AppendHeader("Content-Type", "image/png");
			byte[] image = code.GetImageData(BarcodeLib.SaveTypes.PNG);
			Response.OutputStream.Write(image, 0, image.Length);
			Response.OutputStream.Close();
		}
	}
}
