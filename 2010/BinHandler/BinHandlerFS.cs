﻿using System.Configuration;
using System.Data.SqlClient;

namespace BinHandler
{
    using System.Web;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Получает данные из Filestream поля
    /// </summary>
    public class BinHandlerFS : IHttpHandler
    {
        void IHttpHandler.ProcessRequest(HttpContext context)
        {
            var binHandlerType = (string)GetValue("binHandlerType");//тип используемого бин хэндлера

            var Request = context.Request;
            var Response = context.Response;

            var ID = "";
            if (Request["ID"] != null) ID = Request["ID"];
            else if (Rewrite.GetParam("ID", Request) != null) ID = Rewrite.GetParam("ID", Request);

            using (var conn = new SqlConnection((string)GetValue("dbdata.connection")))
            {
                var cmd = new SqlCommand("select dataStream, mimetype, fileName, data from t_BinaryData where OID = @ID", conn);
                cmd.Parameters.AddWithValue("@ID", ID);
                try
                {
                    conn.Open();
                    var reader = cmd.ExecuteReader();
                    try
                    {
                        if (reader.Read())
                        {
                            var mimeType = ((string)reader["mimetype"]).ToLower().Trim();
                            var fileName = ((string)reader["fileName"]).Trim();

                            if (mimeType.IndexOf("image") != -1)
                                Response.ContentType = mimeType;
                            else
                                if (mimeType.IndexOf("msword") != -1 || mimeType.IndexOf("ms-excel") != -1)
                            {
                                Response.ContentType = mimeType;
                                //Response.ContentEncoding = Encoding.UTF8; 
                                binHandlerType = "1";
                            }
                            else
                                    if (mimeType.IndexOf("pdf") != -1)
                            {
                                Response.ContentType = mimeType;
                                binHandlerType = "1";
                            }
                            else
                            {
                                Response.AppendHeader("Content-Type", "application/download");
                                binHandlerType = "1"; //неизвестные файлы полюбому предлагаем сохранять
                            }
                            if (binHandlerType == "1" || Request.Path.ToLower().IndexOf("binsave") != -1)
                            {
                                Response.AppendHeader("content-disposition", "attachment;filename=" + HttpUtility.UrlEncode(fileName));
                            }
                            Response.Cache.AppendCacheExtension("post-check=900,pre-check=3600");
                            Response.OutputStream.Write(reader.GetSqlBinary(0).Value, 0, reader.GetSqlBinary(0).Length);
                            Response.OutputStream.Close();
                        }
                    }
                    finally
                    {
                        reader.Close();
                    }
                }
                catch (Exception ex)
                {
                    Response.Write(ex.Message);
                }
                finally
                {
                    conn.Close();
                }
            }
        }

        bool IHttpHandler.IsReusable
        {
            get { return true; }
        }

        public static object GetValue(string name)
        {
            string str = ConfigurationManager.AppSettings[name];
            if (str == null)
            {
                return string.Empty;
            }
            return str;
        }
    }
}
