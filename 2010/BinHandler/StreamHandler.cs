using System;
using System.Web;
using System.Data.SqlClient;
using System.IO;

namespace BinHandler {

	public class StreamHandler : IHttpHandler 
    {
		bool IHttpHandler.IsReusable 
        {
			get {return true;}
		}
		
		void IHttpHandler.ProcessRequest(HttpContext context) 
        {
			HttpRequest Request = context.Request;
			HttpResponse Response = context.Response;
			string OID = Request.Params["OID"];
			using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["dbdata.connection"]))
			{
				SqlCommand cmd = new SqlCommand(String.Format("spGetObject '{0}'", OID), conn);
				string locationID = System.Configuration.ConfigurationManager.AppSettings["fileDir"];
				string fileName = "";
				string mimeType = "";
				int fileSize = 0;
				SqlDataReader dr;
				conn.Open();
				try
				{
					dr = cmd.ExecuteReader();
					if (dr.Read())
					{
						fileName = dr["fileName"].ToString();
						mimeType = dr["mimeType"].ToString();
						fileSize = (int)dr["fileSize"];
					}
				}
				finally
				{
					conn.Close();
				}
				string fullFileName = context.Server.MapPath(String.Format("{0}/{1}", locationID, fileName));
				FileInfo fi = new FileInfo(fullFileName);
				if (fi.Exists)
					Response.Redirect(String.Format("{0}/{1}", locationID, fileName));
				//				Response.ContentType = mimeType;
				//				Response.AddHeader("content-disposition", "attachment; filename="+HttpUtility.UrlEncode(fileName)+"; size="+fileSize.ToString());
				//				context.Response.WriteFile(fullFileName);
			}
		}
	}
}