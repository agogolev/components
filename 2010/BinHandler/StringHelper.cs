using System;
using System.Text.RegularExpressions;

namespace BinHandler
{
	/// <summary>
	/// Summary description for StringHelper.
	/// </summary>
	public static class StringHelper
	{
		public static string RemoveTags(this string source) 
		{
			Regex re = new Regex("<[^>]*>");
			return re.Replace(source, "");
		}
	}
}
