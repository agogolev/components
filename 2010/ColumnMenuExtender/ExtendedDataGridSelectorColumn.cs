﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.ComponentModel;
using System.Drawing.Design;

namespace ColumnMenuExtender
{
	public class ExtendedDataGridSelectorColumn : DataGridColumnStyle, IExtendedQuery
	{

		//public event GetColorForRow OnGetColorForRow;
		public event GetColorForRow OnGetForeColorForRow;
		public event FormatCellEventHandler SetCellFormat;

		// Methods
		public ExtendedDataGridSelectorColumn()
		{
			this.xMargin = 0;
			this.yMargin = 0;
			this.OldVal = new string(string.Empty.ToCharArray());
			this.InEdit = false;
			this.btnEllipsis = new Button();
			this.btnEllipsis.Visible = false;
			this.btnEllipsis.Text = "\x2026"; //"\x0324";
			this.btnEllipsis.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
			this.btnEllipsis.Size = new Size(20, 20);
			this.btnEllipsis.BackColor = System.Drawing.SystemColors.Control;
			this.btnEllipsis.Click += new EventHandler(btnEllipsis_Click);
		}

		protected override void Abort(int currentRow)
		{
			this.HideBtnEllipsis();
			this.EndEdit();
		}

		protected override bool Commit(CurrencyManager DataSource, int currentRow)
		{
			this.HideBtnEllipsis();
			if (this.InEdit)
			{
				try
				{

				}
				catch
				{
					return false;
				}
				this.EndEdit();
			}
			return true;
		}

		protected override void ConcedeFocus()
		{
			this.btnEllipsis.Visible = false;
		}

		protected override void Edit(CurrencyManager Source, int currentRow, Rectangle Bounds, bool ReadOnly, string InstantText, bool CellIsVisible)
		{
			//Rectangle rectangle1 = Bounds;
			this.currentSource = Source;
			this.currentRow = currentRow;
			if (this.ReadOnly)
			{
				this.btnEllipsis.Enabled = false;
			}
			else
			{
				this.btnEllipsis.Enabled = true;
			}


			if (CellIsVisible)
			{
				Bounds.Offset(this.xMargin, this.yMargin);
				Bounds.Width -= (this.xMargin * 2);
				Bounds.Height -= this.yMargin;
				this.btnEllipsis.Top = Bounds.Top;
				this.btnEllipsis.Left = Bounds.Right - this.btnEllipsis.Width;
				this.btnEllipsis.Height = Bounds.Height;
				this.btnEllipsis.Visible = true;
			}
			else
			{
				this.btnEllipsis.Top = Bounds.Top;
				this.btnEllipsis.Left = Bounds.Right - this.btnEllipsis.Width;
				this.btnEllipsis.Height = Bounds.Height;
				this.btnEllipsis.Visible = false;
			}

			if (this.btnEllipsis.Visible)
			{
				this.DataGridTableStyle.DataGrid.Invalidate(Bounds);
			}
			this.InEdit = true;
		}

		private void HideBtnEllipsis()
		{
			if (this.btnEllipsis.Focused)
			{
				this.DataGridTableStyle.DataGrid.Focus();
			}
			this.btnEllipsis.Visible = false;
		}
		public void EndEdit()
		{
			this.InEdit = false;
			this.Invalidate();
		}

		protected override int GetMinimumHeight()
		{
			return ((this.btnEllipsis.Height + this.yMargin) + 1);
		}

		protected override int GetPreferredHeight(Graphics g, object Value)
		{
			int num1 = 0;
			int num2 = 0;
			string text1 = this.GetText(Value);
			do
			{
				num1 = text1.IndexOf("r\n", (int)(num1 + 1));
				num2++;
			}
			while (num1 != -1);
			return ((base.FontHeight * num2) + this.yMargin);
		}

		protected override Size GetPreferredSize(Graphics g, object Value)
		{
			Size size1 = Size.Ceiling(g.MeasureString(this.GetText(Value), this.DataGridTableStyle.DataGrid.Font));
			size1.Width += ((this.xMargin * 2) + this.DataGridTableGridLineWidth);
			size1.Height += this.yMargin;
			return size1;
		}


		private string GetText(object Value)
		{
			if (Value == DBNull.Value)
			{
				return this.NullText;
			}
			if (Value != null)
			{
				return Value.ToString();
			}
			return string.Empty;

		}

		protected override void Paint(Graphics g, Rectangle Bounds, CurrencyManager Source, int currentRow)
		{
			this.Paint(g, Bounds, Source, currentRow, false);
		}

		protected override void Paint(Graphics g, Rectangle Bounds, CurrencyManager Source, int currentRow, bool AlignToRight)
		{
			Brush backBrush = new SolidBrush(this.DataGridTableStyle.BackColor);
			Brush foreBrush = new SolidBrush(this.DataGridTableStyle.ForeColor);
			DataGridFormatCellEventArgs e = new DataGridFormatCellEventArgs(this.GetColumnValueAtRow(Source, currentRow), Source.List[currentRow]);

			System.Drawing.Brush foreBr = foreBrush;

			bool isNewForeBrush = SetCellForeColor(currentRow, ref foreBr, Source);
			if (isNewForeBrush)
			{
				if (foreBrush != foreBr)
				{
					foreBrush.Dispose();
					foreBrush = foreBr;
				}
			}
			if (SetCellFormat != null)
			{
				SetCellFormat(this, e);
				if (e.ForeBrush != null)
				{
					foreBrush.Dispose();
					foreBrush = e.ForeBrush;
					isNewForeBrush = false;
				}
			}
			if (e.TextFont == null)
				e.TextFont = this.DataGridTableStyle.DataGrid.Font;
			string text1 = this.GetText(this.GetColumnValueAtRow(Source, currentRow));
			g.FillRectangle(backBrush, Bounds);
			g.DrawString(text1, e.TextFont, foreBrush, (RectangleF)new Rectangle(Bounds.X, Bounds.Y + 2, Bounds.Width, Bounds.Height));
		}

		protected override void Paint(Graphics g, Rectangle Bounds, CurrencyManager Source, int currentRow, Brush BackBrush, Brush ForeBrush, bool AlignToRight)
		{
			System.Drawing.Brush foreBr = ForeBrush;
			DataGridFormatCellEventArgs e = new DataGridFormatCellEventArgs(this.GetColumnValueAtRow(Source, currentRow), Source.List[currentRow]);

			bool isNewForeBrush = SetCellForeColor(currentRow, ref foreBr, Source);
			if (isNewForeBrush)
			{
				if (ForeBrush != foreBr)
				{
					ForeBrush = foreBr;
				}
			}
			if (SetCellFormat != null)
			{
				SetCellFormat(this, e);
			}
			if (e.TextFont == null)
				e.TextFont = this.DataGridTableStyle.DataGrid.Font;
			string text1 = this.GetText(this.GetColumnValueAtRow(Source, currentRow));
			g.FillRectangle(BackBrush, Bounds);
			g.DrawString(text1, e.TextFont, ForeBrush, (RectangleF)new Rectangle(Bounds.X, Bounds.Y + 2, Bounds.Width, Bounds.Height));
		}

		private bool SetCellForeColor(int currentRow, ref System.Drawing.Brush foreBrush, System.Windows.Forms.CurrencyManager source)
		{
			bool isNewBrush = false;

			if (OnGetForeColorForRow != null)
			{//был определён делегат для выбора цвета
				System.Drawing.Brush foreBr = OnGetForeColorForRow(currentRow, source);
				if (foreBr != null)
				{
					foreBrush = foreBr;
					isNewBrush = true;
				}
				else
				{
					foreBr = (this.DataGridTableStyle.DataGrid as ExtendedDataGrid).FireEventGetForeColorForRow(currentRow, source);
					if (foreBr != null)
					{
						foreBrush = foreBr;
						isNewBrush = true;
					}
				}
			}
			else
			{
				if ((this.DataGridTableStyle.DataGrid is ExtendedDataGrid))
				{
					System.Drawing.Brush foreBr = (this.DataGridTableStyle.DataGrid as ExtendedDataGrid).FireEventGetForeColorForRow(currentRow, source);
					if (foreBr != null)
					{
						foreBrush = foreBr;
						isNewBrush = true;
					}
				}
			}
			return isNewBrush;
		}


		protected override void SetDataGridInColumn(DataGrid Value)
		{
			base.SetDataGridInColumn(Value);
			if ((this.btnEllipsis.Parent != Value) && (this.btnEllipsis.Parent != null))
			{
				this.btnEllipsis.Parent.Controls.Remove(this.btnEllipsis);
			}
			if (Value != null)
			{
				Value.Controls.Add(this.btnEllipsis);
			}
		}

		// Properties
		private int DataGridTableGridLineWidth
		{
			get
			{
				if (this.DataGridTableStyle.GridLineStyle == DataGridLineStyle.Solid)
				{
					return 1;
				}
				return 0;
			}
		}


		// Fields
		private Button btnEllipsis;
		private bool InEdit;
		private string OldVal;
		private int xMargin;
		private int yMargin;

		private CurrencyManager currentSource;
		private int currentRow;

		public event CellEventHandler ButtonClick;

		private void btnEllipsis_Click(object sender, EventArgs e)
		{
			if (ButtonClick != null)
			{
				ButtonClick(this, new DataGridCellEventArgs(this.GetColumnValueAtRow(this.currentSource, this.currentRow), currentSource.List[this.currentRow]));
			}
		}

		#region Public Properties

		private string fieldName;
		private string filterFieldName;

		[DefaultValue(typeof(string), ""), Category("Misc")]
		public string FieldName
		{
			set
			{
				fieldName = value;
			}
			get
			{
				return fieldName;
			}
		}

		[DefaultValue(typeof(string), ""), Category("Misc")]
		public string FilterFieldName
		{
			set
			{
				filterFieldName = value;
			}
			get
			{
				return filterFieldName;
			}
		}
		#endregion
	}

	public delegate void CellEventHandler(object sender, DataGridCellEventArgs e);

	public class DataGridCellEventArgs : EventArgs
	{
		private object cellValue;
		private object currentRow;

		public DataGridCellEventArgs(object cellValue, object currentRow)
		{
			this.cellValue = cellValue;
			this.currentRow = currentRow;
		}

		//contains the current cell value
		public object CellValue
		{
			get { return cellValue; }
		}

		//contains the current row
		public object CurrentRow
		{
			get { return currentRow; }
		}

	}
}
