﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using System.ComponentModel.Design;
using System.Drawing.Design;
using System.Data;
using System.Drawing;
using System.Collections;
//using System.ComponentModel.Design;
//using System.Drawing.Design;
using System.Runtime.InteropServices;

namespace ColumnMenuExtender
{
	public class ExtendedDataGrid : DataGrid
	{
		private bool _allowCustomRowColorList = false;
		[DefaultValue(false)]
		public bool AllowCustomRowColorList
		{
			get
			{
				return _allowCustomRowColorList;
			}
			set
			{
				_allowCustomRowColorList = value;
			}
		}		
		public event GetColorForRow GetColorForRow;
		public event GetColorForRow GetForeColorForRow;
		public System.Drawing.Brush FireEventGetColorForRow(int rowNum, System.Windows.Forms.CurrencyManager source) {
			if(GetColorForRow != null) return GetColorForRow(rowNum, source);
			else return null;
		}
		public System.Drawing.Brush FireEventGetForeColorForRow(int rowNum, System.Windows.Forms.CurrencyManager source) {
			if(GetForeColorForRow != null) return GetForeColorForRow(rowNum, source);
			else return null;
		}
		private bool autoRowHeight = false;
		protected DataTable tblRowColors;
				
		[Browsable(false)]
		public DataTable RowColorsTable
		{
			get { return tblRowColors; }
		}

		[Browsable(true), DefaultValue(false)]
		public bool AutoRowHeight
		{
			get { return autoRowHeight; }
			set { autoRowHeight = value; }
		}

		public ExtendedDataGrid() : base()
		{			
			//////////////////////////////////////////////////////////////////////////
			//tblRowColors
			tblRowColors = new DataTable("RowColors");
			tblRowColors.Columns.Add("color",typeof(Color));	
			tblRowColors.Columns.Add("quantity",typeof(int));
			tblRowColors.Columns.Add("position",typeof(int));	
			tblRowColors.ExtendedProperties["currentRowPosition"] = 0;
			//////////////////////////////////////////////////////////////////////////
			
			// DnD funcs
			/////////////
			ResetMembersToDefault();
			this.SetStyle( ControlStyles.DoubleBuffer, true );
			
			this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ExtendedDataGrid_MouseDown);
			this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ExtendedDataGrid_MouseUp);
			this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.ExtendedDataGrid_MouseMove);
			/////////////			
		}

		public void DisableNewDel()
		{
			CurrencyManager cm;
			if(this.DataMember == "")
				cm = BindingContext[this.DataSource] as CurrencyManager;
			else 
				cm = BindingContext[this.DataSource, this.DataMember] as CurrencyManager;
			if (cm != null && (cm.List is DataView))
			{			
				DataView dview1 = (DataView)cm.List;
				dview1.AllowNew = false;
				dview1.AllowDelete = false;
			}
		}

		public void DisableNew()
		{
			CurrencyManager cm;
			if(this.DataMember == "")
				cm = BindingContext[this.DataSource] as CurrencyManager;
			else 
				cm = BindingContext[this.DataSource, this.DataMember] as CurrencyManager;
			DataView dview1 = (DataView)cm.List;
			dview1.AllowNew = false;
		}

		public void DisableDel()
		{
			CurrencyManager cm;
			if(this.DataMember == "")
				cm = BindingContext[this.DataSource] as CurrencyManager;
			else 
				cm = BindingContext[this.DataSource, this.DataMember] as CurrencyManager;
			DataView dview1 = (DataView)cm.List;
			dview1.AllowDelete = false;
		}

		/// <summary>
		/// возвращает ссылку на строку DataTable св¤занную с текущей выделенной строкой
		/// </summary>		
		public DataRow GetSelectedRow()
		{
			if(DataSource == null || DataMember == null)
				return null;
			CurrencyManager cm;
			if(this.DataMember == "")
				cm = BindingContext[this.DataSource] as CurrencyManager;
			else 
				cm = BindingContext[this.DataSource, this.DataMember] as CurrencyManager;
			
			if(cm != null && cm.Position != -1 && cm.Current is DataRowView)
			{
				DataRowView rv = (DataRowView)cm.Current;
				return rv.Row;
			}
			else
				return null;
		}

		public void SetSelectedRow(DataRow row)
		{
			CurrencyManager cm;
			if(DataSource == null || DataMember == null)
				return;
			if( this.DataMember == "")
				cm = (CurrencyManager) this.BindingContext[this.DataSource];
			else 
				cm = (CurrencyManager) this.BindingContext[this.DataSource, this.DataMember];
						
			for(int i = 0; i < cm.List.Count; i++)
			{
				DataRowView rv = (DataRowView) cm.List[i];
				if(rv.Row == row)
				{
					cm.Position = i;
					break;
				}
			}
		}

		/// <summary>
		/// возвращает ссылку на строку DataTable св¤занную с текущей выделенной строкой
		/// </summary>		
		public DataRowView GetSelectedRowView()
		{
			if(DataSource == null || DataMember == null)
				return null;
			CurrencyManager cm;
			if( this.DataMember == "")
				cm = (CurrencyManager) this.BindingContext[this.DataSource];
			else 
				cm = (CurrencyManager) this.BindingContext[this.DataSource, this.DataMember];

			if (cm != null && cm.Position != -1 && cm.Current is DataRowView)
			{
				DataRowView rv = (DataRowView)cm.Current;
				return rv;
			}
			else
				return null;
		}

		/// <summary>
		/// возвращает ссылку на строку DataTable св¤занную с текущей выделенной строкой
		/// </summary>		
		public object GetSelectedItem()
		{
			if (DataSource == null || DataMember == null)
				return null;
			CurrencyManager cm;
			if (this.DataMember == "")
				cm = (CurrencyManager)this.BindingContext[this.DataSource];
			else
				cm = (CurrencyManager)this.BindingContext[this.DataSource, this.DataMember];

			if (cm != null && cm.Position != -1)
			{
				return cm.Current;
			}
			else
				return null;
		}

		/// <summary>
		/// возвращает источник
		/// </summary>		
		public IList GetSourceList()
		{
			if(DataSource == null || DataMember == null)
				return null;
			CurrencyManager cm;
			if( this.DataMember == "")
				cm = (CurrencyManager) this.BindingContext[this.DataSource];
			else 
				cm = (CurrencyManager) this.BindingContext[this.DataSource, this.DataMember];
			return cm.List;
		}

		/// <summary>
		/// возвращает CurrencyManager
		/// </summary>		
		public CurrencyManager GetCurrencyManager()
		{
			if(DataSource == null || DataMember == null)
				return null;
			CurrencyManager cm;
			if( this.DataMember == "")
				cm = (CurrencyManager) this.BindingContext[this.DataSource];
			else 
				cm = (CurrencyManager) this.BindingContext[this.DataSource, this.DataMember];
			return cm;
		}

		/// <summary>
		/// прекращает редактирование текущей строки грида
		/// </summary>
		public void CommitEditRow(int rowNumber)
		{
			if(this.TableStyles["table"] != null && this.TableStyles["table"].ReadOnly == false && this.ReadOnly == false)
			{
				foreach(DataGridColumnStyle column in this.TableStyles["table"].GridColumnStyles)
				{
					if(column.ReadOnly == false)
						this.EndEdit(column,rowNumber,false);
				}
			}			
		}

		public void EndEdit()
		{
			if(CurrentRowIndex >= 0)
				CommitEditRow(CurrentRowIndex);

			if(DataSource == null || DataMember == null)
				return;
			CurrencyManager cm;
			if( this.DataMember == "")
				cm = (CurrencyManager) this.BindingContext[this.DataSource];
			else 
				cm = (CurrencyManager) this.BindingContext[this.DataSource, this.DataMember];
			if(cm != null)
				cm.EndCurrentEdit();
		}

		private void InitializeComponent()
		{
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}

		[Editor(typeof(TableStylesCollectionEditor), typeof(UITypeEditor))]
		public new GridTableStylesCollection TableStyles
		{
			get
			{
				return base.TableStyles;
			}
		}

		public class TableStylesCollectionEditor : CollectionEditor
		{
			// Methods
			public TableStylesCollectionEditor(Type type) : base (type)
			{
			}

			protected override Type[] CreateNewItemTypes()
			{
				return new Type[1] { typeof(ExtendedDataGridTableStyle) } ;
			}
		}

		//ƒл¤ автосайза колонок
		public void AutoSizeCol(int col)
		{ 
			float width = 0; 
			int numRows;

			if(this.DataSource is DataSet)
				numRows =((DataSet)this.DataSource).Tables[0].Rows.Count;
			else if(this.DataSource is DataTable)
				numRows =((DataTable)this.DataSource).Rows.Count;
			else if(this.DataSource is DataView) 
				numRows = ((DataView)this.DataSource).Count;
			else throw new Exception("Unknown DataSource: "+this.DataSource.ToString());
 
			Graphics g = Graphics.FromHwnd(this.Handle); 
			StringFormat sf = new StringFormat(StringFormat.GenericTypographic); 
			SizeF size;

			//размер хидера
			if(this.TableStyles.Count>0)
				width = g.MeasureString(this.TableStyles[0].GridColumnStyles[col].HeaderText, this.Font, 600, sf).Width;
 
			for(int i = 0; i < numRows; ++ i) 
			{
				//строки
				size = g.MeasureString(this[i, col].ToString(), this.Font, 600, sf);
				if(size.Width > width) 
					width = size.Width; 
			} 
 
			g.Dispose(); 
			this.TableStyles[0].GridColumnStyles[col].Width = (int) width + 15;//8 is for leading and trailing padding 
		}

		public void ShowColorForm()
		{
			fmDataGridISMColorDlg form = new fmDataGridISMColorDlg(tblRowColors);
			if(form.ShowDialog() == DialogResult.OK)
			{
				tblRowColors = form.RowColors;
				this.Refresh();
			}
		}

		//Drag'n'Drop (DnD) columns
		#region DnD override methods
		protected override void OnPaint(PaintEventArgs e)
		{
			base.OnPaint (e);			

			if ( m_draggedColumn != null ) 
			{
			
				Graphics g = e.Graphics;
		
				int x = m_draggedColumn.InitialRegion.X;
				int y = m_draggedColumn.InitialRegion.Y;
				int width = m_draggedColumn.InitialRegion.Width;
				int height = m_draggedColumn.InitialRegion.Height;
				Rectangle region = new Rectangle( x, y, width, height );

				SolidBrush blackBrush = new SolidBrush( Color.FromArgb( 255, 0, 0, 0 ) );
				SolidBrush darkGreyBrush = new SolidBrush( Color.FromArgb( 150, 50, 50, 50 ) );
				Pen blackPen = new Pen( blackBrush, 2F );

				g.FillRectangle( darkGreyBrush, m_draggedColumn.InitialRegion );
				g.DrawRectangle( blackPen, region );

				blackBrush.Dispose();
				darkGreyBrush.Dispose();
				blackPen.Dispose();

				// user feedback indicating which column the dragged column is over
				if ( this.m_mouseOverColumnIndex != -1 ) 
				{
				
					using ( SolidBrush b = new SolidBrush( Color.FromArgb( 100, 100, 100, 100 ) ) ) 
					{
						g.FillRectangle( b, m_mouseOverColumnRect );				
					}

				}
				
				// draw bitmap image
				if ( ShowColumnWhileDragging || ShowColumnHeaderWhileDragging ) 
				{ 

					Rectangle rect = new Rectangle( m_draggedColumn.CurrentRegion.X, 
						m_draggedColumn.CurrentRegion.Y, 
						m_draggedColumn.ColumnImage.Width, 
						m_draggedColumn.ColumnImage.Height );
							
					g.DrawImage(m_draggedColumn.ColumnImage, 
						rect, 
						0, 
						0, 
						m_draggedColumn.ColumnImage.Width, 
						m_draggedColumn.ColumnImage.Height, 
						GraphicsUnit.Pixel ); 
				
				}

				// translucent film
				Pen filmBorder = new Pen( new SolidBrush( Color.FromArgb( 255, 200, 200, 230 ) ), 2F );
				SolidBrush filmFill = new SolidBrush( Color.FromArgb( 100, 200, 200, 255 ) );

				g.FillRectangle(  filmFill, m_draggedColumn.CurrentRegion.X, m_draggedColumn.CurrentRegion.Y, m_draggedColumn.CurrentRegion.Width, m_draggedColumn.CurrentRegion.Height );				
				g.DrawRectangle( filmBorder, new Rectangle( m_draggedColumn.CurrentRegion.X, m_draggedColumn.CurrentRegion.Y + Convert.ToInt16( filmBorder.Width ), width, height ) );

				filmBorder.Dispose();
				filmFill.Dispose();

			}	
		}
			
		#endregion

		#region DnD Private fields
		bool m_showColumnWhileDragging = true;
		bool m_showColumnHeaderWhileDragging = true;

		DraggedDataGridColumn m_draggedColumn;
		Rectangle m_mouseOverColumnRect;		
		int m_mouseOverColumnIndex;

		bool columnDragEnabled = false;
		#endregion

		#region DnD Properties

		[DefaultValue(true)]
		public bool ShowColumnWhileDragging 
		{
			get { return m_showColumnWhileDragging; }
			set { m_showColumnWhileDragging = value; }
		}

		[DefaultValue(true)]
		public bool ShowColumnHeaderWhileDragging 
		{
			get { return m_showColumnHeaderWhileDragging; }
			set { m_showColumnHeaderWhileDragging = value; }
		}
		[DefaultValue(false)]
		public bool ColumnDragEnabled
		{
			get { return columnDragEnabled; }
			set { columnDragEnabled = value; }
		}
		#endregion

		#region DnD Event Handlers

		private void ExtendedDataGrid_MouseDown(object sender, MouseEventArgs e) 
		{			
			if(e.Button == MouseButtons.Left && columnDragEnabled)
			{
				DataGrid.HitTestInfo hti = this.HitTest( e.X, e.Y );
			
				// make sure that the user is hovering above a column header and not a column border.
				if ( ( hti.Type & DataGrid.HitTestType.ColumnHeader ) != 0 && ( ( hti.Type & DataGrid.HitTestType.ColumnResize ) == 0 ) && this.m_draggedColumn == null ) 
				{
					Console.WriteLine( hti.Type );

					int xCoordinate = this.GetLeftmostColumnHeaderXCoordinate( hti.Column );
					int yCoordinate = this.GetTopmostColumnHeaderYCoordinate( e.X, e.Y );
					int columnWidth = this.TableStyles[0].GridColumnStyles[hti.Column].Width;
					int columnHeight = this.GetColumnHeight( yCoordinate );

					Size columnSize = Size.Empty;

					// check to see if the column is partially hidden (to the right then to the left)
					if ( ( xCoordinate + columnWidth ) > ( this.ClientSize.Width - this.VertScrollBar.Width ) ) 
					{

						int num = columnWidth - ( this.ClientSize.Width - this.VertScrollBar.Width - xCoordinate ) + 2;
						this.MoveHorizScrollBar( num + this.HorizScrollBar.Value );
						xCoordinate -= num;

					} 
					else if ( this.TableStyles[0].RowHeadersVisible && xCoordinate < this.TableStyles[0].RowHeaderWidth ) 
					{
	
						Console.WriteLine( "condition matches" );
						this.MoveHorizScrollBar( - ( this.TableStyles[0].RowHeaderWidth - xCoordinate) );
						xCoordinate = this.TableStyles[0].GridColumnStyles[hti.Column].Width;
									
					} 
					else if ( xCoordinate < 0 ) 
					{
					
						int numberOfHiddenPixels = 0;
						for( int i=0; i < hti.Column; i++ ) 
						{
							numberOfHiddenPixels += this.TableStyles[0].GridColumnStyles[i].Width;
						}

						this.MoveHorizScrollBar( numberOfHiddenPixels );
						xCoordinate = 0;

					}

					Point startingLocation = new Point( xCoordinate, yCoordinate );
					Rectangle columnRegion = new Rectangle( xCoordinate, yCoordinate, columnWidth, columnHeight );
					Point cursorLocation = new Point( e.X - xCoordinate, e.Y - yCoordinate );
								
					if ( ShowColumnWhileDragging || ShowColumnHeaderWhileDragging ) 
					{

						if ( ShowColumnWhileDragging ) 
						{
							columnSize = new Size( columnWidth, columnHeight );
						} 
						else 
						{
							columnSize = new Size( columnWidth, this.GetColumnHeaderHeight( e.X, yCoordinate ) );
						}

						Bitmap columnImage = ( Bitmap ) ScreenImage.GetScreenshot( this.Handle, startingLocation, columnSize );
						m_draggedColumn = new DraggedDataGridColumn( hti.Column, columnRegion, cursorLocation, columnImage );
				
					} 
					else 
					{				
						m_draggedColumn = new DraggedDataGridColumn( hti.Column, columnRegion, cursorLocation );
					}
				
					m_draggedColumn.CurrentRegion = columnRegion;

				}

				// Force the grid to repaint.
				this.Update();
			}

		}

		private void ExtendedDataGrid_MouseUp(object sender, MouseEventArgs e) 
		{

			DataGrid.HitTestInfo hti = this.HitTest( e.X, e.Y );
																
			// is column being dropped above itself? if so, we don't want 
			// to do anything
			if ( m_draggedColumn != null && hti.Column != -1 && hti.Column != m_draggedColumn.Index ) 
			{
				DataGridTableStyle dgts = this.TableStyles[0];
				DataGridColumnStyle[] columns = new DataGridColumnStyle[dgts.GridColumnStyles.Count];
				
				int index;
				columns[hti.Column] = dgts.GridColumnStyles[m_draggedColumn.Index];
				// NOTE: csi = columnStyleIndex
				for ( int csi = index = 0; csi < dgts.GridColumnStyles.Count; csi++ ) 
				{										
					
					if(csi == m_draggedColumn.Index)
						continue;
					else if(columns[index] != null)
					{
						index++;
						columns[index] = dgts.GridColumnStyles[csi];
					}					
					else
						columns[index] = dgts.GridColumnStyles[csi];

					index++;					
				}
				this.SuspendLayout();
				this.TableStyles[0].GridColumnStyles.Clear();
				this.TableStyles[0].GridColumnStyles.AddRange( columns );
				this.ResumeLayout();

			} 
			else 
			{
				InvalidateColumnArea();
			}

			ResetMembersToDefault();
	
		}
						
		private void ExtendedDataGrid_MouseMove(object sender, MouseEventArgs e) 
		{
			
			DataGrid.HitTestInfo hti = this.HitTest( e.X, e.Y );

			if ( m_draggedColumn != null && e.X >= 0 ) 
			{

				int x = e.X - m_draggedColumn.CursorLocation.X;

				// detect the column that the cursor is currently hovering above and
				// calculate its region.
				if ( hti.Column >= 0 ) 
				{
				
					if ( hti.Column != m_mouseOverColumnIndex ) 
					{
						int mocX = this.GetLeftmostColumnHeaderXCoordinate( hti.Column );
						int mocWidth = this.TableStyles[0].GridColumnStyles[hti.Column].Width;
												
						// indicate that we want to invalidate the old rectangle area
						if ( m_mouseOverColumnRect != Rectangle.Empty ) 
						{
							this.Invalidate( m_mouseOverColumnRect, false );
						}

						// if the mouse is hovering over the original column, we do not want to
						// paint anything, so we negate the index.
						if ( hti.Column == m_draggedColumn.Index ) 
						{
							m_mouseOverColumnIndex = -1;
						} 
						else 
						{
							m_mouseOverColumnIndex = hti.Column;
						}

						m_mouseOverColumnRect = new Rectangle( mocX, m_draggedColumn.InitialRegion.Y, mocWidth, m_draggedColumn.InitialRegion.Height );

						// invalidate this area so it gets painted when OnPaint is called.
						this.Invalidate( m_mouseOverColumnRect, false );

					}
				
					int oldX = m_draggedColumn.CurrentRegion.X;
					Point oldPoint = Point.Empty;
					
					// column is being dragged to the right
					if ( oldX < x ) 
					{
						oldPoint = new Point( oldX - 5, m_draggedColumn.InitialRegion.Y );
						
						// to the left
					} 
					else 
					{
						oldPoint = new Point( x - 5, m_draggedColumn.InitialRegion.Y );
					} 

					Size sizeOfRectangleToInvalidate = new Size( Math.Abs( x - oldX ) + m_draggedColumn.InitialRegion.Width + ( oldPoint.X * 2 ), m_draggedColumn.InitialRegion.Height );
					this.Invalidate( new Rectangle( oldPoint, sizeOfRectangleToInvalidate ) );
										
					m_draggedColumn.CurrentRegion = new Rectangle( x, m_draggedColumn.InitialRegion.Y, m_draggedColumn.InitialRegion.Width, m_draggedColumn.InitialRegion.Height );
				
				} 
				else 
				{
						
					this.Invalidate();
					ResetMembersToDefault();
					this.Update();
									
				}
				
			} 
			else 
			{
		
				this.Invalidate( this.ClientRectangle );
				ResetMembersToDefault();
				this.Update();

			}

		}

		#endregion

		#region DnD Helper Methods

		private void SizeDataTableColumns( DataGridTableStyle dgts ) 
		{
			
			using ( Graphics g = CreateGraphics() ) 
			{ 

				DataTable dataTable = ((DataSet)this.DataSource).Tables[0];
				dgts.GridColumnStyles.Clear();
				
				foreach(DataColumn dataColumn in dataTable.Columns) 
				{

					int maxSize = 0;
					SizeF size = g.MeasureString( dataColumn.ColumnName, this.Font );

					if ( size.Width > maxSize ) 
					{ 
						maxSize = (int)size.Width;
					}

					foreach(DataRow row in dataTable.Rows) 
					{
						
						size = g.MeasureString( row[dataColumn.ColumnName].ToString(), this.Font );
						
						if( size.Width > maxSize ) 
						{ 
							maxSize = (int)size.Width;
						}

					}
						 
					DataGridColumnStyle dataGridColumnStyle = null;
					
					if(dataColumn.DataType == typeof(System.Int32))
						dataGridColumnStyle = new DataGridTextBoxColumn();

					if(dataColumn.DataType == typeof(System.String))
						dataGridColumnStyle = new DataGridTextBoxColumn();

					if(dataColumn.DataType == typeof(System.Boolean))
						dataGridColumnStyle = new DataGridBoolColumn();

					dataGridColumnStyle.MappingName = dataColumn.ColumnName;
					dataGridColumnStyle.HeaderText = dataColumn.ColumnName;
					dataGridColumnStyle.Width = maxSize + 5;
									
					dgts.GridColumnStyles.Add(dataGridColumnStyle);

				}

			}

		}
		

		/// <summary>
		/// When a dragged column is dropped on top of its original location, 
		/// whether itТs because the user has decided that they no longer want 
		/// to drag it, or theyТve just happened to release the column in this 
		/// location, we need to invalidate the area where the current drawings 
		/// reside.
		/// </summary>
		private void InvalidateColumnArea() 
		{
		
			if ( m_draggedColumn != null ) 
			{

				int startX = ( ( m_draggedColumn.InitialRegion.X < m_draggedColumn.CurrentRegion.X ) ? m_draggedColumn.InitialRegion.X : m_draggedColumn.CurrentRegion.X ) - 5;
				int width = m_draggedColumn.InitialRegion.Width + m_draggedColumn.CurrentRegion.Width + 10;
				Rectangle rectangleToInvalidate = new Rectangle( startX, m_draggedColumn.InitialRegion.Y, width, m_draggedColumn.InitialRegion.Height );
				
				this.Invalidate( rectangleToInvalidate );
				this.Update();
			}
			
		}

		/// <summary>
		/// Resets all of the member fields to their default values.
		/// </summary>
		private void ResetMembersToDefault() 
		{

			if ( m_draggedColumn != null ) 
			{
				m_draggedColumn.Dispose();
			}
			
			m_draggedColumn = null;
			m_mouseOverColumnRect = Rectangle.Empty;		
			m_mouseOverColumnIndex = -1;
		
		}

		/// <summary>
		/// Returns the height of the column. The height is defined as the area 
		/// between the bottom portion of the caption area and either the 
		/// bottom of the client rectangle, or the top of the horizontal scroll 
		/// bar if itТs visible.
		/// </summary>
		private int GetColumnHeight( int topmostYCoordinate ) 
		{
					
			int height = this.ClientSize.Height;
			
			if ( this.HorizScrollBar.Visible ) 
			{
				height -= this.HorizScrollBar.Height; 
			}
			
			return height - topmostYCoordinate;
			
		}
		
		/// <summary>
		/// Returns the height of the column header. In order to make this 
		/// calculation, you  need to pass in the topmost y-coordinate of the 
		/// header. This method will then invoke the 
		/// GetBottommostColumnHeaderYCoordinate, which is a recursive method 
		/// that is invoked repeatedly until the DataGrid hit test determines 
		/// that the current coordinates no longer lie within the boundaries 
		/// of a ColumnHeader.
		/// </summary>
		private int GetColumnHeaderHeight( int x, int y ) 
		{
			return GetBottommostColumnHeaderYCoordinate( x, y ) - y;			
		}

		/// <summary>
		/// Calculates the leftmost x coordinate for the column corresponding 
		/// to the parameterized column index. By accessing the GridColumnStyle 
		/// style Ц which is discussed in the article -- weТre able to get the 
		/// current column widths (this changes when you resize columns) for 
		/// the columns that precede the current column. 
		/// </summary>
		private int GetBottommostColumnHeaderYCoordinate( int x, int currentY ) 
		{

			DataGrid.HitTestInfo hti = this.HitTest( x, currentY );
			int yCoordinate = currentY;
			
			if ( hti.Type == DataGrid.HitTestType.ColumnHeader  ) 
			{
				yCoordinate = this.GetBottommostColumnHeaderYCoordinate( x, ++currentY );
			} 

			return yCoordinate;

		}

		/// <summary>
		/// Calculates the leftmost x coordinate for the column corresponding 
		/// to the parameterized column index. By accessing the 
		/// GridColumnStyle style, which is discussed in detail in the article,
		/// weТre able to get the current column widths (this changes when you 
		/// resize columns) for the columns that precede the current column. 		
		/// </summary>
		private int GetLeftmostColumnHeaderXCoordinate( int columnIndex ) 
		{
			
			int xCoordinate = ( this.RowHeadersVisible ) ? this.RowHeaderWidth : 0;

			for ( int i = 0; i < columnIndex; i++ ) 
			{
				DataGridColumnStyle dgcs = this.TableStyles[0].GridColumnStyles[i];
				xCoordinate += dgcs.Width;

			}			

			return xCoordinate - this.HorizScrollBar.Value + 2; // 2 represents the 1px solid this lines on both sides of the cell
		
		}

		/// <summary>
		/// This is another recursive method that returns the Y-coordinate of 
		/// the topmost portion of the column header. First, a check is 
		/// performed to see if the DataGrid caption is visible. If not, the 
		/// Y-coordinate is set to zero and the method returns. Otherwise, a 
		/// recursion is performed until the DataGrid hit test determines that 
		/// the current Y-coordinate value does not fall within the boundaries 
		/// of a ColumnHeader. 
		/// </summary>
		private int GetTopmostColumnHeaderYCoordinate( int currentX, int currentY ) 
		{
		
			DataGrid.HitTestInfo hti = this.HitTest( currentX, currentY );
			int yCoordinate = currentY;

			if ( !this.ColumnHeadersVisible ) 
			{
				yCoordinate = 0;
			} 
			else if ( hti.Type == DataGrid.HitTestType.ColumnHeader ) 
			{
				yCoordinate = this.GetTopmostColumnHeaderYCoordinate( currentX, --currentY );
			} 
			else 
			{
				yCoordinate++;
			}
				
			return yCoordinate;
		
		}

		/// <summary>
		/// Positions the horizontal scroll bar and invalidates the this.
		/// </summary>
		public void MoveHorizScrollBar( int amount ) 
		{

			this.GridHScrolled( this, new ScrollEventArgs( ScrollEventType.ThumbPosition, amount ) );
			this.Update();

		}
	
		#endregion
			
	}

	#region DnD Classes
	//класс позвол¤ющий сделать копию изображени¤ экрана
	public sealed class ScreenImage 
	{

		#region Unmanaged declarations

		[DllImport("gdi32.dll")]
		private static extern bool BitBlt( IntPtr handlerToDestinationDeviceContext, int x, int y, int nWidth, int nHeight, IntPtr handlerToSourceDeviceContext, int xSrc, int ySrc, int opCode);
		
		[DllImport("user32.dll")]
		private static extern IntPtr GetWindowDC( IntPtr windowHandle );
		
		[DllImport("user32.dll")]
		private static extern int ReleaseDC( IntPtr windowHandle, IntPtr dc );

		private static int SRCCOPY = 0x00CC0020;  // dest = source 
		
		#endregion

		/// <summary>
		/// делает копию экрана
		/// </summary>		
		public static Image GetScreenshot( IntPtr windowHandle, Point location, Size size ) 
		{
				
			Image myImage = new Bitmap( size.Width, size.Height );

			using ( Graphics g = Graphics.FromImage( myImage ) ) 
			{
		
				IntPtr destDeviceContext = g.GetHdc();
				IntPtr srcDeviceContext = GetWindowDC( windowHandle );
						
				// TODO: throw exception
				BitBlt( destDeviceContext, 0, 0, size.Width, size.Height, srcDeviceContext, location.X, location.Y, SRCCOPY );
		
				ReleaseDC( windowHandle, srcDeviceContext );
				g.ReleaseHdc( destDeviceContext );

			} // dispose the Graphics object

			return myImage;

		}
		
	} // ScreenImage
	
	internal class DraggedDataGridColumn : IDisposable 
	{

		#region Private data fields

		// private data fields
		private Rectangle m_initialRegion;
		private Rectangle m_currentRegion;

		private int m_index;
		private Image m_columnImage;
		private Point m_cursorLocation;
		
		private bool disposed = false;
		
		#endregion

		#region Properties
		
		/// <summary>
		/// An integer representing the original column index.
		/// </summary>
		public int Index 
		{
			get 
			{ 
				CheckState();
				return m_index; 
			}
		}

		/// <summary>
		/// A Rectangle structure that identifies the region of the column at
		/// the time the drag and drop operation was initiated.
		/// </summary>	
		public Rectangle InitialRegion 
		{
			
			get 
			{ 
				
				CheckState();
				return m_initialRegion; 
			
			}
		
		}

		/// <summary>
		/// A Rectangle structure that identifies the current region of the 
		/// column that is being dragged. This is the only member that can be 
		/// modified after an instance has been created.
		/// </summary>
		public Rectangle CurrentRegion 
		{

			get 
			{ 

				CheckState();
				return m_currentRegion; 
				
			}
			
			set 
			{ 
				
				CheckState();
				m_currentRegion = value; 
			
			}

		}

		/// <summary>
		/// A Bitmap object containing a bitmap representation of the column at 
		/// the time that the drag and drop operation was initiated.
		/// </summary>
		public Image ColumnImage 
		{
			
			get 
			{ 
			
				CheckState();
				return m_columnImage; 
				
			}
		
		}

		/// <summary>
		/// A Point structure representing the cursor location relative to the 
		/// origin of m_initialRegion.
		/// </summary>
		public Point CursorLocation 
		{
			
			get 
			{ 
				
				CheckState();
				return m_cursorLocation; 
				
			}
		
		}

		#endregion

		#region Constructors
		
		public DraggedDataGridColumn( int index, Rectangle initialRegion, Point cursorLocation, Image columnImage ) 
		{
			
			m_index = index;
			m_initialRegion = initialRegion;
			m_currentRegion = initialRegion;
			m_cursorLocation = cursorLocation;
			m_columnImage = columnImage;
				
		}

		public DraggedDataGridColumn( int index, Rectangle initialRegion, Point cursorLocation ) : this( index, initialRegion, cursorLocation, null ) {}

		#endregion

		public void Dispose() 
		{
			
			if ( !disposed ) 
			{
			
				m_initialRegion = Rectangle.Empty;
				m_currentRegion = Rectangle.Empty;

				m_index = -1;
				m_cursorLocation = Point.Empty;

				if ( m_columnImage != null ) 
				{
					m_columnImage.Dispose();
				}

				disposed = true;

			} 
					
			// Remove this object from the finalization queue so the 
			// finalizer doesn't invoke this method again.
			GC.SuppressFinalize( this );

		}

		// NOTE: We do not implement the destructor because we are not 
		// explicitly dealing with unmanaged resources.

		// ~DraggedDataGridColumn() { }
		
		/// <summary>
		/// Thow an ObjectDisposedException if this object has already been
		/// disposed.
		/// </summary>
		private void CheckState() 
		{
			
			if ( disposed ) 
			{
				throw new ObjectDisposedException( "DraggedDataGridColumn object has already been disposed." );
			}
		
		}
	
	} // DraggedDataGridColumn
	#endregion
}
