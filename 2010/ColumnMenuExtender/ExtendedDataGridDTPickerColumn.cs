﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Globalization;
using System.Threading;

namespace ColumnMenuExtender
{
	public class ExtendedDataGridDTPickerColumn : DataGridColumnStyle, IExtendedQuery
	{
		private NullableDateTimePicker customDateTimePicker1;

		// The InEdit field tracks whether or not the user is
		// editing data with the hosted control.
		private bool InEdit;

		public ExtendedDataGridDTPickerColumn()	: base()
		{
			customDateTimePicker1 = new NullableDateTimePicker();
			customDateTimePicker1.Value = DateTime.Now;
			customDateTimePicker1.Visible = false;
		}

		protected override void Abort(int rowNum)
		{
			InEdit = false;
			Invalidate();
		}

		protected override bool Commit(CurrencyManager dataSource, int rowNum)
		{
			HideDTPicker();

			if (!InEdit)
				return true;

			InEdit = false;

			try
			{
				object value = customDateTimePicker1.Value;
				if (!this.GetColumnValueAtRow(dataSource, rowNum).Equals(value))
					this.SetColumnValueAtRow(dataSource, rowNum, value);
			}
			catch
			{
				Abort(rowNum);
				return false;
			}
			EndEdit();
			return true;
		}

		protected override void ConcedeFocus()
		{
			HideDTPicker();
		}

		private void HideDTPicker()
		{
			if (customDateTimePicker1.Focused)
			{
				this.DataGridTableStyle.DataGrid.Focus();
			}
			customDateTimePicker1.Visible = false;
		}

		protected override void Edit(CurrencyManager source,	int rowNum,	Rectangle bounds,	bool readOnly, string displayText,	bool cellIsVisible)
		{
			DateTime? value = null;
			object obj = GetColumnValueAtRow(source, rowNum);
			if (obj != DBNull.Value && ((obj is DateTime) && ((DateTime)obj != DateTime.MinValue)))
			{
				value = (DateTime) obj;
			}

			customDateTimePicker1.Value = value ?? DateTime.MinValue;
			if (cellIsVisible)
			{
				customDateTimePicker1.Bounds = new Rectangle(bounds.X, bounds.Y, bounds.Width, bounds.Height);			
				customDateTimePicker1.Visible = true;
				if (!this.InEdit)
				{
					this.InEdit = true;
					base.ColumnStartedEditing(customDateTimePicker1);
				}
			}
			else
			{
				customDateTimePicker1.Visible = false;
			}

			if (customDateTimePicker1.Visible)
				DataGridTableStyle.DataGrid.Invalidate(bounds);

			customDateTimePicker1.Focus();
		}

		public void EndEdit()
		{
			this.InEdit = false;
			this.Invalidate();
		}

		protected override Size GetPreferredSize(Graphics g, object value)
		{
			return new Size(100, customDateTimePicker1.PreferredHeight + 4);
		}

		protected override int GetMinimumHeight()
		{
			return customDateTimePicker1.PreferredHeight;
		}

		protected override int GetPreferredHeight(Graphics g,	object value)
		{
			return customDateTimePicker1.PreferredHeight;
		}

		protected override void Paint(Graphics g,	Rectangle bounds,	CurrencyManager source,	int rowNum)
		{
			Paint(g, bounds, source, rowNum, false);
		}

		protected override void Paint(Graphics g, Rectangle bounds,	CurrencyManager source,	int rowNum,	bool alignToRight)
		{
			Paint(g, bounds, source, rowNum, Brushes.Red, Brushes.Blue, alignToRight);
		}

		protected override void Paint(Graphics g, Rectangle bounds, CurrencyManager source, int rowNum, Brush backBrush, Brush foreBrush, bool alignToRight)
		{
			DateTime date = DateTime.MinValue;
			bool allow = false;
			if (GetColumnValueAtRow(source, rowNum) != DBNull.Value)
			{
				date = (DateTime)GetColumnValueAtRow(source, rowNum);
				allow = true;
			}

			Rectangle rect = bounds;
			g.FillRectangle(backBrush, rect);
			rect.Offset(0, 2);
			rect.Height -= 2;
			//MinValue
			if (!allow)
			{
				g.DrawString("",	this.DataGridTableStyle.DataGrid.Font,	foreBrush, rect);
			}
			else
			{
				if (Format != null && Format != "")
				{
					g.DrawString(date.ToString(Format),	this.DataGridTableStyle.DataGrid.Font,	foreBrush, rect);
				}
				else
				{

					g.DrawString(date.ToString("d"), this.DataGridTableStyle.DataGrid.Font, foreBrush, rect);
				}
			}
		}

		protected override void SetDataGridInColumn(DataGrid value)
		{
			base.SetDataGridInColumn(value);
			if (customDateTimePicker1.Parent != null)
			{
				customDateTimePicker1.Parent.Controls.Remove
					(customDateTimePicker1);
			}
			if (value != null)
			{
				value.Controls.Add(customDateTimePicker1);
			}
		}

		//private void TimePickerValueChanged(object sender, EventArgs e)
		//{
		//  // Remove the handler to prevent it from being called twice in a row.
		//  customDateTimePicker1.ValueChanged -=
		//    new EventHandler(TimePickerValueChanged);
		//  if (!this.InEdit)
		//  {
		//    this.InEdit = true;
		//    base.ColumnStartedEditing(customDateTimePicker1);
		//  }
		//}

		#region Public Properties
		private string format;
		[DefaultValue(typeof(string), ""), Category("Misc")]
		public string Format
		{
			get { return format; }
			set
			{
				format = value;
				if (format != "")
				{
					customDateTimePicker1.Format = DateTimePickerFormat.Custom;
					customDateTimePicker1.CustomFormat = format;
				}
				else
				{
					customDateTimePicker1.Format = DateTimePickerFormat.Long;
				}
			}
		}

		private string fieldName;
		private string filterFieldName;

		[DefaultValue(typeof(string), ""), Category("Misc")]
		public string FieldName
		{
			set
			{
				fieldName = value;
			}
			get
			{
				return fieldName;
			}
		}

		[DefaultValue(typeof(string), ""), Category("Misc")]
		public string FilterFieldName
		{
			set
			{
				filterFieldName = value;
			}
			get
			{
				return filterFieldName;
			}
		}

		#endregion
	}

	public class CustomDateTimePicker : DateTimePicker
	{
		protected override bool ProcessKeyMessage(ref Message m)
		{
			// Keep all the keys for the DateTimePicker.
			return ProcessKeyEventArgs(ref m);
		}


	}

	/// <summary>
	/// Represents a Windows date time picker control. It enhances the .NET standard <b>DateTimePicker</b>
	/// control with the possibility to show empty values (null values).
	/// </summary>
	[ComVisible(false)]
	public class NullableDateTimePicker : System.Windows.Forms.DateTimePicker
	{
		// true, when no date shall be displayed (empty DateTimePicker)
		private bool _isNull;

		// If _isNull = true, this value is shown in the DTP
		private string _nullValue;

		// The format of the DateTimePicker control
		private DateTimePickerFormat _format = DateTimePickerFormat.Long;

		// The custom format of the DateTimePicker control
		private string _customFormat;

		// The format of the DateTimePicker control as string
		private string _formatAsString;

		/// <summary>
		/// Default Constructor
		/// </summary>
		public NullableDateTimePicker()
			: base()
		{
			base.Format = DateTimePickerFormat.Custom;
			NullValue = " ";
			Format = DateTimePickerFormat.Long;
			this.DataBindings.CollectionChanged += new CollectionChangeEventHandler(DataBindings_CollectionChanged);
		}


		/// <summary>
		/// Gets or sets the date/time value assigned to the control.
		/// </summary>
		/// <value>The DateTime value assigned to the control
		/// </value>
		/// <remarks>
		/// <p>If the <b>Value</b> property has not been changed in code or by the user, it is set
		/// to the current date and time (<see cref="DateTime.Now"/>).</p>
		/// <p>If <b>Value</b> is <b>null</b>, the DateTimePicker shows 
		/// <see cref="NullValue"/>.</p>
		/// </remarks>
		[Bindable(true)]
		[Browsable(false)]
		public new DateTime Value
		{
			get
			{
				if (_isNull)
					return DateTime.MinValue;
				else
					return base.Value;
			}
			set
			{
				if (value == DateTime.MinValue)
				{
					SetToNullValue();
				}
				else
				{
					SetToDateTimeValue();
					base.Value = value;
				}
			}
		}

		/// <summary>
		/// Gets or sets the format of the date and time displayed in the control.
		/// </summary>
		/// <value>One of the <see cref="DateTimePickerFormat"/> values. The default is 
		/// <see cref="DateTimePickerFormat.Long"/>.</value>
		[Browsable(true)]
		[DefaultValue(DateTimePickerFormat.Long), TypeConverter(typeof(Enum))]
		public new DateTimePickerFormat Format
		{
			get { return _format; }
			set
			{
				_format = value;
				if (!_isNull)
					SetFormat();
				OnFormatChanged(EventArgs.Empty);
			}
		}

		/// <summary>
		/// Gets or sets the custom date/time format string.
		/// <value>A string that represents the custom date/time format. The default is a null
		/// reference (<b>Nothing</b> in Visual Basic).</value>
		/// </summary>
		public new String CustomFormat
		{
			get { return _customFormat; }
			set { _customFormat = value; }
		}

		/// <summary>
		/// Gets or sets the string value that is assigned to the control as null value. 
		/// </summary>
		/// <value>The string value assigned to the control as null value.</value>
		/// <remarks>
		/// If the <see cref="Value"/> is <b>null</b>, <b>NullValue</b> is
		/// shown in the <b>DateTimePicker</b> control.
		/// </remarks>
		[Browsable(true)]
		[Category("Behavior")]
		[Description("The string used to display null values in the control")]
		[DefaultValue(" ")]
		public String NullValue
		{
			get { return _nullValue; }
			set { _nullValue = value; }
		}

		/// <summary>
		/// Stores the current format of the DateTimePicker as string. 
		/// </summary>
		private string FormatAsString
		{
			get { return _formatAsString; }
			set
			{
				_formatAsString = value;
				base.CustomFormat = value;
			}
		}

		/// <summary>
		/// Sets the format according to the current DateTimePickerFormat.
		/// </summary>
		private void SetFormat()
		{
			CultureInfo ci = Thread.CurrentThread.CurrentCulture;
			DateTimeFormatInfo dtf = ci.DateTimeFormat;
			switch (_format)
			{
				case DateTimePickerFormat.Long:
					FormatAsString = dtf.LongDatePattern;
					break;
				case DateTimePickerFormat.Short:
					FormatAsString = dtf.ShortDatePattern;
					break;
				case DateTimePickerFormat.Time:
					FormatAsString = dtf.ShortTimePattern;
					break;
				case DateTimePickerFormat.Custom:
					FormatAsString = this.CustomFormat;
					break;
			}
		}

		/// <summary>
		/// Sets the <b>DateTimePicker</b> to the value of the <see cref="NullValue"/> property.
		/// </summary>
		private void SetToNullValue()
		{
			_isNull = true;
			base.CustomFormat = string.IsNullOrWhiteSpace(_nullValue) ? "" : "'" + _nullValue + "'";
		}

		/// <summary>
		/// Sets the <b>DateTimePicker</b> back to a non null value.
		/// </summary>
		private void SetToDateTimeValue()
		{
			if (_isNull)
			{
				SetFormat();
				_isNull = false;
				base.OnValueChanged(new EventArgs());
			}
		}

		/// <summary>
		/// This member overrides <see cref="Control.WndProc"/>.
		/// </summary>
		/// <param name="m"></param>
		protected override void WndProc(ref Message m)
		{
			if (_isNull)
			{
				if (m.Msg == 0x4e)                         // WM_NOTIFY
				{
					NMHDR nm = (NMHDR)m.GetLParam(typeof(NMHDR));
					if (nm.Code == -746 || nm.Code == -722)  // DTN_CLOSEUP || DTN_?
						SetToDateTimeValue();
				}
			}
			base.WndProc(ref m);
		}

		[StructLayout(LayoutKind.Sequential)]
		private struct NMHDR
		{
			public IntPtr HwndFrom;
			public int IdFrom;
			public int Code;
		}

		/// <summary>
		/// This member overrides <see cref="Control.OnKeyDown"/>.
		/// </summary>
		/// <param name="e"></param>
		protected override void OnKeyUp(KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Delete)
			{
				this.Value = DateTime.MinValue;
				OnValueChanged(EventArgs.Empty);
			}
			base.OnKeyUp(e);
		}

		protected override void OnValueChanged(EventArgs eventargs)
		{
			base.OnValueChanged(eventargs);
		}
		private void DataBindings_CollectionChanged(object sender, CollectionChangeEventArgs e)
		{
			if (e.Action == CollectionChangeAction.Add)
				this.DataBindings[this.DataBindings.Count - 1].Parse +=
							 new ConvertEventHandler(NullableDateTimePicker_Parse);
		}

		private void NullableDateTimePicker_Parse(object sender, ConvertEventArgs e)
		{
			//saves null values to the object
			if (_isNull)
				e.Value = null;
		}
	}
}

