﻿using System;
using System.Windows.Forms;
using System.ComponentModel;
using System.Collections;

namespace ColumnMenuExtender
{
	/// <summary>
	/// Summary description for ExtendedDataGridObjectColumn.
	/// </summary>
	public class ExtendedDataGridObjectColumn : FormattableTextBoxColumn
	{
		private string className;
		private string classNameRus;
		public Hashtable Filters = new Hashtable();

		[DefaultValue(typeof(string), ""), Category("Misc")]
		public string ClassName
		{
			set 
			{
				className = value;
			}
			get
			{
				return className;
			}
		}

		[DefaultValue(typeof(string), ""), Category("Misc")]
		public string ClassNameRus
		{
			set 
			{
				classNameRus = value;
			}
			get
			{
				return classNameRus;
			}
		}
	}
}
