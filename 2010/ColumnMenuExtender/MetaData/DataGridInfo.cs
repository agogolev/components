﻿using System;
using System.Windows.Forms;

namespace ColumnMenuExtender.MetaData
{
	public class DataGridInfo
	{
		public bool UseGridMenu;
		public bool EventsWired;
	}
}