﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using MetaData;

namespace ColumnMenuExtender
{
	public class DataBoundTextBox : HintTextBox
	{	
		private bool textChanging = false;
		//private int efforts = 0; //количество попыток преобразования типа decimal
		//используется для того, чтобы при обычном заходе в контрол и выходе из него без изменения содержимого
		//не происходило обновление связанных с ним данных.
		public event EventHandler ValueChanged;
		protected virtual void OnValueChanged() {
			if(ValueChanged != null) ValueChanged(this, EventArgs.Empty);
		}
		private bool isCurrency = false;
		public bool IsCurrency
		{
			get { return isCurrency; }
			set { isCurrency = value; }
		}
		/// Устарело теперь используется св-во Format

		//private int precision = 2;
		//public int Precision
		//{
		//  get { return precision; }
		//  set { precision = value; }
		//}
		[DefaultValue(null)]
		public string Format { get; set; }
		private object _boundProp;
		public object BoundProp 
		{
			get 
			{
				return _boundProp;
			}
			set 
			{
				if(value == null) {
					_boundProp = null;
					return;
				}
				else {
					bool valueChange = false;
					if(value.GetType() == typeof(String)) {
						if(_boundProp == null || _boundProp.GetType() != typeof(string) || (string)_boundProp != (string)value)
							valueChange = true;
						base.Text = Format == null ? (string)value : string.Format(Format, value);
					}
					else if(value.GetType() == typeof(Guid)) {
						if(_boundProp == null || _boundProp.GetType() != typeof(Guid) || (Guid)_boundProp != (Guid)value)
							valueChange = true;
						Guid o = (Guid)value;
						if (o == Guid.Empty) 
							base.Text =  "";
						else 
							base.Text =  o.ToString();
					}
					else if(value.GetType() == typeof(ObjectItem)) {
						if(_boundProp == null || _boundProp.GetType() != typeof(ObjectItem) || ((ObjectItem)_boundProp).OID != ((ObjectItem)value).OID)
							valueChange = true;
						ObjectItem o = (ObjectItem)value;
						if (o.OID == Guid.Empty) 
							base.Text =  "";
						else
							base.Text = Format == null ? o.NK : string.Format(Format, o.NK);
					}
					else if(value.GetType() == typeof(int)) {
						if(_boundProp == null || _boundProp.GetType() != typeof(int) || (int)_boundProp != (int)value)
							valueChange = true;
						int o = (int)value;
						if (o == int.MinValue) 
							base.Text = "";
						else 
							base.Text = Format == null ? o.ToString() : string.Format(Format, o);
					}
					else if(value.GetType() == typeof(Double)) {
						if(_boundProp == null || _boundProp.GetType() != typeof(double) || (double)_boundProp != (double)value)
							valueChange = true;
						double o = (double)value;
						if(!textChanging) {
							if (o == double.MinValue) 
								base.Text = "";
							else
								base.Text = Format == null ? o.ToString() : string.Format(Format, o);
						}
					}
					else if(value.GetType() == typeof(decimal)) {
						if(_boundProp == null || _boundProp.GetType() != typeof(decimal) || (decimal)_boundProp != (decimal)value)
							valueChange = true;
						decimal o = (decimal)value;
						if(!textChanging) {
							if (o == decimal.MinValue) 
								base.Text = "";
							else if(isCurrency)
								base.Text = Math.Round(o,2).ToString();
							else
								base.Text = Format == null ? o.ToString() : string.Format(Format, o);
						}
					}
					else if(value.GetType() == typeof(float)) {
						if(_boundProp == null || _boundProp.GetType() != typeof(float) || (float)_boundProp != (float)value)
							valueChange = true;
						float o = (float)value;
						if(!textChanging) {
							if (o == float.MinValue) 
								base.Text = "";
							else
								base.Text = Format == null ? o.ToString() : string.Format(Format, o);
						}
					}
					else if(value.GetType() == typeof(Single)) {
						if(_boundProp == null || _boundProp.GetType() != typeof(float) || (float)_boundProp != (float)value)
							valueChange = true;
						Single o = (Single)value;
						if(!textChanging) {
							if (o == Single.MinValue) 
								base.Text = "";
							else
								base.Text = Format == null ? o.ToString() : string.Format(Format, o);
						}
					}
					else if(value.GetType() == typeof(DateTime)) {
						if(_boundProp == null || _boundProp.GetType() != typeof(DateTime) || (DateTime)_boundProp != (DateTime)value)
							valueChange = true;
						DateTime o = (DateTime)value;
						if (o == DateTime.MinValue) 
							base.Text = "";
						else
							base.Text = Format == null ? o.ToString() : string.Format(Format, o);
					}
					_boundProp = value;
					Refresh();
					if(valueChange) 
						OnValueChanged();				
				}
			}
		}

		/// <summary>
		/// Если изначально isCurrency=true и требуется в ручную изменить значение свойство объекта - используй это!
		/// </summary>
		public object BoundPropForCurrencyManualChanging
		{
			set
			{
				isCurrency = true;
				BoundProp = value;
			}
		}

		public DataBoundTextBox(): base()
		{}

		protected override void OnTextChanged(System.EventArgs e)
		{
			textChanging = true;
			try {
				if (_boundProp == null) {
					return;
				}
				if(BoundProp.GetType() == typeof(String)) {
					BoundProp = this.Text;
				}
				else if(BoundProp.GetType() == typeof(Guid)) {
					if (this.Text == "") _boundProp = Guid.Empty;
					else BoundProp = new Guid(this.Text);
				}
				else if(BoundProp.GetType() == typeof(Int32)) {
					if (this.Text == "") BoundProp = Int32.MinValue;
					else BoundProp = Convert.ToInt32(this.Text);
				}
				else if(BoundProp.GetType() == typeof(Double)) {
					if (this.Text == "") BoundProp = Double.MinValue;
					else BoundProp = Convert.ToDouble(this.Text);
				}
				else if(BoundProp.GetType() == typeof(Decimal)) {
					if (this.Text == "") BoundProp = Decimal.MinValue;
					else {
						string value = Text;
						string decimalSeparator = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;
						if(decimalSeparator == "," && value.IndexOf(".") != -1){
							value = value.Replace(".", ",");
						}
						else if(decimalSeparator == "." && value.IndexOf(",") != -1){
							value = value.Replace(",", ".");
						}
						BoundProp = Convert.ToDecimal(value); 
					}
				}
				else if(BoundProp.GetType() == typeof(DateTime)) {
					if (this.Text == "") BoundProp = DateTime.MinValue;
					else BoundProp = Convert.ToDateTime(this.Text);
				}
			}
			catch {
			}
			finally {
				textChanging = false;
			}
   
			base.OnTextChanged(e);
		}
	
	}
}

