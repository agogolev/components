﻿//----------------------------------------------------------------------------------
// - Author			   - Pham Minh Tri
// - Last Updated      - 19/Nov/2003
//----------------------------------------------------------------------------------
// - Component:        - Nullable DateTimePicker
// - Version:          - 1.0
// - Description:      - A datetimepicker that allow null value.
//----------------------------------------------------------------------------------
using System;
using System.Windows.Forms;  
using System.Globalization;  
using System.Threading;  

namespace ColumnMenuExtender
{
	/// <summary>
	/// Summary description for DataBoundDTPicker.
	/// </summary>
	public class DataBoundDTPicker : DateTimePicker
	{
		// true, when no date shall be displayed (empty DateTimePicker)
		private bool _isNull;
		private bool isOpened = false;
		public bool IsOpened {
			get {
				return this.isOpened;
			}
			set {
				this.isOpened = value;
			}
		}

		// The format of the DateTimePicker control
		private DateTimePickerFormat _format = DateTimePickerFormat.Long;

		// The custom format of the DateTimePicker control
		private string _customFormat;

		// The format of the DateTimePicker control as string
		private string _formatAsString;

		public new String CustomFormat {
			get { return _customFormat; }
			set {
				_customFormat = value;
			}
		}

		public new DateTimePickerFormat Format {
			get { return _format; }
			set {
				_format = value;
				SetFormat();
				OnFormatChanged(EventArgs.Empty);
			}
		}

		private void SetFormat() {
			CultureInfo ci = Thread.CurrentThread.CurrentCulture;
			DateTimeFormatInfo dtf = ci.DateTimeFormat;
			switch (_format) {
				case DateTimePickerFormat.Long:
					FormatAsString = dtf.LongDatePattern;
					break;
				case DateTimePickerFormat.Short:
					FormatAsString = dtf.ShortDatePattern;
					break;
				case DateTimePickerFormat.Time:
					FormatAsString = dtf.ShortTimePattern;
					break;
				case DateTimePickerFormat.Custom:
					FormatAsString = this.CustomFormat;
					break;
			}
		}
		private string FormatAsString {
			get { return _formatAsString; }
			set {
				_formatAsString = value;
				base.CustomFormat = value;
			}
		}

		public new DateTime Value {
			get {
				if (_isNull)
					return DateTime.MinValue;
				else
					return base.Value;
			}
			set {
				if(value != base.Value) {
					if (value == DateTime.MinValue) {
						SetToNullValue();
					}
					else {
						SetToDateTimeValue();
						base.Value = value;
					}
				}
			}
		}
		private void SetToDateTimeValue() {
			if (_isNull) {
				SetFormat();
				_isNull = false;
				base.OnValueChanged(new EventArgs());
			}
		}
		private void SetToNullValue() {
			_isNull = true;
			base.CustomFormat = " ";
			base.OnValueChanged(new EventArgs());
			Refresh();
		}

		protected override void OnDropDown(EventArgs eventargs) {
			IsOpened = true;
			base.OnDropDown (eventargs);
		}

		protected override void OnCloseUp(EventArgs e) {
			if (Control.MouseButtons == MouseButtons.None && _isNull) {
				SetToDateTimeValue();
				_isNull = false;
			}
			IsOpened = false;
			base.OnCloseUp (e);
		}
		protected override void OnKeyUp(KeyEventArgs e) {
			if (e.KeyCode == Keys.Delete) {
				this.Value = DateTime.MinValue;
				OnValueChanged(EventArgs.Empty);
			}
			base.OnKeyUp(e);
		}
		public DataBoundDTPicker() : base() {
			base.Format = DateTimePickerFormat.Custom;
			this.Format = DateTimePickerFormat.Long;
		}
	}
}
