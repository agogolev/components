﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using System.Xml;

namespace ColumnMenuExtender
{
	public enum SortType {NoSort=0, Alphabet=1, Popularity=2}

	public class HintTextBox : System.Windows.Forms.TextBox
	{
		private bool fText = true;
		private bool needSaveHint = false;
		private bool isHinted = false;
		private ArrayList data;
		private fmHint listBox;
		private string oldText = "";
		private int minLettersForHint = 3;
		private int maxHintCount = 100;

		private SortType sortType = SortType.Alphabet;

		[
		DefaultValue(false)
		]
		public bool IsHinted
		{
			get{ return isHinted; }
			set{ isHinted = value; }
		}
		//Максимально кол-во запоминаемых значений для подсказок
		[DefaultValue(100)]
		public int MaxHintCount
		{
			get{ return maxHintCount; }
			set{ maxHintCount = value; }
		}
		//Минимальное кол-во букв для отображения подподподсказки
		[DefaultValue(3)]
		public int MinLettersForHint
		{
			get { return minLettersForHint; }
			set { minLettersForHint = value;}
		}
		//Высота подсказки
		[DefaultValue(13)]
		public int ItemHeight
		{
			get { return listBox.ItemHeight; }
			set { listBox.ItemHeight = value; }
		}
		[DefaultValue(4)]
		public int HeightInHints
		{
			get { return listBox.HeightInHints; }
			set { listBox.HeightInHints = value; }
		}
		[DefaultValue(SortType.Alphabet)]
		public SortType Sort
		{
			get { return sortType; }
			set
			{
				sortType = value;
				sort();
			}
		}
		public Font HintFont {
			get { 
				return listBox.Font; 
			}
			set {
				listBox.Font = value;
			}
		}

		public void SaveDataXml(XmlNode node)
		{
			XmlDocument xmlDoc = node.OwnerDocument;
			bool newItem = true;

			for( int i=0; i<data.Count; i++ )
			{
				ListItem item = (ListItem)data[i];
				XmlNode newNode = xmlDoc.CreateNode(XmlNodeType.Element, "data", "");
				newNode.InnerText = item.Data;
				if(item.Data == this.Text)
				{
					newItem = false;
					item.Index++;
				}
				XmlAttribute attr = xmlDoc.CreateAttribute("index");
				attr.Value = item.Index.ToString();
				newNode.Attributes.Append(attr);
				node.AppendChild(newNode);
			}
			if( needSaveHint && newItem && this.Text != "" )
			{
				XmlNode newNode = xmlDoc.CreateNode(XmlNodeType.Element, "data", "");
				newNode.InnerText = this.Text;
				XmlAttribute attr = xmlDoc.CreateAttribute("index");
				attr.Value = 1.ToString();
				newNode.Attributes.Append(attr);
				node.AppendChild(newNode);
			}
		}

		public void LoadDataXml(XmlNode node)
		{
			XmlNodeList nl = node.SelectNodes("data");
			ListItem minItem = null;
			int count = 0;
			data.Clear();

			if (nl != null)
			{
				foreach(XmlNode dNode in nl)
				{
					ListItem item = ListItem.ReadXmlNode(dNode);
					data.Add(item);
					count++;
					if(minItem == null || minItem.Index>item.Index)
						minItem = item;
				}
				if (count > MaxHintCount)
					data.Remove(minItem);
			}
			sort();
		}


		public HintTextBox(): base()
		{
			listBox = new fmHint();
			data = new ArrayList();
			listBox.ItemSelect += new ItemSelectEventHandler(listBox_ItemSelect);
		}

		protected override void Dispose(bool disposing)
		{
			if( disposing )
			{
				data.Clear();
				listBox.Dispose();
			}
			base.Dispose (disposing);
		}


		private void listBox_ItemSelect(object sender, ItemSelectEventArgs e)
		{
			fText = false;
			this.Text = ((ListItem)e.SelectedItem).Data;
			this.SelectionStart = this.TextLength;
			oldText = "";
			fText = true;
		}

		protected override void OnTextChanged(System.EventArgs e)
		{
			if (fText)
			{
				#region Блок отвечающий за подсказки
				try
				{
					int len = this.Text.Length;
					string low = this.Text.ToLower();
					if (this.Text.Length >= minLettersForHint)
					{
						if (oldText == this.Text && !listBox.Visible)
						{
							showHint();
						}
						else if (oldText != "" && len >= oldText.Length && oldText == this.Text.Substring(0, oldText.Length))
						{
							int i = 0;
							while (i < listBox.Items.Count)
							{
								ListItem item = (ListItem)listBox.Items[i];
								if (item.Data.Length < len)
									listBox.Items.Remove(item);
								else if (item.Data.Substring(0, len).ToLower() != low)
									listBox.Items.Remove(item);
								else i++;
							}
						}
						else
						{
							listBox.Items.Clear();
							foreach (ListItem item in data)
							{
								if (item.Data.Length >= len && item.Data.Substring(0, len).ToLower() == low)
									listBox.Items.Add(item);
							}
						}
						oldText = this.Text;
						if (listBox.Items.Count > 0)
							showHint();
						else listBox.Hide();
					}
					else listBox.Visible = false;
				}
				catch (Exception ex)
				{
					MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
				}
				#endregion
			}
			base.OnTextChanged(e);
		}
	
		protected override void OnLeave(System.EventArgs e)
		{
			if(listBox.Visible)
				listBox.Hide();
			base.OnLeave(e);
		}

		protected override void OnKeyDown(System.Windows.Forms.KeyEventArgs e)
		{
			needSaveHint = true;
			if (listBox.Visible)
			{
				switch(e.KeyData)
				{
					case Keys.Up:
					case Keys.Down:
						listBox.SendKey(e.KeyData);
						e.Handled = true;
						break;
					case Keys.Enter:
						if (listBox.SelectedIndex != -1)
						{
							listBox.SendKey(e.KeyData);
							e.Handled = true;
						}
						break;
					case Keys.Escape:
						listBox.Hide();
						break;
				}
			}
			base.OnKeyDown(e);
		}

		private void sort()
		{
			IComparer myComp;
			switch(sortType)
			{
				case SortType.NoSort:
					return;
				case SortType.Alphabet:
					myComp = new CompareAlphabet();
					data.Sort(myComp);
					break;
				case SortType.Popularity:
					myComp = new ComparePopularity();
					data.Sort(myComp);
					break;
			}
		}

		private void showHint()
		{
			if (this.Focused)
			{
				int selStart = this.SelectionStart;
				int selLen = this.SelectionLength;
				Point loc = this.Location;
				Point sl = this.PointToScreen(this.Location);
				loc.X = sl.X - loc.X;
				loc.Y = sl.Y - loc.Y + this.Size.Height;
				listBox.Location = loc;
				listBox.Width = this.Width;
				listBox.Show();
				this.SelectionStart = selStart;
				this.SelectionLength = selLen;
				this.Focus();
			}
		}

		class ListItem
		{
			public string Data;
			public int Index;

			ListItem(string data, int index)
			{
				Data = data;
				Index = index;
			}

			public static ListItem ReadXmlNode(XmlNode node)
			{
				int index = Convert.ToInt32(node.Attributes["index"].Value);
				string data = node.InnerText;
				return new ListItem(data, index);
			}

			public override string ToString()
			{
				return Data;
			}
		}


		class CompareAlphabet:IComparer
		{
			int IComparer.Compare(object x, object y)
			{
				int z = x.ToString().CompareTo(y.ToString());
				return z;
			}
		}

		class ComparePopularity:IComparer
		{
			int IComparer.Compare(object x, object y)
			{
				return ((ListItem)y).Index.CompareTo(((ListItem)x).Index);
			}
		}
	}
}

