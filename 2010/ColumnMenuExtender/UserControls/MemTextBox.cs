﻿using System;
using System.Xml;
using System.Text;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace ColumnMenuExtender.UserControls
{
	/// <summary>
	/// Summary description for MemTextBox.
	/// </summary>
	public class MemTextBox : System.Windows.Forms.TextBox
	{
		private bool saveContent;
		[DefaultValue(false)]
		public bool SaveContent {
			get {
				return this.saveContent;
			}
			set {
				this.saveContent = value;
			}
		}
		public void LoadDataXml(XmlNode node) {
			if(SaveContent) {
				XmlNode n = node.SelectSingleNode("Text");
				if(n != null) this.Text = n.InnerText;
			}
		}
		public void SaveDataXml(XmlNode node) {
			XmlDocument xmlDoc = node.OwnerDocument;
			XmlNode n = node.SelectSingleNode("Text");
			if(n == null) n = node.AppendChild(xmlDoc.CreateElement("Text"));
			n.InnerText = this.Text;
		}
	}
}
