﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using ColumnMenuExtender.Forms.Dialogs;
using MetaData;

namespace ColumnMenuExtender.UserControls
{
	/// <summary>
	/// Summary description for BaseSelectorEdtTextBox.
	/// </summary>
	public class BaseSelectorEdtTextBox : System.Windows.Forms.UserControl
	{
		protected virtual BaseListDialog GetBaseListDialog(string className, ArrayList fieldNames, string[] columnNames, string[] columnHeaderNames, 
			string[] formatRows, OrderInfo Order, Hashtable Filters, int[] widths)
		{
			return null;
		}
		protected virtual BaseListDialog GetBaseListDialog(string className, string[] columnNames, string[] columnHeaderNames, string[] formatRows, 
			OrderInfo Order, Hashtable Filters, int[] widths)
		{
			return null;
		}

		private string _autoTextColumn;
		private System.Windows.Forms.Button btnChoose;
		private int x1;
		//private Form currentForm;
		private string className;
		private string columnNames;
		private string fieldNames;
		private string headerNames;
		private string formatRows;
		private ObjectItem _boundProp;
		private System.Windows.Forms.Button btnDel;
		private HintTextBox tbText;

		public OrderInfo Order = null;
		public Hashtable Filters = new Hashtable();

		public string SelectedNK;
		public string[] SelectedNKs;

		public int[] Widths;
		public int NKNumber = -1;

		public string FilterString = "";
		private System.Windows.Forms.ImageList imageList1;
		private System.Windows.Forms.Button btnEdit;
		private System.ComponentModel.IContainer components;

		public BaseSelectorEdtTextBox()
		{
			InitializeComponent();

			x1 = this.Width - tbText.Width;
			className = "";

			//if (autoTextColumn == "")
			//	this.tbText.ReadOnly = true;
		}

		/*public void BindForm(Form form)
		{
			currentForm = form;
		}*/

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(BaseSelectorEdtTextBox));
			this.btnChoose = new System.Windows.Forms.Button();
			this.btnDel = new System.Windows.Forms.Button();
			this.tbText = new HintTextBox();
			this.btnEdit = new System.Windows.Forms.Button();
			this.imageList1 = new System.Windows.Forms.ImageList(this.components);
			this.SuspendLayout();
			// 
			// btnChoose
			// 
			this.btnChoose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnChoose.Location = new System.Drawing.Point(200, 0);
			this.btnChoose.Name = "btnChoose";
			this.btnChoose.Size = new System.Drawing.Size(24, 24);
			this.btnChoose.TabIndex = 1;
			this.btnChoose.Text = "...";
			this.btnChoose.Click += new System.EventHandler(this.btnChoose_Click);
			// 
			// btnDel
			// 
			this.btnDel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnDel.Location = new System.Drawing.Point(224, 0);
			this.btnDel.Name = "btnDel";
			this.btnDel.Size = new System.Drawing.Size(24, 24);
			this.btnDel.TabIndex = 2;
			this.btnDel.Text = "X";
			this.btnDel.Click += new System.EventHandler(this.btnDel_Click);
			// 
			// tbText
			// 
			this.tbText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.tbText.HintFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(204)));
			this.tbText.Location = new System.Drawing.Point(0, 0);
			this.tbText.Name = "tbText";
			this.tbText.ReadOnly = true;
			this.tbText.Size = new System.Drawing.Size(176, 20);
			this.tbText.TabIndex = 3;
			this.tbText.TabStop = false;
			this.tbText.Text = "";
			this.tbText.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbText_KeyDown);
			// 
			// btnEdit
			// 
			this.btnEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnEdit.ForeColor = System.Drawing.SystemColors.ControlText;
			this.btnEdit.ImageAlign = System.Drawing.ContentAlignment.BottomRight;
			this.btnEdit.ImageIndex = 0;
			this.btnEdit.ImageList = this.imageList1;
			this.btnEdit.Location = new System.Drawing.Point(176, 0);
			this.btnEdit.Name = "btnEdit";
			this.btnEdit.Size = new System.Drawing.Size(24, 24);
			this.btnEdit.TabIndex = 3;
			this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
			// 
			// imageList1
			// 
			this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
			this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
			this.imageList1.TransparentColor = System.Drawing.Color.Magenta;
			// 
			// BaseSelectorEdtTextBox
			// 
			this.Controls.Add(this.tbText);
			this.Controls.Add(this.btnDel);
			this.Controls.Add(this.btnChoose);
			this.Controls.Add(this.btnEdit);
			this.Name = "BaseSelectorEdtTextBox";
			this.Size = new System.Drawing.Size(248, 24);
			this.Resize += new System.EventHandler(this.SelectorTextBox_Resize);
			this.ResumeLayout(false);

		}
		#endregion

		private void SelectorTextBox_Resize(object sender, System.EventArgs e)
		{
			tbText.Width = this.Width - x1;
		}

		private string multi_classContainer = "";
		private string multi_propName = "";
		private Guid[] multi_OIDs = null;

		private string listFormName = "";

		public void SetMultiFilter(Guid OID, string classContainer, string propName)
		{
			multi_OIDs = new Guid[] {OID};
			multi_classContainer = classContainer;
			multi_propName = propName;
		}

		public void SetMultiFilter(Guid[] OIDs, string classContainer, string propName)
		{
			multi_OIDs = new Guid[OIDs.Length];
			for(int i = 0; i < OIDs.Length; i++)
				multi_OIDs[i] = OIDs[i];
			multi_classContainer = classContainer;
			multi_propName = propName;
		}

		protected virtual void btnChoose_Click(object sender, System.EventArgs e)
		{
			string[] columnNames;
			string[] columnHeaderNames;	
			string[] formatRows;
			ArrayList fieldNames = new ArrayList();

			if (this.RowNames != null)
				columnNames = this.RowNames.Split(',');
			else
				columnNames = null;
			if (this.HeaderNames != null)
				columnHeaderNames = this.HeaderNames.Split(',');
			else
				columnHeaderNames = null;
			if (this.FormatRows != null)
				formatRows = this.FormatRows.Split(',');
			else
				formatRows = null;

			if(this.FieldNames != null && this.FieldNames != "")
			{
				string[] fields = this.FieldNames.Split(',');
			
				//могла разделиться функция, типа func(a,b,c)
				//объединяем ее опять
				for(int i = 0; i < fields.Length;)
				{
					string field = fields[i];
					if(fields[i].IndexOf('(') != -1)
						while(fields[i].IndexOf(')') == -1)
						{
							i++;
							field += ","+fields[i];
						}
					fieldNames.Add(field);
					i++;
				}
			}
			else
				fieldNames = null;

			BaseListDialog fm;

			Boolean autoSelect = false;
			if (AutoTextColumn != "" && tbText.Text != "" && !tbText.ReadOnly)
			{
				StringFilterInfo _filter = new StringFilterInfo();
				_filter.ColumnName = AutoTextColumn;
				_filter.Values = new string[1];
				_filter.Values[0] = tbText.Text+"*";
				_filter.Verb = FilterVerb.Like;
				Filters[AutoTextColumn] = _filter;
				autoSelect = true;
			}
			else
			{
				Filters.Remove(AutoTextColumn);
			}

			if(fieldNames != null)
			{
				fm = GetBaseListDialog(this.ClassName, fieldNames, columnNames, columnHeaderNames, formatRows, Order, Filters, Widths);
			}
			else 
				fm = GetBaseListDialog(this.ClassName, columnNames, columnHeaderNames, formatRows, Order, Filters, Widths);

			fm.FilterString = this.FilterString;

			//if (AutoTextColumn != "" && tbText.Text != "")
			//	fm.WindowState = FormWindowState.Minimized;

			if (multi_OIDs != null)
			{
				if(multi_OIDs.Length ==1)
					fm.SetMultiFilter(multi_OIDs[0], multi_classContainer, multi_propName);
				else fm.SetMultiFilter(multi_OIDs, multi_classContainer, multi_propName);
			}
			fm.Text = listFormName;

			if (fm.ShowDialog(this, autoSelect) == DialogResult.OK)
			{
				Guid o = fm.SelectedOID;
				SelectedNK = fm.SelectedNK;
				SelectedNKs = fm.SelectedNKs;
				if(NKNumber == -1)
					BoundProp = new ObjectItem(o, SelectedNK, ClassName);
				else BoundProp = new ObjectItem(o, SelectedNKs[NKNumber], ClassName);
				OnChanged();
			}
		}

		private void btnDel_Click(object sender, System.EventArgs e)
		{
			BoundProp = new ObjectItem(Guid.Empty, "", "");
			OnChanged();
			if (AutoTextColumn != "")
			{
				tbText.ReadOnly = false;
				Filters.Remove(AutoTextColumn);
			}
		}

		#region Public Properties
		[DefaultValue(BorderStyle.Fixed3D),
		Category("Appearance")]
		public BorderStyle TextBoxBorderStyle
		{
			set	{tbText.BorderStyle = value;}
			get	{return tbText.BorderStyle;}
		}

		[DefaultValue(FlatStyle.Standard),
		Category("Appearance")]
		public FlatStyle ButtonFlatStyle
		{
			set 
			{
				btnChoose.FlatStyle = value;
				btnDel.FlatStyle = value;}
			get	{return btnChoose.FlatStyle;}
		}

		[Category("Appearance")]
		public new Color BackColor {
			get	{
				return tbText.BackColor;
			}
			set {
				tbText.BackColor = value;
			}
		}

		[Category("Appearance")]
		public Color ButtonBackColor
		{
			set 
			{
				btnChoose.BackColor = value;
				btnDel.BackColor = value;}
			get	{return btnChoose.BackColor;}
		}

		/*[//DefaultValue(System.Drawing.SystemColors.Control),
		Category("Appearance")]
		public Color TextBoxBackColor
		{
			set {tbText.BackColor = value;}
			get	{return tbText.BackColor;}
		}*/

		[DefaultValue(""), Description("Имя класса")]
		public string ClassName
		{
			set {className = value;}
			get	{return className;}
		}
		
		[DefaultValue(""), Description("Имена желаемых полей. Старый вариант, без поддержки функций")]
		public string RowNames
		{
			set {columnNames = value;}
			get	{return columnNames;}
		}

		[DefaultValue(""), Description("Имена желаемых полей. Новый вариант, с поддержкой функций")]
		public string FieldNames
		{
			set {fieldNames = value;}
			get	{return fieldNames;}
		}

		[DefaultValue(""), Description("Имена заголовков")]
		public string HeaderNames
		{
			set {headerNames = value;}
			get	{return headerNames;}
		}

		[DefaultValue(""), Description("Форматы колонок (типа d или 0.00)")]
		public string FormatRows
		{
			set {formatRows = value;}
			get	{return formatRows;}
		}	

		[Browsable(false)]
		public ObjectItem BoundProp
		{
			get {return _boundProp;}
			set
			{
				_boundProp = value;
				if (_boundProp != null)
				{
					tbText.Text = _boundProp.NK;
					if(AutoTextColumn != "")
						tbText.ReadOnly = _boundProp.OID==Guid.Empty?false:true;
				}
				else
				{
					tbText.Text = "";
					if(AutoTextColumn != "")
						tbText.ReadOnly = false;
				}
			}
		}

		[DefaultValue(""), Description("Имя листовой формы (form.Text)")]
		public string ListFormName
		{
			get { return listFormName; }
			set { listFormName = value; }
		}

		[DefaultValue(""), Description("Имя поля, по которому будет производиться поиск (появляется возможность набора руками искомого значения)")]
		public string AutoTextColumn
		{
			get
			{
				if (_autoTextColumn == null) return ""; 
				else return _autoTextColumn;
			}
			set 
			{
				_autoTextColumn = value;
				if (value == null || value == "")
					tbText.ReadOnly = true;
				else tbText.ReadOnly = false;
			}
		}

		[DefaultValue(false), Description("Включить/выключить поддержку хинтов")]
		public bool IsHinted
		{
			get { return tbText.IsHinted; }
			set { tbText.IsHinted = value; }
		}

		[DefaultValue(100), Description("Максимальное количество хинтов")]
		public int MaxHintCount
		{
			get { return tbText.MaxHintCount; }
			set { tbText.MaxHintCount = value; }
		}

		[DefaultValue(3), Description("Минимальное количство набранных букв для срабатывания хинтов")]
		public int MinLettersForHint
		{
			get { return tbText.MinLettersForHint; }
			set { tbText.MinLettersForHint = value; }
		}

		[DefaultValue(13)]
		public int HintItemHeight
		{
			get { return tbText.ItemHeight; }
			set { tbText.ItemHeight = value; }
		}

		[DefaultValue(4)]
		public int HeightInHints
		{
			get { return tbText.HeightInHints; }
			set { tbText.HeightInHints = value; }
		}

		[DefaultValue(SortType.Alphabet)]
		public SortType HintSort
		{
			get { return tbText.Sort; }
			set { tbText.Sort = value; }
		}
		#endregion Public Properties

		protected static readonly object EventChanged = new object();

		[Category("Action"),Description("Fired when data is changed")]
		public event EventHandler Changed 
		{
			add 
			{
				Events.AddHandler(EventChanged, value);
			}
			remove 
			{
				Events.RemoveHandler(EventChanged, value);
			}
		}

		public virtual void OnChanged() 
		{       
			EventHandler initHandler = (EventHandler)Events[EventChanged];
			if (initHandler != null) 
			{
				initHandler(this, EventArgs.Empty);
			} 
		}

		private void tbText_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			if (e.KeyData == Keys.Return && AutoTextColumn!="")
			{
				btnChoose_Click(sender, null);
			}
		}

		public virtual void btnEdit_Click(object sender, System.EventArgs e)
		{
		}
	}
}
