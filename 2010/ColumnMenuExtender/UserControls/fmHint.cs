﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace ColumnMenuExtender
{
	public delegate void ItemSelectEventHandler(object sender, ItemSelectEventArgs e);

	/// <summary>
	/// Summary description for fmHint.
	/// </summary>
	public class fmHint : System.Windows.Forms.Form
	{
		private System.Windows.Forms.ListBox listBox;
		private System.ComponentModel.Container components = null;
		private int heightInHints = 4;
		private int delta;

		#region .ctor
		public fmHint()
		{
			InitializeComponent();
			delta = listBox.Height - ((int)listBox.Height/listBox.ItemHeight)*listBox.ItemHeight;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}
		#endregion


		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.listBox = new System.Windows.Forms.ListBox();
			this.SuspendLayout();
			// 
			// listBox
			// 
			this.listBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.listBox.Dock = System.Windows.Forms.DockStyle.Top;
			this.listBox.Location = new System.Drawing.Point(0, 0);
			this.listBox.Name = "listBox";
			this.listBox.Size = new System.Drawing.Size(144, 67);
			this.listBox.TabIndex = 0;
			this.listBox.SizeChanged += new System.EventHandler(this.listBox_SizeChanged);
			this.listBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.listBox_MouseUp);
			this.listBox.MouseMove += new System.Windows.Forms.MouseEventHandler(this.listBox_MouseMove);
			// 
			// fmHint
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(144, 96);
			this.ControlBox = false;
			this.Controls.Add(this.listBox);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "fmHint";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.Text = "fmHint";
			this.TopMost = true;
			this.ResumeLayout(false);

		}
		#endregion

		
		public void SendKey(Keys key)
		{
			if (listBox.Items.Count == 0)
				return;
			switch (key)
			{
				case Keys.Down:
					if (listBox.SelectedIndex < 0)
						listBox.SelectedIndex = 0;
					else if (listBox.SelectedIndex < listBox.Items.Count-1)
						listBox.SelectedIndex++;
					break;
				case Keys.Up:
					if (listBox.SelectedIndex < 0)
						listBox.SelectedIndex = listBox.Items.Count-1;
					else if (listBox.SelectedIndex > 0)
						listBox.SelectedIndex--;
					break;
				case Keys.Enter:
					if (listBox.SelectedIndex < 0)
						return;
					this.OnItemSelect( new ItemSelectEventArgs(listBox.SelectedIndex, listBox.SelectedItem) );
					break;
			}
		}


		public new void Show()
		{
			int count = listBox.Items.Count>=heightInHints ? heightInHints : listBox.Items.Count;
			listBox.Height = listBox.ItemHeight * count + delta;
			base.Show();
		}

		#region Events
		private void listBox_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			int index = listBox.IndexFromPoint(e.X, e.Y);
			if (index >= 0)
				listBox.SelectedIndex = index;
		}

		private void listBox_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			if (e.Button != MouseButtons.Left)
				return;
			int index = listBox.IndexFromPoint(e.X, e.Y);
			if (index < 0)
				return;
			this.OnItemSelect( new ItemSelectEventArgs(index, listBox.Items[index]) );
		}

		private void listBox_SizeChanged(object sender, System.EventArgs e)
		{
			this.Height = listBox.Height;
		}


		protected static readonly object EventItemSelect = new object();

		[Category("Action"),Description("Fired item is selected")]
		public event ItemSelectEventHandler ItemSelect
		{
			add 
			{
				Events.AddHandler(EventItemSelect, value);
			}
			remove 
			{
				Events.RemoveHandler(EventItemSelect, value);
			}
		}

		protected virtual void OnItemSelect(ItemSelectEventArgs e) 
		{
			this.Hide();
			ItemSelectEventHandler initHandler = (ItemSelectEventHandler)Events[EventItemSelect];
			if (initHandler != null) 
			{
				initHandler(this, e);
			} 
		}
		#endregion

		#region Public properties
		public System.Windows.Forms.ListBox.ObjectCollection Items
		{
			get { return listBox.Items; }
		}

		public int ItemHeight
		{
			get { return listBox.ItemHeight; }
			set
			{
				listBox.ItemHeight = value;
				if (this.Visible)
					this.Show();
			}
		}
		public int SelectedIndex
		{
			get { return listBox.SelectedIndex; }
		}
		public int HeightInHints
		{
			get { return heightInHints; }
			set
			{
				heightInHints = value;
				if (this.Visible)
					this.Show();
			}
		}
		#endregion
	}

	public class ItemSelectEventArgs:EventArgs
	{
		public ItemSelectEventArgs(int selectedIndex, object selectedItem)
		{
			SelectedIndex = selectedIndex;
			SelectedItem = selectedItem;
		}

		public int SelectedIndex;
		public object SelectedItem;
	}
}
