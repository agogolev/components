using System;
using System.Data;
using System.Collections;
using System.Text.RegularExpressions;
using Excel = Microsoft.Office.Interop.Excel;
using System.IO;
using System.Threading;
using ColumnMenuExtender.Forms;
using System.Windows.Forms;

namespace ColumnMenuExtender.XLReport
{
	public enum XLReportTask
	{
		Save,
		Print,
		Export
	}

	public class BaseXLReport
	{
		protected int columnsUsed = 0;

		private string templateFilename;
		private string outputFilename;
		private string outputPath = "";
		private Excel.Application excelApplication;

		protected Excel.Workbook workbook;
		protected Excel.Worksheet worksheet;
		protected Excel.Worksheet newWorksheet;

		
		private string defaultPath;

		
		protected Hashtable objLookupFields;

		private DataSet dataSet;
		private DataView[] dataView;
		private string[] tableNames;
		private string[] rangeNames;

		private string[] stringArray;
		private object[] objectArray;
		//private int rowCounter;

		public string DateFormat = "";
		public bool InsertRows = true;
		public uint QuantityOfConstRows = 0;

		private bool duplicateRows = true;
		public bool DuplicateRows
		{
			get {return duplicateRows;}
			set {duplicateRows = value;}
		}

		private DataTable secDataTable;
		private string keyField="";
		private string secTableRange="";
		private bool needGroupOnSecTable = false;

		private string cutPath(string path)
		{
			if (path.Substring(path.Length - 10, 10) == @"bin\Debug\")
			{
				return path.Substring(0, path.Length - 10);
			}
			else 
				return path;
		}

		protected void initFileName(string tmplFileName, string outFileName)
		{
			defaultPath = Application.StartupPath + @"\";
			templateFilename = cutPath(defaultPath) + tmplFileName;
			if (outputPath == "")
			{
			
				//�������� ����� Reports � ����� ���������� ����
				DirectoryInfo dir = new DirectoryInfo(Application.StartupPath);
				try
				{
					dir.CreateSubdirectory("Reports");
				}
				finally
				{
					outputFilename = defaultPath +@"Reports\"+ outFileName;
				}
			}
			else
				outputFilename = outputPath + outFileName;
		}


		#region .ctor

		public BaseXLReport(DataSet dataSet, string[] tableNames, string[] rangeNames, string[] stringArray, object[] objectArray, string filename, string outFilename, string outputPath)
		{
			this.outputPath = outputPath;
			initFileName(filename, outFilename);
			initStringArray(stringArray);
			initObjectArray(objectArray);
			init(dataSet, tableNames, rangeNames);
		}

		public BaseXLReport(DataView[] dataView, string[] rangeNames, string[] stringArray, object[] objectArray, string filename, string outFilename, string outputPath)
		{
			this.outputPath = outputPath;
			initStringArray(stringArray);
			initObjectArray(objectArray);
			initFileName(filename, outFilename);
			init(dataView, rangeNames);
		}

		public BaseXLReport(DataView dataView, string rangeName, DataTable secDataTable, string secTableRange, string keyField, bool needGroupOnSecTable, string filename, string outFilename)
		{
			initFileName(filename, outFilename);
			init(new DataView []{dataView}, new string []{rangeName});

			this.secDataTable = secDataTable;
			this.keyField = keyField;
			this.secTableRange = secTableRange;
			this.needGroupOnSecTable = needGroupOnSecTable;
		}

		
		#endregion

		#region init

		protected void initObjLookupFields(Hashtable objLookupFields)
		{
			this.objLookupFields = objLookupFields;
		}

		private void initStringArray(string[] stringArray)
		{
			this.stringArray = stringArray;
		}

		private void initObjectArray(object[] objectArray)
		{
			this.objectArray = objectArray;
		}

		protected void init(DataSet dataSet, string[] tableNames, string[] rangeNames)
		{
			if (dataSet != null)
			{
				this.dataSet = dataSet;
				this.dataView = null;
				this.tableNames = tableNames;
				this.rangeNames = rangeNames;
			}
		}

		protected void init(DataView[] dataView, string[] rangeNames)
		{
			if (dataView != null)
			{
				this.dataView = dataView;
				this.dataSet = null;
				this.tableNames = null;
				this.rangeNames = rangeNames;
			}
		}
		
		#endregion

		public void Create()
		{
			Create(false);
		}

		private XLReportTask task;
		private Thread mThread;

		public void Create(bool saveOnly)
		{
			if (saveOnly)
				task = XLReportTask.Save;
			else
				task = XLReportTask.Print;

			mThread = new Thread(new ThreadStart(startCreate));
			mThread.Start();
		}

		public void Export()
		{
			task = XLReportTask.Export;

			mThread = new Thread(new ThreadStart(startCreate));
			mThread.Start();
		}

		protected virtual void createCustom()
		{

		}

		private void startCreate()
		{
			try
			{
			
				excelApplication = new Excel.Application();

				try
				{
					workbook = excelApplication.Workbooks.Open(templateFilename,
						Type.Missing, Type.Missing, Type.Missing, Type.Missing,
						Type.Missing, Type.Missing, Type.Missing, Type.Missing,
						Type.Missing, Type.Missing, Type.Missing, Type.Missing,
						Type.Missing, Type.Missing);
				}
				catch (Exception e)
				{
					BaseGenericForm.ShowError(null,"������� ����������",e);//������ �� ������!
					excelApplication.Quit();
					return;
				}

				string outputFilenameTemp = outputFilename;
				int fileNum = 0;

				/*while (File.Exists(outputFilenameTemp + ".xls"))
				{
					outputFilenameTemp = outputFilename + "(" + (++fileNum).ToString() + ")";
				}*/

				bool looking = true;

				while (looking)
				{
					if (File.Exists(outputFilenameTemp + ".xls"))
					{
						try
						{
							File.Delete(outputFilenameTemp + ".xls");
							looking = false;
						}
						catch
						{
							outputFilenameTemp = outputFilename + "(" + (++fileNum).ToString() + ")";
						}
					}
					else
						looking = false;
				}

//				if (File.Exists(outputFilenameTemp + ".xls"))
//				{
//					if (File.)
//					File.Delete(outputFilenameTemp + ".xls");
//				}
				outputFilename = outputFilenameTemp;

				//ExcelApplication.DisplayAlerts = false;
				try
				{
					workbook.SaveAs(outputFilename, 
						Type.Missing, Type.Missing, Type.Missing,
						Type.Missing, Type.Missing, Excel.XlSaveAsAccessMode.xlNoChange,
						Type.Missing, Type.Missing, Type.Missing, Type.Missing,
						Type.Missing);
				}
				catch (Exception e)
				{
					BaseGenericForm.ShowError(null,"�� ������� ��������� ��������",e);//���������� ���������!
					excelApplication.Quit();
					return;
				}
				//ExcelApplication.DisplayAlerts = true;

				worksheet = (Excel.Worksheet)workbook.Worksheets[1];


				newWorksheet = (Excel.Worksheet)workbook.Worksheets.Add(Type.Missing, Type.Missing, Type.Missing, Type.Missing);
				newWorksheet.Name = "hiddenSheet";
				newWorksheet.Visible = Excel.XlSheetVisibility.xlSheetHidden;

				System.Threading.Thread thisThread = System.Threading.Thread.CurrentThread;
				System.Globalization.CultureInfo originalCulture = thisThread.CurrentCulture;

				try
				{
					thisThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");

					//rowCounter = 0;
					createCustom();
					
					if (dataSet != null)
					{
						if (!DuplicateRows)
						{
							createXLReportByDataTableNonDup();
						}
						else
						{
							if (tableNames.Length != rangeNames.Length)
								throw new ApplicationException("tableNames.Length != rangeNames.Length!!!!!!!");
							for (int i = 0; i < tableNames.Length; i++)
								createXLReportByDataTable(i);
						}
					}
					else if (dataView != null)
					{
						if (!DuplicateRows)
						{
							createXLReportByDataTableNonDup();
						}
						else
						{
							if (dataView.Length != rangeNames.Length)
								throw new ApplicationException("dataView.Length != rangeNames.Length!!!!!!!");
							for (int i = 0; i < dataView.Length; i++)
								createXLReportByDataTable(i);
						}
					}
					if (stringArray != null)
					{
						for (int i = 0; i < stringArray.Length; i++)
						{
							createXLReportByStringArray(i);
						}
						columnsUsed += stringArray.Length;
					}
					if (objectArray != null)
					{
						for (int i = 0; i < objectArray.Length; i++)
							createXLReportByObjectArray(i);
						columnsUsed += objectArray.Length;
					}
				}
				catch (Exception e)
				{
					CustomMessageBox.Show("�������� ������ ��� �������� ������.\r\n�������� ������:\r\n"+e.ToString(), "������!");
					//MessageBox.Show("�������� ������ ��� �������� ������.\n�������� ������:\n-------------\n" + e.Message + "\n-------------\n" + e.StackTrace + "\n-------------");
					workbook.Close(false, Type.Missing, Type.Missing);
					excelApplication.Quit();
					File.Delete(outputFilename + ".xls");
					return;
				}
				finally
				{
					thisThread.CurrentCulture = originalCulture;
				}
	
				if (task == XLReportTask.Print)
				{
					try
					{
						excelApplication.Visible = true;
						worksheet.PrintPreview(false);
						//worksheet.PrintOut(Type.Missing, Type.Missing, 1, true, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
					
					}
					catch (System.Runtime.InteropServices.COMException e)
					{
						//hidePreview();
						//
						MessageBox.Show(e.Message);
					}
					finally 
					{
						excelApplication.Visible = false;
					}
				}
				else if (task == XLReportTask.Export)
				{
					excelApplication.Visible = true;
				}

				workbook.Save();

				if (task != XLReportTask.Export)
				{
					workbook.Close(false, Type.Missing, Type.Missing);

					excelApplication.Quit();
					if (task == XLReportTask.Print)
						File.Delete(outputFilename + ".xls");
				}
			}
			catch (Exception e)
			{
				CustomMessageBox.Show("General failure!\r\n-------------\r\n'"+e.ToString(), "Error!");
				//MessageBox.Show("General failure!\n-------------\n" + e.Message + "\n-------------\n" + e.StackTrace + "\n-------------");
				if (excelApplication != null)
					excelApplication.Quit();
			}

		}
//
//		private int previewCount = 0;
//
//		private void showPreview(Excel.Worksheet worksheet)
//		{
//			previewCount++;
//			ExcelApplication.Visible = true;
//			worksheet.PrintPreview(false);
//		}
//
//		private void hidePreview()
//		{
//			if (--previewCount == 0)
//				ExcelApplication.Visible = false;
//		}

		//public void AddRange(string tableName, string 

		#region ������ � ��������
		private string[] parseAddress(string address)
		{
			string[] arStr;
			arStr = new string[4];
			Regex re = new Regex("([a-zA-Z]+)([0-9]+)");

			Match m = re.Match(address);
			if (m.Success)
			{
				arStr[0] = m.Groups[1].Value;
				arStr[1] = m.Groups[2].Value;

				m = m.NextMatch();
				if (m.Success)
				{
					arStr[2] = m.Groups[1].Value;
					arStr[3] = m.Groups[2].Value;
				}
				else
				{
					arStr[2] = arStr[0];
					arStr[3] = arStr[1];
				}
				return arStr;
			}
			else throw new ApplicationException("Wrong excel address!");
		}

		private string next(string addr, int num)
		{
			string res = addr;
			for (int i = 0; i < num; i++)
			{
				res = next(res);
			}
			return res;
		}

		private string next(string addr)
		{
			try
			{
				int i = Convert.ToInt32(addr);
				return (i+1).ToString();
			}
			catch
			{
				if (addr.Length == 1)
				{
					char chr = addr[0];
					if (chr=='Z')return "AA";
					chr = (char)(chr + 1);
					return "" + chr;
				}
				if (addr.Length == 2)
				{
					if(addr[1]=='Z')
					{
						return ""+(char)(addr[0] + 1)+'A';
					}
					return ""+addr[0]+(char)(addr[1] + 1);
				}
				throw new ApplicationException("Dont use addresses with three letter!");
			}
		}

		private string prev(string addr)
		{
			try
			{
				int i = Convert.ToInt32(addr);
				return (i-1).ToString();
			}
			catch
			{
				if (addr.Length == 1)
				{
					char chr = addr[0];
					if (chr=='A')
						throw new ApplicationException("Out of range!");
					chr = (char)(chr - 1);
					return "" + chr;
				}
				if (addr.Length == 2)
				{
					if(addr[1]=='A')
					{
						if(addr[0]=='A') return "Z";
						else return ""+(char)(addr[0] - 1)+'Z';
					}
					return ""+addr[0]+(char)(addr[1] - 1);
				}
				throw new ApplicationException("Dont use addresses with three letter!");
			}
		}

		private Excel.Range nextToRightRange(Excel.Range rng)
		{
			string addr = rng.get_Address(false, false, Excel.XlReferenceStyle.xlA1, Type.Missing, Type.Missing);
			string[] str = parseAddress(addr);
			string addrNext = next(str[0]) + str[1];
			return worksheet.get_Range(addrNext, addrNext);
		}

		private Excel.Range moveRangeOnThisRow(Excel.Range rng, string LeftColumn)
		{
			string addr = rng.get_Address(false, false, Excel.XlReferenceStyle.xlA1, Type.Missing, Type.Missing);
			string[] str = parseAddress(addr);
			if (LeftColumn == null || LeftColumn == "") LeftColumn = str[0];
			string addrNext = LeftColumn + str[1];
			return worksheet.get_Range(addrNext, addrNext);
		}

		private Excel.Range nextToDownRange(Excel.Range rng)
		{
//			return nextToDownRange(rng, 1);����� ���� ����� ��������� �� ������� ��� ��� �� ������ ��������?
			string addr = rng.get_Address(false, false, Excel.XlReferenceStyle.xlA1, Type.Missing, Type.Missing);
			string[] str = parseAddress(addr);
			string addrNext = str[0] + next(str[1]);
			return worksheet.get_Range(addrNext, addrNext);
		}

		private Excel.Range nextToDownRange(Excel.Range rng, int num)
		{
			string addr = rng.get_Address(false, false, Excel.XlReferenceStyle.xlA1, Type.Missing, Type.Missing);
			string[] str = parseAddress(addr);
			string addrNext = str[0] + next(str[1], num);
			return worksheet.get_Range(addrNext, addrNext);
		}
		
		private Excel.Range nextToUpRange(Excel.Range rng)
		{
			string addr = rng.get_Address(false, false, Excel.XlReferenceStyle.xlA1, Type.Missing, Type.Missing);
			string[] str = parseAddress(addr);
			string addrNext = str[0] + prev(str[1]);
			return worksheet.get_Range(addrNext, addrNext);
		}

		private Excel.Range nextToUpRange(Excel.Range rng, string LeftColumn)
		{
			string addr = rng.get_Address(false, false, Excel.XlReferenceStyle.xlA1, Type.Missing, Type.Missing);
			string[] str = parseAddress(addr);
			if (LeftColumn == null || LeftColumn == "") LeftColumn = str[0];
			string addrNext = LeftColumn + prev(str[1]);
			return worksheet.get_Range(addrNext, addrNext);
		}

		private Excel.Range nextToDownNotSingleRange(Excel.Range rng, int num)
		{
			string addr = rng.get_Address(false, false, Excel.XlReferenceStyle.xlA1, Type.Missing, Type.Missing);
			string[] str = parseAddress(addr);
			string addrNext1 = str[0] + next(str[1], num);
			string addrNext2 = str[2] + next(str[3], num);
			return worksheet.get_Range(addrNext1, addrNext2);
		}

//		private Excel.Range nextToUpNotSingleRange(Excel.Range rng, int num)
//		{
//			string addr = rng.get_Address(false, false, Excel.XlReferenceStyle.xlA1, Type.Missing, Type.Missing);
//			string[] str = parseAddress(addr);
//			string addrNext1 = str[0] + prev(str[1]);
//			string addrNext2 = str[2] + prev(str[3]);
//			return worksheet.get_Range(addrNext1, addrNext2);
//		}

		private string nextToDown(string addr)
		{
			string[] str = parseAddress(addr);
			return str[0] + next(str[1]);
		}
		#endregion

		private void createXLReportByStringArray(int k)
		{
			char chr = (char)('A' + columnsUsed + k);
			string name = "Array_" + k.ToString();
			workbook.Names.Add(name, "=hiddenSheet!$" + chr + "$1", Type.Missing,
				Type.Missing, Type.Missing, Type.Missing, Type.Missing,
				Type.Missing, Type.Missing, Type.Missing, Type.Missing);
			newWorksheet.get_Range(name, Type.Missing).Value2 = "=\"" + stringArray[k] + "\"";
		}

		private void createXLReportByObjectArray(int k)
		{
			char chr = (char)('A' + columnsUsed + k);
			string name = "Array_" + k.ToString();
			workbook.Names.Add(name, "=hiddenSheet!$" + chr + "$1", Type.Missing,
				Type.Missing, Type.Missing, Type.Missing, Type.Missing,
				Type.Missing, Type.Missing, Type.Missing, Type.Missing);
			newWorksheet.get_Range(name, Type.Missing).Value2 = objectArray[k];
		}
		

		private void createXLReportByDataTable(int k)
		{	
			DataTable dtTmp = null;
			if (dataSet != null)
				dtTmp = dataSet.Tables[tableNames[k]];
			else if (dataView != null)
				dtTmp = dataView[k].Table;

			if (dtTmp != null)
			{//dtTmp != null
				Excel.Range rngTable;//Range ������� �������
				Excel.Range rngSecTable = null;//Range ��������� �������
				string rngExc="";

				try
				{
					rngExc = rangeNames[k];
					rngTable = worksheet.get_Range(rangeNames[k], Type.Missing);
					rngExc = secTableRange;
					if (secDataTable != null && secTableRange != null && secTableRange != "" && keyField != null && keyField != "")
						rngSecTable = worksheet.get_Range(secTableRange, Type.Missing);
				}
				catch
				{
					throw new ApplicationException("��� \""+rngExc+"\" � ������� �� �������!");
				}

				ArrayList colRanges = new ArrayList();//����� ����������� �������
				ArrayList secTableColRanges = new ArrayList();//����� ����������� ������� ��� ��������� �������
				object rowNumRange = null;//����� ��� rowNum �������� �������
				object rowNumRangeSec = null;//����� ��� rowNum ��������� ������� (���� �� ������������)

				if (rngTable != null)
				{//rngTable != null
					string[] str = parseAddress(rngTable.get_Address(false, false, Excel.XlReferenceStyle.xlA1, Type.Missing, Type.Missing));
					string l_c = str[0];
					string t_r = str[1];
					string r_c = str[2];
					string b_r = str[3];
				
					//Excel.Range rngLeftTop = worksheet.get_Range(l_c + t_r, l_c + t_r);
					Excel.Range rngTop = worksheet.get_Range(l_c + t_r, r_c + t_r);
					Excel.Range rngLeftBottom = worksheet.get_Range(l_c + b_r, l_c + b_r);

					//��������� ColRanges
					colRanges = GetColRange(rngTop, dtTmp, out rowNumRange);

					Excel.Range rngTopSec = null;
					if (rngSecTable != null)
					{
						string[] strSec = parseAddress(rngSecTable.get_Address(false, false, Excel.XlReferenceStyle.xlA1, Type.Missing, Type.Missing));
						string l_cSec = strSec[0];
						string t_rSec = strSec[1];
						string r_cSec = strSec[2];
						//string b_rSec = str[3];
				
						rngTopSec = worksheet.get_Range(l_cSec + t_rSec, r_cSec + t_rSec);
						//������� ������ ������ ��� ��������� �������
						secTableColRanges = GetColRange(rngTopSec, secDataTable, out rowNumRangeSec);
					}
				
					int end, endTotal;
					if (dataView != null)
						end = endTotal = dataView[k].Count-1;
					else
						end = endTotal = dtTmp.Rows.Count-1;

					if (rngTopSec != null && secTableColRanges.Count > 0)//���� ���� ��������� �������, ���� ��������� � �
						endTotal += secDataTable.Rows.Count;

					int curRowNum = Convert.ToInt32(t_r);//������� ������, ���� ������
					Excel.Range rngToGroup = null;//������� �����������

					for (int j = 0, jTotal = 0; j <= end; j++)//��� ������ ������
					{
						if (j > QuantityOfConstRows && jTotal != endTotal)
						{
							InsertRow(ref rngLeftBottom, rngTop, "");
							if (rngToGroup != null)//���� ���-�� ���� ���� �������������, ����� ������� ����� ������ - ��������� �����������
								rngToGroup.Group(true, Type.Missing, Type.Missing, Type.Missing);
						}

						rngToGroup = null;//���������� ������� �����������
						
						foreach(object[] oArr in colRanges)//��� ������ �������
						{
							string colName = (string)oArr[0];
							object o;
							if (dataView != null)
								o = dataView[k][j][colName];
							else 
								o = dtTmp.Rows[j][colName];

							Excel.Range Rng = worksheet.get_Range(oArr[1]+curRowNum.ToString(),oArr[1]+curRowNum.ToString());
							Rng.Value2 = GetColValue(o, colName);

							if( (bool)Rng.MergeCells &&
								(Excel.XlVAlign)Rng.VerticalAlignment == Excel.XlVAlign.xlVAlignJustify)
							{
								AutoFitMergedCellRowHeight(Rng);
							}
						}

						if (rowNumRange != null)
							worksheet.get_Range(rowNumRange+curRowNum.ToString(),rowNumRange+curRowNum.ToString()).Value2 = j+1;
						
						curRowNum++;//����������� ����� ������
						jTotal++;//����������� ����� ���������� ��� DataRow (� ������� � ��������������� ��������)

						//���� ���� ��������� �������
						if (rngTopSec != null && secTableColRanges.Count > 0)
						{//1
							object keyVal = dtTmp.Rows[j][keyField];
							DataView dv = secDataTable.DefaultView;
							string rf = keyField+"='"+keyVal.ToString()+"'";
							dv.RowFilter = rf;

							string groupBegin="";
							if (dv.Count > 0 && needGroupOnSecTable)
								groupBegin = "A" + curRowNum.ToString();
                            
							foreach (DataRowView drv in dv)//��� ������ ������ �� ��������� �������
							{
								if (jTotal != endTotal)
									InsertRow(ref rngLeftBottom, rngTopSec, (string)(((object [])secTableColRanges[0])[1]));//��������� ������
								else //���� ��������� ������, �� ���� �������������� �����������
									CopyRow(rngLeftBottom, rngTopSec, (string)(((object [])secTableColRanges[0])[1])); 

								foreach(object[] oArrSec in secTableColRanges)//��� ������ ������� �� ��������� �������
								{
									string colName = (string)oArrSec[0];
									object o = drv[colName];

									Excel.Range RngSec = worksheet.get_Range(oArrSec[1]+curRowNum.ToString(),oArrSec[1]+curRowNum.ToString());
									RngSec.Value2 = GetColValue(o, null);
									
									if( (bool)RngSec.MergeCells &&
										(Excel.XlVAlign)RngSec.VerticalAlignment == Excel.XlVAlign.xlVAlignJustify)
									{
										AutoFitMergedCellRowHeight(RngSec);
									}
								}
								
								jTotal++;
								curRowNum++;//����������� ����� ������
							}

							if (dv.Count > 0 && needGroupOnSecTable)
							{
								int prevRowNum = curRowNum-1;
								rngToGroup = worksheet.get_Range(groupBegin, "A"+prevRowNum.ToString());
							}
						}//1
					}

					if (rngToGroup != null)//���� ���-�� ���� ���� �������������, �������� ��������� �����������
						rngToGroup.Group(true, Type.Missing, Type.Missing, Type.Missing);
				}//rngTable != null
			}//dtTmp != null
		}

		private void InsertRow(ref Excel.Range rngLeftBottom, Excel.Range copyRange, string LeftColumn)
		{
			if(InsertRows)
			{
				rngLeftBottom.EntireRow.Insert(Excel.XlInsertShiftDirection.xlShiftDown, false);
				
				if (copyRange != null)//���� ���� ���-�� ����������� (��� EntireRow.Insert �� ����������, ��������, �������)
				{
					Excel.Range rngTmp = nextToUpRange(rngLeftBottom, LeftColumn);//���������� ���������� ������ � ���� ������ � ����������� �������
					copyRange.Copy(rngTmp);//��� Insert ���������� ����� rngLeftBottom
				}
			}
			else//������ ���� ��������� ������ ����� - ������� �������� �� ��� ������
			{
				copyRange.Copy(Type.Missing);
				rngLeftBottom.Insert(Excel.XlInsertShiftDirection.xlShiftDown, Type.Missing);
			}
		}

		private void CopyRow(Excel.Range rngDestination, Excel.Range sourceRange, string LeftColumn)
		{
			if (sourceRange != null)//���� ���� ���-�� �����������
			{
				Excel.Range rngTmp = moveRangeOnThisRow(rngDestination, LeftColumn);
				sourceRange.Copy(rngTmp);
			}
		}

		//�������� �������� � ������� � ������� ����
		private string GetColValue(object o, string colName)
		{
			string result="";

			//���� ��� ����� ��������
			if (colName != null && colName != "" && o != DBNull.Value && 
					objLookupFields != null && objLookupFields.ContainsKey(colName))
			{
				foreach (ListItem li in (ComboBox.ObjectCollection)objLookupFields[colName])
				{
					if (li.Value == (int)o)
					{
						o = li.Text;
						break;
					}
				}
			}

			if (o is DateTime)
			{
				if (DateFormat == "")
					result = ((DateTime)o).ToShortDateString();
				else
					result = ((DateTime)o).ToString(DateFormat);
			}
			else result = o.ToString();

			return result;
		}

		/// <summary>
		///�������� ������ ������� ��� ��������� Range
		///[0] - ��� �������
		///[1] - ����� ������� (������ ������� � �� ������!!!)
		/// </summary>
		/// <param name="rngTemp">��� ������</param>
		/// <param name="table">��� ����� ������� ������</param>
		/// <param name="rowNumRange">�������� � rowNumRange, ���� ������� ��������� (��� ���� ������ ����� ������� �����)</param>
		/// <returns>���������� ������ � ���������� ���� ����������</returns>
		private ArrayList GetColRange(Excel.Range rngTemp, DataTable table, out object rowNumRange)
		{
			ArrayList colRanges = new ArrayList();
			rowNumRange = null;

			if (table != null)
			{//1
				string[] str = parseAddress(rngTemp.get_Address(false, false, Excel.XlReferenceStyle.xlA1, Type.Missing, Type.Missing));
				string l_c = str[0];
				string t_r = str[1];
				string r_c = str[2];
				//string b_r = str[3];
				
				string addrRightTopNext = next(r_c) + t_r;//Right+1;Top

				rngTemp = worksheet.get_Range(l_c + t_r, l_c + t_r);//Left;Top
				string addrTemp = rngTemp.get_Address(false, false, Excel.XlReferenceStyle.xlA1, Type.Missing, Type.Missing);
			
				int i = 0;
			
				do
				{
					object value2 = rngTemp.Formula;
					if (value2 is string)
					{
						string val = (string)value2;
				
						if (val != null && val.Length > 0 && val[0] == '=')
						{
							val = val.Substring(1);
							string [] adrTmpArr = parseAddress(addrTemp);
							//if (dataSet != null)
							//{
							if (table.Columns.IndexOf(val) >= 0)
								colRanges.Add(new object[] {val, adrTmpArr[0]});//���� Left Column
							else if (val.ToLower().Equals("rownum"))
								rowNumRange = adrTmpArr[0];//���� Left Column
							/*}
							else if (dataView != null)
							{
								if (dataView[k].Table.Columns.IndexOf(val) >= 0)
									colRanges.Add(new object[] {val, rngTemp.get_Address(false, false, Excel.XlReferenceStyle.xlA1, Type.Missing, Type.Missing)});
								else if (val.ToLower().Equals("rownum") && rowNumRange != null)
									rowNumRange = rngTemp.get_Address(false, false, Excel.XlReferenceStyle.xlA1, Type.Missing, Type.Missing);
							}*/
						}
					}
					rngTemp = nextToRightRange(rngTemp);//��������� ������
					addrTemp = rngTemp.get_Address(false, false, Excel.XlReferenceStyle.xlA1, Type.Missing, Type.Missing);
					i++;
				} 
				while(addrTemp != addrRightTopNext && i < /*15*/50);
			}//1

			return colRanges;
		}

		private int diff(string str1, string str2)
		{
			try 
			{
				int i1 = Convert.ToInt32(str1);
				int i2 = Convert.ToInt32(str2);
				return Math.Abs(i2-i1) + 1;
			}
			catch
			{
				throw new Exception("XLReport.cs: ������!");
			}
		}

		private void createXLReportByDataTableNonDup()
		{	
			Excel.Range rngTable;

			int rowCount = 0;
			int currentRowNum = 0;

			for(int k = 0; k < rangeNames.Length; k++)
			{
				try
				{
					rngTable = worksheet.get_Range(rangeNames[k], Type.Missing);
				}
				catch
				{
					throw new ApplicationException("��� \""+rangeNames[k]+"\" � ������� �� �������!");
				}
				Excel.Range rngTemp;
				string addrTemp;
				ArrayList colRanges = new ArrayList(); // ������ �������
				object rowNumRange = null; // ����� ��� rowNum

				if (rngTable != null)
				{
					string[] str = parseAddress(rngTable.get_Address(false, false, Excel.XlReferenceStyle.xlA1, Type.Missing, Type.Missing));
					string l_c = str[0];
					string t_r = str[1];
					string r_c = str[2];
					string b_r = str[3];

					currentRowNum = diff(t_r, b_r);
				
					Excel.Range rngLeftTop = worksheet.get_Range(l_c + t_r, l_c + t_r);
					Excel.Range rngRightTop = rngTable.get_Range(r_c + t_r, r_c + t_r);
					Excel.Range rngTop = worksheet.get_Range(l_c + t_r, r_c + t_r);

					Excel.Range rngLeftBottom = nextToDownRange(rngLeftTop);//rngTable.get_Range("A2","A2");

					string addrRightTopNext = next(r_c) + t_r;

					rngTemp = rngLeftTop;
					addrTemp = rngTemp.get_Address(false, false, Excel.XlReferenceStyle.xlA1, Type.Missing, Type.Missing);
					int i = 0;

					// ��������� colRanges 
					do
					{
						object value2 = rngTemp.Formula;
						//object value2 = rngTemp.Value2;
						if (value2 is string)
						{
							string val = (string)value2;
				
							if (val != null && val.Length > 0 && val[0] == '=')
							{
								val = val.Substring(1);
								if (dataSet != null)
								{
									if (dataSet.Tables[tableNames[0]].Columns.IndexOf(val) >= 0)
										colRanges.Add(new object[] {val, rngTemp.get_Address(false, false, Excel.XlReferenceStyle.xlA1, Type.Missing, Type.Missing)});
									else if (val.ToLower().Equals("rownum"))
										rowNumRange = rngTemp.get_Address(false, false, Excel.XlReferenceStyle.xlA1, Type.Missing, Type.Missing);
									//rngTemp.Value2 = i.ToString();
								}
								else if (dataView != null)
								{
									if (dataView[0].Table.Columns.IndexOf(val) >= 0)
										colRanges.Add(new object[] {val, rngTemp.get_Address(false, false, Excel.XlReferenceStyle.xlA1, Type.Missing, Type.Missing)});
									else if (val.ToLower().Equals("rownum"))
										rowNumRange = rngTemp.get_Address(false, false, Excel.XlReferenceStyle.xlA1, Type.Missing, Type.Missing);
								}
							}
						}
						rngTemp = nextToRightRange(rngTemp);
						addrTemp = rngTemp.get_Address(false, false, Excel.XlReferenceStyle.xlA1, Type.Missing, Type.Missing);
						i++;
					} 
					while(addrTemp != addrRightTopNext && i < 100);

					if (dataSet != null)
					{
						#region ...
						if (rowCount >= dataSet.Tables[tableNames[0]].Rows.Count)
						{
							foreach(object[] oArr in colRanges)
							{
								Excel.Range rngExact = worksheet.get_Range(oArr[1],oArr[1]);
								rngExact.Value2 = "";
							}
							if (rowNumRange != null)
							{
								Excel.Range rngExact = worksheet.get_Range(rowNumRange, rowNumRange);
								rngExact.Value2 = "";
							}
						}
						else
						{
							for (int j = rowCount; j < rowCount+currentRowNum && j < dataSet.Tables[tableNames[0]].Rows.Count; j++)
							{
								//rngTop.Copy(Type.Missing);
								//rngLeftBottom.Insert(Excel.XlInsertShiftDirection.xlShiftDown, Type.Missing);
								//rngLeftBottom = nextToDownRange(rngLeftTop);//rngTable.get_Range("A2","A2");
								foreach(object[] oArr in colRanges)
								{
									string colName = (string)oArr[0];
									object o = dataSet.Tables[tableNames[0]].Rows[j][colName];
									if (o != DBNull.Value && objLookupFields != null && objLookupFields.ContainsKey(colName))
									{
										foreach (ListItem li in (ComboBox.ObjectCollection)objLookupFields[colName])
										{
											if (li.Value == (int)o)
											{
												o = li.Text;
												break;
											}
										}
									}
									Excel.Range rngExact = worksheet.get_Range(oArr[1],oArr[1]);
									rngExact = nextToDownRange(rngExact, j - rowCount);
									if (o is DateTime)
									{
										if (DateFormat == "")
											rngExact.Value2 = ((DateTime)o).ToShortDateString();
										else
											rngExact.Value2 = ((DateTime)o).ToString(DateFormat);
									}
									else
										rngExact.Value2 = o.ToString();
								}
								if (rowNumRange != null)
								{
									Excel.Range rngExact = worksheet.get_Range(rowNumRange, rowNumRange);
									rngExact = nextToDownRange(rngExact, j - rowCount);
									rngExact.Value2 = j+1;
								}
							}
						
						}
						#endregion
					}
					else if (dataView != null)
					{
						#region ...
						if (rowCount >= dataView[0].Count)
						{
							foreach(object[] oArr in colRanges)
							{
								Excel.Range rngExact = worksheet.get_Range(oArr[1],oArr[1]);
								rngExact.Value2 = "";
							}
							if (rowNumRange != null)
							{
								Excel.Range rngExact = worksheet.get_Range(rowNumRange, rowNumRange);
								rngExact.Value2 = "";
							}
						}
						else
						{
							for (int j = rowCount; j < rowCount+currentRowNum && j < dataView[0].Count; j++)
							{
								//rngTop.Copy(Type.Missing);
								//rngLeftBottom.Insert(Excel.XlInsertShiftDirection.xlShiftDown, Type.Missing);
								//rngLeftBottom = nextToDownRange(rngLeftTop);//rngTable.get_Range("A2","A2");
								foreach(object[] oArr in colRanges)
								{
									string colName = (string)oArr[0];
									object o = dataView[0][j][colName];
									if (o != DBNull.Value && objLookupFields != null && objLookupFields.ContainsKey(colName))
									{
										foreach (ListItem li in (ComboBox.ObjectCollection)objLookupFields[colName])
										{
											if (li.Value == (int)o)
											{
												o = li.Text;
												break;
											}
										}
									}
									Excel.Range rngExact = worksheet.get_Range(oArr[1],oArr[1]);
									rngExact = nextToDownRange(rngExact, j - rowCount);
									if (o is DateTime)
									{
										if (DateFormat == "")
											rngExact.Value2 = ((DateTime)o).ToShortDateString();
										else
											rngExact.Value2 = ((DateTime)o).ToString(DateFormat);
									}
									else
										rngExact.Value2 = o.ToString();
								}
								if (rowNumRange != null)
								{
									Excel.Range rngExact = worksheet.get_Range(rowNumRange, rowNumRange);
									rngExact = nextToDownRange(rngExact, j - rowCount);
									rngExact.Value2 = j+1;
								}
							}
						
						}
						#endregion
					}
					rowCount += currentRowNum;
				
					//if (rowCount >= dataSet.Tables[tableNames[0]].Rows.Count)
					//	continue;

					//rngTop.Delete(Excel.XlDeleteShiftDirection.xlShiftUp);
				}
			}
		}

		

		public string TemplateFilename
		{
			get 
			{
				return templateFilename;
			}
			set
			{
				templateFilename = value;
			}
		}


		private void AutoFitMergedCellRowHeight(Excel.Range Rng)
		{

			double mergedCellRgWidth = 0, rngHeight, rngWidth, possNewRowHeight;

			Rng = Rng.MergeArea;
			rngWidth = (double)((Excel.Range)Rng.Cells[1,1]).ColumnWidth;
			rngHeight= (double)((Excel.Range)Rng.Cells[1,1]).RowHeight;

			for(int i=1; i<=Rng.Columns.Count; i++)
				mergedCellRgWidth = (double)((Excel.Range)Rng.Cells[1, i]).Width + mergedCellRgWidth;

			mergedCellRgWidth = (mergedCellRgWidth*4/3 - 5) / 7;
			((Excel.Worksheet)Rng.Parent).Application.ScreenUpdating = false;
			Rng.MergeCells = false;
			((Excel.Range)Rng.Cells[1, 1]).ColumnWidth = mergedCellRgWidth;
			Rng.EntireRow.AutoFit();
			possNewRowHeight = (double)Rng.RowHeight;
			((Excel.Range)Rng.Cells[1, 1]).ColumnWidth = rngWidth;
			Rng.MergeCells = true;
			Rng.EntireRow.RowHeight = possNewRowHeight>rngHeight ? possNewRowHeight : rngHeight;
			((Excel.Worksheet)Rng.Parent).Application.ScreenUpdating = true;
		}
	}
}
