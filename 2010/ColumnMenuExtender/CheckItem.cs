﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

namespace ColumnMenuExtender
{
	/// <summary>
	/// Summary description for CheckItem.
	/// </summary>
	public class CheckItem : System.Windows.Forms.UserControl
	{
		public bool AllowNull { get; set; }
		private System.Windows.Forms.ImageList imageList1;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.ComponentModel.IContainer components;
		private object _value;
		private ImageSelector images;

		public CheckItem()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call
			images = new ImageSelector(imageList1);
			this.Value = Convert.DBNull;
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(CheckItem));
			this.imageList1 = new System.Windows.Forms.ImageList(this.components);
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.SuspendLayout();
			// 
			// imageList1
			// 
			this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
			this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
			this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
			// 
			// pictureBox1
			// 
			this.pictureBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.pictureBox1.Location = new System.Drawing.Point(0, 0);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(16, 16);
			this.pictureBox1.TabIndex = 0;
			this.pictureBox1.TabStop = false;
			this.pictureBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDown);
			// 
			// CheckItem
			// 
			this.BackColor = System.Drawing.Color.White;
			this.Controls.Add(this.pictureBox1);
			this.Name = "CheckItem";
			this.Size = new System.Drawing.Size(16, 16);
			this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.CheckItem_MouseDown);
			this.ResumeLayout(false);

		}
		#endregion


		public void NextValue()
		{
			if (Value is bool)
			{
				if (Value == Convert.DBNull)
					Value = true;
				else if ((bool)Value == true)
					Value = false;
				else
					Value = Convert.DBNull;
			}
			else if (Value is int)
			{
				if (Value == Convert.DBNull)
					Value = 1;
				else if ((int)Value == 1)
					Value = 0;
				else
					Value = Convert.DBNull;
			}
			else if (Value is System.DBNull)
			{
				Value = true;
			}
		}

		private void pictureBox1_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			NextValue();
		}

		private void CheckItem_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			NextValue();
		}

		public object Value
		{
			get 
			{
				return _value;
			}
			set 
			{
				pictureBox1.Image = Images[value];
				pictureBox1.Invalidate();
				_value = value;
			}
		}

		public ImageSelector Images
		{
			get {return images;}
		}


		public int ImageWidth
		{
			get {return pictureBox1.Width;}
		}

		public int ImageHeight
		{
			get {return pictureBox1.Height;}
		}
	}

	public class ImageSelector
	{
		private ImageList imageList;
		public ImageSelector(ImageList imageList)
		{
			this.imageList = imageList;
		}

		public Image this[Object obj]
		{
			get 
			{
				if (obj == Convert.DBNull)
					return imageList.Images[0];
				else if (obj is bool)
				{
					if ((bool)obj == true)
						return imageList.Images[1];
					else
						return imageList.Images[2];
				}
				else if (obj is int)
				{
					if ((int)obj == 1)
						return imageList.Images[1];
					else
						return imageList.Images[2];
				}
				else 
					throw new ApplicationException("Returned Object type must be bool or int!");
			}
		}
	}
}
