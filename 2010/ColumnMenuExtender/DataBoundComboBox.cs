﻿using System;
using System.Collections;
using System.Windows.Forms;
using System.Xml;

//using System.Collections;

namespace ColumnMenuExtender
{
	public class DataBoundComboBox : ComboBox
	{
		public string DataXml = "";
		public string PropName = "";
		public DataBoundComboBox()
		{
			DropDownStyle = ComboBoxStyle.DropDownList;
			ItemsHash = new Hashtable();
		}

		private new ComboBoxStyle DropDownStyle
		{
			set
			{
				base.DropDownStyle = value;
			}
		}
		public new int SelectedValue
		{
			get
			{
				if (SelectedItem != null)
					return ((ListItem)SelectedItem).Value;
				return -1;
			}
			set
			{
				if (SelectedValue == value) return;
				if (usingHash)
					SelectedItem = ItemsHash[value];
				else
				{
					var i = 0;
					foreach (ListItem item in Items)
					{
						if (item.Value == value)
						{
							SelectedIndex = i;
							break;
						}
						i++;
					}
				}
			}
		}

		/// <summary>
		/// Загружает lookup поля с именем propName из xml
		/// </summary>
		public void LoadXml(string xml, string propName)
		{
			if (xml != "" && propName != "")
			{
				DataXml = xml;
				PropName = propName;
				var xmlDoc = new XmlDocument();
				xmlDoc.LoadXml(xml);
				var nodes = xmlDoc.SelectNodes("/root/lookupField[@propName='" + propName + "']/item");
				Items.Clear();
				ItemsHash.Clear();
				foreach (XmlNode n in nodes)
				{
					var val = Convert.ToInt32(n.Attributes["ID"].Value);
					var li = new ListItem(val, n.InnerText);
					Items.Add(li);
					ItemsHash.Add(val, li);
				}
			}
			usingHash = true;
		}

		public void AddItems(ICollection ar)
		{
			foreach (ListItem li in ar)
			{
				Items.Add(li);
				ItemsHash.Add(li.Value, li);
			}
			usingHash = true;
		}
		public void AddItem(ListItem li)
		{
			Items.Add(li);
			ItemsHash[li.Value] = li;
		}

		public void InsertItem(int index, ListItem li)
		{
			Items.Insert(index, li);
			ItemsHash[li.Value] = li;
		}

		public Hashtable ItemsHash;
		private bool usingHash;

	}

	public class ListItem
	{
		public ListItem()
		{
			Value = 0;
			Text = "";
			OID = Guid.Empty;
		}

		public ListItem(int val, string text)
		{
			Value = val;
			Text = text;
			OID = Guid.Empty;
		}

		public ListItem(int val, string text, Guid oid)
		{
			Value = val;
			Text = text;
			OID = oid;
		}

		public override string ToString()
		{
			return Text;
		}

		public int Value { get; set; }

		public string Text { get; set; }

		public Guid OID { get; set; }

	    public object Tag { get; set; }
	}
}
