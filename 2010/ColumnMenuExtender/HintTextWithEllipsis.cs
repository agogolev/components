﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;
using MetaData;
using System.Data;

namespace ColumnMenuExtender
{
	public class HintTextWithEllipsis : TextBox
	{
		// Fields
		private Button btnEllipsis;
		private int xMargin;
		private int yMargin;
		private ArrayList data;
		private fmHint listBox;

		//Высота подсказки
		[DefaultValue(13)]
		public int ItemHeight
		{
			get { return listBox.ItemHeight; }
			set { listBox.ItemHeight = value; }
		}
		[DefaultValue(4)]
		public int HeightInHints
		{
			get { return listBox.HeightInHints; }
			set { listBox.HeightInHints = value; }
		}
		[DefaultValue(SortType.Alphabet)]
		public SortType Sort
		{
			get;
			set;
		}
		public Font HintFont
		{
			get
			{
				return listBox.Font;
			}
			set
			{
				listBox.Font = value;
			}
		}
		public object SelectedValue { get; set; }
		public string ValueMember { get; set; }
		public string StringMember { get; set; }
		public event EventHandler ClickButton;
		public HintTextWithEllipsis()
		{
			btnEllipsis = new Button();
			btnEllipsis.Visible = false;
			btnEllipsis.Text = "…";
			btnEllipsis.Size = new Size(20, 20);
			btnEllipsis.BackColor = SystemColors.Control;
			btnEllipsis.Click += btnEllipsis_Click;
			btnEllipsis.Visible = false;
			AjustMargin();
			listBox = new fmHint();
			data = new ArrayList();
			listBox.ItemSelect += new ItemSelectEventHandler(listBox_ItemSelect);

		}
		private void listBox_ItemSelect(object sender, ItemSelectEventArgs e)
		{
			this.Text = ((HintItem)e.SelectedItem).NK;
			this.SelectedValue = ((HintItem)e.SelectedItem).Value;
			this.SelectionStart = this.TextLength;
		}
		protected override void OnBorderStyleChanged(EventArgs e)
		{
			AjustMargin();
			base.OnBorderStyleChanged(e);
		}

		private void AjustMargin()
		{
			switch (BorderStyle)
			{
				case BorderStyle.Fixed3D:
					xMargin = 1;
					yMargin = 1;
					btnEllipsis.FlatStyle = FlatStyle.Standard;
					break;
				case BorderStyle.FixedSingle:
					xMargin = 1;
					yMargin = 0;
					btnEllipsis.FlatStyle = FlatStyle.Popup;
					break;
				case BorderStyle.None:
					xMargin = 0;
					yMargin = 0;
					btnEllipsis.FlatStyle = FlatStyle.Popup;
					break;
			}
		}

		private void btnEllipsis_Click(object sender, EventArgs e)
		{
			if (ClickButton != null)
			{
				ClickButton(this, e);
			}
			ShowHint();
		}
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				btnEllipsis.Dispose();
				data.Clear();
				listBox.Dispose();
			}
			base.Dispose(disposing);
		}
		#region Public Properties


		#endregion
		// Methods
		private void LayoutButton()
		{
			Rectangle rect = Bounds;
			btnEllipsis.Top = rect.Top - yMargin;
			btnEllipsis.Left = rect.Right + xMargin; // -this.btnEllipsis.Width;
			btnEllipsis.Height = rect.Height;
			if (btnEllipsis.Parent == null && Parent != null)
			{
				Parent.Controls.Add(btnEllipsis);
			}
		}
		private void HideBtnEllipsis()
		{
			if (btnEllipsis.Focused)
			{
				Focus();
			}
			else btnEllipsis.Visible = false;
		}
		private void ShowBtnEllipsis()
		{
			LayoutButton();
			btnEllipsis.Visible = true;
		}
		private void ShowHint()
		{
			if (this.Focused)
			{
				//int selStart = this.SelectionStart;
				//int selLen = this.SelectionLength;
				Point loc = this.Location;
				Point sl = this.PointToScreen(this.Location);
				loc.X = sl.X - loc.X;
				loc.Y = sl.Y - loc.Y + this.Size.Height;
				listBox.Location = loc;
				listBox.Width = this.Width;
				listBox.Show();
				//this.SelectionStart = selStart;
				//this.SelectionLength = selLen;
				this.Focus();
			}
		}
		public void SetDataBinding(DataSetISM dataSource)
		{
			try
			{
				listBox.Items.Clear();
				if (dataSource.Table.Rows.Count != 0)
				{
					foreach (DataRow dr in dataSource.Table.Rows)
					{
						listBox.Items.Add(new HintItem(dr[ValueMember], dr[StringMember].ToString()));
					}
				}
				else
				{
					listBox.Items.Add(new HintItem(null, ""));
				}
			}
			catch (Exception e)
			{
				throw new Exception("Ошибка привязки данных " + e.Message);
			}
		}
		protected override void OnLeave(EventArgs e)
		{
			HideBtnEllipsis();
			if (listBox.Visible)
				listBox.Hide();
			base.OnLeave(e);
		}
		protected override void OnEnter(EventArgs e)
		{
			ShowBtnEllipsis();
			base.OnEnter(e);
		}
		protected override void OnKeyDown(System.Windows.Forms.KeyEventArgs e)
		{
			if (listBox.Visible)
			{
				switch (e.KeyData)
				{
					case Keys.Up:
					case Keys.Down:
						SelectedValue = null;
						listBox.SendKey(e.KeyData);
						e.Handled = true;
						break;
					case Keys.Enter:
						if (listBox.SelectedIndex != -1)
						{
							listBox.SendKey(e.KeyData);
							e.Handled = true;
						}
						else
							btnEllipsis_Click(this, EventArgs.Empty);
						break;
					case Keys.Escape:
						listBox.Hide();
						break;
					default:
						SelectedValue = null;
						break;
				}
				
			}
			else
			{
				SelectedValue = null;
				if (e.KeyData == Keys.Enter)
					btnEllipsis_Click(this, EventArgs.Empty);
				
			}
			base.OnKeyDown(e);
		}
		class HintItem
		{
			public object Value;
			public string NK;

			public HintItem(object value, string data)
			{
				Value = value;
				NK = data;
			}

			public override string ToString()
			{
				return NK;
			}
		}
	}

}
