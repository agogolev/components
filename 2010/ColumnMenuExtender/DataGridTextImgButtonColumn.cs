﻿using System;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using System.ComponentModel;
using System.Diagnostics;
using System.Reflection;

namespace ColumnMenuExtender
{
	/// <summary>
	/// Summary description for DataGridButtonColumn.
	/// This class builds the Button column user control for datagrids for windows application. 
	/// It inherits DataGridTextBoxColumn, since this is already an available control in the .Net Framework.
	/// What needs to be modified, depending on the application, is the ValueChanged event. 
	/// To use this class, simply add it to the project, construct an instance, and use its properties
	/// this is a sample usage:
	/// 
	///	DataGridButtonColumn col4=new DataGridButtonColumn(10);
	///	col4.HeaderText="Text";
	///	col4.MappingName="Name";
	/// //hookup our cellbutton handler...
	///	col4.CellButtonClicked += 
	///	new DataGridCellButtonClickEventHandler(HandleCellButtonClick);
	///	style.GridColumnStyles.Add(col4);
	///	//hook the mouse handlers
	///	dataGrid1.MouseDown += new MouseEventHandler(col4.HandleMouseDown);
	///	dataGrid1.MouseUp += new MouseEventHandler(col4.HandleMouseUp);
	///	
	///	Trick is that you have to mention thecolumn number when creating an instance
	///	of the class, because the event handlers depend on the position of the cell
	///	
	/// </summary>
	public class DataGridTextImgButtonColumn : DataGridTextBoxColumn
	{
		public event DataGridCellButtonClickEventHandler CellButtonClicked;

		private Bitmap _buttonFace;
		private Bitmap _buttonFacePressed;
		//private int _columnNum = -1;
		private int _pressedRow = -1;

		public DataGridTextImgButtonColumn()//(int colNum)
		{
			//_columnNum = colNum;
			_pressedRow = -1;

			/*try
			{
				char[] c={','};
				System.IO.Stream strm = this.GetType().Assembly.GetManifestResourceStream(this.GetType().Assembly.ToString().Split(c,10)[0]+".button.bmp");
				_button = new Bitmap(strm);
				strm = this.GetType().Assembly.GetManifestResourceStream(this.GetType().Assembly.ToString().Split(c,10)[0]+".buttonpressed.bmp");
				_buttonPressed = new Bitmap(strm);
			}
			catch{}*/
		}

		public void SetBitmaps(Bitmap Face, Bitmap FacePressed)
		{
			_buttonFace = Face;
			_buttonFacePressed = FacePressed;
			base.Width = Face.Width > FacePressed.Width ? Face.Width : FacePressed.Width;
		}

		protected override void Edit(System.Windows.Forms.CurrencyManager source, int rowNum, System.Drawing.Rectangle bounds, bool readOnly, string instantText, bool cellIsVisible) 
		{ 
			// dont call the baseclass so no editing done...
			//	base.Edit(source, rowNum, bounds, readOnly, instantText, cellIsVisible); 
		}

		public override int Width
		{
			get
			{
				return base.Width;
			}
			set
			{
				//base.Width = value;
			}
		}

		private void DrawButton(Graphics g, Bitmap bm, Rectangle bounds, int row)
		{
			DataGrid dg = this.DataGridTableStyle.DataGrid;
			int columnNum = this.DataGridTableStyle.GridColumnStyles.IndexOf(this);
			string s = dg[row, columnNum].ToString();

			SizeF sz = g.MeasureString(s, dg.Font, bounds.Width - 4, StringFormat.GenericTypographic);

			int x = bounds.Left + Math.Max(0, (bounds.Width - (int)sz.Width)/2);

			//нарисуем каринку по центру области
			int wx = Math.Max(0, ((bounds.Right-bounds.Left)-bm.Width)/2);
			int wy = Math.Max(0, ((bounds.Bottom-bounds.Top)-bm.Height)/2);

			int width = bm.Width;
			if (bounds.Left+wx+bm.Width > bounds.Right)
				width = bounds.Right - (bounds.Left+wx);

			int height = bm.Height;
			if (bounds.Top+wy+bm.Height > bounds.Bottom)
				height = bounds.Bottom - (bounds.Top+wy);
			
			Rectangle r = new Rectangle(bounds.Left+wx, bounds.Top+wy, width, height);

			g.DrawImage(bm, r, 0, 0, bm.Width, bm.Height, GraphicsUnit.Pixel);
			//g.DrawImage(bm, bounds, 0, 0, bm.Width, bm.Height, GraphicsUnit.Pixel);
			
			if(sz.Height < bounds.Height)
			{
				int y = bounds.Top + (bounds.Height - (int) sz.Height) / 2;
				if(_buttonFacePressed == bm)
				{
					x++;
				}

				g.DrawString(s, dg.Font, new SolidBrush(dg.ForeColor), x, y);
			}
		}

		public void HandleMouseUp(object sender, MouseEventArgs e)
		{
			DataGrid dg = this.DataGridTableStyle.DataGrid;
			DataGrid.HitTestInfo hti = dg.HitTest(new Point(e.X, e.Y));
			int columnNum = this.DataGridTableStyle.GridColumnStyles.IndexOf(this);
			bool isClickInCell = (hti.Column == columnNum
				&& hti.Row > -1);

			_pressedRow = -1;

			Rectangle rect = new Rectangle(0,0,0,0);

			if(isClickInCell)
			{
				rect = dg.GetCellBounds(hti.Row, hti.Column);
				isClickInCell = (e.X > rect.Right - this._buttonFace.Width);
			}
			if(isClickInCell)
			{
				Graphics g = Graphics.FromHwnd(dg.Handle);
				//	g.DrawImage(this._buttonFace, rect.Right - this._buttonFace.Width, rect.Y);
				DrawButton(g, this._buttonFace, rect, hti.Row);
				g.Dispose();
				if(CellButtonClicked != null)
					CellButtonClicked(this, new DataGridCellButtonClickEventArgs(hti.Row, hti.Column));
			}
		}

		public void HandleMouseDown(object sender, MouseEventArgs e)
		{
			DataGrid dg = this.DataGridTableStyle.DataGrid;
			DataGrid.HitTestInfo hti = dg.HitTest(new Point(e.X, e.Y));
			int columnNum = this.DataGridTableStyle.GridColumnStyles.IndexOf(this);
			bool isClickInCell = (hti.Column == columnNum
				&& hti.Row > -1);

			Rectangle rect = new Rectangle(0,0,0,0);
			if(isClickInCell)
			{
				rect = dg.GetCellBounds(hti.Row, hti.Column);
				isClickInCell = (e.X > rect.Right - this._buttonFace.Width);
			}

			if(isClickInCell)
			{
				//Console.WriteLine("HandleMouseDown " + hti.Row.ToString());
				Graphics g = Graphics.FromHwnd(dg.Handle);
				//g.DrawImage(this._buttonFacePressed, rect.Right - this._buttonFacePressed.Width, rect.Y);
				DrawButton(g, _buttonFacePressed, rect, hti.Row);
				g.Dispose();
				_pressedRow = hti.Row;
			}
		}

		protected override void Paint(System.Drawing.Graphics g, System.Drawing.Rectangle bounds, System.Windows.Forms.CurrencyManager source, int rowNum, System.Drawing.Brush backBrush, System.Drawing.Brush foreBrush, bool alignToRight)
		{
			//base.Paint(g, bounds, source, rowNum, backBrush, foreBrush, alignToRight);
	
			DataGrid parent = this.DataGridTableStyle.DataGrid;
			int columnNum = this.DataGridTableStyle.GridColumnStyles.IndexOf(this);
			bool current = parent.IsSelected(rowNum) ||
				( parent.CurrentRowIndex == rowNum && 
				parent.CurrentCell.ColumnNumber == columnNum);

			g.FillRectangle(backBrush, bounds);
		
			//draw the button
			Bitmap bm = _pressedRow == rowNum ? this._buttonFacePressed : this._buttonFace;
			this.DrawButton(g, bm, bounds, rowNum);
			
			//font.Dispose();
		
		}

		public delegate void DataGridCellButtonClickEventHandler(object sender, DataGridCellButtonClickEventArgs e);

		public class DataGridCellButtonClickEventArgs : EventArgs
		{
			private int _row;
			private int _col;

			public DataGridCellButtonClickEventArgs(int row, int col)
			{
				_row = row;
				_col = col;
			}

			public int RowIndex	{get{return _row;}}
			public int ColIndex	{get{return _col;}}
		}
	}
}