﻿using System;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using System.ComponentModel;
using System.Diagnostics;
using System.Reflection;

namespace ColumnMenuExtender
{
	public delegate System.Drawing.Brush GetColorForRowExt(object sender, int rowNum, System.Windows.Forms.CurrencyManager source);
	public class FormattableTextBoxColumnExt : DataGridTextBoxColumn, IExtendedQuery
	{	
		private bool _allowExpand = false;
		[DefaultValue(false)]
		public bool AllowExpand {
			get {
				return _allowExpand;
			}
			set {
				_allowExpand = value;
			}
		}
		//Для того чтобы определить метод, определяющий цвет строки (например, если какое-то значение строки больше чего-либо - цвет бэкграунда строки розовый)
		
		public event GetColorForRowExt OnGetColorForRow;
		public event GetColorForRowExt OnGetForeColorForRow;
	
		public event FormatCellEventHandler SetCellFormat;
		private bool allowPoint;
		private int efforts;
		/// <summary>
		/// Remember how many times the GetMinimumHeight method is called.
		/// </summary>
		protected int currentIteration = 0;

		public FormattableTextBoxColumnExt()
		{
			this.TextBox.TextChanged += new EventHandler(TextBox_TextChanged);
			allowPoint = true;
			efforts = 0;
		}

		//used to fire an event to retrieve formatting info
		//and then draw the cell with this formatting info
		protected override void Paint(System.Drawing.Graphics g, System.Drawing.Rectangle bounds, System.Windows.Forms.CurrencyManager source, int rowNum, System.Drawing.Brush backBrush, System.Drawing.Brush foreBrush, bool alignToRight)
		{
			System.Drawing.Brush backBr = backBrush;			
			System.Drawing.Brush foreBr = foreBrush;			
			DataGridFormatCellEventArgs e = new DataGridFormatCellEventArgs(this.GetColumnValueAtRow(source, rowNum), source.List[rowNum]);

			bool isNewBrush = SetCellColor(rowNum, ref backBrush, source);
			if(!isNewBrush) {
				if(backBrush != backBr) {
					backBrush.Dispose();
					backBrush = backBr;
					isNewBrush = false;
				}
			}
			bool isNewForeBrush = SetCellForeColor(rowNum, ref foreBrush, source);
			if(!isNewForeBrush) {
				if(foreBrush != foreBr) {
					foreBrush.Dispose();
					foreBrush = foreBr;
					isNewForeBrush = false;
				}
			}

			//fire the formatting event
			if(SetCellFormat != null) {
				SetCellFormat(this, e);
				if(e.BackBrush != null) {
					backBrush.Dispose();
					backBrush = e.BackBrush;
					isNewBrush = false;
				}
				if(e.ForeBrush != null) {
					foreBrush.Dispose();
					foreBrush = e.ForeBrush;	
					isNewForeBrush = false;
				}
			}
			if(((SolidBrush)backBrush).Color == this.DataGridTableStyle.SelectionBackColor) {
				if(backBrush != backBr) {
					backBrush.Dispose();
					backBrush = backBr;
					isNewBrush = false;
				}
			}
			if(((SolidBrush)foreBrush).Color == this.DataGridTableStyle.SelectionForeColor) {
				if(foreBrush != foreBr) {
					foreBrush.Dispose();
					foreBrush = foreBr;
					isNewForeBrush = false;
				}
			}
			if(e.TextFont == null)
				e.TextFont = this.DataGridTableStyle.DataGrid.Font;
			g.FillRectangle(backBrush, bounds);
			Region saveRegion = g.Clip;
			Rectangle rect = new Rectangle(bounds.X, bounds.Y, bounds.Width, bounds.Height);
			using(Region newRegion = new Region(rect)) {
				g.Clip = newRegion;

				string s = null;
				if(Format == "") 
					s = this.GetColumnValueAtRow(source, rowNum).ToString();
				else {
					object o = this.GetColumnValueAtRow(source, rowNum);
					if(o is DateTime) s = ((DateTime)o).ToString(Format, FormatInfo);
					else if(o is int) s = ((int)o).ToString(Format, FormatInfo);
					else if(o is Int64) s = ((Int64)o).ToString(Format, FormatInfo);
					else if(o is decimal) s = ((decimal)o).ToString(Format, FormatInfo);
					else if(o is double) s = ((double)o).ToString(Format, FormatInfo);
					else if(o is float) s = ((float)o).ToString(Format, FormatInfo);
				}

				try {
					g.DrawString(s, e.TextFont, foreBrush, bounds.X, bounds.Y + 2);
				}
				catch(Exception ex) {
					Console.WriteLine(ex.Message.ToString());
				} //empty catch
				finally {
					g.Clip = saveRegion;
				}
			}
			//clean up
			if(e != null)
			{
				if(e.BackBrushDispose)
					e.BackBrush.Dispose();
				if(e.ForeBrushDispose)
					e.ForeBrush.Dispose();
				if(e.TextFontDispose)
					e.TextFont.Dispose();
			}
			if(isNewBrush)
				backBrush.Dispose();
			if(isNewForeBrush)
				foreBrush.Dispose();
			
		}

		private bool SetCellColor(int rowNum, ref System.Drawing.Brush backBrush, System.Windows.Forms.CurrencyManager source)
		{
			bool isNewBrush = false;

			if (!(DataGridTableStyle.DataGrid is ExtendedDataGrid))
				return false;
			ExtendedDataGrid dataGrid = (ExtendedDataGrid)DataGridTableStyle.DataGrid;
			DataTable RowColorsTable = dataGrid.RowColorsTable;
			//число строк в этой таблицe
			int rowColorCount = (dataGrid.AllowCustomRowColorList ? RowColorsTable.Rows.Count : 0);

			if (rowColorCount > 0)
			{ //если были указаны цвета для отображения грида
				//в последней строкие таблицы RowColorsTable хранится общее число строк,
				//для которых указана цветновая последовательность. После этой строки начинается повторение				
				//rowsInBatch - число строк в гриде, после которых начинается повторение цветновой последовательности
				int rowsInBatch = (int)RowColorsTable.Rows[rowColorCount - 1]["position"];
				//номер строки в текущем блоке(rowsInBatch)
				int rowColorIndex = rowNum % rowsInBatch + 1;
				if (rowColorIndex == 1) //если начинается прорисовка нового блока, обнуляем позицию строки в таблице RowColorsTable					
					RowColorsTable.ExtendedProperties["currentRowColorsPosition"] = 0;

				//текущая позиция строки в таблице RowColorsTable
				//требуется для того, чтобы несколько раз напрасно не пробегать по таблице  RowColorsTable в поисках требцемой строки
				//т.к. строки которые уже были ипользованы, не будут использоваться до прорисовки следующего блока
				int currentRowColorsPosition = (int)RowColorsTable.ExtendedProperties["currentRowColorsPosition"];
				//ищем строку в которой указан цвет, рисуемой строки
				for (int i = currentRowColorsPosition; i < rowColorCount; i++)
				{
					if (((int)RowColorsTable.Rows[i]["position"]) >= rowColorIndex)
					{
						if (((SolidBrush)backBrush).Color != this.DataGridTableStyle.SelectionBackColor)
						{
							backBrush = new SolidBrush((Color)RowColorsTable.Rows[i]["color"]);
							isNewBrush = true;
						}
						RowColorsTable.ExtendedProperties["currentRowColorsPosition"] = i;
						break;
					}
				}
			}
			else if (OnGetColorForRow != null)
			{//был определён делегат для выбора цвета
				if (((SolidBrush)backBrush).Color != this.DataGridTableStyle.SelectionBackColor)
				{
					System.Drawing.Brush backBr = OnGetColorForRow(this, rowNum, source);
					if (backBr != null)
					{
						backBrush = backBr;
						isNewBrush = true;
					}
				}
			}
			else
			{
				if (((SolidBrush)backBrush).Color == this.DataGridTableStyle.SelectionBackColor)
					return false;
				if ((this.DataGridTableStyle.DataGrid is ExtendedDataGrid))
				{
					System.Drawing.Brush backBr = (this.DataGridTableStyle.DataGrid as ExtendedDataGrid).FireEventGetColorForRow(rowNum, source);
					if (backBr != null)
					{
						backBrush = backBr;
						isNewBrush = true;
					}
				}
			}
			return isNewBrush;
		}
		private bool SetCellForeColor(int rowNum, ref System.Drawing.Brush foreBrush, System.Windows.Forms.CurrencyManager source)
		{
			if (((SolidBrush)foreBrush).Color == this.DataGridTableStyle.SelectionForeColor)
				return false;
			bool isNewBrush = false;

			if (OnGetForeColorForRow != null)
			{//был определён делегат для выбора цвета
				System.Drawing.Brush foreBr = OnGetForeColorForRow(this, rowNum, source);
				if (foreBr != null)
				{
					foreBrush = foreBr;
					isNewBrush = true;
				}
			}
			else
			{
				if ((this.DataGridTableStyle.DataGrid is ExtendedDataGrid))
				{
					System.Drawing.Brush foreBr = (this.DataGridTableStyle.DataGrid as ExtendedDataGrid).FireEventGetForeColorForRow(rowNum, source);
					if (foreBr != null)
					{
						foreBrush = foreBr;
						isNewBrush = true;
					}
				}
			}
			return isNewBrush;
		}

		protected override void Edit(CurrencyManager source, int rowNum, Rectangle bounds, bool readOnly, string instantText, bool cellIsVisible)
		{
			if (!(readOnly || this.ReadOnly))
				base.Edit(source, rowNum, bounds, readOnly, instantText, cellIsVisible);
			else
			{
				int cri = this.DataGridTableStyle.DataGrid.CurrentRowIndex;
				if (cri > 0)
					this.DataGridTableStyle.DataGrid.UnSelect(cri-1);
				this.DataGridTableStyle.DataGrid.Select(cri);
				if (cri < this.DataGridTableStyle.DataGrid.VisibleRowCount-1)
					this.DataGridTableStyle.DataGrid.UnSelect(cri+1);
			}
		}

		#region Public Properties

		private string fieldName;
		private string filterFieldName;

		[DefaultValue(typeof(string), ""), Category("Misc")]
		public string FieldName
		{
			set 
			{
				fieldName = value;
			}
			get
			{
				return fieldName;
			}
		}

		[DefaultValue(typeof(string), ""), Category("Misc")]
		public string FilterFieldName
		{
			set 
			{
				filterFieldName = value;
			}
			get
			{
				return filterFieldName;
			}
		}

		[Browsable(true), DefaultValue(true)]
		public bool AllowPoint
		{
			get{ return allowPoint;}
			set{ allowPoint = value;}
		}

		#endregion

		private void TextBox_TextChanged(object sender, EventArgs e)
		{			
			if(!allowPoint)
				return;
			//Здесь происходит подмена знака разделителя дробной части
			//в числе с плавающей точкой. Чтобы можно было вводить значения, как 
			//с точкой так и с запятой
			DataGrid dg = this.DataGridTableStyle.DataGrid;
			if(dg == null) return;
			
			DataTable dt = null;
//			DataView dv = null;			
//			DataSet ds = dg.DataSource as DataSet;
//			if(ds == null) {
//				dv = dg.DataSource as DataView;
//				if(dv == null) return;
//				dt = dv.Table;
//			}
//			else
//				dt = ds.Tables[dg.DataMember];

			if (dg.DataSource is DataSet)
				dt = ((DataSet)dg.DataSource).Tables[dg.DataMember];
			else if (dg.DataSource is DataView)
				dt = ((DataView)dg.DataSource).Table;
			else if (dg.DataSource is DataTable)
				dt = (DataTable)dg.DataSource;
			else return;

			if(dt == null) return;
			DataColumn dc = dt.Columns[MappingName];
			if(dc == null) return;

			Type columnType = dc.DataType;

			if(columnType == typeof(Decimal))
			{
				decimal tmp;
				try 
				{ 
					if(efforts < 2)
						tmp = Convert.ToDecimal(TextBox.Text); 
					else
						efforts = 0;
				}
				catch 
				{
					efforts++;
					int selectionStart = TextBox.SelectionStart;
					if(TextBox.Text.IndexOf('.') != -1)
						TextBox.Text = TextBox.Text.Replace('.',',');
					else
						TextBox.Text = TextBox.Text.Replace(',','.');

					TextBox.SelectionStart = selectionStart;
					//TextBox.SelectionStart + TextBox.SelectionLength
					return;
				} 
			}
		}

		/// <summary>
		/// Reset the iteration count to 0.
		/// </summary>
		public void ResetIterations()
		{
			currentIteration = 0;
		}

		/// <summary>
		/// The actual work is done in this method. 
		/// - First verify if there are rows, find out from CurrencyManager
		/// - increment iteration count
		/// - calculate height of cell at iterationCount - 1
		/// - if iterationCount is rowcount, reset the iterationcount
		/// - return
		/// </summary>
		/// <returns>The height of the requested row.</returns>
		protected override int GetMinimumHeight()
		{	
			if(!AllowExpand) return base.GetMinimumHeight();

			ExtendedDataGrid dg = this.DataGridTableStyle.DataGrid as ExtendedDataGrid;
			if(dg == null || (dg != null && !dg.AutoRowHeight))	return base.GetMinimumHeight();

			try
			{
				StackFrame frame = new StackFrame(4);
				MethodBase method = frame.GetMethod();
				string s = method.DeclaringType.FullName;
				if(s.EndsWith("DataGridAddNewRow"))
				{
					this.ResetIterations();
					return base.GetMinimumHeight();
				}
				else
				{
				
					CurrencyManager cur = (CurrencyManager)this.DataGridTableStyle.DataGrid.BindingContext
						[this.DataGridTableStyle.DataGrid.DataSource,this.DataGridTableStyle.DataGrid.DataMember];
					if(cur == null || cur.Count == 0)
						return base.GetMinimumHeight();
					
					this.currentIteration++;
			
					int rowNum = dg.TableStyles[0].GridColumnStyles.IndexOf(this);
					int retVal = base.GetMinimumHeight();

					int stringHeight = this.CalcStringHeight(GetColumnValueAtRow(cur, currentIteration - 1).ToString());
					if(currentIteration ==	cur.Count)
						this.ResetIterations();	
					return retVal > stringHeight ? retVal : stringHeight;
				}
			}
			catch
			{
				// Error, return default
				this.ResetIterations();	
				return base.GetMinimumHeight();
			}
		}

		/// <summary>
		/// Calculates the height of the supplied string.
		/// </summary>
		/// <param name="s"></param>
		/// <returns></returns>
		private int CalcStringHeight(string s)
		{
			try
			{
				// Create graphics for calculation
				System.Drawing.Graphics g = this.TextBox.CreateGraphics();
				// Do measure, and add a bit (4 pixels for me)
				return (int)g.MeasureString(s.Trim(), this.TextBox.Font, this.Width).Height + 4;
			}
			catch
			{
				// Error, return default font height.
				return base.GetMinimumHeight();
			}
		}
	}
}