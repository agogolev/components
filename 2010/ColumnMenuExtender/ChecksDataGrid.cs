﻿using System;
using System.Data;
using System.Drawing;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing.Design;
using System.Windows.Forms;

namespace ColumnMenuExtender 
{
	public class ChecksDataGrid : DataGrid
	{
		private bool isCellClicked = false;
		//private int colNum = 0;

		public ChecksDataGrid()
		{
			
		}

		public void DisableNewDel()
		{
			CurrencyManager cmanager1 = (CurrencyManager)BindingContext[this.DataSource, this.DataMember];
			DataView dview1 = (DataView)cmanager1.List;
			dview1.AllowNew = false;
			dview1.AllowDelete = false;
		}

		/// <summary>
		/// возвращает ссылку на строку DataTable связанную с текущей выделенной строкой
		/// </summary>		
		public DataRow GetSelectedRow()
		{
			CurrencyManager cm = (CurrencyManager) this.BindingContext[this.DataSource, this.DataMember];
			if(cm.Position != -1)
			{
				DataRowView rv = (DataRowView)cm.Current;
				return rv.Row;
			}
			else
				return null;
		}
		
		[Editor(typeof(TableStylesCollectionEditor), typeof(UITypeEditor))]
		public new GridTableStylesCollection TableStyles
		{
			get
			{
				return base.TableStyles;
			}
		}



		public class TableStylesCollectionEditor : CollectionEditor
		{
			// Methods
			public TableStylesCollectionEditor(Type type) : base (type)
			{
			}

			protected override Type[] CreateNewItemTypes()
			{
				return new Type[1] { typeof(ChecksDataGridTableStyle) } ;
			}
		}

		public bool IsCellClicked
		{
			get {return isCellClicked;}
		}

		protected override void OnMouseDown(MouseEventArgs e)
		{
			DataGrid.HitTestInfo hti = this.HitTest(e.X, e.Y);
			if (hti.Type == HitTestType.Cell)
				isCellClicked = true;
			base.OnMouseDown (e);
			isCellClicked = false;
		}
	}
}
