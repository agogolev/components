﻿using System;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using System.ComponentModel;
using System.Diagnostics;
using System.Reflection;

namespace ColumnMenuExtender
{
	public delegate System.Drawing.Brush GetColorForRow(int rowNum, System.Windows.Forms.CurrencyManager source);
	public class FormattableTextBoxColumn : DataGridTextBoxColumn, IExtendedQuery
	{
		private string oldText;
		private bool isEditing = false;
		private bool _allowExpand = false;
		[DefaultValue(false)]
		public bool AllowExpand
		{
			get
			{
				return _allowExpand;
			}
			set
			{
				_allowExpand = value;
			}
		}
		//ƒл¤ того чтобы определить метод, определ¤ющий цвет строки (например, если какое-то значение строки больше чего-либо - цвет бэкграунда строки розовый)

		public event GetColorForRow OnGetColorForRow;
		public event GetColorForRow OnGetForeColorForRow;

		public event FormatCellEventHandler SetCellFormat;
		public event ValidateCellEventHandler CommitCell;
		/// <summary>
		/// Remember how many times the GetMinimumHeight method is called.
		/// </summary>
		protected int currentIteration = 0;

		public FormattableTextBoxColumn()
		{
			//this.TextBox.TextChanged += new EventHandler(TextBox_TextChanged);
		}

		//used to fire an event to retrieve formatting info
		//and then draw the cell with this formatting info
		protected override void Paint(System.Drawing.Graphics g, System.Drawing.Rectangle bounds, System.Windows.Forms.CurrencyManager source, int rowNum, System.Drawing.Brush backBrush, System.Drawing.Brush foreBrush, bool alignToRight)
		{
			System.Drawing.Brush backBr = backBrush;
			System.Drawing.Brush foreBr = foreBrush;
			DataGridFormatCellEventArgs e = new DataGridFormatCellEventArgs(this.GetColumnValueAtRow(source, rowNum), source.List[rowNum]);

			bool isNewBrush = SetCellColor(rowNum, ref backBrush, source);
			if (!isNewBrush)
			{
				if (backBrush != backBr)
				{
					backBrush.Dispose();
					backBrush = backBr;
					isNewBrush = false;
				}
			}
			bool isNewForeBrush = SetCellForeColor(rowNum, ref foreBrush, source);
			if (!isNewForeBrush)
			{
				if (foreBrush != foreBr)
				{
					foreBrush.Dispose();
					foreBrush = foreBr;
					isNewForeBrush = false;
				}
			}

			//fire the formatting event
			if (SetCellFormat != null)
			{
				SetCellFormat(this, e);
				if (e.BackBrush != null)
				{
					backBrush.Dispose();
					backBrush = e.BackBrush;
					isNewBrush = false;
				}
				if (e.ForeBrush != null)
				{
					foreBrush.Dispose();
					foreBrush = e.ForeBrush;
					isNewForeBrush = false;
				}
			}
			if (((SolidBrush)backBrush).Color == this.DataGridTableStyle.SelectionBackColor)
			{
				if (backBrush != backBr)
				{
					backBrush.Dispose();
					backBrush = backBr;
					isNewBrush = false;
				}
			}
			if (((SolidBrush)foreBrush).Color == this.DataGridTableStyle.SelectionForeColor)
			{
				if (foreBrush != foreBr)
				{
					foreBrush.Dispose();
					foreBrush = foreBr;
					isNewForeBrush = false;
				}
			}
			if (e.TextFont == null)
				e.TextFont = this.DataGridTableStyle.DataGrid.Font;
			g.FillRectangle(backBrush, bounds);
			Region saveRegion = g.Clip;
			Rectangle rect = new Rectangle(bounds.X, bounds.Y, bounds.Width, bounds.Height);
			using (Region newRegion = new Region(rect))
			{
				g.Clip = newRegion;

				string s = null;
				if (Format == "" && this.GetColumnValueAtRow(source, rowNum) != null)
					s = this.GetColumnValueAtRow(source, rowNum).ToString();
				else
				{
					object o = this.GetColumnValueAtRow(source, rowNum);
					if (o is DateTime) s = ((DateTime)o).ToString(Format, FormatInfo);
					else if (o is int) s = ((int)o).ToString(Format, FormatInfo);
					else if (o is Int64) s = ((Int64)o).ToString(Format, FormatInfo);
					else if (o is decimal) s = ((decimal)o).ToString(Format, FormatInfo);
					else if (o is double) s = ((double)o).ToString(Format, FormatInfo);
					else if (o is float) s = ((float)o).ToString(Format, FormatInfo);
				}

				try
				{
					g.DrawString(s, e.TextFont, foreBrush, bounds.X, bounds.Y + 2);
				}
				catch (Exception ex)
				{
					Console.WriteLine(ex.Message.ToString());
				} //empty catch
				finally
				{
					g.Clip = saveRegion;
				}
			}
			//clean up
			if (e != null)
			{
				if (e.BackBrushDispose)
					e.BackBrush.Dispose();
				if (e.ForeBrushDispose)
					e.ForeBrush.Dispose();
				if (e.TextFontDispose)
					e.TextFont.Dispose();
			}
			if (isNewBrush)
				backBrush.Dispose();
			if (isNewForeBrush)
				foreBrush.Dispose();

		}

		private bool SetCellColor(int rowNum, ref System.Drawing.Brush backBrush, System.Windows.Forms.CurrencyManager source)
		{
			bool isNewBrush = false;

			if (!(DataGridTableStyle.DataGrid is ExtendedDataGrid))
				return false;
			ExtendedDataGrid dataGrid = (ExtendedDataGrid)DataGridTableStyle.DataGrid;
			DataTable RowColorsTable = dataGrid.RowColorsTable;
			//число строк в этой таблицe
			int rowColorCount = (dataGrid.AllowCustomRowColorList ? RowColorsTable.Rows.Count : 0);

			if (rowColorCount > 0)
			{ //если были указаны цвета дл¤ отображени¤ грида
				//в последней строкие таблицы RowColorsTable хранитс¤ общее число строк,
				//дл¤ которых указана цветнова¤ последовательность. ѕосле этой строки начинаетс¤ повторение				
				//rowsInBatch - число строк в гриде, после которых начинаетс¤ повторение цветновой последовательности
				int rowsInBatch = (int)RowColorsTable.Rows[rowColorCount - 1]["position"];
				//номер строки в текущем блоке(rowsInBatch)
				int rowColorIndex = rowNum % rowsInBatch + 1;
				if (rowColorIndex == 1) //если начинаетс¤ прорисовка нового блока, обнул¤ем позицию строки в таблице RowColorsTable					
					RowColorsTable.ExtendedProperties["currentRowColorsPosition"] = 0;

				//текуща¤ позици¤ строки в таблице RowColorsTable
				//требуетс¤ дл¤ того, чтобы несколько раз напрасно не пробегать по таблице  RowColorsTable в поисках требцемой строки
				//т.к. строки которые уже были ипользованы, не будут использоватьс¤ до прорисовки следующего блока
				int currentRowColorsPosition = (int)RowColorsTable.ExtendedProperties["currentRowColorsPosition"];
				//ищем строку в которой указан цвет, рисуемой строки
				for (int i = currentRowColorsPosition; i < rowColorCount; i++)
				{
					if (((int)RowColorsTable.Rows[i]["position"]) >= rowColorIndex)
					{
						if (((SolidBrush)backBrush).Color != this.DataGridTableStyle.SelectionBackColor)
						{
							backBrush = new SolidBrush((Color)RowColorsTable.Rows[i]["color"]);
							isNewBrush = true;
						}
						RowColorsTable.ExtendedProperties["currentRowColorsPosition"] = i;
						break;
					}
				}
			}
			else if (OnGetColorForRow != null)
			{//был определЄн делегат дл¤ выбора цвета
				if (((SolidBrush)backBrush).Color != this.DataGridTableStyle.SelectionBackColor)
				{
					System.Drawing.Brush backBr = OnGetColorForRow(rowNum, source);
					if (backBr != null)
					{
						backBrush = backBr;
						isNewBrush = true;
					}
				}
			}
			else
			{
				if (((SolidBrush)backBrush).Color == this.DataGridTableStyle.SelectionBackColor)
					return false;
				if ((this.DataGridTableStyle.DataGrid is ExtendedDataGrid))
				{
					System.Drawing.Brush backBr = (this.DataGridTableStyle.DataGrid as ExtendedDataGrid).FireEventGetColorForRow(rowNum, source);
					if (backBr != null)
					{
						backBrush = backBr;
						isNewBrush = true;
					}
				}
			}
			return isNewBrush;
		}
		private bool SetCellForeColor(int rowNum, ref System.Drawing.Brush foreBrush, System.Windows.Forms.CurrencyManager source)
		{
			if (((SolidBrush)foreBrush).Color == this.DataGridTableStyle.SelectionForeColor)
				return false;
			bool isNewBrush = false;

			if (OnGetForeColorForRow != null)
			{//был определЄн делегат дл¤ выбора цвета
				System.Drawing.Brush foreBr = OnGetForeColorForRow(rowNum, source);
				if (foreBr != null)
				{
					foreBrush = foreBr;
					isNewBrush = true;
				}
			}
			else
			{
				if ((this.DataGridTableStyle.DataGrid is ExtendedDataGrid))
				{
					System.Drawing.Brush foreBr = (this.DataGridTableStyle.DataGrid as ExtendedDataGrid).FireEventGetForeColorForRow(rowNum, source);
					if (foreBr != null)
					{
						foreBrush = foreBr;
						isNewBrush = true;
					}
				}
			}
			return isNewBrush;
		}


		protected override void Edit(CurrencyManager source, int rowNum, Rectangle bounds, bool readOnly, string instantText, bool cellIsVisible)
		{
			if (!(readOnly || this.ReadOnly))
			{
				base.Edit(source, rowNum, bounds, readOnly, instantText, cellIsVisible);
				oldText = this.TextBox.Text;
				this.TextBox.TextChanged += new EventHandler(TextBox_TextChanged);
			}
			else
			{
				int cri = this.DataGridTableStyle.DataGrid.CurrentRowIndex;
				if (cri > 0)
					this.DataGridTableStyle.DataGrid.UnSelect(cri - 1);
				this.DataGridTableStyle.DataGrid.Select(cri);
				if (cri < this.DataGridTableStyle.DataGrid.VisibleRowCount - 1)
					this.DataGridTableStyle.DataGrid.UnSelect(cri + 1);
			}
		}
		protected override void Abort(int rowNum)
		{
			isEditing = false;
			TextBox.TextChanged -= new EventHandler(TextBox_TextChanged);
			base.Abort(rowNum);
		}

		protected override bool Commit(CurrencyManager dataSource, int rowNum)
		{

			TextBox.TextChanged -= new EventHandler(TextBox_TextChanged);
			if (!isEditing)
				return true;
			isEditing = false;

			try
			{
				object text = TextBox.Text;
				if (this.NullText.Equals(text))
				{
					text = Convert.DBNull;
					//TextBox.Text = this.NullText;
				}

				if (CommitCell != null)
				{
					DataGridValidateCellEventArgs e = new DataGridValidateCellEventArgs(dataSource, rowNum, text);
					CommitCell(this, e);
					if (!e.IsCommit)
						throw new Exception();
				}
				return base.Commit(dataSource, rowNum);

			}
			catch (Exception)
			{
				this.TextBox.Text = oldText;
				Abort(rowNum);
				return false;
			}
		}

		#region Public Properties

		private string fieldName;
		private string filterFieldName;

		[DefaultValue(typeof(string), ""), Category("Misc")]
		public string FieldName
		{
			set
			{
				fieldName = value;
			}
			get
			{
				return fieldName;
			}
		}

		[DefaultValue(typeof(string), ""), Category("Misc")]
		public string FilterFieldName
		{
			set
			{
				filterFieldName = value;
			}
			get
			{
				return filterFieldName;
			}
		}

		#endregion

		private void TextBox_TextChanged(object sender, EventArgs e)
		{
			this.TextBox.TextChanged -= new EventHandler(TextBox_TextChanged);
			this.isEditing = true;
		}

		/// <summary>
		/// Reset the iteration count to 0.
		/// </summary>
		public void ResetIterations()
		{
			currentIteration = 0;
		}

		/// <summary>
		/// The actual work is done in this method. 
		/// - First verify if there are rows, find out from CurrencyManager
		/// - increment iteration count
		/// - calculate height of cell at iterationCount - 1
		/// - if iterationCount is rowcount, reset the iterationcount
		/// - return
		/// </summary>
		/// <returns>The height of the requested row.</returns>
		protected override int GetMinimumHeight()
		{
			if (!AllowExpand) return base.GetMinimumHeight();

			ExtendedDataGrid dg = this.DataGridTableStyle.DataGrid as ExtendedDataGrid;
			if (dg == null || (dg != null && !dg.AutoRowHeight)) return base.GetMinimumHeight();

			try
			{
				StackFrame frame = new StackFrame(4);
				MethodBase method = frame.GetMethod();
				string s = method.DeclaringType.FullName;
				if (s.EndsWith("DataGridAddNewRow"))
				{
					this.ResetIterations();
					return base.GetMinimumHeight();
				}
				else
				{

					CurrencyManager cur = (CurrencyManager)this.DataGridTableStyle.DataGrid.BindingContext
						[this.DataGridTableStyle.DataGrid.DataSource, this.DataGridTableStyle.DataGrid.DataMember];
					if (cur == null || cur.Count == 0)
						return base.GetMinimumHeight();

					this.currentIteration++;

					int rowNum = dg.TableStyles[0].GridColumnStyles.IndexOf(this);
					int retVal = base.GetMinimumHeight();

					int stringHeight = this.CalcStringHeight(GetColumnValueAtRow(cur, currentIteration - 1).ToString());
					if (currentIteration == cur.Count)
						this.ResetIterations();
					return retVal > stringHeight ? retVal : stringHeight;
				}
			}
			catch
			{
				// Error, return default
				this.ResetIterations();
				return base.GetMinimumHeight();
			}
		}

		/// <summary>
		/// Calculates the height of the supplied string.
		/// </summary>
		/// <param name="s"></param>
		/// <returns></returns>
		private int CalcStringHeight(string s)
		{
			try
			{
				// Create graphics for calculation
				System.Drawing.Graphics g = this.TextBox.CreateGraphics();
				// Do measure, and add a bit (4 pixels for me)
				return (int)g.MeasureString(s.Trim(), this.TextBox.Font, this.Width).Height + 4;
			}
			catch
			{
				// Error, return default font height.
				return base.GetMinimumHeight();
			}
		}
		protected override void EnterNullValue()
		{
			base.EnterNullValue();
		}
	}

	#region CellFormatting Event

	public delegate void FormatCellEventHandler(object sender, DataGridFormatCellEventArgs e);
	public delegate void ValidateCellEventHandler(object sender, DataGridValidateCellEventArgs e);

	public class DataGridFormatCellEventArgs : DataGridCellEventArgs
	{
		//private int colNum;
		//private int rowNum;
		private Font fontVal;
		private Brush backBrushVal;
		private Brush foreBrushVal;
		private bool fontDispose;
		private bool backBrushDispose;
		private bool foreBrushDispose;
		private bool useBaseClassDrawingVal;
		//private object currentCellValue;

		public DataGridFormatCellEventArgs(object cellValue, object currentRow)
			: base(cellValue, currentRow)
		{
			//			rowNum = row;
			//			colNum = col;
			fontVal = null;
			backBrushVal = null;
			foreBrushVal = null;
			fontDispose = false;
			backBrushDispose = false;
			foreBrushDispose = false;
			useBaseClassDrawingVal = true;
			//			currentCellValue = cellValue;
		}


		//		//column being painted
		//		public int Column
		//		{
		//			get{ return colNum;}
		//			set{ colNum = value;}
		//		}
		//
		//		//row being painted
		//		public int Row
		//		{
		//			get{ return rowNum;}
		//			set{ rowNum = value;}
		//		}

		//font used for drawing the text
		public Font TextFont
		{
			get { return fontVal; }
			set { fontVal = value; }
		}

		//background brush
		public Brush BackBrush
		{
			get { return backBrushVal; }
			set { backBrushVal = value; }
		}

		//foreground brush
		public Brush ForeBrush
		{
			get { return foreBrushVal; }
			set { foreBrushVal = value; }
		}

		//set true if you want the Paint method to call Dispose on the font
		public bool TextFontDispose
		{
			get { return fontDispose; }
			set { fontDispose = value; }
		}

		//set true if you want the Paint method to call Dispose on the brush
		public bool BackBrushDispose
		{
			get { return backBrushDispose; }
			set { backBrushDispose = value; }
		}

		//set true if you want the Paint method to call Dispose on the brush
		public bool ForeBrushDispose
		{
			get { return foreBrushDispose; }
			set { foreBrushDispose = value; }
		}

		//set true if you want the Paint method to call base class
		public bool UseBaseClassDrawing
		{
			get { return useBaseClassDrawingVal; }
			set { useBaseClassDrawingVal = value; }
		}

		//contains the current cell value
		//		public object CurrentCellValue
		//		{
		//			get{ return currentCellValue;}
		//		}
	}
	/// <summary>
	///  ласс с предпологаемыми параметрами редактируемой ¤чейки
	/// </summary>
	public class DataGridValidateCellEventArgs : EventArgs
	{
		private CurrencyManager _dataSource;
		private int _rowNum;
		private bool _isCommit = false;
		private object _value;

		/// <summary>
		/// —оздает класс с параметрами редактируемой ¤чейки
		/// </summary>
		/// <param name="dataSource">»сточник данных.</param>
		/// <param name="rowNum">Ќомер строки.</param>
		/// <param name="value">ѕредпологаемое значение ¤чейки.</param>
		public DataGridValidateCellEventArgs(CurrencyManager dataSource, int rowNum, object value)
		{
			_dataSource = dataSource;
			_rowNum = rowNum;
			_value = value;
		}

		/// <summary>
		/// »сточник данных
		/// </summary>
		public CurrencyManager CurrencyManager
		{
			get { return _dataSource; }
			set { _dataSource = value; }
		}

		/// <summary>
		/// Ќомер строки ¤чейки
		/// </summary>
		public int RowNum
		{
			get { return _rowNum; }
			set { _rowNum = value; }
		}

		/// <summary>
		/// ѕодтверждать ли предпологаемое значение ¤ченйки.
		/// </summary>
		public bool IsCommit
		{
			get { return _isCommit; }
			set { _isCommit = value; }
		}

		/// <summary>
		/// ѕредпологаемое значение ¤чейки
		/// </summary>
		public object Value
		{
			get { return _value; }
			set { _value = value; }
		}

	}
	#endregion
}