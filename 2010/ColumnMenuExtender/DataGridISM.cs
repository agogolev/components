﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Xml;
using System.Text;
using System.Windows.Forms;
using MetaData;
using System.Text.RegularExpressions;
using System.ComponentModel.Design;
using System.Drawing.Design;
using System.Runtime.InteropServices;
using System.Reflection;

namespace ColumnMenuExtender
{
	public enum CombinationType { AND, OR };

	public class DataGridISM : ExtendedDataGrid
	{
		public DataGrid.HitTestInfo Hit;
		private string stockClass;
		private int stockPageNum = 1;
		private int stockNumInBatch = 30;
		private string keyFields = "";

		private OrderInfo order = null;
		[Browsable(false)]
		public Hashtable Filters { get; private set; }
		[Browsable(false)]
		public Hashtable StorableFilters { get; private set; }//хранимые фильтры (которые в форме добавляем)
		[Browsable(false)]
		public Hashtable QueryParams { get; private set; }//параметры запроса (точно так же как в SQL)
		[Browsable(false)]
		public Hashtable Additionals { get; private set; }
		

		private bool canEdit = false;
		private string orderColumnName = null;
		private OrderDir orderOrderDir = OrderDir.Asc;

		public CombinationType FilterCombinationType;
		public string FilterString = "";
		public event EventHandler FilterSet;
		[DefaultValue(false)]
		public new bool CaptionVisible
		{
			get
			{
				return base.CaptionVisible;
			}
			set
			{
				base.CaptionVisible = value;
			}
		}
		[DefaultValue(typeof(System.Drawing.Color), "Color.White")]
		public new Color BackgroundColor
		{
			get
			{
				return base.BackgroundColor;
			}
			set
			{
				base.BackgroundColor = value;
			}
		}
		public DataGridISM() //: base()
		{
			Filters = new Hashtable();
			StorableFilters = new Hashtable();
			QueryParams = new Hashtable();
			Additionals = new Hashtable();
			CaptionVisible = false;
			BackgroundColor = Color.White;
		}

		public XmlDocument GetDataXml()
		{
			setOrder();

			XmlDocument xml = new XmlDocument();

			XmlNode root = xml.CreateNode(XmlNodeType.Element, "list", "");
			XmlNode node, node2;

			XmlAttribute attr = xml.CreateAttribute("class");
			attr.Value = StockClass;
			root.Attributes.Append(attr);

			attr = xml.CreateAttribute("pageNum");
			attr.Value = StockPageNum.ToString();
			root.Attributes.Append(attr);

			attr = xml.CreateAttribute("numInBatch");
			attr.Value = (StockNumInBatch < 0 ? "30" : StockNumInBatch.ToString());
			root.Attributes.Append(attr);

			attr = xml.CreateAttribute("keyFields");
			attr.Value = keyFields;
			root.Attributes.Append(attr);

			if (multi_OIDs != null)
			{
				attr = xml.CreateAttribute("OID");
				StringBuilder sb = new StringBuilder();
				for (int i = 0; i < multi_OIDs.Length; i++)
				{
					sb.Append(multi_OIDs[i].ToString() + ",");
				}
				attr.Value = sb.ToString().Substring(0, sb.Length - 1);
				root.Attributes.Append(attr);

				attr = xml.CreateAttribute("propName");
				attr.Value = multi_propName.ToString();
				root.Attributes.Append(attr);

				attr = xml.CreateAttribute("classContainer");
				attr.Value = multi_classContainer.ToString();
				root.Attributes.Append(attr);
			}

			xml.AppendChild(root);

			//Проверяем не является ли запрос запросом в новом формате
			bool isNew = false;
			if (TableStyles != null && TableStyles.Count > 0)
			{
				foreach (DataGridColumnStyle style in TableStyles[0].GridColumnStyles)
				{
					if (style is IExtendedQuery)
					{
						if (((IExtendedQuery)style).FieldName != null && ((IExtendedQuery)style).FieldName != "")
						{
							isNew = true;
							break;
						}
					}
				}
			}

			if (!isNew)
			{//!isNew	Старый формат запроса (без FieldNames)

				StringBuilder fieldNames = new StringBuilder();
				if (TableStyles != null && TableStyles.Count > 0)
				{
					foreach (DataGridColumnStyle style in TableStyles[0].GridColumnStyles)
					{
						fieldNames.Append(style.MappingName + ",");
					}
					fieldNames.Remove(fieldNames.Length - 1, 1);
				}

				node = xml.CreateNode(XmlNodeType.Element, "fieldNames", "");
				node.InnerText = fieldNames.ToString();
				root.AppendChild(node);
			}//!isNew

			else
			{//isNew	Новый формат запроса (с FieldNames)
				node = xml.CreateNode(XmlNodeType.Element, "fieldNames", "");
				root.AppendChild(node);

				if (TableStyles != null && TableStyles.Count > 0)
				{
					foreach (DataGridColumnStyle style in TableStyles[0].GridColumnStyles)
					{
						XmlNode childField = xml.CreateNode(XmlNodeType.Element, "field", "");
						XmlAttribute attrName = xml.CreateAttribute("name");

						if (style is IExtendedQuery)
						{
							if (((IExtendedQuery)style).FieldName != "" && ((IExtendedQuery)style).FieldName != null)
							{
								if (string.Compare(((IExtendedQuery)style).FieldName, "ignore", true) == 0) continue;
								attrName.Value = ((IExtendedQuery)style).FieldName;
								XmlAttribute attrColumnName = xml.CreateAttribute("columnName");
								attrColumnName.Value = style.MappingName;
								childField.Attributes.Append(attrColumnName);
							}
							else //нет FieldName - используем MappingName
								if (style.MappingName != "")
									attrName.Value = style.MappingName;

							if (attrName.Value != "")
							{
								childField.Attributes.Append(attrName);
								node.AppendChild(childField);
							}
						}
					}
				}

				//Params
				node2 = xml.CreateNode(XmlNodeType.Element, "params", "");
				root.AppendChild(node2);
				foreach (DictionaryEntry de in QueryParams)
				{
					XmlNode paramNode = xml.CreateNode(XmlNodeType.Element, "param", "");

					node2.AppendChild(paramNode);

					attr = xml.CreateAttribute("name");
					attr.Value = de.Key.ToString();
					paramNode.Attributes.Append(attr);

					attr = xml.CreateAttribute("value");
					attr.Value = de.Value.ToString();
					paramNode.Attributes.Append(attr);

					attr = xml.CreateAttribute("type");
					attr.Value = de.Value.GetType().ToString();
					paramNode.Attributes.Append(attr);
				}
			}//isNew

			//FILTERS & ORDER
			node2 = xml.CreateNode(XmlNodeType.Element, "filters", "");
			root.AppendChild(node2);

			//Если задана строка фильтров
			if (FilterString != null && FilterString != "")
			{
				//1 необходимо проверить все ли фильтры указаны в строке, если нет, то необходимо добавить их в конце
				//попутно проверим у всех ли фильтров есть filterName, если нет то подсовываем туда columnName
				string filterStringTemp = FilterString;
				foreach (DictionaryEntry de in Filters)
				{
					if (((FilterInfo)de.Value).FilterName == null || ((FilterInfo)de.Value).FilterName == "")
						((FilterInfo)de.Value).FilterName = ((FilterInfo)de.Value).ColumnName;

					if (FilterString.IndexOf(((FilterInfo)de.Value).FilterName) == -1)
						filterStringTemp = "(" + filterStringTemp + ") AND " + ((FilterInfo)de.Value).FilterName;
				}

				foreach (DictionaryEntry de in StorableFilters)
				{
					if (((FilterInfo)de.Value).FilterName == null || ((FilterInfo)de.Value).FilterName == "")
						((FilterInfo)de.Value).FilterName = ((FilterInfo)de.Value).ColumnName;

					if (FilterString.IndexOf(((FilterInfo)de.Value).FilterName) == -1)
						filterStringTemp = "(" + filterStringTemp + ") AND " + ((FilterInfo)de.Value).FilterName;
				}

				//2
				attr = xml.CreateAttribute("filterString");
				attr.Value = filterStringTemp;
				node2.Attributes.Append(attr);
			}

			if (FilterCombinationType != CombinationType.AND)
			{
				attr = xml.CreateAttribute("combinationType");
				attr.Value = "OR";
				node2.Attributes.Append(attr);
			}

			foreach (DictionaryEntry de in Filters)
			{
				((FilterInfo)de.Value).CreateXmlNode(node2);
			}

			foreach (DictionaryEntry de in StorableFilters)
			{
				((FilterInfo)de.Value).CreateXmlNode(node2);
			}

			node2 = xml.CreateNode(XmlNodeType.Element, "orders", "");
			root.AppendChild(node2);

			if (order != null)
			{
				order.CreateXmlNode(node2);
			}
			//Если запрос в новом формате, то необходимо заменить ColumnName с MappingName на FieldName
			//или на FilterFieldName, если он указан (причём FilterFieldName важней FieldName)
			if (isNew)
			{
				//filters
				foreach (XmlNode a in xml.DocumentElement.SelectNodes("filters/filter/@ColumnName"))
				{
					foreach (DataGridColumnStyle style in TableStyles[0].GridColumnStyles)
					{
						if (a.InnerText == style.MappingName && style is IExtendedQuery)
						{
							if (((IExtendedQuery)style).FilterFieldName != "" && ((IExtendedQuery)style).FilterFieldName != null)
							{
								a.InnerText = ((IExtendedQuery)style).FilterFieldName;
								break;
							}
							else if (((IExtendedQuery)style).FieldName != "" && ((IExtendedQuery)style).FieldName != null)
							{
								a.InnerText = ((IExtendedQuery)style).FieldName;
								break;
							}
						}
					}
				}
				//orders
				foreach (XmlNode a in xml.DocumentElement.SelectNodes("orders/order/@ColumnName"))
				{
					foreach (DataGridColumnStyle style in TableStyles[0].GridColumnStyles)
					{
						if (a.InnerText == style.MappingName && style is IExtendedQuery)
						{
							if (((IExtendedQuery)style).FieldName != "" && ((IExtendedQuery)style).FieldName != null)
							{
								a.InnerText = ((IExtendedQuery)style).FieldName;
								break;
							}
						}
					}
				}
			}

			//ADDITIONALS - любые произвольные дополнительные параметры
			//Params
			node = xml.CreateNode(XmlNodeType.Element, "additionals", "");
			root.AppendChild(node);
			foreach (DictionaryEntry de in Additionals)
			{
				XmlNode paramNode = xml.CreateNode(XmlNodeType.Element, "var", "");

				node.AppendChild(paramNode);

				attr = xml.CreateAttribute("name");
				attr.Value = de.Key.ToString();
				paramNode.Attributes.Append(attr);

				attr = xml.CreateAttribute("value");
				attr.Value = de.Value.ToString();
				paramNode.Attributes.Append(attr);

				attr = xml.CreateAttribute("type");
				attr.Value = de.Value.GetType().ToString();
				paramNode.Attributes.Append(attr);
			}

			return xml;
		}

		private void InitializeComponent()
		{
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		public void CreateAuxFilter(string columnName, FilterVerb verb, bool negation, object[] values)
		{
			CreateAuxFilter(columnName, "", verb, false, values);
		}

		public void DeleteFilter(string filterName)
		{
			foreach (DictionaryEntry de in Filters)
			{
				if (string.Compare(((FilterInfo)de.Value).FilterName, filterName, true) == 0)
				{
					Filters.Remove(de.Key);
					break;
				}
			}
		}
		public void CreateAuxFilter(string columnName, string filterName, FilterVerb verb, bool negation, object[] values)
		{
			FilterInfo filter;
			if (values != null && values.Length > 0)
			{
				if (values[0] is Int32)
				{
					filter = new IntegerFilterInfo();
					((IntegerFilterInfo)filter).Values = new int[values.Length];
					for (int i = 0; i < values.Length; i++)
						((IntegerFilterInfo)filter).Values[i] = (int)values[i];
				}
				else if (values[0] is String)
				{
					filter = new StringFilterInfo();
					((StringFilterInfo)filter).Values = new string[values.Length];
					for (int i = 0; i < values.Length; i++)
						((StringFilterInfo)filter).Values[i] = (string)values[i];
				}
				else if (values[0] is Double)
				{
					filter = new DoubleFilterInfo();
					((DoubleFilterInfo)filter).Values = new double[values.Length];
					for (int i = 0; i < values.Length; i++)
						((DoubleFilterInfo)filter).Values[i] = (double)values[i];
				}
				else if (values[0] is Guid)
				{
					filter = new ObjectFilterInfo();
					((ObjectFilterInfo)filter).Values = new Guid[values.Length];
					for (int i = 0; i < values.Length; i++)
						((ObjectFilterInfo)filter).Values[i] = (Guid)values[i];
				}
				else if (values[0] is DateTime)
				{
					filter = new DateTimeFilterInfo();
					((DateTimeFilterInfo)filter).Values = new DateTime[values.Length];
					for (int i = 0; i < values.Length; i++)
						((DateTimeFilterInfo)filter).Values[i] = (DateTime)values[i];
				}
				else if (values[0] is bool)
				{
					filter = new IntegerFilterInfo();
					((IntegerFilterInfo)filter).Values = new int[values.Length];
					for (int i = 0; i < values.Length; i++)
						((IntegerFilterInfo)filter).Values[i] = (bool)values[i] ? 1 : 0;
				}
				else throw new ApplicationException("Cannot create filter!");

				filter.ColumnName = columnName;
				filter.FilterName = filterName;
				filter.Verb = verb;
				filter.Negation = negation;

				Filters[filter.FilterName.ToLower()] = filter;
			}
			else
			{
				if (verb == FilterVerb.IsNull)
				{
					filter = new StringFilterInfo();
					filter.ColumnName = columnName;
					filter.FilterName = filterName;
					filter.Verb = verb;
					filter.Negation = negation;
					Filters[filter.FilterName.ToLower()] = filter;
				}
				else throw new ApplicationException("Cannot create filter!");
			}
		}

		private string multi_classContainer = "";
		private string multi_propName = "";
		private Guid[] multi_OIDs = null;

		public void SetMultiFilter(Guid OID, string classContainer, string propName)
		{
			if (OID != Guid.Empty)
			{
				multi_OIDs = new Guid[] { OID };
				multi_classContainer = classContainer;
				multi_propName = propName;
			}
			else
			{
				multi_OIDs = null;
				multi_classContainer = "";
				multi_propName = "";
			}
		}

		public void SetMultiFilter(Guid[] OIDs, string classContainer, string propName)
		{
			multi_OIDs = new Guid[OIDs.Length];
			for (int i = 0; i < OIDs.Length; i++)
				multi_OIDs[i] = OIDs[i];
			multi_classContainer = classContainer;
			multi_propName = propName;
		}

		private void setOrder()
		{
			if (order == null)//firstTime)
			{
				if (orderColumnName != null)
				{
					order = new OrderInfo(orderColumnName, orderOrderDir);

				}
				//firstTime = false;
			}
		}

		[
		Bindable(true),
		Browsable(true),
		DefaultValue("")
		]
		public string StockClass
		{
			get
			{
				if (stockClass == null)
					return "";
				else
					return stockClass;
			}
			set
			{
				stockClass = value;
			}
		}

		[
		Bindable(true),
		Browsable(true),
		DefaultValue(1)
		]
		public int StockPageNum
		{
			get
			{
				return stockPageNum;
			}
			set
			{
				if (stockPageNum != value)
				{
					stockPageNum = value;
					this.OnReload(new EventArgs());
				}
			}
		}

		[
		Bindable(true),
		Browsable(true),
		DefaultValue(30)
		]
		public int StockNumInBatch
		{
			get
			{
				return stockNumInBatch;
			}
			set
			{
				if (stockNumInBatch != value)
				{
					stockNumInBatch = value;
					stockPageNum = 1;
					this.OnReload(new EventArgs());
				}
			}
		}
		public void SetNumInBatchWOReload(int value)
		{
			stockNumInBatch = value;
			stockPageNum = 1;
		}

		[Browsable(true), DefaultValue("")]
		public string KeyFields
		{
			get { return keyFields; }
			set { keyFields = value; }
		}

		[Browsable(true), DefaultValue(false),
			Category("Хлам"), Description("Уже нифига не используется")]
		public bool CanEdit
		{
			get { return canEdit; }
			set { canEdit = value; }
		}

		[
		DefaultValue("")
		]
		public string OrderColumnName
		{
			get
			{
				if (orderColumnName == null)
					return "";
				else
					return orderColumnName;
			}
			set
			{
				orderColumnName = value;
			}
		}

		[
		DefaultValue(OrderDir.Asc)
		]
		public OrderDir OrderOrderDir
		{
			get { return orderOrderDir; }
			set { orderOrderDir = value; }
		}

		[Browsable(false)]
		public OrderInfo Order
		{
			get { return order; }
			set { order = value; }
		}

		[Browsable(false)]
		public int HorizScrollBarPos
		{
			get { return this.HorizScrollBar.Value; }
		}

		protected static readonly object EventReload = new object();
		protected static readonly object EventActionKeyPressed = new object();

		[Category("Action"), Description("Raised when the DataGrid trying to Reload Data")]
		public event EventHandler Reload
		{
			add
			{
				Events.AddHandler(EventReload, value);
			}
			remove
			{
				Events.RemoveHandler(EventReload, value);
			}
		}

		[Category("Action"), Description("Edit, New, Delete keys event handler")]
		public event KeyEventHandler ActionKeyPressed
		{
			add
			{
				Events.AddHandler(EventActionKeyPressed, value);
			}
			remove
			{
				Events.RemoveHandler(EventActionKeyPressed, value);
			}
		}

		public virtual void OnReload(EventArgs e)
		{
			EventHandler initHandler = (EventHandler)Events[EventReload];
			if (initHandler != null)
			{
				initHandler(this, e);
			}
		}

		public void OnFilterSet(string columnName)
		{
			if (FilterSet != null)
				FilterSet(this, new FilterSetEventArgs(columnName));
		}

		public virtual void OnActionKeyPressed(KeyEventArgs e)
		{
			KeyEventHandler initHandler = (KeyEventHandler)Events[EventActionKeyPressed];
			if (initHandler != null)
			{
				initHandler(this, e);
			}
		}

		private bool paintEnabled = true;
		protected override void OnPaint(PaintEventArgs e)
		{
			if (paintEnabled)
			{
				base.OnPaint(e);
				if (DesignMode) return;
				if (this.TableStyles.Count == 0) return;
				//рисуем значки фильтров в заголовках столбцов
				Type type = this.GetType().BaseType.BaseType;
				FieldInfo fi = type.GetField("layout", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
				object layout = fi.GetValue(this);

				Type layoutType = layout.GetType();
				fi = layoutType.GetField("ColumnHeaders", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
				Rectangle ColumnHeaders = (Rectangle)fi.GetValue(layout);

				e.Graphics.SetClip(ColumnHeaders);
				if (this.ColumnHeadersVisible && this.DataSource != null)
				{
					for (int index = 0; index < this.TableStyles[0].GridColumnStyles.Count; index++)
					{
						DataGridColumnStyle columnStyle = this.TableStyles[0].GridColumnStyles[index] as DataGridColumnStyle;

						if (columnStyle != null)
						{
							if (!((StorableFilters != null && StorableFilters.ContainsKey(columnStyle.MappingName.ToLower())) || (Order != null && Order.ColumnName.ToLower() == columnStyle.MappingName.ToLower())))
								continue;

							int width = columnStyle.Width; //получаем ширину заголовка

							//получаем х-координату рисования прямоугольника заголовка
							int xCoordinate = (this.RowHeadersVisible) ? this.RowHeaderWidth : 0;
							for (int i = 0; i < index; i++)
							{
								DataGridColumnStyle dgcs = this.TableStyles[0].GridColumnStyles[i];
								xCoordinate += dgcs.Width;
							}

							xCoordinate = xCoordinate - this.HorizScrollBarPos;
							int filterWidth = 5;
							int rightOffset = 5;
							int toptOffset = (ColumnHeaders.Height - filterWidth) / 2;
							if (width < filterWidth + rightOffset)
								continue;
							int start = xCoordinate + width - (filterWidth + rightOffset);
							e.Graphics.FillRectangle(SystemBrushes.Control, start, 3, filterWidth + rightOffset - 1, ColumnHeaders.Height - 6);
							e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
							e.Graphics.FillEllipse(SystemBrushes.ControlText, start, toptOffset, filterWidth, filterWidth);
						}
					}
				}
			}
		}

		protected override void OnMouseDown(MouseEventArgs e)
		{
			Hit = this.HitTest(e.X, e.Y);
			base.OnMouseDown(e);
		}


		public ArrayList GetMultiSelectedRows()
		{
			ArrayList arrRows = new ArrayList();
			if (DataSource == null) return null;
			DataView dataView = null;
			if (DataMember == "") dataView = (DataView)((CurrencyManager)BindingContext[DataSource]).List;
			else dataView = (DataView)((CurrencyManager)BindingContext[DataSource, DataMember]).List;
			int rowCount = dataView.Count;
			for (int i = 0; i < rowCount; i++)
			{
				if (IsSelected(i))
					arrRows.Add(dataView[i].Row);
			}
			return arrRows;
		}

		protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
		{
			const int WM_KEYDOWN = 0x100;
			const int WM_SYSKEYDOWN = 0x104;

			//если эта яйчейка редактируема, то необходимо вызвать базовый метод
			if ((msg.Msg == WM_KEYDOWN || msg.Msg == WM_SYSKEYDOWN) && (this.TableStyles["table"] != null && (this.TableStyles["table"].GridColumnStyles[this.CurrentCell.ColumnNumber].ReadOnly == true ||
							this.TableStyles["table"].ReadOnly == true || this.ReadOnly == true)))
			{
				switch (keyData)
				{
					case Keys.Left:
					case Keys.Right:
						//если есть редактируемые колонки, то необходимо переходить на них(на самую ближайшую)!
						DataGridCell cell = getClosestNotReadOnlyColumn(keyData, this.CurrentCell.ColumnNumber);
						if (cell.RowNumber != -1)
							this.BeginEdit(this.TableStyles["table"].GridColumnStyles[cell.ColumnNumber], cell.RowNumber);
						else//если нет то на другую строку!
							move(keyData);
						return true;
					case Keys.Up:
					case (Keys.Shift | Keys.Tab):
					case Keys.Down:
					case Keys.Tab:
						move(keyData);
						return true;
					case Keys.Enter:
					case Keys.Delete:
					case Keys.Insert:
						OnActionKeyPressed(new KeyEventArgs(keyData));
						//this.Select();
						return true;
					case Keys.F5:
						OnReload(new EventArgs());
						this.Select();
						return true;
				}
			}
			return base.ProcessCmdKey(ref msg, keyData);
		}

		//данный метод возвращает первую редактируемую колонку относительно данной позиции (если есть)
		private DataGridCell getClosestNotReadOnlyColumn(Keys key, int start)
		{
			if (this.TableStyles["table"].ReadOnly != true && this.ReadOnly != true && isNotReadOnly())
			{
				int x = searchNotReadOnly(key, start);
				if (x != -1)
					return new DataGridCell(CurrentRowIndex, x);
				if (move(key))//если не нашли то поднимаемся/спускаемся и пытаемся найти там
				{
					if (key == Keys.Left)
						return getClosestNotReadOnlyColumn(key, this.TableStyles["table"].GridColumnStyles.Count);
					else if (key == Keys.Right)
						return getClosestNotReadOnlyColumn(key, -1);
				}
			}
			return new DataGridCell(-1, -1);//нету вообще ничего
		}

		private int searchNotReadOnly(Keys key, int start)
		{
			if (key == Keys.Left)//пытаемся найти слева редактирумую колонку
			{
				for (int i = start - 1; i >= 0; i--)
				{
					if (this.TableStyles["table"].GridColumnStyles[i].ReadOnly != true)
						return i; ;
				}
			}
			else if (key == Keys.Right)//пытаемся найти справа редактирумую колонку
			{
				for (int i = start + 1; i < this.TableStyles["table"].GridColumnStyles.Count; i++)
				{
					if (this.TableStyles["table"].GridColumnStyles[i].ReadOnly != true)
						return i; ;
				}
			}
			return -1;//ничего на этой строке не нашли
		}

		private bool move(Keys key)
		{
			switch (key)
			{
				case Keys.Left:
				case Keys.Up:
				case (Keys.Shift | Keys.Tab):
					if (CurrentRowIndex >= 1)
					{
						this.UnSelect(CurrentRowIndex);
						CurrentRowIndex--;
						this.Select(CurrentRowIndex);
						return true;
					}
					break;
				case Keys.Right:
				case Keys.Down:
				case Keys.Tab:
					if ((CurrentRowIndex + 1) < TableInGrid.GetTable(this).Rows.Count)
					{
						this.UnSelect(CurrentRowIndex);
						CurrentRowIndex++;
						this.Select(CurrentRowIndex);
						return true;
					}
					break;
			}
			return false;
		}

		//проверяет, существует ли хоть одна редактируемая колонка в данном датагриде
		private bool isNotReadOnly()
		{
			foreach (DataGridColumnStyle dc in this.TableStyles["table"].GridColumnStyles)
			{
				if (dc.ReadOnly != true)
					return true;
			}
			return false;
		}

		public void ScrollToRow(int theRow)
		{
			//
			// Expose the protected GridVScrolled method allowing you
			// to programmatically scroll the grid to a particular row.
			//
			if (DataSource != null)
			{
				GridVScrolled(this, new ScrollEventArgs(ScrollEventType.LargeIncrement, theRow));
			}
		}
		public static XmlDocument GetDataXmlNow(string stock_class, string page_num, string num_in_batch, string fieldNames, FilterInfo filter)
		{
			XmlDocument xml = new XmlDocument();

			XmlNode root = xml.CreateNode(XmlNodeType.Element, "list", "");
			XmlNode node;

			XmlAttribute attr = xml.CreateAttribute("class");
			attr.Value = stock_class;
			root.Attributes.Append(attr);

			attr = xml.CreateAttribute("pageNum");
			attr.Value = page_num;
			root.Attributes.Append(attr);

			attr = xml.CreateAttribute("numInBatch");
			attr.Value = num_in_batch;
			root.Attributes.Append(attr);

			xml.AppendChild(root);

			/*StringBuilder fieldNames = new StringBuilder();
			if (TableStyles != null)
			{
				foreach (DataGridColumnStyle style in TableStyles[0].GridColumnStyles)
				{
					fieldNames.Append(style.MappingName + ",");
				}
				fieldNames.Remove(fieldNames.Length - 1, 1);
			}
*/
			node = xml.CreateNode(XmlNodeType.Element, "fieldNames", "");
			node.InnerText = fieldNames.ToString();
			root.AppendChild(node);

			//FILTERS & ORDER
			XmlNode node2 = xml.CreateNode(XmlNodeType.Element, "filters", "");
			root.AppendChild(node2);

			if (filter != null)
				filter.CreateXmlNode(node2);

			return xml;
		}

		public static XmlDocument GetDataXmlNow(string stock_class, string page_num, string num_in_batch, string fieldNames, FilterInfo filter, OrderInfo order)
		{
			XmlDocument xml = new XmlDocument();

			XmlNode root = xml.CreateNode(XmlNodeType.Element, "list", "");
			XmlNode node;

			XmlAttribute attr = xml.CreateAttribute("class");
			attr.Value = stock_class;
			root.Attributes.Append(attr);

			attr = xml.CreateAttribute("pageNum");
			attr.Value = page_num;
			root.Attributes.Append(attr);

			attr = xml.CreateAttribute("numInBatch");
			attr.Value = num_in_batch;
			root.Attributes.Append(attr);

			xml.AppendChild(root);

			/*StringBuilder fieldNames = new StringBuilder();
			if (TableStyles != null)
			{
				foreach (DataGridColumnStyle style in TableStyles[0].GridColumnStyles)
				{
					fieldNames.Append(style.MappingName + ",");
				}
				fieldNames.Remove(fieldNames.Length - 1, 1);
			}
*/
			node = xml.CreateNode(XmlNodeType.Element, "fieldNames", "");
			node.InnerText = fieldNames.ToString();
			root.AppendChild(node);

			//FILTERS & ORDER
			XmlNode node2 = xml.CreateNode(XmlNodeType.Element, "filters", "");
			root.AppendChild(node2);

			if (filter != null)
				filter.CreateXmlNode(node2);

			node2 = xml.CreateNode(XmlNodeType.Element, "orders", "");
			root.AppendChild(node2);

			if (order != null)
				order.CreateXmlNode(node2);

			return xml;
		}

		public static FilterInfo CreateFilter(string columnName, string filterName, FilterVerb verb, bool negation, object[] values)
		{
			FilterInfo filter;
			if (values != null && values.Length > 0)
			{
				if (values[0] is Int32)
				{
					filter = new IntegerFilterInfo();
					((IntegerFilterInfo)filter).Values = new int[values.Length];
					for (int i = 0; i < values.Length; i++)
						((IntegerFilterInfo)filter).Values[i] = (int)values[i];
				}
				else if (values[0] is String)
				{
					filter = new StringFilterInfo();
					((StringFilterInfo)filter).Values = new string[values.Length];
					for (int i = 0; i < values.Length; i++)
						((StringFilterInfo)filter).Values[i] = (string)values[i];
				}
				else if (values[0] is Double)
				{
					filter = new DoubleFilterInfo();
					((DoubleFilterInfo)filter).Values = new double[values.Length];
					for (int i = 0; i < values.Length; i++)
						((DoubleFilterInfo)filter).Values[i] = (double)values[i];
				}
				else if (values[0] is Guid)
				{
					filter = new ObjectFilterInfo();
					((ObjectFilterInfo)filter).Values = new Guid[values.Length];
					for (int i = 0; i < values.Length; i++)
						((ObjectFilterInfo)filter).Values[i] = (Guid)values[i];
				}
				else throw new ApplicationException("Cannot create filter!");

				filter.ColumnName = columnName;
				filter.FilterName = filterName;
				filter.Verb = verb;
				filter.Negation = negation;

				return filter;
			}
			else
			{
				if (verb == FilterVerb.IsNull)
				{
					filter = new StringFilterInfo();
					filter.ColumnName = columnName;
					filter.FilterName = filterName;
					filter.Verb = verb;
					filter.Negation = negation;
					return filter;
				}
				else throw new ApplicationException("Cannot create filter!");
			}
		}

		public static FilterInfo CreateFilter(string columnName, string filterName, FilterVerb verb, bool negation, object value)
		{
			FilterInfo filter;
			if (value != null)
			{
				if (value is Int32)
				{
					filter = new IntegerFilterInfo();
					((IntegerFilterInfo)filter).Values = new int[] { (int)value };
				}
				else if (value is String)
				{
					filter = new StringFilterInfo();
					((StringFilterInfo)filter).Values = new string[] { (string)value };
				}
				else if (value is Double)
				{
					filter = new DoubleFilterInfo();
					((DoubleFilterInfo)filter).Values = new double[] { (double)value };
				}
				else if (value is Guid)
				{
					filter = new ObjectFilterInfo();
					((ObjectFilterInfo)filter).Values = new Guid[] { (Guid)value };
				}
				else throw new ApplicationException("Cannot create filter!");

				filter.ColumnName = columnName;
				filter.FilterName = filterName;
				filter.Verb = verb;
				filter.Negation = negation;

				return filter;
			}
			else
			{
				if (verb == FilterVerb.IsNull)
				{
					filter = new StringFilterInfo();
					filter.ColumnName = columnName;
					filter.FilterName = filterName;
					filter.Verb = verb;
					filter.Negation = negation;
					return filter;
				}
				else throw new ApplicationException("Cannot create filter!");
			}
		}
	}

	public class FilterSetEventArgs : EventArgs
	{
		public string columnName = "";
		public FilterSetEventArgs(string columnName)
		{
			this.columnName = columnName;
		}
	}
}
